#ifndef __PARAMS__HybridDatapath__
#define __PARAMS__HybridDatapath__

class HybridDatapath;

#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include <string>
#include <cstddef>
#include <string>
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include <string>
#include <cstddef>
#include <string>
#include <cstddef>
#include <string>
#include <cstddef>
#include <string>
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include <cstddef>
#include <cstddef>
#include <cstddef>
#include <string>
#include <cstddef>
#include <cstddef>
#include <cstddef>
#include <string>
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include <string>
#include <cstddef>
#include <cstddef>
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "params/System.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include <string>
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include <string>
#include <cstddef>
#include <cstddef>
#include <cstddef>

#include "params/MemObject.hh"

struct HybridDatapathParams
    : public MemObjectParams
{
    HybridDatapath * create();
    int acceleratorId;
    std::string acceleratorName;
    std::string benchName;
    int cacheAssoc;
    int cacheBandwidth;
    int cacheHitLatency;
    unsigned cacheLineFlushLatency;
    unsigned cacheLineInvalidateLatency;
    int cacheLineSize;
    int cacheQueueSize;
    std::string cacheSize;
    std::string cactiCacheConfig;
    std::string cactiCacheQueueConfig;
    std::string configFileName;
    unsigned cycleTime;
    unsigned dmaChunkSize;
    unsigned dmaSetupOverhead;
    bool enableAcp;
    bool enableStatsDump;
    bool executeStandalone;
    std::string experimentName;
    bool ignoreCacheFlush;
    bool invalidateOnDmaStore;
    std::string l2cacheSize;
    unsigned maxDmaRequests;
    unsigned numDmaChannels;
    int numOutStandingWalks;
    std::string outputPrefix;
    bool pipelinedDma;
    bool recordMemoryTrace;
    unsigned spadPorts;
    System * system;
    int tlbAssoc;
    int tlbBandwidth;
    std::string tlbCactiConfig;
    int tlbEntries;
    Cycles tlbHitLatency;
    Cycles tlbMissLatency;
    int tlbPageBytes;
    std::string traceFileName;
    bool useAcpCache;
    bool useAladdinDebugger;
    bool useDb;
    unsigned int port_acp_port_connection_count;
    unsigned int port_cache_port_connection_count;
    unsigned int port_spad_port_connection_count;
};

#endif // __PARAMS__HybridDatapath__
