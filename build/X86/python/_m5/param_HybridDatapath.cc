#include "pybind11/pybind11.h"
#include "pybind11/stl.h"

#include "params/HybridDatapath.hh"
#include "python/pybind11/core.hh"
#include "sim/init.hh"
#include "sim/sim_object.hh"

#include "aladdin/gem5/HybridDatapath.h"

#include "base/types.hh"
#include <string>
#include <string>
#include "base/types.hh"
#include "base/types.hh"
#include "base/types.hh"
#include "base/types.hh"
#include "base/types.hh"
#include "base/types.hh"
#include "base/types.hh"
#include <string>
#include <string>
#include <string>
#include <string>
#include "base/types.hh"
#include "base/types.hh"
#include "base/types.hh"
#include <string>
#include <string>
#include "base/types.hh"
#include "base/types.hh"
#include "base/types.hh"
#include <string>
#include "base/types.hh"
#include "sim/system.hh"
#include "base/types.hh"
#include "base/types.hh"
#include <string>
#include "base/types.hh"
#include "base/types.hh"
#include "base/types.hh"
#include "base/types.hh"
#include <string>
namespace py = pybind11;

static void
module_init(py::module &m_internal)
{
    py::module m = m_internal.def_submodule("param_HybridDatapath");
    py::class_<HybridDatapathParams, MemObjectParams, std::unique_ptr<HybridDatapathParams, py::nodelete>>(m, "HybridDatapathParams")
        .def(py::init<>())
        .def("create", &HybridDatapathParams::create)
        .def_readwrite("acceleratorId", &HybridDatapathParams::acceleratorId)
        .def_readwrite("acceleratorName", &HybridDatapathParams::acceleratorName)
        .def_readwrite("benchName", &HybridDatapathParams::benchName)
        .def_readwrite("cacheAssoc", &HybridDatapathParams::cacheAssoc)
        .def_readwrite("cacheBandwidth", &HybridDatapathParams::cacheBandwidth)
        .def_readwrite("cacheHitLatency", &HybridDatapathParams::cacheHitLatency)
        .def_readwrite("cacheLineFlushLatency", &HybridDatapathParams::cacheLineFlushLatency)
        .def_readwrite("cacheLineInvalidateLatency", &HybridDatapathParams::cacheLineInvalidateLatency)
        .def_readwrite("cacheLineSize", &HybridDatapathParams::cacheLineSize)
        .def_readwrite("cacheQueueSize", &HybridDatapathParams::cacheQueueSize)
        .def_readwrite("cacheSize", &HybridDatapathParams::cacheSize)
        .def_readwrite("cactiCacheConfig", &HybridDatapathParams::cactiCacheConfig)
        .def_readwrite("cactiCacheQueueConfig", &HybridDatapathParams::cactiCacheQueueConfig)
        .def_readwrite("configFileName", &HybridDatapathParams::configFileName)
        .def_readwrite("cycleTime", &HybridDatapathParams::cycleTime)
        .def_readwrite("dmaChunkSize", &HybridDatapathParams::dmaChunkSize)
        .def_readwrite("dmaSetupOverhead", &HybridDatapathParams::dmaSetupOverhead)
        .def_readwrite("enableAcp", &HybridDatapathParams::enableAcp)
        .def_readwrite("enableStatsDump", &HybridDatapathParams::enableStatsDump)
        .def_readwrite("executeStandalone", &HybridDatapathParams::executeStandalone)
        .def_readwrite("experimentName", &HybridDatapathParams::experimentName)
        .def_readwrite("ignoreCacheFlush", &HybridDatapathParams::ignoreCacheFlush)
        .def_readwrite("invalidateOnDmaStore", &HybridDatapathParams::invalidateOnDmaStore)
        .def_readwrite("l2cacheSize", &HybridDatapathParams::l2cacheSize)
        .def_readwrite("maxDmaRequests", &HybridDatapathParams::maxDmaRequests)
        .def_readwrite("numDmaChannels", &HybridDatapathParams::numDmaChannels)
        .def_readwrite("numOutStandingWalks", &HybridDatapathParams::numOutStandingWalks)
        .def_readwrite("outputPrefix", &HybridDatapathParams::outputPrefix)
        .def_readwrite("pipelinedDma", &HybridDatapathParams::pipelinedDma)
        .def_readwrite("recordMemoryTrace", &HybridDatapathParams::recordMemoryTrace)
        .def_readwrite("spadPorts", &HybridDatapathParams::spadPorts)
        .def_readwrite("system", &HybridDatapathParams::system)
        .def_readwrite("tlbAssoc", &HybridDatapathParams::tlbAssoc)
        .def_readwrite("tlbBandwidth", &HybridDatapathParams::tlbBandwidth)
        .def_readwrite("tlbCactiConfig", &HybridDatapathParams::tlbCactiConfig)
        .def_readwrite("tlbEntries", &HybridDatapathParams::tlbEntries)
        .def_readwrite("tlbHitLatency", &HybridDatapathParams::tlbHitLatency)
        .def_readwrite("tlbMissLatency", &HybridDatapathParams::tlbMissLatency)
        .def_readwrite("tlbPageBytes", &HybridDatapathParams::tlbPageBytes)
        .def_readwrite("traceFileName", &HybridDatapathParams::traceFileName)
        .def_readwrite("useAcpCache", &HybridDatapathParams::useAcpCache)
        .def_readwrite("useAladdinDebugger", &HybridDatapathParams::useAladdinDebugger)
        .def_readwrite("useDb", &HybridDatapathParams::useDb)
        .def_readwrite("port_acp_port_connection_count", &HybridDatapathParams::port_acp_port_connection_count)
        .def_readwrite("port_cache_port_connection_count", &HybridDatapathParams::port_cache_port_connection_count)
        .def_readwrite("port_spad_port_connection_count", &HybridDatapathParams::port_spad_port_connection_count)
        ;

    py::class_<HybridDatapath, MemObject, ScratchpadDatapath, std::unique_ptr<HybridDatapath, py::nodelete>>(m, "HybridDatapath")
        ;

}

static EmbeddedPyBind embed_obj("HybridDatapath", module_init, "MemObject");
