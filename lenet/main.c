/*
 * A pure c implementation of a lenet suitable for cifar 10.
 * 
 * Ben Schreiber 5/4/2019
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "main.h"

int main() {
    printf("dear god...\n");

    srand(0);

    printf("wow! it's a %f\n", run());

    return 0;
}

float
run(void) {
    float input[BATCH_SIZE][3*32*32];
    float kern1[6][3][5][5];
    float kern2[16][6][5][5];
    float weight1[120][16*5*5];
    float weight2[84][120];
    float weight3[10][84];

    init(input, kern1, kern2, weight1, weight2, weight3);

    float **out1 = (float**)malloc(sizeof(float*) * BATCH_SIZE);
    float **out2 = (float**)malloc(sizeof(float*) * BATCH_SIZE);
    float **out3 = (float**)malloc(sizeof(float*) * BATCH_SIZE);
    float **out4 = (float**)malloc(sizeof(float*) * BATCH_SIZE);
    float **out5 = (float**)malloc(sizeof(float*) * BATCH_SIZE);
    float **out6 = (float**)malloc(sizeof(float*) * BATCH_SIZE);
    float **out7 = (float**)malloc(sizeof(float*) * BATCH_SIZE);

    for (int i = 0; i < BATCH_SIZE; i++) {
        out1[i] = (float*)malloc(sizeof(float) * 6*28*28);
        out2[i] = (float*)malloc(sizeof(float) * 6*14*14);
        out3[i] = (float*)malloc(sizeof(float) * 16*10*10);
        out4[i] = (float*)malloc(sizeof(float) * 16*5*5);
        out5[i] = (float*)malloc(sizeof(float) * 120);
        out6[i] = (float*)malloc(sizeof(float) * 84);
        out7[i] = (float*)malloc(sizeof(float) * 10);
    }

    float ret = forward(input, kern1, kern2, weight1, weight2, weight3,
            out1, out2, out3, out4, out5, out6, out7); 

    for (int i = 0; i < BATCH_SIZE; i++) {
        free(out1[i]);
        free(out2[i]);
        free(out3[i]);
        free(out4[i]);
        free(out5[i]);
        free(out6[i]);
        free(out7[i]);
    }

    free(out1);
    free(out2);
    free(out3);
    free(out4);
    free(out5);
    free(out6);
    free(out7);

    return ret;
}

void init(float input[BATCH_SIZE][3*32*32],
          float kern1[6][3][5][5],
          float kern2[16][6][5][5],
          float weight1[120][16*5*5],
          float weight2[84][120],
          float weight3[10][84]) {
    for (int i = 0; i < BATCH_SIZE; i++)
        for (int j = 0; j < 3*32*32; j++)
            input[i][j] = (float)rand() / 10000000000 - 1.0;
    for (int i = 0; i < 6; i++)
        for (int j = 0; j < 3; j++)
            for (int k = 0; k < 5; k++)
                for (int ii = 0; ii < 5; ii++)
                    kern1[i][j][k][ii] = (float)rand() / 1000000000 - 1.0;
    for (int i = 0; i < 16; i++)
        for (int j = 0; j < 6; j++)
            for (int k = 0; k < 5; k++)
                for (int ii = 0; ii < 5; ii++)
                    kern2[i][j][k][ii] = (float)rand() / 10000000000 - 1.0;
    for (int i = 0; i < 120; i++)
        for (int j = 0; j < 16*5*5; j++)
            weight1[i][j] = (float)rand() / 1000000000 - 1.0;
    for (int i = 0; i < 84; i++)
        for (int j = 0; j < 120; j++)
            weight2[i][j] = (float)rand() / 10000000000 - 1.0;
    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 84; j++) {
            weight3[i][j] = (float)rand() / 1000000000 - 1.0;
        }
    }

}

float
forward(float input[BATCH_SIZE][3*32*32],
        float kern1[6][3][5][5],
        float kern2[16][6][5][5],
        float weight1[120][16*5*5],
        float weight2[84][120],
        float weight3[10][84],
        float** out1, float** out2, float** out3, float** out4,
        float** out5, float** out6, float** out7) {
    conv1(kern1, input, out1);
    relu(out1, 6*28*28);
    pool1(out1, out2);
    conv2(kern2, out2, out3);
    relu(out3, 16*10*10);
    pool2(out3, out4);
    fc1(out4, weight1, out5);
    relu(out5, 120);
    fc2(out5, weight2, out6);
    relu(out6, 84);
    fc3(out6, weight3, out7);

    float acc = 0;
    for (int b = 0; b < BATCH_SIZE; b++) {
        for (int i = 0; i < 10; i++) {
            acc += out7[b][i];
        }
    }

    return acc;
}

// 3 in to 5 out convolution with 5x5 kernel
void 
conv1(float kernel[6][3][5][5], 
      float input[BATCH_SIZE][3*32*32], 
      float ** output) {

    for (int b = 0; b < BATCH_SIZE; b++) {
        for (int c_out = 0; c_out < 6; c_out++) {
            for (int y = 2; y < 30; y++) {
                for (int x = 2; x < 30; x++) {
                    // Inner loop
                    float acc = 0;
                    for (int c_in = 0; c_in < 3; c_in++) {
                        for (int off_y = -2; off_y <= 2; off_y++) {
                            for (int off_x = -2; off_x <= 2; off_x++) {
                                acc += input[b][c_in*1024 + (y+off_y)*32 + x+off_x] * kernel[c_out][c_in][off_y+2][off_x+2];
                            }
                        }
                    }
                    output[b][c_out*784 + (y-2)*28 + x-2] = acc;
                }
            }
        }
    }
}

// 6 in to 16 out convolution with 5x5 kernel
void 
conv2(float kernel[16][6][5][5], 
      float **input,
      float **output) {

    for (int b = 0; b < BATCH_SIZE; b++) {
        for (int c_out = 0; c_out < 16; c_out++) {
            for (int y = 2; y < 12; y++) {
                for (int x = 2; x < 12; x++) {
                    // Inner loop
                    float acc = 0;
                    for (int c_in = 0; c_in < 6; c_in++) {
                        for (int off_y = -2; off_y <= 2; off_y++) {
                            for (int off_x = -2; off_x <= 2; off_x++) {
                                acc += input[b][c_in*196 + (y+off_y)*14 + x+off_x] * kernel[c_out][c_in][off_y+2][off_x+2];
                            }
                        }
                    }
                    output[b][c_out*100 + (y-2)*10 + x-2] = acc;
                }
            }
        }
    }
}

// Max pool kernel 2 stride 2
void 
pool1(float **input,
      float **output) {

    for (int b = 0; b < BATCH_SIZE; b++) {
        for (int c = 0; c < 6; c++) {
            for (int y = 0; y < 14; y++) {
                for (int x = 0; x < 14; x++) {
                    float top_max = fmaxf(input[b][c*784 + (2*y)*28 + 2*x], input[b][c*784 + (2*y)*28 + 2*x+1]);
                    float bot_max = fmaxf(input[b][c*784 + (2*y+1)*28 + 2*x], input[b][c*784 + (2*y+1)*28 + 2*x+1]);
                    output[b][c*196 + y*14 + x] = fmaxf(top_max, bot_max);
                }
            }
        }
    }
}

// Max pool kernel 2 stride 2
void 
pool2(float **input,
      float **output) {

    for (int b = 0; b < BATCH_SIZE; b++) {
        for (int c = 0; c < 16; c++) {
            for (int y = 0; y < 5; y++) {
                for (int x = 0; x < 5; x++) {
                    float top_max = fmaxf(input[b][c*100 + (2*y)*10 + 2*x], input[b][c*100 + (2*y)*10 + 2*x+1]);
                    float bot_max = fmaxf(input[b][c*100 + (2*y+1)*10 + 2*x], input[b][c*100 + (2*y+1)*10 + 2*x+1]);
                    output[b][c*25 + y*5 + x] = fmaxf(top_max, bot_max);
                }
            }
        }
    }
}

// fully connected
void 
fc1(float ** input,
    float weights[120][16*5*5],
    float ** output) {

    for (int b = 0; b < BATCH_SIZE; b++) {
        for (int i = 0; i < 120; i++) {
            float acc = 0;
            for (int j = 0; j < 16*5*5; j++) {
                acc += weights[i][j] * input[b][j];
            }
            output[b][i] = acc;
        }
    }
}

void 
fc2(float ** input,
    float weights[84][120],
    float ** output) {

    for (int b = 0; b < BATCH_SIZE; b++) {
        for (int i = 0; i < 84; i++) {
            float acc = 0;
            for (int j = 0; j < 120; j++) {
                acc += weights[i][j] * input[b][j];
            }
            output[b][i] = acc;
        }
    }
}

void
fc3(float ** input,
    float weights[10][84],
    float ** output) {

    for (int b = 0; b < BATCH_SIZE; b++) {
        for (int i = 0; i < 10; i++) {
            float acc = 0;
            for (int j = 0; j < 84; j++) {
                acc += weights[i][j] * input[b][j];
            }
            output[b][i] = acc;
        }
    }
}

void
relu(float** arr, unsigned len) {
    for (int b = 0; b < BATCH_SIZE; b++) {
        for (unsigned i = 0; i < len; i++) {
            arr[b][i] = arr[b][i] > 0.0 ? arr[b][i] : 0.01 * arr[b][i];
        }
    }
}
