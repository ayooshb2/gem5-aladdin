#define BATCH_SIZE 128

float
run(void);

float
forward(float input[BATCH_SIZE][3*32*32],
        float kern1[6][3][5][5],
        float kern2[16][6][5][5],
        float weight1[120][16*5*5],
        float weight2[84][120],
        float weight3[10][84],
        float **out1, float **out2, float **out3, float **out4,
        float **out5, float **out6, float **out7);

void
init(float input[BATCH_SIZE][3*32*32],
     float kern1[6][3][5][5],
     float kern2[16][6][5][5],
     float weight1[120][16*5*5],
     float weight2[84][120],
     float weight3[10][84]);

void 
conv1(float kernel[6][3][5][5], 
      float input[BATCH_SIZE][3*32*32],
      float **output);

void 
conv2(float kernel[16][6][5][5], 
      float **input, 
      float **output);

void 
pool1(float **input,
      float **output);

void 
pool2(float **input,
      float **output);

void 
fc1(float **input,
    float weights[120][16*5*5],
    float **output);

void 
fc2(float **input,
    float weights[84][120],
    float **output);

void
fc3(float **input,
    float weights[10][84],
    float **output);

void
relu(float **arr, unsigned len);

