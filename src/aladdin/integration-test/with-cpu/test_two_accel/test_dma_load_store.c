/* Test stores performed in the kernel.
 *
 * The values stored should be able to be loaded by the CPU after the kernel is
 * finished.
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "aladdin_sys_connection.h"
#include "aladdin_sys_constants.h"
#include "store_kernel/store_kernel.h"
#include "test_kernel/test_kernel.h"
#define TYPE int

#ifdef DMA_MODE
#include "gem5/dma_interface.h"
#endif

#define CACHELINE_SIZE 64

int main() {
  const int num_vals = 1024;
  TYPE *store_vals, *store_loc;
  int err = posix_memalign(
      (void**)&store_vals, CACHELINE_SIZE, sizeof(TYPE) * num_vals);
  assert(err == 0 && "Failed to allocate memory!");
  err = posix_memalign(
      (void**)&store_loc, CACHELINE_SIZE, sizeof(TYPE) * num_vals);
  assert(err == 0 && "Failed to allocate memory!");
  for (int i = 0; i < num_vals; i++) {
    store_vals[i] = i;
    store_loc[i] = -1;
  }

#ifdef GEM5_HARNESS
  mapArrayToAccelerator(
      INTEGRATION_TEST, "store_vals", &(store_vals[0]), num_vals * sizeof(int));
  mapArrayToAccelerator(
      INTEGRATION_TEST, "store_loc", &(store_loc[0]), num_vals * sizeof(int));
  fprintf(stdout,"Accelerator 1 mapped!\n");
  mapArrayToAccelerator(
      INTEGRATION_TEST+1, "store_vals", &(store_vals[0]), num_vals * sizeof(int));
  mapArrayToAccelerator(
      INTEGRATION_TEST+1, "store_loc", &(store_loc[0]), num_vals * sizeof(int));
  fprintf(stdout,"Accelerator 2 mapped!\n");

  fprintf(stdout, "Invoking accelerator 1!\n");
  invokeAcceleratorAndBlock(INTEGRATION_TEST);
  fprintf(stdout, "Accelerator 1 finished!\n");
  
  fprintf(stdout, "Invoking accelerator 2!\n");
  invokeAcceleratorAndBlock(INTEGRATION_TEST+1);
  fprintf(stdout, "Accelerator 2 finished!\n");
#else
  store_kernel(store_vals, store_loc, num_vals);
  test_kernel(store_vals, store_loc, num_vals);
#endif

  int num_failures = test_stores_plus(store_vals, store_loc, num_vals);
  if (num_failures != 0) {
    fprintf(stdout, "Test failed with %d errors.", num_failures);
    return -1;
  }
  fprintf(stdout, "Test passed!\n");
  return 0;
}
