	.file	"full.llvm"
	.section	.debug_info,"",@progbits
.Lsection_info:
	.section	.debug_abbrev,"",@progbits
.Lsection_abbrev:
	.section	.debug_aranges,"",@progbits
	.section	.debug_macinfo,"",@progbits
	.section	.debug_line,"",@progbits
.Lsection_line:
	.section	.debug_loc,"",@progbits
	.section	.debug_pubnames,"",@progbits
	.section	.debug_pubtypes,"",@progbits
	.section	.debug_str,"MS",@progbits,1
.Linfo_string:
	.section	.debug_ranges,"",@progbits
.Ldebug_range:
	.section	.debug_loc,"",@progbits
.Lsection_debug_loc:
	.text
.Ltext_begin:
	.data
	.file	1 "store_kernel.c"
	.file	2 "/workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c"
	.text
	.globl	store_kernel
	.align	16, 0x90
	.type	store_kernel,@function
store_kernel:                           # @store_kernel
	.cfi_startproc
.Lfunc_begin0:
	.loc	1 24 0                  # store_kernel.c:24:0
# BB#0:
	pushq	%rbp
.Ltmp7:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Ltmp8:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Ltmp9:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Ltmp10:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Ltmp11:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Ltmp12:
	.cfi_def_cfa_offset 56
	subq	$1128, %rsp             # imm = 0x468
.Ltmp13:
	.cfi_def_cfa_offset 1184
.Ltmp14:
	.cfi_offset %rbx, -56
.Ltmp15:
	.cfi_offset %r12, -48
.Ltmp16:
	.cfi_offset %r13, -40
.Ltmp17:
	.cfi_offset %r14, -32
.Ltmp18:
	.cfi_offset %r15, -24
.Ltmp19:
	.cfi_offset %rbp, -16
	movabsq	$3, %rax
	movabsq	$64, %rcx
	movabsq	$1, %r8
	leaq	.L.str12, %r9
	movabsq	$0, %r10
	leaq	.L.str9, %r11
	movabsq	$2, %rbx
	leaq	.L.str8, %r14
	movabsq	$4, %r15
	leaq	dmaLoad, %r12
	leaq	.L.str15, %r13
	movabsq	$26, %rbp
	movq	%rax, 1120(%rsp)        # 8-byte Spill
	leaq	.L.str1, %rax
	movq	%rax, 1112(%rsp)        # 8-byte Spill
	leaq	.L.str6, %rax
	movq	%rax, 1104(%rsp)        # 8-byte Spill
	leaq	.L.str14, %rax
	movq	%rax, 1096(%rsp)        # 8-byte Spill
	movabsq	$99, %rax
	movl	%edx, 1092(%rsp)        # 4-byte Spill
	movl	$1, %edx
	movq	%rax, 1080(%rsp)        # 8-byte Spill
	movabsq	$19134, %rax            # imm = 0x4ABE
	movq	%rax, 1072(%rsp)        # 8-byte Spill
	leaq	.L.str10, %rax
	movq	%rax, 1064(%rsp)        # 8-byte Spill
	leaq	.L.str13, %rax
	movq	%rax, 1056(%rsp)        # 8-byte Spill
	movabsq	$20, %rax
	movq	%rax, 1048(%rsp)        # 8-byte Spill
	movabsq	$32, %rax
	movq	%rax, 1040(%rsp)        # 8-byte Spill
	leaq	.L.str11, %rax
	movq	%rax, 1032(%rsp)        # 8-byte Spill
	movabsq	$35, %rax
	movq	%rax, 1024(%rsp)        # 8-byte Spill
	leaq	.L.str7, %rax
	movq	%rax, 1016(%rsp)        # 8-byte Spill
	movabsq	$44, %rax
	movq	%rax, 1008(%rsp)        # 8-byte Spill
	leaq	.L.str17, %rax
	.loc	1 24 0 prologue_end     # store_kernel.c:24:0
.Ltmp20:
	movq	%rax, 1000(%rsp)        # 8-byte Spill
	movq	1112(%rsp), %rax        # 8-byte Reload
	movq	%rdi, 992(%rsp)         # 8-byte Spill
	movq	%rax, %rdi
	movq	1120(%rsp), %rax        # 8-byte Reload
	movq	%rsi, 984(%rsp)         # 8-byte Spill
	movq	%rax, %rsi
	movq	%r13, 976(%rsp)         # 8-byte Spill
	movq	%rbp, 968(%rsp)         # 8-byte Spill
	movl	%edx, 964(%rsp)         # 4-byte Spill
	movq	%rcx, 952(%rsp)         # 8-byte Spill
	movq	%r8, 944(%rsp)          # 8-byte Spill
	movq	%r9, 936(%rsp)          # 8-byte Spill
	movq	%r10, 928(%rsp)         # 8-byte Spill
	movq	%r11, 920(%rsp)         # 8-byte Spill
	movq	%rbx, 912(%rsp)         # 8-byte Spill
	movq	%r14, 904(%rsp)         # 8-byte Spill
	movq	%r15, 896(%rsp)         # 8-byte Spill
	movq	%r12, 888(%rsp)         # 8-byte Spill
	callq	trace_logger_log_entry
	movq	944(%rsp), %rdi         # 8-byte Reload
	movq	952(%rsp), %rsi         # 8-byte Reload
	movq	992(%rsp), %rdx         # 8-byte Reload
	movq	944(%rsp), %rcx         # 8-byte Reload
	movq	904(%rsp), %r8          # 8-byte Reload
	movq	928(%rsp), %r9          # 8-byte Reload
	movq	920(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	912(%rsp), %rdi         # 8-byte Reload
	movq	952(%rsp), %rsi         # 8-byte Reload
	movq	984(%rsp), %rdx         # 8-byte Reload
	movq	944(%rsp), %rcx         # 8-byte Reload
	movq	1000(%rsp), %r8         # 8-byte Reload
	movq	928(%rsp), %r9          # 8-byte Reload
	movq	920(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movl	1092(%rsp), %esi        # 4-byte Reload
	movl	%esi, %esi
	movl	%esi, %edx
	movq	1120(%rsp), %rdi        # 8-byte Reload
	movq	1040(%rsp), %rsi        # 8-byte Reload
	movq	944(%rsp), %rcx         # 8-byte Reload
	movq	1032(%rsp), %r8         # 8-byte Reload
	movq	928(%rsp), %r9          # 8-byte Reload
	movq	920(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	#DEBUG_VALUE: store_kernel:store_vals <- [RSP+992]
	#DEBUG_VALUE: store_kernel:store_loc <- [RSP+984]
	#DEBUG_VALUE: store_kernel:num_vals <- [RSP+1092]
	.loc	1 26 0                  # store_kernel.c:26:0
	movq	968(%rsp), %rdi         # 8-byte Reload
	movq	1112(%rsp), %rsi        # 8-byte Reload
	movq	1104(%rsp), %rdx        # 8-byte Reload
	movq	1016(%rsp), %rcx        # 8-byte Reload
	movq	1008(%rsp), %r8         # 8-byte Reload
	movl	964(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	944(%rsp), %rdi         # 8-byte Reload
	movq	952(%rsp), %rsi         # 8-byte Reload
	movq	992(%rsp), %rdx         # 8-byte Reload
	movq	944(%rsp), %rcx         # 8-byte Reload
	movq	904(%rsp), %r8          # 8-byte Reload
	movq	928(%rsp), %r9          # 8-byte Reload
	movq	920(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	992(%rsp), %rax         # 8-byte Reload
	movq	1072(%rsp), %rdi        # 8-byte Reload
	movq	952(%rsp), %rsi         # 8-byte Reload
	movq	%rax, %rdx
	movq	944(%rsp), %rcx         # 8-byte Reload
	movq	904(%rsp), %r8          # 8-byte Reload
	movq	928(%rsp), %r9          # 8-byte Reload
	movq	920(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movq	%rax, 880(%rsp)         # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	968(%rsp), %rdi         # 8-byte Reload
	movq	1112(%rsp), %rsi        # 8-byte Reload
	movq	1104(%rsp), %rdx        # 8-byte Reload
	movq	1064(%rsp), %rcx        # 8-byte Reload
	movq	1024(%rsp), %r8         # 8-byte Reload
	movl	964(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movl	1092(%rsp), %r9d        # 4-byte Reload
	movl	%r9d, %esi
	movl	%esi, %edx
	movq	944(%rsp), %rdi         # 8-byte Reload
	movq	1040(%rsp), %rsi        # 8-byte Reload
	movq	944(%rsp), %rcx         # 8-byte Reload
	movq	1032(%rsp), %r8         # 8-byte Reload
	movq	928(%rsp), %r9          # 8-byte Reload
	movq	920(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	1092(%rsp), %esi        # 4-byte Reload
	movslq	%esi, %rax
	movq	1072(%rsp), %rdi        # 8-byte Reload
	movq	952(%rsp), %rsi         # 8-byte Reload
	movq	%rax, %rdx
	movq	944(%rsp), %rcx         # 8-byte Reload
	movq	1064(%rsp), %r8         # 8-byte Reload
	movq	928(%rsp), %r9          # 8-byte Reload
	movq	920(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movq	%rax, 872(%rsp)         # 8-byte Spill
	callq	trace_logger_log_int
	movq	968(%rsp), %rdi         # 8-byte Reload
	movq	1112(%rsp), %rsi        # 8-byte Reload
	movq	1104(%rsp), %rdx        # 8-byte Reload
	movq	936(%rsp), %rcx         # 8-byte Reload
	movq	1048(%rsp), %r8         # 8-byte Reload
	movl	964(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	912(%rsp), %rdi         # 8-byte Reload
	movq	952(%rsp), %rsi         # 8-byte Reload
	movq	912(%rsp), %rdx         # 8-byte Reload
	movq	928(%rsp), %rcx         # 8-byte Reload
	movq	1056(%rsp), %r8         # 8-byte Reload
	movq	928(%rsp), %r9          # 8-byte Reload
	movq	920(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	944(%rsp), %rdi         # 8-byte Reload
	movq	952(%rsp), %rsi         # 8-byte Reload
	movq	872(%rsp), %rdx         # 8-byte Reload
	movq	944(%rsp), %rcx         # 8-byte Reload
	movq	1064(%rsp), %r8         # 8-byte Reload
	movq	928(%rsp), %r9          # 8-byte Reload
	movq	920(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	872(%rsp), %rax         # 8-byte Reload
	shlq	$2, %rax
	movq	1072(%rsp), %rdi        # 8-byte Reload
	movq	952(%rsp), %rsi         # 8-byte Reload
	movq	%rax, %rdx
	movq	944(%rsp), %rcx         # 8-byte Reload
	movq	936(%rsp), %r8          # 8-byte Reload
	movq	928(%rsp), %r9          # 8-byte Reload
	movq	920(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movq	%rax, 864(%rsp)         # 8-byte Spill
	callq	trace_logger_log_int
	movq	968(%rsp), %rdi         # 8-byte Reload
	movq	1112(%rsp), %rsi        # 8-byte Reload
	movq	1104(%rsp), %rdx        # 8-byte Reload
	movq	1096(%rsp), %rcx        # 8-byte Reload
	movq	1080(%rsp), %r8         # 8-byte Reload
	movl	964(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	896(%rsp), %rdi         # 8-byte Reload
	movq	952(%rsp), %rsi         # 8-byte Reload
	movq	888(%rsp), %rdx         # 8-byte Reload
	movq	944(%rsp), %rcx         # 8-byte Reload
	movq	976(%rsp), %r8          # 8-byte Reload
	movq	928(%rsp), %r9          # 8-byte Reload
	movq	920(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	944(%rsp), %rdi         # 8-byte Reload
	movq	952(%rsp), %rsi         # 8-byte Reload
	movq	880(%rsp), %rdx         # 8-byte Reload
	movq	944(%rsp), %rcx         # 8-byte Reload
	movq	904(%rsp), %r8          # 8-byte Reload
	movq	928(%rsp), %r9          # 8-byte Reload
	movq	920(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	912(%rsp), %rdi         # 8-byte Reload
	movq	952(%rsp), %rsi         # 8-byte Reload
	movq	880(%rsp), %rdx         # 8-byte Reload
	movq	944(%rsp), %rcx         # 8-byte Reload
	movq	904(%rsp), %r8          # 8-byte Reload
	movq	928(%rsp), %r9          # 8-byte Reload
	movq	920(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	1120(%rsp), %rdi        # 8-byte Reload
	movq	952(%rsp), %rsi         # 8-byte Reload
	movq	864(%rsp), %rdx         # 8-byte Reload
	movq	944(%rsp), %rcx         # 8-byte Reload
	movq	936(%rsp), %r8          # 8-byte Reload
	movq	928(%rsp), %r9          # 8-byte Reload
	movq	920(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	880(%rsp), %rdi         # 8-byte Reload
	movq	880(%rsp), %rsi         # 8-byte Reload
	movq	864(%rsp), %rdx         # 8-byte Reload
	callq	dmaLoad
.Ltmp21:
	movabsq	$3, %rdi
	movabsq	$64, %rcx
	movabsq	$1, %rdx
	leaq	.L.str12, %r8
	movabsq	$0, %rsi
	leaq	.L.str9, %r9
	movabsq	$2, %r10
	leaq	.L.str17, %r11
	movabsq	$4, %rbx
	leaq	dmaLoad, %r14
	leaq	.L.str15, %r15
	movabsq	$27, %r12
	leaq	.L.str1, %r13
	leaq	.L.str6, %rbp
	movq	%rcx, 856(%rsp)         # 8-byte Spill
	leaq	.L.str18, %rcx
	movq	%rcx, 848(%rsp)         # 8-byte Spill
	movabsq	$99, %rcx
	movl	%eax, 844(%rsp)         # 4-byte Spill
	movl	$1, %eax
	movq	%rcx, 832(%rsp)         # 8-byte Spill
	movabsq	$19134, %rcx            # imm = 0x4ABE
	movq	%rcx, 824(%rsp)         # 8-byte Spill
	leaq	.L.str16, %rcx
	movq	%rcx, 816(%rsp)         # 8-byte Spill
	movabsq	$44, %rcx
	movq	%rcx, 808(%rsp)         # 8-byte Spill
	movabsq	$32, %rcx
	movq	%rcx, 800(%rsp)         # 8-byte Spill
	leaq	.L.str14, %rcx
	.loc	1 27 0                  # store_kernel.c:27:0
	movl	%eax, 796(%rsp)         # 4-byte Spill
	movl	844(%rsp), %eax         # 4-byte Reload
	movl	%eax, %eax
                                        # kill: RAX<def> EAX<kill>
	movq	%rax, 784(%rsp)         # 8-byte Spill
	movq	824(%rsp), %rax         # 8-byte Reload
	movq	%rdi, 776(%rsp)         # 8-byte Spill
	movq	%rax, %rdi
	movq	800(%rsp), %rax         # 8-byte Reload
	movq	%rsi, 768(%rsp)         # 8-byte Spill
	movq	%rax, %rsi
	movq	784(%rsp), %rax         # 8-byte Reload
	movq	%rdx, 760(%rsp)         # 8-byte Spill
	movq	%rax, %rdx
	movq	760(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 752(%rsp)         # 8-byte Spill
	movq	%rax, %rcx
	movq	752(%rsp), %rax         # 8-byte Reload
	movq	%r8, 744(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movq	768(%rsp), %rax         # 8-byte Reload
	movq	%r9, 736(%rsp)          # 8-byte Spill
	movq	%rax, %r9
	movq	736(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	movq	%rbp, 728(%rsp)         # 8-byte Spill
	movq	%r15, 720(%rsp)         # 8-byte Spill
	movq	%r12, 712(%rsp)         # 8-byte Spill
	movq	%r14, 704(%rsp)         # 8-byte Spill
	movq	%r13, 696(%rsp)         # 8-byte Spill
	movq	%rbx, 688(%rsp)         # 8-byte Spill
	movq	%r10, 680(%rsp)         # 8-byte Spill
	movq	%r11, 672(%rsp)         # 8-byte Spill
	callq	trace_logger_log_int
	movq	712(%rsp), %rdi         # 8-byte Reload
	movq	696(%rsp), %rsi         # 8-byte Reload
	movq	728(%rsp), %rdx         # 8-byte Reload
	movq	816(%rsp), %rcx         # 8-byte Reload
	movq	808(%rsp), %r8          # 8-byte Reload
	movl	796(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	760(%rsp), %rdi         # 8-byte Reload
	movq	856(%rsp), %rsi         # 8-byte Reload
	movq	984(%rsp), %rdx         # 8-byte Reload
	movq	760(%rsp), %rcx         # 8-byte Reload
	movq	672(%rsp), %r8          # 8-byte Reload
	movq	768(%rsp), %r9          # 8-byte Reload
	movq	736(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	984(%rsp), %rax         # 8-byte Reload
	movq	824(%rsp), %rdi         # 8-byte Reload
	movq	856(%rsp), %rsi         # 8-byte Reload
	movq	%rax, %rdx
	movq	760(%rsp), %rcx         # 8-byte Reload
	movq	672(%rsp), %r8          # 8-byte Reload
	movq	768(%rsp), %r9          # 8-byte Reload
	movq	736(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movq	%rax, 664(%rsp)         # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	712(%rsp), %rdi         # 8-byte Reload
	movq	696(%rsp), %rsi         # 8-byte Reload
	movq	728(%rsp), %rdx         # 8-byte Reload
	movq	848(%rsp), %rcx         # 8-byte Reload
	movq	832(%rsp), %r8          # 8-byte Reload
	movl	796(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	688(%rsp), %rdi         # 8-byte Reload
	movq	856(%rsp), %rsi         # 8-byte Reload
	movq	704(%rsp), %rdx         # 8-byte Reload
	movq	760(%rsp), %rcx         # 8-byte Reload
	movq	720(%rsp), %r8          # 8-byte Reload
	movq	768(%rsp), %r9          # 8-byte Reload
	movq	736(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	760(%rsp), %rdi         # 8-byte Reload
	movq	856(%rsp), %rsi         # 8-byte Reload
	movq	664(%rsp), %rdx         # 8-byte Reload
	movq	760(%rsp), %rcx         # 8-byte Reload
	movq	672(%rsp), %r8          # 8-byte Reload
	movq	768(%rsp), %r9          # 8-byte Reload
	movq	736(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	680(%rsp), %rdi         # 8-byte Reload
	movq	856(%rsp), %rsi         # 8-byte Reload
	movq	664(%rsp), %rdx         # 8-byte Reload
	movq	760(%rsp), %rcx         # 8-byte Reload
	movq	672(%rsp), %r8          # 8-byte Reload
	movq	768(%rsp), %r9          # 8-byte Reload
	movq	736(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	776(%rsp), %rdi         # 8-byte Reload
	movq	856(%rsp), %rsi         # 8-byte Reload
	movq	864(%rsp), %rdx         # 8-byte Reload
	movq	760(%rsp), %rcx         # 8-byte Reload
	movq	744(%rsp), %r8          # 8-byte Reload
	movq	768(%rsp), %r9          # 8-byte Reload
	movq	736(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	664(%rsp), %rdi         # 8-byte Reload
	movq	664(%rsp), %rsi         # 8-byte Reload
	movq	864(%rsp), %rdx         # 8-byte Reload
	callq	dmaLoad
	movabsq	$0, %rcx
	movabsq	$1, %rdx
	leaq	.L.str19, %rsi
	leaq	.L.str9, %rdi
	movabsq	$2, %r8
	leaq	.L.str22, %r9
	movabsq	$3, %r10
	leaq	.L.str21, %r11
	movabsq	$30, %rbx
	leaq	.L.str1, %r14
	leaq	.L.str6, %r15
	leaq	.L.str20, %r12
	movl	$1, %ebp
	movabsq	$19134, %r13            # imm = 0x4ABE
	movq	%rcx, 656(%rsp)         # 8-byte Spill
	movabsq	$32, %rcx
	movq	%rcx, 648(%rsp)         # 8-byte Spill
	leaq	.L.str11, %rcx
	movq	%rcx, 640(%rsp)         # 8-byte Spill
	leaq	.L.str13, %rcx
	movq	%rcx, 632(%rsp)         # 8-byte Spill
	movabsq	$46, %rcx
	movq	%rcx, 624(%rsp)         # 8-byte Spill
	leaq	.L.str18, %rcx
	.loc	1 30 0                  # store_kernel.c:30:0
.Ltmp22:
	movl	%eax, %eax
                                        # kill: RAX<def> EAX<kill>
	movq	%rdi, 616(%rsp)         # 8-byte Spill
	movq	%r13, %rdi
	movq	%rax, 608(%rsp)         # 8-byte Spill
	movq	648(%rsp), %rax         # 8-byte Reload
	movq	%rsi, 600(%rsp)         # 8-byte Spill
	movq	%rax, %rsi
	movq	608(%rsp), %rax         # 8-byte Reload
	movq	%rdx, 592(%rsp)         # 8-byte Spill
	movq	%rax, %rdx
	movq	592(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 584(%rsp)         # 8-byte Spill
	movq	%rax, %rcx
	movq	584(%rsp), %rax         # 8-byte Reload
	movq	%r8, 576(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movq	656(%rsp), %rax         # 8-byte Reload
	movq	%r9, 568(%rsp)          # 8-byte Spill
	movq	%rax, %r9
	movq	616(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	movq	%r13, 560(%rsp)         # 8-byte Spill
	movq	%r15, 552(%rsp)         # 8-byte Spill
	movq	%r12, 544(%rsp)         # 8-byte Spill
	movl	%ebp, 540(%rsp)         # 4-byte Spill
	movq	%r14, 528(%rsp)         # 8-byte Spill
	movq	%rbx, 520(%rsp)         # 8-byte Spill
	movq	%r10, 512(%rsp)         # 8-byte Spill
	movq	%r11, 504(%rsp)         # 8-byte Spill
	callq	trace_logger_log_int
.Ltmp23:
	#DEBUG_VALUE: i <- 0
	movq	520(%rsp), %rdi         # 8-byte Reload
	movq	528(%rsp), %rsi         # 8-byte Reload
	movq	552(%rsp), %rdx         # 8-byte Reload
	movq	600(%rsp), %rcx         # 8-byte Reload
	movq	624(%rsp), %r8          # 8-byte Reload
	movl	540(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	576(%rsp), %rdi         # 8-byte Reload
	movq	648(%rsp), %rsi         # 8-byte Reload
	movq	656(%rsp), %rdx         # 8-byte Reload
	movq	656(%rsp), %rcx         # 8-byte Reload
	movq	632(%rsp), %r8          # 8-byte Reload
	movq	656(%rsp), %r9          # 8-byte Reload
	movq	616(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	1092(%rsp), %ebp        # 4-byte Reload
	movl	%ebp, %esi
	movl	%esi, %edx
	movq	592(%rsp), %rdi         # 8-byte Reload
	movq	648(%rsp), %rsi         # 8-byte Reload
	movq	592(%rsp), %rcx         # 8-byte Reload
	movq	640(%rsp), %r8          # 8-byte Reload
	movq	656(%rsp), %r9          # 8-byte Reload
	movq	616(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	1092(%rsp), %ebp        # 4-byte Reload
	cmpl	$0, %ebp
	setg	%al
	movb	%al, %cl
	andb	$1, %cl
	movzbl	%cl, %ecx
	movl	%ecx, %edx
	movq	560(%rsp), %rdi         # 8-byte Reload
	movq	592(%rsp), %rsi         # 8-byte Reload
	movq	592(%rsp), %rcx         # 8-byte Reload
	movq	600(%rsp), %r8          # 8-byte Reload
	movq	656(%rsp), %r9          # 8-byte Reload
	movq	616(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movb	%al, 503(%rsp)          # 1-byte Spill
	callq	trace_logger_log_int
	movq	520(%rsp), %rdi         # 8-byte Reload
	movq	528(%rsp), %rsi         # 8-byte Reload
	movq	552(%rsp), %rdx         # 8-byte Reload
	movq	544(%rsp), %rcx         # 8-byte Reload
	movq	576(%rsp), %r8          # 8-byte Reload
	movl	540(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	512(%rsp), %rdi         # 8-byte Reload
	movq	656(%rsp), %rsi         # 8-byte Reload
	movq	656(%rsp), %rdx         # 8-byte Reload
	movq	592(%rsp), %rcx         # 8-byte Reload
	movq	504(%rsp), %r8          # 8-byte Reload
	movq	656(%rsp), %r9          # 8-byte Reload
	movq	616(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	576(%rsp), %rdi         # 8-byte Reload
	movq	656(%rsp), %rsi         # 8-byte Reload
	movq	656(%rsp), %rdx         # 8-byte Reload
	movq	592(%rsp), %rcx         # 8-byte Reload
	movq	568(%rsp), %r8          # 8-byte Reload
	movq	656(%rsp), %r9          # 8-byte Reload
	movq	616(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	503(%rsp), %al          # 1-byte Reload
	andb	$1, %al
	movzbl	%al, %ebp
	movl	%ebp, %edx
	movq	592(%rsp), %rdi         # 8-byte Reload
	movq	592(%rsp), %rsi         # 8-byte Reload
	movq	592(%rsp), %rcx         # 8-byte Reload
	movq	600(%rsp), %r8          # 8-byte Reload
	movq	656(%rsp), %r9          # 8-byte Reload
	movq	616(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	503(%rsp), %al          # 1-byte Reload
	testb	$1, %al
	movq	656(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, 488(%rsp)         # 8-byte Spill
	jne	.LBB0_1
	jmp	.LBB0_2
.Ltmp24:
.LBB0_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	488(%rsp), %rax         # 8-byte Reload
	movabsq	$1, %rcx
	leaq	.L.str30, %rdx
	movabsq	$0, %rsi
	leaq	.L.str9, %rdi
	movabsq	$2, %r8
	leaq	.L.str21, %r9
	movabsq	$3, %r10
	leaq	.L.str22, %r11
	movabsq	$30, %rbx
	leaq	.L.str1, %r14
	leaq	.L.str31, %r15
	movl	$1, %ebp
	movabsq	$19134, %r12            # imm = 0x4ABE
	movabsq	$32, %r13
	movq	%rax, 480(%rsp)         # 8-byte Spill
	leaq	.L.str29, %rax
	movq	%rax, 472(%rsp)         # 8-byte Spill
	leaq	.L.str11, %rax
	movq	%rax, 464(%rsp)         # 8-byte Spill
	movabsq	$46, %rax
	movq	%rax, 456(%rsp)         # 8-byte Spill
	movabsq	$64, %rax
	movq	%rax, 448(%rsp)         # 8-byte Spill
	leaq	.L.str24, %rax
	movq	%rax, 440(%rsp)         # 8-byte Spill
	movabsq	$33, %rax
	movq	%rax, 432(%rsp)         # 8-byte Spill
	leaq	.L.str23, %rax
	movq	%rax, 424(%rsp)         # 8-byte Spill
	leaq	.L.str13, %rax
	movq	%rax, 416(%rsp)         # 8-byte Spill
	movabsq	$8, %rax
	movq	%rax, 408(%rsp)         # 8-byte Spill
	leaq	.L.str26, %rax
	movq	%rax, 400(%rsp)         # 8-byte Spill
	leaq	.L.str27, %rax
	movq	%rax, 392(%rsp)         # 8-byte Spill
	movabsq	$31, %rax
	movq	%rax, 384(%rsp)         # 8-byte Spill
	leaq	.L.str28, %rax
	movq	%rax, 376(%rsp)         # 8-byte Spill
	movabsq	$28, %rax
	movq	%rax, 368(%rsp)         # 8-byte Spill
	leaq	.L.str17, %rax
	movq	%rax, 360(%rsp)         # 8-byte Spill
	movabsq	$29, %rax
	movq	%rax, 352(%rsp)         # 8-byte Spill
	leaq	.L.str25, %rax
	movq	%rax, 344(%rsp)         # 8-byte Spill
	movabsq	$27, %rax
	movq	%rax, 336(%rsp)         # 8-byte Spill
	leaq	.L.str8, %rax
	movq	%rax, 328(%rsp)         # 8-byte Spill
	leaq	.L.str6, %rax
	movq	%rax, 320(%rsp)         # 8-byte Spill
	movabsq	$-1, %rax
	movq	%rax, 312(%rsp)         # 8-byte Spill
	movabsq	$48, %rax
	.loc	1 31 0                  # store_kernel.c:31:0
	movq	%rax, 304(%rsp)         # 8-byte Spill
	movq	312(%rsp), %rax         # 8-byte Reload
	movq	%rdi, 296(%rsp)         # 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, 288(%rsp)         # 8-byte Spill
	movq	%r14, %rsi
	movq	%rdx, 280(%rsp)         # 8-byte Spill
	movq	%r9, %rdx
	movq	424(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 272(%rsp)         # 8-byte Spill
	movq	%rax, %rcx
	movq	304(%rsp), %rax         # 8-byte Reload
	movq	%r8, 264(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movq	%r9, 256(%rsp)          # 8-byte Spill
	movl	%ebp, %r9d
	movl	$1, (%rsp)
	movq	%r15, 248(%rsp)         # 8-byte Spill
	movl	%ebp, 244(%rsp)         # 4-byte Spill
	movq	%r12, 232(%rsp)         # 8-byte Spill
	movq	%r13, 224(%rsp)         # 8-byte Spill
	movq	%r14, 216(%rsp)         # 8-byte Spill
	movq	%rbx, 208(%rsp)         # 8-byte Spill
	movq	%r10, 200(%rsp)         # 8-byte Spill
	movq	%r11, 192(%rsp)         # 8-byte Spill
	callq	trace_logger_log0
	movq	264(%rsp), %rdi         # 8-byte Reload
	movq	448(%rsp), %rsi         # 8-byte Reload
	movq	288(%rsp), %rdx         # 8-byte Reload
	movq	288(%rsp), %rcx         # 8-byte Reload
	movq	416(%rsp), %r8          # 8-byte Reload
	movq	272(%rsp), %r9          # 8-byte Reload
	movq	320(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	272(%rsp), %rdi         # 8-byte Reload
	movq	448(%rsp), %rsi         # 8-byte Reload
	movq	288(%rsp), %rdx         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	440(%rsp), %r8          # 8-byte Reload
	movq	272(%rsp), %r9          # 8-byte Reload
	movq	256(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	232(%rsp), %rdi         # 8-byte Reload
	movq	448(%rsp), %rsi         # 8-byte Reload
	movq	480(%rsp), %rdx         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	424(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	256(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	384(%rsp), %rdi         # 8-byte Reload
	movq	216(%rsp), %rsi         # 8-byte Reload
	movq	256(%rsp), %rdx         # 8-byte Reload
	movq	344(%rsp), %rcx         # 8-byte Reload
	movq	352(%rsp), %r8          # 8-byte Reload
	movl	244(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	264(%rsp), %rdi         # 8-byte Reload
	movq	448(%rsp), %rsi         # 8-byte Reload
	movq	480(%rsp), %rdx         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	424(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	272(%rsp), %rdi         # 8-byte Reload
	movq	448(%rsp), %rsi         # 8-byte Reload
	movq	992(%rsp), %rdx         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	328(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	480(%rsp), %rax         # 8-byte Reload
	shlq	$2, %rax
	movq	992(%rsp), %rcx         # 8-byte Reload
	addq	%rax, %rcx
	movq	232(%rsp), %rdi         # 8-byte Reload
	movq	448(%rsp), %rsi         # 8-byte Reload
	movq	%rcx, %rdx
	movq	272(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 184(%rsp)         # 8-byte Spill
	movq	%rax, %rcx
	movq	344(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	384(%rsp), %rdi         # 8-byte Reload
	movq	216(%rsp), %rsi         # 8-byte Reload
	movq	256(%rsp), %rdx         # 8-byte Reload
	movq	400(%rsp), %rcx         # 8-byte Reload
	movq	336(%rsp), %r8          # 8-byte Reload
	movl	244(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	272(%rsp), %rdi         # 8-byte Reload
	movq	448(%rsp), %rsi         # 8-byte Reload
	movq	184(%rsp), %rdx         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	344(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	992(%rsp), %rax         # 8-byte Reload
	movq	480(%rsp), %rcx         # 8-byte Reload
	movl	(%rax,%rcx,4), %ebp
	movl	%ebp, %esi
	movl	%esi, %edx
	movq	232(%rsp), %rdi         # 8-byte Reload
	movq	224(%rsp), %rsi         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	400(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movl	%ebp, 180(%rsp)         # 4-byte Spill
	callq	trace_logger_log_int
	movq	384(%rsp), %rdi         # 8-byte Reload
	movq	216(%rsp), %rsi         # 8-byte Reload
	movq	256(%rsp), %rdx         # 8-byte Reload
	movq	392(%rsp), %rcx         # 8-byte Reload
	movq	352(%rsp), %r8          # 8-byte Reload
	movl	244(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	264(%rsp), %rdi         # 8-byte Reload
	movq	448(%rsp), %rsi         # 8-byte Reload
	movq	480(%rsp), %rdx         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	424(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	272(%rsp), %rdi         # 8-byte Reload
	movq	448(%rsp), %rsi         # 8-byte Reload
	movq	984(%rsp), %rdx         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	360(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	480(%rsp), %rax         # 8-byte Reload
	shlq	$2, %rax
	movq	984(%rsp), %rcx         # 8-byte Reload
	addq	%rax, %rcx
	movq	232(%rsp), %rdi         # 8-byte Reload
	movq	448(%rsp), %rsi         # 8-byte Reload
	movq	%rcx, %rdx
	movq	272(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 168(%rsp)         # 8-byte Spill
	movq	%rax, %rcx
	movq	392(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	384(%rsp), %rdi         # 8-byte Reload
	movq	216(%rsp), %rsi         # 8-byte Reload
	movq	256(%rsp), %rdx         # 8-byte Reload
	movq	376(%rsp), %rcx         # 8-byte Reload
	movq	368(%rsp), %r8          # 8-byte Reload
	movl	244(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	264(%rsp), %rdi         # 8-byte Reload
	movq	448(%rsp), %rsi         # 8-byte Reload
	movq	168(%rsp), %rdx         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	392(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movl	180(%rsp), %ebp         # 4-byte Reload
	movl	%ebp, %esi
	movl	%esi, %edx
	movq	272(%rsp), %rdi         # 8-byte Reload
	movq	224(%rsp), %rsi         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	400(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	984(%rsp), %rax         # 8-byte Reload
	movq	480(%rsp), %rcx         # 8-byte Reload
	movl	180(%rsp), %ebp         # 4-byte Reload
	movl	%ebp, (%rax,%rcx,4)
	.loc	1 30 0                  # store_kernel.c:30:0
	movq	208(%rsp), %rdi         # 8-byte Reload
	movq	216(%rsp), %rsi         # 8-byte Reload
	movq	256(%rsp), %rdx         # 8-byte Reload
	movq	440(%rsp), %rcx         # 8-byte Reload
	movq	408(%rsp), %r8          # 8-byte Reload
	movl	244(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	264(%rsp), %rdi         # 8-byte Reload
	movq	448(%rsp), %rsi         # 8-byte Reload
	movq	272(%rsp), %rdx         # 8-byte Reload
	movq	288(%rsp), %rcx         # 8-byte Reload
	movq	416(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	272(%rsp), %rdi         # 8-byte Reload
	movq	448(%rsp), %rsi         # 8-byte Reload
	movq	480(%rsp), %rdx         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	424(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	480(%rsp), %rax         # 8-byte Reload
	addq	$1, %rax
	movq	232(%rsp), %rdi         # 8-byte Reload
	movq	448(%rsp), %rsi         # 8-byte Reload
	movq	%rax, %rdx
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	440(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movq	%rax, 160(%rsp)         # 8-byte Spill
	callq	trace_logger_log_int
	movq	208(%rsp), %rdi         # 8-byte Reload
	movq	216(%rsp), %rsi         # 8-byte Reload
	movq	256(%rsp), %rdx         # 8-byte Reload
	movq	472(%rsp), %rcx         # 8-byte Reload
	movq	432(%rsp), %r8          # 8-byte Reload
	movl	244(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	272(%rsp), %rdi         # 8-byte Reload
	movq	448(%rsp), %rsi         # 8-byte Reload
	movq	160(%rsp), %rdx         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	440(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	160(%rsp), %rax         # 8-byte Reload
	movl	%eax, %ebp
	movl	%ebp, %esi
	movl	%esi, %edx
	movq	232(%rsp), %rdi         # 8-byte Reload
	movq	224(%rsp), %rsi         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	472(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movl	%ebp, 156(%rsp)         # 4-byte Spill
	callq	trace_logger_log_int
	movq	208(%rsp), %rdi         # 8-byte Reload
	movq	216(%rsp), %rsi         # 8-byte Reload
	movq	256(%rsp), %rdx         # 8-byte Reload
	movq	280(%rsp), %rcx         # 8-byte Reload
	movq	456(%rsp), %r8          # 8-byte Reload
	movl	244(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movl	1092(%rsp), %r9d        # 4-byte Reload
	movl	%r9d, %ebp
	movl	%ebp, %edx
	movq	264(%rsp), %rdi         # 8-byte Reload
	movq	224(%rsp), %rsi         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	464(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	156(%rsp), %ebp         # 4-byte Reload
	movl	%ebp, %esi
	movl	%esi, %edx
	movq	272(%rsp), %rdi         # 8-byte Reload
	movq	224(%rsp), %rsi         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	472(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	156(%rsp), %ebp         # 4-byte Reload
	movl	1092(%rsp), %esi        # 4-byte Reload
	cmpl	%esi, %ebp
	sete	%al
	movb	%al, %cl
	andb	$1, %cl
	movzbl	%cl, %ecx
	movl	%ecx, %edx
	movq	232(%rsp), %rdi         # 8-byte Reload
	movq	272(%rsp), %rsi         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	280(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movb	%al, 155(%rsp)          # 1-byte Spill
	callq	trace_logger_log_int
	movq	208(%rsp), %rdi         # 8-byte Reload
	movq	216(%rsp), %rsi         # 8-byte Reload
	movq	256(%rsp), %rdx         # 8-byte Reload
	movq	248(%rsp), %rcx         # 8-byte Reload
	movq	264(%rsp), %r8          # 8-byte Reload
	movl	244(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	200(%rsp), %rdi         # 8-byte Reload
	movq	288(%rsp), %rsi         # 8-byte Reload
	movq	288(%rsp), %rdx         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	192(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	264(%rsp), %rdi         # 8-byte Reload
	movq	288(%rsp), %rsi         # 8-byte Reload
	movq	288(%rsp), %rdx         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	256(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	155(%rsp), %al          # 1-byte Reload
	andb	$1, %al
	movzbl	%al, %ebp
	movl	%ebp, %edx
	movq	272(%rsp), %rdi         # 8-byte Reload
	movq	272(%rsp), %rsi         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	280(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	155(%rsp), %al          # 1-byte Reload
	testb	$1, %al
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, 488(%rsp)         # 8-byte Spill
	jne	.LBB0_2
	jmp	.LBB0_1
.Ltmp25:
.LBB0_2:                                # %._crit_edge
	movabsq	$3, %rdi
	movabsq	$64, %rax
	movabsq	$1, %rcx
	leaq	.L.str12, %r8
	movabsq	$0, %rdx
	leaq	.L.str9, %rsi
	movabsq	$2, %r9
	leaq	.L.str17, %r10
	movabsq	$4, %r11
	leaq	dmaStore, %rbx
	leaq	.L.str33, %r14
	movabsq	$34, %r15
	leaq	.L.str1, %r12
	leaq	.L.str22, %r13
	leaq	.L.str32, %rbp
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movabsq	$98, %rax
	movq	%rsi, 136(%rsp)         # 8-byte Spill
	movl	$1, %esi
	.loc	1 34 0                  # store_kernel.c:34:0
	movq	%rdi, 128(%rsp)         # 8-byte Spill
	movq	%r15, %rdi
	movl	%esi, 124(%rsp)         # 4-byte Spill
	movq	%r12, %rsi
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	movq	%r13, %rdx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movq	%rbp, %rcx
	movq	%r8, 96(%rsp)           # 8-byte Spill
	movq	%rax, %r8
	movl	124(%rsp), %ebp         # 4-byte Reload
	movq	%r9, 88(%rsp)           # 8-byte Spill
	movl	%ebp, %r9d
	movl	$1, (%rsp)
	movq	%r14, 80(%rsp)          # 8-byte Spill
	movq	%r10, 72(%rsp)          # 8-byte Spill
	movq	%r11, 64(%rsp)          # 8-byte Spill
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	callq	trace_logger_log0
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	144(%rsp), %rsi         # 8-byte Reload
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	104(%rsp), %rcx         # 8-byte Reload
	movq	80(%rsp), %r8           # 8-byte Reload
	movq	112(%rsp), %r9          # 8-byte Reload
	movq	136(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	104(%rsp), %rdi         # 8-byte Reload
	movq	144(%rsp), %rsi         # 8-byte Reload
	movq	664(%rsp), %rdx         # 8-byte Reload
	movq	104(%rsp), %rcx         # 8-byte Reload
	movq	72(%rsp), %r8           # 8-byte Reload
	movq	112(%rsp), %r9          # 8-byte Reload
	movq	136(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	88(%rsp), %rdi          # 8-byte Reload
	movq	144(%rsp), %rsi         # 8-byte Reload
	movq	664(%rsp), %rdx         # 8-byte Reload
	movq	104(%rsp), %rcx         # 8-byte Reload
	movq	72(%rsp), %r8           # 8-byte Reload
	movq	112(%rsp), %r9          # 8-byte Reload
	movq	136(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	128(%rsp), %rdi         # 8-byte Reload
	movq	144(%rsp), %rsi         # 8-byte Reload
	movq	864(%rsp), %rdx         # 8-byte Reload
	movq	104(%rsp), %rcx         # 8-byte Reload
	movq	96(%rsp), %r8           # 8-byte Reload
	movq	112(%rsp), %r9          # 8-byte Reload
	movq	136(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	664(%rsp), %rdi         # 8-byte Reload
	movq	664(%rsp), %rsi         # 8-byte Reload
	movq	864(%rsp), %rdx         # 8-byte Reload
	callq	dmaStore
	movabsq	$36, %rdi
	leaq	.L.str1, %rsi
	leaq	.L.str22, %rdx
	leaq	.L.str34, %rcx
	movabsq	$1, %r8
	movl	$1, %r9d
	movabsq	$19134, %r10            # imm = 0x4ABE
	movabsq	$32, %r11
	leaq	.L.str32, %rbx
	movabsq	$0, %r14
	leaq	.L.str9, %r15
	.loc	1 36 0                  # store_kernel.c:36:0
	movl	%eax, %eax
	movl	%eax, %r12d
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	movq	%r10, %rdi
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movq	%r11, %rsi
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%r12, %rdx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%r8, %rcx
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%rbx, %r8
	movl	%r9d, 12(%rsp)          # 4-byte Spill
	movq	%r14, %r9
	movq	%r15, (%rsp)
	callq	trace_logger_log_int
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	movl	12(%rsp), %r9d          # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	addq	$1128, %rsp             # imm = 0x468
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.Ltmp26:
.Ltmp27:
	.size	store_kernel, .Ltmp27-store_kernel
.Lfunc_end0:
	.cfi_endproc

	.globl	test_stores
	.align	16, 0x90
	.type	test_stores,@function
test_stores:                            # @test_stores
	.cfi_startproc
.Lfunc_begin1:
	.loc	1 38 0                  # store_kernel.c:38:0
# BB#0:
	pushq	%rbp
.Ltmp35:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Ltmp36:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Ltmp37:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Ltmp38:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Ltmp39:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Ltmp40:
	.cfi_def_cfa_offset 56
	subq	$1352, %rsp             # imm = 0x548
.Ltmp41:
	.cfi_def_cfa_offset 1408
.Ltmp42:
	.cfi_offset %rbx, -56
.Ltmp43:
	.cfi_offset %r12, -48
.Ltmp44:
	.cfi_offset %r13, -40
.Ltmp45:
	.cfi_offset %r14, -32
.Ltmp46:
	.cfi_offset %r15, -24
.Ltmp47:
	.cfi_offset %rbp, -16
	movabsq	$0, %rax
	movl	$0, %ecx
	movabsq	$1, %r8
	leaq	.L.str7, %r9
	leaq	.L.str9, %r10
	movabsq	$2, %r11
	leaq	.L.str22, %rbx
	movabsq	$3, %r14
	leaq	.L.str21, %r15
	movabsq	$40, %r12
	leaq	.L.str35, %r13
	leaq	.L.str6, %rbp
	movq	%rax, 1344(%rsp)        # 8-byte Spill
	leaq	.L.str36, %rax
	movl	%ecx, 1340(%rsp)        # 4-byte Spill
	movl	$1, %ecx
	movq	%rax, 1328(%rsp)        # 8-byte Spill
	movabsq	$19134, %rax            # imm = 0x4ABE
	movq	%rax, 1320(%rsp)        # 8-byte Spill
	movabsq	$32, %rax
	movq	%rax, 1312(%rsp)        # 8-byte Spill
	leaq	.L.str11, %rax
	movq	%rax, 1304(%rsp)        # 8-byte Spill
	leaq	.L.str13, %rax
	movq	%rax, 1296(%rsp)        # 8-byte Spill
	movabsq	$46, %rax
	movq	%rax, 1288(%rsp)        # 8-byte Spill
	movabsq	$24601, %rax            # imm = 0x6019
	movq	%rax, 1280(%rsp)        # 8-byte Spill
	movabsq	$64, %rax
	movq	%rax, 1272(%rsp)        # 8-byte Spill
	leaq	.L.str17, %rax
	movq	%rax, 1264(%rsp)        # 8-byte Spill
	leaq	.L.str8, %rax
	.loc	1 38 0 prologue_end     # store_kernel.c:38:0
.Ltmp48:
	movq	%rax, 1256(%rsp)        # 8-byte Spill
	movq	1280(%rsp), %rax        # 8-byte Reload
	movq	%rdi, 1248(%rsp)        # 8-byte Spill
	movq	%rax, %rdi
	movq	1272(%rsp), %rax        # 8-byte Reload
	movq	%rsi, 1240(%rsp)        # 8-byte Spill
	movq	%rax, %rsi
	movq	1248(%rsp), %rax        # 8-byte Reload
	movl	%edx, 1236(%rsp)        # 4-byte Spill
	movq	%rax, %rdx
	movl	%ecx, 1232(%rsp)        # 4-byte Spill
	movq	%r8, %rcx
	movq	1256(%rsp), %rax        # 8-byte Reload
	movq	%r8, 1224(%rsp)         # 8-byte Spill
	movq	%rax, %r8
	movq	1344(%rsp), %rax        # 8-byte Reload
	movq	%r9, 1216(%rsp)         # 8-byte Spill
	movq	%rax, %r9
	movq	%r10, (%rsp)
	movq	%r13, 1208(%rsp)        # 8-byte Spill
	movq	%rbp, 1200(%rsp)        # 8-byte Spill
	movq	%r12, 1192(%rsp)        # 8-byte Spill
	movq	%r15, 1184(%rsp)        # 8-byte Spill
	movq	%r14, 1176(%rsp)        # 8-byte Spill
	movq	%rbx, 1168(%rsp)        # 8-byte Spill
	movq	%r10, 1160(%rsp)        # 8-byte Spill
	movq	%r11, 1152(%rsp)        # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	1280(%rsp), %rdi        # 8-byte Reload
	movq	1272(%rsp), %rsi        # 8-byte Reload
	movq	1240(%rsp), %rdx        # 8-byte Reload
	movq	1224(%rsp), %rcx        # 8-byte Reload
	movq	1264(%rsp), %r8         # 8-byte Reload
	movq	1344(%rsp), %r9         # 8-byte Reload
	movq	1160(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movl	1236(%rsp), %esi        # 4-byte Reload
	movl	%esi, %esi
	movl	%esi, %edx
	movq	1280(%rsp), %rdi        # 8-byte Reload
	movq	1312(%rsp), %rsi        # 8-byte Reload
	movq	1224(%rsp), %rcx        # 8-byte Reload
	movq	1304(%rsp), %r8         # 8-byte Reload
	movq	1344(%rsp), %r9         # 8-byte Reload
	movq	1160(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	#DEBUG_VALUE: test_stores:store_vals <- [RSP+1248]
	#DEBUG_VALUE: test_stores:store_loc <- [RSP+1240]
	#DEBUG_VALUE: test_stores:num_vals <- [RSP+1236]
.Ltmp49:
	#DEBUG_VALUE: test_stores:num_failures <- 0
	#DEBUG_VALUE: i <- 0
	.loc	1 40 0                  # store_kernel.c:40:0
	movq	1192(%rsp), %rdi        # 8-byte Reload
	movq	1208(%rsp), %rsi        # 8-byte Reload
	movq	1200(%rsp), %rdx        # 8-byte Reload
	movq	1216(%rsp), %rcx        # 8-byte Reload
	movq	1288(%rsp), %r8         # 8-byte Reload
	movl	1340(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1152(%rsp), %rdi        # 8-byte Reload
	movq	1312(%rsp), %rsi        # 8-byte Reload
	movq	1344(%rsp), %rdx        # 8-byte Reload
	movq	1344(%rsp), %rcx        # 8-byte Reload
	movq	1296(%rsp), %r8         # 8-byte Reload
	movq	1344(%rsp), %r9         # 8-byte Reload
	movq	1160(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	1236(%rsp), %esi        # 4-byte Reload
	movl	%esi, %esi
	movl	%esi, %edx
	movq	1224(%rsp), %rdi        # 8-byte Reload
	movq	1312(%rsp), %rsi        # 8-byte Reload
	movq	1224(%rsp), %rcx        # 8-byte Reload
	movq	1304(%rsp), %r8         # 8-byte Reload
	movq	1344(%rsp), %r9         # 8-byte Reload
	movq	1160(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	1236(%rsp), %esi        # 4-byte Reload
	cmpl	$0, %esi
	setg	%al
	movb	%al, %cl
	andb	$1, %cl
	movzbl	%cl, %ecx
	movl	%ecx, %edx
	movq	1320(%rsp), %rdi        # 8-byte Reload
	movq	1224(%rsp), %rsi        # 8-byte Reload
	movq	1224(%rsp), %rcx        # 8-byte Reload
	movq	1216(%rsp), %r8         # 8-byte Reload
	movq	1344(%rsp), %r9         # 8-byte Reload
	movq	1160(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	movb	%al, 1151(%rsp)         # 1-byte Spill
	callq	trace_logger_log_int
	movq	1192(%rsp), %rdi        # 8-byte Reload
	movq	1208(%rsp), %rsi        # 8-byte Reload
	movq	1200(%rsp), %rdx        # 8-byte Reload
	movq	1328(%rsp), %rcx        # 8-byte Reload
	movq	1152(%rsp), %r8         # 8-byte Reload
	movl	1340(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1176(%rsp), %rdi        # 8-byte Reload
	movq	1344(%rsp), %rsi        # 8-byte Reload
	movq	1344(%rsp), %rdx        # 8-byte Reload
	movq	1224(%rsp), %rcx        # 8-byte Reload
	movq	1184(%rsp), %r8         # 8-byte Reload
	movq	1344(%rsp), %r9         # 8-byte Reload
	movq	1160(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	1152(%rsp), %rdi        # 8-byte Reload
	movq	1344(%rsp), %rsi        # 8-byte Reload
	movq	1344(%rsp), %rdx        # 8-byte Reload
	movq	1224(%rsp), %rcx        # 8-byte Reload
	movq	1168(%rsp), %r8         # 8-byte Reload
	movq	1344(%rsp), %r9         # 8-byte Reload
	movq	1160(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	1151(%rsp), %al         # 1-byte Reload
	andb	$1, %al
	movzbl	%al, %eax
	movl	%eax, %edx
	movq	1224(%rsp), %rdi        # 8-byte Reload
	movq	1224(%rsp), %rsi        # 8-byte Reload
	movq	1224(%rsp), %rcx        # 8-byte Reload
	movq	1216(%rsp), %r8         # 8-byte Reload
	movq	1344(%rsp), %r9         # 8-byte Reload
	movq	1160(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	1151(%rsp), %al         # 1-byte Reload
	testb	$1, %al
	movq	1344(%rsp), %rcx        # 8-byte Reload
	movl	1340(%rsp), %eax        # 4-byte Reload
	movl	%eax, 1144(%rsp)        # 4-byte Spill
	movq	%rcx, 1136(%rsp)        # 8-byte Spill
	jne	.LBB1_1
	jmp	.LBB1_4
.Ltmp50:
.LBB1_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	1144(%rsp), %eax        # 4-byte Reload
	movq	1136(%rsp), %rcx        # 8-byte Reload
	movabsq	$1, %rdx
	leaq	.L.str18, %rsi
	movabsq	$0, %rdi
	leaq	.L.str9, %r8
	movabsq	$2, %r9
	leaq	.L.str40, %r10
	movabsq	$3, %r11
	leaq	.L.str37, %rbx
	movabsq	$41, %r14
	leaq	.L.str35, %r15
	leaq	.L.str21, %r12
	leaq	.L.str28, %r13
	movl	$0, %ebp
	movl	%eax, 1132(%rsp)        # 4-byte Spill
	movl	$1, %eax
	movq	%rcx, 1120(%rsp)        # 8-byte Spill
	movabsq	$19134, %rcx            # imm = 0x4ABE
	movq	%rcx, 1112(%rsp)        # 8-byte Spill
	movabsq	$32, %rcx
	movq	%rcx, 1104(%rsp)        # 8-byte Spill
	leaq	.L.str12, %rcx
	movq	%rcx, 1096(%rsp)        # 8-byte Spill
	leaq	.L.str16, %rcx
	movq	%rcx, 1088(%rsp)        # 8-byte Spill
	movabsq	$46, %rcx
	movq	%rcx, 1080(%rsp)        # 8-byte Spill
	movabsq	$64, %rcx
	movq	%rcx, 1072(%rsp)        # 8-byte Spill
	leaq	.L.str14, %rcx
	movq	%rcx, 1064(%rsp)        # 8-byte Spill
	movabsq	$27, %rcx
	movq	%rcx, 1056(%rsp)        # 8-byte Spill
	leaq	.L.str8, %rcx
	movq	%rcx, 1048(%rsp)        # 8-byte Spill
	leaq	.L.str23, %rcx
	movq	%rcx, 1040(%rsp)        # 8-byte Spill
	movabsq	$29, %rcx
	movq	%rcx, 1032(%rsp)        # 8-byte Spill
	leaq	.L.str10, %rcx
	movq	%rcx, 1024(%rsp)        # 8-byte Spill
	leaq	.L.str17, %rcx
	movq	%rcx, 1016(%rsp)        # 8-byte Spill
	leaq	.L.str38, %rcx
	movq	%rcx, 1008(%rsp)        # 8-byte Spill
	leaq	.L.str39, %rcx
	movq	%rcx, 1000(%rsp)        # 8-byte Spill
	leaq	.L.str13, %rcx
	movq	%rcx, 992(%rsp)         # 8-byte Spill
	leaq	.L.str6, %rcx
	movq	%rcx, 984(%rsp)         # 8-byte Spill
	movabsq	$-1, %rcx
	movq	%rcx, 976(%rsp)         # 8-byte Spill
	movabsq	$48, %rcx
	movq	%rcx, 968(%rsp)         # 8-byte Spill
	leaq	.L.str24, %rcx
	.loc	1 41 0                  # store_kernel.c:41:0
.Ltmp51:
	movq	%rcx, 960(%rsp)         # 8-byte Spill
	movq	976(%rsp), %rcx         # 8-byte Reload
	movq	%rdi, 952(%rsp)         # 8-byte Spill
	movq	%rcx, %rdi
	movq	%rsi, 944(%rsp)         # 8-byte Spill
	movq	%r15, %rsi
	movq	%rdx, 936(%rsp)         # 8-byte Spill
	movq	%r12, %rdx
	movq	1040(%rsp), %rcx        # 8-byte Reload
	movq	%r8, 928(%rsp)          # 8-byte Spill
	movq	968(%rsp), %r8          # 8-byte Reload
	movq	%r9, 920(%rsp)          # 8-byte Spill
	movl	%ebp, %r9d
	movl	$1, (%rsp)
	movl	%eax, 916(%rsp)         # 4-byte Spill
	movq	%r15, 904(%rsp)         # 8-byte Spill
	movq	%r12, 896(%rsp)         # 8-byte Spill
	movq	%r13, 888(%rsp)         # 8-byte Spill
	movl	%ebp, 884(%rsp)         # 4-byte Spill
	movq	%r14, 872(%rsp)         # 8-byte Spill
	movq	%rbx, 864(%rsp)         # 8-byte Spill
	movq	%r10, 856(%rsp)         # 8-byte Spill
	movq	%r11, 848(%rsp)         # 8-byte Spill
	callq	trace_logger_log0
	movq	920(%rsp), %rdi         # 8-byte Reload
	movq	1072(%rsp), %rsi        # 8-byte Reload
	movq	952(%rsp), %rdx         # 8-byte Reload
	movq	952(%rsp), %rcx         # 8-byte Reload
	movq	992(%rsp), %r8          # 8-byte Reload
	movq	936(%rsp), %r9          # 8-byte Reload
	movq	984(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	936(%rsp), %rdi         # 8-byte Reload
	movq	1072(%rsp), %rsi        # 8-byte Reload
	movq	952(%rsp), %rdx         # 8-byte Reload
	movq	936(%rsp), %rcx         # 8-byte Reload
	movq	960(%rsp), %r8          # 8-byte Reload
	movq	936(%rsp), %r9          # 8-byte Reload
	movq	864(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	1112(%rsp), %rdi        # 8-byte Reload
	movq	1072(%rsp), %rsi        # 8-byte Reload
	movq	1120(%rsp), %rdx        # 8-byte Reload
	movq	936(%rsp), %rcx         # 8-byte Reload
	movq	1040(%rsp), %r8         # 8-byte Reload
	movq	952(%rsp), %r9          # 8-byte Reload
	movq	864(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	976(%rsp), %rdi         # 8-byte Reload
	movq	904(%rsp), %rsi         # 8-byte Reload
	movq	896(%rsp), %rdx         # 8-byte Reload
	movq	1008(%rsp), %rcx        # 8-byte Reload
	movq	968(%rsp), %r8          # 8-byte Reload
	movl	884(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	920(%rsp), %rdi         # 8-byte Reload
	movq	1104(%rsp), %rsi        # 8-byte Reload
	movq	952(%rsp), %rdx         # 8-byte Reload
	movq	952(%rsp), %rcx         # 8-byte Reload
	movq	992(%rsp), %r8          # 8-byte Reload
	movq	936(%rsp), %r9          # 8-byte Reload
	movq	984(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	936(%rsp), %rdi         # 8-byte Reload
	movq	1104(%rsp), %rsi        # 8-byte Reload
	movq	952(%rsp), %rdx         # 8-byte Reload
	movq	936(%rsp), %rcx         # 8-byte Reload
	movq	1000(%rsp), %r8         # 8-byte Reload
	movq	936(%rsp), %r9          # 8-byte Reload
	movq	864(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movl	1132(%rsp), %eax        # 4-byte Reload
	movl	%eax, %ebp
	movl	%ebp, %edx
	movq	1112(%rsp), %rdi        # 8-byte Reload
	movq	1104(%rsp), %rsi        # 8-byte Reload
	movq	936(%rsp), %rcx         # 8-byte Reload
	movq	1008(%rsp), %r8         # 8-byte Reload
	movq	952(%rsp), %r9          # 8-byte Reload
	movq	864(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	872(%rsp), %rdi         # 8-byte Reload
	movq	904(%rsp), %rsi         # 8-byte Reload
	movq	896(%rsp), %rdx         # 8-byte Reload
	movq	1024(%rsp), %rcx        # 8-byte Reload
	movq	1032(%rsp), %r8         # 8-byte Reload
	movl	884(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	920(%rsp), %rdi         # 8-byte Reload
	movq	1072(%rsp), %rsi        # 8-byte Reload
	movq	1120(%rsp), %rdx        # 8-byte Reload
	movq	936(%rsp), %rcx         # 8-byte Reload
	movq	1040(%rsp), %r8         # 8-byte Reload
	movq	952(%rsp), %r9          # 8-byte Reload
	movq	928(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	936(%rsp), %rdi         # 8-byte Reload
	movq	1072(%rsp), %rsi        # 8-byte Reload
	movq	1240(%rsp), %rdx        # 8-byte Reload
	movq	936(%rsp), %rcx         # 8-byte Reload
	movq	1016(%rsp), %r8         # 8-byte Reload
	movq	952(%rsp), %r9          # 8-byte Reload
	movq	928(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	1120(%rsp), %rcx        # 8-byte Reload
	shlq	$2, %rcx
	movq	1240(%rsp), %rdx        # 8-byte Reload
	addq	%rcx, %rdx
	movq	1112(%rsp), %rdi        # 8-byte Reload
	movq	1072(%rsp), %rsi        # 8-byte Reload
	movq	%rdx, 840(%rsp)         # 8-byte Spill
	movq	936(%rsp), %rcx         # 8-byte Reload
	movq	1024(%rsp), %r8         # 8-byte Reload
	movq	952(%rsp), %r9          # 8-byte Reload
	movq	928(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	872(%rsp), %rdi         # 8-byte Reload
	movq	904(%rsp), %rsi         # 8-byte Reload
	movq	896(%rsp), %rdx         # 8-byte Reload
	movq	1096(%rsp), %rcx        # 8-byte Reload
	movq	1056(%rsp), %r8         # 8-byte Reload
	movl	884(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	936(%rsp), %rdi         # 8-byte Reload
	movq	1072(%rsp), %rsi        # 8-byte Reload
	movq	840(%rsp), %rdx         # 8-byte Reload
	movq	936(%rsp), %rcx         # 8-byte Reload
	movq	1024(%rsp), %r8         # 8-byte Reload
	movq	952(%rsp), %r9          # 8-byte Reload
	movq	928(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	1240(%rsp), %rcx        # 8-byte Reload
	movq	1120(%rsp), %rdx        # 8-byte Reload
	movl	(%rcx,%rdx,4), %eax
	movl	%eax, %ebp
	movl	%ebp, %edx
	movq	1112(%rsp), %rdi        # 8-byte Reload
	movq	1104(%rsp), %rsi        # 8-byte Reload
	movq	936(%rsp), %rcx         # 8-byte Reload
	movq	1096(%rsp), %r8         # 8-byte Reload
	movq	952(%rsp), %r9          # 8-byte Reload
	movq	928(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movl	%eax, 836(%rsp)         # 4-byte Spill
	callq	trace_logger_log_int
	movq	872(%rsp), %rdi         # 8-byte Reload
	movq	904(%rsp), %rsi         # 8-byte Reload
	movq	896(%rsp), %rdx         # 8-byte Reload
	movq	1064(%rsp), %rcx        # 8-byte Reload
	movq	1032(%rsp), %r8         # 8-byte Reload
	movl	884(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	920(%rsp), %rdi         # 8-byte Reload
	movq	1072(%rsp), %rsi        # 8-byte Reload
	movq	1120(%rsp), %rdx        # 8-byte Reload
	movq	936(%rsp), %rcx         # 8-byte Reload
	movq	1040(%rsp), %r8         # 8-byte Reload
	movq	952(%rsp), %r9          # 8-byte Reload
	movq	928(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	936(%rsp), %rdi         # 8-byte Reload
	movq	1072(%rsp), %rsi        # 8-byte Reload
	movq	1248(%rsp), %rdx        # 8-byte Reload
	movq	936(%rsp), %rcx         # 8-byte Reload
	movq	1048(%rsp), %r8         # 8-byte Reload
	movq	952(%rsp), %r9          # 8-byte Reload
	movq	928(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	1120(%rsp), %rcx        # 8-byte Reload
	shlq	$2, %rcx
	movq	1248(%rsp), %rdx        # 8-byte Reload
	addq	%rcx, %rdx
	movq	1112(%rsp), %rdi        # 8-byte Reload
	movq	1072(%rsp), %rsi        # 8-byte Reload
	movq	%rdx, 824(%rsp)         # 8-byte Spill
	movq	936(%rsp), %rcx         # 8-byte Reload
	movq	1064(%rsp), %r8         # 8-byte Reload
	movq	952(%rsp), %r9          # 8-byte Reload
	movq	928(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	872(%rsp), %rdi         # 8-byte Reload
	movq	904(%rsp), %rsi         # 8-byte Reload
	movq	896(%rsp), %rdx         # 8-byte Reload
	movq	1088(%rsp), %rcx        # 8-byte Reload
	movq	1056(%rsp), %r8         # 8-byte Reload
	movl	884(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	936(%rsp), %rdi         # 8-byte Reload
	movq	1072(%rsp), %rsi        # 8-byte Reload
	movq	824(%rsp), %rdx         # 8-byte Reload
	movq	936(%rsp), %rcx         # 8-byte Reload
	movq	1064(%rsp), %r8         # 8-byte Reload
	movq	952(%rsp), %r9          # 8-byte Reload
	movq	928(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	1248(%rsp), %rcx        # 8-byte Reload
	movq	1120(%rsp), %rdx        # 8-byte Reload
	movl	(%rcx,%rdx,4), %eax
	movl	%eax, %ebp
	movl	%ebp, %edx
	movq	1112(%rsp), %rdi        # 8-byte Reload
	movq	1104(%rsp), %rsi        # 8-byte Reload
	movq	936(%rsp), %rcx         # 8-byte Reload
	movq	1088(%rsp), %r8         # 8-byte Reload
	movq	952(%rsp), %r9          # 8-byte Reload
	movq	928(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movl	%eax, 820(%rsp)         # 4-byte Spill
	callq	trace_logger_log_int
	movq	872(%rsp), %rdi         # 8-byte Reload
	movq	904(%rsp), %rsi         # 8-byte Reload
	movq	896(%rsp), %rdx         # 8-byte Reload
	movq	944(%rsp), %rcx         # 8-byte Reload
	movq	1080(%rsp), %r8         # 8-byte Reload
	movl	884(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movl	820(%rsp), %eax         # 4-byte Reload
	movl	%eax, %r9d
	movl	%r9d, %edx
	movq	920(%rsp), %rdi         # 8-byte Reload
	movq	1104(%rsp), %rsi        # 8-byte Reload
	movq	936(%rsp), %rcx         # 8-byte Reload
	movq	1088(%rsp), %r8         # 8-byte Reload
	movq	952(%rsp), %r9          # 8-byte Reload
	movq	928(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movl	836(%rsp), %eax         # 4-byte Reload
	movl	%eax, %ebp
	movl	%ebp, %edx
	movq	936(%rsp), %rdi         # 8-byte Reload
	movq	1104(%rsp), %rsi        # 8-byte Reload
	movq	936(%rsp), %rcx         # 8-byte Reload
	movq	1096(%rsp), %r8         # 8-byte Reload
	movq	952(%rsp), %r9          # 8-byte Reload
	movq	928(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movl	836(%rsp), %eax         # 4-byte Reload
	movl	820(%rsp), %ebp         # 4-byte Reload
	cmpl	%ebp, %eax
	sete	%al
	movb	%al, %cl
	andb	$1, %cl
	movzbl	%cl, %ecx
	movl	%ecx, %edx
	movq	1112(%rsp), %rdi        # 8-byte Reload
	movq	936(%rsp), %rsi         # 8-byte Reload
	movq	936(%rsp), %rcx         # 8-byte Reload
	movq	944(%rsp), %r8          # 8-byte Reload
	movq	952(%rsp), %r9          # 8-byte Reload
	movq	928(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movb	%al, 819(%rsp)          # 1-byte Spill
	callq	trace_logger_log_int
	movq	872(%rsp), %rdi         # 8-byte Reload
	movq	904(%rsp), %rsi         # 8-byte Reload
	movq	896(%rsp), %rdx         # 8-byte Reload
	movq	888(%rsp), %rcx         # 8-byte Reload
	movq	920(%rsp), %r8          # 8-byte Reload
	movl	884(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	848(%rsp), %rdi         # 8-byte Reload
	movq	952(%rsp), %rsi         # 8-byte Reload
	movq	952(%rsp), %rdx         # 8-byte Reload
	movq	936(%rsp), %rcx         # 8-byte Reload
	movq	864(%rsp), %r8          # 8-byte Reload
	movq	952(%rsp), %r9          # 8-byte Reload
	movq	928(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	920(%rsp), %rdi         # 8-byte Reload
	movq	952(%rsp), %rsi         # 8-byte Reload
	movq	952(%rsp), %rdx         # 8-byte Reload
	movq	936(%rsp), %rcx         # 8-byte Reload
	movq	856(%rsp), %r8          # 8-byte Reload
	movq	952(%rsp), %r9          # 8-byte Reload
	movq	928(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	819(%rsp), %al          # 1-byte Reload
	andb	$1, %al
	movzbl	%al, %ebp
	movl	%ebp, %edx
	movq	936(%rsp), %rdi         # 8-byte Reload
	movq	936(%rsp), %rsi         # 8-byte Reload
	movq	936(%rsp), %rcx         # 8-byte Reload
	movq	944(%rsp), %r8          # 8-byte Reload
	movq	952(%rsp), %r9          # 8-byte Reload
	movq	928(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	819(%rsp), %al          # 1-byte Reload
	testb	$1, %al
	movl	1132(%rsp), %ebp        # 4-byte Reload
	movl	%ebp, 812(%rsp)         # 4-byte Spill
	jne	.LBB1_3
# BB#2:                                 #   in Loop: Header=BB1_1 Depth=1
	movabsq	$2, %rdi
	movabsq	$64, %rax
	leaq	.L.str, %rdx
	movabsq	$0, %rcx
	leaq	.L.str13, %r8
	leaq	.L.str9, %rsi
	movabsq	$1, %r9
	leaq	.L.str25, %r10
	movabsq	$6, %r11
	leaq	fprintf, %rbx
	leaq	.L.str42, %r14
	movabsq	$42, %r15
	leaq	.L.str35, %r12
	leaq	.L.str40, %r13
	leaq	.L.str27, %rbp
	movq	%rax, 800(%rsp)         # 8-byte Spill
	movabsq	$49, %rax
	movq	%rsi, 792(%rsp)         # 8-byte Spill
	movl	$0, %esi
	movl	%esi, 788(%rsp)         # 4-byte Spill
	movl	$1, %esi
	movq	%rax, 776(%rsp)         # 8-byte Spill
	movabsq	$19134, %rax            # imm = 0x4ABE
	movq	%rax, 768(%rsp)         # 8-byte Spill
	movabsq	$32, %rax
	movq	%rax, 760(%rsp)         # 8-byte Spill
	leaq	.L.str26, %rax
	movq	%rax, 752(%rsp)         # 8-byte Spill
	leaq	.L.str23, %rax
	movq	%rax, 744(%rsp)         # 8-byte Spill
	movabsq	$33, %rax
	movq	%rax, 736(%rsp)         # 8-byte Spill
	leaq	stdout, %rax
	movq	%rax, 728(%rsp)         # 8-byte Spill
	leaq	.L.str41, %rax
	movq	%rax, 720(%rsp)         # 8-byte Spill
	movabsq	$27, %rax
	.loc	1 42 0                  # store_kernel.c:42:0
.Ltmp52:
	movq	%rdi, 712(%rsp)         # 8-byte Spill
	movq	%r15, %rdi
	movl	%esi, 708(%rsp)         # 4-byte Spill
	movq	%r12, %rsi
	movq	%rdx, 696(%rsp)         # 8-byte Spill
	movq	%r13, %rdx
	movq	%rcx, 688(%rsp)         # 8-byte Spill
	movq	%r10, %rcx
	movq	%r8, 680(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movl	788(%rsp), %eax         # 4-byte Reload
	movq	%r9, 672(%rsp)          # 8-byte Spill
	movl	%eax, %r9d
	movl	$1, (%rsp)
	movq	%rbp, 664(%rsp)         # 8-byte Spill
	movq	%r14, 656(%rsp)         # 8-byte Spill
	movq	%r12, 648(%rsp)         # 8-byte Spill
	movq	%r15, 640(%rsp)         # 8-byte Spill
	movq	%rbx, 632(%rsp)         # 8-byte Spill
	movq	%r13, 624(%rsp)         # 8-byte Spill
	movq	%r10, 616(%rsp)         # 8-byte Spill
	movq	%r11, 608(%rsp)         # 8-byte Spill
	callq	trace_logger_log0
	movq	672(%rsp), %rdi         # 8-byte Reload
	movq	800(%rsp), %rsi         # 8-byte Reload
	movq	728(%rsp), %rdx         # 8-byte Reload
	movq	672(%rsp), %rcx         # 8-byte Reload
	movq	720(%rsp), %r8          # 8-byte Reload
	movq	688(%rsp), %r9          # 8-byte Reload
	movq	792(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	stdout, %rcx
	movq	768(%rsp), %rdi         # 8-byte Reload
	movq	800(%rsp), %rsi         # 8-byte Reload
	movq	%rcx, %rdx
	movq	672(%rsp), %r8          # 8-byte Reload
	movq	%rcx, 600(%rsp)         # 8-byte Spill
	movq	%r8, %rcx
	movq	616(%rsp), %r8          # 8-byte Reload
	movq	688(%rsp), %r9          # 8-byte Reload
	movq	792(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	640(%rsp), %rdi         # 8-byte Reload
	movq	648(%rsp), %rsi         # 8-byte Reload
	movq	624(%rsp), %rdx         # 8-byte Reload
	movq	752(%rsp), %rcx         # 8-byte Reload
	movq	736(%rsp), %r8          # 8-byte Reload
	movl	788(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	672(%rsp), %rdi         # 8-byte Reload
	movq	800(%rsp), %rsi         # 8-byte Reload
	movq	1120(%rsp), %rdx        # 8-byte Reload
	movq	672(%rsp), %rcx         # 8-byte Reload
	movq	744(%rsp), %r8          # 8-byte Reload
	movq	688(%rsp), %r9          # 8-byte Reload
	movq	792(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	1120(%rsp), %rcx        # 8-byte Reload
	movl	%ecx, %eax
	movl	%eax, %esi
	movl	%esi, %edx
	movq	768(%rsp), %rdi         # 8-byte Reload
	movq	760(%rsp), %rsi         # 8-byte Reload
	movq	672(%rsp), %rcx         # 8-byte Reload
	movq	752(%rsp), %r8          # 8-byte Reload
	movq	688(%rsp), %r9          # 8-byte Reload
	movq	792(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movl	%eax, 596(%rsp)         # 4-byte Spill
	callq	trace_logger_log_int
	movq	640(%rsp), %rdi         # 8-byte Reload
	movq	648(%rsp), %rsi         # 8-byte Reload
	movq	624(%rsp), %rdx         # 8-byte Reload
	movq	664(%rsp), %rcx         # 8-byte Reload
	movq	776(%rsp), %r8          # 8-byte Reload
	movl	788(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	608(%rsp), %rdi         # 8-byte Reload
	movq	800(%rsp), %rsi         # 8-byte Reload
	movq	632(%rsp), %rdx         # 8-byte Reload
	movq	672(%rsp), %rcx         # 8-byte Reload
	movq	656(%rsp), %r8          # 8-byte Reload
	movq	688(%rsp), %r9          # 8-byte Reload
	movq	792(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	672(%rsp), %rdi         # 8-byte Reload
	movq	800(%rsp), %rsi         # 8-byte Reload
	movq	600(%rsp), %rdx         # 8-byte Reload
	movq	672(%rsp), %rcx         # 8-byte Reload
	movq	616(%rsp), %r8          # 8-byte Reload
	movq	688(%rsp), %r9          # 8-byte Reload
	movq	792(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	712(%rsp), %rdi         # 8-byte Reload
	movq	800(%rsp), %rsi         # 8-byte Reload
	movq	696(%rsp), %rdx         # 8-byte Reload
	movq	688(%rsp), %rcx         # 8-byte Reload
	movq	680(%rsp), %r8          # 8-byte Reload
	movq	688(%rsp), %r9          # 8-byte Reload
	movq	792(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movl	$.L.str, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
                                        # kill: AL<def> AL<kill> EAX<kill>
	movq	600(%rsp), %rdi         # 8-byte Reload
	movl	596(%rsp), %edx         # 4-byte Reload
	movl	836(%rsp), %ecx         # 4-byte Reload
	movl	820(%rsp), %r8d         # 4-byte Reload
	callq	fprintf
	movabsq	$1, %rsi
	movabsq	$0, %rdi
	leaq	.L.str37, %r8
	leaq	.L.str9, %r9
	movabsq	$45, %r10
	leaq	.L.str35, %r11
	leaq	.L.str40, %rbx
	leaq	.L.str43, %rcx
	movabsq	$2, %r14
	movl	$0, %edx
	movl	$1, %ebp
	movabsq	$19134, %r15            # imm = 0x4ABE
	movabsq	$32, %r12
	leaq	.L.str32, %r13
	movq	%rcx, 584(%rsp)         # 8-byte Spill
	leaq	.L.str38, %rcx
	movq	%rcx, 576(%rsp)         # 8-byte Spill
	leaq	.L.str13, %rcx
	movq	%rcx, 568(%rsp)         # 8-byte Spill
	movabsq	$44, %rcx
	movq	%rcx, 560(%rsp)         # 8-byte Spill
	movabsq	$8, %rcx
	movq	%rcx, 552(%rsp)         # 8-byte Spill
	leaq	.L.str27, %rcx
	.loc	1 44 0                  # store_kernel.c:44:0
	movl	%eax, %eax
                                        # kill: RAX<def> EAX<kill>
	movq	%rdi, 544(%rsp)         # 8-byte Spill
	movq	%r15, %rdi
	movq	%rsi, 536(%rsp)         # 8-byte Spill
	movq	%r12, %rsi
	movl	%edx, 532(%rsp)         # 4-byte Spill
	movq	%rax, %rdx
	movq	536(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 520(%rsp)         # 8-byte Spill
	movq	%rax, %rcx
	movq	520(%rsp), %rax         # 8-byte Reload
	movq	%r8, 512(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movq	544(%rsp), %rax         # 8-byte Reload
	movq	%r9, 504(%rsp)          # 8-byte Spill
	movq	%rax, %r9
	movq	504(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	movq	%r13, 496(%rsp)         # 8-byte Spill
	movq	%r12, 488(%rsp)         # 8-byte Spill
	movq	%r15, 480(%rsp)         # 8-byte Spill
	movl	%ebp, 476(%rsp)         # 4-byte Spill
	movq	%r14, 464(%rsp)         # 8-byte Spill
	movq	%r10, 456(%rsp)         # 8-byte Spill
	movq	%r11, 448(%rsp)         # 8-byte Spill
	movq	%rbx, 440(%rsp)         # 8-byte Spill
	callq	trace_logger_log_int
	movq	560(%rsp), %rdi         # 8-byte Reload
	movq	448(%rsp), %rsi         # 8-byte Reload
	movq	440(%rsp), %rdx         # 8-byte Reload
	movq	496(%rsp), %rcx         # 8-byte Reload
	movq	552(%rsp), %r8          # 8-byte Reload
	movl	532(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	464(%rsp), %rdi         # 8-byte Reload
	movq	488(%rsp), %rsi         # 8-byte Reload
	movq	536(%rsp), %rdx         # 8-byte Reload
	movq	544(%rsp), %rcx         # 8-byte Reload
	movq	568(%rsp), %r8          # 8-byte Reload
	movq	544(%rsp), %r9          # 8-byte Reload
	movq	504(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	1132(%rsp), %ebp        # 4-byte Reload
	movl	%ebp, %esi
	movl	%esi, %edx
	movq	536(%rsp), %rdi         # 8-byte Reload
	movq	488(%rsp), %rsi         # 8-byte Reload
	movq	536(%rsp), %rcx         # 8-byte Reload
	movq	576(%rsp), %r8          # 8-byte Reload
	movq	544(%rsp), %r9          # 8-byte Reload
	movq	504(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	1132(%rsp), %ebp        # 4-byte Reload
	addl	$1, %ebp
	movl	%ebp, %esi
	movl	%esi, %edx
	movq	480(%rsp), %rdi         # 8-byte Reload
	movq	488(%rsp), %rsi         # 8-byte Reload
	movq	536(%rsp), %rcx         # 8-byte Reload
	movq	496(%rsp), %r8          # 8-byte Reload
	movq	544(%rsp), %r9          # 8-byte Reload
	movq	504(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	movl	%ebp, 436(%rsp)         # 4-byte Spill
	callq	trace_logger_log_int
.Ltmp53:
	#DEBUG_VALUE: test_stores:num_failures <- [RSP+436]
	.loc	1 45 0                  # store_kernel.c:45:0
	movq	456(%rsp), %rdi         # 8-byte Reload
	movq	448(%rsp), %rsi         # 8-byte Reload
	movq	440(%rsp), %rdx         # 8-byte Reload
	movq	584(%rsp), %rcx         # 8-byte Reload
	movq	464(%rsp), %r8          # 8-byte Reload
	movl	532(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	536(%rsp), %rdi         # 8-byte Reload
	movq	544(%rsp), %rsi         # 8-byte Reload
	movq	544(%rsp), %rdx         # 8-byte Reload
	movq	536(%rsp), %rcx         # 8-byte Reload
	movq	512(%rsp), %r8          # 8-byte Reload
	movq	544(%rsp), %r9          # 8-byte Reload
	movq	504(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	436(%rsp), %ebp         # 4-byte Reload
	movl	%ebp, 812(%rsp)         # 4-byte Spill
.Ltmp54:
.LBB1_3:                                #   in Loop: Header=BB1_1 Depth=1
	movl	812(%rsp), %eax         # 4-byte Reload
	movabsq	$1, %rcx
	leaq	.L.str30, %rdx
	movabsq	$0, %rsi
	leaq	.L.str9, %rdi
	movabsq	$2, %r8
	leaq	.L.str21, %r9
	movabsq	$3, %r10
	leaq	.L.str22, %r11
	movabsq	$40, %rbx
	leaq	.L.str35, %r14
	leaq	.L.str37, %r15
	leaq	.L.str44, %r12
	movl	$0, %ebp
	movl	$1, %r13d
	movq	%rcx, 424(%rsp)         # 8-byte Spill
	movabsq	$19134, %rcx            # imm = 0x4ABE
	movq	%rcx, 416(%rsp)         # 8-byte Spill
	movabsq	$32, %rcx
	movq	%rcx, 408(%rsp)         # 8-byte Spill
	leaq	.L.str29, %rcx
	movq	%rcx, 400(%rsp)         # 8-byte Spill
	leaq	.L.str11, %rcx
	movq	%rcx, 392(%rsp)         # 8-byte Spill
	movabsq	$46, %rcx
	movq	%rcx, 384(%rsp)         # 8-byte Spill
	movabsq	$64, %rcx
	movq	%rcx, 376(%rsp)         # 8-byte Spill
	leaq	.L.str24, %rcx
	movq	%rcx, 368(%rsp)         # 8-byte Spill
	movabsq	$33, %rcx
	movq	%rcx, 360(%rsp)         # 8-byte Spill
	leaq	.L.str23, %rcx
	movq	%rcx, 352(%rsp)         # 8-byte Spill
	leaq	.L.str13, %rcx
	movq	%rcx, 344(%rsp)         # 8-byte Spill
	movabsq	$8, %rcx
	movq	%rcx, 336(%rsp)         # 8-byte Spill
	leaq	.L.str39, %rcx
	movq	%rcx, 328(%rsp)         # 8-byte Spill
	leaq	.L.str40, %rcx
	movq	%rcx, 320(%rsp)         # 8-byte Spill
	leaq	.L.str32, %rcx
	movq	%rcx, 312(%rsp)         # 8-byte Spill
	leaq	.L.str38, %rcx
	movq	%rcx, 304(%rsp)         # 8-byte Spill
	movabsq	$-1, %rcx
	movq	%rcx, 296(%rsp)         # 8-byte Spill
	movabsq	$48, %rcx
	.loc	1 40 0                  # store_kernel.c:40:0
	movq	%rcx, 288(%rsp)         # 8-byte Spill
	movq	296(%rsp), %rcx         # 8-byte Reload
	movq	%rdi, 280(%rsp)         # 8-byte Spill
	movq	%rcx, %rdi
	movq	%rsi, 272(%rsp)         # 8-byte Spill
	movq	%r14, %rsi
	movq	%rdx, 264(%rsp)         # 8-byte Spill
	movq	%r15, %rdx
	movq	328(%rsp), %rcx         # 8-byte Reload
	movq	%r8, 256(%rsp)          # 8-byte Spill
	movq	288(%rsp), %r8          # 8-byte Reload
	movq	%r9, 248(%rsp)          # 8-byte Spill
	movl	%ebp, %r9d
	movl	$1, (%rsp)
	movl	%eax, 244(%rsp)         # 4-byte Spill
	movq	%r15, 232(%rsp)         # 8-byte Spill
	movq	%r12, 224(%rsp)         # 8-byte Spill
	movl	%ebp, 220(%rsp)         # 4-byte Spill
	movl	%r13d, 216(%rsp)        # 4-byte Spill
	movq	%r14, 208(%rsp)         # 8-byte Spill
	movq	%rbx, 200(%rsp)         # 8-byte Spill
	movq	%r10, 192(%rsp)         # 8-byte Spill
	movq	%r11, 184(%rsp)         # 8-byte Spill
	callq	trace_logger_log0
	movq	256(%rsp), %rdi         # 8-byte Reload
	movq	408(%rsp), %rsi         # 8-byte Reload
	movq	272(%rsp), %rdx         # 8-byte Reload
	movq	424(%rsp), %rcx         # 8-byte Reload
	movq	304(%rsp), %r8          # 8-byte Reload
	movq	424(%rsp), %r9          # 8-byte Reload
	movq	248(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	424(%rsp), %rdi         # 8-byte Reload
	movq	408(%rsp), %rsi         # 8-byte Reload
	movq	272(%rsp), %rdx         # 8-byte Reload
	movq	424(%rsp), %rcx         # 8-byte Reload
	movq	312(%rsp), %r8          # 8-byte Reload
	movq	424(%rsp), %r9          # 8-byte Reload
	movq	320(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movl	244(%rsp), %eax         # 4-byte Reload
	movl	%eax, %ebp
	movl	%ebp, %edx
	movq	416(%rsp), %rdi         # 8-byte Reload
	movq	408(%rsp), %rsi         # 8-byte Reload
	movq	424(%rsp), %rcx         # 8-byte Reload
	movq	328(%rsp), %r8          # 8-byte Reload
	movq	272(%rsp), %r9          # 8-byte Reload
	movq	320(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	200(%rsp), %rdi         # 8-byte Reload
	movq	208(%rsp), %rsi         # 8-byte Reload
	movq	232(%rsp), %rdx         # 8-byte Reload
	movq	368(%rsp), %rcx         # 8-byte Reload
	movq	336(%rsp), %r8          # 8-byte Reload
	movl	220(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	256(%rsp), %rdi         # 8-byte Reload
	movq	376(%rsp), %rsi         # 8-byte Reload
	movq	424(%rsp), %rdx         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	344(%rsp), %r8          # 8-byte Reload
	movq	272(%rsp), %r9          # 8-byte Reload
	movq	280(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	424(%rsp), %rdi         # 8-byte Reload
	movq	376(%rsp), %rsi         # 8-byte Reload
	movq	1120(%rsp), %rdx        # 8-byte Reload
	movq	424(%rsp), %rcx         # 8-byte Reload
	movq	352(%rsp), %r8          # 8-byte Reload
	movq	272(%rsp), %r9          # 8-byte Reload
	movq	280(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	1120(%rsp), %rcx        # 8-byte Reload
	addq	$1, %rcx
	movq	416(%rsp), %rdi         # 8-byte Reload
	movq	376(%rsp), %rsi         # 8-byte Reload
	movq	%rcx, %rdx
	movq	424(%rsp), %r8          # 8-byte Reload
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	movq	%r8, %rcx
	movq	368(%rsp), %r8          # 8-byte Reload
	movq	272(%rsp), %r9          # 8-byte Reload
	movq	280(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	200(%rsp), %rdi         # 8-byte Reload
	movq	208(%rsp), %rsi         # 8-byte Reload
	movq	232(%rsp), %rdx         # 8-byte Reload
	movq	400(%rsp), %rcx         # 8-byte Reload
	movq	360(%rsp), %r8          # 8-byte Reload
	movl	220(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	424(%rsp), %rdi         # 8-byte Reload
	movq	376(%rsp), %rsi         # 8-byte Reload
	movq	176(%rsp), %rdx         # 8-byte Reload
	movq	424(%rsp), %rcx         # 8-byte Reload
	movq	368(%rsp), %r8          # 8-byte Reload
	movq	272(%rsp), %r9          # 8-byte Reload
	movq	280(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	176(%rsp), %rcx         # 8-byte Reload
	movl	%ecx, %eax
	movl	%eax, %ebp
	movl	%ebp, %edx
	movq	416(%rsp), %rdi         # 8-byte Reload
	movq	408(%rsp), %rsi         # 8-byte Reload
	movq	424(%rsp), %rcx         # 8-byte Reload
	movq	400(%rsp), %r8          # 8-byte Reload
	movq	272(%rsp), %r9          # 8-byte Reload
	movq	280(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movl	%eax, 172(%rsp)         # 4-byte Spill
	callq	trace_logger_log_int
	movq	200(%rsp), %rdi         # 8-byte Reload
	movq	208(%rsp), %rsi         # 8-byte Reload
	movq	232(%rsp), %rdx         # 8-byte Reload
	movq	264(%rsp), %rcx         # 8-byte Reload
	movq	384(%rsp), %r8          # 8-byte Reload
	movl	220(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movl	1236(%rsp), %eax        # 4-byte Reload
	movl	%eax, %r9d
	movl	%r9d, %edx
	movq	256(%rsp), %rdi         # 8-byte Reload
	movq	408(%rsp), %rsi         # 8-byte Reload
	movq	424(%rsp), %rcx         # 8-byte Reload
	movq	392(%rsp), %r8          # 8-byte Reload
	movq	272(%rsp), %r9          # 8-byte Reload
	movq	280(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movl	172(%rsp), %eax         # 4-byte Reload
	movl	%eax, %ebp
	movl	%ebp, %edx
	movq	424(%rsp), %rdi         # 8-byte Reload
	movq	408(%rsp), %rsi         # 8-byte Reload
	movq	424(%rsp), %rcx         # 8-byte Reload
	movq	400(%rsp), %r8          # 8-byte Reload
	movq	272(%rsp), %r9          # 8-byte Reload
	movq	280(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movl	172(%rsp), %eax         # 4-byte Reload
	movl	1236(%rsp), %ebp        # 4-byte Reload
	cmpl	%ebp, %eax
	sete	%al
	movb	%al, %cl
	andb	$1, %cl
	movzbl	%cl, %r13d
	movl	%r13d, %edx
	movq	416(%rsp), %rdi         # 8-byte Reload
	movq	424(%rsp), %rsi         # 8-byte Reload
	movq	424(%rsp), %rcx         # 8-byte Reload
	movq	264(%rsp), %r8          # 8-byte Reload
	movq	272(%rsp), %r9          # 8-byte Reload
	movq	280(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movb	%al, 171(%rsp)          # 1-byte Spill
	callq	trace_logger_log_int
	movq	200(%rsp), %rdi         # 8-byte Reload
	movq	208(%rsp), %rsi         # 8-byte Reload
	movq	232(%rsp), %rdx         # 8-byte Reload
	movq	224(%rsp), %rcx         # 8-byte Reload
	movq	256(%rsp), %r8          # 8-byte Reload
	movl	220(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	192(%rsp), %rdi         # 8-byte Reload
	movq	272(%rsp), %rsi         # 8-byte Reload
	movq	272(%rsp), %rdx         # 8-byte Reload
	movq	424(%rsp), %rcx         # 8-byte Reload
	movq	184(%rsp), %r8          # 8-byte Reload
	movq	272(%rsp), %r9          # 8-byte Reload
	movq	280(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	256(%rsp), %rdi         # 8-byte Reload
	movq	272(%rsp), %rsi         # 8-byte Reload
	movq	272(%rsp), %rdx         # 8-byte Reload
	movq	424(%rsp), %rcx         # 8-byte Reload
	movq	248(%rsp), %r8          # 8-byte Reload
	movq	272(%rsp), %r9          # 8-byte Reload
	movq	280(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	171(%rsp), %al          # 1-byte Reload
	andb	$1, %al
	movzbl	%al, %ebp
	movl	%ebp, %edx
	movq	424(%rsp), %rdi         # 8-byte Reload
	movq	424(%rsp), %rsi         # 8-byte Reload
	movq	424(%rsp), %rcx         # 8-byte Reload
	movq	264(%rsp), %r8          # 8-byte Reload
	movq	272(%rsp), %r9          # 8-byte Reload
	movq	280(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	171(%rsp), %al          # 1-byte Reload
	testb	$1, %al
	movq	176(%rsp), %rcx         # 8-byte Reload
	movl	244(%rsp), %ebp         # 4-byte Reload
	movq	%rcx, 1136(%rsp)        # 8-byte Spill
	movl	%ebp, 1144(%rsp)        # 4-byte Spill
	jne	.LBB1_4
	jmp	.LBB1_1
.Ltmp55:
.LBB1_4:                                # %._crit_edge
	movl	1144(%rsp), %eax        # 4-byte Reload
	movabsq	$1, %rcx
	movabsq	$32, %rdx
	leaq	.L.str45, %rsi
	movabsq	$0, %rdi
	leaq	.L.str9, %r8
	movabsq	$47, %r9
	leaq	.L.str35, %r10
	leaq	.L.str22, %r11
	leaq	.L.str34, %rbx
	movl	$0, %ebp
	movl	$1, %r14d
	movabsq	$19134, %r15            # imm = 0x4ABE
	leaq	.L.str6, %r12
	leaq	.L.str13, %r13
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	movabsq	$2, %rcx
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	leaq	.L.str39, %rcx
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	leaq	.L.str37, %rcx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	movabsq	$-1, %rcx
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	movabsq	$48, %rcx
	.loc	1 47 0                  # store_kernel.c:47:0
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	movq	128(%rsp), %rcx         # 8-byte Reload
	movq	%rdi, 112(%rsp)         # 8-byte Spill
	movq	%rcx, %rdi
	movq	%rsi, 104(%rsp)         # 8-byte Spill
	movq	%r10, %rsi
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	movq	%r11, %rdx
	movq	104(%rsp), %rcx         # 8-byte Reload
	movq	%r8, 88(%rsp)           # 8-byte Spill
	movq	120(%rsp), %r8          # 8-byte Reload
	movq	%r9, 80(%rsp)           # 8-byte Spill
	movl	%ebp, %r9d
	movl	$1, (%rsp)
	movl	%eax, 76(%rsp)          # 4-byte Spill
	movl	%r14d, 72(%rsp)         # 4-byte Spill
	movq	%r15, 64(%rsp)          # 8-byte Spill
	movq	%r12, 56(%rsp)          # 8-byte Spill
	movq	%r13, 48(%rsp)          # 8-byte Spill
	movl	%ebp, 44(%rsp)          # 4-byte Spill
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	%r10, 24(%rsp)          # 8-byte Spill
	movq	%r11, 16(%rsp)          # 8-byte Spill
	callq	trace_logger_log0
	movq	152(%rsp), %rdi         # 8-byte Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	112(%rsp), %rdx         # 8-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	144(%rsp), %r8          # 8-byte Reload
	movq	160(%rsp), %r9          # 8-byte Reload
	movq	136(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	160(%rsp), %rdi         # 8-byte Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	112(%rsp), %rdx         # 8-byte Reload
	movq	112(%rsp), %rcx         # 8-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	160(%rsp), %r9          # 8-byte Reload
	movq	56(%rsp), %r10          # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movl	76(%rsp), %eax          # 4-byte Reload
	movl	%eax, %ebp
	movl	%ebp, %edx
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	104(%rsp), %r8          # 8-byte Reload
	movq	112(%rsp), %r9          # 8-byte Reload
	movq	56(%rsp), %r10          # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	80(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	160(%rsp), %r8          # 8-byte Reload
	movl	44(%rsp), %r9d          # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movl	76(%rsp), %eax          # 4-byte Reload
	movl	%eax, %r9d
	movl	%r9d, %edx
	movq	160(%rsp), %rdi         # 8-byte Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	104(%rsp), %r8          # 8-byte Reload
	movq	112(%rsp), %r9          # 8-byte Reload
	movq	88(%rsp), %r10          # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movl	76(%rsp), %eax          # 4-byte Reload
	addq	$1352, %rsp             # imm = 0x548
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.Ltmp56:
.Ltmp57:
	.size	test_stores, .Ltmp57-test_stores
.Lfunc_end1:
	.cfi_endproc

	.globl	main
	.align	16, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
.Lfunc_begin2:
	.loc	1 51 0                  # store_kernel.c:51:0
# BB#0:
	pushq	%rbp
.Ltmp65:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Ltmp66:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Ltmp67:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Ltmp68:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Ltmp69:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Ltmp70:
	.cfi_def_cfa_offset 56
	subq	$2968, %rsp             # imm = 0xB98
.Ltmp71:
	.cfi_def_cfa_offset 3024
.Ltmp72:
	.cfi_offset %rbx, -56
.Ltmp73:
	.cfi_offset %r12, -48
.Ltmp74:
	.cfi_offset %r13, -40
.Ltmp75:
	.cfi_offset %r14, -32
.Ltmp76:
	.cfi_offset %r15, -24
.Ltmp77:
	.cfi_offset %rbp, -16
	leaq	.L.str84, %rdi
	movabsq	$21, %rsi
	callq	trace_logger_write_labelmap
	movabsq	$64, %rsi
	movabsq	$4096, %rdi             # imm = 0x1000
	movabsq	$3, %rax
	movabsq	$0, %rcx
	leaq	.L.str13, %rdx
	leaq	.L.str9, %r8
	movabsq	$2, %r9
	movabsq	$1, %r10
	leaq	.L.str8, %r11
	movabsq	$4, %rbx
	leaq	posix_memalign, %r14
	leaq	.L.str47, %r15
	movabsq	$54, %r12
	leaq	.L.str46, %r13
	leaq	.L.str6, %rbp
	movq	%rax, 2944(%rsp)        # 8-byte Spill
	leaq	.L.str10, %rax
	movq	%rax, 2936(%rsp)        # 8-byte Spill
	movabsq	$49, %rax
	movq	%rsi, 2928(%rsp)        # 8-byte Spill
	movl	$0, %esi
	movl	%esi, 2924(%rsp)        # 4-byte Spill
	movl	$1, %esi
	movq	%rax, 2912(%rsp)        # 8-byte Spill
	movabsq	$19134, %rax            # imm = 0x4ABE
	movq	%rax, 2904(%rsp)        # 8-byte Spill
	leaq	2960(%rsp), %rax
	movq	%rax, 2896(%rsp)        # 8-byte Spill
	leaq	.L.str7, %rax
	movq	%rax, 2888(%rsp)        # 8-byte Spill
	movabsq	$44, %rax
	movq	%rax, 2880(%rsp)        # 8-byte Spill
	leaq	.L.str17, %rax
	movq	%rax, 2872(%rsp)        # 8-byte Spill
	leaq	2952(%rsp), %rax
	movq	%rax, 2864(%rsp)        # 8-byte Spill
	movabsq	$32, %rax
	movq	%rax, 2856(%rsp)        # 8-byte Spill
	movabsq	$-1, %rax
	movq	%rax, 2848(%rsp)        # 8-byte Spill
	movabsq	$26, %rax
	movq	%rax, 2840(%rsp)        # 8-byte Spill
	movq	2848(%rsp), %rax        # 8-byte Reload
	movq	%rdi, 2832(%rsp)        # 8-byte Spill
	movq	%rax, %rdi
	movl	%esi, 2828(%rsp)        # 4-byte Spill
	movq	%r13, %rsi
	movq	%rdx, 2816(%rsp)        # 8-byte Spill
	movq	%rbp, %rdx
	movq	%rcx, 2808(%rsp)        # 8-byte Spill
	movq	%r11, %rcx
	movq	2840(%rsp), %rax        # 8-byte Reload
	movq	%r8, 2800(%rsp)         # 8-byte Spill
	movq	%rax, %r8
	movl	2924(%rsp), %eax        # 4-byte Reload
	movq	%r9, 2792(%rsp)         # 8-byte Spill
	movl	%eax, %r9d
	movl	$1, (%rsp)
	movq	%r13, 2784(%rsp)        # 8-byte Spill
	movq	%rbp, 2776(%rsp)        # 8-byte Spill
	movq	%r15, 2768(%rsp)        # 8-byte Spill
	movq	%r12, 2760(%rsp)        # 8-byte Spill
	movq	%r14, 2752(%rsp)        # 8-byte Spill
	movq	%rbx, 2744(%rsp)        # 8-byte Spill
	movq	%r10, 2736(%rsp)        # 8-byte Spill
	movq	%r11, 2728(%rsp)        # 8-byte Spill
	callq	trace_logger_log0
	movq	2736(%rsp), %rdi        # 8-byte Reload
	movq	2856(%rsp), %rsi        # 8-byte Reload
	movq	2736(%rsp), %rdx        # 8-byte Reload
	movq	2808(%rsp), %rcx        # 8-byte Reload
	movq	2816(%rsp), %r8         # 8-byte Reload
	movq	2808(%rsp), %r9         # 8-byte Reload
	movq	2800(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	2904(%rsp), %rdi        # 8-byte Reload
	movq	2928(%rsp), %rsi        # 8-byte Reload
	movq	2896(%rsp), %rdx        # 8-byte Reload
	movq	2736(%rsp), %rcx        # 8-byte Reload
	movq	2728(%rsp), %r8         # 8-byte Reload
	movq	2808(%rsp), %r9         # 8-byte Reload
	movq	2800(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	2848(%rsp), %rdi        # 8-byte Reload
	movq	2784(%rsp), %rsi        # 8-byte Reload
	movq	2776(%rsp), %rdx        # 8-byte Reload
	movq	2872(%rsp), %rcx        # 8-byte Reload
	movq	2840(%rsp), %r8         # 8-byte Reload
	movl	2924(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	2736(%rsp), %rdi        # 8-byte Reload
	movq	2856(%rsp), %rsi        # 8-byte Reload
	movq	2736(%rsp), %rdx        # 8-byte Reload
	movq	2808(%rsp), %rcx        # 8-byte Reload
	movq	2816(%rsp), %r8         # 8-byte Reload
	movq	2808(%rsp), %r9         # 8-byte Reload
	movq	2800(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	.loc	1 52 0 prologue_end     # store_kernel.c:52:0
.Ltmp78:
	movq	2904(%rsp), %rdi        # 8-byte Reload
	movq	2928(%rsp), %rsi        # 8-byte Reload
	movq	2864(%rsp), %rdx        # 8-byte Reload
	movq	2736(%rsp), %rcx        # 8-byte Reload
	movq	2872(%rsp), %r8         # 8-byte Reload
	movq	2808(%rsp), %r9         # 8-byte Reload
	movq	2800(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
.Ltmp79:
	#DEBUG_VALUE: main:num_vals <- 1024
	#DEBUG_VALUE: main:store_vals <- [RSP+2896]
	.loc	1 54 0                  # store_kernel.c:54:0
	movq	2760(%rsp), %rdi        # 8-byte Reload
	movq	2784(%rsp), %rsi        # 8-byte Reload
	movq	2776(%rsp), %rdx        # 8-byte Reload
	movq	2888(%rsp), %rcx        # 8-byte Reload
	movq	2880(%rsp), %r8         # 8-byte Reload
	movl	2924(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	2736(%rsp), %rdi        # 8-byte Reload
	movq	2928(%rsp), %rsi        # 8-byte Reload
	movq	2896(%rsp), %rdx        # 8-byte Reload
	movq	2736(%rsp), %rcx        # 8-byte Reload
	movq	2728(%rsp), %r8         # 8-byte Reload
	movq	2808(%rsp), %r9         # 8-byte Reload
	movq	2800(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	2896(%rsp), %rcx        # 8-byte Reload
	movq	2904(%rsp), %rdi        # 8-byte Reload
	movq	2928(%rsp), %rsi        # 8-byte Reload
	movq	%rcx, %rdx
	movq	2736(%rsp), %r8         # 8-byte Reload
	movq	%rcx, 2720(%rsp)        # 8-byte Spill
	movq	%r8, %rcx
	movq	2728(%rsp), %r8         # 8-byte Reload
	movq	2808(%rsp), %r9         # 8-byte Reload
	movq	2800(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	2760(%rsp), %rdi        # 8-byte Reload
	movq	2784(%rsp), %rsi        # 8-byte Reload
	movq	2776(%rsp), %rdx        # 8-byte Reload
	movq	2936(%rsp), %rcx        # 8-byte Reload
	movq	2912(%rsp), %r8         # 8-byte Reload
	movl	2924(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	2744(%rsp), %rdi        # 8-byte Reload
	movq	2928(%rsp), %rsi        # 8-byte Reload
	movq	2752(%rsp), %rdx        # 8-byte Reload
	movq	2736(%rsp), %rcx        # 8-byte Reload
	movq	2768(%rsp), %r8         # 8-byte Reload
	movq	2808(%rsp), %r9         # 8-byte Reload
	movq	2800(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	2736(%rsp), %rdi        # 8-byte Reload
	movq	2928(%rsp), %rsi        # 8-byte Reload
	movq	2720(%rsp), %rdx        # 8-byte Reload
	movq	2736(%rsp), %rcx        # 8-byte Reload
	movq	2728(%rsp), %r8         # 8-byte Reload
	movq	2808(%rsp), %r9         # 8-byte Reload
	movq	2800(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	2792(%rsp), %rdi        # 8-byte Reload
	movq	2928(%rsp), %rsi        # 8-byte Reload
	movq	2928(%rsp), %rdx        # 8-byte Reload
	movq	2808(%rsp), %rcx        # 8-byte Reload
	movq	2816(%rsp), %r8         # 8-byte Reload
	movq	2808(%rsp), %r9         # 8-byte Reload
	movq	2800(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	2944(%rsp), %rdi        # 8-byte Reload
	movq	2928(%rsp), %rsi        # 8-byte Reload
	movq	2832(%rsp), %rdx        # 8-byte Reload
	movq	2808(%rsp), %rcx        # 8-byte Reload
	movq	2816(%rsp), %r8         # 8-byte Reload
	movq	2808(%rsp), %r9         # 8-byte Reload
	movq	2800(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	2720(%rsp), %rdi        # 8-byte Reload
	movq	2928(%rsp), %rsi        # 8-byte Reload
	movq	2832(%rsp), %rdx        # 8-byte Reload
	callq	posix_memalign
	movabsq	$1, %rcx
	leaq	.L.str12, %rdx
	movabsq	$0, %rsi
	leaq	.L.str9, %rdi
	movabsq	$2, %r8
	leaq	.L.str49, %r9
	movabsq	$3, %r10
	leaq	.L.str48, %r11
	movabsq	$56, %rbx
	leaq	.L.str46, %r14
	leaq	.L.str6, %r15
	leaq	.L.str20, %r12
	movl	$0, %ebp
	movl	$1, %r13d
	movq	%rcx, 2712(%rsp)        # 8-byte Spill
	movabsq	$19134, %rcx            # imm = 0x4ABE
	movq	%rcx, 2704(%rsp)        # 8-byte Spill
	movabsq	$32, %rcx
	movq	%rcx, 2696(%rsp)        # 8-byte Spill
	leaq	.L.str10, %rcx
	movq	%rcx, 2688(%rsp)        # 8-byte Spill
	leaq	.L.str13, %rcx
	movq	%rcx, 2680(%rsp)        # 8-byte Spill
	movabsq	$46, %rcx
	movl	%eax, 2676(%rsp)        # 4-byte Spill
	movl	%eax, %eax
                                        # kill: RAX<def> EAX<kill>
	movq	%rax, 2664(%rsp)        # 8-byte Spill
	movq	2704(%rsp), %rax        # 8-byte Reload
	movq	%rdi, 2656(%rsp)        # 8-byte Spill
	movq	%rax, %rdi
	movq	2696(%rsp), %rax        # 8-byte Reload
	movq	%rsi, 2648(%rsp)        # 8-byte Spill
	movq	%rax, %rsi
	movq	2664(%rsp), %rax        # 8-byte Reload
	movq	%rdx, 2640(%rsp)        # 8-byte Spill
	movq	%rax, %rdx
	movq	2712(%rsp), %rax        # 8-byte Reload
	movq	%rcx, 2632(%rsp)        # 8-byte Spill
	movq	%rax, %rcx
	movq	2688(%rsp), %rax        # 8-byte Reload
	movq	%r8, 2624(%rsp)         # 8-byte Spill
	movq	%rax, %r8
	movq	2648(%rsp), %rax        # 8-byte Reload
	movq	%r9, 2616(%rsp)         # 8-byte Spill
	movq	%rax, %r9
	movq	2656(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	movl	%r13d, 2612(%rsp)       # 4-byte Spill
	movq	%r15, 2600(%rsp)        # 8-byte Spill
	movq	%r12, 2592(%rsp)        # 8-byte Spill
	movl	%ebp, 2588(%rsp)        # 4-byte Spill
	movq	%r14, 2576(%rsp)        # 8-byte Spill
	movq	%rbx, 2568(%rsp)        # 8-byte Spill
	movq	%r10, 2560(%rsp)        # 8-byte Spill
	movq	%r11, 2552(%rsp)        # 8-byte Spill
	callq	trace_logger_log_int
.Ltmp80:
	#DEBUG_VALUE: main:err <- [RSP+2676]
	.loc	1 56 0                  # store_kernel.c:56:0
	movq	2568(%rsp), %rdi        # 8-byte Reload
	movq	2576(%rsp), %rsi        # 8-byte Reload
	movq	2600(%rsp), %rdx        # 8-byte Reload
	movq	2640(%rsp), %rcx        # 8-byte Reload
	movq	2632(%rsp), %r8         # 8-byte Reload
	movl	2588(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	2624(%rsp), %rdi        # 8-byte Reload
	movq	2696(%rsp), %rsi        # 8-byte Reload
	movq	2648(%rsp), %rdx        # 8-byte Reload
	movq	2648(%rsp), %rcx        # 8-byte Reload
	movq	2680(%rsp), %r8         # 8-byte Reload
	movq	2648(%rsp), %r9         # 8-byte Reload
	movq	2656(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	2676(%rsp), %ebp        # 4-byte Reload
	movl	%ebp, %r13d
	movl	%r13d, %edx
	movq	2712(%rsp), %rdi        # 8-byte Reload
	movq	2696(%rsp), %rsi        # 8-byte Reload
	movq	2712(%rsp), %rcx        # 8-byte Reload
	movq	2688(%rsp), %r8         # 8-byte Reload
	movq	2648(%rsp), %r9         # 8-byte Reload
	movq	2656(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	2676(%rsp), %ebp        # 4-byte Reload
	cmpl	$0, %ebp
	sete	%al
	movb	%al, %cl
	andb	$1, %cl
	movzbl	%cl, %r13d
	movl	%r13d, %edx
	movq	2704(%rsp), %rdi        # 8-byte Reload
	movq	2712(%rsp), %rsi        # 8-byte Reload
	movq	2712(%rsp), %rcx        # 8-byte Reload
	movq	2640(%rsp), %r8         # 8-byte Reload
	movq	2648(%rsp), %r9         # 8-byte Reload
	movq	2656(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	movb	%al, 2551(%rsp)         # 1-byte Spill
	callq	trace_logger_log_int
	movq	2568(%rsp), %rdi        # 8-byte Reload
	movq	2576(%rsp), %rsi        # 8-byte Reload
	movq	2600(%rsp), %rdx        # 8-byte Reload
	movq	2592(%rsp), %rcx        # 8-byte Reload
	movq	2624(%rsp), %r8         # 8-byte Reload
	movl	2588(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	2560(%rsp), %rdi        # 8-byte Reload
	movq	2648(%rsp), %rsi        # 8-byte Reload
	movq	2648(%rsp), %rdx        # 8-byte Reload
	movq	2712(%rsp), %rcx        # 8-byte Reload
	movq	2552(%rsp), %r8         # 8-byte Reload
	movq	2648(%rsp), %r9         # 8-byte Reload
	movq	2656(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	2624(%rsp), %rdi        # 8-byte Reload
	movq	2648(%rsp), %rsi        # 8-byte Reload
	movq	2648(%rsp), %rdx        # 8-byte Reload
	movq	2712(%rsp), %rcx        # 8-byte Reload
	movq	2616(%rsp), %r8         # 8-byte Reload
	movq	2648(%rsp), %r9         # 8-byte Reload
	movq	2656(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	2551(%rsp), %al         # 1-byte Reload
	andb	$1, %al
	movzbl	%al, %ebp
	movl	%ebp, %edx
	movq	2712(%rsp), %rdi        # 8-byte Reload
	movq	2712(%rsp), %rsi        # 8-byte Reload
	movq	2712(%rsp), %rcx        # 8-byte Reload
	movq	2640(%rsp), %r8         # 8-byte Reload
	movq	2648(%rsp), %r9         # 8-byte Reload
	movq	2656(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	2551(%rsp), %al         # 1-byte Reload
	testb	$1, %al
	jne	.LBB2_2
.Ltmp81:
# BB#1:
	leaq	.L.str2, %rax
	leaq	.L.str3, %rcx
	movl	$56, %edx
	leaq	.L__PRETTY_FUNCTION__.main, %rsi
	movabsq	$4, %rdi
	movabsq	$64, %r8
	movabsq	$0, %r9
	leaq	.L.str13, %r10
	leaq	.L.str9, %r11
	movabsq	$3, %rbx
	movabsq	$32, %r14
	movabsq	$56, %r15
	movabsq	$2, %r12
	movabsq	$1, %r13
	movabsq	$5, %rbp
	movq	%rax, 2536(%rsp)        # 8-byte Spill
	leaq	__assert_fail, %rax
	movq	%rax, 2528(%rsp)        # 8-byte Spill
	leaq	.L.str51, %rax
	movq	%rax, 2520(%rsp)        # 8-byte Spill
	leaq	.L.str46, %rax
	movq	%rax, 2512(%rsp)        # 8-byte Spill
	leaq	.L.str49, %rax
	movq	%rax, 2504(%rsp)        # 8-byte Spill
	leaq	.L.str50, %rax
	movq	%rax, 2496(%rsp)        # 8-byte Spill
	movabsq	$49, %rax
	movl	%edx, 2492(%rsp)        # 4-byte Spill
	movl	$0, %edx
	movl	%edx, 2488(%rsp)        # 4-byte Spill
	movl	$1, %edx
	movq	%rdi, 2480(%rsp)        # 8-byte Spill
	movq	%r15, %rdi
	movq	%rax, 2472(%rsp)        # 8-byte Spill
	movq	2512(%rsp), %rax        # 8-byte Reload
	movq	%rsi, 2464(%rsp)        # 8-byte Spill
	movq	%rax, %rsi
	movq	2504(%rsp), %rax        # 8-byte Reload
	movl	%edx, 2460(%rsp)        # 4-byte Spill
	movq	%rax, %rdx
	movq	2496(%rsp), %rax        # 8-byte Reload
	movq	%rcx, 2448(%rsp)        # 8-byte Spill
	movq	%rax, %rcx
	movq	2472(%rsp), %rax        # 8-byte Reload
	movq	%r8, 2440(%rsp)         # 8-byte Spill
	movq	%rax, %r8
	movl	2488(%rsp), %eax        # 4-byte Reload
	movq	%r9, 2432(%rsp)         # 8-byte Spill
	movl	%eax, %r9d
	movl	$1, (%rsp)
	movq	%r15, 2424(%rsp)        # 8-byte Spill
	movq	%r12, 2416(%rsp)        # 8-byte Spill
	movq	%r13, 2408(%rsp)        # 8-byte Spill
	movq	%rbp, 2400(%rsp)        # 8-byte Spill
	movq	%r14, 2392(%rsp)        # 8-byte Spill
	movq	%rbx, 2384(%rsp)        # 8-byte Spill
	movq	%r10, 2376(%rsp)        # 8-byte Spill
	movq	%r11, 2368(%rsp)        # 8-byte Spill
	callq	trace_logger_log0
	movq	2400(%rsp), %rdi        # 8-byte Reload
	movq	2440(%rsp), %rsi        # 8-byte Reload
	movq	2528(%rsp), %rdx        # 8-byte Reload
	movq	2408(%rsp), %rcx        # 8-byte Reload
	movq	2520(%rsp), %r8         # 8-byte Reload
	movq	2432(%rsp), %r9         # 8-byte Reload
	movq	2368(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	2408(%rsp), %rdi        # 8-byte Reload
	movq	2440(%rsp), %rsi        # 8-byte Reload
	movq	2536(%rsp), %rdx        # 8-byte Reload
	movq	2432(%rsp), %rcx        # 8-byte Reload
	movq	2376(%rsp), %r8         # 8-byte Reload
	movq	2432(%rsp), %r9         # 8-byte Reload
	movq	2368(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	2416(%rsp), %rdi        # 8-byte Reload
	movq	2440(%rsp), %rsi        # 8-byte Reload
	movq	2448(%rsp), %rdx        # 8-byte Reload
	movq	2432(%rsp), %rcx        # 8-byte Reload
	movq	2376(%rsp), %r8         # 8-byte Reload
	movq	2432(%rsp), %r9         # 8-byte Reload
	movq	2368(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	2384(%rsp), %rdi        # 8-byte Reload
	movq	2392(%rsp), %rsi        # 8-byte Reload
	movq	2424(%rsp), %rdx        # 8-byte Reload
	movq	2432(%rsp), %rcx        # 8-byte Reload
	movq	2376(%rsp), %r8         # 8-byte Reload
	movq	2432(%rsp), %r9         # 8-byte Reload
	movq	2368(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	2480(%rsp), %rdi        # 8-byte Reload
	movq	2440(%rsp), %rsi        # 8-byte Reload
	movq	2464(%rsp), %rdx        # 8-byte Reload
	movq	2432(%rsp), %rcx        # 8-byte Reload
	movq	2376(%rsp), %r8         # 8-byte Reload
	movq	2432(%rsp), %r9         # 8-byte Reload
	movq	2368(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	2536(%rsp), %rdi        # 8-byte Reload
	movq	2448(%rsp), %rsi        # 8-byte Reload
	movl	2492(%rsp), %edx        # 4-byte Reload
	movq	2464(%rsp), %rcx        # 8-byte Reload
	callq	__assert_fail
	movabsq	$56, %rdi
	leaq	.L.str46, %rsi
	leaq	.L.str49, %rdx
	leaq	.L.str52, %rcx
	movabsq	$7, %r8
	movl	$0, %r9d
	movl	$1, %eax
	movl	$1, (%rsp)
	movl	%eax, 2364(%rsp)        # 4-byte Spill
	callq	trace_logger_log0
.LBB2_2:
	movabsq	$64, %rax
	movabsq	$4096, %rcx             # imm = 0x1000
	movabsq	$3, %rdi
	movabsq	$0, %rdx
	leaq	.L.str13, %rsi
	leaq	.L.str9, %r8
	movabsq	$2, %r9
	movabsq	$1, %r10
	leaq	.L.str17, %r11
	movabsq	$4, %rbx
	leaq	posix_memalign, %r14
	leaq	.L.str47, %r15
	movabsq	$57, %r12
	leaq	.L.str46, %r13
	leaq	.L.str48, %rbp
	movq	%rax, 2352(%rsp)        # 8-byte Spill
	leaq	.L.str19, %rax
	movq	%rax, 2344(%rsp)        # 8-byte Spill
	movabsq	$49, %rax
	movq	%rsi, 2336(%rsp)        # 8-byte Spill
	movl	$0, %esi
	movl	%esi, 2332(%rsp)        # 4-byte Spill
	movl	$1, %esi
	movq	%rax, 2320(%rsp)        # 8-byte Spill
	movabsq	$19134, %rax            # imm = 0x4ABE
	movq	%rax, 2312(%rsp)        # 8-byte Spill
	leaq	2952(%rsp), %rax
	movq	%rax, 2304(%rsp)        # 8-byte Spill
	leaq	.L.str18, %rax
	movq	%rax, 2296(%rsp)        # 8-byte Spill
	movabsq	$44, %rax
	.loc	1 57 0                  # store_kernel.c:57:0
	movq	%rdi, 2288(%rsp)        # 8-byte Spill
	movq	%r12, %rdi
	movl	%esi, 2284(%rsp)        # 4-byte Spill
	movq	%r13, %rsi
	movq	%rdx, 2272(%rsp)        # 8-byte Spill
	movq	%rbp, %rdx
	movq	%rax, 2264(%rsp)        # 8-byte Spill
	movq	2296(%rsp), %rax        # 8-byte Reload
	movq	%rcx, 2256(%rsp)        # 8-byte Spill
	movq	%rax, %rcx
	movq	2264(%rsp), %rax        # 8-byte Reload
	movq	%r8, 2248(%rsp)         # 8-byte Spill
	movq	%rax, %r8
	movl	2332(%rsp), %eax        # 4-byte Reload
	movq	%r9, 2240(%rsp)         # 8-byte Spill
	movl	%eax, %r9d
	movl	$1, (%rsp)
	movq	%r15, 2232(%rsp)        # 8-byte Spill
	movq	%rbp, 2224(%rsp)        # 8-byte Spill
	movq	%r12, 2216(%rsp)        # 8-byte Spill
	movq	%r13, 2208(%rsp)        # 8-byte Spill
	movq	%r14, 2200(%rsp)        # 8-byte Spill
	movq	%rbx, 2192(%rsp)        # 8-byte Spill
	movq	%r10, 2184(%rsp)        # 8-byte Spill
	movq	%r11, 2176(%rsp)        # 8-byte Spill
	callq	trace_logger_log0
	movq	2184(%rsp), %rdi        # 8-byte Reload
	movq	2352(%rsp), %rsi        # 8-byte Reload
	movq	2304(%rsp), %rdx        # 8-byte Reload
	movq	2184(%rsp), %rcx        # 8-byte Reload
	movq	2176(%rsp), %r8         # 8-byte Reload
	movq	2272(%rsp), %r9         # 8-byte Reload
	movq	2248(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	2304(%rsp), %rcx        # 8-byte Reload
	movq	2312(%rsp), %rdi        # 8-byte Reload
	movq	2352(%rsp), %rsi        # 8-byte Reload
	movq	%rcx, %rdx
	movq	2184(%rsp), %r8         # 8-byte Reload
	movq	%rcx, 2168(%rsp)        # 8-byte Spill
	movq	%r8, %rcx
	movq	2176(%rsp), %r8         # 8-byte Reload
	movq	2272(%rsp), %r9         # 8-byte Reload
	movq	2248(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	2216(%rsp), %rdi        # 8-byte Reload
	movq	2208(%rsp), %rsi        # 8-byte Reload
	movq	2224(%rsp), %rdx        # 8-byte Reload
	movq	2344(%rsp), %rcx        # 8-byte Reload
	movq	2320(%rsp), %r8         # 8-byte Reload
	movl	2332(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	2192(%rsp), %rdi        # 8-byte Reload
	movq	2352(%rsp), %rsi        # 8-byte Reload
	movq	2200(%rsp), %rdx        # 8-byte Reload
	movq	2184(%rsp), %rcx        # 8-byte Reload
	movq	2232(%rsp), %r8         # 8-byte Reload
	movq	2272(%rsp), %r9         # 8-byte Reload
	movq	2248(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	2184(%rsp), %rdi        # 8-byte Reload
	movq	2352(%rsp), %rsi        # 8-byte Reload
	movq	2168(%rsp), %rdx        # 8-byte Reload
	movq	2184(%rsp), %rcx        # 8-byte Reload
	movq	2176(%rsp), %r8         # 8-byte Reload
	movq	2272(%rsp), %r9         # 8-byte Reload
	movq	2248(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	2240(%rsp), %rdi        # 8-byte Reload
	movq	2352(%rsp), %rsi        # 8-byte Reload
	movq	2352(%rsp), %rdx        # 8-byte Reload
	movq	2272(%rsp), %rcx        # 8-byte Reload
	movq	2336(%rsp), %r8         # 8-byte Reload
	movq	2272(%rsp), %r9         # 8-byte Reload
	movq	2248(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	2288(%rsp), %rdi        # 8-byte Reload
	movq	2352(%rsp), %rsi        # 8-byte Reload
	movq	2256(%rsp), %rdx        # 8-byte Reload
	movq	2272(%rsp), %rcx        # 8-byte Reload
	movq	2336(%rsp), %r8         # 8-byte Reload
	movq	2272(%rsp), %r9         # 8-byte Reload
	movq	2248(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	2168(%rsp), %rdi        # 8-byte Reload
	movq	2352(%rsp), %rsi        # 8-byte Reload
	movq	2256(%rsp), %rdx        # 8-byte Reload
	callq	posix_memalign
	movabsq	$1, %rcx
	leaq	.L.str25, %rdx
	movabsq	$0, %rsi
	leaq	.L.str9, %rdi
	movabsq	$2, %r8
	leaq	.L.str55, %r9
	movabsq	$3, %r10
	leaq	.L.str54, %r11
	movabsq	$59, %rbx
	leaq	.L.str46, %r14
	leaq	.L.str48, %r15
	leaq	.L.str53, %r12
	movl	$0, %ebp
	movl	$1, %r13d
	movq	%rcx, 2160(%rsp)        # 8-byte Spill
	movabsq	$19134, %rcx            # imm = 0x4ABE
	movq	%rcx, 2152(%rsp)        # 8-byte Spill
	movabsq	$32, %rcx
	movq	%rcx, 2144(%rsp)        # 8-byte Spill
	leaq	.L.str19, %rcx
	movq	%rcx, 2136(%rsp)        # 8-byte Spill
	leaq	.L.str13, %rcx
	movq	%rcx, 2128(%rsp)        # 8-byte Spill
	movabsq	$46, %rcx
	movl	%eax, 2124(%rsp)        # 4-byte Spill
	movl	%eax, %eax
                                        # kill: RAX<def> EAX<kill>
	movq	%rax, 2112(%rsp)        # 8-byte Spill
	movq	2152(%rsp), %rax        # 8-byte Reload
	movq	%rdi, 2104(%rsp)        # 8-byte Spill
	movq	%rax, %rdi
	movq	2144(%rsp), %rax        # 8-byte Reload
	movq	%rsi, 2096(%rsp)        # 8-byte Spill
	movq	%rax, %rsi
	movq	2112(%rsp), %rax        # 8-byte Reload
	movq	%rdx, 2088(%rsp)        # 8-byte Spill
	movq	%rax, %rdx
	movq	2160(%rsp), %rax        # 8-byte Reload
	movq	%rcx, 2080(%rsp)        # 8-byte Spill
	movq	%rax, %rcx
	movq	2136(%rsp), %rax        # 8-byte Reload
	movq	%r8, 2072(%rsp)         # 8-byte Spill
	movq	%rax, %r8
	movq	2096(%rsp), %rax        # 8-byte Reload
	movq	%r9, 2064(%rsp)         # 8-byte Spill
	movq	%rax, %r9
	movq	2104(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	movl	%r13d, 2060(%rsp)       # 4-byte Spill
	movq	%r15, 2048(%rsp)        # 8-byte Spill
	movq	%r12, 2040(%rsp)        # 8-byte Spill
	movl	%ebp, 2036(%rsp)        # 4-byte Spill
	movq	%r14, 2024(%rsp)        # 8-byte Spill
	movq	%rbx, 2016(%rsp)        # 8-byte Spill
	movq	%r10, 2008(%rsp)        # 8-byte Spill
	movq	%r11, 2000(%rsp)        # 8-byte Spill
	callq	trace_logger_log_int
.Ltmp82:
	#DEBUG_VALUE: main:err <- [RSP+2124]
	.loc	1 59 0                  # store_kernel.c:59:0
	movq	2016(%rsp), %rdi        # 8-byte Reload
	movq	2024(%rsp), %rsi        # 8-byte Reload
	movq	2048(%rsp), %rdx        # 8-byte Reload
	movq	2088(%rsp), %rcx        # 8-byte Reload
	movq	2080(%rsp), %r8         # 8-byte Reload
	movl	2036(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	2072(%rsp), %rdi        # 8-byte Reload
	movq	2144(%rsp), %rsi        # 8-byte Reload
	movq	2096(%rsp), %rdx        # 8-byte Reload
	movq	2096(%rsp), %rcx        # 8-byte Reload
	movq	2128(%rsp), %r8         # 8-byte Reload
	movq	2096(%rsp), %r9         # 8-byte Reload
	movq	2104(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	2124(%rsp), %ebp        # 4-byte Reload
	movl	%ebp, %r13d
	movl	%r13d, %edx
	movq	2160(%rsp), %rdi        # 8-byte Reload
	movq	2144(%rsp), %rsi        # 8-byte Reload
	movq	2160(%rsp), %rcx        # 8-byte Reload
	movq	2136(%rsp), %r8         # 8-byte Reload
	movq	2096(%rsp), %r9         # 8-byte Reload
	movq	2104(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	2124(%rsp), %ebp        # 4-byte Reload
	cmpl	$0, %ebp
	sete	%al
	movb	%al, %cl
	andb	$1, %cl
	movzbl	%cl, %r13d
	movl	%r13d, %edx
	movq	2152(%rsp), %rdi        # 8-byte Reload
	movq	2160(%rsp), %rsi        # 8-byte Reload
	movq	2160(%rsp), %rcx        # 8-byte Reload
	movq	2088(%rsp), %r8         # 8-byte Reload
	movq	2096(%rsp), %r9         # 8-byte Reload
	movq	2104(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	movb	%al, 1999(%rsp)         # 1-byte Spill
	callq	trace_logger_log_int
	movq	2016(%rsp), %rdi        # 8-byte Reload
	movq	2024(%rsp), %rsi        # 8-byte Reload
	movq	2048(%rsp), %rdx        # 8-byte Reload
	movq	2040(%rsp), %rcx        # 8-byte Reload
	movq	2072(%rsp), %r8         # 8-byte Reload
	movl	2036(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	2008(%rsp), %rdi        # 8-byte Reload
	movq	2096(%rsp), %rsi        # 8-byte Reload
	movq	2096(%rsp), %rdx        # 8-byte Reload
	movq	2160(%rsp), %rcx        # 8-byte Reload
	movq	2000(%rsp), %r8         # 8-byte Reload
	movq	2096(%rsp), %r9         # 8-byte Reload
	movq	2104(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	2072(%rsp), %rdi        # 8-byte Reload
	movq	2096(%rsp), %rsi        # 8-byte Reload
	movq	2096(%rsp), %rdx        # 8-byte Reload
	movq	2160(%rsp), %rcx        # 8-byte Reload
	movq	2064(%rsp), %r8         # 8-byte Reload
	movq	2096(%rsp), %r9         # 8-byte Reload
	movq	2104(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	1999(%rsp), %al         # 1-byte Reload
	andb	$1, %al
	movzbl	%al, %ebp
	movl	%ebp, %edx
	movq	2160(%rsp), %rdi        # 8-byte Reload
	movq	2160(%rsp), %rsi        # 8-byte Reload
	movq	2160(%rsp), %rcx        # 8-byte Reload
	movq	2088(%rsp), %r8         # 8-byte Reload
	movq	2096(%rsp), %r9         # 8-byte Reload
	movq	2104(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	1999(%rsp), %al         # 1-byte Reload
	testb	$1, %al
	jne	.LBB2_3
	jmp	.LBB2_4
.Ltmp83:
.LBB2_3:                                # %.preheader
	movabsq	$0, %rax
	movabsq	$1, %rcx
	leaq	.L.str37, %r8
	leaq	.L.str9, %rdx
	movabsq	$60, %rdi
	leaq	.L.str46, %rsi
	leaq	.L.str54, %r9
	leaq	.L.str56, %r10
	movabsq	$2, %r11
	movl	$0, %ebx
	movl	$1, %ebp
	movabsq	$19134, %r14            # imm = 0x4ABE
	movabsq	$64, %r15
	leaq	.L.str27, %r12
	leaq	.L.str17, %r13
	movq	%rax, 1984(%rsp)        # 8-byte Spill
	leaq	2952(%rsp), %rax
	movq	%rax, 1976(%rsp)        # 8-byte Spill
	movabsq	$62, %rax
	movq	%rax, 1968(%rsp)        # 8-byte Spill
	movabsq	$27, %rax
	movq	%rax, 1960(%rsp)        # 8-byte Spill
	leaq	.L.str26, %rax
	movq	%rax, 1952(%rsp)        # 8-byte Spill
	leaq	.L.str8, %rax
	movq	%rax, 1944(%rsp)        # 8-byte Spill
	leaq	2960(%rsp), %rax
	movq	%rax, 1936(%rsp)        # 8-byte Spill
	movabsq	$61, %rax
.Ltmp84:
	#DEBUG_VALUE: main:store_vals <- [RSP+1936]
	.loc	1 61 0                  # store_kernel.c:61:0
	movq	%rdi, 1928(%rsp)        # 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, 1920(%rsp)        # 8-byte Spill
	movq	%rdx, 1912(%rsp)        # 8-byte Spill
	movq	%r9, %rdx
	movq	1952(%rsp), %rax        # 8-byte Reload
	movq	%rcx, 1904(%rsp)        # 8-byte Spill
	movq	%rax, %rcx
	movq	1960(%rsp), %rax        # 8-byte Reload
	movq	%r8, 1896(%rsp)         # 8-byte Spill
	movq	%rax, %r8
	movq	%r9, 1888(%rsp)         # 8-byte Spill
	movl	%ebx, %r9d
	movl	$1, (%rsp)
	movq	%r13, 1880(%rsp)        # 8-byte Spill
	movl	%ebp, 1876(%rsp)        # 4-byte Spill
	movq	%r14, 1864(%rsp)        # 8-byte Spill
	movq	%r12, 1856(%rsp)        # 8-byte Spill
	movq	%r15, 1848(%rsp)        # 8-byte Spill
	movl	%ebx, 1844(%rsp)        # 4-byte Spill
	movq	%r10, 1832(%rsp)        # 8-byte Spill
	movq	%r11, 1824(%rsp)        # 8-byte Spill
	callq	trace_logger_log0
	movq	1904(%rsp), %rdi        # 8-byte Reload
	movq	1848(%rsp), %rsi        # 8-byte Reload
	movq	1936(%rsp), %rdx        # 8-byte Reload
	movq	1904(%rsp), %rcx        # 8-byte Reload
	movq	1944(%rsp), %r8         # 8-byte Reload
	movq	1984(%rsp), %r9         # 8-byte Reload
	movq	1912(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	2960(%rsp), %rax
	.loc	1 62 0                  # store_kernel.c:62:0
	movq	1864(%rsp), %rdi        # 8-byte Reload
	movq	1848(%rsp), %rsi        # 8-byte Reload
	movq	%rax, %rdx
	movq	1904(%rsp), %rcx        # 8-byte Reload
	movq	1952(%rsp), %r8         # 8-byte Reload
	movq	1984(%rsp), %r9         # 8-byte Reload
	movq	1912(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	movq	%rax, 1816(%rsp)        # 8-byte Spill
	callq	trace_logger_log_ptr
.Ltmp85:
	#DEBUG_VALUE: main:store_loc <- [RSP+1976]
	movq	1968(%rsp), %rdi        # 8-byte Reload
	movq	1920(%rsp), %rsi        # 8-byte Reload
	movq	1888(%rsp), %rdx        # 8-byte Reload
	movq	1856(%rsp), %rcx        # 8-byte Reload
	movq	1960(%rsp), %r8         # 8-byte Reload
	movl	1844(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1904(%rsp), %rdi        # 8-byte Reload
	movq	1848(%rsp), %rsi        # 8-byte Reload
	movq	1976(%rsp), %rdx        # 8-byte Reload
	movq	1904(%rsp), %rcx        # 8-byte Reload
	movq	1880(%rsp), %r8         # 8-byte Reload
	movq	1984(%rsp), %r9         # 8-byte Reload
	movq	1912(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	2952(%rsp), %rax
.Ltmp86:
	.loc	1 60 0                  # store_kernel.c:60:0
	movq	1864(%rsp), %rdi        # 8-byte Reload
	movq	1848(%rsp), %rsi        # 8-byte Reload
	movq	%rax, %rdx
	movq	1904(%rsp), %rcx        # 8-byte Reload
	movq	1856(%rsp), %r8         # 8-byte Reload
	movq	1984(%rsp), %r9         # 8-byte Reload
	movq	1912(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	movq	%rax, 1808(%rsp)        # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	1928(%rsp), %rdi        # 8-byte Reload
	movq	1920(%rsp), %rsi        # 8-byte Reload
	movq	1888(%rsp), %rdx        # 8-byte Reload
	movq	1832(%rsp), %rcx        # 8-byte Reload
	movq	1824(%rsp), %r8         # 8-byte Reload
	movl	1844(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1904(%rsp), %rdi        # 8-byte Reload
	movq	1984(%rsp), %rsi        # 8-byte Reload
	movq	1984(%rsp), %rdx        # 8-byte Reload
	movq	1904(%rsp), %rcx        # 8-byte Reload
	movq	1896(%rsp), %r8         # 8-byte Reload
	movq	1984(%rsp), %r9         # 8-byte Reload
	movq	1912(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	1984(%rsp), %rax        # 8-byte Reload
	movq	%rax, 1800(%rsp)        # 8-byte Spill
	jmp	.LBB2_5
.Ltmp87:
.LBB2_4:
	leaq	.L.str2, %rax
	leaq	.L.str3, %rcx
	movl	$59, %edx
	leaq	.L__PRETTY_FUNCTION__.main, %rsi
	movabsq	$4, %rdi
	movabsq	$64, %r8
	movabsq	$0, %r9
	leaq	.L.str13, %r10
	leaq	.L.str9, %r11
	movabsq	$3, %rbx
	movabsq	$32, %r14
	movabsq	$59, %r15
	movabsq	$2, %r12
	movabsq	$1, %r13
	movabsq	$5, %rbp
	movq	%rax, 1792(%rsp)        # 8-byte Spill
	leaq	__assert_fail, %rax
	movq	%rax, 1784(%rsp)        # 8-byte Spill
	leaq	.L.str51, %rax
	movq	%rax, 1776(%rsp)        # 8-byte Spill
	leaq	.L.str46, %rax
	movq	%rax, 1768(%rsp)        # 8-byte Spill
	leaq	.L.str55, %rax
	movq	%rax, 1760(%rsp)        # 8-byte Spill
	leaq	.L.str57, %rax
	movq	%rax, 1752(%rsp)        # 8-byte Spill
	movabsq	$49, %rax
	movl	%edx, 1748(%rsp)        # 4-byte Spill
	movl	$0, %edx
	movl	%edx, 1744(%rsp)        # 4-byte Spill
	movl	$1, %edx
	.loc	1 59 0                  # store_kernel.c:59:0
	movq	%rdi, 1736(%rsp)        # 8-byte Spill
	movq	%r15, %rdi
	movq	%rax, 1728(%rsp)        # 8-byte Spill
	movq	1768(%rsp), %rax        # 8-byte Reload
	movq	%rsi, 1720(%rsp)        # 8-byte Spill
	movq	%rax, %rsi
	movq	1760(%rsp), %rax        # 8-byte Reload
	movl	%edx, 1716(%rsp)        # 4-byte Spill
	movq	%rax, %rdx
	movq	1752(%rsp), %rax        # 8-byte Reload
	movq	%rcx, 1704(%rsp)        # 8-byte Spill
	movq	%rax, %rcx
	movq	1728(%rsp), %rax        # 8-byte Reload
	movq	%r8, 1696(%rsp)         # 8-byte Spill
	movq	%rax, %r8
	movl	1744(%rsp), %eax        # 4-byte Reload
	movq	%r9, 1688(%rsp)         # 8-byte Spill
	movl	%eax, %r9d
	movl	$1, (%rsp)
	movq	%r15, 1680(%rsp)        # 8-byte Spill
	movq	%r12, 1672(%rsp)        # 8-byte Spill
	movq	%r13, 1664(%rsp)        # 8-byte Spill
	movq	%rbp, 1656(%rsp)        # 8-byte Spill
	movq	%r14, 1648(%rsp)        # 8-byte Spill
	movq	%rbx, 1640(%rsp)        # 8-byte Spill
	movq	%r10, 1632(%rsp)        # 8-byte Spill
	movq	%r11, 1624(%rsp)        # 8-byte Spill
	callq	trace_logger_log0
	movq	1656(%rsp), %rdi        # 8-byte Reload
	movq	1696(%rsp), %rsi        # 8-byte Reload
	movq	1784(%rsp), %rdx        # 8-byte Reload
	movq	1664(%rsp), %rcx        # 8-byte Reload
	movq	1776(%rsp), %r8         # 8-byte Reload
	movq	1688(%rsp), %r9         # 8-byte Reload
	movq	1624(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	1664(%rsp), %rdi        # 8-byte Reload
	movq	1696(%rsp), %rsi        # 8-byte Reload
	movq	1792(%rsp), %rdx        # 8-byte Reload
	movq	1688(%rsp), %rcx        # 8-byte Reload
	movq	1632(%rsp), %r8         # 8-byte Reload
	movq	1688(%rsp), %r9         # 8-byte Reload
	movq	1624(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	1672(%rsp), %rdi        # 8-byte Reload
	movq	1696(%rsp), %rsi        # 8-byte Reload
	movq	1704(%rsp), %rdx        # 8-byte Reload
	movq	1688(%rsp), %rcx        # 8-byte Reload
	movq	1632(%rsp), %r8         # 8-byte Reload
	movq	1688(%rsp), %r9         # 8-byte Reload
	movq	1624(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	1640(%rsp), %rdi        # 8-byte Reload
	movq	1648(%rsp), %rsi        # 8-byte Reload
	movq	1680(%rsp), %rdx        # 8-byte Reload
	movq	1688(%rsp), %rcx        # 8-byte Reload
	movq	1632(%rsp), %r8         # 8-byte Reload
	movq	1688(%rsp), %r9         # 8-byte Reload
	movq	1624(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	1736(%rsp), %rdi        # 8-byte Reload
	movq	1696(%rsp), %rsi        # 8-byte Reload
	movq	1720(%rsp), %rdx        # 8-byte Reload
	movq	1688(%rsp), %rcx        # 8-byte Reload
	movq	1632(%rsp), %r8         # 8-byte Reload
	movq	1688(%rsp), %r9         # 8-byte Reload
	movq	1624(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	1792(%rsp), %rdi        # 8-byte Reload
	movq	1704(%rsp), %rsi        # 8-byte Reload
	movl	1748(%rsp), %edx        # 4-byte Reload
	movq	1720(%rsp), %rcx        # 8-byte Reload
	callq	__assert_fail
	movabsq	$59, %rdi
	leaq	.L.str46, %rsi
	leaq	.L.str55, %rdx
	leaq	.L.str58, %rcx
	movabsq	$7, %r8
	movl	$0, %r9d
	movl	$1, %eax
	movl	$1, (%rsp)
	movl	%eax, 1620(%rsp)        # 4-byte Spill
	callq	trace_logger_log0
.LBB2_5:                                # =>This Inner Loop Header: Depth=1
	movq	1800(%rsp), %rax        # 8-byte Reload
	movabsq	$1, %rcx
	leaq	.L.str30, %rdx
	movabsq	$0, %rsi
	leaq	.L.str9, %rdi
	movabsq	$2, %r8
	leaq	.L.str37, %r9
	movabsq	$3, %r10
	leaq	.L.str65, %r11
	movabsq	$60, %rbx
	leaq	.L.str46, %r14
	leaq	.L.str64, %r15
	movl	$0, %ebp
	movl	$1, %r12d
	movabsq	$19134, %r13            # imm = 0x4ABE
	movq	%rax, 1608(%rsp)        # 8-byte Spill
	movabsq	$64, %rax
	movq	%rax, 1600(%rsp)        # 8-byte Spill
	leaq	.L.str24, %rax
	movq	%rax, 1592(%rsp)        # 8-byte Spill
	movabsq	$1024, %rax             # imm = 0x400
	movq	%rax, 1584(%rsp)        # 8-byte Spill
	leaq	.L.str13, %rax
	movq	%rax, 1576(%rsp)        # 8-byte Spill
	movabsq	$46, %rax
	movq	%rax, 1568(%rsp)        # 8-byte Spill
	leaq	.L.str23, %rax
	movq	%rax, 1560(%rsp)        # 8-byte Spill
	movabsq	$8, %rax
	movq	%rax, 1552(%rsp)        # 8-byte Spill
	movabsq	$32, %rax
	movq	%rax, 1544(%rsp)        # 8-byte Spill
	movabsq	$4294967295, %rax       # imm = 0xFFFFFFFF
	movq	%rax, 1536(%rsp)        # 8-byte Spill
	leaq	.L.str62, %rax
	movq	%rax, 1528(%rsp)        # 8-byte Spill
	movabsq	$62, %rax
	movq	%rax, 1520(%rsp)        # 8-byte Spill
	leaq	.L.str63, %rax
	movq	%rax, 1512(%rsp)        # 8-byte Spill
	movabsq	$28, %rax
	movq	%rax, 1504(%rsp)        # 8-byte Spill
	leaq	.L.str27, %rax
	movq	%rax, 1496(%rsp)        # 8-byte Spill
	movabsq	$29, %rax
	movq	%rax, 1488(%rsp)        # 8-byte Spill
	leaq	.L.str60, %rax
	movq	%rax, 1480(%rsp)        # 8-byte Spill
	leaq	.L.str59, %rax
	movq	%rax, 1472(%rsp)        # 8-byte Spill
	movabsq	$61, %rax
	movq	%rax, 1464(%rsp)        # 8-byte Spill
	leaq	.L.str61, %rax
	movq	%rax, 1456(%rsp)        # 8-byte Spill
	movabsq	$33, %rax
	movq	%rax, 1448(%rsp)        # 8-byte Spill
	leaq	.L.str26, %rax
	movq	%rax, 1440(%rsp)        # 8-byte Spill
	leaq	.L.str54, %rax
	movq	%rax, 1432(%rsp)        # 8-byte Spill
	movabsq	$-1, %rax
	movq	%rax, 1424(%rsp)        # 8-byte Spill
	movabsq	$48, %rax
	.loc	1 61 0                  # store_kernel.c:61:0
.Ltmp88:
	movq	%rax, 1416(%rsp)        # 8-byte Spill
	movq	1424(%rsp), %rax        # 8-byte Reload
	movq	%rdi, 1408(%rsp)        # 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, 1400(%rsp)        # 8-byte Spill
	movq	%r14, %rsi
	movq	%rdx, 1392(%rsp)        # 8-byte Spill
	movq	%r9, %rdx
	movq	1560(%rsp), %rax        # 8-byte Reload
	movq	%rcx, 1384(%rsp)        # 8-byte Spill
	movq	%rax, %rcx
	movq	1416(%rsp), %rax        # 8-byte Reload
	movq	%r8, 1376(%rsp)         # 8-byte Spill
	movq	%rax, %r8
	movq	%r9, 1368(%rsp)         # 8-byte Spill
	movl	%ebp, %r9d
	movl	$1, (%rsp)
	movq	%r15, 1360(%rsp)        # 8-byte Spill
	movl	%ebp, 1356(%rsp)        # 4-byte Spill
	movl	%r12d, 1352(%rsp)       # 4-byte Spill
	movq	%r13, 1344(%rsp)        # 8-byte Spill
	movq	%r14, 1336(%rsp)        # 8-byte Spill
	movq	%rbx, 1328(%rsp)        # 8-byte Spill
	movq	%r10, 1320(%rsp)        # 8-byte Spill
	movq	%r11, 1312(%rsp)        # 8-byte Spill
	callq	trace_logger_log0
	movq	1376(%rsp), %rdi        # 8-byte Reload
	movq	1600(%rsp), %rsi        # 8-byte Reload
	movq	1400(%rsp), %rdx        # 8-byte Reload
	movq	1384(%rsp), %rcx        # 8-byte Reload
	movq	1592(%rsp), %r8         # 8-byte Reload
	movq	1384(%rsp), %r9         # 8-byte Reload
	movq	1368(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	1384(%rsp), %rdi        # 8-byte Reload
	movq	1600(%rsp), %rsi        # 8-byte Reload
	movq	1400(%rsp), %rdx        # 8-byte Reload
	movq	1400(%rsp), %rcx        # 8-byte Reload
	movq	1576(%rsp), %r8         # 8-byte Reload
	movq	1384(%rsp), %r9         # 8-byte Reload
	movq	1432(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	1344(%rsp), %rdi        # 8-byte Reload
	movq	1600(%rsp), %rsi        # 8-byte Reload
	movq	1608(%rsp), %rdx        # 8-byte Reload
	movq	1384(%rsp), %rcx        # 8-byte Reload
	movq	1560(%rsp), %r8         # 8-byte Reload
	movq	1400(%rsp), %r9         # 8-byte Reload
	movq	1432(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	1464(%rsp), %rdi        # 8-byte Reload
	movq	1336(%rsp), %rsi        # 8-byte Reload
	movq	1368(%rsp), %rdx        # 8-byte Reload
	movq	1472(%rsp), %rcx        # 8-byte Reload
	movq	1488(%rsp), %r8         # 8-byte Reload
	movl	1356(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1376(%rsp), %rdi        # 8-byte Reload
	movq	1600(%rsp), %rsi        # 8-byte Reload
	movq	1608(%rsp), %rdx        # 8-byte Reload
	movq	1384(%rsp), %rcx        # 8-byte Reload
	movq	1560(%rsp), %r8         # 8-byte Reload
	movq	1400(%rsp), %r9         # 8-byte Reload
	movq	1408(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	1384(%rsp), %rdi        # 8-byte Reload
	movq	1600(%rsp), %rsi        # 8-byte Reload
	movq	1816(%rsp), %rdx        # 8-byte Reload
	movq	1384(%rsp), %rcx        # 8-byte Reload
	movq	1440(%rsp), %r8         # 8-byte Reload
	movq	1400(%rsp), %r9         # 8-byte Reload
	movq	1408(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	1608(%rsp), %rax        # 8-byte Reload
	shlq	$2, %rax
	movq	1816(%rsp), %rcx        # 8-byte Reload
	addq	%rax, %rcx
	movq	1344(%rsp), %rdi        # 8-byte Reload
	movq	1600(%rsp), %rsi        # 8-byte Reload
	movq	%rcx, %rdx
	movq	1384(%rsp), %rax        # 8-byte Reload
	movq	%rcx, 1304(%rsp)        # 8-byte Spill
	movq	%rax, %rcx
	movq	1472(%rsp), %r8         # 8-byte Reload
	movq	1400(%rsp), %r9         # 8-byte Reload
	movq	1408(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	1464(%rsp), %rdi        # 8-byte Reload
	movq	1336(%rsp), %rsi        # 8-byte Reload
	movq	1368(%rsp), %rdx        # 8-byte Reload
	movq	1480(%rsp), %rcx        # 8-byte Reload
	movq	1448(%rsp), %r8         # 8-byte Reload
	movl	1356(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1384(%rsp), %rdi        # 8-byte Reload
	movq	1600(%rsp), %rsi        # 8-byte Reload
	movq	1608(%rsp), %rdx        # 8-byte Reload
	movq	1384(%rsp), %rcx        # 8-byte Reload
	movq	1560(%rsp), %r8         # 8-byte Reload
	movq	1400(%rsp), %r9         # 8-byte Reload
	movq	1408(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	1608(%rsp), %rax        # 8-byte Reload
	movl	%eax, %ebp
	movl	%ebp, %r12d
	movl	%r12d, %edx
	movq	1344(%rsp), %rdi        # 8-byte Reload
	movq	1544(%rsp), %rsi        # 8-byte Reload
	movq	1384(%rsp), %rcx        # 8-byte Reload
	movq	1480(%rsp), %r8         # 8-byte Reload
	movq	1400(%rsp), %r9         # 8-byte Reload
	movq	1408(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	movl	%ebp, 1300(%rsp)        # 4-byte Spill
	callq	trace_logger_log_int
	movq	1464(%rsp), %rdi        # 8-byte Reload
	movq	1336(%rsp), %rsi        # 8-byte Reload
	movq	1368(%rsp), %rdx        # 8-byte Reload
	movq	1456(%rsp), %rcx        # 8-byte Reload
	movq	1504(%rsp), %r8         # 8-byte Reload
	movl	1356(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1376(%rsp), %rdi        # 8-byte Reload
	movq	1600(%rsp), %rsi        # 8-byte Reload
	movq	1304(%rsp), %rdx        # 8-byte Reload
	movq	1384(%rsp), %rcx        # 8-byte Reload
	movq	1472(%rsp), %r8         # 8-byte Reload
	movq	1400(%rsp), %r9         # 8-byte Reload
	movq	1408(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movl	1300(%rsp), %ebp        # 4-byte Reload
	movl	%ebp, %r12d
	movl	%r12d, %edx
	movq	1384(%rsp), %rdi        # 8-byte Reload
	movq	1544(%rsp), %rsi        # 8-byte Reload
	movq	1384(%rsp), %rcx        # 8-byte Reload
	movq	1480(%rsp), %r8         # 8-byte Reload
	movq	1400(%rsp), %r9         # 8-byte Reload
	movq	1408(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	1816(%rsp), %rax        # 8-byte Reload
	movq	1608(%rsp), %rcx        # 8-byte Reload
	movl	1300(%rsp), %ebp        # 4-byte Reload
	movl	%ebp, (%rax,%rcx,4)
	.loc	1 62 0                  # store_kernel.c:62:0
	movq	1520(%rsp), %rdi        # 8-byte Reload
	movq	1336(%rsp), %rsi        # 8-byte Reload
	movq	1368(%rsp), %rdx        # 8-byte Reload
	movq	1528(%rsp), %rcx        # 8-byte Reload
	movq	1488(%rsp), %r8         # 8-byte Reload
	movl	1356(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1376(%rsp), %rdi        # 8-byte Reload
	movq	1600(%rsp), %rsi        # 8-byte Reload
	movq	1608(%rsp), %rdx        # 8-byte Reload
	movq	1384(%rsp), %rcx        # 8-byte Reload
	movq	1560(%rsp), %r8         # 8-byte Reload
	movq	1400(%rsp), %r9         # 8-byte Reload
	movq	1408(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	1384(%rsp), %rdi        # 8-byte Reload
	movq	1600(%rsp), %rsi        # 8-byte Reload
	movq	1808(%rsp), %rdx        # 8-byte Reload
	movq	1384(%rsp), %rcx        # 8-byte Reload
	movq	1496(%rsp), %r8         # 8-byte Reload
	movq	1400(%rsp), %r9         # 8-byte Reload
	movq	1408(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	1608(%rsp), %rax        # 8-byte Reload
	shlq	$2, %rax
	movq	1808(%rsp), %rcx        # 8-byte Reload
	addq	%rax, %rcx
	movq	1344(%rsp), %rdi        # 8-byte Reload
	movq	1600(%rsp), %rsi        # 8-byte Reload
	movq	%rcx, %rdx
	movq	1384(%rsp), %rax        # 8-byte Reload
	movq	%rcx, 1288(%rsp)        # 8-byte Spill
	movq	%rax, %rcx
	movq	1528(%rsp), %r8         # 8-byte Reload
	movq	1400(%rsp), %r9         # 8-byte Reload
	movq	1408(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	1520(%rsp), %rdi        # 8-byte Reload
	movq	1336(%rsp), %rsi        # 8-byte Reload
	movq	1368(%rsp), %rdx        # 8-byte Reload
	movq	1512(%rsp), %rcx        # 8-byte Reload
	movq	1504(%rsp), %r8         # 8-byte Reload
	movl	1356(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1376(%rsp), %rdi        # 8-byte Reload
	movq	1600(%rsp), %rsi        # 8-byte Reload
	movq	1288(%rsp), %rdx        # 8-byte Reload
	movq	1384(%rsp), %rcx        # 8-byte Reload
	movq	1528(%rsp), %r8         # 8-byte Reload
	movq	1400(%rsp), %r9         # 8-byte Reload
	movq	1408(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	1384(%rsp), %rdi        # 8-byte Reload
	movq	1544(%rsp), %rsi        # 8-byte Reload
	movq	1536(%rsp), %rdx        # 8-byte Reload
	movq	1400(%rsp), %rcx        # 8-byte Reload
	movq	1576(%rsp), %r8         # 8-byte Reload
	movq	1400(%rsp), %r9         # 8-byte Reload
	movq	1408(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	1808(%rsp), %rax        # 8-byte Reload
	movq	1608(%rsp), %rcx        # 8-byte Reload
	movl	$-1, (%rax,%rcx,4)
.Ltmp89:
	.loc	1 60 0                  # store_kernel.c:60:0
	movq	1328(%rsp), %rdi        # 8-byte Reload
	movq	1336(%rsp), %rsi        # 8-byte Reload
	movq	1368(%rsp), %rdx        # 8-byte Reload
	movq	1592(%rsp), %rcx        # 8-byte Reload
	movq	1552(%rsp), %r8         # 8-byte Reload
	movl	1356(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1376(%rsp), %rdi        # 8-byte Reload
	movq	1600(%rsp), %rsi        # 8-byte Reload
	movq	1384(%rsp), %rdx        # 8-byte Reload
	movq	1400(%rsp), %rcx        # 8-byte Reload
	movq	1576(%rsp), %r8         # 8-byte Reload
	movq	1400(%rsp), %r9         # 8-byte Reload
	movq	1408(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	1384(%rsp), %rdi        # 8-byte Reload
	movq	1600(%rsp), %rsi        # 8-byte Reload
	movq	1608(%rsp), %rdx        # 8-byte Reload
	movq	1384(%rsp), %rcx        # 8-byte Reload
	movq	1560(%rsp), %r8         # 8-byte Reload
	movq	1400(%rsp), %r9         # 8-byte Reload
	movq	1408(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	1608(%rsp), %rax        # 8-byte Reload
	addq	$1, %rax
	movq	1344(%rsp), %rdi        # 8-byte Reload
	movq	1600(%rsp), %rsi        # 8-byte Reload
	movq	%rax, %rdx
	movq	1384(%rsp), %rcx        # 8-byte Reload
	movq	1592(%rsp), %r8         # 8-byte Reload
	movq	1400(%rsp), %r9         # 8-byte Reload
	movq	1408(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	movq	%rax, 1280(%rsp)        # 8-byte Spill
	callq	trace_logger_log_int
	movq	1328(%rsp), %rdi        # 8-byte Reload
	movq	1336(%rsp), %rsi        # 8-byte Reload
	movq	1368(%rsp), %rdx        # 8-byte Reload
	movq	1392(%rsp), %rcx        # 8-byte Reload
	movq	1568(%rsp), %r8         # 8-byte Reload
	movl	1356(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1376(%rsp), %rdi        # 8-byte Reload
	movq	1600(%rsp), %rsi        # 8-byte Reload
	movq	1584(%rsp), %rdx        # 8-byte Reload
	movq	1400(%rsp), %rcx        # 8-byte Reload
	movq	1576(%rsp), %r8         # 8-byte Reload
	movq	1400(%rsp), %r9         # 8-byte Reload
	movq	1408(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	1384(%rsp), %rdi        # 8-byte Reload
	movq	1600(%rsp), %rsi        # 8-byte Reload
	movq	1280(%rsp), %rdx        # 8-byte Reload
	movq	1384(%rsp), %rcx        # 8-byte Reload
	movq	1592(%rsp), %r8         # 8-byte Reload
	movq	1400(%rsp), %r9         # 8-byte Reload
	movq	1408(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	1280(%rsp), %rax        # 8-byte Reload
	cmpq	$1024, %rax             # imm = 0x400
	sete	%al
	movb	%al, %cl
	andb	$1, %cl
	movzbl	%cl, %ebp
	movl	%ebp, %edx
	movq	1344(%rsp), %rdi        # 8-byte Reload
	movq	1384(%rsp), %rsi        # 8-byte Reload
	movq	1384(%rsp), %rcx        # 8-byte Reload
	movq	1392(%rsp), %r8         # 8-byte Reload
	movq	1400(%rsp), %r9         # 8-byte Reload
	movq	1408(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	movb	%al, 1279(%rsp)         # 1-byte Spill
	callq	trace_logger_log_int
	movq	1328(%rsp), %rdi        # 8-byte Reload
	movq	1336(%rsp), %rsi        # 8-byte Reload
	movq	1368(%rsp), %rdx        # 8-byte Reload
	movq	1360(%rsp), %rcx        # 8-byte Reload
	movq	1376(%rsp), %r8         # 8-byte Reload
	movl	1356(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1320(%rsp), %rdi        # 8-byte Reload
	movq	1400(%rsp), %rsi        # 8-byte Reload
	movq	1400(%rsp), %rdx        # 8-byte Reload
	movq	1384(%rsp), %rcx        # 8-byte Reload
	movq	1312(%rsp), %r8         # 8-byte Reload
	movq	1400(%rsp), %r9         # 8-byte Reload
	movq	1408(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	1376(%rsp), %rdi        # 8-byte Reload
	movq	1400(%rsp), %rsi        # 8-byte Reload
	movq	1400(%rsp), %rdx        # 8-byte Reload
	movq	1384(%rsp), %rcx        # 8-byte Reload
	movq	1368(%rsp), %r8         # 8-byte Reload
	movq	1400(%rsp), %r9         # 8-byte Reload
	movq	1408(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	1279(%rsp), %al         # 1-byte Reload
	andb	$1, %al
	movzbl	%al, %ebp
	movl	%ebp, %edx
	movq	1384(%rsp), %rdi        # 8-byte Reload
	movq	1384(%rsp), %rsi        # 8-byte Reload
	movq	1384(%rsp), %rcx        # 8-byte Reload
	movq	1392(%rsp), %r8         # 8-byte Reload
	movq	1400(%rsp), %r9         # 8-byte Reload
	movq	1408(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	1279(%rsp), %al         # 1-byte Reload
	testb	$1, %al
	movq	1280(%rsp), %rcx        # 8-byte Reload
	movq	%rcx, 1800(%rsp)        # 8-byte Spill
	jne	.LBB2_6
	jmp	.LBB2_5
.Ltmp90:
.LBB2_6:
	movl	$1024, %edx             # imm = 0x400
	movabsq	$3, %rdi
	movabsq	$32, %rsi
	movabsq	$1024, %rax             # imm = 0x400
	movabsq	$0, %rcx
	leaq	.L.str13, %r8
	leaq	.L.str9, %r9
	movabsq	$2, %r10
	movabsq	$64, %r11
	movabsq	$1, %rbx
	leaq	.L.str67, %r14
	leaq	.L.str66, %r15
	movabsq	$4, %r12
	leaq	store_kernel, %r13
	leaq	.L.str1, %rbp
	movq	%rax, 1264(%rsp)        # 8-byte Spill
	movabsq	$77, %rax
	movq	%rax, 1256(%rsp)        # 8-byte Spill
	leaq	.L.str46, %rax
	movq	%rax, 1248(%rsp)        # 8-byte Spill
	leaq	.L.str65, %rax
	movq	%rax, 1240(%rsp)        # 8-byte Spill
	leaq	.L.str68, %rax
	movq	%rax, 1232(%rsp)        # 8-byte Spill
	movabsq	$49, %rax
	movl	%edx, 1228(%rsp)        # 4-byte Spill
	movl	$0, %edx
	movl	%edx, 1224(%rsp)        # 4-byte Spill
	movl	$1, %edx
	movq	%rax, 1216(%rsp)        # 8-byte Spill
	movabsq	$19134, %rax            # imm = 0x4ABE
	movq	%rax, 1208(%rsp)        # 8-byte Spill
	leaq	.L.str17, %rax
	movq	%rax, 1200(%rsp)        # 8-byte Spill
	leaq	2952(%rsp), %rax
	movq	%rax, 1192(%rsp)        # 8-byte Spill
	movabsq	$27, %rax
	movq	%rax, 1184(%rsp)        # 8-byte Spill
	leaq	.L.str8, %rax
	movq	%rax, 1176(%rsp)        # 8-byte Spill
	leaq	2960(%rsp), %rax
.Ltmp91:
	#DEBUG_VALUE: main:store_vals <- RAX
	.loc	1 77 0                  # store_kernel.c:77:0
	movq	%rax, 1168(%rsp)        # 8-byte Spill
.Ltmp92:
	#DEBUG_VALUE: main:store_vals <- [RSP+1168]
	movq	1256(%rsp), %rax        # 8-byte Reload
	movq	%rdi, 1160(%rsp)        # 8-byte Spill
	movq	%rax, %rdi
	movq	1248(%rsp), %rax        # 8-byte Reload
	movq	%rsi, 1152(%rsp)        # 8-byte Spill
	movq	%rax, %rsi
	movq	1240(%rsp), %rax        # 8-byte Reload
	movl	%edx, 1148(%rsp)        # 4-byte Spill
	movq	%rax, %rdx
	movq	%rcx, 1136(%rsp)        # 8-byte Spill
	movq	%r15, %rcx
	movq	1184(%rsp), %rax        # 8-byte Reload
	movq	%r8, 1128(%rsp)         # 8-byte Spill
	movq	%rax, %r8
	movl	1224(%rsp), %eax        # 4-byte Reload
	movq	%r9, 1120(%rsp)         # 8-byte Spill
	movl	%eax, %r9d
	movl	$1, (%rsp)
	movq	%r12, 1112(%rsp)        # 8-byte Spill
	movq	%rbp, 1104(%rsp)        # 8-byte Spill
	movq	%r13, 1096(%rsp)        # 8-byte Spill
	movq	%r15, 1088(%rsp)        # 8-byte Spill
	movq	%r14, 1080(%rsp)        # 8-byte Spill
	movq	%rbx, 1072(%rsp)        # 8-byte Spill
	movq	%r10, 1064(%rsp)        # 8-byte Spill
	movq	%r11, 1056(%rsp)        # 8-byte Spill
	callq	trace_logger_log0
	movq	1072(%rsp), %rdi        # 8-byte Reload
	movq	1056(%rsp), %rsi        # 8-byte Reload
	movq	1168(%rsp), %rdx        # 8-byte Reload
	movq	1072(%rsp), %rcx        # 8-byte Reload
	movq	1176(%rsp), %r8         # 8-byte Reload
	movq	1136(%rsp), %r9         # 8-byte Reload
	movq	1120(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	2960(%rsp), %rcx
	movq	1208(%rsp), %rdi        # 8-byte Reload
	movq	1056(%rsp), %rsi        # 8-byte Reload
	movq	%rcx, %rdx
	movq	1072(%rsp), %r8         # 8-byte Reload
	movq	%rcx, 1048(%rsp)        # 8-byte Spill
	movq	%r8, %rcx
	movq	1088(%rsp), %r8         # 8-byte Reload
	movq	1136(%rsp), %r9         # 8-byte Reload
	movq	1120(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
.Ltmp93:
	#DEBUG_VALUE: main:store_loc <- [RSP+1192]
	movq	1256(%rsp), %rdi        # 8-byte Reload
	movq	1248(%rsp), %rsi        # 8-byte Reload
	movq	1240(%rsp), %rdx        # 8-byte Reload
	movq	1080(%rsp), %rcx        # 8-byte Reload
	movq	1184(%rsp), %r8         # 8-byte Reload
	movl	1224(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1072(%rsp), %rdi        # 8-byte Reload
	movq	1056(%rsp), %rsi        # 8-byte Reload
	movq	1192(%rsp), %rdx        # 8-byte Reload
	movq	1072(%rsp), %rcx        # 8-byte Reload
	movq	1200(%rsp), %r8         # 8-byte Reload
	movq	1136(%rsp), %r9         # 8-byte Reload
	movq	1120(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	2952(%rsp), %rcx
	movq	1208(%rsp), %rdi        # 8-byte Reload
	movq	1056(%rsp), %rsi        # 8-byte Reload
	movq	%rcx, %rdx
	movq	1072(%rsp), %r8         # 8-byte Reload
	movq	%rcx, 1040(%rsp)        # 8-byte Spill
	movq	%r8, %rcx
	movq	1080(%rsp), %r8         # 8-byte Reload
	movq	1136(%rsp), %r9         # 8-byte Reload
	movq	1120(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	1256(%rsp), %rdi        # 8-byte Reload
	movq	1248(%rsp), %rsi        # 8-byte Reload
	movq	1240(%rsp), %rdx        # 8-byte Reload
	movq	1232(%rsp), %rcx        # 8-byte Reload
	movq	1216(%rsp), %r8         # 8-byte Reload
	movl	1224(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1112(%rsp), %rdi        # 8-byte Reload
	movq	1056(%rsp), %rsi        # 8-byte Reload
	movq	1096(%rsp), %rdx        # 8-byte Reload
	movq	1072(%rsp), %rcx        # 8-byte Reload
	movq	1104(%rsp), %r8         # 8-byte Reload
	movq	1136(%rsp), %r9         # 8-byte Reload
	movq	1120(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	1072(%rsp), %rdi        # 8-byte Reload
	movq	1056(%rsp), %rsi        # 8-byte Reload
	movq	1048(%rsp), %rdx        # 8-byte Reload
	movq	1072(%rsp), %rcx        # 8-byte Reload
	movq	1088(%rsp), %r8         # 8-byte Reload
	movq	1136(%rsp), %r9         # 8-byte Reload
	movq	1120(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	1064(%rsp), %rdi        # 8-byte Reload
	movq	1056(%rsp), %rsi        # 8-byte Reload
	movq	1040(%rsp), %rdx        # 8-byte Reload
	movq	1072(%rsp), %rcx        # 8-byte Reload
	movq	1080(%rsp), %r8         # 8-byte Reload
	movq	1136(%rsp), %r9         # 8-byte Reload
	movq	1120(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	1160(%rsp), %rdi        # 8-byte Reload
	movq	1152(%rsp), %rsi        # 8-byte Reload
	movq	1264(%rsp), %rdx        # 8-byte Reload
	movq	1136(%rsp), %rcx        # 8-byte Reload
	movq	1128(%rsp), %r8         # 8-byte Reload
	movq	1136(%rsp), %r9         # 8-byte Reload
	movq	1120(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	1048(%rsp), %rdi        # 8-byte Reload
	movq	1040(%rsp), %rsi        # 8-byte Reload
	movl	1228(%rsp), %edx        # 4-byte Reload
	callq	store_kernel
	movl	$1024, %edx             # imm = 0x400
	movabsq	$3, %rdi
	movabsq	$32, %rsi
	movabsq	$1024, %rcx             # imm = 0x400
	movabsq	$0, %r8
	leaq	.L.str13, %r9
	leaq	.L.str9, %r10
	movabsq	$2, %r11
	movabsq	$64, %rbx
	movabsq	$1, %r14
	leaq	.L.str70, %r15
	leaq	.L.str69, %r12
	movabsq	$4, %r13
	leaq	test_stores, %rbp
	leaq	.L.str35, %rax
	movq	%rax, 1032(%rsp)        # 8-byte Spill
	movabsq	$81, %rax
	movq	%rax, 1024(%rsp)        # 8-byte Spill
	leaq	.L.str46, %rax
	movq	%rax, 1016(%rsp)        # 8-byte Spill
	leaq	.L.str65, %rax
	movq	%rax, 1008(%rsp)        # 8-byte Spill
	leaq	.L.str71, %rax
	movq	%rax, 1000(%rsp)        # 8-byte Spill
	movabsq	$49, %rax
	movl	%edx, 996(%rsp)         # 4-byte Spill
	movl	$0, %edx
	movl	%edx, 992(%rsp)         # 4-byte Spill
	movl	$1, %edx
	movq	%rax, 984(%rsp)         # 8-byte Spill
	movabsq	$19134, %rax            # imm = 0x4ABE
	movq	%rax, 976(%rsp)         # 8-byte Spill
	leaq	.L.str17, %rax
	movq	%rax, 968(%rsp)         # 8-byte Spill
	leaq	2952(%rsp), %rax
	movq	%rax, 960(%rsp)         # 8-byte Spill
	movabsq	$27, %rax
	movq	%rax, 952(%rsp)         # 8-byte Spill
	leaq	.L.str8, %rax
	movq	%rax, 944(%rsp)         # 8-byte Spill
	leaq	2960(%rsp), %rax
.Ltmp94:
	#DEBUG_VALUE: main:store_vals <- RAX
	.loc	1 81 0                  # store_kernel.c:81:0
	movq	%rax, 936(%rsp)         # 8-byte Spill
.Ltmp95:
	#DEBUG_VALUE: main:store_vals <- [RSP+936]
	movq	1024(%rsp), %rax        # 8-byte Reload
	movq	%rdi, 928(%rsp)         # 8-byte Spill
	movq	%rax, %rdi
	movq	1016(%rsp), %rax        # 8-byte Reload
	movq	%rsi, 920(%rsp)         # 8-byte Spill
	movq	%rax, %rsi
	movq	1008(%rsp), %rax        # 8-byte Reload
	movl	%edx, 916(%rsp)         # 4-byte Spill
	movq	%rax, %rdx
	movq	%rcx, 904(%rsp)         # 8-byte Spill
	movq	%r12, %rcx
	movq	952(%rsp), %rax         # 8-byte Reload
	movq	%r8, 896(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movl	992(%rsp), %eax         # 4-byte Reload
	movq	%r9, 888(%rsp)          # 8-byte Spill
	movl	%eax, %r9d
	movl	$1, (%rsp)
	movq	%r13, 880(%rsp)         # 8-byte Spill
	movq	%rbp, 872(%rsp)         # 8-byte Spill
	movq	%r12, 864(%rsp)         # 8-byte Spill
	movq	%r15, 856(%rsp)         # 8-byte Spill
	movq	%r14, 848(%rsp)         # 8-byte Spill
	movq	%r10, 840(%rsp)         # 8-byte Spill
	movq	%r11, 832(%rsp)         # 8-byte Spill
	movq	%rbx, 824(%rsp)         # 8-byte Spill
	callq	trace_logger_log0
	movq	848(%rsp), %rdi         # 8-byte Reload
	movq	824(%rsp), %rsi         # 8-byte Reload
	movq	936(%rsp), %rdx         # 8-byte Reload
	movq	848(%rsp), %rcx         # 8-byte Reload
	movq	944(%rsp), %r8          # 8-byte Reload
	movq	896(%rsp), %r9          # 8-byte Reload
	movq	840(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	2960(%rsp), %rcx
	movq	976(%rsp), %rdi         # 8-byte Reload
	movq	824(%rsp), %rsi         # 8-byte Reload
	movq	%rcx, %rdx
	movq	848(%rsp), %r8          # 8-byte Reload
	movq	%rcx, 816(%rsp)         # 8-byte Spill
	movq	%r8, %rcx
	movq	864(%rsp), %r8          # 8-byte Reload
	movq	896(%rsp), %r9          # 8-byte Reload
	movq	840(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
.Ltmp96:
	#DEBUG_VALUE: main:store_loc <- [RSP+960]
	movq	1024(%rsp), %rdi        # 8-byte Reload
	movq	1016(%rsp), %rsi        # 8-byte Reload
	movq	1008(%rsp), %rdx        # 8-byte Reload
	movq	856(%rsp), %rcx         # 8-byte Reload
	movq	952(%rsp), %r8          # 8-byte Reload
	movl	992(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	848(%rsp), %rdi         # 8-byte Reload
	movq	824(%rsp), %rsi         # 8-byte Reload
	movq	960(%rsp), %rdx         # 8-byte Reload
	movq	848(%rsp), %rcx         # 8-byte Reload
	movq	968(%rsp), %r8          # 8-byte Reload
	movq	896(%rsp), %r9          # 8-byte Reload
	movq	840(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	2952(%rsp), %rcx
	movq	976(%rsp), %rdi         # 8-byte Reload
	movq	824(%rsp), %rsi         # 8-byte Reload
	movq	%rcx, %rdx
	movq	848(%rsp), %r8          # 8-byte Reload
	movq	%rcx, 808(%rsp)         # 8-byte Spill
	movq	%r8, %rcx
	movq	856(%rsp), %r8          # 8-byte Reload
	movq	896(%rsp), %r9          # 8-byte Reload
	movq	840(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	1024(%rsp), %rdi        # 8-byte Reload
	movq	1016(%rsp), %rsi        # 8-byte Reload
	movq	1008(%rsp), %rdx        # 8-byte Reload
	movq	1000(%rsp), %rcx        # 8-byte Reload
	movq	984(%rsp), %r8          # 8-byte Reload
	movl	992(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	880(%rsp), %rdi         # 8-byte Reload
	movq	824(%rsp), %rsi         # 8-byte Reload
	movq	872(%rsp), %rdx         # 8-byte Reload
	movq	848(%rsp), %rcx         # 8-byte Reload
	movq	1032(%rsp), %r8         # 8-byte Reload
	movq	896(%rsp), %r9          # 8-byte Reload
	movq	840(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	848(%rsp), %rdi         # 8-byte Reload
	movq	824(%rsp), %rsi         # 8-byte Reload
	movq	816(%rsp), %rdx         # 8-byte Reload
	movq	848(%rsp), %rcx         # 8-byte Reload
	movq	864(%rsp), %r8          # 8-byte Reload
	movq	896(%rsp), %r9          # 8-byte Reload
	movq	840(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	832(%rsp), %rdi         # 8-byte Reload
	movq	824(%rsp), %rsi         # 8-byte Reload
	movq	808(%rsp), %rdx         # 8-byte Reload
	movq	848(%rsp), %rcx         # 8-byte Reload
	movq	856(%rsp), %r8          # 8-byte Reload
	movq	896(%rsp), %r9          # 8-byte Reload
	movq	840(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	928(%rsp), %rdi         # 8-byte Reload
	movq	920(%rsp), %rsi         # 8-byte Reload
	movq	904(%rsp), %rdx         # 8-byte Reload
	movq	896(%rsp), %rcx         # 8-byte Reload
	movq	888(%rsp), %r8          # 8-byte Reload
	movq	896(%rsp), %r9          # 8-byte Reload
	movq	840(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	816(%rsp), %rdi         # 8-byte Reload
	movq	808(%rsp), %rsi         # 8-byte Reload
	movl	996(%rsp), %edx         # 4-byte Reload
	callq	test_stores
	movabsq	$1, %rcx
	leaq	.L.str72, %rsi
	movabsq	$0, %rdi
	leaq	.L.str9, %r8
	movabsq	$2, %r9
	leaq	.L.str76, %r10
	movabsq	$3, %r11
	leaq	.L.str75, %rbx
	movabsq	$82, %r14
	leaq	.L.str46, %r15
	leaq	.L.str65, %r12
	leaq	.L.str74, %r13
	movl	$0, %edx
	movl	$1, %ebp
	movq	%rcx, 800(%rsp)         # 8-byte Spill
	movabsq	$19134, %rcx            # imm = 0x4ABE
	movq	%rcx, 792(%rsp)         # 8-byte Spill
	movabsq	$64, %rcx
	movq	%rcx, 784(%rsp)         # 8-byte Spill
	leaq	.L.str73, %rcx
	movq	%rcx, 776(%rsp)         # 8-byte Spill
	leaq	stdout, %rcx
	movq	%rcx, 768(%rsp)         # 8-byte Spill
	leaq	.L.str41, %rcx
	movq	%rcx, 760(%rsp)         # 8-byte Spill
	movabsq	$83, %rcx
	movq	%rcx, 752(%rsp)         # 8-byte Spill
	movabsq	$27, %rcx
	movq	%rcx, 744(%rsp)         # 8-byte Spill
	movabsq	$32, %rcx
	movq	%rcx, 736(%rsp)         # 8-byte Spill
	leaq	.L.str71, %rcx
	movq	%rcx, 728(%rsp)         # 8-byte Spill
	leaq	.L.str13, %rcx
	movq	%rcx, 720(%rsp)         # 8-byte Spill
	movabsq	$46, %rcx
	movl	%eax, 716(%rsp)         # 4-byte Spill
	movl	%eax, %eax
                                        # kill: RAX<def> EAX<kill>
	movq	%rax, 704(%rsp)         # 8-byte Spill
	movq	792(%rsp), %rax         # 8-byte Reload
	movq	%rdi, 696(%rsp)         # 8-byte Spill
	movq	%rax, %rdi
	movq	736(%rsp), %rax         # 8-byte Reload
	movq	%rsi, 688(%rsp)         # 8-byte Spill
	movq	%rax, %rsi
	movq	704(%rsp), %rax         # 8-byte Reload
	movl	%edx, 684(%rsp)         # 4-byte Spill
	movq	%rax, %rdx
	movq	800(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 672(%rsp)         # 8-byte Spill
	movq	%rax, %rcx
	movq	728(%rsp), %rax         # 8-byte Reload
	movq	%r8, 664(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movq	696(%rsp), %rax         # 8-byte Reload
	movq	%r9, 656(%rsp)          # 8-byte Spill
	movq	%rax, %r9
	movq	664(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	movl	%ebp, 652(%rsp)         # 4-byte Spill
	movq	%r12, 640(%rsp)         # 8-byte Spill
	movq	%r13, 632(%rsp)         # 8-byte Spill
	movq	%r15, 624(%rsp)         # 8-byte Spill
	movq	%r14, 616(%rsp)         # 8-byte Spill
	movq	%r10, 608(%rsp)         # 8-byte Spill
	movq	%r11, 600(%rsp)         # 8-byte Spill
	movq	%rbx, 592(%rsp)         # 8-byte Spill
	callq	trace_logger_log_int
.Ltmp97:
	#DEBUG_VALUE: main:num_failures <- [RSP+716]
	.loc	1 82 0                  # store_kernel.c:82:0
	movq	616(%rsp), %rdi         # 8-byte Reload
	movq	624(%rsp), %rsi         # 8-byte Reload
	movq	640(%rsp), %rdx         # 8-byte Reload
	movq	688(%rsp), %rcx         # 8-byte Reload
	movq	672(%rsp), %r8          # 8-byte Reload
	movl	684(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	656(%rsp), %rdi         # 8-byte Reload
	movq	736(%rsp), %rsi         # 8-byte Reload
	movq	696(%rsp), %rdx         # 8-byte Reload
	movq	696(%rsp), %rcx         # 8-byte Reload
	movq	720(%rsp), %r8          # 8-byte Reload
	movq	696(%rsp), %r9          # 8-byte Reload
	movq	664(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	716(%rsp), %ebp         # 4-byte Reload
	movl	%ebp, %esi
	movl	%esi, %edx
	movq	800(%rsp), %rdi         # 8-byte Reload
	movq	736(%rsp), %rsi         # 8-byte Reload
	movq	800(%rsp), %rcx         # 8-byte Reload
	movq	728(%rsp), %r8          # 8-byte Reload
	movq	696(%rsp), %r9          # 8-byte Reload
	movq	664(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	716(%rsp), %ebp         # 4-byte Reload
	cmpl	$0, %ebp
	sete	%al
	.loc	1 83 0                  # store_kernel.c:83:0
.Ltmp98:
	movb	%al, %cl
	andb	$1, %cl
	movzbl	%cl, %ecx
	movl	%ecx, %edx
	movq	792(%rsp), %rdi         # 8-byte Reload
	movq	800(%rsp), %rsi         # 8-byte Reload
	movq	800(%rsp), %rcx         # 8-byte Reload
	movq	688(%rsp), %r8          # 8-byte Reload
	movq	696(%rsp), %r9          # 8-byte Reload
	movq	664(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movb	%al, 591(%rsp)          # 1-byte Spill
	callq	trace_logger_log_int
	movq	752(%rsp), %rdi         # 8-byte Reload
	movq	624(%rsp), %rsi         # 8-byte Reload
	movq	640(%rsp), %rdx         # 8-byte Reload
	movq	776(%rsp), %rcx         # 8-byte Reload
	movq	744(%rsp), %r8          # 8-byte Reload
	movl	684(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	800(%rsp), %rdi         # 8-byte Reload
	movq	784(%rsp), %rsi         # 8-byte Reload
	movq	768(%rsp), %rdx         # 8-byte Reload
	movq	800(%rsp), %rcx         # 8-byte Reload
	movq	760(%rsp), %r8          # 8-byte Reload
	movq	696(%rsp), %r9          # 8-byte Reload
	movq	664(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	stdout, %rcx
.Ltmp99:
	.loc	1 82 0                  # store_kernel.c:82:0
	movq	792(%rsp), %rdi         # 8-byte Reload
	movq	784(%rsp), %rsi         # 8-byte Reload
	movq	%rcx, %rdx
	movq	800(%rsp), %r8          # 8-byte Reload
	movq	%rcx, 576(%rsp)         # 8-byte Spill
	movq	%r8, %rcx
	movq	776(%rsp), %r8          # 8-byte Reload
	movq	696(%rsp), %r9          # 8-byte Reload
	movq	664(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	616(%rsp), %rdi         # 8-byte Reload
	movq	624(%rsp), %rsi         # 8-byte Reload
	movq	640(%rsp), %rdx         # 8-byte Reload
	movq	632(%rsp), %rcx         # 8-byte Reload
	movq	656(%rsp), %r8          # 8-byte Reload
	movl	684(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	600(%rsp), %rdi         # 8-byte Reload
	movq	696(%rsp), %rsi         # 8-byte Reload
	movq	696(%rsp), %rdx         # 8-byte Reload
	movq	800(%rsp), %rcx         # 8-byte Reload
	movq	592(%rsp), %r8          # 8-byte Reload
	movq	696(%rsp), %r9          # 8-byte Reload
	movq	664(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	656(%rsp), %rdi         # 8-byte Reload
	movq	696(%rsp), %rsi         # 8-byte Reload
	movq	696(%rsp), %rdx         # 8-byte Reload
	movq	800(%rsp), %rcx         # 8-byte Reload
	movq	608(%rsp), %r8          # 8-byte Reload
	movq	696(%rsp), %r9          # 8-byte Reload
	movq	664(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	591(%rsp), %al          # 1-byte Reload
	andb	$1, %al
	movzbl	%al, %ebp
	movl	%ebp, %edx
	movq	800(%rsp), %rdi         # 8-byte Reload
	movq	800(%rsp), %rsi         # 8-byte Reload
	movq	800(%rsp), %rcx         # 8-byte Reload
	movq	688(%rsp), %r8          # 8-byte Reload
	movq	696(%rsp), %r9          # 8-byte Reload
	movq	664(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	591(%rsp), %al          # 1-byte Reload
	testb	$1, %al
	jne	.LBB2_8
.Ltmp100:
# BB#7:
	leaq	.L.str4, %rax
	movabsq	$2, %rdi
	movabsq	$64, %rcx
	movabsq	$0, %rdx
	leaq	.L.str13, %r8
	leaq	.L.str9, %rsi
	movabsq	$1, %r9
	leaq	.L.str73, %r10
	movabsq	$4, %r11
	leaq	fprintf, %rbx
	leaq	.L.str42, %r14
	movabsq	$83, %r15
	leaq	.L.str46, %r12
	leaq	.L.str76, %r13
	leaq	.L.str77, %rbp
	movq	%rax, 568(%rsp)         # 8-byte Spill
	movabsq	$49, %rax
	movq	%rsi, 560(%rsp)         # 8-byte Spill
	movl	$0, %esi
	movl	%esi, 556(%rsp)         # 4-byte Spill
	movl	$1, %esi
	.loc	1 83 0                  # store_kernel.c:83:0
.Ltmp101:
	movq	%rdi, 544(%rsp)         # 8-byte Spill
	movq	%r15, %rdi
	movl	%esi, 540(%rsp)         # 4-byte Spill
	movq	%r12, %rsi
	movq	%rdx, 528(%rsp)         # 8-byte Spill
	movq	%r13, %rdx
	movq	%rcx, 520(%rsp)         # 8-byte Spill
	movq	%rbp, %rcx
	movq	%r8, 512(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movl	556(%rsp), %ebp         # 4-byte Reload
	movq	%r9, 504(%rsp)          # 8-byte Spill
	movl	%ebp, %r9d
	movl	$1, (%rsp)
	movq	%r14, 496(%rsp)         # 8-byte Spill
	movq	%r10, 488(%rsp)         # 8-byte Spill
	movq	%rbx, 480(%rsp)         # 8-byte Spill
	movq	%r11, 472(%rsp)         # 8-byte Spill
	callq	trace_logger_log0
	movq	472(%rsp), %rdi         # 8-byte Reload
	movq	520(%rsp), %rsi         # 8-byte Reload
	movq	480(%rsp), %rdx         # 8-byte Reload
	movq	504(%rsp), %rcx         # 8-byte Reload
	movq	496(%rsp), %r8          # 8-byte Reload
	movq	528(%rsp), %r9          # 8-byte Reload
	movq	560(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	504(%rsp), %rdi         # 8-byte Reload
	movq	520(%rsp), %rsi         # 8-byte Reload
	movq	576(%rsp), %rdx         # 8-byte Reload
	movq	504(%rsp), %rcx         # 8-byte Reload
	movq	488(%rsp), %r8          # 8-byte Reload
	movq	528(%rsp), %r9          # 8-byte Reload
	movq	560(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	544(%rsp), %rdi         # 8-byte Reload
	movq	520(%rsp), %rsi         # 8-byte Reload
	movq	568(%rsp), %rdx         # 8-byte Reload
	movq	528(%rsp), %rcx         # 8-byte Reload
	movq	512(%rsp), %r8          # 8-byte Reload
	movq	528(%rsp), %r9          # 8-byte Reload
	movq	560(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	576(%rsp), %rdi         # 8-byte Reload
	movq	568(%rsp), %rsi         # 8-byte Reload
	movl	716(%rsp), %edx         # 4-byte Reload
	movb	$0, %al
	callq	fprintf
	movl	$4294967295, %edx       # imm = 0xFFFFFFFF
	movabsq	$1, %rcx
	movabsq	$0, %rsi
	leaq	.L.str79, %r8
	leaq	.L.str9, %rdi
	movabsq	$84, %r9
	leaq	.L.str46, %r10
	leaq	.L.str76, %r11
	leaq	.L.str78, %rbx
	movabsq	$2, %r14
	movl	$0, %ebp
	movl	$1, %r15d
	movabsq	$19134, %r12            # imm = 0x4ABE
	movabsq	$32, %r13
	movq	%rcx, 464(%rsp)         # 8-byte Spill
	leaq	.L.str77, %rcx
	.loc	1 84 0                  # store_kernel.c:84:0
	movl	%eax, %eax
                                        # kill: RAX<def> EAX<kill>
	movq	%rdi, 456(%rsp)         # 8-byte Spill
	movq	%r12, %rdi
	movq	%rsi, 448(%rsp)         # 8-byte Spill
	movq	%r13, %rsi
	movl	%edx, 444(%rsp)         # 4-byte Spill
	movq	%rax, %rdx
	movq	464(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 432(%rsp)         # 8-byte Spill
	movq	%rax, %rcx
	movq	432(%rsp), %r12         # 8-byte Reload
	movq	%r8, 424(%rsp)          # 8-byte Spill
	movq	%r12, %r8
	movq	448(%rsp), %r13         # 8-byte Reload
	movq	%r9, 416(%rsp)          # 8-byte Spill
	movq	%r13, %r9
	movq	456(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	movl	%r15d, 412(%rsp)        # 4-byte Spill
	movq	%rbx, 400(%rsp)         # 8-byte Spill
	movl	%ebp, 396(%rsp)         # 4-byte Spill
	movq	%r14, 384(%rsp)         # 8-byte Spill
	movq	%r10, 376(%rsp)         # 8-byte Spill
	movq	%r11, 368(%rsp)         # 8-byte Spill
	callq	trace_logger_log_int
	movq	416(%rsp), %rdi         # 8-byte Reload
	movq	376(%rsp), %rsi         # 8-byte Reload
	movq	368(%rsp), %rdx         # 8-byte Reload
	movq	400(%rsp), %rcx         # 8-byte Reload
	movq	384(%rsp), %r8          # 8-byte Reload
	movl	396(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	464(%rsp), %rdi         # 8-byte Reload
	movq	448(%rsp), %rsi         # 8-byte Reload
	movq	448(%rsp), %rdx         # 8-byte Reload
	movq	464(%rsp), %rcx         # 8-byte Reload
	movq	424(%rsp), %r8          # 8-byte Reload
	movq	448(%rsp), %r9          # 8-byte Reload
	movq	456(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	444(%rsp), %ebp         # 4-byte Reload
	movl	%ebp, 364(%rsp)         # 4-byte Spill
	jmp	.LBB2_9
.Ltmp102:
.LBB2_8:
	leaq	.L.str5, %rax
	movabsq	$2, %rdi
	movabsq	$64, %rcx
	movabsq	$0, %rdx
	leaq	.L.str13, %r8
	leaq	.L.str9, %rsi
	movabsq	$1, %r9
	leaq	.L.str73, %r10
	movabsq	$3, %r11
	leaq	fprintf, %rbx
	leaq	.L.str42, %r14
	movabsq	$86, %r15
	leaq	.L.str46, %r12
	leaq	.L.str75, %r13
	leaq	.L.str80, %rbp
	movq	%rax, 352(%rsp)         # 8-byte Spill
	movabsq	$49, %rax
	movq	%rsi, 344(%rsp)         # 8-byte Spill
	movl	$0, %esi
	movl	%esi, 340(%rsp)         # 4-byte Spill
	movl	$1, %esi
	.loc	1 86 0                  # store_kernel.c:86:0
	movq	%rdi, 328(%rsp)         # 8-byte Spill
	movq	%r15, %rdi
	movl	%esi, 324(%rsp)         # 4-byte Spill
	movq	%r12, %rsi
	movq	%rdx, 312(%rsp)         # 8-byte Spill
	movq	%r13, %rdx
	movq	%rcx, 304(%rsp)         # 8-byte Spill
	movq	%rbp, %rcx
	movq	%r8, 296(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movl	340(%rsp), %ebp         # 4-byte Reload
	movq	%r9, 288(%rsp)          # 8-byte Spill
	movl	%ebp, %r9d
	movl	$1, (%rsp)
	movq	%r14, 280(%rsp)         # 8-byte Spill
	movq	%r10, 272(%rsp)         # 8-byte Spill
	movq	%rbx, 264(%rsp)         # 8-byte Spill
	movq	%r11, 256(%rsp)         # 8-byte Spill
	callq	trace_logger_log0
	movq	256(%rsp), %rdi         # 8-byte Reload
	movq	304(%rsp), %rsi         # 8-byte Reload
	movq	264(%rsp), %rdx         # 8-byte Reload
	movq	288(%rsp), %rcx         # 8-byte Reload
	movq	280(%rsp), %r8          # 8-byte Reload
	movq	312(%rsp), %r9          # 8-byte Reload
	movq	344(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	288(%rsp), %rdi         # 8-byte Reload
	movq	304(%rsp), %rsi         # 8-byte Reload
	movq	576(%rsp), %rdx         # 8-byte Reload
	movq	288(%rsp), %rcx         # 8-byte Reload
	movq	272(%rsp), %r8          # 8-byte Reload
	movq	312(%rsp), %r9          # 8-byte Reload
	movq	344(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	328(%rsp), %rdi         # 8-byte Reload
	movq	304(%rsp), %rsi         # 8-byte Reload
	movq	352(%rsp), %rdx         # 8-byte Reload
	movq	312(%rsp), %rcx         # 8-byte Reload
	movq	296(%rsp), %r8          # 8-byte Reload
	movq	312(%rsp), %r9          # 8-byte Reload
	movq	344(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	576(%rsp), %rdi         # 8-byte Reload
	movq	352(%rsp), %rsi         # 8-byte Reload
	movb	$0, %al
	callq	fprintf
	movl	$0, %ebp
	movabsq	$1, %rcx
	movabsq	$0, %rdx
	leaq	.L.str79, %r8
	leaq	.L.str9, %rsi
	movabsq	$87, %rdi
	leaq	.L.str46, %r9
	leaq	.L.str75, %r10
	leaq	.L.str81, %r11
	movabsq	$2, %rbx
	movl	$1, %r14d
	movabsq	$19134, %r15            # imm = 0x4ABE
	movabsq	$32, %r12
	leaq	.L.str80, %r13
	.loc	1 87 0                  # store_kernel.c:87:0
	movl	%eax, %eax
                                        # kill: RAX<def> EAX<kill>
	movq	%rdi, 248(%rsp)         # 8-byte Spill
	movq	%r15, %rdi
	movq	%rsi, 240(%rsp)         # 8-byte Spill
	movq	%r12, %rsi
	movq	%rdx, 232(%rsp)         # 8-byte Spill
	movq	%rax, %rdx
	movq	%rcx, 224(%rsp)         # 8-byte Spill
	movq	%r8, 216(%rsp)          # 8-byte Spill
	movq	%r13, %r8
	movq	232(%rsp), %rax         # 8-byte Reload
	movq	%r9, 208(%rsp)          # 8-byte Spill
	movq	%rax, %r9
	movq	240(%rsp), %r15         # 8-byte Reload
	movq	%r15, (%rsp)
	movl	%ebp, 204(%rsp)         # 4-byte Spill
	movq	%r10, 192(%rsp)         # 8-byte Spill
	movq	%r11, 184(%rsp)         # 8-byte Spill
	movq	%rbx, 176(%rsp)         # 8-byte Spill
	movl	%r14d, 172(%rsp)        # 4-byte Spill
	callq	trace_logger_log_int
	movq	248(%rsp), %rdi         # 8-byte Reload
	movq	208(%rsp), %rsi         # 8-byte Reload
	movq	192(%rsp), %rdx         # 8-byte Reload
	movq	184(%rsp), %rcx         # 8-byte Reload
	movq	176(%rsp), %r8          # 8-byte Reload
	movl	204(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	224(%rsp), %rdi         # 8-byte Reload
	movq	232(%rsp), %rsi         # 8-byte Reload
	movq	232(%rsp), %rdx         # 8-byte Reload
	movq	224(%rsp), %rcx         # 8-byte Reload
	movq	216(%rsp), %r8          # 8-byte Reload
	movq	232(%rsp), %r9          # 8-byte Reload
	movq	240(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	204(%rsp), %ebp         # 4-byte Reload
	movl	%ebp, 364(%rsp)         # 4-byte Spill
.LBB2_9:
	movl	364(%rsp), %eax         # 4-byte Reload
	movabsq	$1, %rcx
	movabsq	$32, %rdx
	leaq	.L.str82, %rsi
	movabsq	$0, %rdi
	leaq	.L.str9, %r8
	movabsq	$88, %r9
	leaq	.L.str46, %r10
	leaq	.L.str79, %r11
	leaq	.L.str83, %rbx
	movl	$0, %ebp
	movl	$1, %r14d
	movabsq	$19134, %r15            # imm = 0x4ABE
	leaq	.L.str76, %r12
	movabsq	$4294967295, %r13       # imm = 0xFFFFFFFF
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	leaq	.L.str13, %rcx
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	movabsq	$2, %rcx
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	leaq	.L.str75, %rcx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	movabsq	$-1, %rcx
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	movabsq	$48, %rcx
	.loc	1 88 0                  # store_kernel.c:88:0
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	movq	128(%rsp), %rcx         # 8-byte Reload
	movq	%rdi, 112(%rsp)         # 8-byte Spill
	movq	%rcx, %rdi
	movq	%rsi, 104(%rsp)         # 8-byte Spill
	movq	%r10, %rsi
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	movq	%r11, %rdx
	movq	104(%rsp), %rcx         # 8-byte Reload
	movq	%r8, 88(%rsp)           # 8-byte Spill
	movq	120(%rsp), %r8          # 8-byte Reload
	movq	%r9, 80(%rsp)           # 8-byte Spill
	movl	%ebp, %r9d
	movl	$1, (%rsp)
	movl	%eax, 76(%rsp)          # 4-byte Spill
	movl	%r14d, 72(%rsp)         # 4-byte Spill
	movq	%r15, 64(%rsp)          # 8-byte Spill
	movq	%r12, 56(%rsp)          # 8-byte Spill
	movq	%r13, 48(%rsp)          # 8-byte Spill
	movl	%ebp, 44(%rsp)          # 4-byte Spill
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	%r10, 24(%rsp)          # 8-byte Spill
	movq	%r11, 16(%rsp)          # 8-byte Spill
	callq	trace_logger_log0
	movq	144(%rsp), %rdi         # 8-byte Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	112(%rsp), %rdx         # 8-byte Reload
	movq	112(%rsp), %rcx         # 8-byte Reload
	movq	152(%rsp), %r8          # 8-byte Reload
	movq	160(%rsp), %r9          # 8-byte Reload
	movq	136(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	160(%rsp), %rdi         # 8-byte Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	112(%rsp), %rcx         # 8-byte Reload
	movq	152(%rsp), %r8          # 8-byte Reload
	movq	160(%rsp), %r9          # 8-byte Reload
	movq	56(%rsp), %r10          # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movl	76(%rsp), %eax          # 4-byte Reload
	movl	%eax, %ebp
	movl	%ebp, %edx
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	104(%rsp), %r8          # 8-byte Reload
	movq	112(%rsp), %r9          # 8-byte Reload
	movq	56(%rsp), %r10          # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	80(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	160(%rsp), %r8          # 8-byte Reload
	movl	44(%rsp), %r9d          # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movl	76(%rsp), %eax          # 4-byte Reload
	movl	%eax, %r9d
	movl	%r9d, %edx
	movq	160(%rsp), %rdi         # 8-byte Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	104(%rsp), %r8          # 8-byte Reload
	movq	112(%rsp), %r9          # 8-byte Reload
	movq	88(%rsp), %r10          # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movl	76(%rsp), %eax          # 4-byte Reload
	addq	$2968, %rsp             # imm = 0xB98
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.Ltmp103:
.Ltmp104:
	.size	main, .Ltmp104-main
.Lfunc_end2:
	.cfi_endproc

	.globl	_dmaImpl3
	.align	16, 0x90
	.type	_dmaImpl3,@function
_dmaImpl3:                              # @_dmaImpl3
	.cfi_startproc
.Lfunc_begin3:
	.loc	2 14 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:14:0
# BB#0:
	pushq	%rbp
.Ltmp112:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Ltmp113:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Ltmp114:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Ltmp115:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Ltmp116:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Ltmp117:
	.cfi_def_cfa_offset 56
	subq	$632, %rsp              # imm = 0x278
.Ltmp118:
	.cfi_def_cfa_offset 688
.Ltmp119:
	.cfi_offset %rbx, -56
.Ltmp120:
	.cfi_offset %r12, -48
.Ltmp121:
	.cfi_offset %r13, -40
.Ltmp122:
	.cfi_offset %r14, -32
.Ltmp123:
	.cfi_offset %r15, -24
.Ltmp124:
	.cfi_offset %rbp, -16
	movabsq	$1, %rax
	leaq	.L.str489, %rcx
	movabsq	$0, %r8
	leaq	.L.str691, %r9
	movabsq	$2, %r10
	leaq	.L.str1095, %r11
	movabsq	$3, %rbx
	leaq	.L.str994, %r14
	movabsq	$15, %r15
	leaq	.L.str287, %r12
	leaq	.L.str388, %r13
	leaq	.L.str893, %rbp
	movq	%rsi, 624(%rsp)         # 8-byte Spill
	movl	$0, %esi
	movl	%esi, 620(%rsp)         # 4-byte Spill
	movl	$1, %esi
	movq	%rax, 608(%rsp)         # 8-byte Spill
	movabsq	$19134, %rax            # imm = 0x4ABE
	movq	%rax, 600(%rsp)         # 8-byte Spill
	movabsq	$64, %rax
	movq	%rax, 592(%rsp)         # 8-byte Spill
	leaq	.L.str792, %rax
	movq	%rax, 584(%rsp)         # 8-byte Spill
	leaq	.L.str590, %rax
	movq	%rax, 576(%rsp)         # 8-byte Spill
	movabsq	$46, %rax
	movq	%rax, 568(%rsp)         # 8-byte Spill
	movabsq	$24601, %rax            # imm = 0x6019
	movq	%rax, 560(%rsp)         # 8-byte Spill
	leaq	.L.str17102, %rax
	movq	%rax, 552(%rsp)         # 8-byte Spill
	leaq	.L.str16101, %rax
	.loc	2 14 0 prologue_end     # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:14:0
.Ltmp125:
	movq	%rax, 544(%rsp)         # 8-byte Spill
	movq	560(%rsp), %rax         # 8-byte Reload
	movq	%rdi, 536(%rsp)         # 8-byte Spill
	movq	%rax, %rdi
	movq	592(%rsp), %rax         # 8-byte Reload
	movl	%esi, 532(%rsp)         # 4-byte Spill
	movq	%rax, %rsi
	movq	536(%rsp), %rax         # 8-byte Reload
	movq	%rdx, 520(%rsp)         # 8-byte Spill
	movq	%rax, %rdx
	movq	608(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 512(%rsp)         # 8-byte Spill
	movq	%rax, %rcx
	movq	544(%rsp), %rax         # 8-byte Reload
	movq	%r8, 504(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movq	504(%rsp), %rax         # 8-byte Reload
	movq	%r9, 496(%rsp)          # 8-byte Spill
	movq	%rax, %r9
	movq	496(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	movq	%r13, 488(%rsp)         # 8-byte Spill
	movq	%rbp, 480(%rsp)         # 8-byte Spill
	movq	%r12, 472(%rsp)         # 8-byte Spill
	movq	%r15, 464(%rsp)         # 8-byte Spill
	movq	%r14, 456(%rsp)         # 8-byte Spill
	movq	%rbx, 448(%rsp)         # 8-byte Spill
	movq	%r10, 440(%rsp)         # 8-byte Spill
	movq	%r11, 432(%rsp)         # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	560(%rsp), %rdi         # 8-byte Reload
	movq	592(%rsp), %rsi         # 8-byte Reload
	movq	624(%rsp), %rdx         # 8-byte Reload
	movq	608(%rsp), %rcx         # 8-byte Reload
	movq	552(%rsp), %r8          # 8-byte Reload
	movq	504(%rsp), %r9          # 8-byte Reload
	movq	496(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	560(%rsp), %rdi         # 8-byte Reload
	movq	592(%rsp), %rsi         # 8-byte Reload
	movq	520(%rsp), %rdx         # 8-byte Reload
	movq	608(%rsp), %rcx         # 8-byte Reload
	movq	584(%rsp), %r8          # 8-byte Reload
	movq	504(%rsp), %r9          # 8-byte Reload
	movq	496(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	#DEBUG_VALUE: _dmaImpl3:dst_addr <- [RSP+536]
	#DEBUG_VALUE: _dmaImpl3:src_addr <- [RSP+624]
	#DEBUG_VALUE: _dmaImpl3:size <- [RSP+520]
	.loc	2 15 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:15:0
	movq	464(%rsp), %rdi         # 8-byte Reload
	movq	472(%rsp), %rsi         # 8-byte Reload
	movq	488(%rsp), %rdx         # 8-byte Reload
	movq	512(%rsp), %rcx         # 8-byte Reload
	movq	568(%rsp), %r8          # 8-byte Reload
	movl	620(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	440(%rsp), %rdi         # 8-byte Reload
	movq	592(%rsp), %rsi         # 8-byte Reload
	movq	504(%rsp), %rdx         # 8-byte Reload
	movq	504(%rsp), %rcx         # 8-byte Reload
	movq	576(%rsp), %r8          # 8-byte Reload
	movq	504(%rsp), %r9          # 8-byte Reload
	movq	496(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	608(%rsp), %rdi         # 8-byte Reload
	movq	592(%rsp), %rsi         # 8-byte Reload
	movq	520(%rsp), %rdx         # 8-byte Reload
	movq	608(%rsp), %rcx         # 8-byte Reload
	movq	584(%rsp), %r8          # 8-byte Reload
	movq	504(%rsp), %r9          # 8-byte Reload
	movq	496(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	520(%rsp), %rax         # 8-byte Reload
	cmpq	$0, %rax
	sete	%al
	movb	%al, %cl
	andb	$1, %cl
	movzbl	%cl, %ecx
	movl	%ecx, %edx
	movq	600(%rsp), %rdi         # 8-byte Reload
	movq	608(%rsp), %rsi         # 8-byte Reload
	movq	608(%rsp), %rcx         # 8-byte Reload
	movq	512(%rsp), %r8          # 8-byte Reload
	movq	504(%rsp), %r9          # 8-byte Reload
	movq	496(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movb	%al, 431(%rsp)          # 1-byte Spill
	callq	trace_logger_log_int
	movq	464(%rsp), %rdi         # 8-byte Reload
	movq	472(%rsp), %rsi         # 8-byte Reload
	movq	488(%rsp), %rdx         # 8-byte Reload
	movq	480(%rsp), %rcx         # 8-byte Reload
	movq	440(%rsp), %r8          # 8-byte Reload
	movl	620(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	448(%rsp), %rdi         # 8-byte Reload
	movq	504(%rsp), %rsi         # 8-byte Reload
	movq	504(%rsp), %rdx         # 8-byte Reload
	movq	608(%rsp), %rcx         # 8-byte Reload
	movq	456(%rsp), %r8          # 8-byte Reload
	movq	504(%rsp), %r9          # 8-byte Reload
	movq	496(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	440(%rsp), %rdi         # 8-byte Reload
	movq	504(%rsp), %rsi         # 8-byte Reload
	movq	504(%rsp), %rdx         # 8-byte Reload
	movq	608(%rsp), %rcx         # 8-byte Reload
	movq	432(%rsp), %r8          # 8-byte Reload
	movq	504(%rsp), %r9          # 8-byte Reload
	movq	496(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	431(%rsp), %al          # 1-byte Reload
	andb	$1, %al
	movzbl	%al, %eax
	movl	%eax, %edx
	movq	608(%rsp), %rdi         # 8-byte Reload
	movq	608(%rsp), %rsi         # 8-byte Reload
	movq	608(%rsp), %rcx         # 8-byte Reload
	movq	512(%rsp), %r8          # 8-byte Reload
	movq	504(%rsp), %r9          # 8-byte Reload
	movq	496(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	431(%rsp), %al          # 1-byte Reload
	testb	$1, %al
	jne	.LBB3_1
	jmp	.LBB3_2
.Ltmp126:
.LBB3_1:
	movabsq	$4, %rdi
	movabsq	$64, %rax
	leaq	.L__PRETTY_FUNCTION__._dmaImpl3, %rdx
	movabsq	$0, %rcx
	leaq	.L.str590, %rsi
	leaq	.L.str691, %r8
	movabsq	$3, %r9
	movabsq	$32, %r10
	movabsq	$15, %r11
	movabsq	$2, %rbx
	leaq	.L.str186, %r14
	movabsq	$1, %r15
	leaq	.L.str85, %r12
	movabsq	$5, %r13
	leaq	__assert_fail, %rbp
	movq	%rax, 416(%rsp)         # 8-byte Spill
	leaq	.L.str1297, %rax
	movq	%rax, 408(%rsp)         # 8-byte Spill
	leaq	.L.str287, %rax
	movq	%rax, 400(%rsp)         # 8-byte Spill
	leaq	.L.str994, %rax
	movq	%rax, 392(%rsp)         # 8-byte Spill
	leaq	.L.str1196, %rax
	movq	%rax, 384(%rsp)         # 8-byte Spill
	movabsq	$49, %rax
	movq	%rsi, 376(%rsp)         # 8-byte Spill
	movl	$0, %esi
	movl	%esi, 372(%rsp)         # 4-byte Spill
	movl	$1, %esi
	movq	%rdi, 360(%rsp)         # 8-byte Spill
	movq	%r11, %rdi
	movq	%rax, 352(%rsp)         # 8-byte Spill
	movq	400(%rsp), %rax         # 8-byte Reload
	movl	%esi, 348(%rsp)         # 4-byte Spill
	movq	%rax, %rsi
	movq	392(%rsp), %rax         # 8-byte Reload
	movq	%rdx, 336(%rsp)         # 8-byte Spill
	movq	%rax, %rdx
	movq	384(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 328(%rsp)         # 8-byte Spill
	movq	%rax, %rcx
	movq	352(%rsp), %rax         # 8-byte Reload
	movq	%r8, 320(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movl	372(%rsp), %eax         # 4-byte Reload
	movq	%r9, 312(%rsp)          # 8-byte Spill
	movl	%eax, %r9d
	movl	$1, (%rsp)
	movq	%rbp, 304(%rsp)         # 8-byte Spill
	movq	%r12, 296(%rsp)         # 8-byte Spill
	movq	%r15, 288(%rsp)         # 8-byte Spill
	movq	%r13, 280(%rsp)         # 8-byte Spill
	movq	%r14, 272(%rsp)         # 8-byte Spill
	movq	%rbx, 264(%rsp)         # 8-byte Spill
	movq	%r10, 256(%rsp)         # 8-byte Spill
	movq	%r11, 248(%rsp)         # 8-byte Spill
	callq	trace_logger_log0
	movq	280(%rsp), %rdi         # 8-byte Reload
	movq	416(%rsp), %rsi         # 8-byte Reload
	movq	304(%rsp), %rdx         # 8-byte Reload
	movq	288(%rsp), %rcx         # 8-byte Reload
	movq	408(%rsp), %r8          # 8-byte Reload
	movq	328(%rsp), %r9          # 8-byte Reload
	movq	320(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	288(%rsp), %rdi         # 8-byte Reload
	movq	416(%rsp), %rsi         # 8-byte Reload
	movq	296(%rsp), %rdx         # 8-byte Reload
	movq	328(%rsp), %rcx         # 8-byte Reload
	movq	376(%rsp), %r8          # 8-byte Reload
	movq	328(%rsp), %r9          # 8-byte Reload
	movq	320(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	264(%rsp), %rdi         # 8-byte Reload
	movq	416(%rsp), %rsi         # 8-byte Reload
	movq	272(%rsp), %rdx         # 8-byte Reload
	movq	328(%rsp), %rcx         # 8-byte Reload
	movq	376(%rsp), %r8          # 8-byte Reload
	movq	328(%rsp), %r9          # 8-byte Reload
	movq	320(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	312(%rsp), %rdi         # 8-byte Reload
	movq	256(%rsp), %rsi         # 8-byte Reload
	movq	248(%rsp), %rdx         # 8-byte Reload
	movq	328(%rsp), %rcx         # 8-byte Reload
	movq	376(%rsp), %r8          # 8-byte Reload
	movq	328(%rsp), %r9          # 8-byte Reload
	movq	320(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	360(%rsp), %rdi         # 8-byte Reload
	movq	416(%rsp), %rsi         # 8-byte Reload
	movq	336(%rsp), %rdx         # 8-byte Reload
	movq	328(%rsp), %rcx         # 8-byte Reload
	movq	376(%rsp), %r8          # 8-byte Reload
	movq	328(%rsp), %r9          # 8-byte Reload
	movq	320(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movl	$.L.str85, %eax
	movl	%eax, %edi
	movl	$.L.str186, %eax
	movl	%eax, %esi
	movl	$.L__PRETTY_FUNCTION__._dmaImpl3, %eax
	movl	%eax, %ecx
	movl	$15, %edx
	callq	__assert_fail
	movabsq	$15, %rdi
	leaq	.L.str287, %rsi
	leaq	.L.str994, %rdx
	leaq	.L.str1398, %rcx
	movabsq	$7, %r8
	movl	$0, %r9d
	movl	$1, %eax
	movl	$1, (%rsp)
	movl	%eax, 244(%rsp)         # 4-byte Spill
	callq	trace_logger_log0
.LBB3_2:
	movabsq	$3, %rdi
	movabsq	$64, %rax
	movabsq	$1, %rcx
	leaq	.L.str792, %r8
	movabsq	$0, %rdx
	leaq	.L.str691, %rsi
	movabsq	$2, %r9
	leaq	.L.str17102, %r10
	leaq	.L.str16101, %r11
	movabsq	$4, %rbx
	leaq	memmove, %r14
	leaq	.L.str15100, %r15
	movabsq	$16, %r12
	leaq	.L.str287, %r13
	leaq	.L.str1095, %rbp
	movq	%rax, 232(%rsp)         # 8-byte Spill
	leaq	.L.str1499, %rax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	movabsq	$49, %rax
	movq	%rsi, 216(%rsp)         # 8-byte Spill
	movl	$0, %esi
	movl	%esi, 212(%rsp)         # 4-byte Spill
	movl	$1, %esi
	.loc	2 16 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:16:0
	movq	%rdi, 200(%rsp)         # 8-byte Spill
	movq	%r12, %rdi
	movl	%esi, 196(%rsp)         # 4-byte Spill
	movq	%r13, %rsi
	movq	%rdx, 184(%rsp)         # 8-byte Spill
	movq	%rbp, %rdx
	movq	224(%rsp), %r12         # 8-byte Reload
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	movq	%r12, %rcx
	movq	%r8, 168(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movl	212(%rsp), %ebp         # 4-byte Reload
	movq	%r9, 160(%rsp)          # 8-byte Spill
	movl	%ebp, %r9d
	movl	$1, (%rsp)
	movq	%r14, 152(%rsp)         # 8-byte Spill
	movq	%r11, 144(%rsp)         # 8-byte Spill
	movq	%r10, 136(%rsp)         # 8-byte Spill
	movq	%rbx, 128(%rsp)         # 8-byte Spill
	movq	%r15, 120(%rsp)         # 8-byte Spill
	callq	trace_logger_log0
	movq	128(%rsp), %rdi         # 8-byte Reload
	movq	232(%rsp), %rsi         # 8-byte Reload
	movq	152(%rsp), %rdx         # 8-byte Reload
	movq	176(%rsp), %rcx         # 8-byte Reload
	movq	120(%rsp), %r8          # 8-byte Reload
	movq	184(%rsp), %r9          # 8-byte Reload
	movq	216(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	176(%rsp), %rdi         # 8-byte Reload
	movq	232(%rsp), %rsi         # 8-byte Reload
	movq	536(%rsp), %rdx         # 8-byte Reload
	movq	176(%rsp), %rcx         # 8-byte Reload
	movq	144(%rsp), %r8          # 8-byte Reload
	movq	184(%rsp), %r9          # 8-byte Reload
	movq	216(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	160(%rsp), %rdi         # 8-byte Reload
	movq	232(%rsp), %rsi         # 8-byte Reload
	movq	624(%rsp), %rdx         # 8-byte Reload
	movq	176(%rsp), %rcx         # 8-byte Reload
	movq	136(%rsp), %r8          # 8-byte Reload
	movq	184(%rsp), %r9          # 8-byte Reload
	movq	216(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	200(%rsp), %rdi         # 8-byte Reload
	movq	232(%rsp), %rsi         # 8-byte Reload
	movq	520(%rsp), %rdx         # 8-byte Reload
	movq	176(%rsp), %rcx         # 8-byte Reload
	movq	168(%rsp), %r8          # 8-byte Reload
	movq	184(%rsp), %r9          # 8-byte Reload
	movq	216(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	536(%rsp), %rdi         # 8-byte Reload
	movq	624(%rsp), %rsi         # 8-byte Reload
	movq	520(%rsp), %rdx         # 8-byte Reload
	callq	memmove
	movl	$3, %ebp
	movabsq	$1, %rcx
	movabsq	$32, %rsi
	movabsq	$3, %rdx
	movabsq	$0, %rdi
	leaq	.L.str590, %r8
	leaq	.L.str691, %r9
	movabsq	$17, %r10
	leaq	.L.str287, %r11
	leaq	.L.str1095, %rbx
	leaq	.L.str18103, %r14
	movl	$0, %r15d
	movl	$1, %r12d
	movabsq	$19134, %r13            # imm = 0x4ABE
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movabsq	$64, %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	leaq	.L.str1499, %rax
	.loc	2 17 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:17:0
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	movq	%r13, %rdi
	movq	104(%rsp), %r13         # 8-byte Reload
	movq	%rsi, 88(%rsp)          # 8-byte Spill
	movq	%r13, %rsi
	movq	112(%rsp), %r13         # 8-byte Reload
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movq	%r13, %rdx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	%r8, 64(%rsp)           # 8-byte Spill
	movq	%rax, %r8
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	%r9, 56(%rsp)           # 8-byte Spill
	movq	%rax, %r9
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	movl	%ebp, 44(%rsp)          # 4-byte Spill
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movl	%r15d, 28(%rsp)         # 4-byte Spill
	movl	%r12d, 24(%rsp)         # 4-byte Spill
	movq	%r11, 16(%rsp)          # 8-byte Spill
	movq	%r10, 8(%rsp)           # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	72(%rsp), %r8           # 8-byte Reload
	movl	28(%rsp), %r9d          # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	88(%rsp), %rsi          # 8-byte Reload
	movq	80(%rsp), %rdx          # 8-byte Reload
	movq	96(%rsp), %rcx          # 8-byte Reload
	movq	64(%rsp), %r8           # 8-byte Reload
	movq	96(%rsp), %r9           # 8-byte Reload
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	44(%rsp), %eax          # 4-byte Reload
	addq	$632, %rsp              # imm = 0x278
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.Ltmp127:
.Ltmp128:
	.size	_dmaImpl3, .Ltmp128-_dmaImpl3
.Lfunc_end3:
	.cfi_endproc

	.globl	dmaLoad
	.align	16, 0x90
	.type	dmaLoad,@function
dmaLoad:                                # @dmaLoad
	.cfi_startproc
.Lfunc_begin4:
	.loc	2 20 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:20:0
# BB#0:
	pushq	%r15
.Ltmp133:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Ltmp134:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Ltmp135:
	.cfi_def_cfa_offset 32
	subq	$96, %rsp
.Ltmp136:
	.cfi_def_cfa_offset 128
.Ltmp137:
	.cfi_offset %rbx, -32
.Ltmp138:
	.cfi_offset %r14, -24
.Ltmp139:
	.cfi_offset %r15, -16
	movabsq	$24601, %rax            # imm = 0x6019
	movabsq	$64, %rcx
	movabsq	$1, %r8
	leaq	.L.str792, %r9
	movabsq	$0, %r10
	leaq	.L.str691, %r11
	leaq	.L.str19104, %rbx
	leaq	.L.str16101, %r14
	.loc	2 20 0 prologue_end     # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:20:0
.Ltmp140:
	movq	%rdi, 88(%rsp)          # 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movq	%rcx, %rsi
	movq	88(%rsp), %r15          # 8-byte Reload
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movq	%r15, %rdx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%r8, %rcx
	movq	%r8, 56(%rsp)           # 8-byte Spill
	movq	%r14, %r8
	movq	%r9, 48(%rsp)           # 8-byte Spill
	movq	%r10, %r9
	movq	%r11, (%rsp)
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movq	%r10, 32(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%r11, 16(%rsp)          # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	80(%rsp), %rdx          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	72(%rsp), %rdx          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	#DEBUG_VALUE: dmaLoad:dst_addr <- [RSP+88]
	#DEBUG_VALUE: dmaLoad:src_host_addr <- [RSP+80]
	#DEBUG_VALUE: dmaLoad:size <- [RSP+72]
.Ltmp141:
	#DEBUG_VALUE: _dmaImpl3:dst_addr <- [RSP+88]
	#DEBUG_VALUE: _dmaImpl3:src_addr <- [RSP+80]
	#DEBUG_VALUE: _dmaImpl3:size <- [RSP+72]
	.loc	2 15 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:15:0
	movq	72(%rsp), %rax          # 8-byte Reload
	cmpq	$0, %rax
	jne	.LBB4_2
.Ltmp142:
# BB#1:
	movl	$.L.str85, %eax
	movl	%eax, %edi
	movl	$.L.str186, %eax
	movl	%eax, %esi
	movl	$.L__PRETTY_FUNCTION__._dmaImpl3, %eax
	movl	%eax, %ecx
	movl	$15, %edx
	callq	__assert_fail
.LBB4_2:                                # %_dmaImpl3.exit
	.loc	2 16 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:16:0
	movq	88(%rsp), %rdi          # 8-byte Reload
	movq	80(%rsp), %rsi          # 8-byte Reload
	movq	72(%rsp), %rdx          # 8-byte Reload
	callq	memmove
	movl	$3, %ecx
.Ltmp143:
	.loc	2 21 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:21:0
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	%ecx, %eax
	addq	$96, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	ret
.Ltmp144:
.Ltmp145:
	.size	dmaLoad, .Ltmp145-dmaLoad
.Lfunc_end4:
	.cfi_endproc

	.globl	dmaStore
	.align	16, 0x90
	.type	dmaStore,@function
dmaStore:                               # @dmaStore
	.cfi_startproc
.Lfunc_begin5:
	.loc	2 24 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:24:0
# BB#0:
	pushq	%r15
.Ltmp150:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Ltmp151:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Ltmp152:
	.cfi_def_cfa_offset 32
	subq	$96, %rsp
.Ltmp153:
	.cfi_def_cfa_offset 128
.Ltmp154:
	.cfi_offset %rbx, -32
.Ltmp155:
	.cfi_offset %r14, -24
.Ltmp156:
	.cfi_offset %r15, -16
	movabsq	$24601, %rax            # imm = 0x6019
	movabsq	$64, %rcx
	movabsq	$1, %r8
	leaq	.L.str792, %r9
	movabsq	$0, %r10
	leaq	.L.str691, %r11
	leaq	.L.str17102, %rbx
	leaq	.L.str20105, %r14
	.loc	2 24 0 prologue_end     # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:24:0
.Ltmp157:
	movq	%rdi, 88(%rsp)          # 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movq	%rcx, %rsi
	movq	88(%rsp), %r15          # 8-byte Reload
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movq	%r15, %rdx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%r8, %rcx
	movq	%r8, 56(%rsp)           # 8-byte Spill
	movq	%r14, %r8
	movq	%r9, 48(%rsp)           # 8-byte Spill
	movq	%r10, %r9
	movq	%r11, (%rsp)
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movq	%r10, 32(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%r11, 16(%rsp)          # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	80(%rsp), %rdx          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	72(%rsp), %rdx          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	#DEBUG_VALUE: dmaStore:dst_host_addr <- [RSP+88]
	#DEBUG_VALUE: dmaStore:src_addr <- [RSP+80]
	#DEBUG_VALUE: dmaStore:size <- [RSP+72]
.Ltmp158:
	#DEBUG_VALUE: _dmaImpl3:dst_addr <- [RSP+88]
	#DEBUG_VALUE: _dmaImpl3:src_addr <- [RSP+80]
	#DEBUG_VALUE: _dmaImpl3:size <- [RSP+72]
	.loc	2 15 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:15:0
	movq	72(%rsp), %rax          # 8-byte Reload
	cmpq	$0, %rax
	jne	.LBB5_2
.Ltmp159:
# BB#1:
	movl	$.L.str85, %eax
	movl	%eax, %edi
	movl	$.L.str186, %eax
	movl	%eax, %esi
	movl	$.L__PRETTY_FUNCTION__._dmaImpl3, %eax
	movl	%eax, %ecx
	movl	$15, %edx
	callq	__assert_fail
.LBB5_2:                                # %_dmaImpl3.exit
	.loc	2 16 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:16:0
	movq	88(%rsp), %rdi          # 8-byte Reload
	movq	80(%rsp), %rsi          # 8-byte Reload
	movq	72(%rsp), %rdx          # 8-byte Reload
	callq	memmove
	movl	$3, %ecx
.Ltmp160:
	.loc	2 25 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:25:0
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	%ecx, %eax
	addq	$96, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	ret
.Ltmp161:
.Ltmp162:
	.size	dmaStore, .Ltmp162-dmaStore
.Lfunc_end5:
	.cfi_endproc

	.globl	setReadyBits
	.align	16, 0x90
	.type	setReadyBits,@function
setReadyBits:                           # @setReadyBits
	.cfi_startproc
.Lfunc_begin6:
	.loc	2 28 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:28:0
# BB#0:
	pushq	%rbp
.Ltmp170:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Ltmp171:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Ltmp172:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Ltmp173:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Ltmp174:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Ltmp175:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Ltmp176:
	.cfi_def_cfa_offset 160
.Ltmp177:
	.cfi_offset %rbx, -56
.Ltmp178:
	.cfi_offset %r12, -48
.Ltmp179:
	.cfi_offset %r13, -40
.Ltmp180:
	.cfi_offset %r14, -32
.Ltmp181:
	.cfi_offset %r15, -24
.Ltmp182:
	.cfi_offset %rbp, -16
	movl	$0, %eax
	movabsq	$24601, %rcx            # imm = 0x6019
	movabsq	$32, %r8
	movabsq	$1, %r9
	leaq	.L.str22107, %r10
	movabsq	$0, %r11
	leaq	.L.str691, %rbx
	movabsq	$64, %r14
	leaq	.L.str792, %r15
	leaq	.L.str21106, %r12
	.loc	2 28 0 prologue_end     # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:28:0
.Ltmp183:
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	movq	%rcx, %rdi
	movq	%rsi, 88(%rsp)          # 8-byte Spill
	movq	%r14, %rsi
	movq	96(%rsp), %r13          # 8-byte Reload
	movl	%edx, 84(%rsp)          # 4-byte Spill
	movq	%r13, %rdx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	%r9, %rcx
	movq	%r8, 64(%rsp)           # 8-byte Spill
	movq	%r12, %r8
	movq	%r9, 56(%rsp)           # 8-byte Spill
	movq	%r11, %r9
	movq	%rbx, (%rsp)
	movq	%r15, 48(%rsp)          # 8-byte Spill
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movl	%eax, 36(%rsp)          # 4-byte Spill
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movq	%r11, 16(%rsp)          # 8-byte Spill
	movq	%r10, 8(%rsp)           # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	88(%rsp), %rdx          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	movq	40(%rsp), %r10          # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movl	84(%rsp), %eax          # 4-byte Reload
	movl	%eax, %ebp
	movl	%ebp, %edx
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	movq	40(%rsp), %r10          # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	#DEBUG_VALUE: setReadyBits:start_addr <- [RSP+96]
	#DEBUG_VALUE: setReadyBits:size <- [RSP+88]
	#DEBUG_VALUE: setReadyBits:value <- [RSP+84]
	.loc	2 29 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:29:0
	#APP
	#NO_APP
	.loc	2 30 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:30:0
	movl	36(%rsp), %eax          # 4-byte Reload
	addq	$104, %rsp
.Ltmp184:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.Ltmp185:
.Ltmp186:
	.size	setReadyBits, .Ltmp186-setReadyBits
.Lfunc_end6:
	.cfi_endproc

	.globl	dmaFence
	.align	16, 0x90
	.type	dmaFence,@function
dmaFence:                               # @dmaFence
	.cfi_startproc
.Lfunc_begin7:
	.loc	2 71 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:71:0
# BB#0:
	.loc	2 72 0 prologue_end     # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:72:0
	#APP
	#NO_APP
	.loc	2 73 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:73:0
	ret
.Ltmp187:
.Ltmp188:
	.size	dmaFence, .Ltmp188-dmaFence
.Lfunc_end7:
	.cfi_endproc

	.globl	trace_logger_write_labelmap
	.align	16, 0x90
	.type	trace_logger_write_labelmap,@function
trace_logger_write_labelmap:            # @trace_logger_write_labelmap
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Ltmp190:
	.cfi_def_cfa_offset 32
	cmpb	$0, initp
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	jne	.LBB8_2
# BB#1:
	callq	trace_logger_init
.LBB8_2:
	movq	full_trace_file(%rip), %rdi
	movl	$.L.str112, %eax
	movl	%eax, %esi
	movl	$26, %edx
	callq	gzwrite
	movq	full_trace_file(%rip), %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%esi, %edx
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	%eax, 4(%rsp)           # 4-byte Spill
	callq	gzwrite
	movq	full_trace_file(%rip), %rdi
	movl	$.L.str1113, %edx
	movl	%edx, %esi
	movl	$25, %edx
	movl	%eax, (%rsp)            # 4-byte Spill
	addq	$24, %rsp
	jmp	gzwrite                 # TAILCALL
.Ltmp191:
	.size	trace_logger_write_labelmap, .Ltmp191-trace_logger_write_labelmap
	.cfi_endproc

	.globl	trace_logger_init
	.align	16, 0x90
	.type	trace_logger_init,@function
trace_logger_init:                      # @trace_logger_init
	.cfi_startproc
# BB#0:
	pushq	%rax
.Ltmp193:
	.cfi_def_cfa_offset 16
	movl	$.L.str2114, %eax
	movl	%eax, %edi
	movl	$.L.str3115, %eax
	movl	%eax, %esi
	callq	gzopen
	movq	%rax, full_trace_file
	cmpq	$0, %rax
	jne	.LBB9_2
# BB#1:
	movl	$.L.str4116, %eax
	movl	%eax, %edi
	callq	perror
	movl	$-1, %edi
	callq	exit
.LBB9_2:
	movl	$512, %eax              # imm = 0x200
	movl	%eax, %edi
	movl	$1, %eax
	movl	%eax, %esi
	callq	calloc
	movq	%rax, current_toplevel_function
	movl	$2, current_logging_status
	movl	$trace_logger_fin, %ecx
	movl	%ecx, %edi
	callq	atexit
	movb	$1, initp
	movl	%eax, 4(%rsp)           # 4-byte Spill
	popq	%rax
	ret
.Ltmp194:
	.size	trace_logger_init, .Ltmp194-trace_logger_init
	.cfi_endproc

	.globl	trace_logger_fin
	.align	16, 0x90
	.type	trace_logger_fin,@function
trace_logger_fin:                       # @trace_logger_fin
	.cfi_startproc
# BB#0:
	pushq	%rax
.Ltmp196:
	.cfi_def_cfa_offset 16
	movq	current_toplevel_function(%rip), %rdi
	callq	free
	movq	full_trace_file(%rip), %rdi
	popq	%rax
	jmp	gzclose                 # TAILCALL
.Ltmp197:
	.size	trace_logger_fin, .Ltmp197-trace_logger_fin
	.cfi_endproc

	.globl	log_or_not
	.align	16, 0x90
	.type	log_or_not,@function
log_or_not:                             # @log_or_not
	.cfi_startproc
# BB#0:
	subq	$40, %rsp
.Ltmp199:
	.cfi_def_cfa_offset 48
	movb	%sil, %al
	movb	%dil, %r8b
	testb	$1, %r8b
	movl	%edx, 36(%rsp)          # 4-byte Spill
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movb	%al, 23(%rsp)           # 1-byte Spill
	jne	.LBB11_2
	jmp	.LBB11_1
.LBB11_1:
	movl	$1, %eax
	movl	$2, %ecx
	movb	23(%rsp), %dl           # 1-byte Reload
	testb	%dl, %dl
	cmovel	%ecx, %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	jmp	.LBB11_9
.LBB11_2:
	movb	23(%rsp), %al           # 1-byte Reload
	testb	$1, %al
	jne	.LBB11_4
	jmp	.LBB11_3
.LBB11_3:
	movl	current_logging_status, %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	jmp	.LBB11_9
.LBB11_4:
	movl	$1, %eax
	movl	36(%rsp), %ecx          # 4-byte Reload
	cmpl	$1, %ecx
	movl	%eax, 16(%rsp)          # 4-byte Spill
	jne	.LBB11_9
# BB#5:
	movq	current_toplevel_function, %rax
	cmpb	$0, (%rax)
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jne	.LBB11_7
# BB#6:
	movl	$.L.str5117, %eax
	movl	%eax, %edi
	movl	$.L.str6118, %eax
	movl	%eax, %esi
	movl	$.L__PRETTY_FUNCTION__.log_or_not, %eax
	movl	%eax, %ecx
	movl	$118, %edx
	callq	__assert_fail
.LBB11_7:
	movl	$0, %eax
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%eax, 4(%rsp)           # 4-byte Spill
	callq	strcmp
	cmpl	$0, %eax
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	%eax, 16(%rsp)          # 4-byte Spill
	je	.LBB11_9
# BB#8:
	movl	$.L.str7119, %eax
	movl	%eax, %edi
	movl	$.L.str6118, %eax
	movl	%eax, %esi
	movl	$.L__PRETTY_FUNCTION__.log_or_not, %eax
	movl	%eax, %ecx
	movl	$123, %edx
	callq	__assert_fail
.LBB11_9:
	movl	16(%rsp), %eax          # 4-byte Reload
	addq	$40, %rsp
	ret
.Ltmp200:
	.size	log_or_not, .Ltmp200-log_or_not
	.cfi_endproc

	.globl	convert_bytes_to_hex
	.align	16, 0x90
	.type	convert_bytes_to_hex,@function
convert_bytes_to_hex:                   # @convert_bytes_to_hex
	.cfi_startproc
# BB#0:
	subq	$56, %rsp
.Ltmp202:
	.cfi_def_cfa_offset 64
	movabsq	$0, %rax
	movw	.L.str8120, %cx
	movw	%cx, (%rdi)
	movb	.L.str8120+2, %r8b
	movb	%r8b, 2(%rdi)
	addq	$2, %rdi
	cmpl	$0, %edx
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	%edx, 36(%rsp)          # 4-byte Spill
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	jle	.LBB12_2
.LBB12_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	movzbl	(%rdx,%rcx), %edx
	movl	$.L.str9121, %esi
                                        # kill: RSI<def> ESI<kill>
	xorl	%edi, %edi
	movb	%dil, %r8b
	movq	%rax, %rdi
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movb	%r8b, %al
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	callq	sprintf
	movslq	%eax, %rcx
	movq	16(%rsp), %rsi          # 8-byte Reload
	addq	%rcx, %rsi
	movq	8(%rsp), %rcx           # 8-byte Reload
	addq	$1, %rcx
	movl	%ecx, %eax
	movl	36(%rsp), %edx          # 4-byte Reload
	cmpl	%edx, %eax
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	jne	.LBB12_1
.LBB12_2:                               # %._crit_edge
	movq	24(%rsp), %rax          # 8-byte Reload
	movb	$0, (%rax)
	addq	$56, %rsp
	ret
.Ltmp203:
	.size	convert_bytes_to_hex, .Ltmp203-convert_bytes_to_hex
	.cfi_endproc

	.globl	update_logging_status
	.align	16, 0x90
	.type	update_logging_status,@function
update_logging_status:                  # @update_logging_status
	.cfi_startproc
# BB#0:
	subq	$56, %rsp
.Ltmp205:
	.cfi_def_cfa_offset 64
	movb	%cl, %al
	movb	%dl, %r8b
	movl	current_logging_status, %ecx
	cmpl	$0, %ecx
	movl	%esi, 52(%rsp)          # 4-byte Spill
	movb	%al, 51(%rsp)           # 1-byte Spill
	movb	%r8b, 50(%rsp)          # 1-byte Spill
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	movl	%ecx, 36(%rsp)          # 4-byte Spill
	jne	.LBB13_2
# BB#1:
	movl	inst_count, %esi
	movl	$.L.str10122, %eax
	movl	%eax, %edi
	xorl	%eax, %eax
	movb	%al, %cl
	movb	%cl, %al
	callq	printf
	movl	$2, current_logging_status
	movl	%eax, 32(%rsp)          # 4-byte Spill
	jmp	.LBB13_8
.LBB13_2:
	movb	51(%rsp), %al           # 1-byte Reload
	movzbl	%al, %ecx
	andl	$1, %ecx
	movb	50(%rsp), %dl           # 1-byte Reload
	movzbl	%dl, %esi
	andl	$1, %esi
	movl	%ecx, %edi
	movl	52(%rsp), %edx          # 4-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	callq	log_or_not
	movl	%eax, current_logging_status
	movl	36(%rsp), %edx          # 4-byte Reload
	cmpl	$2, %edx
	setne	%r8b
	cmpl	%edx, %eax
	sete	%r9b
	orb	%r9b, %r8b
	testb	$1, %r8b
	movl	%eax, 28(%rsp)          # 4-byte Spill
	jne	.LBB13_4
# BB#3:
	movl	inst_count, %esi
	movl	$.L.str11123, %eax
	movl	%eax, %edi
	xorl	%eax, %eax
	movb	%al, %cl
	movb	%cl, %al
	callq	printf
	movl	current_logging_status, %esi
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movl	%esi, 28(%rsp)          # 4-byte Spill
.LBB13_4:
	movl	28(%rsp), %eax          # 4-byte Reload
	movq	current_toplevel_function, %rcx
	cmpb	$0, (%rcx)
	sete	%dl
	cmpl	$1, %eax
	sete	%sil
	andb	%sil, %dl
	testb	$1, %dl
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	jne	.LBB13_5
	jmp	.LBB13_6
.LBB13_5:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
	callq	strcpy
	movq	%rax, (%rsp)            # 8-byte Spill
	jmp	.LBB13_8
.LBB13_6:
	movl	20(%rsp), %eax          # 4-byte Reload
	cmpl	$0, %eax
	jne	.LBB13_8
# BB#7:
	movl	$0, %esi
	movabsq	$512, %rdx              # imm = 0x200
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	memset
.LBB13_8:
	addq	$56, %rsp
	ret
.Ltmp206:
	.size	update_logging_status, .Ltmp206-update_logging_status
	.cfi_endproc

	.globl	do_not_log
	.align	16, 0x90
	.type	do_not_log,@function
do_not_log:                             # @do_not_log
	.cfi_startproc
# BB#0:
	cmpl	$2, current_logging_status
	sete	%al
	andb	$1, %al
	movzbl	%al, %eax
	ret
.Ltmp207:
	.size	do_not_log, .Ltmp207-do_not_log
	.cfi_endproc

	.globl	trace_logger_log_entry
	.align	16, 0x90
	.type	trace_logger_log_entry,@function
trace_logger_log_entry:                 # @trace_logger_log_entry
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Ltmp209:
	.cfi_def_cfa_offset 32
	cmpb	$0, initp
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movl	%esi, 12(%rsp)          # 4-byte Spill
	jne	.LBB15_2
# BB#1:
	callq	trace_logger_init
.LBB15_2:
	xorl	%esi, %esi
	movl	$1, %eax
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	%eax, %edx
	movl	%eax, %ecx
	callq	update_logging_status
	cmpl	$2, current_logging_status
	je	.LBB15_4
# BB#3:
	movq	full_trace_file, %rdi
	movl	$.L.str12124, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	12(%rsp), %eax          # 4-byte Reload
	movb	%cl, 11(%rsp)           # 1-byte Spill
	movl	%eax, %ecx
	movb	11(%rsp), %al           # 1-byte Reload
	callq	gzprintf
	movl	%eax, 4(%rsp)           # 4-byte Spill
.LBB15_4:
	addq	$24, %rsp
	ret
.Ltmp210:
	.size	trace_logger_log_entry, .Ltmp210-trace_logger_log_entry
	.cfi_endproc

	.globl	trace_logger_log0
	.align	16, 0x90
	.type	trace_logger_log0,@function
trace_logger_log0:                      # @trace_logger_log0
	.cfi_startproc
# BB#0:
	subq	$72, %rsp
.Ltmp212:
	.cfi_def_cfa_offset 80
	movb	%r9b, %al
	movb	80(%rsp), %r10b
	cmpb	$0, initp
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	movb	%r10b, 63(%rsp)         # 1-byte Spill
	movl	%r8d, 56(%rsp)          # 4-byte Spill
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movb	%al, 39(%rsp)           # 1-byte Spill
	movl	%edi, 32(%rsp)          # 4-byte Spill
	jne	.LBB16_2
# BB#1:
	callq	trace_logger_init
.LBB16_2:
	movb	39(%rsp), %al           # 1-byte Reload
	movzbl	%al, %ecx
	andl	$1, %ecx
	movb	63(%rsp), %dl           # 1-byte Reload
	movzbl	%dl, %esi
	andl	$1, %esi
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	56(%rsp), %r8d          # 4-byte Reload
	movl	%esi, 28(%rsp)          # 4-byte Spill
	movl	%r8d, %esi
	movl	%ecx, %edx
	movl	28(%rsp), %ecx          # 4-byte Reload
	callq	update_logging_status
	cmpl	$2, current_logging_status
	je	.LBB16_4
# BB#3:
	movq	full_trace_file, %rdi
	movl	inst_count, %eax
	movq	%rsp, %rcx
	movl	%eax, 8(%rcx)
	movl	56(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%rcx)
	movl	$.L.str13125, %edx
	movl	%edx, %esi
	xorl	%edx, %edx
	movb	%dl, %r8b
	movl	32(%rsp), %edx          # 4-byte Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	40(%rsp), %r9           # 8-byte Reload
	movb	%r8b, 27(%rsp)          # 1-byte Spill
	movq	%r9, %r8
	movq	48(%rsp), %r9           # 8-byte Reload
	movb	27(%rsp), %al           # 1-byte Reload
	callq	gzprintf
	movl	inst_count, %edx
	addl	$1, %edx
	movl	%edx, inst_count
	movl	%eax, 20(%rsp)          # 4-byte Spill
.LBB16_4:
	addq	$72, %rsp
	ret
.Ltmp213:
	.size	trace_logger_log0, .Ltmp213-trace_logger_log0
	.cfi_endproc

	.globl	trace_logger_log_int
	.align	16, 0x90
	.type	trace_logger_log_int,@function
trace_logger_log_int:                   # @trace_logger_log_int
	.cfi_startproc
# BB#0:
	subq	$120, %rsp
.Ltmp215:
	.cfi_def_cfa_offset 128
	movq	128(%rsp), %rax
	cmpb	$0, initp
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movl	%r9d, 108(%rsp)         # 4-byte Spill
	movq	%r8, 96(%rsp)           # 8-byte Spill
	movl	%ecx, 92(%rsp)          # 4-byte Spill
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movl	%esi, 76(%rsp)          # 4-byte Spill
	movl	%edi, 72(%rsp)          # 4-byte Spill
	jne	.LBB17_2
# BB#1:
	movl	$.L.str14126, %eax
	movl	%eax, %edi
	movl	$.L.str6118, %eax
	movl	%eax, %esi
	movl	$.L__PRETTY_FUNCTION__.trace_logger_log_int, %eax
	movl	%eax, %ecx
	movl	$204, %edx
	callq	__assert_fail
.LBB17_2:
	callq	do_not_log
	testb	$1, %al
	jne	.LBB17_14
# BB#3:
	movl	72(%rsp), %eax          # 4-byte Reload
	cmpl	$19134, %eax            # imm = 0x4ABE
	jne	.LBB17_5
# BB#4:
	movq	full_trace_file, %rdi
	movl	$.L.str15127, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movl	76(%rsp), %edx          # 4-byte Reload
	movq	80(%rsp), %r8           # 8-byte Reload
	movb	%cl, 71(%rsp)           # 1-byte Spill
	movq	%r8, %rcx
	movl	92(%rsp), %r8d          # 4-byte Reload
	movb	71(%rsp), %al           # 1-byte Reload
	callq	gzprintf
	movl	%eax, 64(%rsp)          # 4-byte Spill
	jmp	.LBB17_8
.LBB17_5:
	movq	full_trace_file, %rax
	movl	72(%rsp), %ecx          # 4-byte Reload
	cmpl	$24601, %ecx            # imm = 0x6019
	movq	%rax, 56(%rsp)          # 8-byte Spill
	jne	.LBB17_7
# BB#6:
	movl	$.L.str16128, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	56(%rsp), %rdi          # 8-byte Reload
	movl	76(%rsp), %edx          # 4-byte Reload
	movq	80(%rsp), %r8           # 8-byte Reload
	movb	%cl, 55(%rsp)           # 1-byte Spill
	movq	%r8, %rcx
	movl	92(%rsp), %r8d          # 4-byte Reload
	movb	55(%rsp), %al           # 1-byte Reload
	callq	gzprintf
	movl	%eax, 48(%rsp)          # 4-byte Spill
	jmp	.LBB17_8
.LBB17_7:
	movl	$.L.str17129, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	56(%rsp), %rdi          # 8-byte Reload
	movl	72(%rsp), %edx          # 4-byte Reload
	movl	76(%rsp), %eax          # 4-byte Reload
	movb	%cl, 47(%rsp)           # 1-byte Spill
	movl	%eax, %ecx
	movq	80(%rsp), %r8           # 8-byte Reload
	movl	92(%rsp), %r9d          # 4-byte Reload
	movb	47(%rsp), %al           # 1-byte Reload
	callq	gzprintf
	movl	%eax, 40(%rsp)          # 4-byte Spill
.LBB17_8:
	movq	full_trace_file, %rax
	movl	92(%rsp), %ecx          # 4-byte Reload
	cmpl	$0, %ecx
	movq	%rax, 32(%rsp)          # 8-byte Spill
	je	.LBB17_10
# BB#9:
	movl	$.L.str18130, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	96(%rsp), %rdx          # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, 28(%rsp)          # 4-byte Spill
	jmp	.LBB17_11
.LBB17_10:
	movl	$.L.str19131, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	32(%rsp), %rdi          # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, 24(%rsp)          # 4-byte Spill
.LBB17_11:
	movq	full_trace_file, %rax
	movl	108(%rsp), %ecx         # 4-byte Reload
	cmpl	$0, %ecx
	movq	%rax, 16(%rsp)          # 8-byte Spill
	je	.LBB17_13
# BB#12:
	movl	$.L.str20132, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	112(%rsp), %rdx         # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, 12(%rsp)          # 4-byte Spill
	jmp	.LBB17_14
.LBB17_13:
	movl	$.L.str21133, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	16(%rsp), %rdi          # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, 8(%rsp)           # 4-byte Spill
.LBB17_14:
	addq	$120, %rsp
	ret
.Ltmp216:
	.size	trace_logger_log_int, .Ltmp216-trace_logger_log_int
	.cfi_endproc

	.globl	trace_logger_log_ptr
	.align	16, 0x90
	.type	trace_logger_log_ptr,@function
trace_logger_log_ptr:                   # @trace_logger_log_ptr
	.cfi_startproc
# BB#0:
	subq	$120, %rsp
.Ltmp218:
	.cfi_def_cfa_offset 128
	movq	128(%rsp), %rax
	cmpb	$0, initp
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movl	%r9d, 108(%rsp)         # 4-byte Spill
	movq	%r8, 96(%rsp)           # 8-byte Spill
	movl	%ecx, 92(%rsp)          # 4-byte Spill
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movl	%esi, 76(%rsp)          # 4-byte Spill
	movl	%edi, 72(%rsp)          # 4-byte Spill
	jne	.LBB18_2
# BB#1:
	movl	$.L.str14126, %eax
	movl	%eax, %edi
	movl	$.L.str6118, %eax
	movl	%eax, %esi
	movl	$.L__PRETTY_FUNCTION__.trace_logger_log_ptr, %eax
	movl	%eax, %ecx
	movl	$225, %edx
	callq	__assert_fail
.LBB18_2:
	callq	do_not_log
	testb	$1, %al
	jne	.LBB18_14
# BB#3:
	movl	72(%rsp), %eax          # 4-byte Reload
	cmpl	$19134, %eax            # imm = 0x4ABE
	jne	.LBB18_5
# BB#4:
	movq	full_trace_file, %rdi
	movl	$.L.str22134, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movl	76(%rsp), %edx          # 4-byte Reload
	movq	80(%rsp), %r8           # 8-byte Reload
	movb	%cl, 71(%rsp)           # 1-byte Spill
	movq	%r8, %rcx
	movl	92(%rsp), %r8d          # 4-byte Reload
	movb	71(%rsp), %al           # 1-byte Reload
	callq	gzprintf
	movl	%eax, 64(%rsp)          # 4-byte Spill
	jmp	.LBB18_8
.LBB18_5:
	movq	full_trace_file, %rax
	movl	72(%rsp), %ecx          # 4-byte Reload
	cmpl	$24601, %ecx            # imm = 0x6019
	movq	%rax, 56(%rsp)          # 8-byte Spill
	jne	.LBB18_7
# BB#6:
	movl	$.L.str23135, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	56(%rsp), %rdi          # 8-byte Reload
	movl	76(%rsp), %edx          # 4-byte Reload
	movq	80(%rsp), %r8           # 8-byte Reload
	movb	%cl, 55(%rsp)           # 1-byte Spill
	movq	%r8, %rcx
	movl	92(%rsp), %r8d          # 4-byte Reload
	movb	55(%rsp), %al           # 1-byte Reload
	callq	gzprintf
	movl	%eax, 48(%rsp)          # 4-byte Spill
	jmp	.LBB18_8
.LBB18_7:
	movl	$.L.str24136, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	56(%rsp), %rdi          # 8-byte Reload
	movl	72(%rsp), %edx          # 4-byte Reload
	movl	76(%rsp), %eax          # 4-byte Reload
	movb	%cl, 47(%rsp)           # 1-byte Spill
	movl	%eax, %ecx
	movq	80(%rsp), %r8           # 8-byte Reload
	movl	92(%rsp), %r9d          # 4-byte Reload
	movb	47(%rsp), %al           # 1-byte Reload
	callq	gzprintf
	movl	%eax, 40(%rsp)          # 4-byte Spill
.LBB18_8:
	movq	full_trace_file, %rax
	movl	92(%rsp), %ecx          # 4-byte Reload
	cmpl	$0, %ecx
	movq	%rax, 32(%rsp)          # 8-byte Spill
	je	.LBB18_10
# BB#9:
	movl	$.L.str18130, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	96(%rsp), %rdx          # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, 28(%rsp)          # 4-byte Spill
	jmp	.LBB18_11
.LBB18_10:
	movl	$.L.str19131, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	32(%rsp), %rdi          # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, 24(%rsp)          # 4-byte Spill
.LBB18_11:
	movq	full_trace_file, %rax
	movl	108(%rsp), %ecx         # 4-byte Reload
	cmpl	$0, %ecx
	movq	%rax, 16(%rsp)          # 8-byte Spill
	je	.LBB18_13
# BB#12:
	movl	$.L.str20132, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	112(%rsp), %rdx         # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, 12(%rsp)          # 4-byte Spill
	jmp	.LBB18_14
.LBB18_13:
	movl	$.L.str21133, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	16(%rsp), %rdi          # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, 8(%rsp)           # 4-byte Spill
.LBB18_14:
	addq	$120, %rsp
	ret
.Ltmp219:
	.size	trace_logger_log_ptr, .Ltmp219-trace_logger_log_ptr
	.cfi_endproc

	.globl	trace_logger_log_double
	.align	16, 0x90
	.type	trace_logger_log_double,@function
trace_logger_log_double:                # @trace_logger_log_double
	.cfi_startproc
# BB#0:
	subq	$104, %rsp
.Ltmp221:
	.cfi_def_cfa_offset 112
	cmpb	$0, initp
	movl	%esi, 100(%rsp)         # 4-byte Spill
	movq	%r9, 88(%rsp)           # 8-byte Spill
	movl	%r8d, 84(%rsp)          # 4-byte Spill
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movl	%edx, 68(%rsp)          # 4-byte Spill
	vmovsd	%xmm0, 56(%rsp)         # 8-byte Spill
	movl	%edi, 52(%rsp)          # 4-byte Spill
	jne	.LBB19_2
# BB#1:
	movl	$.L.str14126, %eax
	movl	%eax, %edi
	movl	$.L.str6118, %eax
	movl	%eax, %esi
	movl	$.L__PRETTY_FUNCTION__.trace_logger_log_double, %eax
	movl	%eax, %ecx
	movl	$246, %edx
	callq	__assert_fail
.LBB19_2:
	callq	do_not_log
	testb	$1, %al
	jne	.LBB19_14
# BB#3:
	movl	52(%rsp), %eax          # 4-byte Reload
	cmpl	$19134, %eax            # imm = 0x4ABE
	jne	.LBB19_5
# BB#4:
	movq	full_trace_file, %rdi
	movl	$.L.str25137, %eax
	movl	%eax, %esi
	movb	$1, %al
	movl	100(%rsp), %edx         # 4-byte Reload
	vmovsd	56(%rsp), %xmm0         # 8-byte Reload
	movl	68(%rsp), %ecx          # 4-byte Reload
	callq	gzprintf
	movl	%eax, 48(%rsp)          # 4-byte Spill
	jmp	.LBB19_8
.LBB19_5:
	movq	full_trace_file, %rax
	movl	52(%rsp), %ecx          # 4-byte Reload
	cmpl	$24601, %ecx            # imm = 0x6019
	movq	%rax, 40(%rsp)          # 8-byte Spill
	jne	.LBB19_7
# BB#6:
	movl	$.L.str26138, %eax
	movl	%eax, %esi
	movb	$1, %al
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	100(%rsp), %edx         # 4-byte Reload
	vmovsd	56(%rsp), %xmm0         # 8-byte Reload
	movl	68(%rsp), %ecx          # 4-byte Reload
	callq	gzprintf
	movl	%eax, 36(%rsp)          # 4-byte Spill
	jmp	.LBB19_8
.LBB19_7:
	movl	$.L.str27139, %eax
	movl	%eax, %esi
	movb	$1, %al
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	52(%rsp), %edx          # 4-byte Reload
	movl	100(%rsp), %ecx         # 4-byte Reload
	vmovsd	56(%rsp), %xmm0         # 8-byte Reload
	movl	68(%rsp), %r8d          # 4-byte Reload
	callq	gzprintf
	movl	%eax, 32(%rsp)          # 4-byte Spill
.LBB19_8:
	movq	full_trace_file, %rax
	movl	68(%rsp), %ecx          # 4-byte Reload
	cmpl	$0, %ecx
	movq	%rax, 24(%rsp)          # 8-byte Spill
	je	.LBB19_10
# BB#9:
	movl	$.L.str18130, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	72(%rsp), %rdx          # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, 20(%rsp)          # 4-byte Spill
	jmp	.LBB19_11
.LBB19_10:
	movl	$.L.str19131, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	24(%rsp), %rdi          # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, 16(%rsp)          # 4-byte Spill
.LBB19_11:
	movq	full_trace_file, %rax
	movl	84(%rsp), %ecx          # 4-byte Reload
	cmpl	$0, %ecx
	movq	%rax, 8(%rsp)           # 8-byte Spill
	je	.LBB19_13
# BB#12:
	movl	$.L.str20132, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	88(%rsp), %rdx          # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, 4(%rsp)           # 4-byte Spill
	jmp	.LBB19_14
.LBB19_13:
	movl	$.L.str21133, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	8(%rsp), %rdi           # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, (%rsp)            # 4-byte Spill
.LBB19_14:
	addq	$104, %rsp
	ret
.Ltmp222:
	.size	trace_logger_log_double, .Ltmp222-trace_logger_log_double
	.cfi_endproc

	.globl	trace_logger_log_vector
	.align	16, 0x90
	.type	trace_logger_log_vector,@function
trace_logger_log_vector:                # @trace_logger_log_vector
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Ltmp225:
	.cfi_def_cfa_offset 16
.Ltmp226:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
.Ltmp227:
	.cfi_def_cfa_register %rbp
	subq	$128, %rsp
	movq	16(%rbp), %rax
	cmpb	$0, initp
	movq	%rax, -8(%rbp)          # 8-byte Spill
	movl	%r9d, -12(%rbp)         # 4-byte Spill
	movq	%r8, -24(%rbp)          # 8-byte Spill
	movl	%ecx, -28(%rbp)         # 4-byte Spill
	movq	%rdx, -40(%rbp)         # 8-byte Spill
	movl	%esi, -44(%rbp)         # 4-byte Spill
	movl	%edi, -48(%rbp)         # 4-byte Spill
	jne	.LBB20_2
# BB#1:
	leaq	.L.str14126, %rdi
	leaq	.L.str6118, %rsi
	movl	$267, %edx              # imm = 0x10B
	leaq	.L__PRETTY_FUNCTION__.trace_logger_log_vector, %rcx
	callq	__assert_fail
.LBB20_2:
	callq	do_not_log
	testb	$1, %al
	jne	.LBB20_15
# BB#3:
	movl	$8, %eax
	movl	-44(%rbp), %ecx         # 4-byte Reload
	sarl	$31, %ecx
	shrl	$30, %ecx
	movl	-44(%rbp), %edx         # 4-byte Reload
	addl	%ecx, %edx
	sarl	$2, %edx
	addl	$3, %edx
	movl	%edx, %esi
	movq	%rsp, %rdi
	addq	$15, %rsi
	andq	$-16, %rsi
	movq	%rsp, %r8
	subq	%rsi, %r8
	movq	%r8, %rsp
	movl	-44(%rbp), %ecx         # 4-byte Reload
	movl	%eax, -52(%rbp)         # 4-byte Spill
	movl	%ecx, %eax
	cltd
	movl	-52(%rbp), %r9d         # 4-byte Reload
	idivl	%r9d
	movq	%rdi, -64(%rbp)         # 8-byte Spill
	movq	%r8, %rdi
	movq	-40(%rbp), %rsi         # 8-byte Reload
	movl	%eax, %edx
	movq	%r8, -72(%rbp)          # 8-byte Spill
	callq	convert_bytes_to_hex
	movl	-48(%rbp), %eax         # 4-byte Reload
	cmpl	$19134, %eax            # imm = 0x4ABE
	jne	.LBB20_5
# BB#4:
	leaq	.L.str28140, %rsi
	movq	full_trace_file, %rdi
	movl	-44(%rbp), %edx         # 4-byte Reload
	movq	-72(%rbp), %rcx         # 8-byte Reload
	movl	-28(%rbp), %r8d         # 4-byte Reload
	movb	$0, %al
	callq	gzprintf
	movl	%eax, -76(%rbp)         # 4-byte Spill
	jmp	.LBB20_8
.LBB20_5:
	movq	full_trace_file, %rax
	movl	-48(%rbp), %ecx         # 4-byte Reload
	cmpl	$24601, %ecx            # imm = 0x6019
	movq	%rax, -88(%rbp)         # 8-byte Spill
	jne	.LBB20_7
# BB#6:
	leaq	.L.str29141, %rsi
	movq	-88(%rbp), %rdi         # 8-byte Reload
	movl	-44(%rbp), %edx         # 4-byte Reload
	movq	-72(%rbp), %rcx         # 8-byte Reload
	movl	-28(%rbp), %r8d         # 4-byte Reload
	movb	$0, %al
	callq	gzprintf
	movl	%eax, -92(%rbp)         # 4-byte Spill
	jmp	.LBB20_8
.LBB20_7:
	leaq	.L.str30142, %rsi
	movq	-88(%rbp), %rdi         # 8-byte Reload
	movl	-48(%rbp), %edx         # 4-byte Reload
	movl	-44(%rbp), %ecx         # 4-byte Reload
	movq	-72(%rbp), %r8          # 8-byte Reload
	movl	-28(%rbp), %r9d         # 4-byte Reload
	movb	$0, %al
	callq	gzprintf
	movl	%eax, -96(%rbp)         # 4-byte Spill
.LBB20_8:
	movq	full_trace_file, %rax
	movl	-28(%rbp), %ecx         # 4-byte Reload
	cmpl	$0, %ecx
	movq	%rax, -104(%rbp)        # 8-byte Spill
	je	.LBB20_10
# BB#9:
	leaq	.L.str18130, %rsi
	movq	-104(%rbp), %rdi        # 8-byte Reload
	movq	-24(%rbp), %rdx         # 8-byte Reload
	movb	$0, %al
	callq	gzprintf
	movl	%eax, -108(%rbp)        # 4-byte Spill
	jmp	.LBB20_11
.LBB20_10:
	leaq	.L.str19131, %rsi
	movq	-104(%rbp), %rdi        # 8-byte Reload
	movb	$0, %al
	callq	gzprintf
	movl	%eax, -112(%rbp)        # 4-byte Spill
.LBB20_11:
	movq	full_trace_file, %rax
	movl	-12(%rbp), %ecx         # 4-byte Reload
	cmpl	$0, %ecx
	movq	%rax, -120(%rbp)        # 8-byte Spill
	je	.LBB20_13
# BB#12:
	leaq	.L.str20132, %rsi
	movq	-120(%rbp), %rdi        # 8-byte Reload
	movq	-8(%rbp), %rdx          # 8-byte Reload
	movb	$0, %al
	callq	gzprintf
	movl	%eax, -124(%rbp)        # 4-byte Spill
	jmp	.LBB20_14
.LBB20_13:
	leaq	.L.str21133, %rsi
	movq	-120(%rbp), %rdi        # 8-byte Reload
	movb	$0, %al
	callq	gzprintf
	movl	%eax, -128(%rbp)        # 4-byte Spill
.LBB20_14:
	movq	-64(%rbp), %rax         # 8-byte Reload
	movq	%rax, %rsp
.LBB20_15:
	movq	%rbp, %rsp
	popq	%rbp
	ret
.Ltmp228:
	.size	trace_logger_log_vector, .Ltmp228-trace_logger_log_vector
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"FAILED: store_loc[%d] = %d, should be %d\n"
	.size	.L.str, 42

	.type	.L.str2,@object         # @.str2
.L.str2:
	.asciz	"err == 0 && \"Failed to allocate memory!\""
	.size	.L.str2, 41

	.type	.L.str3,@object         # @.str3
.L.str3:
	.asciz	"store_kernel.c"
	.size	.L.str3, 15

	.type	.L__PRETTY_FUNCTION__.main,@object # @__PRETTY_FUNCTION__.main
.L__PRETTY_FUNCTION__.main:
	.asciz	"int main()"
	.size	.L__PRETTY_FUNCTION__.main, 11

	.type	.L.str4,@object         # @.str4
.L.str4:
	.asciz	"Test failed with %d errors."
	.size	.L.str4, 28

	.type	.L.str5,@object         # @.str5
.L.str5:
	.asciz	"Test passed!\n"
	.size	.L.str5, 14

	.type	.L.str1,@object         # @.str1
	.section	.rodata,"a",@progbits
.L.str1:
	.asciz	"store_kernel"
	.size	.L.str1, 13

	.type	.L.str6,@object         # @.str6
.L.str6:
	.asciz	"0:0"
	.size	.L.str6, 4

	.type	.L.str7,@object         # @.str7
.L.str7:
	.asciz	"1"
	.size	.L.str7, 2

	.type	.L.str8,@object         # @.str8
.L.str8:
	.asciz	"store_vals"
	.size	.L.str8, 11

	.type	.L.str9,@object         # @.str9
.L.str9:
	.asciz	"phi"
	.size	.L.str9, 4

	.type	.L.str10,@object        # @.str10
.L.str10:
	.asciz	"2"
	.size	.L.str10, 2

	.type	.L.str11,@object        # @.str11
.L.str11:
	.asciz	"num_vals"
	.size	.L.str11, 9

	.type	.L.str12,@object        # @.str12
.L.str12:
	.asciz	"3"
	.size	.L.str12, 2

	.type	.L.str13,@object        # @.str13
.L.str13:
	.zero	1
	.size	.L.str13, 1

	.type	.L.str14,@object        # @.str14
.L.str14:
	.asciz	"4"
	.size	.L.str14, 2

	.type	.L.str15,@object        # @.str15
.L.str15:
	.asciz	"dmaLoad"
	.size	.L.str15, 8

	.type	.L.str16,@object        # @.str16
.L.str16:
	.asciz	"5"
	.size	.L.str16, 2

	.type	.L.str17,@object        # @.str17
.L.str17:
	.asciz	"store_loc"
	.size	.L.str17, 10

	.type	.L.str18,@object        # @.str18
.L.str18:
	.asciz	"6"
	.size	.L.str18, 2

	.type	.L.str19,@object        # @.str19
.L.str19:
	.asciz	"7"
	.size	.L.str19, 2

	.type	.L.str20,@object        # @.str20
.L.str20:
	.asciz	"0:0-4"
	.size	.L.str20, 6

	.type	.L.str21,@object        # @.str21
.L.str21:
	.asciz	".lr.ph:1"
	.size	.L.str21, 9

	.type	.L.str22,@object        # @.str22
.L.str22:
	.asciz	"._crit_edge:0"
	.size	.L.str22, 14

	.type	.L.str23,@object        # @.str23
.L.str23:
	.asciz	"indvars.iv"
	.size	.L.str23, 11

	.type	.L.str24,@object        # @.str24
.L.str24:
	.asciz	"indvars.iv.next"
	.size	.L.str24, 16

	.type	.L.str25,@object        # @.str25
.L.str25:
	.asciz	"8"
	.size	.L.str25, 2

	.type	.L.str26,@object        # @.str26
.L.str26:
	.asciz	"9"
	.size	.L.str26, 2

	.type	.L.str27,@object        # @.str27
.L.str27:
	.asciz	"10"
	.size	.L.str27, 3

	.type	.L.str28,@object        # @.str28
.L.str28:
	.asciz	".lr.ph:1-0"
	.size	.L.str28, 11

	.type	.L.str29,@object        # @.str29
.L.str29:
	.asciz	"lftr.wideiv"
	.size	.L.str29, 12

	.type	.L.str30,@object        # @.str30
.L.str30:
	.asciz	"exitcond"
	.size	.L.str30, 9

	.type	.L.str31,@object        # @.str31
.L.str31:
	.asciz	".lr.ph:1-1"
	.size	.L.str31, 11

	.type	.L.str32,@object        # @.str32
.L.str32:
	.asciz	"11"
	.size	.L.str32, 3

	.type	.L.str33,@object        # @.str33
.L.str33:
	.asciz	"dmaStore"
	.size	.L.str33, 9

	.type	.L.str34,@object        # @.str34
.L.str34:
	.asciz	"._crit_edge:0-0"
	.size	.L.str34, 16

	.type	.L.str35,@object        # @.str35
.L.str35:
	.asciz	"test_stores"
	.size	.L.str35, 12

	.type	.L.str36,@object        # @.str36
.L.str36:
	.asciz	"0:0-5"
	.size	.L.str36, 6

	.type	.L.str37,@object        # @.str37
.L.str37:
	.asciz	"12:1"
	.size	.L.str37, 5

	.type	.L.str38,@object        # @.str38
.L.str38:
	.asciz	"num_failures.01"
	.size	.L.str38, 16

	.type	.L.str39,@object        # @.str39
.L.str39:
	.asciz	"num_failures.1"
	.size	.L.str39, 15

	.type	.L.str40,@object        # @.str40
.L.str40:
	.asciz	"7:1"
	.size	.L.str40, 4

	.type	.L.str41,@object        # @.str41
.L.str41:
	.asciz	"stdout"
	.size	.L.str41, 7

	.type	.L.str42,@object        # @.str42
.L.str42:
	.asciz	"fprintf"
	.size	.L.str42, 8

	.type	.L.str43,@object        # @.str43
.L.str43:
	.asciz	"7:1-1"
	.size	.L.str43, 6

	.type	.L.str44,@object        # @.str44
.L.str44:
	.asciz	"12:1-0"
	.size	.L.str44, 7

	.type	.L.str45,@object        # @.str45
	.align	16
.L.str45:
	.asciz	"num_failures.0.lcssa"
	.size	.L.str45, 21

	.type	.L.str46,@object        # @.str46
.L.str46:
	.asciz	"main"
	.size	.L.str46, 5

	.type	.L.str47,@object        # @.str47
.L.str47:
	.asciz	"posix_memalign"
	.size	.L.str47, 15

	.type	.L.str48,@object        # @.str48
.L.str48:
	.asciz	"5:0"
	.size	.L.str48, 4

	.type	.L.str49,@object        # @.str49
.L.str49:
	.asciz	"4:0"
	.size	.L.str49, 4

	.type	.L.str50,@object        # @.str50
.L.str50:
	.asciz	"4:0-0"
	.size	.L.str50, 6

	.type	.L.str51,@object        # @.str51
.L.str51:
	.asciz	"__assert_fail"
	.size	.L.str51, 14

	.type	.L.str52,@object        # @.str52
.L.str52:
	.asciz	"4:0-1"
	.size	.L.str52, 6

	.type	.L.str53,@object        # @.str53
.L.str53:
	.asciz	"5:0-1"
	.size	.L.str53, 6

	.type	.L.str54,@object        # @.str54
.L.str54:
	.asciz	".preheader:0"
	.size	.L.str54, 13

	.type	.L.str55,@object        # @.str55
.L.str55:
	.asciz	"11:0"
	.size	.L.str55, 5

	.type	.L.str56,@object        # @.str56
.L.str56:
	.asciz	".preheader:0-2"
	.size	.L.str56, 15

	.type	.L.str57,@object        # @.str57
.L.str57:
	.asciz	"11:0-0"
	.size	.L.str57, 7

	.type	.L.str58,@object        # @.str58
.L.str58:
	.asciz	"11:0-1"
	.size	.L.str58, 7

	.type	.L.str59,@object        # @.str59
.L.str59:
	.asciz	"13"
	.size	.L.str59, 3

	.type	.L.str60,@object        # @.str60
.L.str60:
	.asciz	"14"
	.size	.L.str60, 3

	.type	.L.str61,@object        # @.str61
.L.str61:
	.asciz	"12:1-1"
	.size	.L.str61, 7

	.type	.L.str62,@object        # @.str62
.L.str62:
	.asciz	"15"
	.size	.L.str62, 3

	.type	.L.str63,@object        # @.str63
.L.str63:
	.asciz	"12:1-3"
	.size	.L.str63, 7

	.type	.L.str64,@object        # @.str64
.L.str64:
	.asciz	"12:1-4"
	.size	.L.str64, 7

	.type	.L.str65,@object        # @.str65
.L.str65:
	.asciz	"16:0"
	.size	.L.str65, 5

	.type	.L.str66,@object        # @.str66
.L.str66:
	.asciz	"17"
	.size	.L.str66, 3

	.type	.L.str67,@object        # @.str67
.L.str67:
	.asciz	"18"
	.size	.L.str67, 3

	.type	.L.str68,@object        # @.str68
.L.str68:
	.asciz	"16:0-2"
	.size	.L.str68, 7

	.type	.L.str69,@object        # @.str69
.L.str69:
	.asciz	"19"
	.size	.L.str69, 3

	.type	.L.str70,@object        # @.str70
.L.str70:
	.asciz	"20"
	.size	.L.str70, 3

	.type	.L.str71,@object        # @.str71
.L.str71:
	.asciz	"21"
	.size	.L.str71, 3

	.type	.L.str72,@object        # @.str72
.L.str72:
	.asciz	"22"
	.size	.L.str72, 3

	.type	.L.str73,@object        # @.str73
.L.str73:
	.asciz	"23"
	.size	.L.str73, 3

	.type	.L.str74,@object        # @.str74
.L.str74:
	.asciz	"16:0-6"
	.size	.L.str74, 7

	.type	.L.str75,@object        # @.str75
.L.str75:
	.asciz	"26:0"
	.size	.L.str75, 5

	.type	.L.str76,@object        # @.str76
.L.str76:
	.asciz	"24:0"
	.size	.L.str76, 5

	.type	.L.str77,@object        # @.str77
.L.str77:
	.asciz	"25"
	.size	.L.str77, 3

	.type	.L.str78,@object        # @.str78
.L.str78:
	.asciz	"24:0-0"
	.size	.L.str78, 7

	.type	.L.str79,@object        # @.str79
.L.str79:
	.asciz	"28:0"
	.size	.L.str79, 5

	.type	.L.str80,@object        # @.str80
.L.str80:
	.asciz	"27"
	.size	.L.str80, 3

	.type	.L.str81,@object        # @.str81
.L.str81:
	.asciz	"26:0-0"
	.size	.L.str81, 7

	.type	.L.str82,@object        # @.str82
.L.str82:
	.asciz	".0"
	.size	.L.str82, 3

	.type	.L.str83,@object        # @.str83
.L.str83:
	.asciz	"28:0-0"
	.size	.L.str83, 7

	.type	.L.str84,@object        # @.str84
	.align	16
.L.str84:
	.asciz	"store_kernel/loop 30\n"
	.size	.L.str84, 22

	.type	.L.str85,@object        # @.str85
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str85:
	.asciz	"size > 0"
	.size	.L.str85, 9

	.type	.L.str186,@object       # @.str186
.L.str186:
	.asciz	"/workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c"
	.size	.L.str186, 57

	.type	.L__PRETTY_FUNCTION__._dmaImpl3,@object # @__PRETTY_FUNCTION__._dmaImpl3
.L__PRETTY_FUNCTION__._dmaImpl3:
	.asciz	"int _dmaImpl3(void *, void *, size_t)"
	.size	.L__PRETTY_FUNCTION__._dmaImpl3, 38

	.type	.L.str287,@object       # @.str287
	.section	.rodata,"a",@progbits
.L.str287:
	.asciz	"_dmaImpl3"
	.size	.L.str287, 10

	.type	.L.str388,@object       # @.str388
.L.str388:
	.asciz	"0:0"
	.size	.L.str388, 4

	.type	.L.str489,@object       # @.str489
.L.str489:
	.asciz	"1"
	.size	.L.str489, 2

	.type	.L.str590,@object       # @.str590
.L.str590:
	.zero	1
	.size	.L.str590, 1

	.type	.L.str691,@object       # @.str691
.L.str691:
	.asciz	"phi"
	.size	.L.str691, 4

	.type	.L.str792,@object       # @.str792
.L.str792:
	.asciz	"size"
	.size	.L.str792, 5

	.type	.L.str893,@object       # @.str893
.L.str893:
	.asciz	"0:0-3"
	.size	.L.str893, 6

	.type	.L.str994,@object       # @.str994
.L.str994:
	.asciz	"2:0"
	.size	.L.str994, 4

	.type	.L.str1095,@object      # @.str1095
.L.str1095:
	.asciz	"3:0"
	.size	.L.str1095, 4

	.type	.L.str1196,@object      # @.str1196
.L.str1196:
	.asciz	"2:0-0"
	.size	.L.str1196, 6

	.type	.L.str1297,@object      # @.str1297
.L.str1297:
	.asciz	"__assert_fail"
	.size	.L.str1297, 14

	.type	.L.str1398,@object      # @.str1398
.L.str1398:
	.asciz	"2:0-1"
	.size	.L.str1398, 6

	.type	.L.str1499,@object      # @.str1499
.L.str1499:
	.asciz	"4"
	.size	.L.str1499, 2

	.type	.L.str15100,@object     # @.str15100
.L.str15100:
	.asciz	"memmove"
	.size	.L.str15100, 8

	.type	.L.str16101,@object     # @.str16101
.L.str16101:
	.asciz	"dst_addr"
	.size	.L.str16101, 9

	.type	.L.str17102,@object     # @.str17102
.L.str17102:
	.asciz	"src_addr"
	.size	.L.str17102, 9

	.type	.L.str18103,@object     # @.str18103
.L.str18103:
	.asciz	"3:0-0"
	.size	.L.str18103, 6

	.type	.L.str19104,@object     # @.str19104
.L.str19104:
	.asciz	"src_host_addr"
	.size	.L.str19104, 14

	.type	.L.str20105,@object     # @.str20105
.L.str20105:
	.asciz	"dst_host_addr"
	.size	.L.str20105, 14

	.type	.L.str21106,@object     # @.str21106
.L.str21106:
	.asciz	"start_addr"
	.size	.L.str21106, 11

	.type	.L.str22107,@object     # @.str22107
.L.str22107:
	.asciz	"value"
	.size	.L.str22107, 6

	.type	initp,@object           # @initp
	.bss
	.globl	initp
initp:
	.byte	0                       # 0x0
	.size	initp, 1

	.type	inst_count,@object      # @inst_count
	.globl	inst_count
	.align	4
inst_count:
	.long	0                       # 0x0
	.size	inst_count, 4

	.type	.L.str112,@object       # @.str112
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str112:
	.asciz	"%%%% LABEL MAP START %%%%\n"
	.size	.L.str112, 27

	.type	.L.str1113,@object      # @.str1113
.L.str1113:
	.asciz	"%%%% LABEL MAP END %%%%\n\n"
	.size	.L.str1113, 26

	.type	full_trace_file,@object # @full_trace_file
	.comm	full_trace_file,8,8
	.type	.L.str2114,@object      # @.str2114
.L.str2114:
	.asciz	"dynamic_trace.gz"
	.size	.L.str2114, 17

	.type	.L.str3115,@object      # @.str3115
.L.str3115:
	.asciz	"w"
	.size	.L.str3115, 2

	.type	.L.str4116,@object      # @.str4116
.L.str4116:
	.asciz	"Failed to open logfile \"dynamic_trace\""
	.size	.L.str4116, 39

	.type	current_toplevel_function,@object # @current_toplevel_function
	.comm	current_toplevel_function,8,8
	.type	current_logging_status,@object # @current_logging_status
	.comm	current_logging_status,4,4
	.type	.L.str5117,@object      # @.str5117
.L.str5117:
	.asciz	"false && \"Returning from within a toplevel function before it was called!\""
	.size	.L.str5117, 75

	.type	.L.str6118,@object      # @.str6118
.L.str6118:
	.asciz	"/workspace/LLVM-Tracer/profile-func/trace_logger.c"
	.size	.L.str6118, 51

	.type	.L__PRETTY_FUNCTION__.log_or_not,@object # @__PRETTY_FUNCTION__.log_or_not
.L__PRETTY_FUNCTION__.log_or_not:
	.asciz	"logging_status log_or_not(_Bool, _Bool, int, char *)"
	.size	.L__PRETTY_FUNCTION__.log_or_not, 53

	.type	.L.str7119,@object      # @.str7119
.L.str7119:
	.asciz	"false && \"Cannot call a top level function from within another one!\""
	.size	.L.str7119, 69

	.type	.L.str8120,@object      # @.str8120
.L.str8120:
	.asciz	"0x"
	.size	.L.str8120, 3

	.type	.L.str9121,@object      # @.str9121
.L.str9121:
	.asciz	"%02x"
	.size	.L.str9121, 5

	.type	.L.str10122,@object     # @.str10122
.L.str10122:
	.asciz	"Stopping logging at inst %d.\n"
	.size	.L.str10122, 30

	.type	.L.str11123,@object     # @.str11123
.L.str11123:
	.asciz	"Starting to log at inst = %d.\n"
	.size	.L.str11123, 31

	.type	.L.str12124,@object     # @.str12124
.L.str12124:
	.asciz	"\nentry,%s,%d,\n"
	.size	.L.str12124, 15

	.type	.L.str13125,@object     # @.str13125
.L.str13125:
	.asciz	"\n0,%d,%s,%s,%s,%d,%d\n"
	.size	.L.str13125, 22

	.type	.L.str14126,@object     # @.str14126
.L.str14126:
	.asciz	"initp == true"
	.size	.L.str14126, 14

	.type	.L__PRETTY_FUNCTION__.trace_logger_log_int,@object # @__PRETTY_FUNCTION__.trace_logger_log_int
.L__PRETTY_FUNCTION__.trace_logger_log_int:
	.asciz	"void trace_logger_log_int(int, int, int64_t, int, char *, int, char *)"
	.size	.L__PRETTY_FUNCTION__.trace_logger_log_int, 71

	.type	.L.str15127,@object     # @.str15127
.L.str15127:
	.asciz	"r,%d,%ld,%d"
	.size	.L.str15127, 12

	.type	.L.str16128,@object     # @.str16128
.L.str16128:
	.asciz	"f,%d,%ld,%d"
	.size	.L.str16128, 12

	.type	.L.str17129,@object     # @.str17129
.L.str17129:
	.asciz	"%d,%d,%ld,%d"
	.size	.L.str17129, 13

	.type	.L.str18130,@object     # @.str18130
.L.str18130:
	.asciz	",%s"
	.size	.L.str18130, 4

	.type	.L.str19131,@object     # @.str19131
.L.str19131:
	.asciz	", "
	.size	.L.str19131, 3

	.type	.L.str20132,@object     # @.str20132
.L.str20132:
	.asciz	",%s,\n"
	.size	.L.str20132, 6

	.type	.L.str21133,@object     # @.str21133
.L.str21133:
	.asciz	",\n"
	.size	.L.str21133, 3

	.type	.L__PRETTY_FUNCTION__.trace_logger_log_ptr,@object # @__PRETTY_FUNCTION__.trace_logger_log_ptr
.L__PRETTY_FUNCTION__.trace_logger_log_ptr:
	.asciz	"void trace_logger_log_ptr(int, int, uint64_t, int, char *, int, char *)"
	.size	.L__PRETTY_FUNCTION__.trace_logger_log_ptr, 72

	.type	.L.str22134,@object     # @.str22134
.L.str22134:
	.asciz	"r,%d,%#llx,%d"
	.size	.L.str22134, 14

	.type	.L.str23135,@object     # @.str23135
.L.str23135:
	.asciz	"f,%d,%#llx,%d"
	.size	.L.str23135, 14

	.type	.L.str24136,@object     # @.str24136
.L.str24136:
	.asciz	"%d,%d,%#llx,%d"
	.size	.L.str24136, 15

	.type	.L__PRETTY_FUNCTION__.trace_logger_log_double,@object # @__PRETTY_FUNCTION__.trace_logger_log_double
.L__PRETTY_FUNCTION__.trace_logger_log_double:
	.asciz	"void trace_logger_log_double(int, int, double, int, char *, int, char *)"
	.size	.L__PRETTY_FUNCTION__.trace_logger_log_double, 73

	.type	.L.str25137,@object     # @.str25137
.L.str25137:
	.asciz	"r,%d,%f,%d"
	.size	.L.str25137, 11

	.type	.L.str26138,@object     # @.str26138
.L.str26138:
	.asciz	"f,%d,%f,%d"
	.size	.L.str26138, 11

	.type	.L.str27139,@object     # @.str27139
.L.str27139:
	.asciz	"%d,%d,%f,%d"
	.size	.L.str27139, 12

	.type	.L__PRETTY_FUNCTION__.trace_logger_log_vector,@object # @__PRETTY_FUNCTION__.trace_logger_log_vector
.L__PRETTY_FUNCTION__.trace_logger_log_vector:
	.asciz	"void trace_logger_log_vector(int, int, uint8_t *, int, char *, int, char *)"
	.size	.L__PRETTY_FUNCTION__.trace_logger_log_vector, 76

	.type	.L.str28140,@object     # @.str28140
.L.str28140:
	.asciz	"r,%d,%s,%d"
	.size	.L.str28140, 11

	.type	.L.str29141,@object     # @.str29141
.L.str29141:
	.asciz	"f,%d,%s,%d"
	.size	.L.str29141, 11

	.type	.L.str30142,@object     # @.str30142
.L.str30142:
	.asciz	"%d,%d,%s,%d"
	.size	.L.str30142, 12

	.text
.Ldebug_end0:
	.section	.debug_str,"MS",@progbits,1
.Linfo_string0:
	.asciz	"clang version 3.4.2 (tags/RELEASE_34/dot2-final)"
.Linfo_string1:
	.asciz	"store_kernel.c"
.Linfo_string2:
	.asciz	"/workspace/gem5-aladdin/src/aladdin/integration-test/with-cpu/test_dma_load_store/store_kernel"
.Linfo_string3:
	.asciz	"num_vals"
.Linfo_string4:
	.asciz	"int"
.Linfo_string5:
	.asciz	"store_kernel"
.Linfo_string6:
	.asciz	"test_stores"
.Linfo_string7:
	.asciz	"main"
.Linfo_string8:
	.asciz	"/workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c"
.Linfo_string9:
	.asciz	"_dmaImpl3"
.Linfo_string10:
	.asciz	"dmaLoad"
.Linfo_string11:
	.asciz	"dmaStore"
.Linfo_string12:
	.asciz	"setReadyBits"
.Linfo_string13:
	.asciz	"dmaFence"
.Linfo_string14:
	.asciz	"store_vals"
.Linfo_string15:
	.asciz	"store_loc"
.Linfo_string16:
	.asciz	"i"
.Linfo_string17:
	.asciz	"num_failures"
.Linfo_string18:
	.asciz	"err"
.Linfo_string19:
	.asciz	"dst_addr"
.Linfo_string20:
	.asciz	"src_addr"
.Linfo_string21:
	.asciz	"size"
.Linfo_string22:
	.asciz	"long unsigned int"
.Linfo_string23:
	.asciz	"size_t"
.Linfo_string24:
	.asciz	"src_host_addr"
.Linfo_string25:
	.asciz	"dst_host_addr"
.Linfo_string26:
	.asciz	"start_addr"
.Linfo_string27:
	.asciz	"value"
.Linfo_string28:
	.asciz	"unsigned int"
	.section	.debug_info,"",@progbits
.L.debug_info_begin0:
	.long	406                     # Length of Unit
	.short	4                       # DWARF version number
	.long	.L.debug_abbrev_begin   # Offset Into Abbrev. Section
	.byte	8                       # Address Size (in bytes)
	.byte	1                       # Abbrev [1] 0xb:0x18f DW_TAG_compile_unit
	.long	.Linfo_string0          # DW_AT_producer
	.short	12                      # DW_AT_language
	.long	.Linfo_string1          # DW_AT_name
	.quad	0                       # DW_AT_low_pc
	.long	.Lsection_line          # DW_AT_stmt_list
	.long	.Linfo_string2          # DW_AT_comp_dir
                                        # DW_AT_APPLE_optimized
	.byte	2                       # Abbrev [2] 0x26:0xd DW_TAG_variable
	.long	.Linfo_string3          # DW_AT_name
	.long	51                      # DW_AT_type
	.byte	1                       # DW_AT_decl_file
	.byte	52                      # DW_AT_decl_line
	.ascii	"\200\b"                # DW_AT_const_value
	.byte	3                       # Abbrev [3] 0x33:0x5 DW_TAG_const_type
	.long	56                      # DW_AT_type
	.byte	4                       # Abbrev [4] 0x38:0x7 DW_TAG_base_type
	.long	.Linfo_string4          # DW_AT_name
	.byte	5                       # DW_AT_encoding
	.byte	4                       # DW_AT_byte_size
	.byte	5                       # Abbrev [5] 0x3f:0x65 DW_TAG_subprogram
	.long	.Linfo_string5          # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	24                      # DW_AT_decl_line
                                        # DW_AT_prototyped
                                        # DW_AT_external
	.quad	.Lfunc_begin0           # DW_AT_low_pc
	.quad	.Lfunc_end0             # DW_AT_high_pc
	.byte	1                       # DW_AT_frame_base
	.byte	87
                                        # DW_AT_APPLE_omit_frame_ptr
	.byte	6                       # Abbrev [6] 0x58:0xf DW_TAG_formal_parameter
	.long	.Linfo_string14         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	24                      # DW_AT_decl_line
	.long	404                     # DW_AT_type
	.long	.Ldebug_loc0            # DW_AT_location
	.byte	6                       # Abbrev [6] 0x67:0xf DW_TAG_formal_parameter
	.long	.Linfo_string15         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	24                      # DW_AT_decl_line
	.long	404                     # DW_AT_type
	.long	.Ldebug_loc2            # DW_AT_location
	.byte	6                       # Abbrev [6] 0x76:0xf DW_TAG_formal_parameter
	.long	.Linfo_string3          # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	24                      # DW_AT_decl_line
	.long	56                      # DW_AT_type
	.long	.Ldebug_loc4            # DW_AT_location
	.byte	7                       # Abbrev [7] 0x85:0x1e DW_TAG_lexical_block
	.quad	.Ltmp22                 # DW_AT_low_pc
	.quad	.Ltmp25                 # DW_AT_high_pc
	.byte	8                       # Abbrev [8] 0x96:0xc DW_TAG_variable
	.long	.Linfo_string16         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	30                      # DW_AT_decl_line
	.long	56                      # DW_AT_type
	.byte	0                       # DW_AT_const_value
	.byte	0                       # End Of Children Mark
	.byte	0                       # End Of Children Mark
	.byte	9                       # Abbrev [9] 0xa4:0x78 DW_TAG_subprogram
	.long	.Linfo_string6          # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	38                      # DW_AT_decl_line
                                        # DW_AT_prototyped
	.long	56                      # DW_AT_type
                                        # DW_AT_external
	.quad	.Lfunc_begin1           # DW_AT_low_pc
	.quad	.Lfunc_end1             # DW_AT_high_pc
	.byte	1                       # DW_AT_frame_base
	.byte	87
                                        # DW_AT_APPLE_omit_frame_ptr
	.byte	6                       # Abbrev [6] 0xc1:0xf DW_TAG_formal_parameter
	.long	.Linfo_string14         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	38                      # DW_AT_decl_line
	.long	404                     # DW_AT_type
	.long	.Ldebug_loc6            # DW_AT_location
	.byte	6                       # Abbrev [6] 0xd0:0xf DW_TAG_formal_parameter
	.long	.Linfo_string15         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	38                      # DW_AT_decl_line
	.long	404                     # DW_AT_type
	.long	.Ldebug_loc8            # DW_AT_location
	.byte	6                       # Abbrev [6] 0xdf:0xf DW_TAG_formal_parameter
	.long	.Linfo_string3          # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	38                      # DW_AT_decl_line
	.long	56                      # DW_AT_type
	.long	.Ldebug_loc10           # DW_AT_location
	.byte	10                      # Abbrev [10] 0xee:0xf DW_TAG_variable
	.long	.Linfo_string17         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	39                      # DW_AT_decl_line
	.long	56                      # DW_AT_type
	.long	.Ldebug_loc12           # DW_AT_location
	.byte	7                       # Abbrev [7] 0xfd:0x1e DW_TAG_lexical_block
	.quad	.Ltmp49                 # DW_AT_low_pc
	.quad	.Ltmp55                 # DW_AT_high_pc
	.byte	8                       # Abbrev [8] 0x10e:0xc DW_TAG_variable
	.long	.Linfo_string16         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	40                      # DW_AT_decl_line
	.long	56                      # DW_AT_type
	.byte	0                       # DW_AT_const_value
	.byte	0                       # End Of Children Mark
	.byte	0                       # End Of Children Mark
	.byte	11                      # Abbrev [11] 0x11c:0x78 DW_TAG_subprogram
	.long	.Linfo_string7          # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	51                      # DW_AT_decl_line
	.long	56                      # DW_AT_type
                                        # DW_AT_external
	.quad	.Lfunc_begin2           # DW_AT_low_pc
	.quad	.Lfunc_end2             # DW_AT_high_pc
	.byte	1                       # DW_AT_frame_base
	.byte	87
                                        # DW_AT_APPLE_omit_frame_ptr
	.byte	12                      # Abbrev [12] 0x139:0xf DW_TAG_variable
	.long	.Linfo_string14         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	53                      # DW_AT_decl_line
	.long	404                     # DW_AT_type
	.byte	3                       # DW_AT_location
	.byte	145
	.ascii	"\220\027"
	.byte	12                      # Abbrev [12] 0x148:0xf DW_TAG_variable
	.long	.Linfo_string15         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	53                      # DW_AT_decl_line
	.long	404                     # DW_AT_type
	.byte	3                       # DW_AT_location
	.byte	145
	.ascii	"\210\027"
	.byte	8                       # Abbrev [8] 0x157:0xd DW_TAG_variable
	.long	.Linfo_string3          # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	52                      # DW_AT_decl_line
	.long	51                      # DW_AT_type
	.ascii	"\200\b"                # DW_AT_const_value
	.byte	10                      # Abbrev [10] 0x164:0xf DW_TAG_variable
	.long	.Linfo_string18         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	54                      # DW_AT_decl_line
	.long	56                      # DW_AT_type
	.long	.Ldebug_loc15           # DW_AT_location
	.byte	10                      # Abbrev [10] 0x173:0xf DW_TAG_variable
	.long	.Linfo_string17         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	81                      # DW_AT_decl_line
	.long	56                      # DW_AT_type
	.long	.Ldebug_loc18           # DW_AT_location
	.byte	13                      # Abbrev [13] 0x182:0x11 DW_TAG_lexical_block
	.long	.Ldebug_range           # DW_AT_ranges
	.byte	14                      # Abbrev [14] 0x187:0xb DW_TAG_variable
	.long	.Linfo_string16         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	60                      # DW_AT_decl_line
	.long	56                      # DW_AT_type
	.byte	0                       # End Of Children Mark
	.byte	0                       # End Of Children Mark
	.byte	15                      # Abbrev [15] 0x194:0x5 DW_TAG_pointer_type
	.long	56                      # DW_AT_type
	.byte	0                       # End Of Children Mark
.L.debug_info_end0:
.L.debug_info_begin1:
	.long	525                     # Length of Unit
	.short	4                       # DWARF version number
	.long	.L.debug_abbrev_begin   # Offset Into Abbrev. Section
	.byte	8                       # Address Size (in bytes)
	.byte	1                       # Abbrev [1] 0xb:0x206 DW_TAG_compile_unit
	.long	.Linfo_string0          # DW_AT_producer
	.short	12                      # DW_AT_language
	.long	.Linfo_string8          # DW_AT_name
	.quad	0                       # DW_AT_low_pc
	.long	.Lsection_line          # DW_AT_stmt_list
	.long	.Linfo_string2          # DW_AT_comp_dir
                                        # DW_AT_APPLE_optimized
	.byte	16                      # Abbrev [16] 0x26:0x4c DW_TAG_subprogram
	.long	.Linfo_string9          # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	14                      # DW_AT_decl_line
                                        # DW_AT_prototyped
	.long	.Lsection_info+56       # DW_AT_type
                                        # DW_AT_external
	.quad	.Lfunc_begin3           # DW_AT_low_pc
	.quad	.Lfunc_end3             # DW_AT_high_pc
	.byte	1                       # DW_AT_frame_base
	.byte	87
                                        # DW_AT_APPLE_omit_frame_ptr
	.byte	1                       # DW_AT_inline
	.byte	6                       # Abbrev [6] 0x44:0xf DW_TAG_formal_parameter
	.long	.Linfo_string19         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	14                      # DW_AT_decl_line
	.long	502                     # DW_AT_type
	.long	.Ldebug_loc20           # DW_AT_location
	.byte	6                       # Abbrev [6] 0x53:0xf DW_TAG_formal_parameter
	.long	.Linfo_string20         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	14                      # DW_AT_decl_line
	.long	502                     # DW_AT_type
	.long	.Ldebug_loc22           # DW_AT_location
	.byte	6                       # Abbrev [6] 0x62:0xf DW_TAG_formal_parameter
	.long	.Linfo_string21         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	14                      # DW_AT_decl_line
	.long	503                     # DW_AT_type
	.long	.Ldebug_loc24           # DW_AT_location
	.byte	0                       # End Of Children Mark
	.byte	17                      # Abbrev [17] 0x72:0x90 DW_TAG_subprogram
	.long	.Linfo_string10         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	20                      # DW_AT_decl_line
                                        # DW_AT_prototyped
	.long	.Lsection_info+56       # DW_AT_type
                                        # DW_AT_external
	.quad	.Lfunc_begin4           # DW_AT_low_pc
	.quad	.Lfunc_end4             # DW_AT_high_pc
	.byte	1                       # DW_AT_frame_base
	.byte	87
                                        # DW_AT_APPLE_omit_frame_ptr
	.byte	6                       # Abbrev [6] 0x8f:0xf DW_TAG_formal_parameter
	.long	.Linfo_string19         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	20                      # DW_AT_decl_line
	.long	502                     # DW_AT_type
	.long	.Ldebug_loc26           # DW_AT_location
	.byte	6                       # Abbrev [6] 0x9e:0xf DW_TAG_formal_parameter
	.long	.Linfo_string24         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	20                      # DW_AT_decl_line
	.long	502                     # DW_AT_type
	.long	.Ldebug_loc28           # DW_AT_location
	.byte	6                       # Abbrev [6] 0xad:0xf DW_TAG_formal_parameter
	.long	.Linfo_string21         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	20                      # DW_AT_decl_line
	.long	503                     # DW_AT_type
	.long	.Ldebug_loc30           # DW_AT_location
	.byte	18                      # Abbrev [18] 0xbc:0x45 DW_TAG_inlined_subroutine
	.long	38                      # DW_AT_abstract_origin
	.quad	.Ltmp141                # DW_AT_low_pc
	.quad	.Ltmp143                # DW_AT_high_pc
	.byte	2                       # DW_AT_call_file
	.byte	21                      # DW_AT_call_line
	.byte	6                       # Abbrev [6] 0xd3:0xf DW_TAG_formal_parameter
	.long	.Linfo_string19         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	14                      # DW_AT_decl_line
	.long	502                     # DW_AT_type
	.long	.Ldebug_loc32           # DW_AT_location
	.byte	6                       # Abbrev [6] 0xe2:0xf DW_TAG_formal_parameter
	.long	.Linfo_string20         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	14                      # DW_AT_decl_line
	.long	502                     # DW_AT_type
	.long	.Ldebug_loc34           # DW_AT_location
	.byte	6                       # Abbrev [6] 0xf1:0xf DW_TAG_formal_parameter
	.long	.Linfo_string21         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	14                      # DW_AT_decl_line
	.long	503                     # DW_AT_type
	.long	.Ldebug_loc36           # DW_AT_location
	.byte	0                       # End Of Children Mark
	.byte	0                       # End Of Children Mark
	.byte	17                      # Abbrev [17] 0x102:0x90 DW_TAG_subprogram
	.long	.Linfo_string11         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	24                      # DW_AT_decl_line
                                        # DW_AT_prototyped
	.long	.Lsection_info+56       # DW_AT_type
                                        # DW_AT_external
	.quad	.Lfunc_begin5           # DW_AT_low_pc
	.quad	.Lfunc_end5             # DW_AT_high_pc
	.byte	1                       # DW_AT_frame_base
	.byte	87
                                        # DW_AT_APPLE_omit_frame_ptr
	.byte	6                       # Abbrev [6] 0x11f:0xf DW_TAG_formal_parameter
	.long	.Linfo_string25         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	24                      # DW_AT_decl_line
	.long	502                     # DW_AT_type
	.long	.Ldebug_loc38           # DW_AT_location
	.byte	6                       # Abbrev [6] 0x12e:0xf DW_TAG_formal_parameter
	.long	.Linfo_string20         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	24                      # DW_AT_decl_line
	.long	502                     # DW_AT_type
	.long	.Ldebug_loc40           # DW_AT_location
	.byte	6                       # Abbrev [6] 0x13d:0xf DW_TAG_formal_parameter
	.long	.Linfo_string21         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	24                      # DW_AT_decl_line
	.long	503                     # DW_AT_type
	.long	.Ldebug_loc42           # DW_AT_location
	.byte	18                      # Abbrev [18] 0x14c:0x45 DW_TAG_inlined_subroutine
	.long	38                      # DW_AT_abstract_origin
	.quad	.Ltmp158                # DW_AT_low_pc
	.quad	.Ltmp160                # DW_AT_high_pc
	.byte	2                       # DW_AT_call_file
	.byte	25                      # DW_AT_call_line
	.byte	6                       # Abbrev [6] 0x163:0xf DW_TAG_formal_parameter
	.long	.Linfo_string19         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	14                      # DW_AT_decl_line
	.long	502                     # DW_AT_type
	.long	.Ldebug_loc44           # DW_AT_location
	.byte	6                       # Abbrev [6] 0x172:0xf DW_TAG_formal_parameter
	.long	.Linfo_string20         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	14                      # DW_AT_decl_line
	.long	502                     # DW_AT_type
	.long	.Ldebug_loc46           # DW_AT_location
	.byte	6                       # Abbrev [6] 0x181:0xf DW_TAG_formal_parameter
	.long	.Linfo_string21         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	14                      # DW_AT_decl_line
	.long	503                     # DW_AT_type
	.long	.Ldebug_loc48           # DW_AT_location
	.byte	0                       # End Of Children Mark
	.byte	0                       # End Of Children Mark
	.byte	17                      # Abbrev [17] 0x192:0x4b DW_TAG_subprogram
	.long	.Linfo_string12         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	28                      # DW_AT_decl_line
                                        # DW_AT_prototyped
	.long	.Lsection_info+56       # DW_AT_type
                                        # DW_AT_external
	.quad	.Lfunc_begin6           # DW_AT_low_pc
	.quad	.Lfunc_end6             # DW_AT_high_pc
	.byte	1                       # DW_AT_frame_base
	.byte	87
                                        # DW_AT_APPLE_omit_frame_ptr
	.byte	19                      # Abbrev [19] 0x1af:0xf DW_TAG_formal_parameter
	.long	.Linfo_string26         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	28                      # DW_AT_decl_line
	.long	502                     # DW_AT_type
	.byte	3                       # DW_AT_location
	.byte	145
	.asciz	"\340"
	.byte	19                      # Abbrev [19] 0x1be:0xf DW_TAG_formal_parameter
	.long	.Linfo_string21         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	28                      # DW_AT_decl_line
	.long	503                     # DW_AT_type
	.byte	3                       # DW_AT_location
	.byte	145
	.asciz	"\330"
	.byte	6                       # Abbrev [6] 0x1cd:0xf DW_TAG_formal_parameter
	.long	.Linfo_string27         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	28                      # DW_AT_decl_line
	.long	521                     # DW_AT_type
	.long	.Ldebug_loc50           # DW_AT_location
	.byte	0                       # End Of Children Mark
	.byte	20                      # Abbrev [20] 0x1dd:0x19 DW_TAG_subprogram
	.long	.Linfo_string13         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	71                      # DW_AT_decl_line
                                        # DW_AT_external
	.quad	.Lfunc_begin7           # DW_AT_low_pc
	.quad	.Lfunc_end7             # DW_AT_high_pc
	.byte	1                       # DW_AT_frame_base
	.byte	87
                                        # DW_AT_APPLE_omit_frame_ptr
	.byte	21                      # Abbrev [21] 0x1f6:0x1 DW_TAG_pointer_type
	.byte	22                      # Abbrev [22] 0x1f7:0xb DW_TAG_typedef
	.long	514                     # DW_AT_type
	.long	.Linfo_string23         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	42                      # DW_AT_decl_line
	.byte	4                       # Abbrev [4] 0x202:0x7 DW_TAG_base_type
	.long	.Linfo_string22         # DW_AT_name
	.byte	7                       # DW_AT_encoding
	.byte	8                       # DW_AT_byte_size
	.byte	4                       # Abbrev [4] 0x209:0x7 DW_TAG_base_type
	.long	.Linfo_string28         # DW_AT_name
	.byte	7                       # DW_AT_encoding
	.byte	4                       # DW_AT_byte_size
	.byte	0                       # End Of Children Mark
.L.debug_info_end1:
	.section	.debug_abbrev,"",@progbits
.L.debug_abbrev_begin:
	.byte	1                       # Abbreviation Code
	.byte	17                      # DW_TAG_compile_unit
	.byte	1                       # DW_CHILDREN_yes
	.byte	37                      # DW_AT_producer
	.byte	14                      # DW_FORM_strp
	.byte	19                      # DW_AT_language
	.byte	5                       # DW_FORM_data2
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	17                      # DW_AT_low_pc
	.byte	1                       # DW_FORM_addr
	.byte	16                      # DW_AT_stmt_list
	.byte	23                      # DW_FORM_sec_offset
	.byte	27                      # DW_AT_comp_dir
	.byte	14                      # DW_FORM_strp
	.ascii	"\341\177"              # DW_AT_APPLE_optimized
	.byte	25                      # DW_FORM_flag_present
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	2                       # Abbreviation Code
	.byte	52                      # DW_TAG_variable
	.byte	0                       # DW_CHILDREN_no
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	28                      # DW_AT_const_value
	.byte	13                      # DW_FORM_sdata
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	3                       # Abbreviation Code
	.byte	38                      # DW_TAG_const_type
	.byte	0                       # DW_CHILDREN_no
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	4                       # Abbreviation Code
	.byte	36                      # DW_TAG_base_type
	.byte	0                       # DW_CHILDREN_no
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	62                      # DW_AT_encoding
	.byte	11                      # DW_FORM_data1
	.byte	11                      # DW_AT_byte_size
	.byte	11                      # DW_FORM_data1
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	5                       # Abbreviation Code
	.byte	46                      # DW_TAG_subprogram
	.byte	1                       # DW_CHILDREN_yes
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	39                      # DW_AT_prototyped
	.byte	25                      # DW_FORM_flag_present
	.byte	63                      # DW_AT_external
	.byte	25                      # DW_FORM_flag_present
	.byte	17                      # DW_AT_low_pc
	.byte	1                       # DW_FORM_addr
	.byte	18                      # DW_AT_high_pc
	.byte	1                       # DW_FORM_addr
	.byte	64                      # DW_AT_frame_base
	.byte	10                      # DW_FORM_block1
	.ascii	"\347\177"              # DW_AT_APPLE_omit_frame_ptr
	.byte	25                      # DW_FORM_flag_present
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	6                       # Abbreviation Code
	.byte	5                       # DW_TAG_formal_parameter
	.byte	0                       # DW_CHILDREN_no
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	2                       # DW_AT_location
	.byte	23                      # DW_FORM_sec_offset
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	7                       # Abbreviation Code
	.byte	11                      # DW_TAG_lexical_block
	.byte	1                       # DW_CHILDREN_yes
	.byte	17                      # DW_AT_low_pc
	.byte	1                       # DW_FORM_addr
	.byte	18                      # DW_AT_high_pc
	.byte	1                       # DW_FORM_addr
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	8                       # Abbreviation Code
	.byte	52                      # DW_TAG_variable
	.byte	0                       # DW_CHILDREN_no
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	28                      # DW_AT_const_value
	.byte	13                      # DW_FORM_sdata
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	9                       # Abbreviation Code
	.byte	46                      # DW_TAG_subprogram
	.byte	1                       # DW_CHILDREN_yes
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	39                      # DW_AT_prototyped
	.byte	25                      # DW_FORM_flag_present
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	63                      # DW_AT_external
	.byte	25                      # DW_FORM_flag_present
	.byte	17                      # DW_AT_low_pc
	.byte	1                       # DW_FORM_addr
	.byte	18                      # DW_AT_high_pc
	.byte	1                       # DW_FORM_addr
	.byte	64                      # DW_AT_frame_base
	.byte	10                      # DW_FORM_block1
	.ascii	"\347\177"              # DW_AT_APPLE_omit_frame_ptr
	.byte	25                      # DW_FORM_flag_present
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	10                      # Abbreviation Code
	.byte	52                      # DW_TAG_variable
	.byte	0                       # DW_CHILDREN_no
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	2                       # DW_AT_location
	.byte	23                      # DW_FORM_sec_offset
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	11                      # Abbreviation Code
	.byte	46                      # DW_TAG_subprogram
	.byte	1                       # DW_CHILDREN_yes
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	63                      # DW_AT_external
	.byte	25                      # DW_FORM_flag_present
	.byte	17                      # DW_AT_low_pc
	.byte	1                       # DW_FORM_addr
	.byte	18                      # DW_AT_high_pc
	.byte	1                       # DW_FORM_addr
	.byte	64                      # DW_AT_frame_base
	.byte	10                      # DW_FORM_block1
	.ascii	"\347\177"              # DW_AT_APPLE_omit_frame_ptr
	.byte	25                      # DW_FORM_flag_present
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	12                      # Abbreviation Code
	.byte	52                      # DW_TAG_variable
	.byte	0                       # DW_CHILDREN_no
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	2                       # DW_AT_location
	.byte	10                      # DW_FORM_block1
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	13                      # Abbreviation Code
	.byte	11                      # DW_TAG_lexical_block
	.byte	1                       # DW_CHILDREN_yes
	.byte	85                      # DW_AT_ranges
	.byte	6                       # DW_FORM_data4
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	14                      # Abbreviation Code
	.byte	52                      # DW_TAG_variable
	.byte	0                       # DW_CHILDREN_no
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	15                      # Abbreviation Code
	.byte	15                      # DW_TAG_pointer_type
	.byte	0                       # DW_CHILDREN_no
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	16                      # Abbreviation Code
	.byte	46                      # DW_TAG_subprogram
	.byte	1                       # DW_CHILDREN_yes
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	39                      # DW_AT_prototyped
	.byte	25                      # DW_FORM_flag_present
	.byte	73                      # DW_AT_type
	.byte	16                      # DW_FORM_ref_addr
	.byte	63                      # DW_AT_external
	.byte	25                      # DW_FORM_flag_present
	.byte	17                      # DW_AT_low_pc
	.byte	1                       # DW_FORM_addr
	.byte	18                      # DW_AT_high_pc
	.byte	1                       # DW_FORM_addr
	.byte	64                      # DW_AT_frame_base
	.byte	10                      # DW_FORM_block1
	.ascii	"\347\177"              # DW_AT_APPLE_omit_frame_ptr
	.byte	25                      # DW_FORM_flag_present
	.byte	32                      # DW_AT_inline
	.byte	11                      # DW_FORM_data1
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	17                      # Abbreviation Code
	.byte	46                      # DW_TAG_subprogram
	.byte	1                       # DW_CHILDREN_yes
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	39                      # DW_AT_prototyped
	.byte	25                      # DW_FORM_flag_present
	.byte	73                      # DW_AT_type
	.byte	16                      # DW_FORM_ref_addr
	.byte	63                      # DW_AT_external
	.byte	25                      # DW_FORM_flag_present
	.byte	17                      # DW_AT_low_pc
	.byte	1                       # DW_FORM_addr
	.byte	18                      # DW_AT_high_pc
	.byte	1                       # DW_FORM_addr
	.byte	64                      # DW_AT_frame_base
	.byte	10                      # DW_FORM_block1
	.ascii	"\347\177"              # DW_AT_APPLE_omit_frame_ptr
	.byte	25                      # DW_FORM_flag_present
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	18                      # Abbreviation Code
	.byte	29                      # DW_TAG_inlined_subroutine
	.byte	1                       # DW_CHILDREN_yes
	.byte	49                      # DW_AT_abstract_origin
	.byte	19                      # DW_FORM_ref4
	.byte	17                      # DW_AT_low_pc
	.byte	1                       # DW_FORM_addr
	.byte	18                      # DW_AT_high_pc
	.byte	1                       # DW_FORM_addr
	.byte	88                      # DW_AT_call_file
	.byte	11                      # DW_FORM_data1
	.byte	89                      # DW_AT_call_line
	.byte	11                      # DW_FORM_data1
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	19                      # Abbreviation Code
	.byte	5                       # DW_TAG_formal_parameter
	.byte	0                       # DW_CHILDREN_no
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	2                       # DW_AT_location
	.byte	10                      # DW_FORM_block1
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	20                      # Abbreviation Code
	.byte	46                      # DW_TAG_subprogram
	.byte	0                       # DW_CHILDREN_no
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	63                      # DW_AT_external
	.byte	25                      # DW_FORM_flag_present
	.byte	17                      # DW_AT_low_pc
	.byte	1                       # DW_FORM_addr
	.byte	18                      # DW_AT_high_pc
	.byte	1                       # DW_FORM_addr
	.byte	64                      # DW_AT_frame_base
	.byte	10                      # DW_FORM_block1
	.ascii	"\347\177"              # DW_AT_APPLE_omit_frame_ptr
	.byte	25                      # DW_FORM_flag_present
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	21                      # Abbreviation Code
	.byte	15                      # DW_TAG_pointer_type
	.byte	0                       # DW_CHILDREN_no
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	22                      # Abbreviation Code
	.byte	22                      # DW_TAG_typedef
	.byte	0                       # DW_CHILDREN_no
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	0                       # EOM(3)
.L.debug_abbrev_end:
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
	.quad	.Lfunc_begin0
	.quad	.Ltmp24
.Lset0 = .Ltmp230-.Ltmp229              # Loc expr size
	.short	.Lset0
.Ltmp229:
	.byte	119                     # DW_OP_breg7
	.ascii	"\340\007"
.Ltmp230:
	.quad	0
	.quad	0
.Ldebug_loc2:
	.quad	.Lfunc_begin0
	.quad	.Ltmp24
.Lset1 = .Ltmp232-.Ltmp231              # Loc expr size
	.short	.Lset1
.Ltmp231:
	.byte	119                     # DW_OP_breg7
	.ascii	"\330\007"
.Ltmp232:
	.quad	0
	.quad	0
.Ldebug_loc4:
	.quad	.Lfunc_begin0
	.quad	.Ltmp21
.Lset2 = .Ltmp234-.Ltmp233              # Loc expr size
	.short	.Lset2
.Ltmp233:
	.byte	119                     # DW_OP_breg7
	.ascii	"\304\b"
.Ltmp234:
	.quad	0
	.quad	0
.Ldebug_loc6:
	.quad	.Lfunc_begin1
	.quad	.Ltmp50
.Lset3 = .Ltmp236-.Ltmp235              # Loc expr size
	.short	.Lset3
.Ltmp235:
	.byte	119                     # DW_OP_breg7
	.ascii	"\340\t"
.Ltmp236:
	.quad	0
	.quad	0
.Ldebug_loc8:
	.quad	.Lfunc_begin1
	.quad	.Ltmp50
.Lset4 = .Ltmp238-.Ltmp237              # Loc expr size
	.short	.Lset4
.Ltmp237:
	.byte	119                     # DW_OP_breg7
	.ascii	"\330\t"
.Ltmp238:
	.quad	0
	.quad	0
.Ldebug_loc10:
	.quad	.Lfunc_begin1
	.quad	.Ltmp50
.Lset5 = .Ltmp240-.Ltmp239              # Loc expr size
	.short	.Lset5
.Ltmp239:
	.byte	119                     # DW_OP_breg7
	.ascii	"\324\t"
.Ltmp240:
	.quad	0
	.quad	0
.Ldebug_loc12:
	.quad	.Ltmp49
	.quad	.Ltmp53
.Lset6 = .Ltmp242-.Ltmp241              # Loc expr size
	.short	.Lset6
.Ltmp241:
	.byte	16                      # DW_OP_constu
	.byte	0
.Ltmp242:
	.quad	.Ltmp53
	.quad	.Ltmp54
.Lset7 = .Ltmp244-.Ltmp243              # Loc expr size
	.short	.Lset7
.Ltmp243:
	.byte	119                     # DW_OP_breg7
	.ascii	"\264\003"
.Ltmp244:
	.quad	0
	.quad	0
.Ldebug_loc15:
	.quad	.Ltmp80
	.quad	.Ltmp81
.Lset8 = .Ltmp246-.Ltmp245              # Loc expr size
	.short	.Lset8
.Ltmp245:
	.byte	119                     # DW_OP_breg7
	.ascii	"\364\024"
.Ltmp246:
	.quad	.Ltmp82
	.quad	.Ltmp83
.Lset9 = .Ltmp248-.Ltmp247              # Loc expr size
	.short	.Lset9
.Ltmp247:
	.byte	119                     # DW_OP_breg7
	.ascii	"\314\020"
.Ltmp248:
	.quad	0
	.quad	0
.Ldebug_loc18:
	.quad	.Ltmp97
	.quad	.Ltmp100
.Lset10 = .Ltmp250-.Ltmp249             # Loc expr size
	.short	.Lset10
.Ltmp249:
	.byte	119                     # DW_OP_breg7
	.ascii	"\314\005"
.Ltmp250:
	.quad	0
	.quad	0
.Ldebug_loc20:
	.quad	.Lfunc_begin3
	.quad	.Ltmp126
.Lset11 = .Ltmp252-.Ltmp251             # Loc expr size
	.short	.Lset11
.Ltmp251:
	.byte	119                     # DW_OP_breg7
	.ascii	"\230\004"
.Ltmp252:
	.quad	0
	.quad	0
.Ldebug_loc22:
	.quad	.Lfunc_begin3
	.quad	.Ltmp126
.Lset12 = .Ltmp254-.Ltmp253             # Loc expr size
	.short	.Lset12
.Ltmp253:
	.byte	119                     # DW_OP_breg7
	.ascii	"\360\004"
.Ltmp254:
	.quad	0
	.quad	0
.Ldebug_loc24:
	.quad	.Lfunc_begin3
	.quad	.Ltmp126
.Lset13 = .Ltmp256-.Ltmp255             # Loc expr size
	.short	.Lset13
.Ltmp255:
	.byte	119                     # DW_OP_breg7
	.ascii	"\210\004"
.Ltmp256:
	.quad	0
	.quad	0
.Ldebug_loc26:
	.quad	.Lfunc_begin4
	.quad	.Ltmp142
.Lset14 = .Ltmp258-.Ltmp257             # Loc expr size
	.short	.Lset14
.Ltmp257:
	.byte	119                     # DW_OP_breg7
	.asciz	"\330"
.Ltmp258:
	.quad	0
	.quad	0
.Ldebug_loc28:
	.quad	.Lfunc_begin4
	.quad	.Ltmp142
.Lset15 = .Ltmp260-.Ltmp259             # Loc expr size
	.short	.Lset15
.Ltmp259:
	.byte	119                     # DW_OP_breg7
	.asciz	"\320"
.Ltmp260:
	.quad	0
	.quad	0
.Ldebug_loc30:
	.quad	.Lfunc_begin4
	.quad	.Ltmp142
.Lset16 = .Ltmp262-.Ltmp261             # Loc expr size
	.short	.Lset16
.Ltmp261:
	.byte	119                     # DW_OP_breg7
	.asciz	"\310"
.Ltmp262:
	.quad	0
	.quad	0
.Ldebug_loc32:
	.quad	.Ltmp141
	.quad	.Ltmp142
.Lset17 = .Ltmp264-.Ltmp263             # Loc expr size
	.short	.Lset17
.Ltmp263:
	.byte	119                     # DW_OP_breg7
	.asciz	"\330"
.Ltmp264:
	.quad	0
	.quad	0
.Ldebug_loc34:
	.quad	.Ltmp141
	.quad	.Ltmp142
.Lset18 = .Ltmp266-.Ltmp265             # Loc expr size
	.short	.Lset18
.Ltmp265:
	.byte	119                     # DW_OP_breg7
	.asciz	"\320"
.Ltmp266:
	.quad	0
	.quad	0
.Ldebug_loc36:
	.quad	.Ltmp141
	.quad	.Ltmp142
.Lset19 = .Ltmp268-.Ltmp267             # Loc expr size
	.short	.Lset19
.Ltmp267:
	.byte	119                     # DW_OP_breg7
	.asciz	"\310"
.Ltmp268:
	.quad	0
	.quad	0
.Ldebug_loc38:
	.quad	.Lfunc_begin5
	.quad	.Ltmp159
.Lset20 = .Ltmp270-.Ltmp269             # Loc expr size
	.short	.Lset20
.Ltmp269:
	.byte	119                     # DW_OP_breg7
	.asciz	"\330"
.Ltmp270:
	.quad	0
	.quad	0
.Ldebug_loc40:
	.quad	.Lfunc_begin5
	.quad	.Ltmp159
.Lset21 = .Ltmp272-.Ltmp271             # Loc expr size
	.short	.Lset21
.Ltmp271:
	.byte	119                     # DW_OP_breg7
	.asciz	"\320"
.Ltmp272:
	.quad	0
	.quad	0
.Ldebug_loc42:
	.quad	.Lfunc_begin5
	.quad	.Ltmp159
.Lset22 = .Ltmp274-.Ltmp273             # Loc expr size
	.short	.Lset22
.Ltmp273:
	.byte	119                     # DW_OP_breg7
	.asciz	"\310"
.Ltmp274:
	.quad	0
	.quad	0
.Ldebug_loc44:
	.quad	.Ltmp158
	.quad	.Ltmp159
.Lset23 = .Ltmp276-.Ltmp275             # Loc expr size
	.short	.Lset23
.Ltmp275:
	.byte	119                     # DW_OP_breg7
	.asciz	"\330"
.Ltmp276:
	.quad	0
	.quad	0
.Ldebug_loc46:
	.quad	.Ltmp158
	.quad	.Ltmp159
.Lset24 = .Ltmp278-.Ltmp277             # Loc expr size
	.short	.Lset24
.Ltmp277:
	.byte	119                     # DW_OP_breg7
	.asciz	"\320"
.Ltmp278:
	.quad	0
	.quad	0
.Ldebug_loc48:
	.quad	.Ltmp158
	.quad	.Ltmp159
.Lset25 = .Ltmp280-.Ltmp279             # Loc expr size
	.short	.Lset25
.Ltmp279:
	.byte	119                     # DW_OP_breg7
	.asciz	"\310"
.Ltmp280:
	.quad	0
	.quad	0
.Ldebug_loc50:
	.quad	.Lfunc_begin6
	.quad	.Ltmp184
.Lset26 = .Ltmp282-.Ltmp281             # Loc expr size
	.short	.Lset26
.Ltmp281:
	.byte	119                     # DW_OP_breg7
	.asciz	"\324"
.Ltmp282:
	.quad	0
	.quad	0
.Ldebug_loc52:
	.section	.debug_aranges,"",@progbits
	.long	44                      # Length of ARange Set
	.short	2                       # DWARF Arange version number
	.long	.L.debug_info_begin0    # Offset Into Debug Info Section
	.byte	8                       # Address Size (in bytes)
	.byte	0                       # Segment Size (in bytes)
	.byte	255
	.byte	255
	.byte	255
	.byte	255
	.quad	.Lfunc_begin0
.Lset27 = .Lfunc_begin3-.Lfunc_begin0
	.quad	.Lset27
	.quad	0                       # ARange terminator
	.quad	0
	.long	44                      # Length of ARange Set
	.short	2                       # DWARF Arange version number
	.long	.L.debug_info_begin1    # Offset Into Debug Info Section
	.byte	8                       # Address Size (in bytes)
	.byte	0                       # Segment Size (in bytes)
	.byte	255
	.byte	255
	.byte	255
	.byte	255
	.quad	.Lfunc_begin3
.Lset28 = .Ldebug_end0-.Lfunc_begin3
	.quad	.Lset28
	.quad	0                       # ARange terminator
	.quad	0
	.section	.debug_ranges,"",@progbits
	.quad	.Ltmp84
	.quad	.Ltmp87
	.quad	.Ltmp88
	.quad	.Ltmp90
	.quad	0
	.quad	0
	.section	.debug_macinfo,"",@progbits
	.section	.debug_pubnames,"",@progbits
.Lset29 = .Lpubnames_end0-.Lpubnames_begin0 # Length of Public Names Info
	.long	.Lset29
.Lpubnames_begin0:
	.short	2                       # DWARF Version
	.long	.L.debug_info_begin0    # Offset of Compilation Unit Info
.Lset30 = .L.debug_info_end0-.L.debug_info_begin0 # Compilation Unit Length
	.long	.Lset30
	.long	284                     # DIE offset
	.asciz	"main"                  # External Name
	.long	63                      # DIE offset
	.asciz	"store_kernel"          # External Name
	.long	164                     # DIE offset
	.asciz	"test_stores"           # External Name
	.long	0                       # End Mark
.Lpubnames_end0:
.Lset31 = .Lpubnames_end1-.Lpubnames_begin1 # Length of Public Names Info
	.long	.Lset31
.Lpubnames_begin1:
	.short	2                       # DWARF Version
	.long	.L.debug_info_begin1    # Offset of Compilation Unit Info
.Lset32 = .L.debug_info_end1-.L.debug_info_begin1 # Compilation Unit Length
	.long	.Lset32
	.long	114                     # DIE offset
	.asciz	"dmaLoad"               # External Name
	.long	402                     # DIE offset
	.asciz	"setReadyBits"          # External Name
	.long	477                     # DIE offset
	.asciz	"dmaFence"              # External Name
	.long	38                      # DIE offset
	.asciz	"_dmaImpl3"             # External Name
	.long	258                     # DIE offset
	.asciz	"dmaStore"              # External Name
	.long	0                       # End Mark
.Lpubnames_end1:
	.section	.debug_pubtypes,"",@progbits
.Lset33 = .Lpubtypes_end0-.Lpubtypes_begin0 # Length of Public Types Info
	.long	.Lset33
.Lpubtypes_begin0:
	.short	2                       # DWARF Version
	.long	.L.debug_info_begin0    # Offset of Compilation Unit Info
.Lset34 = .L.debug_info_end0-.L.debug_info_begin0 # Compilation Unit Length
	.long	.Lset34
	.long	56                      # DIE offset
	.asciz	"int"                   # External Name
	.long	0                       # End Mark
.Lpubtypes_end0:
.Lset35 = .Lpubtypes_end1-.Lpubtypes_begin1 # Length of Public Types Info
	.long	.Lset35
.Lpubtypes_begin1:
	.short	2                       # DWARF Version
	.long	.L.debug_info_begin1    # Offset of Compilation Unit Info
.Lset36 = .L.debug_info_end1-.L.debug_info_begin1 # Compilation Unit Length
	.long	.Lset36
	.long	514                     # DIE offset
	.asciz	"long unsigned int"     # External Name
	.long	521                     # DIE offset
	.asciz	"unsigned int"          # External Name
	.long	56                      # DIE offset
	.asciz	"int"                   # External Name
	.long	503                     # DIE offset
	.asciz	"size_t"                # External Name
	.long	0                       # End Mark
.Lpubtypes_end1:

	.ident	"clang version 3.4.2 (tags/RELEASE_34/dot2-final)"
	.ident	"clang version 3.4.2 (tags/RELEASE_34/dot2-final)"
	.ident	"clang version 3.4.2 (tags/RELEASE_34/dot2-final)"
	.section	".note.GNU-stack","",@progbits
