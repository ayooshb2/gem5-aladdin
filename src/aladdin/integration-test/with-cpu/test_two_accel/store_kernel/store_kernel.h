#ifndef TYPE
#define TYPE int
#endif

void store_kernel(TYPE* store_vals, TYPE* store_loc, int num_vals);
int test_stores(TYPE* store_vals, TYPE* store_loc, int num_vals);
