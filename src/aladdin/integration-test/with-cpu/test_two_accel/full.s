	.file	"full.llvm"
	.section	.debug_info,"",@progbits
.Lsection_info:
	.section	.debug_abbrev,"",@progbits
.Lsection_abbrev:
	.section	.debug_aranges,"",@progbits
	.section	.debug_macinfo,"",@progbits
	.section	.debug_line,"",@progbits
.Lsection_line:
	.section	.debug_loc,"",@progbits
	.section	.debug_pubnames,"",@progbits
	.section	.debug_pubtypes,"",@progbits
	.section	.debug_str,"MS",@progbits,1
.Linfo_string:
	.section	.debug_ranges,"",@progbits
.Ldebug_range:
	.section	.debug_loc,"",@progbits
.Lsection_debug_loc:
	.text
.Ltext_begin:
	.data
	.file	1 "test_dma_load_store.c"
	.file	2 "store_kernel/store_kernel.c"
	.file	3 "test_kernel/test_kernel.c"
	.file	4 "/workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c"
	.text
	.globl	main
	.align	16, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
.Lfunc_begin0:
	.loc	1 22 0                  # test_dma_load_store.c:22:0
# BB#0:
	subq	$72, %rsp
.Ltmp1:
	.cfi_def_cfa_offset 80
	leaq	.L.str, %rdi
	movabsq	$41, %rsi
	callq	trace_logger_write_labelmap
	movabsq	$64, %rsi
	movabsq	$4096, %rdx             # imm = 0x1000
	leaq	64(%rsp), %rdi
.Ltmp2:
	#DEBUG_VALUE: main:num_vals <- 1024
	#DEBUG_VALUE: main:store_vals <- RDI
	.loc	1 25 0 prologue_end     # test_dma_load_store.c:25:0
	callq	posix_memalign
.Ltmp3:
	#DEBUG_VALUE: main:err <- EAX
	.loc	1 27 0                  # test_dma_load_store.c:27:0
	cmpl	$0, %eax
	je	.LBB0_2
.Ltmp4:
# BB#1:
	leaq	.L.str1, %rdi
	leaq	.L.str2, %rsi
	movl	$27, %edx
	leaq	.L__PRETTY_FUNCTION__.main, %rcx
	callq	__assert_fail
.LBB0_2:
	movabsq	$64, %rsi
	movabsq	$4096, %rdx             # imm = 0x1000
	leaq	56(%rsp), %rax
	.loc	1 28 0                  # test_dma_load_store.c:28:0
	movq	%rax, %rdi
	callq	posix_memalign
.Ltmp5:
	#DEBUG_VALUE: main:err <- EAX
	.loc	1 30 0                  # test_dma_load_store.c:30:0
	cmpl	$0, %eax
	jne	.LBB0_4
.Ltmp6:
# BB#3:                                 # %.preheader
	movabsq	$0, %rax
	.loc	1 32 0                  # test_dma_load_store.c:32:0
.Ltmp7:
	movq	64(%rsp), %rcx
	.loc	1 33 0                  # test_dma_load_store.c:33:0
	movq	56(%rsp), %rdx
.Ltmp8:
	.loc	1 31 0                  # test_dma_load_store.c:31:0
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rax, 32(%rsp)          # 8-byte Spill
	jmp	.LBB0_5
.Ltmp9:
.LBB0_4:
	leaq	.L.str1, %rdi
	leaq	.L.str2, %rsi
	movl	$30, %edx
	leaq	.L__PRETTY_FUNCTION__.main, %rcx
	.loc	1 30 0                  # test_dma_load_store.c:30:0
	callq	__assert_fail
.LBB0_5:                                # =>This Inner Loop Header: Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	.loc	1 32 0                  # test_dma_load_store.c:32:0
.Ltmp10:
	movl	%eax, %ecx
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	%ecx, (%rdx,%rax,4)
	.loc	1 33 0                  # test_dma_load_store.c:33:0
	movq	48(%rsp), %rsi          # 8-byte Reload
	movl	$-1, (%rsi,%rax,4)
.Ltmp11:
	.loc	1 31 0                  # test_dma_load_store.c:31:0
	addq	$1, %rax
	cmpq	$1024, %rax             # imm = 0x400
	movq	%rax, 32(%rsp)          # 8-byte Spill
	jne	.LBB0_5
.Ltmp12:
# BB#6:
	movl	$1024, %edx             # imm = 0x400
	.loc	1 56 0                  # test_dma_load_store.c:56:0
	movq	64(%rsp), %rdi
	movq	56(%rsp), %rsi
	callq	store_kernel
	movl	$1024, %edx             # imm = 0x400
	.loc	1 57 0                  # test_dma_load_store.c:57:0
	movq	64(%rsp), %rdi
	movq	56(%rsp), %rsi
	callq	test_kernel
	movl	$1024, %edx             # imm = 0x400
	.loc	1 60 0                  # test_dma_load_store.c:60:0
	movq	64(%rsp), %rdi
	movq	56(%rsp), %rsi
	callq	test_stores_plus
.Ltmp13:
	#DEBUG_VALUE: main:num_failures <- EAX
	.loc	1 62 0                  # test_dma_load_store.c:62:0
	movq	stdout, %rsi
.Ltmp14:
	.loc	1 61 0                  # test_dma_load_store.c:61:0
	cmpl	$0, %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
.Ltmp15:
	#DEBUG_VALUE: main:num_failures <- [RSP+28]
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	je	.LBB0_8
.Ltmp16:
# BB#7:
	leaq	.L.str3, %rsi
	.loc	1 62 0                  # test_dma_load_store.c:62:0
.Ltmp17:
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	28(%rsp), %edx          # 4-byte Reload
	movb	$0, %al
	callq	fprintf
	movl	$4294967295, %edx       # imm = 0xFFFFFFFF
	.loc	1 63 0                  # test_dma_load_store.c:63:0
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	%edx, 8(%rsp)           # 4-byte Spill
	jmp	.LBB0_9
.Ltmp18:
.LBB0_8:
	leaq	.L.str4, %rsi
	.loc	1 65 0                  # test_dma_load_store.c:65:0
	movq	16(%rsp), %rdi          # 8-byte Reload
	movb	$0, %al
	callq	fprintf
	movl	$0, %ecx
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movl	%ecx, 8(%rsp)           # 4-byte Spill
.LBB0_9:
	movl	8(%rsp), %eax           # 4-byte Reload
	.loc	1 67 0                  # test_dma_load_store.c:67:0
	addq	$72, %rsp
	ret
.Ltmp19:
.Ltmp20:
	.size	main, .Ltmp20-main
.Lfunc_end0:
	.cfi_endproc

	.globl	store_kernel
	.align	16, 0x90
	.type	store_kernel,@function
store_kernel:                           # @store_kernel
	.cfi_startproc
.Lfunc_begin1:
	.loc	2 24 0                  # store_kernel/store_kernel.c:24:0
# BB#0:
	pushq	%rbp
.Ltmp28:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Ltmp29:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Ltmp30:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Ltmp31:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Ltmp32:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Ltmp33:
	.cfi_def_cfa_offset 56
	subq	$1160, %rsp             # imm = 0x488
.Ltmp34:
	.cfi_def_cfa_offset 1216
.Ltmp35:
	.cfi_offset %rbx, -56
.Ltmp36:
	.cfi_offset %r12, -48
.Ltmp37:
	.cfi_offset %r13, -40
.Ltmp38:
	.cfi_offset %r14, -32
.Ltmp39:
	.cfi_offset %r15, -24
.Ltmp40:
	.cfi_offset %rbp, -16
	movabsq	$3, %rax
	movabsq	$64, %rcx
	movabsq	$1, %r8
	leaq	.L.str8, %r9
	movabsq	$0, %r10
	leaq	.L.str510, %r11
	movabsq	$2, %rbx
	leaq	.L.str49, %r14
	movabsq	$4, %r15
	leaq	dmaLoad, %r12
	leaq	.L.str11, %r13
	movabsq	$26, %rbp
	movq	%rax, 1152(%rsp)        # 8-byte Spill
	leaq	.L.str16, %rax
	movq	%rax, 1144(%rsp)        # 8-byte Spill
	leaq	.L.str27, %rax
	movq	%rax, 1136(%rsp)        # 8-byte Spill
	leaq	.L.str10, %rax
	movq	%rax, 1128(%rsp)        # 8-byte Spill
	movabsq	$99, %rax
	movl	%edx, 1124(%rsp)        # 4-byte Spill
	movl	$1, %edx
	movl	%edx, 1120(%rsp)        # 4-byte Spill
	movl	$0, %edx
	movq	%rax, 1112(%rsp)        # 8-byte Spill
	movabsq	$19134, %rax            # imm = 0x4ABE
	movq	%rax, 1104(%rsp)        # 8-byte Spill
	leaq	.L.str6, %rax
	movq	%rax, 1096(%rsp)        # 8-byte Spill
	leaq	.L.str9, %rax
	movq	%rax, 1088(%rsp)        # 8-byte Spill
	movabsq	$20, %rax
	movq	%rax, 1080(%rsp)        # 8-byte Spill
	movabsq	$32, %rax
	movq	%rax, 1072(%rsp)        # 8-byte Spill
	leaq	.L.str7, %rax
	movq	%rax, 1064(%rsp)        # 8-byte Spill
	movabsq	$35, %rax
	movq	%rax, 1056(%rsp)        # 8-byte Spill
	leaq	.L.str38, %rax
	movq	%rax, 1048(%rsp)        # 8-byte Spill
	movabsq	$44, %rax
	movq	%rax, 1040(%rsp)        # 8-byte Spill
	movabsq	$24601, %rax            # imm = 0x6019
	movq	%rax, 1032(%rsp)        # 8-byte Spill
	leaq	.L.str13, %rax
	.loc	2 24 0 prologue_end     # store_kernel/store_kernel.c:24:0
.Ltmp41:
	movq	%rax, 1024(%rsp)        # 8-byte Spill
	movq	1032(%rsp), %rax        # 8-byte Reload
	movq	%rdi, 1016(%rsp)        # 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, 1008(%rsp)        # 8-byte Spill
	movq	%rcx, %rsi
	movq	1016(%rsp), %rax        # 8-byte Reload
	movl	%edx, 1004(%rsp)        # 4-byte Spill
	movq	%rax, %rdx
	movq	%rcx, 992(%rsp)         # 8-byte Spill
	movq	%r8, %rcx
	movq	%r8, 984(%rsp)          # 8-byte Spill
	movq	%r14, %r8
	movq	%r9, 976(%rsp)          # 8-byte Spill
	movq	%r10, %r9
	movq	%r11, (%rsp)
	movq	%r13, 968(%rsp)         # 8-byte Spill
	movq	%rbp, 960(%rsp)         # 8-byte Spill
	movq	%r12, 952(%rsp)         # 8-byte Spill
	movq	%r15, 944(%rsp)         # 8-byte Spill
	movq	%r14, 936(%rsp)         # 8-byte Spill
	movq	%rbx, 928(%rsp)         # 8-byte Spill
	movq	%r10, 920(%rsp)         # 8-byte Spill
	movq	%r11, 912(%rsp)         # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	1032(%rsp), %rdi        # 8-byte Reload
	movq	992(%rsp), %rsi         # 8-byte Reload
	movq	1008(%rsp), %rdx        # 8-byte Reload
	movq	984(%rsp), %rcx         # 8-byte Reload
	movq	1024(%rsp), %r8         # 8-byte Reload
	movq	920(%rsp), %r9          # 8-byte Reload
	movq	912(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movl	1124(%rsp), %esi        # 4-byte Reload
	movl	%esi, %esi
	movl	%esi, %edx
	movq	1032(%rsp), %rdi        # 8-byte Reload
	movq	1072(%rsp), %rsi        # 8-byte Reload
	movq	984(%rsp), %rcx         # 8-byte Reload
	movq	1064(%rsp), %r8         # 8-byte Reload
	movq	920(%rsp), %r9          # 8-byte Reload
	movq	912(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	#DEBUG_VALUE: store_kernel:store_vals <- [RSP+1016]
	#DEBUG_VALUE: store_kernel:store_loc <- [RSP+1008]
	#DEBUG_VALUE: store_kernel:num_vals <- [RSP+1124]
	.loc	2 26 0                  # store_kernel/store_kernel.c:26:0
	movq	960(%rsp), %rdi         # 8-byte Reload
	movq	1144(%rsp), %rsi        # 8-byte Reload
	movq	1136(%rsp), %rdx        # 8-byte Reload
	movq	1048(%rsp), %rcx        # 8-byte Reload
	movq	1040(%rsp), %r8         # 8-byte Reload
	movl	1120(%rsp), %r9d        # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	movq	984(%rsp), %rdi         # 8-byte Reload
	movq	992(%rsp), %rsi         # 8-byte Reload
	movq	1016(%rsp), %rdx        # 8-byte Reload
	movq	984(%rsp), %rcx         # 8-byte Reload
	movq	936(%rsp), %r8          # 8-byte Reload
	movq	920(%rsp), %r9          # 8-byte Reload
	movq	912(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	1016(%rsp), %rax        # 8-byte Reload
	movq	1104(%rsp), %rdi        # 8-byte Reload
	movq	992(%rsp), %rsi         # 8-byte Reload
	movq	%rax, %rdx
	movq	984(%rsp), %rcx         # 8-byte Reload
	movq	936(%rsp), %r8          # 8-byte Reload
	movq	920(%rsp), %r9          # 8-byte Reload
	movq	912(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movq	%rax, 904(%rsp)         # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	960(%rsp), %rdi         # 8-byte Reload
	movq	1144(%rsp), %rsi        # 8-byte Reload
	movq	1136(%rsp), %rdx        # 8-byte Reload
	movq	1096(%rsp), %rcx        # 8-byte Reload
	movq	1056(%rsp), %r8         # 8-byte Reload
	movl	1120(%rsp), %r9d        # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	movl	1124(%rsp), %r9d        # 4-byte Reload
	movl	%r9d, %esi
	movl	%esi, %edx
	movq	984(%rsp), %rdi         # 8-byte Reload
	movq	1072(%rsp), %rsi        # 8-byte Reload
	movq	984(%rsp), %rcx         # 8-byte Reload
	movq	1064(%rsp), %r8         # 8-byte Reload
	movq	920(%rsp), %r9          # 8-byte Reload
	movq	912(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	1124(%rsp), %esi        # 4-byte Reload
	movslq	%esi, %rax
	movq	1104(%rsp), %rdi        # 8-byte Reload
	movq	992(%rsp), %rsi         # 8-byte Reload
	movq	%rax, %rdx
	movq	984(%rsp), %rcx         # 8-byte Reload
	movq	1096(%rsp), %r8         # 8-byte Reload
	movq	920(%rsp), %r9          # 8-byte Reload
	movq	912(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movq	%rax, 896(%rsp)         # 8-byte Spill
	callq	trace_logger_log_int
	movq	960(%rsp), %rdi         # 8-byte Reload
	movq	1144(%rsp), %rsi        # 8-byte Reload
	movq	1136(%rsp), %rdx        # 8-byte Reload
	movq	976(%rsp), %rcx         # 8-byte Reload
	movq	1080(%rsp), %r8         # 8-byte Reload
	movl	1120(%rsp), %r9d        # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	movq	928(%rsp), %rdi         # 8-byte Reload
	movq	992(%rsp), %rsi         # 8-byte Reload
	movq	928(%rsp), %rdx         # 8-byte Reload
	movq	920(%rsp), %rcx         # 8-byte Reload
	movq	1088(%rsp), %r8         # 8-byte Reload
	movq	920(%rsp), %r9          # 8-byte Reload
	movq	912(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	984(%rsp), %rdi         # 8-byte Reload
	movq	992(%rsp), %rsi         # 8-byte Reload
	movq	896(%rsp), %rdx         # 8-byte Reload
	movq	984(%rsp), %rcx         # 8-byte Reload
	movq	1096(%rsp), %r8         # 8-byte Reload
	movq	920(%rsp), %r9          # 8-byte Reload
	movq	912(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	896(%rsp), %rax         # 8-byte Reload
	shlq	$2, %rax
	movq	1104(%rsp), %rdi        # 8-byte Reload
	movq	992(%rsp), %rsi         # 8-byte Reload
	movq	%rax, %rdx
	movq	984(%rsp), %rcx         # 8-byte Reload
	movq	976(%rsp), %r8          # 8-byte Reload
	movq	920(%rsp), %r9          # 8-byte Reload
	movq	912(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movq	%rax, 888(%rsp)         # 8-byte Spill
	callq	trace_logger_log_int
	movq	960(%rsp), %rdi         # 8-byte Reload
	movq	1144(%rsp), %rsi        # 8-byte Reload
	movq	1136(%rsp), %rdx        # 8-byte Reload
	movq	1128(%rsp), %rcx        # 8-byte Reload
	movq	1112(%rsp), %r8         # 8-byte Reload
	movl	1120(%rsp), %r9d        # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	movq	944(%rsp), %rdi         # 8-byte Reload
	movq	992(%rsp), %rsi         # 8-byte Reload
	movq	952(%rsp), %rdx         # 8-byte Reload
	movq	984(%rsp), %rcx         # 8-byte Reload
	movq	968(%rsp), %r8          # 8-byte Reload
	movq	920(%rsp), %r9          # 8-byte Reload
	movq	912(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	984(%rsp), %rdi         # 8-byte Reload
	movq	992(%rsp), %rsi         # 8-byte Reload
	movq	904(%rsp), %rdx         # 8-byte Reload
	movq	984(%rsp), %rcx         # 8-byte Reload
	movq	936(%rsp), %r8          # 8-byte Reload
	movq	920(%rsp), %r9          # 8-byte Reload
	movq	912(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	928(%rsp), %rdi         # 8-byte Reload
	movq	992(%rsp), %rsi         # 8-byte Reload
	movq	904(%rsp), %rdx         # 8-byte Reload
	movq	984(%rsp), %rcx         # 8-byte Reload
	movq	936(%rsp), %r8          # 8-byte Reload
	movq	920(%rsp), %r9          # 8-byte Reload
	movq	912(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	1152(%rsp), %rdi        # 8-byte Reload
	movq	992(%rsp), %rsi         # 8-byte Reload
	movq	888(%rsp), %rdx         # 8-byte Reload
	movq	984(%rsp), %rcx         # 8-byte Reload
	movq	976(%rsp), %r8          # 8-byte Reload
	movq	920(%rsp), %r9          # 8-byte Reload
	movq	912(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	904(%rsp), %rdi         # 8-byte Reload
	movq	904(%rsp), %rsi         # 8-byte Reload
	movq	888(%rsp), %rdx         # 8-byte Reload
	callq	dmaLoad
.Ltmp42:
	movabsq	$3, %rdi
	movabsq	$64, %rcx
	movabsq	$1, %rdx
	leaq	.L.str8, %r8
	movabsq	$0, %rsi
	leaq	.L.str510, %r9
	movabsq	$2, %r10
	leaq	.L.str13, %r11
	movabsq	$4, %rbx
	leaq	dmaLoad, %r14
	leaq	.L.str11, %r15
	movabsq	$27, %r12
	leaq	.L.str16, %r13
	leaq	.L.str27, %rbp
	movq	%rcx, 880(%rsp)         # 8-byte Spill
	leaq	.L.str14, %rcx
	movq	%rcx, 872(%rsp)         # 8-byte Spill
	movabsq	$99, %rcx
	movl	%eax, 868(%rsp)         # 4-byte Spill
	movl	$1, %eax
	movl	%eax, 864(%rsp)         # 4-byte Spill
	movl	$0, %eax
	movq	%rcx, 856(%rsp)         # 8-byte Spill
	movabsq	$19134, %rcx            # imm = 0x4ABE
	movq	%rcx, 848(%rsp)         # 8-byte Spill
	leaq	.L.str12, %rcx
	movq	%rcx, 840(%rsp)         # 8-byte Spill
	movabsq	$44, %rcx
	movq	%rcx, 832(%rsp)         # 8-byte Spill
	movabsq	$32, %rcx
	movq	%rcx, 824(%rsp)         # 8-byte Spill
	leaq	.L.str10, %rcx
	.loc	2 27 0                  # store_kernel/store_kernel.c:27:0
	movl	%eax, 820(%rsp)         # 4-byte Spill
	movl	868(%rsp), %eax         # 4-byte Reload
	movl	%eax, %eax
                                        # kill: RAX<def> EAX<kill>
	movq	%rax, 808(%rsp)         # 8-byte Spill
	movq	848(%rsp), %rax         # 8-byte Reload
	movq	%rdi, 800(%rsp)         # 8-byte Spill
	movq	%rax, %rdi
	movq	824(%rsp), %rax         # 8-byte Reload
	movq	%rsi, 792(%rsp)         # 8-byte Spill
	movq	%rax, %rsi
	movq	808(%rsp), %rax         # 8-byte Reload
	movq	%rdx, 784(%rsp)         # 8-byte Spill
	movq	%rax, %rdx
	movq	784(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 776(%rsp)         # 8-byte Spill
	movq	%rax, %rcx
	movq	776(%rsp), %rax         # 8-byte Reload
	movq	%r8, 768(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movq	792(%rsp), %rax         # 8-byte Reload
	movq	%r9, 760(%rsp)          # 8-byte Spill
	movq	%rax, %r9
	movq	760(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	movq	%rbp, 752(%rsp)         # 8-byte Spill
	movq	%r15, 744(%rsp)         # 8-byte Spill
	movq	%r12, 736(%rsp)         # 8-byte Spill
	movq	%r14, 728(%rsp)         # 8-byte Spill
	movq	%r13, 720(%rsp)         # 8-byte Spill
	movq	%rbx, 712(%rsp)         # 8-byte Spill
	movq	%r10, 704(%rsp)         # 8-byte Spill
	movq	%r11, 696(%rsp)         # 8-byte Spill
	callq	trace_logger_log_int
	movq	736(%rsp), %rdi         # 8-byte Reload
	movq	720(%rsp), %rsi         # 8-byte Reload
	movq	752(%rsp), %rdx         # 8-byte Reload
	movq	840(%rsp), %rcx         # 8-byte Reload
	movq	832(%rsp), %r8          # 8-byte Reload
	movl	864(%rsp), %r9d         # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	movq	784(%rsp), %rdi         # 8-byte Reload
	movq	880(%rsp), %rsi         # 8-byte Reload
	movq	1008(%rsp), %rdx        # 8-byte Reload
	movq	784(%rsp), %rcx         # 8-byte Reload
	movq	696(%rsp), %r8          # 8-byte Reload
	movq	792(%rsp), %r9          # 8-byte Reload
	movq	760(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	1008(%rsp), %rax        # 8-byte Reload
	movq	848(%rsp), %rdi         # 8-byte Reload
	movq	880(%rsp), %rsi         # 8-byte Reload
	movq	%rax, %rdx
	movq	784(%rsp), %rcx         # 8-byte Reload
	movq	696(%rsp), %r8          # 8-byte Reload
	movq	792(%rsp), %r9          # 8-byte Reload
	movq	760(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movq	%rax, 688(%rsp)         # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	736(%rsp), %rdi         # 8-byte Reload
	movq	720(%rsp), %rsi         # 8-byte Reload
	movq	752(%rsp), %rdx         # 8-byte Reload
	movq	872(%rsp), %rcx         # 8-byte Reload
	movq	856(%rsp), %r8          # 8-byte Reload
	movl	864(%rsp), %r9d         # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	movq	712(%rsp), %rdi         # 8-byte Reload
	movq	880(%rsp), %rsi         # 8-byte Reload
	movq	728(%rsp), %rdx         # 8-byte Reload
	movq	784(%rsp), %rcx         # 8-byte Reload
	movq	744(%rsp), %r8          # 8-byte Reload
	movq	792(%rsp), %r9          # 8-byte Reload
	movq	760(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	784(%rsp), %rdi         # 8-byte Reload
	movq	880(%rsp), %rsi         # 8-byte Reload
	movq	688(%rsp), %rdx         # 8-byte Reload
	movq	784(%rsp), %rcx         # 8-byte Reload
	movq	696(%rsp), %r8          # 8-byte Reload
	movq	792(%rsp), %r9          # 8-byte Reload
	movq	760(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	704(%rsp), %rdi         # 8-byte Reload
	movq	880(%rsp), %rsi         # 8-byte Reload
	movq	688(%rsp), %rdx         # 8-byte Reload
	movq	784(%rsp), %rcx         # 8-byte Reload
	movq	696(%rsp), %r8          # 8-byte Reload
	movq	792(%rsp), %r9          # 8-byte Reload
	movq	760(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	800(%rsp), %rdi         # 8-byte Reload
	movq	880(%rsp), %rsi         # 8-byte Reload
	movq	888(%rsp), %rdx         # 8-byte Reload
	movq	784(%rsp), %rcx         # 8-byte Reload
	movq	768(%rsp), %r8          # 8-byte Reload
	movq	792(%rsp), %r9          # 8-byte Reload
	movq	760(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	688(%rsp), %rdi         # 8-byte Reload
	movq	688(%rsp), %rsi         # 8-byte Reload
	movq	888(%rsp), %rdx         # 8-byte Reload
	callq	dmaLoad
	movabsq	$0, %rcx
	movabsq	$1, %rdx
	leaq	.L.str15, %rsi
	leaq	.L.str510, %rdi
	movabsq	$2, %r8
	leaq	.L.str18, %r9
	movabsq	$3, %r10
	leaq	.L.str17, %r11
	movabsq	$30, %rbx
	leaq	.L.str16, %r14
	leaq	.L.str27, %r15
	leaq	.L.str1611, %r12
	movl	$1, %ebp
	movl	$0, %r13d
	movq	%rcx, 680(%rsp)         # 8-byte Spill
	movabsq	$19134, %rcx            # imm = 0x4ABE
	movq	%rcx, 672(%rsp)         # 8-byte Spill
	movabsq	$32, %rcx
	movq	%rcx, 664(%rsp)         # 8-byte Spill
	leaq	.L.str7, %rcx
	movq	%rcx, 656(%rsp)         # 8-byte Spill
	leaq	.L.str9, %rcx
	movq	%rcx, 648(%rsp)         # 8-byte Spill
	movabsq	$46, %rcx
	movq	%rcx, 640(%rsp)         # 8-byte Spill
	leaq	.L.str14, %rcx
	.loc	2 30 0                  # store_kernel/store_kernel.c:30:0
.Ltmp43:
	movl	%eax, %eax
                                        # kill: RAX<def> EAX<kill>
	movq	%rax, 632(%rsp)         # 8-byte Spill
	movq	672(%rsp), %rax         # 8-byte Reload
	movq	%rdi, 624(%rsp)         # 8-byte Spill
	movq	%rax, %rdi
	movq	664(%rsp), %rax         # 8-byte Reload
	movq	%rsi, 616(%rsp)         # 8-byte Spill
	movq	%rax, %rsi
	movq	632(%rsp), %rax         # 8-byte Reload
	movq	%rdx, 608(%rsp)         # 8-byte Spill
	movq	%rax, %rdx
	movq	608(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 600(%rsp)         # 8-byte Spill
	movq	%rax, %rcx
	movq	600(%rsp), %rax         # 8-byte Reload
	movq	%r8, 592(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movq	680(%rsp), %rax         # 8-byte Reload
	movq	%r9, 584(%rsp)          # 8-byte Spill
	movq	%rax, %r9
	movq	624(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	movl	%r13d, 580(%rsp)        # 4-byte Spill
	movq	%r15, 568(%rsp)         # 8-byte Spill
	movq	%r12, 560(%rsp)         # 8-byte Spill
	movl	%ebp, 556(%rsp)         # 4-byte Spill
	movq	%r14, 544(%rsp)         # 8-byte Spill
	movq	%rbx, 536(%rsp)         # 8-byte Spill
	movq	%r10, 528(%rsp)         # 8-byte Spill
	movq	%r11, 520(%rsp)         # 8-byte Spill
	callq	trace_logger_log_int
.Ltmp44:
	#DEBUG_VALUE: i <- 0
	movq	536(%rsp), %rdi         # 8-byte Reload
	movq	544(%rsp), %rsi         # 8-byte Reload
	movq	568(%rsp), %rdx         # 8-byte Reload
	movq	616(%rsp), %rcx         # 8-byte Reload
	movq	640(%rsp), %r8          # 8-byte Reload
	movl	556(%rsp), %r9d         # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	movq	592(%rsp), %rdi         # 8-byte Reload
	movq	664(%rsp), %rsi         # 8-byte Reload
	movq	680(%rsp), %rdx         # 8-byte Reload
	movq	680(%rsp), %rcx         # 8-byte Reload
	movq	648(%rsp), %r8          # 8-byte Reload
	movq	680(%rsp), %r9          # 8-byte Reload
	movq	624(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	1124(%rsp), %ebp        # 4-byte Reload
	movl	%ebp, %r13d
	movl	%r13d, %edx
	movq	608(%rsp), %rdi         # 8-byte Reload
	movq	664(%rsp), %rsi         # 8-byte Reload
	movq	608(%rsp), %rcx         # 8-byte Reload
	movq	656(%rsp), %r8          # 8-byte Reload
	movq	680(%rsp), %r9          # 8-byte Reload
	movq	624(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	1124(%rsp), %ebp        # 4-byte Reload
	cmpl	$0, %ebp
	setg	%al
	movb	%al, %cl
	andb	$1, %cl
	movzbl	%cl, %r13d
	movl	%r13d, %edx
	movq	672(%rsp), %rdi         # 8-byte Reload
	movq	608(%rsp), %rsi         # 8-byte Reload
	movq	608(%rsp), %rcx         # 8-byte Reload
	movq	616(%rsp), %r8          # 8-byte Reload
	movq	680(%rsp), %r9          # 8-byte Reload
	movq	624(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movb	%al, 519(%rsp)          # 1-byte Spill
	callq	trace_logger_log_int
	movq	536(%rsp), %rdi         # 8-byte Reload
	movq	544(%rsp), %rsi         # 8-byte Reload
	movq	568(%rsp), %rdx         # 8-byte Reload
	movq	560(%rsp), %rcx         # 8-byte Reload
	movq	592(%rsp), %r8          # 8-byte Reload
	movl	556(%rsp), %r9d         # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	movq	528(%rsp), %rdi         # 8-byte Reload
	movq	680(%rsp), %rsi         # 8-byte Reload
	movq	680(%rsp), %rdx         # 8-byte Reload
	movq	608(%rsp), %rcx         # 8-byte Reload
	movq	520(%rsp), %r8          # 8-byte Reload
	movq	680(%rsp), %r9          # 8-byte Reload
	movq	624(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	592(%rsp), %rdi         # 8-byte Reload
	movq	680(%rsp), %rsi         # 8-byte Reload
	movq	680(%rsp), %rdx         # 8-byte Reload
	movq	608(%rsp), %rcx         # 8-byte Reload
	movq	584(%rsp), %r8          # 8-byte Reload
	movq	680(%rsp), %r9          # 8-byte Reload
	movq	624(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	519(%rsp), %al          # 1-byte Reload
	andb	$1, %al
	movzbl	%al, %ebp
	movl	%ebp, %edx
	movq	608(%rsp), %rdi         # 8-byte Reload
	movq	608(%rsp), %rsi         # 8-byte Reload
	movq	608(%rsp), %rcx         # 8-byte Reload
	movq	616(%rsp), %r8          # 8-byte Reload
	movq	680(%rsp), %r9          # 8-byte Reload
	movq	624(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	519(%rsp), %al          # 1-byte Reload
	testb	$1, %al
	movq	680(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, 504(%rsp)         # 8-byte Spill
	jne	.LBB1_1
	jmp	.LBB1_2
.Ltmp45:
.LBB1_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	504(%rsp), %rax         # 8-byte Reload
	movabsq	$1, %rcx
	leaq	.L.str26, %rdx
	movabsq	$0, %rsi
	leaq	.L.str510, %rdi
	movabsq	$2, %r8
	leaq	.L.str17, %r9
	movabsq	$3, %r10
	leaq	.L.str18, %r11
	movabsq	$30, %rbx
	leaq	.L.str16, %r14
	leaq	.L.str2712, %r15
	movl	$1, %ebp
	movl	$0, %r12d
	movabsq	$19134, %r13            # imm = 0x4ABE
	movq	%rax, 496(%rsp)         # 8-byte Spill
	movabsq	$32, %rax
	movq	%rax, 488(%rsp)         # 8-byte Spill
	leaq	.L.str25, %rax
	movq	%rax, 480(%rsp)         # 8-byte Spill
	leaq	.L.str7, %rax
	movq	%rax, 472(%rsp)         # 8-byte Spill
	movabsq	$46, %rax
	movq	%rax, 464(%rsp)         # 8-byte Spill
	movabsq	$64, %rax
	movq	%rax, 456(%rsp)         # 8-byte Spill
	leaq	.L.str20, %rax
	movq	%rax, 448(%rsp)         # 8-byte Spill
	movabsq	$33, %rax
	movq	%rax, 440(%rsp)         # 8-byte Spill
	leaq	.L.str19, %rax
	movq	%rax, 432(%rsp)         # 8-byte Spill
	leaq	.L.str9, %rax
	movq	%rax, 424(%rsp)         # 8-byte Spill
	movabsq	$8, %rax
	movq	%rax, 416(%rsp)         # 8-byte Spill
	leaq	.L.str22, %rax
	movq	%rax, 408(%rsp)         # 8-byte Spill
	leaq	.L.str23, %rax
	movq	%rax, 400(%rsp)         # 8-byte Spill
	movabsq	$31, %rax
	movq	%rax, 392(%rsp)         # 8-byte Spill
	leaq	.L.str24, %rax
	movq	%rax, 384(%rsp)         # 8-byte Spill
	movabsq	$28, %rax
	movq	%rax, 376(%rsp)         # 8-byte Spill
	leaq	.L.str13, %rax
	movq	%rax, 368(%rsp)         # 8-byte Spill
	movabsq	$29, %rax
	movq	%rax, 360(%rsp)         # 8-byte Spill
	leaq	.L.str21, %rax
	movq	%rax, 352(%rsp)         # 8-byte Spill
	movabsq	$27, %rax
	movq	%rax, 344(%rsp)         # 8-byte Spill
	leaq	.L.str49, %rax
	movq	%rax, 336(%rsp)         # 8-byte Spill
	leaq	.L.str27, %rax
	movq	%rax, 328(%rsp)         # 8-byte Spill
	movabsq	$-1, %rax
	movq	%rax, 320(%rsp)         # 8-byte Spill
	movabsq	$48, %rax
	.loc	2 31 0                  # store_kernel/store_kernel.c:31:0
	movq	%rax, 312(%rsp)         # 8-byte Spill
	movq	320(%rsp), %rax         # 8-byte Reload
	movq	%rdi, 304(%rsp)         # 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, 296(%rsp)         # 8-byte Spill
	movq	%r14, %rsi
	movq	%rdx, 288(%rsp)         # 8-byte Spill
	movq	%r9, %rdx
	movq	432(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 280(%rsp)         # 8-byte Spill
	movq	%rax, %rcx
	movq	312(%rsp), %rax         # 8-byte Reload
	movq	%r8, 272(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movq	%r9, 264(%rsp)          # 8-byte Spill
	movl	%ebp, %r9d
	movl	$0, (%rsp)
	movq	%r15, 256(%rsp)         # 8-byte Spill
	movl	%ebp, 252(%rsp)         # 4-byte Spill
	movl	%r12d, 248(%rsp)        # 4-byte Spill
	movq	%r13, 240(%rsp)         # 8-byte Spill
	movq	%r14, 232(%rsp)         # 8-byte Spill
	movq	%rbx, 224(%rsp)         # 8-byte Spill
	movq	%r10, 216(%rsp)         # 8-byte Spill
	movq	%r11, 208(%rsp)         # 8-byte Spill
	callq	trace_logger_log0
	movq	272(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	296(%rsp), %rdx         # 8-byte Reload
	movq	296(%rsp), %rcx         # 8-byte Reload
	movq	424(%rsp), %r8          # 8-byte Reload
	movq	280(%rsp), %r9          # 8-byte Reload
	movq	328(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	280(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	296(%rsp), %rdx         # 8-byte Reload
	movq	280(%rsp), %rcx         # 8-byte Reload
	movq	448(%rsp), %r8          # 8-byte Reload
	movq	280(%rsp), %r9          # 8-byte Reload
	movq	264(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	240(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	496(%rsp), %rdx         # 8-byte Reload
	movq	280(%rsp), %rcx         # 8-byte Reload
	movq	432(%rsp), %r8          # 8-byte Reload
	movq	296(%rsp), %r9          # 8-byte Reload
	movq	264(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	392(%rsp), %rdi         # 8-byte Reload
	movq	232(%rsp), %rsi         # 8-byte Reload
	movq	264(%rsp), %rdx         # 8-byte Reload
	movq	352(%rsp), %rcx         # 8-byte Reload
	movq	360(%rsp), %r8          # 8-byte Reload
	movl	252(%rsp), %r9d         # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	movq	272(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	496(%rsp), %rdx         # 8-byte Reload
	movq	280(%rsp), %rcx         # 8-byte Reload
	movq	432(%rsp), %r8          # 8-byte Reload
	movq	296(%rsp), %r9          # 8-byte Reload
	movq	304(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	280(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	1016(%rsp), %rdx        # 8-byte Reload
	movq	280(%rsp), %rcx         # 8-byte Reload
	movq	336(%rsp), %r8          # 8-byte Reload
	movq	296(%rsp), %r9          # 8-byte Reload
	movq	304(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	496(%rsp), %rax         # 8-byte Reload
	shlq	$2, %rax
	movq	1016(%rsp), %rcx        # 8-byte Reload
	addq	%rax, %rcx
	movq	240(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	%rcx, %rdx
	movq	280(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 200(%rsp)         # 8-byte Spill
	movq	%rax, %rcx
	movq	352(%rsp), %r8          # 8-byte Reload
	movq	296(%rsp), %r9          # 8-byte Reload
	movq	304(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	392(%rsp), %rdi         # 8-byte Reload
	movq	232(%rsp), %rsi         # 8-byte Reload
	movq	264(%rsp), %rdx         # 8-byte Reload
	movq	408(%rsp), %rcx         # 8-byte Reload
	movq	344(%rsp), %r8          # 8-byte Reload
	movl	252(%rsp), %r9d         # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	movq	280(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	200(%rsp), %rdx         # 8-byte Reload
	movq	280(%rsp), %rcx         # 8-byte Reload
	movq	352(%rsp), %r8          # 8-byte Reload
	movq	296(%rsp), %r9          # 8-byte Reload
	movq	304(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	1016(%rsp), %rax        # 8-byte Reload
	movq	496(%rsp), %rcx         # 8-byte Reload
	movl	(%rax,%rcx,4), %ebp
	movl	%ebp, %r12d
	movl	%r12d, %edx
	movq	240(%rsp), %rdi         # 8-byte Reload
	movq	488(%rsp), %rsi         # 8-byte Reload
	movq	280(%rsp), %rcx         # 8-byte Reload
	movq	408(%rsp), %r8          # 8-byte Reload
	movq	296(%rsp), %r9          # 8-byte Reload
	movq	304(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movl	%ebp, 196(%rsp)         # 4-byte Spill
	callq	trace_logger_log_int
	movq	392(%rsp), %rdi         # 8-byte Reload
	movq	232(%rsp), %rsi         # 8-byte Reload
	movq	264(%rsp), %rdx         # 8-byte Reload
	movq	400(%rsp), %rcx         # 8-byte Reload
	movq	360(%rsp), %r8          # 8-byte Reload
	movl	252(%rsp), %r9d         # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	movq	272(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	496(%rsp), %rdx         # 8-byte Reload
	movq	280(%rsp), %rcx         # 8-byte Reload
	movq	432(%rsp), %r8          # 8-byte Reload
	movq	296(%rsp), %r9          # 8-byte Reload
	movq	304(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	280(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	1008(%rsp), %rdx        # 8-byte Reload
	movq	280(%rsp), %rcx         # 8-byte Reload
	movq	368(%rsp), %r8          # 8-byte Reload
	movq	296(%rsp), %r9          # 8-byte Reload
	movq	304(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	496(%rsp), %rax         # 8-byte Reload
	shlq	$2, %rax
	movq	1008(%rsp), %rcx        # 8-byte Reload
	addq	%rax, %rcx
	movq	240(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	%rcx, %rdx
	movq	280(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 184(%rsp)         # 8-byte Spill
	movq	%rax, %rcx
	movq	400(%rsp), %r8          # 8-byte Reload
	movq	296(%rsp), %r9          # 8-byte Reload
	movq	304(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	392(%rsp), %rdi         # 8-byte Reload
	movq	232(%rsp), %rsi         # 8-byte Reload
	movq	264(%rsp), %rdx         # 8-byte Reload
	movq	384(%rsp), %rcx         # 8-byte Reload
	movq	376(%rsp), %r8          # 8-byte Reload
	movl	252(%rsp), %r9d         # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	movq	272(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	184(%rsp), %rdx         # 8-byte Reload
	movq	280(%rsp), %rcx         # 8-byte Reload
	movq	400(%rsp), %r8          # 8-byte Reload
	movq	296(%rsp), %r9          # 8-byte Reload
	movq	304(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movl	196(%rsp), %ebp         # 4-byte Reload
	movl	%ebp, %r12d
	movl	%r12d, %edx
	movq	280(%rsp), %rdi         # 8-byte Reload
	movq	488(%rsp), %rsi         # 8-byte Reload
	movq	280(%rsp), %rcx         # 8-byte Reload
	movq	408(%rsp), %r8          # 8-byte Reload
	movq	296(%rsp), %r9          # 8-byte Reload
	movq	304(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	1008(%rsp), %rax        # 8-byte Reload
	movq	496(%rsp), %rcx         # 8-byte Reload
	movl	196(%rsp), %ebp         # 4-byte Reload
	movl	%ebp, (%rax,%rcx,4)
	.loc	2 30 0                  # store_kernel/store_kernel.c:30:0
	movq	224(%rsp), %rdi         # 8-byte Reload
	movq	232(%rsp), %rsi         # 8-byte Reload
	movq	264(%rsp), %rdx         # 8-byte Reload
	movq	448(%rsp), %rcx         # 8-byte Reload
	movq	416(%rsp), %r8          # 8-byte Reload
	movl	252(%rsp), %r9d         # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	movq	272(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	280(%rsp), %rdx         # 8-byte Reload
	movq	296(%rsp), %rcx         # 8-byte Reload
	movq	424(%rsp), %r8          # 8-byte Reload
	movq	296(%rsp), %r9          # 8-byte Reload
	movq	304(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	280(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	496(%rsp), %rdx         # 8-byte Reload
	movq	280(%rsp), %rcx         # 8-byte Reload
	movq	432(%rsp), %r8          # 8-byte Reload
	movq	296(%rsp), %r9          # 8-byte Reload
	movq	304(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	496(%rsp), %rax         # 8-byte Reload
	addq	$1, %rax
	movq	240(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	%rax, %rdx
	movq	280(%rsp), %rcx         # 8-byte Reload
	movq	448(%rsp), %r8          # 8-byte Reload
	movq	296(%rsp), %r9          # 8-byte Reload
	movq	304(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movq	%rax, 176(%rsp)         # 8-byte Spill
	callq	trace_logger_log_int
	movq	224(%rsp), %rdi         # 8-byte Reload
	movq	232(%rsp), %rsi         # 8-byte Reload
	movq	264(%rsp), %rdx         # 8-byte Reload
	movq	480(%rsp), %rcx         # 8-byte Reload
	movq	440(%rsp), %r8          # 8-byte Reload
	movl	252(%rsp), %r9d         # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	movq	280(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	176(%rsp), %rdx         # 8-byte Reload
	movq	280(%rsp), %rcx         # 8-byte Reload
	movq	448(%rsp), %r8          # 8-byte Reload
	movq	296(%rsp), %r9          # 8-byte Reload
	movq	304(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	176(%rsp), %rax         # 8-byte Reload
	movl	%eax, %ebp
	movl	%ebp, %r12d
	movl	%r12d, %edx
	movq	240(%rsp), %rdi         # 8-byte Reload
	movq	488(%rsp), %rsi         # 8-byte Reload
	movq	280(%rsp), %rcx         # 8-byte Reload
	movq	480(%rsp), %r8          # 8-byte Reload
	movq	296(%rsp), %r9          # 8-byte Reload
	movq	304(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movl	%ebp, 172(%rsp)         # 4-byte Spill
	callq	trace_logger_log_int
	movq	224(%rsp), %rdi         # 8-byte Reload
	movq	232(%rsp), %rsi         # 8-byte Reload
	movq	264(%rsp), %rdx         # 8-byte Reload
	movq	288(%rsp), %rcx         # 8-byte Reload
	movq	464(%rsp), %r8          # 8-byte Reload
	movl	252(%rsp), %r9d         # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	movl	1124(%rsp), %r9d        # 4-byte Reload
	movl	%r9d, %ebp
	movl	%ebp, %edx
	movq	272(%rsp), %rdi         # 8-byte Reload
	movq	488(%rsp), %rsi         # 8-byte Reload
	movq	280(%rsp), %rcx         # 8-byte Reload
	movq	472(%rsp), %r8          # 8-byte Reload
	movq	296(%rsp), %r9          # 8-byte Reload
	movq	304(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	172(%rsp), %ebp         # 4-byte Reload
	movl	%ebp, %r12d
	movl	%r12d, %edx
	movq	280(%rsp), %rdi         # 8-byte Reload
	movq	488(%rsp), %rsi         # 8-byte Reload
	movq	280(%rsp), %rcx         # 8-byte Reload
	movq	480(%rsp), %r8          # 8-byte Reload
	movq	296(%rsp), %r9          # 8-byte Reload
	movq	304(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	172(%rsp), %ebp         # 4-byte Reload
	movl	1124(%rsp), %r12d       # 4-byte Reload
	cmpl	%r12d, %ebp
	sete	%al
	movb	%al, %cl
	andb	$1, %cl
	movzbl	%cl, %ecx
	movl	%ecx, %edx
	movq	240(%rsp), %rdi         # 8-byte Reload
	movq	280(%rsp), %rsi         # 8-byte Reload
	movq	280(%rsp), %rcx         # 8-byte Reload
	movq	288(%rsp), %r8          # 8-byte Reload
	movq	296(%rsp), %r9          # 8-byte Reload
	movq	304(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movb	%al, 171(%rsp)          # 1-byte Spill
	callq	trace_logger_log_int
	movq	224(%rsp), %rdi         # 8-byte Reload
	movq	232(%rsp), %rsi         # 8-byte Reload
	movq	264(%rsp), %rdx         # 8-byte Reload
	movq	256(%rsp), %rcx         # 8-byte Reload
	movq	272(%rsp), %r8          # 8-byte Reload
	movl	252(%rsp), %r9d         # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	movq	216(%rsp), %rdi         # 8-byte Reload
	movq	296(%rsp), %rsi         # 8-byte Reload
	movq	296(%rsp), %rdx         # 8-byte Reload
	movq	280(%rsp), %rcx         # 8-byte Reload
	movq	208(%rsp), %r8          # 8-byte Reload
	movq	296(%rsp), %r9          # 8-byte Reload
	movq	304(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	272(%rsp), %rdi         # 8-byte Reload
	movq	296(%rsp), %rsi         # 8-byte Reload
	movq	296(%rsp), %rdx         # 8-byte Reload
	movq	280(%rsp), %rcx         # 8-byte Reload
	movq	264(%rsp), %r8          # 8-byte Reload
	movq	296(%rsp), %r9          # 8-byte Reload
	movq	304(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	171(%rsp), %al          # 1-byte Reload
	andb	$1, %al
	movzbl	%al, %ebp
	movl	%ebp, %edx
	movq	280(%rsp), %rdi         # 8-byte Reload
	movq	280(%rsp), %rsi         # 8-byte Reload
	movq	280(%rsp), %rcx         # 8-byte Reload
	movq	288(%rsp), %r8          # 8-byte Reload
	movq	296(%rsp), %r9          # 8-byte Reload
	movq	304(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	171(%rsp), %al          # 1-byte Reload
	testb	$1, %al
	movq	176(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, 504(%rsp)         # 8-byte Spill
	jne	.LBB1_2
	jmp	.LBB1_1
.Ltmp46:
.LBB1_2:                                # %._crit_edge
	movabsq	$3, %rdi
	movabsq	$64, %rax
	movabsq	$1, %rcx
	leaq	.L.str8, %r8
	movabsq	$0, %rdx
	leaq	.L.str510, %rsi
	movabsq	$2, %r9
	leaq	.L.str13, %r10
	movabsq	$4, %r11
	leaq	dmaStore, %rbx
	leaq	.L.str29, %r14
	movabsq	$34, %r15
	leaq	.L.str16, %r12
	leaq	.L.str18, %r13
	leaq	.L.str28, %rbp
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movabsq	$98, %rax
	movq	%rsi, 152(%rsp)         # 8-byte Spill
	movl	$1, %esi
	movl	%esi, 148(%rsp)         # 4-byte Spill
	movl	$0, %esi
	.loc	2 34 0                  # store_kernel/store_kernel.c:34:0
	movq	%rdi, 136(%rsp)         # 8-byte Spill
	movq	%r15, %rdi
	movl	%esi, 132(%rsp)         # 4-byte Spill
	movq	%r12, %rsi
	movq	%rdx, 120(%rsp)         # 8-byte Spill
	movq	%r13, %rdx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movq	%rbp, %rcx
	movq	%r8, 104(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movl	148(%rsp), %ebp         # 4-byte Reload
	movq	%r9, 96(%rsp)           # 8-byte Spill
	movl	%ebp, %r9d
	movl	$0, (%rsp)
	movq	%r14, 88(%rsp)          # 8-byte Spill
	movq	%r10, 80(%rsp)          # 8-byte Spill
	movq	%r11, 72(%rsp)          # 8-byte Spill
	movq	%rbx, 64(%rsp)          # 8-byte Spill
	callq	trace_logger_log0
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	160(%rsp), %rsi         # 8-byte Reload
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	112(%rsp), %rcx         # 8-byte Reload
	movq	88(%rsp), %r8           # 8-byte Reload
	movq	120(%rsp), %r9          # 8-byte Reload
	movq	152(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	112(%rsp), %rdi         # 8-byte Reload
	movq	160(%rsp), %rsi         # 8-byte Reload
	movq	688(%rsp), %rdx         # 8-byte Reload
	movq	112(%rsp), %rcx         # 8-byte Reload
	movq	80(%rsp), %r8           # 8-byte Reload
	movq	120(%rsp), %r9          # 8-byte Reload
	movq	152(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	96(%rsp), %rdi          # 8-byte Reload
	movq	160(%rsp), %rsi         # 8-byte Reload
	movq	688(%rsp), %rdx         # 8-byte Reload
	movq	112(%rsp), %rcx         # 8-byte Reload
	movq	80(%rsp), %r8           # 8-byte Reload
	movq	120(%rsp), %r9          # 8-byte Reload
	movq	152(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	136(%rsp), %rdi         # 8-byte Reload
	movq	160(%rsp), %rsi         # 8-byte Reload
	movq	888(%rsp), %rdx         # 8-byte Reload
	movq	112(%rsp), %rcx         # 8-byte Reload
	movq	104(%rsp), %r8          # 8-byte Reload
	movq	120(%rsp), %r9          # 8-byte Reload
	movq	152(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	688(%rsp), %rdi         # 8-byte Reload
	movq	688(%rsp), %rsi         # 8-byte Reload
	movq	888(%rsp), %rdx         # 8-byte Reload
	callq	dmaStore
	movabsq	$36, %rdi
	leaq	.L.str16, %rsi
	leaq	.L.str18, %rdx
	leaq	.L.str30, %rcx
	movabsq	$1, %r8
	movl	$1, %r9d
	movl	$0, %ebp
	movabsq	$19134, %r10            # imm = 0x4ABE
	movabsq	$32, %r11
	leaq	.L.str28, %rbx
	movabsq	$0, %r14
	leaq	.L.str510, %r15
	.loc	2 36 0                  # store_kernel/store_kernel.c:36:0
	movl	%eax, %eax
	movl	%eax, %r12d
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	movq	%r10, %rdi
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	%r11, %rsi
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	%r12, %rdx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%r8, %rcx
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movq	%rbx, %r8
	movl	%r9d, 20(%rsp)          # 4-byte Spill
	movq	%r14, %r9
	movq	%r15, (%rsp)
	movl	%ebp, 16(%rsp)          # 4-byte Spill
	callq	trace_logger_log_int
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	24(%rsp), %r8           # 8-byte Reload
	movl	20(%rsp), %r9d          # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	addq	$1160, %rsp             # imm = 0x488
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.Ltmp47:
.Ltmp48:
	.size	store_kernel, .Ltmp48-store_kernel
.Lfunc_end1:
	.cfi_endproc

	.globl	test_stores
	.align	16, 0x90
	.type	test_stores,@function
test_stores:                            # @test_stores
	.cfi_startproc
.Lfunc_begin2:
	.loc	2 38 0                  # store_kernel/store_kernel.c:38:0
# BB#0:
	pushq	%rbp
.Ltmp56:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Ltmp57:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Ltmp58:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Ltmp59:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Ltmp60:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Ltmp61:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Ltmp62:
	.cfi_def_cfa_offset 208
.Ltmp63:
	.cfi_offset %rbx, -56
.Ltmp64:
	.cfi_offset %r12, -48
.Ltmp65:
	.cfi_offset %r13, -40
.Ltmp66:
	.cfi_offset %r14, -32
.Ltmp67:
	.cfi_offset %r15, -24
.Ltmp68:
	.cfi_offset %rbp, -16
	movabsq	$0, %rax
	movl	$0, %ecx
	movabsq	$24601, %r8             # imm = 0x6019
	movabsq	$32, %r9
	movabsq	$1, %r10
	leaq	.L.str7, %r11
	leaq	.L.str510, %rbx
	movabsq	$64, %r14
	leaq	.L.str13, %r15
	leaq	.L.str49, %r12
	.loc	2 38 0 prologue_end     # store_kernel/store_kernel.c:38:0
.Ltmp69:
	movq	%rdi, 144(%rsp)         # 8-byte Spill
	movq	%r8, %rdi
	movq	%rsi, 136(%rsp)         # 8-byte Spill
	movq	%r14, %rsi
	movq	144(%rsp), %r13         # 8-byte Reload
	movl	%edx, 132(%rsp)         # 4-byte Spill
	movq	%r13, %rdx
	movl	%ecx, 128(%rsp)         # 4-byte Spill
	movq	%r10, %rcx
	movq	%r8, 120(%rsp)          # 8-byte Spill
	movq	%r12, %r8
	movq	%r9, 112(%rsp)          # 8-byte Spill
	movq	%rax, %r9
	movq	%rbx, (%rsp)
	movq	%r15, 104(%rsp)         # 8-byte Spill
	movq	%rbx, 96(%rsp)          # 8-byte Spill
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	%r14, 80(%rsp)          # 8-byte Spill
	movq	%r11, 72(%rsp)          # 8-byte Spill
	movq	%r10, 64(%rsp)          # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	120(%rsp), %rdi         # 8-byte Reload
	movq	80(%rsp), %rsi          # 8-byte Reload
	movq	136(%rsp), %rdx         # 8-byte Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	104(%rsp), %r8          # 8-byte Reload
	movq	88(%rsp), %r9           # 8-byte Reload
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movl	132(%rsp), %ebp         # 4-byte Reload
	movl	%ebp, %esi
	movl	%esi, %edx
	movq	120(%rsp), %rdi         # 8-byte Reload
	movq	112(%rsp), %rsi         # 8-byte Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	72(%rsp), %r8           # 8-byte Reload
	movq	88(%rsp), %r9           # 8-byte Reload
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	#DEBUG_VALUE: test_stores:store_vals <- [RSP+144]
	#DEBUG_VALUE: test_stores:store_loc <- [RSP+136]
	#DEBUG_VALUE: test_stores:num_vals <- [RSP+132]
.Ltmp70:
	#DEBUG_VALUE: test_stores:num_failures <- 0
	#DEBUG_VALUE: i <- 0
	.loc	2 40 0                  # store_kernel/store_kernel.c:40:0
	movl	132(%rsp), %ebp         # 4-byte Reload
	cmpl	$0, %ebp
	movq	88(%rsp), %rax          # 8-byte Reload
	movl	128(%rsp), %esi         # 4-byte Reload
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	%esi, 52(%rsp)          # 4-byte Spill
	jle	.LBB2_4
.Ltmp71:
.LBB2_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	52(%rsp), %eax          # 4-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	.loc	2 41 0                  # store_kernel/store_kernel.c:41:0
.Ltmp72:
	movq	136(%rsp), %rdx         # 8-byte Reload
	movl	(%rdx,%rcx,4), %esi
	movq	144(%rsp), %rdi         # 8-byte Reload
	movl	(%rdi,%rcx,4), %r8d
	cmpl	%r8d, %esi
	movl	%eax, %r9d
	movl	%eax, 48(%rsp)          # 4-byte Spill
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movl	%esi, 36(%rsp)          # 4-byte Spill
	movl	%r8d, 32(%rsp)          # 4-byte Spill
	movl	%r9d, 28(%rsp)          # 4-byte Spill
	je	.LBB2_3
# BB#2:                                 #   in Loop: Header=BB2_1 Depth=1
	.loc	2 42 0                  # store_kernel/store_kernel.c:42:0
.Ltmp73:
	movq	stdout, %rdi
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ecx
	movl	$.L.str5, %edx
	movl	%edx, %esi
	xorl	%edx, %edx
	movb	%dl, %r8b
	movl	%ecx, %edx
	movl	36(%rsp), %ecx          # 4-byte Reload
	movl	32(%rsp), %r9d          # 4-byte Reload
	movb	%r8b, 27(%rsp)          # 1-byte Spill
	movl	%r9d, %r8d
	movb	27(%rsp), %al           # 1-byte Reload
	callq	fprintf
	.loc	2 44 0                  # store_kernel/store_kernel.c:44:0
	movl	48(%rsp), %ecx          # 4-byte Reload
	addl	$1, %ecx
.Ltmp74:
	#DEBUG_VALUE: test_stores:num_failures <- ECX
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	%ecx, 28(%rsp)          # 4-byte Spill
.Ltmp75:
.LBB2_3:                                #   in Loop: Header=BB2_1 Depth=1
	movl	28(%rsp), %eax          # 4-byte Reload
	.loc	2 40 0                  # store_kernel/store_kernel.c:40:0
	movq	40(%rsp), %rcx          # 8-byte Reload
	addq	$1, %rcx
	movl	%ecx, %edx
	movl	132(%rsp), %esi         # 4-byte Reload
	cmpl	%esi, %edx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movl	%eax, 52(%rsp)          # 4-byte Spill
	jne	.LBB2_1
.Ltmp76:
.LBB2_4:                                # %._crit_edge
	movl	52(%rsp), %eax          # 4-byte Reload
	.loc	2 47 0                  # store_kernel/store_kernel.c:47:0
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.Ltmp77:
.Ltmp78:
	.size	test_stores, .Ltmp78-test_stores
.Lfunc_end2:
	.cfi_endproc

	.globl	test_kernel
	.align	16, 0x90
	.type	test_kernel,@function
test_kernel:                            # @test_kernel
	.cfi_startproc
.Lfunc_begin3:
	.loc	3 24 0                  # test_kernel/test_kernel.c:24:0
# BB#0:
	pushq	%rbp
.Ltmp86:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Ltmp87:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Ltmp88:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Ltmp89:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Ltmp90:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Ltmp91:
	.cfi_def_cfa_offset 56
	subq	$1160, %rsp             # imm = 0x488
.Ltmp92:
	.cfi_def_cfa_offset 1216
.Ltmp93:
	.cfi_offset %rbx, -56
.Ltmp94:
	.cfi_offset %r12, -48
.Ltmp95:
	.cfi_offset %r13, -40
.Ltmp96:
	.cfi_offset %r14, -32
.Ltmp97:
	.cfi_offset %r15, -24
.Ltmp98:
	.cfi_offset %rbp, -16
	movabsq	$3, %rax
	movabsq	$64, %rcx
	movabsq	$1, %r8
	leaq	.L.str839, %r9
	movabsq	$0, %r10
	leaq	.L.str536, %r11
	movabsq	$2, %rbx
	leaq	.L.str435, %r14
	movabsq	$4, %r15
	leaq	dmaLoad, %r12
	leaq	.L.str1142, %r13
	movabsq	$26, %rbp
	movq	%rax, 1152(%rsp)        # 8-byte Spill
	leaq	.L.str132, %rax
	movq	%rax, 1144(%rsp)        # 8-byte Spill
	leaq	.L.str233, %rax
	movq	%rax, 1136(%rsp)        # 8-byte Spill
	leaq	.L.str1041, %rax
	movq	%rax, 1128(%rsp)        # 8-byte Spill
	movabsq	$99, %rax
	movl	%edx, 1124(%rsp)        # 4-byte Spill
	movl	$1, %edx
	movl	%edx, 1120(%rsp)        # 4-byte Spill
	movl	$0, %edx
	movq	%rax, 1112(%rsp)        # 8-byte Spill
	movabsq	$19134, %rax            # imm = 0x4ABE
	movq	%rax, 1104(%rsp)        # 8-byte Spill
	leaq	.L.str637, %rax
	movq	%rax, 1096(%rsp)        # 8-byte Spill
	leaq	.L.str940, %rax
	movq	%rax, 1088(%rsp)        # 8-byte Spill
	movabsq	$20, %rax
	movq	%rax, 1080(%rsp)        # 8-byte Spill
	movabsq	$32, %rax
	movq	%rax, 1072(%rsp)        # 8-byte Spill
	leaq	.L.str738, %rax
	movq	%rax, 1064(%rsp)        # 8-byte Spill
	movabsq	$35, %rax
	movq	%rax, 1056(%rsp)        # 8-byte Spill
	leaq	.L.str334, %rax
	movq	%rax, 1048(%rsp)        # 8-byte Spill
	movabsq	$44, %rax
	movq	%rax, 1040(%rsp)        # 8-byte Spill
	movabsq	$24601, %rax            # imm = 0x6019
	movq	%rax, 1032(%rsp)        # 8-byte Spill
	leaq	.L.str1344, %rax
	.loc	3 24 0 prologue_end     # test_kernel/test_kernel.c:24:0
.Ltmp99:
	movq	%rax, 1024(%rsp)        # 8-byte Spill
	movq	1032(%rsp), %rax        # 8-byte Reload
	movq	%rdi, 1016(%rsp)        # 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, 1008(%rsp)        # 8-byte Spill
	movq	%rcx, %rsi
	movq	1016(%rsp), %rax        # 8-byte Reload
	movl	%edx, 1004(%rsp)        # 4-byte Spill
	movq	%rax, %rdx
	movq	%rcx, 992(%rsp)         # 8-byte Spill
	movq	%r8, %rcx
	movq	%r8, 984(%rsp)          # 8-byte Spill
	movq	%r14, %r8
	movq	%r9, 976(%rsp)          # 8-byte Spill
	movq	%r10, %r9
	movq	%r11, (%rsp)
	movq	%r13, 968(%rsp)         # 8-byte Spill
	movq	%rbp, 960(%rsp)         # 8-byte Spill
	movq	%r12, 952(%rsp)         # 8-byte Spill
	movq	%r15, 944(%rsp)         # 8-byte Spill
	movq	%r14, 936(%rsp)         # 8-byte Spill
	movq	%rbx, 928(%rsp)         # 8-byte Spill
	movq	%r10, 920(%rsp)         # 8-byte Spill
	movq	%r11, 912(%rsp)         # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	1032(%rsp), %rdi        # 8-byte Reload
	movq	992(%rsp), %rsi         # 8-byte Reload
	movq	1008(%rsp), %rdx        # 8-byte Reload
	movq	984(%rsp), %rcx         # 8-byte Reload
	movq	1024(%rsp), %r8         # 8-byte Reload
	movq	920(%rsp), %r9          # 8-byte Reload
	movq	912(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movl	1124(%rsp), %esi        # 4-byte Reload
	movl	%esi, %esi
	movl	%esi, %edx
	movq	1032(%rsp), %rdi        # 8-byte Reload
	movq	1072(%rsp), %rsi        # 8-byte Reload
	movq	984(%rsp), %rcx         # 8-byte Reload
	movq	1064(%rsp), %r8         # 8-byte Reload
	movq	920(%rsp), %r9          # 8-byte Reload
	movq	912(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	#DEBUG_VALUE: test_kernel:store_vals <- [RSP+1016]
	#DEBUG_VALUE: test_kernel:store_loc <- [RSP+1008]
	#DEBUG_VALUE: test_kernel:num_vals <- [RSP+1124]
	.loc	3 26 0                  # test_kernel/test_kernel.c:26:0
	movq	960(%rsp), %rdi         # 8-byte Reload
	movq	1144(%rsp), %rsi        # 8-byte Reload
	movq	1136(%rsp), %rdx        # 8-byte Reload
	movq	1048(%rsp), %rcx        # 8-byte Reload
	movq	1040(%rsp), %r8         # 8-byte Reload
	movl	1120(%rsp), %r9d        # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	movq	984(%rsp), %rdi         # 8-byte Reload
	movq	992(%rsp), %rsi         # 8-byte Reload
	movq	1016(%rsp), %rdx        # 8-byte Reload
	movq	984(%rsp), %rcx         # 8-byte Reload
	movq	936(%rsp), %r8          # 8-byte Reload
	movq	920(%rsp), %r9          # 8-byte Reload
	movq	912(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	1016(%rsp), %rax        # 8-byte Reload
	movq	1104(%rsp), %rdi        # 8-byte Reload
	movq	992(%rsp), %rsi         # 8-byte Reload
	movq	%rax, %rdx
	movq	984(%rsp), %rcx         # 8-byte Reload
	movq	936(%rsp), %r8          # 8-byte Reload
	movq	920(%rsp), %r9          # 8-byte Reload
	movq	912(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movq	%rax, 904(%rsp)         # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	960(%rsp), %rdi         # 8-byte Reload
	movq	1144(%rsp), %rsi        # 8-byte Reload
	movq	1136(%rsp), %rdx        # 8-byte Reload
	movq	1096(%rsp), %rcx        # 8-byte Reload
	movq	1056(%rsp), %r8         # 8-byte Reload
	movl	1120(%rsp), %r9d        # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	movl	1124(%rsp), %r9d        # 4-byte Reload
	movl	%r9d, %esi
	movl	%esi, %edx
	movq	984(%rsp), %rdi         # 8-byte Reload
	movq	1072(%rsp), %rsi        # 8-byte Reload
	movq	984(%rsp), %rcx         # 8-byte Reload
	movq	1064(%rsp), %r8         # 8-byte Reload
	movq	920(%rsp), %r9          # 8-byte Reload
	movq	912(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	1124(%rsp), %esi        # 4-byte Reload
	movslq	%esi, %rax
	movq	1104(%rsp), %rdi        # 8-byte Reload
	movq	992(%rsp), %rsi         # 8-byte Reload
	movq	%rax, %rdx
	movq	984(%rsp), %rcx         # 8-byte Reload
	movq	1096(%rsp), %r8         # 8-byte Reload
	movq	920(%rsp), %r9          # 8-byte Reload
	movq	912(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movq	%rax, 896(%rsp)         # 8-byte Spill
	callq	trace_logger_log_int
	movq	960(%rsp), %rdi         # 8-byte Reload
	movq	1144(%rsp), %rsi        # 8-byte Reload
	movq	1136(%rsp), %rdx        # 8-byte Reload
	movq	976(%rsp), %rcx         # 8-byte Reload
	movq	1080(%rsp), %r8         # 8-byte Reload
	movl	1120(%rsp), %r9d        # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	movq	928(%rsp), %rdi         # 8-byte Reload
	movq	992(%rsp), %rsi         # 8-byte Reload
	movq	928(%rsp), %rdx         # 8-byte Reload
	movq	920(%rsp), %rcx         # 8-byte Reload
	movq	1088(%rsp), %r8         # 8-byte Reload
	movq	920(%rsp), %r9          # 8-byte Reload
	movq	912(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	984(%rsp), %rdi         # 8-byte Reload
	movq	992(%rsp), %rsi         # 8-byte Reload
	movq	896(%rsp), %rdx         # 8-byte Reload
	movq	984(%rsp), %rcx         # 8-byte Reload
	movq	1096(%rsp), %r8         # 8-byte Reload
	movq	920(%rsp), %r9          # 8-byte Reload
	movq	912(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	896(%rsp), %rax         # 8-byte Reload
	shlq	$2, %rax
	movq	1104(%rsp), %rdi        # 8-byte Reload
	movq	992(%rsp), %rsi         # 8-byte Reload
	movq	%rax, %rdx
	movq	984(%rsp), %rcx         # 8-byte Reload
	movq	976(%rsp), %r8          # 8-byte Reload
	movq	920(%rsp), %r9          # 8-byte Reload
	movq	912(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movq	%rax, 888(%rsp)         # 8-byte Spill
	callq	trace_logger_log_int
	movq	960(%rsp), %rdi         # 8-byte Reload
	movq	1144(%rsp), %rsi        # 8-byte Reload
	movq	1136(%rsp), %rdx        # 8-byte Reload
	movq	1128(%rsp), %rcx        # 8-byte Reload
	movq	1112(%rsp), %r8         # 8-byte Reload
	movl	1120(%rsp), %r9d        # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	movq	944(%rsp), %rdi         # 8-byte Reload
	movq	992(%rsp), %rsi         # 8-byte Reload
	movq	952(%rsp), %rdx         # 8-byte Reload
	movq	984(%rsp), %rcx         # 8-byte Reload
	movq	968(%rsp), %r8          # 8-byte Reload
	movq	920(%rsp), %r9          # 8-byte Reload
	movq	912(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	984(%rsp), %rdi         # 8-byte Reload
	movq	992(%rsp), %rsi         # 8-byte Reload
	movq	904(%rsp), %rdx         # 8-byte Reload
	movq	984(%rsp), %rcx         # 8-byte Reload
	movq	936(%rsp), %r8          # 8-byte Reload
	movq	920(%rsp), %r9          # 8-byte Reload
	movq	912(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	928(%rsp), %rdi         # 8-byte Reload
	movq	992(%rsp), %rsi         # 8-byte Reload
	movq	904(%rsp), %rdx         # 8-byte Reload
	movq	984(%rsp), %rcx         # 8-byte Reload
	movq	936(%rsp), %r8          # 8-byte Reload
	movq	920(%rsp), %r9          # 8-byte Reload
	movq	912(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	1152(%rsp), %rdi        # 8-byte Reload
	movq	992(%rsp), %rsi         # 8-byte Reload
	movq	888(%rsp), %rdx         # 8-byte Reload
	movq	984(%rsp), %rcx         # 8-byte Reload
	movq	976(%rsp), %r8          # 8-byte Reload
	movq	920(%rsp), %r9          # 8-byte Reload
	movq	912(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	904(%rsp), %rdi         # 8-byte Reload
	movq	904(%rsp), %rsi         # 8-byte Reload
	movq	888(%rsp), %rdx         # 8-byte Reload
	callq	dmaLoad
.Ltmp100:
	movabsq	$3, %rdi
	movabsq	$64, %rcx
	movabsq	$1, %rdx
	leaq	.L.str839, %r8
	movabsq	$0, %rsi
	leaq	.L.str536, %r9
	movabsq	$2, %r10
	leaq	.L.str1344, %r11
	movabsq	$4, %rbx
	leaq	dmaLoad, %r14
	leaq	.L.str1142, %r15
	movabsq	$27, %r12
	leaq	.L.str132, %r13
	leaq	.L.str233, %rbp
	movq	%rcx, 880(%rsp)         # 8-byte Spill
	leaq	.L.str1445, %rcx
	movq	%rcx, 872(%rsp)         # 8-byte Spill
	movabsq	$99, %rcx
	movl	%eax, 868(%rsp)         # 4-byte Spill
	movl	$1, %eax
	movl	%eax, 864(%rsp)         # 4-byte Spill
	movl	$0, %eax
	movq	%rcx, 856(%rsp)         # 8-byte Spill
	movabsq	$19134, %rcx            # imm = 0x4ABE
	movq	%rcx, 848(%rsp)         # 8-byte Spill
	leaq	.L.str1243, %rcx
	movq	%rcx, 840(%rsp)         # 8-byte Spill
	movabsq	$44, %rcx
	movq	%rcx, 832(%rsp)         # 8-byte Spill
	movabsq	$32, %rcx
	movq	%rcx, 824(%rsp)         # 8-byte Spill
	leaq	.L.str1041, %rcx
	.loc	3 27 0                  # test_kernel/test_kernel.c:27:0
	movl	%eax, 820(%rsp)         # 4-byte Spill
	movl	868(%rsp), %eax         # 4-byte Reload
	movl	%eax, %eax
                                        # kill: RAX<def> EAX<kill>
	movq	%rax, 808(%rsp)         # 8-byte Spill
	movq	848(%rsp), %rax         # 8-byte Reload
	movq	%rdi, 800(%rsp)         # 8-byte Spill
	movq	%rax, %rdi
	movq	824(%rsp), %rax         # 8-byte Reload
	movq	%rsi, 792(%rsp)         # 8-byte Spill
	movq	%rax, %rsi
	movq	808(%rsp), %rax         # 8-byte Reload
	movq	%rdx, 784(%rsp)         # 8-byte Spill
	movq	%rax, %rdx
	movq	784(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 776(%rsp)         # 8-byte Spill
	movq	%rax, %rcx
	movq	776(%rsp), %rax         # 8-byte Reload
	movq	%r8, 768(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movq	792(%rsp), %rax         # 8-byte Reload
	movq	%r9, 760(%rsp)          # 8-byte Spill
	movq	%rax, %r9
	movq	760(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	movq	%rbp, 752(%rsp)         # 8-byte Spill
	movq	%r15, 744(%rsp)         # 8-byte Spill
	movq	%r12, 736(%rsp)         # 8-byte Spill
	movq	%r14, 728(%rsp)         # 8-byte Spill
	movq	%r13, 720(%rsp)         # 8-byte Spill
	movq	%rbx, 712(%rsp)         # 8-byte Spill
	movq	%r10, 704(%rsp)         # 8-byte Spill
	movq	%r11, 696(%rsp)         # 8-byte Spill
	callq	trace_logger_log_int
	movq	736(%rsp), %rdi         # 8-byte Reload
	movq	720(%rsp), %rsi         # 8-byte Reload
	movq	752(%rsp), %rdx         # 8-byte Reload
	movq	840(%rsp), %rcx         # 8-byte Reload
	movq	832(%rsp), %r8          # 8-byte Reload
	movl	864(%rsp), %r9d         # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	movq	784(%rsp), %rdi         # 8-byte Reload
	movq	880(%rsp), %rsi         # 8-byte Reload
	movq	1008(%rsp), %rdx        # 8-byte Reload
	movq	784(%rsp), %rcx         # 8-byte Reload
	movq	696(%rsp), %r8          # 8-byte Reload
	movq	792(%rsp), %r9          # 8-byte Reload
	movq	760(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	1008(%rsp), %rax        # 8-byte Reload
	movq	848(%rsp), %rdi         # 8-byte Reload
	movq	880(%rsp), %rsi         # 8-byte Reload
	movq	%rax, %rdx
	movq	784(%rsp), %rcx         # 8-byte Reload
	movq	696(%rsp), %r8          # 8-byte Reload
	movq	792(%rsp), %r9          # 8-byte Reload
	movq	760(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movq	%rax, 688(%rsp)         # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	736(%rsp), %rdi         # 8-byte Reload
	movq	720(%rsp), %rsi         # 8-byte Reload
	movq	752(%rsp), %rdx         # 8-byte Reload
	movq	872(%rsp), %rcx         # 8-byte Reload
	movq	856(%rsp), %r8          # 8-byte Reload
	movl	864(%rsp), %r9d         # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	movq	712(%rsp), %rdi         # 8-byte Reload
	movq	880(%rsp), %rsi         # 8-byte Reload
	movq	728(%rsp), %rdx         # 8-byte Reload
	movq	784(%rsp), %rcx         # 8-byte Reload
	movq	744(%rsp), %r8          # 8-byte Reload
	movq	792(%rsp), %r9          # 8-byte Reload
	movq	760(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	784(%rsp), %rdi         # 8-byte Reload
	movq	880(%rsp), %rsi         # 8-byte Reload
	movq	688(%rsp), %rdx         # 8-byte Reload
	movq	784(%rsp), %rcx         # 8-byte Reload
	movq	696(%rsp), %r8          # 8-byte Reload
	movq	792(%rsp), %r9          # 8-byte Reload
	movq	760(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	704(%rsp), %rdi         # 8-byte Reload
	movq	880(%rsp), %rsi         # 8-byte Reload
	movq	688(%rsp), %rdx         # 8-byte Reload
	movq	784(%rsp), %rcx         # 8-byte Reload
	movq	696(%rsp), %r8          # 8-byte Reload
	movq	792(%rsp), %r9          # 8-byte Reload
	movq	760(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	800(%rsp), %rdi         # 8-byte Reload
	movq	880(%rsp), %rsi         # 8-byte Reload
	movq	888(%rsp), %rdx         # 8-byte Reload
	movq	784(%rsp), %rcx         # 8-byte Reload
	movq	768(%rsp), %r8          # 8-byte Reload
	movq	792(%rsp), %r9          # 8-byte Reload
	movq	760(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	688(%rsp), %rdi         # 8-byte Reload
	movq	688(%rsp), %rsi         # 8-byte Reload
	movq	888(%rsp), %rdx         # 8-byte Reload
	callq	dmaLoad
	movabsq	$0, %rcx
	movabsq	$1, %rdx
	leaq	.L.str1546, %rsi
	leaq	.L.str536, %rdi
	movabsq	$2, %r8
	leaq	.L.str1849, %r9
	movabsq	$3, %r10
	leaq	.L.str1748, %r11
	movabsq	$30, %rbx
	leaq	.L.str132, %r14
	leaq	.L.str233, %r15
	leaq	.L.str1647, %r12
	movl	$1, %ebp
	movl	$0, %r13d
	movq	%rcx, 680(%rsp)         # 8-byte Spill
	movabsq	$19134, %rcx            # imm = 0x4ABE
	movq	%rcx, 672(%rsp)         # 8-byte Spill
	movabsq	$32, %rcx
	movq	%rcx, 664(%rsp)         # 8-byte Spill
	leaq	.L.str738, %rcx
	movq	%rcx, 656(%rsp)         # 8-byte Spill
	leaq	.L.str940, %rcx
	movq	%rcx, 648(%rsp)         # 8-byte Spill
	movabsq	$46, %rcx
	movq	%rcx, 640(%rsp)         # 8-byte Spill
	leaq	.L.str1445, %rcx
	.loc	3 30 0                  # test_kernel/test_kernel.c:30:0
.Ltmp101:
	movl	%eax, %eax
                                        # kill: RAX<def> EAX<kill>
	movq	%rax, 632(%rsp)         # 8-byte Spill
	movq	672(%rsp), %rax         # 8-byte Reload
	movq	%rdi, 624(%rsp)         # 8-byte Spill
	movq	%rax, %rdi
	movq	664(%rsp), %rax         # 8-byte Reload
	movq	%rsi, 616(%rsp)         # 8-byte Spill
	movq	%rax, %rsi
	movq	632(%rsp), %rax         # 8-byte Reload
	movq	%rdx, 608(%rsp)         # 8-byte Spill
	movq	%rax, %rdx
	movq	608(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 600(%rsp)         # 8-byte Spill
	movq	%rax, %rcx
	movq	600(%rsp), %rax         # 8-byte Reload
	movq	%r8, 592(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movq	680(%rsp), %rax         # 8-byte Reload
	movq	%r9, 584(%rsp)          # 8-byte Spill
	movq	%rax, %r9
	movq	624(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	movl	%r13d, 580(%rsp)        # 4-byte Spill
	movq	%r15, 568(%rsp)         # 8-byte Spill
	movq	%r12, 560(%rsp)         # 8-byte Spill
	movl	%ebp, 556(%rsp)         # 4-byte Spill
	movq	%r14, 544(%rsp)         # 8-byte Spill
	movq	%rbx, 536(%rsp)         # 8-byte Spill
	movq	%r10, 528(%rsp)         # 8-byte Spill
	movq	%r11, 520(%rsp)         # 8-byte Spill
	callq	trace_logger_log_int
.Ltmp102:
	#DEBUG_VALUE: i <- 0
	movq	536(%rsp), %rdi         # 8-byte Reload
	movq	544(%rsp), %rsi         # 8-byte Reload
	movq	568(%rsp), %rdx         # 8-byte Reload
	movq	616(%rsp), %rcx         # 8-byte Reload
	movq	640(%rsp), %r8          # 8-byte Reload
	movl	556(%rsp), %r9d         # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	movq	592(%rsp), %rdi         # 8-byte Reload
	movq	664(%rsp), %rsi         # 8-byte Reload
	movq	680(%rsp), %rdx         # 8-byte Reload
	movq	680(%rsp), %rcx         # 8-byte Reload
	movq	648(%rsp), %r8          # 8-byte Reload
	movq	680(%rsp), %r9          # 8-byte Reload
	movq	624(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	1124(%rsp), %ebp        # 4-byte Reload
	movl	%ebp, %r13d
	movl	%r13d, %edx
	movq	608(%rsp), %rdi         # 8-byte Reload
	movq	664(%rsp), %rsi         # 8-byte Reload
	movq	608(%rsp), %rcx         # 8-byte Reload
	movq	656(%rsp), %r8          # 8-byte Reload
	movq	680(%rsp), %r9          # 8-byte Reload
	movq	624(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	1124(%rsp), %ebp        # 4-byte Reload
	cmpl	$0, %ebp
	setg	%al
	movb	%al, %cl
	andb	$1, %cl
	movzbl	%cl, %r13d
	movl	%r13d, %edx
	movq	672(%rsp), %rdi         # 8-byte Reload
	movq	608(%rsp), %rsi         # 8-byte Reload
	movq	608(%rsp), %rcx         # 8-byte Reload
	movq	616(%rsp), %r8          # 8-byte Reload
	movq	680(%rsp), %r9          # 8-byte Reload
	movq	624(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movb	%al, 519(%rsp)          # 1-byte Spill
	callq	trace_logger_log_int
	movq	536(%rsp), %rdi         # 8-byte Reload
	movq	544(%rsp), %rsi         # 8-byte Reload
	movq	568(%rsp), %rdx         # 8-byte Reload
	movq	560(%rsp), %rcx         # 8-byte Reload
	movq	592(%rsp), %r8          # 8-byte Reload
	movl	556(%rsp), %r9d         # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	movq	528(%rsp), %rdi         # 8-byte Reload
	movq	680(%rsp), %rsi         # 8-byte Reload
	movq	680(%rsp), %rdx         # 8-byte Reload
	movq	608(%rsp), %rcx         # 8-byte Reload
	movq	520(%rsp), %r8          # 8-byte Reload
	movq	680(%rsp), %r9          # 8-byte Reload
	movq	624(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	592(%rsp), %rdi         # 8-byte Reload
	movq	680(%rsp), %rsi         # 8-byte Reload
	movq	680(%rsp), %rdx         # 8-byte Reload
	movq	608(%rsp), %rcx         # 8-byte Reload
	movq	584(%rsp), %r8          # 8-byte Reload
	movq	680(%rsp), %r9          # 8-byte Reload
	movq	624(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	519(%rsp), %al          # 1-byte Reload
	andb	$1, %al
	movzbl	%al, %ebp
	movl	%ebp, %edx
	movq	608(%rsp), %rdi         # 8-byte Reload
	movq	608(%rsp), %rsi         # 8-byte Reload
	movq	608(%rsp), %rcx         # 8-byte Reload
	movq	616(%rsp), %r8          # 8-byte Reload
	movq	680(%rsp), %r9          # 8-byte Reload
	movq	624(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	519(%rsp), %al          # 1-byte Reload
	testb	$1, %al
	movq	680(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, 504(%rsp)         # 8-byte Spill
	jne	.LBB3_1
	jmp	.LBB3_2
.Ltmp103:
.LBB3_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	504(%rsp), %rax         # 8-byte Reload
	movabsq	$1, %rcx
	leaq	.L.str2758, %rdx
	movabsq	$0, %rsi
	leaq	.L.str536, %rdi
	movabsq	$2, %r8
	leaq	.L.str1748, %r9
	movabsq	$3, %r10
	leaq	.L.str1849, %r11
	movabsq	$30, %rbx
	leaq	.L.str132, %r14
	leaq	.L.str2859, %r15
	movl	$1, %ebp
	movl	$0, %r12d
	movabsq	$19134, %r13            # imm = 0x4ABE
	movq	%rax, 496(%rsp)         # 8-byte Spill
	movabsq	$32, %rax
	movq	%rax, 488(%rsp)         # 8-byte Spill
	leaq	.L.str2657, %rax
	movq	%rax, 480(%rsp)         # 8-byte Spill
	leaq	.L.str738, %rax
	movq	%rax, 472(%rsp)         # 8-byte Spill
	movabsq	$46, %rax
	movq	%rax, 464(%rsp)         # 8-byte Spill
	movabsq	$64, %rax
	movq	%rax, 456(%rsp)         # 8-byte Spill
	leaq	.L.str2051, %rax
	movq	%rax, 448(%rsp)         # 8-byte Spill
	movabsq	$33, %rax
	movq	%rax, 440(%rsp)         # 8-byte Spill
	leaq	.L.str1950, %rax
	movq	%rax, 432(%rsp)         # 8-byte Spill
	leaq	.L.str940, %rax
	movq	%rax, 424(%rsp)         # 8-byte Spill
	movabsq	$8, %rax
	movq	%rax, 416(%rsp)         # 8-byte Spill
	leaq	.L.str2354, %rax
	movq	%rax, 408(%rsp)         # 8-byte Spill
	leaq	.L.str2455, %rax
	movq	%rax, 400(%rsp)         # 8-byte Spill
	movabsq	$31, %rax
	movq	%rax, 392(%rsp)         # 8-byte Spill
	leaq	.L.str2556, %rax
	movq	%rax, 384(%rsp)         # 8-byte Spill
	movabsq	$28, %rax
	movq	%rax, 376(%rsp)         # 8-byte Spill
	leaq	.L.str1344, %rax
	movq	%rax, 368(%rsp)         # 8-byte Spill
	movabsq	$29, %rax
	movq	%rax, 360(%rsp)         # 8-byte Spill
	leaq	.L.str2253, %rax
	movq	%rax, 352(%rsp)         # 8-byte Spill
	leaq	.L.str2152, %rax
	movq	%rax, 344(%rsp)         # 8-byte Spill
	movabsq	$27, %rax
	movq	%rax, 336(%rsp)         # 8-byte Spill
	leaq	.L.str435, %rax
	movq	%rax, 328(%rsp)         # 8-byte Spill
	leaq	.L.str233, %rax
	movq	%rax, 320(%rsp)         # 8-byte Spill
	movabsq	$-1, %rax
	movq	%rax, 312(%rsp)         # 8-byte Spill
	movabsq	$48, %rax
	.loc	3 31 0                  # test_kernel/test_kernel.c:31:0
	movq	%rax, 304(%rsp)         # 8-byte Spill
	movq	312(%rsp), %rax         # 8-byte Reload
	movq	%rdi, 296(%rsp)         # 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, 288(%rsp)         # 8-byte Spill
	movq	%r14, %rsi
	movq	%rdx, 280(%rsp)         # 8-byte Spill
	movq	%r9, %rdx
	movq	432(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 272(%rsp)         # 8-byte Spill
	movq	%rax, %rcx
	movq	304(%rsp), %rax         # 8-byte Reload
	movq	%r8, 264(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movq	%r9, 256(%rsp)          # 8-byte Spill
	movl	%ebp, %r9d
	movl	$0, (%rsp)
	movq	%r15, 248(%rsp)         # 8-byte Spill
	movl	%ebp, 244(%rsp)         # 4-byte Spill
	movl	%r12d, 240(%rsp)        # 4-byte Spill
	movq	%r13, 232(%rsp)         # 8-byte Spill
	movq	%r14, 224(%rsp)         # 8-byte Spill
	movq	%rbx, 216(%rsp)         # 8-byte Spill
	movq	%r10, 208(%rsp)         # 8-byte Spill
	movq	%r11, 200(%rsp)         # 8-byte Spill
	callq	trace_logger_log0
	movq	264(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	288(%rsp), %rdx         # 8-byte Reload
	movq	288(%rsp), %rcx         # 8-byte Reload
	movq	424(%rsp), %r8          # 8-byte Reload
	movq	272(%rsp), %r9          # 8-byte Reload
	movq	320(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	272(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	288(%rsp), %rdx         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	448(%rsp), %r8          # 8-byte Reload
	movq	272(%rsp), %r9          # 8-byte Reload
	movq	256(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	232(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	496(%rsp), %rdx         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	432(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	256(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	392(%rsp), %rdi         # 8-byte Reload
	movq	224(%rsp), %rsi         # 8-byte Reload
	movq	256(%rsp), %rdx         # 8-byte Reload
	movq	344(%rsp), %rcx         # 8-byte Reload
	movq	360(%rsp), %r8          # 8-byte Reload
	movl	244(%rsp), %r9d         # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	movq	264(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	496(%rsp), %rdx         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	432(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	272(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	1016(%rsp), %rdx        # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	328(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	496(%rsp), %rax         # 8-byte Reload
	shlq	$2, %rax
	movq	1016(%rsp), %rcx        # 8-byte Reload
	addq	%rax, %rcx
	movq	232(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	%rcx, %rdx
	movq	272(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 192(%rsp)         # 8-byte Spill
	movq	%rax, %rcx
	movq	344(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	392(%rsp), %rdi         # 8-byte Reload
	movq	224(%rsp), %rsi         # 8-byte Reload
	movq	256(%rsp), %rdx         # 8-byte Reload
	movq	352(%rsp), %rcx         # 8-byte Reload
	movq	336(%rsp), %r8          # 8-byte Reload
	movl	244(%rsp), %r9d         # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	movq	272(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	192(%rsp), %rdx         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	344(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	1016(%rsp), %rax        # 8-byte Reload
	movq	496(%rsp), %rcx         # 8-byte Reload
	movl	(%rax,%rcx,4), %ebp
	movl	%ebp, %r12d
	movl	%r12d, %edx
	movq	232(%rsp), %rdi         # 8-byte Reload
	movq	488(%rsp), %rsi         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	352(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movl	%ebp, 188(%rsp)         # 4-byte Spill
	callq	trace_logger_log_int
	movq	392(%rsp), %rdi         # 8-byte Reload
	movq	224(%rsp), %rsi         # 8-byte Reload
	movq	256(%rsp), %rdx         # 8-byte Reload
	movq	408(%rsp), %rcx         # 8-byte Reload
	movq	416(%rsp), %r8          # 8-byte Reload
	movl	244(%rsp), %r9d         # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	movq	264(%rsp), %rdi         # 8-byte Reload
	movq	488(%rsp), %rsi         # 8-byte Reload
	movq	272(%rsp), %rdx         # 8-byte Reload
	movq	288(%rsp), %rcx         # 8-byte Reload
	movq	424(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	188(%rsp), %ebp         # 4-byte Reload
	movl	%ebp, %r12d
	movl	%r12d, %edx
	movq	272(%rsp), %rdi         # 8-byte Reload
	movq	488(%rsp), %rsi         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	352(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	188(%rsp), %ebp         # 4-byte Reload
	addl	$1, %ebp
	movl	%ebp, %r12d
	movl	%r12d, %edx
	movq	232(%rsp), %rdi         # 8-byte Reload
	movq	488(%rsp), %rsi         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	408(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	movl	%ebp, 184(%rsp)         # 4-byte Spill
	callq	trace_logger_log_int
	movq	392(%rsp), %rdi         # 8-byte Reload
	movq	224(%rsp), %rsi         # 8-byte Reload
	movq	256(%rsp), %rdx         # 8-byte Reload
	movq	400(%rsp), %rcx         # 8-byte Reload
	movq	360(%rsp), %r8          # 8-byte Reload
	movl	244(%rsp), %r9d         # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	movq	264(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	496(%rsp), %rdx         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	432(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	272(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	1008(%rsp), %rdx        # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	368(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	496(%rsp), %rax         # 8-byte Reload
	shlq	$2, %rax
	movq	1008(%rsp), %rcx        # 8-byte Reload
	addq	%rax, %rcx
	movq	232(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	%rcx, %rdx
	movq	272(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	movq	%rax, %rcx
	movq	400(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	392(%rsp), %rdi         # 8-byte Reload
	movq	224(%rsp), %rsi         # 8-byte Reload
	movq	256(%rsp), %rdx         # 8-byte Reload
	movq	384(%rsp), %rcx         # 8-byte Reload
	movq	376(%rsp), %r8          # 8-byte Reload
	movl	244(%rsp), %r9d         # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	movq	264(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	176(%rsp), %rdx         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	400(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movl	184(%rsp), %ebp         # 4-byte Reload
	movl	%ebp, %r12d
	movl	%r12d, %edx
	movq	272(%rsp), %rdi         # 8-byte Reload
	movq	488(%rsp), %rsi         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	408(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	1008(%rsp), %rax        # 8-byte Reload
	movq	496(%rsp), %rcx         # 8-byte Reload
	movl	184(%rsp), %ebp         # 4-byte Reload
	movl	%ebp, (%rax,%rcx,4)
	.loc	3 30 0                  # test_kernel/test_kernel.c:30:0
	movq	216(%rsp), %rdi         # 8-byte Reload
	movq	224(%rsp), %rsi         # 8-byte Reload
	movq	256(%rsp), %rdx         # 8-byte Reload
	movq	448(%rsp), %rcx         # 8-byte Reload
	movq	416(%rsp), %r8          # 8-byte Reload
	movl	244(%rsp), %r9d         # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	movq	264(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	272(%rsp), %rdx         # 8-byte Reload
	movq	288(%rsp), %rcx         # 8-byte Reload
	movq	424(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	272(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	496(%rsp), %rdx         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	432(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	496(%rsp), %rax         # 8-byte Reload
	addq	$1, %rax
	movq	232(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	%rax, %rdx
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	448(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movq	%rax, 168(%rsp)         # 8-byte Spill
	callq	trace_logger_log_int
	movq	216(%rsp), %rdi         # 8-byte Reload
	movq	224(%rsp), %rsi         # 8-byte Reload
	movq	256(%rsp), %rdx         # 8-byte Reload
	movq	480(%rsp), %rcx         # 8-byte Reload
	movq	440(%rsp), %r8          # 8-byte Reload
	movl	244(%rsp), %r9d         # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	movq	272(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	168(%rsp), %rdx         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	448(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	168(%rsp), %rax         # 8-byte Reload
	movl	%eax, %ebp
	movl	%ebp, %r12d
	movl	%r12d, %edx
	movq	232(%rsp), %rdi         # 8-byte Reload
	movq	488(%rsp), %rsi         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	480(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movl	%ebp, 164(%rsp)         # 4-byte Spill
	callq	trace_logger_log_int
	movq	216(%rsp), %rdi         # 8-byte Reload
	movq	224(%rsp), %rsi         # 8-byte Reload
	movq	256(%rsp), %rdx         # 8-byte Reload
	movq	280(%rsp), %rcx         # 8-byte Reload
	movq	464(%rsp), %r8          # 8-byte Reload
	movl	244(%rsp), %r9d         # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	movl	1124(%rsp), %r9d        # 4-byte Reload
	movl	%r9d, %ebp
	movl	%ebp, %edx
	movq	264(%rsp), %rdi         # 8-byte Reload
	movq	488(%rsp), %rsi         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	472(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	164(%rsp), %ebp         # 4-byte Reload
	movl	%ebp, %r12d
	movl	%r12d, %edx
	movq	272(%rsp), %rdi         # 8-byte Reload
	movq	488(%rsp), %rsi         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	480(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	164(%rsp), %ebp         # 4-byte Reload
	movl	1124(%rsp), %r12d       # 4-byte Reload
	cmpl	%r12d, %ebp
	sete	%al
	movb	%al, %cl
	andb	$1, %cl
	movzbl	%cl, %ecx
	movl	%ecx, %edx
	movq	232(%rsp), %rdi         # 8-byte Reload
	movq	272(%rsp), %rsi         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	280(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movb	%al, 163(%rsp)          # 1-byte Spill
	callq	trace_logger_log_int
	movq	216(%rsp), %rdi         # 8-byte Reload
	movq	224(%rsp), %rsi         # 8-byte Reload
	movq	256(%rsp), %rdx         # 8-byte Reload
	movq	248(%rsp), %rcx         # 8-byte Reload
	movq	264(%rsp), %r8          # 8-byte Reload
	movl	244(%rsp), %r9d         # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	movq	208(%rsp), %rdi         # 8-byte Reload
	movq	288(%rsp), %rsi         # 8-byte Reload
	movq	288(%rsp), %rdx         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	200(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	264(%rsp), %rdi         # 8-byte Reload
	movq	288(%rsp), %rsi         # 8-byte Reload
	movq	288(%rsp), %rdx         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	256(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	163(%rsp), %al          # 1-byte Reload
	andb	$1, %al
	movzbl	%al, %ebp
	movl	%ebp, %edx
	movq	272(%rsp), %rdi         # 8-byte Reload
	movq	272(%rsp), %rsi         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	280(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	296(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	163(%rsp), %al          # 1-byte Reload
	testb	$1, %al
	movq	168(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, 504(%rsp)         # 8-byte Spill
	jne	.LBB3_2
	jmp	.LBB3_1
.Ltmp104:
.LBB3_2:                                # %._crit_edge
	movabsq	$3, %rdi
	movabsq	$64, %rax
	movabsq	$1, %rcx
	leaq	.L.str839, %r8
	movabsq	$0, %rdx
	leaq	.L.str536, %rsi
	movabsq	$2, %r9
	leaq	.L.str1344, %r10
	movabsq	$4, %r11
	leaq	dmaStore, %rbx
	leaq	.L.str3061, %r14
	movabsq	$34, %r15
	leaq	.L.str132, %r12
	leaq	.L.str1849, %r13
	leaq	.L.str2960, %rbp
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movabsq	$98, %rax
	movq	%rsi, 144(%rsp)         # 8-byte Spill
	movl	$1, %esi
	movl	%esi, 140(%rsp)         # 4-byte Spill
	movl	$0, %esi
	.loc	3 34 0                  # test_kernel/test_kernel.c:34:0
	movq	%rdi, 128(%rsp)         # 8-byte Spill
	movq	%r15, %rdi
	movl	%esi, 124(%rsp)         # 4-byte Spill
	movq	%r12, %rsi
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	movq	%r13, %rdx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movq	%rbp, %rcx
	movq	%r8, 96(%rsp)           # 8-byte Spill
	movq	%rax, %r8
	movl	140(%rsp), %ebp         # 4-byte Reload
	movq	%r9, 88(%rsp)           # 8-byte Spill
	movl	%ebp, %r9d
	movl	$0, (%rsp)
	movq	%r14, 80(%rsp)          # 8-byte Spill
	movq	%r10, 72(%rsp)          # 8-byte Spill
	movq	%r11, 64(%rsp)          # 8-byte Spill
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	callq	trace_logger_log0
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	152(%rsp), %rsi         # 8-byte Reload
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	104(%rsp), %rcx         # 8-byte Reload
	movq	80(%rsp), %r8           # 8-byte Reload
	movq	112(%rsp), %r9          # 8-byte Reload
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	104(%rsp), %rdi         # 8-byte Reload
	movq	152(%rsp), %rsi         # 8-byte Reload
	movq	688(%rsp), %rdx         # 8-byte Reload
	movq	104(%rsp), %rcx         # 8-byte Reload
	movq	72(%rsp), %r8           # 8-byte Reload
	movq	112(%rsp), %r9          # 8-byte Reload
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	88(%rsp), %rdi          # 8-byte Reload
	movq	152(%rsp), %rsi         # 8-byte Reload
	movq	688(%rsp), %rdx         # 8-byte Reload
	movq	104(%rsp), %rcx         # 8-byte Reload
	movq	72(%rsp), %r8           # 8-byte Reload
	movq	112(%rsp), %r9          # 8-byte Reload
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	128(%rsp), %rdi         # 8-byte Reload
	movq	152(%rsp), %rsi         # 8-byte Reload
	movq	888(%rsp), %rdx         # 8-byte Reload
	movq	104(%rsp), %rcx         # 8-byte Reload
	movq	96(%rsp), %r8           # 8-byte Reload
	movq	112(%rsp), %r9          # 8-byte Reload
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	688(%rsp), %rdi         # 8-byte Reload
	movq	688(%rsp), %rsi         # 8-byte Reload
	movq	888(%rsp), %rdx         # 8-byte Reload
	callq	dmaStore
	movabsq	$36, %rdi
	leaq	.L.str132, %rsi
	leaq	.L.str1849, %rdx
	leaq	.L.str3162, %rcx
	movabsq	$1, %r8
	movl	$1, %r9d
	movl	$0, %ebp
	movabsq	$19134, %r10            # imm = 0x4ABE
	movabsq	$32, %r11
	leaq	.L.str2960, %rbx
	movabsq	$0, %r14
	leaq	.L.str536, %r15
	.loc	3 36 0                  # test_kernel/test_kernel.c:36:0
	movl	%eax, %eax
	movl	%eax, %r12d
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	movq	%r10, %rdi
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movq	%r11, %rsi
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%r12, %rdx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%r8, %rcx
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%rbx, %r8
	movl	%r9d, 12(%rsp)          # 4-byte Spill
	movq	%r14, %r9
	movq	%r15, (%rsp)
	movl	%ebp, 8(%rsp)           # 4-byte Spill
	callq	trace_logger_log_int
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	movl	12(%rsp), %r9d          # 4-byte Reload
	movl	$0, (%rsp)
	callq	trace_logger_log0
	addq	$1160, %rsp             # imm = 0x488
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.Ltmp105:
.Ltmp106:
	.size	test_kernel, .Ltmp106-test_kernel
.Lfunc_end3:
	.cfi_endproc

	.globl	test_stores_plus
	.align	16, 0x90
	.type	test_stores_plus,@function
test_stores_plus:                       # @test_stores_plus
	.cfi_startproc
.Lfunc_begin4:
	.loc	3 38 0                  # test_kernel/test_kernel.c:38:0
# BB#0:
	pushq	%rbp
.Ltmp114:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Ltmp115:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Ltmp116:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Ltmp117:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Ltmp118:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Ltmp119:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Ltmp120:
	.cfi_def_cfa_offset 208
.Ltmp121:
	.cfi_offset %rbx, -56
.Ltmp122:
	.cfi_offset %r12, -48
.Ltmp123:
	.cfi_offset %r13, -40
.Ltmp124:
	.cfi_offset %r14, -32
.Ltmp125:
	.cfi_offset %r15, -24
.Ltmp126:
	.cfi_offset %rbp, -16
	movabsq	$0, %rax
	movl	$0, %ecx
	movabsq	$24601, %r8             # imm = 0x6019
	movabsq	$32, %r9
	movabsq	$1, %r10
	leaq	.L.str738, %r11
	leaq	.L.str536, %rbx
	movabsq	$64, %r14
	leaq	.L.str1344, %r15
	leaq	.L.str435, %r12
	.loc	3 38 0 prologue_end     # test_kernel/test_kernel.c:38:0
.Ltmp127:
	movq	%rdi, 144(%rsp)         # 8-byte Spill
	movq	%r8, %rdi
	movq	%rsi, 136(%rsp)         # 8-byte Spill
	movq	%r14, %rsi
	movq	144(%rsp), %r13         # 8-byte Reload
	movl	%edx, 132(%rsp)         # 4-byte Spill
	movq	%r13, %rdx
	movl	%ecx, 128(%rsp)         # 4-byte Spill
	movq	%r10, %rcx
	movq	%r8, 120(%rsp)          # 8-byte Spill
	movq	%r12, %r8
	movq	%r9, 112(%rsp)          # 8-byte Spill
	movq	%rax, %r9
	movq	%rbx, (%rsp)
	movq	%r15, 104(%rsp)         # 8-byte Spill
	movq	%rbx, 96(%rsp)          # 8-byte Spill
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	%r14, 80(%rsp)          # 8-byte Spill
	movq	%r11, 72(%rsp)          # 8-byte Spill
	movq	%r10, 64(%rsp)          # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	120(%rsp), %rdi         # 8-byte Reload
	movq	80(%rsp), %rsi          # 8-byte Reload
	movq	136(%rsp), %rdx         # 8-byte Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	104(%rsp), %r8          # 8-byte Reload
	movq	88(%rsp), %r9           # 8-byte Reload
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movl	132(%rsp), %ebp         # 4-byte Reload
	movl	%ebp, %esi
	movl	%esi, %edx
	movq	120(%rsp), %rdi         # 8-byte Reload
	movq	112(%rsp), %rsi         # 8-byte Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	72(%rsp), %r8           # 8-byte Reload
	movq	88(%rsp), %r9           # 8-byte Reload
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	#DEBUG_VALUE: test_stores_plus:store_vals <- [RSP+144]
	#DEBUG_VALUE: test_stores_plus:store_loc <- [RSP+136]
	#DEBUG_VALUE: test_stores_plus:num_vals <- [RSP+132]
.Ltmp128:
	#DEBUG_VALUE: test_stores_plus:num_failures <- 0
	#DEBUG_VALUE: i <- 0
	.loc	3 40 0                  # test_kernel/test_kernel.c:40:0
	movl	132(%rsp), %ebp         # 4-byte Reload
	cmpl	$0, %ebp
	movq	88(%rsp), %rax          # 8-byte Reload
	movl	128(%rsp), %esi         # 4-byte Reload
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	%esi, 52(%rsp)          # 4-byte Spill
	jle	.LBB4_4
.Ltmp129:
.LBB4_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	52(%rsp), %eax          # 4-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	.loc	3 41 0                  # test_kernel/test_kernel.c:41:0
.Ltmp130:
	movq	136(%rsp), %rdx         # 8-byte Reload
	movl	(%rdx,%rcx,4), %esi
	movq	144(%rsp), %rdi         # 8-byte Reload
	movl	(%rdi,%rcx,4), %r8d
	movl	%r8d, %r9d
	addl	$1, %r9d
	cmpl	%r9d, %esi
	movl	%eax, %r9d
	movl	%eax, 48(%rsp)          # 4-byte Spill
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movl	%esi, 36(%rsp)          # 4-byte Spill
	movl	%r8d, 32(%rsp)          # 4-byte Spill
	movl	%r9d, 28(%rsp)          # 4-byte Spill
	je	.LBB4_3
# BB#2:                                 #   in Loop: Header=BB4_1 Depth=1
	.loc	3 42 0                  # test_kernel/test_kernel.c:42:0
.Ltmp131:
	movq	stdout, %rdi
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ecx
	movl	$.L.str31, %edx
	movl	%edx, %esi
	xorl	%edx, %edx
	movb	%dl, %r8b
	movl	%ecx, %edx
	movl	36(%rsp), %ecx          # 4-byte Reload
	movl	32(%rsp), %r9d          # 4-byte Reload
	movb	%r8b, 27(%rsp)          # 1-byte Spill
	movl	%r9d, %r8d
	movb	27(%rsp), %al           # 1-byte Reload
	callq	fprintf
	.loc	3 44 0                  # test_kernel/test_kernel.c:44:0
	movl	48(%rsp), %ecx          # 4-byte Reload
	addl	$1, %ecx
.Ltmp132:
	#DEBUG_VALUE: test_stores_plus:num_failures <- ECX
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	%ecx, 28(%rsp)          # 4-byte Spill
.Ltmp133:
.LBB4_3:                                #   in Loop: Header=BB4_1 Depth=1
	movl	28(%rsp), %eax          # 4-byte Reload
	.loc	3 40 0                  # test_kernel/test_kernel.c:40:0
	movq	40(%rsp), %rcx          # 8-byte Reload
	addq	$1, %rcx
	movl	%ecx, %edx
	movl	132(%rsp), %esi         # 4-byte Reload
	cmpl	%esi, %edx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movl	%eax, 52(%rsp)          # 4-byte Spill
	jne	.LBB4_1
.Ltmp134:
.LBB4_4:                                # %._crit_edge
	movl	52(%rsp), %eax          # 4-byte Reload
	.loc	3 47 0                  # test_kernel/test_kernel.c:47:0
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.Ltmp135:
.Ltmp136:
	.size	test_stores_plus, .Ltmp136-test_stores_plus
.Lfunc_end4:
	.cfi_endproc

	.globl	_dmaImpl3
	.align	16, 0x90
	.type	_dmaImpl3,@function
_dmaImpl3:                              # @_dmaImpl3
	.cfi_startproc
.Lfunc_begin5:
	.loc	4 14 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:14:0
# BB#0:
	pushq	%r15
.Ltmp141:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Ltmp142:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Ltmp143:
	.cfi_def_cfa_offset 32
	subq	$96, %rsp
.Ltmp144:
	.cfi_def_cfa_offset 128
.Ltmp145:
	.cfi_offset %rbx, -32
.Ltmp146:
	.cfi_offset %r14, -24
.Ltmp147:
	.cfi_offset %r15, -16
	movabsq	$24601, %rax            # imm = 0x6019
	movabsq	$64, %rcx
	movabsq	$1, %r8
	leaq	.L.str572, %r9
	movabsq	$0, %r10
	leaq	.L.str370, %r11
	leaq	.L.str471, %rbx
	leaq	.L.str269, %r14
	.loc	4 14 0 prologue_end     # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:14:0
.Ltmp148:
	movq	%rdi, 88(%rsp)          # 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movq	%rcx, %rsi
	movq	88(%rsp), %r15          # 8-byte Reload
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movq	%r15, %rdx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%r8, %rcx
	movq	%r8, 56(%rsp)           # 8-byte Spill
	movq	%r14, %r8
	movq	%r9, 48(%rsp)           # 8-byte Spill
	movq	%r10, %r9
	movq	%r11, (%rsp)
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movq	%r10, 32(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%r11, 16(%rsp)          # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	80(%rsp), %rdx          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	72(%rsp), %rdx          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	#DEBUG_VALUE: _dmaImpl3:dst_addr <- [RSP+88]
	#DEBUG_VALUE: _dmaImpl3:src_addr <- [RSP+80]
	#DEBUG_VALUE: _dmaImpl3:size <- [RSP+72]
	.loc	4 15 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:15:0
	movq	72(%rsp), %rax          # 8-byte Reload
	cmpq	$0, %rax
	jne	.LBB5_2
.Ltmp149:
# BB#1:
	movl	$.L.str67, %eax
	movl	%eax, %edi
	movl	$.L.str168, %eax
	movl	%eax, %esi
	movl	$.L__PRETTY_FUNCTION__._dmaImpl3, %eax
	movl	%eax, %ecx
	movl	$15, %edx
	callq	__assert_fail
.LBB5_2:
	.loc	4 16 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:16:0
	movq	88(%rsp), %rdi          # 8-byte Reload
	movq	80(%rsp), %rsi          # 8-byte Reload
	movq	72(%rsp), %rdx          # 8-byte Reload
	callq	memmove
	movl	$3, %ecx
	.loc	4 17 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:17:0
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	%ecx, %eax
	addq	$96, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	ret
.Ltmp150:
.Ltmp151:
	.size	_dmaImpl3, .Ltmp151-_dmaImpl3
.Lfunc_end5:
	.cfi_endproc

	.globl	dmaLoad
	.align	16, 0x90
	.type	dmaLoad,@function
dmaLoad:                                # @dmaLoad
	.cfi_startproc
.Lfunc_begin6:
	.loc	4 20 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:20:0
# BB#0:
	pushq	%r15
.Ltmp156:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Ltmp157:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Ltmp158:
	.cfi_def_cfa_offset 32
	subq	$96, %rsp
.Ltmp159:
	.cfi_def_cfa_offset 128
.Ltmp160:
	.cfi_offset %rbx, -32
.Ltmp161:
	.cfi_offset %r14, -24
.Ltmp162:
	.cfi_offset %r15, -16
	movabsq	$24601, %rax            # imm = 0x6019
	movabsq	$64, %rcx
	movabsq	$1, %r8
	leaq	.L.str572, %r9
	movabsq	$0, %r10
	leaq	.L.str370, %r11
	leaq	.L.str673, %rbx
	leaq	.L.str269, %r14
	.loc	4 20 0 prologue_end     # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:20:0
.Ltmp163:
	movq	%rdi, 88(%rsp)          # 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movq	%rcx, %rsi
	movq	88(%rsp), %r15          # 8-byte Reload
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movq	%r15, %rdx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%r8, %rcx
	movq	%r8, 56(%rsp)           # 8-byte Spill
	movq	%r14, %r8
	movq	%r9, 48(%rsp)           # 8-byte Spill
	movq	%r10, %r9
	movq	%r11, (%rsp)
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movq	%r10, 32(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%r11, 16(%rsp)          # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	80(%rsp), %rdx          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	72(%rsp), %rdx          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	#DEBUG_VALUE: dmaLoad:dst_addr <- [RSP+88]
	#DEBUG_VALUE: dmaLoad:src_host_addr <- [RSP+80]
	#DEBUG_VALUE: dmaLoad:size <- [RSP+72]
.Ltmp164:
	#DEBUG_VALUE: _dmaImpl3:dst_addr <- [RSP+88]
	#DEBUG_VALUE: _dmaImpl3:src_addr <- [RSP+80]
	#DEBUG_VALUE: _dmaImpl3:size <- [RSP+72]
	.loc	4 15 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:15:0
	movq	72(%rsp), %rax          # 8-byte Reload
	cmpq	$0, %rax
	jne	.LBB6_2
.Ltmp165:
# BB#1:
	movl	$.L.str67, %eax
	movl	%eax, %edi
	movl	$.L.str168, %eax
	movl	%eax, %esi
	movl	$.L__PRETTY_FUNCTION__._dmaImpl3, %eax
	movl	%eax, %ecx
	movl	$15, %edx
	callq	__assert_fail
.LBB6_2:                                # %_dmaImpl3.exit
	.loc	4 16 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:16:0
	movq	88(%rsp), %rdi          # 8-byte Reload
	movq	80(%rsp), %rsi          # 8-byte Reload
	movq	72(%rsp), %rdx          # 8-byte Reload
	callq	memmove
	movl	$3, %ecx
.Ltmp166:
	.loc	4 21 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:21:0
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	%ecx, %eax
	addq	$96, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	ret
.Ltmp167:
.Ltmp168:
	.size	dmaLoad, .Ltmp168-dmaLoad
.Lfunc_end6:
	.cfi_endproc

	.globl	dmaStore
	.align	16, 0x90
	.type	dmaStore,@function
dmaStore:                               # @dmaStore
	.cfi_startproc
.Lfunc_begin7:
	.loc	4 24 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:24:0
# BB#0:
	pushq	%r15
.Ltmp173:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Ltmp174:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Ltmp175:
	.cfi_def_cfa_offset 32
	subq	$96, %rsp
.Ltmp176:
	.cfi_def_cfa_offset 128
.Ltmp177:
	.cfi_offset %rbx, -32
.Ltmp178:
	.cfi_offset %r14, -24
.Ltmp179:
	.cfi_offset %r15, -16
	movabsq	$24601, %rax            # imm = 0x6019
	movabsq	$64, %rcx
	movabsq	$1, %r8
	leaq	.L.str572, %r9
	movabsq	$0, %r10
	leaq	.L.str370, %r11
	leaq	.L.str471, %rbx
	leaq	.L.str774, %r14
	.loc	4 24 0 prologue_end     # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:24:0
.Ltmp180:
	movq	%rdi, 88(%rsp)          # 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movq	%rcx, %rsi
	movq	88(%rsp), %r15          # 8-byte Reload
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movq	%r15, %rdx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%r8, %rcx
	movq	%r8, 56(%rsp)           # 8-byte Spill
	movq	%r14, %r8
	movq	%r9, 48(%rsp)           # 8-byte Spill
	movq	%r10, %r9
	movq	%r11, (%rsp)
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movq	%r10, 32(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%r11, 16(%rsp)          # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	80(%rsp), %rdx          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	72(%rsp), %rdx          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	#DEBUG_VALUE: dmaStore:dst_host_addr <- [RSP+88]
	#DEBUG_VALUE: dmaStore:src_addr <- [RSP+80]
	#DEBUG_VALUE: dmaStore:size <- [RSP+72]
.Ltmp181:
	#DEBUG_VALUE: _dmaImpl3:dst_addr <- [RSP+88]
	#DEBUG_VALUE: _dmaImpl3:src_addr <- [RSP+80]
	#DEBUG_VALUE: _dmaImpl3:size <- [RSP+72]
	.loc	4 15 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:15:0
	movq	72(%rsp), %rax          # 8-byte Reload
	cmpq	$0, %rax
	jne	.LBB7_2
.Ltmp182:
# BB#1:
	movl	$.L.str67, %eax
	movl	%eax, %edi
	movl	$.L.str168, %eax
	movl	%eax, %esi
	movl	$.L__PRETTY_FUNCTION__._dmaImpl3, %eax
	movl	%eax, %ecx
	movl	$15, %edx
	callq	__assert_fail
.LBB7_2:                                # %_dmaImpl3.exit
	.loc	4 16 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:16:0
	movq	88(%rsp), %rdi          # 8-byte Reload
	movq	80(%rsp), %rsi          # 8-byte Reload
	movq	72(%rsp), %rdx          # 8-byte Reload
	callq	memmove
	movl	$3, %ecx
.Ltmp183:
	.loc	4 25 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:25:0
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	%ecx, %eax
	addq	$96, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	ret
.Ltmp184:
.Ltmp185:
	.size	dmaStore, .Ltmp185-dmaStore
.Lfunc_end7:
	.cfi_endproc

	.globl	setReadyBits
	.align	16, 0x90
	.type	setReadyBits,@function
setReadyBits:                           # @setReadyBits
	.cfi_startproc
.Lfunc_begin8:
	.loc	4 28 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:28:0
# BB#0:
	pushq	%rbp
.Ltmp193:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Ltmp194:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Ltmp195:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Ltmp196:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Ltmp197:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Ltmp198:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Ltmp199:
	.cfi_def_cfa_offset 160
.Ltmp200:
	.cfi_offset %rbx, -56
.Ltmp201:
	.cfi_offset %r12, -48
.Ltmp202:
	.cfi_offset %r13, -40
.Ltmp203:
	.cfi_offset %r14, -32
.Ltmp204:
	.cfi_offset %r15, -24
.Ltmp205:
	.cfi_offset %rbp, -16
	movl	$0, %eax
	movabsq	$24601, %rcx            # imm = 0x6019
	movabsq	$32, %r8
	movabsq	$1, %r9
	leaq	.L.str976, %r10
	movabsq	$0, %r11
	leaq	.L.str370, %rbx
	movabsq	$64, %r14
	leaq	.L.str572, %r15
	leaq	.L.str875, %r12
	.loc	4 28 0 prologue_end     # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:28:0
.Ltmp206:
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	movq	%rcx, %rdi
	movq	%rsi, 88(%rsp)          # 8-byte Spill
	movq	%r14, %rsi
	movq	96(%rsp), %r13          # 8-byte Reload
	movl	%edx, 84(%rsp)          # 4-byte Spill
	movq	%r13, %rdx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	%r9, %rcx
	movq	%r8, 64(%rsp)           # 8-byte Spill
	movq	%r12, %r8
	movq	%r9, 56(%rsp)           # 8-byte Spill
	movq	%r11, %r9
	movq	%rbx, (%rsp)
	movq	%r15, 48(%rsp)          # 8-byte Spill
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movl	%eax, 36(%rsp)          # 4-byte Spill
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movq	%r11, 16(%rsp)          # 8-byte Spill
	movq	%r10, 8(%rsp)           # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	88(%rsp), %rdx          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	movq	40(%rsp), %r10          # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movl	84(%rsp), %eax          # 4-byte Reload
	movl	%eax, %ebp
	movl	%ebp, %edx
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	movq	40(%rsp), %r10          # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	#DEBUG_VALUE: setReadyBits:start_addr <- [RSP+96]
	#DEBUG_VALUE: setReadyBits:size <- [RSP+88]
	#DEBUG_VALUE: setReadyBits:value <- [RSP+84]
	.loc	4 29 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:29:0
	#APP
	#NO_APP
	.loc	4 30 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:30:0
	movl	36(%rsp), %eax          # 4-byte Reload
	addq	$104, %rsp
.Ltmp207:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.Ltmp208:
.Ltmp209:
	.size	setReadyBits, .Ltmp209-setReadyBits
.Lfunc_end8:
	.cfi_endproc

	.globl	dmaFence
	.align	16, 0x90
	.type	dmaFence,@function
dmaFence:                               # @dmaFence
	.cfi_startproc
.Lfunc_begin9:
	.loc	4 71 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:71:0
# BB#0:
	.loc	4 72 0 prologue_end     # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:72:0
	#APP
	#NO_APP
	.loc	4 73 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:73:0
	ret
.Ltmp210:
.Ltmp211:
	.size	dmaFence, .Ltmp211-dmaFence
.Lfunc_end9:
	.cfi_endproc

	.globl	trace_logger_write_labelmap
	.align	16, 0x90
	.type	trace_logger_write_labelmap,@function
trace_logger_write_labelmap:            # @trace_logger_write_labelmap
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Ltmp213:
	.cfi_def_cfa_offset 32
	cmpb	$0, initp
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	jne	.LBB10_2
# BB#1:
	callq	trace_logger_init
.LBB10_2:
	movq	full_trace_file(%rip), %rdi
	movl	$.L.str81, %eax
	movl	%eax, %esi
	movl	$26, %edx
	callq	gzwrite
	movq	full_trace_file(%rip), %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%esi, %edx
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	%eax, 4(%rsp)           # 4-byte Spill
	callq	gzwrite
	movq	full_trace_file(%rip), %rdi
	movl	$.L.str182, %edx
	movl	%edx, %esi
	movl	$25, %edx
	movl	%eax, (%rsp)            # 4-byte Spill
	addq	$24, %rsp
	jmp	gzwrite                 # TAILCALL
.Ltmp214:
	.size	trace_logger_write_labelmap, .Ltmp214-trace_logger_write_labelmap
	.cfi_endproc

	.globl	trace_logger_init
	.align	16, 0x90
	.type	trace_logger_init,@function
trace_logger_init:                      # @trace_logger_init
	.cfi_startproc
# BB#0:
	pushq	%rax
.Ltmp216:
	.cfi_def_cfa_offset 16
	movl	$.L.str283, %eax
	movl	%eax, %edi
	movl	$.L.str384, %eax
	movl	%eax, %esi
	callq	gzopen
	movq	%rax, full_trace_file
	cmpq	$0, %rax
	jne	.LBB11_2
# BB#1:
	movl	$.L.str485, %eax
	movl	%eax, %edi
	callq	perror
	movl	$-1, %edi
	callq	exit
.LBB11_2:
	movl	$512, %eax              # imm = 0x200
	movl	%eax, %edi
	movl	$1, %eax
	movl	%eax, %esi
	callq	calloc
	movq	%rax, current_toplevel_function
	movl	$2, current_logging_status
	movl	$trace_logger_fin, %ecx
	movl	%ecx, %edi
	callq	atexit
	movb	$1, initp
	movl	%eax, 4(%rsp)           # 4-byte Spill
	popq	%rax
	ret
.Ltmp217:
	.size	trace_logger_init, .Ltmp217-trace_logger_init
	.cfi_endproc

	.globl	trace_logger_fin
	.align	16, 0x90
	.type	trace_logger_fin,@function
trace_logger_fin:                       # @trace_logger_fin
	.cfi_startproc
# BB#0:
	pushq	%rax
.Ltmp219:
	.cfi_def_cfa_offset 16
	movq	current_toplevel_function(%rip), %rdi
	callq	free
	movq	full_trace_file(%rip), %rdi
	popq	%rax
	jmp	gzclose                 # TAILCALL
.Ltmp220:
	.size	trace_logger_fin, .Ltmp220-trace_logger_fin
	.cfi_endproc

	.globl	log_or_not
	.align	16, 0x90
	.type	log_or_not,@function
log_or_not:                             # @log_or_not
	.cfi_startproc
# BB#0:
	subq	$40, %rsp
.Ltmp222:
	.cfi_def_cfa_offset 48
	movb	%sil, %al
	movb	%dil, %r8b
	testb	$1, %r8b
	movl	%edx, 36(%rsp)          # 4-byte Spill
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movb	%al, 23(%rsp)           # 1-byte Spill
	jne	.LBB13_2
	jmp	.LBB13_1
.LBB13_1:
	movl	$1, %eax
	movl	$2, %ecx
	movb	23(%rsp), %dl           # 1-byte Reload
	testb	%dl, %dl
	cmovel	%ecx, %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	jmp	.LBB13_9
.LBB13_2:
	movb	23(%rsp), %al           # 1-byte Reload
	testb	$1, %al
	jne	.LBB13_4
	jmp	.LBB13_3
.LBB13_3:
	movl	current_logging_status, %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	jmp	.LBB13_9
.LBB13_4:
	movl	$1, %eax
	movl	36(%rsp), %ecx          # 4-byte Reload
	cmpl	$1, %ecx
	movl	%eax, 16(%rsp)          # 4-byte Spill
	jne	.LBB13_9
# BB#5:
	movq	current_toplevel_function, %rax
	cmpb	$0, (%rax)
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jne	.LBB13_7
# BB#6:
	movl	$.L.str586, %eax
	movl	%eax, %edi
	movl	$.L.str687, %eax
	movl	%eax, %esi
	movl	$.L__PRETTY_FUNCTION__.log_or_not, %eax
	movl	%eax, %ecx
	movl	$118, %edx
	callq	__assert_fail
.LBB13_7:
	movl	$0, %eax
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%eax, 4(%rsp)           # 4-byte Spill
	callq	strcmp
	cmpl	$0, %eax
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	%eax, 16(%rsp)          # 4-byte Spill
	je	.LBB13_9
# BB#8:
	movl	$.L.str788, %eax
	movl	%eax, %edi
	movl	$.L.str687, %eax
	movl	%eax, %esi
	movl	$.L__PRETTY_FUNCTION__.log_or_not, %eax
	movl	%eax, %ecx
	movl	$123, %edx
	callq	__assert_fail
.LBB13_9:
	movl	16(%rsp), %eax          # 4-byte Reload
	addq	$40, %rsp
	ret
.Ltmp223:
	.size	log_or_not, .Ltmp223-log_or_not
	.cfi_endproc

	.globl	convert_bytes_to_hex
	.align	16, 0x90
	.type	convert_bytes_to_hex,@function
convert_bytes_to_hex:                   # @convert_bytes_to_hex
	.cfi_startproc
# BB#0:
	subq	$56, %rsp
.Ltmp225:
	.cfi_def_cfa_offset 64
	movabsq	$0, %rax
	movw	.L.str889, %cx
	movw	%cx, (%rdi)
	movb	.L.str889+2, %r8b
	movb	%r8b, 2(%rdi)
	addq	$2, %rdi
	cmpl	$0, %edx
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	%edx, 36(%rsp)          # 4-byte Spill
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	jle	.LBB14_2
.LBB14_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	movzbl	(%rdx,%rcx), %edx
	movl	$.L.str990, %esi
                                        # kill: RSI<def> ESI<kill>
	xorl	%edi, %edi
	movb	%dil, %r8b
	movq	%rax, %rdi
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movb	%r8b, %al
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	callq	sprintf
	movslq	%eax, %rcx
	movq	16(%rsp), %rsi          # 8-byte Reload
	addq	%rcx, %rsi
	movq	8(%rsp), %rcx           # 8-byte Reload
	addq	$1, %rcx
	movl	%ecx, %eax
	movl	36(%rsp), %edx          # 4-byte Reload
	cmpl	%edx, %eax
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	jne	.LBB14_1
.LBB14_2:                               # %._crit_edge
	movq	24(%rsp), %rax          # 8-byte Reload
	movb	$0, (%rax)
	addq	$56, %rsp
	ret
.Ltmp226:
	.size	convert_bytes_to_hex, .Ltmp226-convert_bytes_to_hex
	.cfi_endproc

	.globl	update_logging_status
	.align	16, 0x90
	.type	update_logging_status,@function
update_logging_status:                  # @update_logging_status
	.cfi_startproc
# BB#0:
	subq	$56, %rsp
.Ltmp228:
	.cfi_def_cfa_offset 64
	movb	%cl, %al
	movb	%dl, %r8b
	movl	current_logging_status, %ecx
	cmpl	$0, %ecx
	movl	%esi, 52(%rsp)          # 4-byte Spill
	movb	%al, 51(%rsp)           # 1-byte Spill
	movb	%r8b, 50(%rsp)          # 1-byte Spill
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	movl	%ecx, 36(%rsp)          # 4-byte Spill
	jne	.LBB15_2
# BB#1:
	movl	inst_count, %esi
	movl	$.L.str1091, %eax
	movl	%eax, %edi
	xorl	%eax, %eax
	movb	%al, %cl
	movb	%cl, %al
	callq	printf
	movl	$2, current_logging_status
	movl	%eax, 32(%rsp)          # 4-byte Spill
	jmp	.LBB15_8
.LBB15_2:
	movb	51(%rsp), %al           # 1-byte Reload
	movzbl	%al, %ecx
	andl	$1, %ecx
	movb	50(%rsp), %dl           # 1-byte Reload
	movzbl	%dl, %esi
	andl	$1, %esi
	movl	%ecx, %edi
	movl	52(%rsp), %edx          # 4-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	callq	log_or_not
	movl	%eax, current_logging_status
	movl	36(%rsp), %edx          # 4-byte Reload
	cmpl	$2, %edx
	setne	%r8b
	cmpl	%edx, %eax
	sete	%r9b
	orb	%r9b, %r8b
	testb	$1, %r8b
	movl	%eax, 28(%rsp)          # 4-byte Spill
	jne	.LBB15_4
# BB#3:
	movl	inst_count, %esi
	movl	$.L.str1192, %eax
	movl	%eax, %edi
	xorl	%eax, %eax
	movb	%al, %cl
	movb	%cl, %al
	callq	printf
	movl	current_logging_status, %esi
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movl	%esi, 28(%rsp)          # 4-byte Spill
.LBB15_4:
	movl	28(%rsp), %eax          # 4-byte Reload
	movq	current_toplevel_function, %rcx
	cmpb	$0, (%rcx)
	sete	%dl
	cmpl	$1, %eax
	sete	%sil
	andb	%sil, %dl
	testb	$1, %dl
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	jne	.LBB15_5
	jmp	.LBB15_6
.LBB15_5:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
	callq	strcpy
	movq	%rax, (%rsp)            # 8-byte Spill
	jmp	.LBB15_8
.LBB15_6:
	movl	20(%rsp), %eax          # 4-byte Reload
	cmpl	$0, %eax
	jne	.LBB15_8
# BB#7:
	movl	$0, %esi
	movabsq	$512, %rdx              # imm = 0x200
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	memset
.LBB15_8:
	addq	$56, %rsp
	ret
.Ltmp229:
	.size	update_logging_status, .Ltmp229-update_logging_status
	.cfi_endproc

	.globl	do_not_log
	.align	16, 0x90
	.type	do_not_log,@function
do_not_log:                             # @do_not_log
	.cfi_startproc
# BB#0:
	cmpl	$2, current_logging_status
	sete	%al
	andb	$1, %al
	movzbl	%al, %eax
	ret
.Ltmp230:
	.size	do_not_log, .Ltmp230-do_not_log
	.cfi_endproc

	.globl	trace_logger_log_entry
	.align	16, 0x90
	.type	trace_logger_log_entry,@function
trace_logger_log_entry:                 # @trace_logger_log_entry
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Ltmp232:
	.cfi_def_cfa_offset 32
	cmpb	$0, initp
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movl	%esi, 12(%rsp)          # 4-byte Spill
	jne	.LBB17_2
# BB#1:
	callq	trace_logger_init
.LBB17_2:
	xorl	%esi, %esi
	movl	$1, %eax
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	%eax, %edx
	movl	%eax, %ecx
	callq	update_logging_status
	cmpl	$2, current_logging_status
	je	.LBB17_4
# BB#3:
	movq	full_trace_file, %rdi
	movl	$.L.str1293, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	12(%rsp), %eax          # 4-byte Reload
	movb	%cl, 11(%rsp)           # 1-byte Spill
	movl	%eax, %ecx
	movb	11(%rsp), %al           # 1-byte Reload
	callq	gzprintf
	movl	%eax, 4(%rsp)           # 4-byte Spill
.LBB17_4:
	addq	$24, %rsp
	ret
.Ltmp233:
	.size	trace_logger_log_entry, .Ltmp233-trace_logger_log_entry
	.cfi_endproc

	.globl	trace_logger_log0
	.align	16, 0x90
	.type	trace_logger_log0,@function
trace_logger_log0:                      # @trace_logger_log0
	.cfi_startproc
# BB#0:
	subq	$72, %rsp
.Ltmp235:
	.cfi_def_cfa_offset 80
	movb	%r9b, %al
	movb	80(%rsp), %r10b
	cmpb	$0, initp
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	movb	%r10b, 63(%rsp)         # 1-byte Spill
	movl	%r8d, 56(%rsp)          # 4-byte Spill
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movb	%al, 39(%rsp)           # 1-byte Spill
	movl	%edi, 32(%rsp)          # 4-byte Spill
	jne	.LBB18_2
# BB#1:
	callq	trace_logger_init
.LBB18_2:
	movb	39(%rsp), %al           # 1-byte Reload
	movzbl	%al, %ecx
	andl	$1, %ecx
	movb	63(%rsp), %dl           # 1-byte Reload
	movzbl	%dl, %esi
	andl	$1, %esi
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	56(%rsp), %r8d          # 4-byte Reload
	movl	%esi, 28(%rsp)          # 4-byte Spill
	movl	%r8d, %esi
	movl	%ecx, %edx
	movl	28(%rsp), %ecx          # 4-byte Reload
	callq	update_logging_status
	cmpl	$2, current_logging_status
	je	.LBB18_4
# BB#3:
	movq	full_trace_file, %rdi
	movl	inst_count, %eax
	movq	%rsp, %rcx
	movl	%eax, 8(%rcx)
	movl	56(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%rcx)
	movl	$.L.str1394, %edx
	movl	%edx, %esi
	xorl	%edx, %edx
	movb	%dl, %r8b
	movl	32(%rsp), %edx          # 4-byte Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	40(%rsp), %r9           # 8-byte Reload
	movb	%r8b, 27(%rsp)          # 1-byte Spill
	movq	%r9, %r8
	movq	48(%rsp), %r9           # 8-byte Reload
	movb	27(%rsp), %al           # 1-byte Reload
	callq	gzprintf
	movl	inst_count, %edx
	addl	$1, %edx
	movl	%edx, inst_count
	movl	%eax, 20(%rsp)          # 4-byte Spill
.LBB18_4:
	addq	$72, %rsp
	ret
.Ltmp236:
	.size	trace_logger_log0, .Ltmp236-trace_logger_log0
	.cfi_endproc

	.globl	trace_logger_log_int
	.align	16, 0x90
	.type	trace_logger_log_int,@function
trace_logger_log_int:                   # @trace_logger_log_int
	.cfi_startproc
# BB#0:
	subq	$120, %rsp
.Ltmp238:
	.cfi_def_cfa_offset 128
	movq	128(%rsp), %rax
	cmpb	$0, initp
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movl	%r9d, 108(%rsp)         # 4-byte Spill
	movq	%r8, 96(%rsp)           # 8-byte Spill
	movl	%ecx, 92(%rsp)          # 4-byte Spill
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movl	%esi, 76(%rsp)          # 4-byte Spill
	movl	%edi, 72(%rsp)          # 4-byte Spill
	jne	.LBB19_2
# BB#1:
	movl	$.L.str1495, %eax
	movl	%eax, %edi
	movl	$.L.str687, %eax
	movl	%eax, %esi
	movl	$.L__PRETTY_FUNCTION__.trace_logger_log_int, %eax
	movl	%eax, %ecx
	movl	$204, %edx
	callq	__assert_fail
.LBB19_2:
	callq	do_not_log
	testb	$1, %al
	jne	.LBB19_14
# BB#3:
	movl	72(%rsp), %eax          # 4-byte Reload
	cmpl	$19134, %eax            # imm = 0x4ABE
	jne	.LBB19_5
# BB#4:
	movq	full_trace_file, %rdi
	movl	$.L.str1596, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movl	76(%rsp), %edx          # 4-byte Reload
	movq	80(%rsp), %r8           # 8-byte Reload
	movb	%cl, 71(%rsp)           # 1-byte Spill
	movq	%r8, %rcx
	movl	92(%rsp), %r8d          # 4-byte Reload
	movb	71(%rsp), %al           # 1-byte Reload
	callq	gzprintf
	movl	%eax, 64(%rsp)          # 4-byte Spill
	jmp	.LBB19_8
.LBB19_5:
	movq	full_trace_file, %rax
	movl	72(%rsp), %ecx          # 4-byte Reload
	cmpl	$24601, %ecx            # imm = 0x6019
	movq	%rax, 56(%rsp)          # 8-byte Spill
	jne	.LBB19_7
# BB#6:
	movl	$.L.str1697, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	56(%rsp), %rdi          # 8-byte Reload
	movl	76(%rsp), %edx          # 4-byte Reload
	movq	80(%rsp), %r8           # 8-byte Reload
	movb	%cl, 55(%rsp)           # 1-byte Spill
	movq	%r8, %rcx
	movl	92(%rsp), %r8d          # 4-byte Reload
	movb	55(%rsp), %al           # 1-byte Reload
	callq	gzprintf
	movl	%eax, 48(%rsp)          # 4-byte Spill
	jmp	.LBB19_8
.LBB19_7:
	movl	$.L.str1798, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	56(%rsp), %rdi          # 8-byte Reload
	movl	72(%rsp), %edx          # 4-byte Reload
	movl	76(%rsp), %eax          # 4-byte Reload
	movb	%cl, 47(%rsp)           # 1-byte Spill
	movl	%eax, %ecx
	movq	80(%rsp), %r8           # 8-byte Reload
	movl	92(%rsp), %r9d          # 4-byte Reload
	movb	47(%rsp), %al           # 1-byte Reload
	callq	gzprintf
	movl	%eax, 40(%rsp)          # 4-byte Spill
.LBB19_8:
	movq	full_trace_file, %rax
	movl	92(%rsp), %ecx          # 4-byte Reload
	cmpl	$0, %ecx
	movq	%rax, 32(%rsp)          # 8-byte Spill
	je	.LBB19_10
# BB#9:
	movl	$.L.str1899, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	96(%rsp), %rdx          # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, 28(%rsp)          # 4-byte Spill
	jmp	.LBB19_11
.LBB19_10:
	movl	$.L.str19100, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	32(%rsp), %rdi          # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, 24(%rsp)          # 4-byte Spill
.LBB19_11:
	movq	full_trace_file, %rax
	movl	108(%rsp), %ecx         # 4-byte Reload
	cmpl	$0, %ecx
	movq	%rax, 16(%rsp)          # 8-byte Spill
	je	.LBB19_13
# BB#12:
	movl	$.L.str20101, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	112(%rsp), %rdx         # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, 12(%rsp)          # 4-byte Spill
	jmp	.LBB19_14
.LBB19_13:
	movl	$.L.str21102, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	16(%rsp), %rdi          # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, 8(%rsp)           # 4-byte Spill
.LBB19_14:
	addq	$120, %rsp
	ret
.Ltmp239:
	.size	trace_logger_log_int, .Ltmp239-trace_logger_log_int
	.cfi_endproc

	.globl	trace_logger_log_ptr
	.align	16, 0x90
	.type	trace_logger_log_ptr,@function
trace_logger_log_ptr:                   # @trace_logger_log_ptr
	.cfi_startproc
# BB#0:
	subq	$120, %rsp
.Ltmp241:
	.cfi_def_cfa_offset 128
	movq	128(%rsp), %rax
	cmpb	$0, initp
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movl	%r9d, 108(%rsp)         # 4-byte Spill
	movq	%r8, 96(%rsp)           # 8-byte Spill
	movl	%ecx, 92(%rsp)          # 4-byte Spill
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movl	%esi, 76(%rsp)          # 4-byte Spill
	movl	%edi, 72(%rsp)          # 4-byte Spill
	jne	.LBB20_2
# BB#1:
	movl	$.L.str1495, %eax
	movl	%eax, %edi
	movl	$.L.str687, %eax
	movl	%eax, %esi
	movl	$.L__PRETTY_FUNCTION__.trace_logger_log_ptr, %eax
	movl	%eax, %ecx
	movl	$225, %edx
	callq	__assert_fail
.LBB20_2:
	callq	do_not_log
	testb	$1, %al
	jne	.LBB20_14
# BB#3:
	movl	72(%rsp), %eax          # 4-byte Reload
	cmpl	$19134, %eax            # imm = 0x4ABE
	jne	.LBB20_5
# BB#4:
	movq	full_trace_file, %rdi
	movl	$.L.str22103, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movl	76(%rsp), %edx          # 4-byte Reload
	movq	80(%rsp), %r8           # 8-byte Reload
	movb	%cl, 71(%rsp)           # 1-byte Spill
	movq	%r8, %rcx
	movl	92(%rsp), %r8d          # 4-byte Reload
	movb	71(%rsp), %al           # 1-byte Reload
	callq	gzprintf
	movl	%eax, 64(%rsp)          # 4-byte Spill
	jmp	.LBB20_8
.LBB20_5:
	movq	full_trace_file, %rax
	movl	72(%rsp), %ecx          # 4-byte Reload
	cmpl	$24601, %ecx            # imm = 0x6019
	movq	%rax, 56(%rsp)          # 8-byte Spill
	jne	.LBB20_7
# BB#6:
	movl	$.L.str23104, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	56(%rsp), %rdi          # 8-byte Reload
	movl	76(%rsp), %edx          # 4-byte Reload
	movq	80(%rsp), %r8           # 8-byte Reload
	movb	%cl, 55(%rsp)           # 1-byte Spill
	movq	%r8, %rcx
	movl	92(%rsp), %r8d          # 4-byte Reload
	movb	55(%rsp), %al           # 1-byte Reload
	callq	gzprintf
	movl	%eax, 48(%rsp)          # 4-byte Spill
	jmp	.LBB20_8
.LBB20_7:
	movl	$.L.str24105, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	56(%rsp), %rdi          # 8-byte Reload
	movl	72(%rsp), %edx          # 4-byte Reload
	movl	76(%rsp), %eax          # 4-byte Reload
	movb	%cl, 47(%rsp)           # 1-byte Spill
	movl	%eax, %ecx
	movq	80(%rsp), %r8           # 8-byte Reload
	movl	92(%rsp), %r9d          # 4-byte Reload
	movb	47(%rsp), %al           # 1-byte Reload
	callq	gzprintf
	movl	%eax, 40(%rsp)          # 4-byte Spill
.LBB20_8:
	movq	full_trace_file, %rax
	movl	92(%rsp), %ecx          # 4-byte Reload
	cmpl	$0, %ecx
	movq	%rax, 32(%rsp)          # 8-byte Spill
	je	.LBB20_10
# BB#9:
	movl	$.L.str1899, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	96(%rsp), %rdx          # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, 28(%rsp)          # 4-byte Spill
	jmp	.LBB20_11
.LBB20_10:
	movl	$.L.str19100, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	32(%rsp), %rdi          # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, 24(%rsp)          # 4-byte Spill
.LBB20_11:
	movq	full_trace_file, %rax
	movl	108(%rsp), %ecx         # 4-byte Reload
	cmpl	$0, %ecx
	movq	%rax, 16(%rsp)          # 8-byte Spill
	je	.LBB20_13
# BB#12:
	movl	$.L.str20101, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	112(%rsp), %rdx         # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, 12(%rsp)          # 4-byte Spill
	jmp	.LBB20_14
.LBB20_13:
	movl	$.L.str21102, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	16(%rsp), %rdi          # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, 8(%rsp)           # 4-byte Spill
.LBB20_14:
	addq	$120, %rsp
	ret
.Ltmp242:
	.size	trace_logger_log_ptr, .Ltmp242-trace_logger_log_ptr
	.cfi_endproc

	.globl	trace_logger_log_double
	.align	16, 0x90
	.type	trace_logger_log_double,@function
trace_logger_log_double:                # @trace_logger_log_double
	.cfi_startproc
# BB#0:
	subq	$104, %rsp
.Ltmp244:
	.cfi_def_cfa_offset 112
	cmpb	$0, initp
	movl	%esi, 100(%rsp)         # 4-byte Spill
	movq	%r9, 88(%rsp)           # 8-byte Spill
	movl	%r8d, 84(%rsp)          # 4-byte Spill
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movl	%edx, 68(%rsp)          # 4-byte Spill
	vmovsd	%xmm0, 56(%rsp)         # 8-byte Spill
	movl	%edi, 52(%rsp)          # 4-byte Spill
	jne	.LBB21_2
# BB#1:
	movl	$.L.str1495, %eax
	movl	%eax, %edi
	movl	$.L.str687, %eax
	movl	%eax, %esi
	movl	$.L__PRETTY_FUNCTION__.trace_logger_log_double, %eax
	movl	%eax, %ecx
	movl	$246, %edx
	callq	__assert_fail
.LBB21_2:
	callq	do_not_log
	testb	$1, %al
	jne	.LBB21_14
# BB#3:
	movl	52(%rsp), %eax          # 4-byte Reload
	cmpl	$19134, %eax            # imm = 0x4ABE
	jne	.LBB21_5
# BB#4:
	movq	full_trace_file, %rdi
	movl	$.L.str25106, %eax
	movl	%eax, %esi
	movb	$1, %al
	movl	100(%rsp), %edx         # 4-byte Reload
	vmovsd	56(%rsp), %xmm0         # 8-byte Reload
	movl	68(%rsp), %ecx          # 4-byte Reload
	callq	gzprintf
	movl	%eax, 48(%rsp)          # 4-byte Spill
	jmp	.LBB21_8
.LBB21_5:
	movq	full_trace_file, %rax
	movl	52(%rsp), %ecx          # 4-byte Reload
	cmpl	$24601, %ecx            # imm = 0x6019
	movq	%rax, 40(%rsp)          # 8-byte Spill
	jne	.LBB21_7
# BB#6:
	movl	$.L.str26107, %eax
	movl	%eax, %esi
	movb	$1, %al
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	100(%rsp), %edx         # 4-byte Reload
	vmovsd	56(%rsp), %xmm0         # 8-byte Reload
	movl	68(%rsp), %ecx          # 4-byte Reload
	callq	gzprintf
	movl	%eax, 36(%rsp)          # 4-byte Spill
	jmp	.LBB21_8
.LBB21_7:
	movl	$.L.str27108, %eax
	movl	%eax, %esi
	movb	$1, %al
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	52(%rsp), %edx          # 4-byte Reload
	movl	100(%rsp), %ecx         # 4-byte Reload
	vmovsd	56(%rsp), %xmm0         # 8-byte Reload
	movl	68(%rsp), %r8d          # 4-byte Reload
	callq	gzprintf
	movl	%eax, 32(%rsp)          # 4-byte Spill
.LBB21_8:
	movq	full_trace_file, %rax
	movl	68(%rsp), %ecx          # 4-byte Reload
	cmpl	$0, %ecx
	movq	%rax, 24(%rsp)          # 8-byte Spill
	je	.LBB21_10
# BB#9:
	movl	$.L.str1899, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	72(%rsp), %rdx          # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, 20(%rsp)          # 4-byte Spill
	jmp	.LBB21_11
.LBB21_10:
	movl	$.L.str19100, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	24(%rsp), %rdi          # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, 16(%rsp)          # 4-byte Spill
.LBB21_11:
	movq	full_trace_file, %rax
	movl	84(%rsp), %ecx          # 4-byte Reload
	cmpl	$0, %ecx
	movq	%rax, 8(%rsp)           # 8-byte Spill
	je	.LBB21_13
# BB#12:
	movl	$.L.str20101, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	88(%rsp), %rdx          # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, 4(%rsp)           # 4-byte Spill
	jmp	.LBB21_14
.LBB21_13:
	movl	$.L.str21102, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	8(%rsp), %rdi           # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, (%rsp)            # 4-byte Spill
.LBB21_14:
	addq	$104, %rsp
	ret
.Ltmp245:
	.size	trace_logger_log_double, .Ltmp245-trace_logger_log_double
	.cfi_endproc

	.globl	trace_logger_log_vector
	.align	16, 0x90
	.type	trace_logger_log_vector,@function
trace_logger_log_vector:                # @trace_logger_log_vector
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Ltmp248:
	.cfi_def_cfa_offset 16
.Ltmp249:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
.Ltmp250:
	.cfi_def_cfa_register %rbp
	subq	$128, %rsp
	movq	16(%rbp), %rax
	cmpb	$0, initp
	movq	%rax, -8(%rbp)          # 8-byte Spill
	movl	%r9d, -12(%rbp)         # 4-byte Spill
	movq	%r8, -24(%rbp)          # 8-byte Spill
	movl	%ecx, -28(%rbp)         # 4-byte Spill
	movq	%rdx, -40(%rbp)         # 8-byte Spill
	movl	%esi, -44(%rbp)         # 4-byte Spill
	movl	%edi, -48(%rbp)         # 4-byte Spill
	jne	.LBB22_2
# BB#1:
	leaq	.L.str1495, %rdi
	leaq	.L.str687, %rsi
	movl	$267, %edx              # imm = 0x10B
	leaq	.L__PRETTY_FUNCTION__.trace_logger_log_vector, %rcx
	callq	__assert_fail
.LBB22_2:
	callq	do_not_log
	testb	$1, %al
	jne	.LBB22_15
# BB#3:
	movl	$8, %eax
	movl	-44(%rbp), %ecx         # 4-byte Reload
	sarl	$31, %ecx
	shrl	$30, %ecx
	movl	-44(%rbp), %edx         # 4-byte Reload
	addl	%ecx, %edx
	sarl	$2, %edx
	addl	$3, %edx
	movl	%edx, %esi
	movq	%rsp, %rdi
	addq	$15, %rsi
	andq	$-16, %rsi
	movq	%rsp, %r8
	subq	%rsi, %r8
	movq	%r8, %rsp
	movl	-44(%rbp), %ecx         # 4-byte Reload
	movl	%eax, -52(%rbp)         # 4-byte Spill
	movl	%ecx, %eax
	cltd
	movl	-52(%rbp), %r9d         # 4-byte Reload
	idivl	%r9d
	movq	%rdi, -64(%rbp)         # 8-byte Spill
	movq	%r8, %rdi
	movq	-40(%rbp), %rsi         # 8-byte Reload
	movl	%eax, %edx
	movq	%r8, -72(%rbp)          # 8-byte Spill
	callq	convert_bytes_to_hex
	movl	-48(%rbp), %eax         # 4-byte Reload
	cmpl	$19134, %eax            # imm = 0x4ABE
	jne	.LBB22_5
# BB#4:
	leaq	.L.str28109, %rsi
	movq	full_trace_file, %rdi
	movl	-44(%rbp), %edx         # 4-byte Reload
	movq	-72(%rbp), %rcx         # 8-byte Reload
	movl	-28(%rbp), %r8d         # 4-byte Reload
	movb	$0, %al
	callq	gzprintf
	movl	%eax, -76(%rbp)         # 4-byte Spill
	jmp	.LBB22_8
.LBB22_5:
	movq	full_trace_file, %rax
	movl	-48(%rbp), %ecx         # 4-byte Reload
	cmpl	$24601, %ecx            # imm = 0x6019
	movq	%rax, -88(%rbp)         # 8-byte Spill
	jne	.LBB22_7
# BB#6:
	leaq	.L.str29110, %rsi
	movq	-88(%rbp), %rdi         # 8-byte Reload
	movl	-44(%rbp), %edx         # 4-byte Reload
	movq	-72(%rbp), %rcx         # 8-byte Reload
	movl	-28(%rbp), %r8d         # 4-byte Reload
	movb	$0, %al
	callq	gzprintf
	movl	%eax, -92(%rbp)         # 4-byte Spill
	jmp	.LBB22_8
.LBB22_7:
	leaq	.L.str30111, %rsi
	movq	-88(%rbp), %rdi         # 8-byte Reload
	movl	-48(%rbp), %edx         # 4-byte Reload
	movl	-44(%rbp), %ecx         # 4-byte Reload
	movq	-72(%rbp), %r8          # 8-byte Reload
	movl	-28(%rbp), %r9d         # 4-byte Reload
	movb	$0, %al
	callq	gzprintf
	movl	%eax, -96(%rbp)         # 4-byte Spill
.LBB22_8:
	movq	full_trace_file, %rax
	movl	-28(%rbp), %ecx         # 4-byte Reload
	cmpl	$0, %ecx
	movq	%rax, -104(%rbp)        # 8-byte Spill
	je	.LBB22_10
# BB#9:
	leaq	.L.str1899, %rsi
	movq	-104(%rbp), %rdi        # 8-byte Reload
	movq	-24(%rbp), %rdx         # 8-byte Reload
	movb	$0, %al
	callq	gzprintf
	movl	%eax, -108(%rbp)        # 4-byte Spill
	jmp	.LBB22_11
.LBB22_10:
	leaq	.L.str19100, %rsi
	movq	-104(%rbp), %rdi        # 8-byte Reload
	movb	$0, %al
	callq	gzprintf
	movl	%eax, -112(%rbp)        # 4-byte Spill
.LBB22_11:
	movq	full_trace_file, %rax
	movl	-12(%rbp), %ecx         # 4-byte Reload
	cmpl	$0, %ecx
	movq	%rax, -120(%rbp)        # 8-byte Spill
	je	.LBB22_13
# BB#12:
	leaq	.L.str20101, %rsi
	movq	-120(%rbp), %rdi        # 8-byte Reload
	movq	-8(%rbp), %rdx          # 8-byte Reload
	movb	$0, %al
	callq	gzprintf
	movl	%eax, -124(%rbp)        # 4-byte Spill
	jmp	.LBB22_14
.LBB22_13:
	leaq	.L.str21102, %rsi
	movq	-120(%rbp), %rdi        # 8-byte Reload
	movb	$0, %al
	callq	gzprintf
	movl	%eax, -128(%rbp)        # 4-byte Spill
.LBB22_14:
	movq	-64(%rbp), %rax         # 8-byte Reload
	movq	%rax, %rsp
.LBB22_15:
	movq	%rbp, %rsp
	popq	%rbp
	ret
.Ltmp251:
	.size	trace_logger_log_vector, .Ltmp251-trace_logger_log_vector
	.cfi_endproc

	.type	.L.str1,@object         # @.str1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str1:
	.asciz	"err == 0 && \"Failed to allocate memory!\""
	.size	.L.str1, 41

	.type	.L.str2,@object         # @.str2
.L.str2:
	.asciz	"test_dma_load_store.c"
	.size	.L.str2, 22

	.type	.L__PRETTY_FUNCTION__.main,@object # @__PRETTY_FUNCTION__.main
.L__PRETTY_FUNCTION__.main:
	.asciz	"int main()"
	.size	.L__PRETTY_FUNCTION__.main, 11

	.type	.L.str3,@object         # @.str3
.L.str3:
	.asciz	"Test failed with %d errors."
	.size	.L.str3, 28

	.type	.L.str4,@object         # @.str4
.L.str4:
	.asciz	"Test passed!\n"
	.size	.L.str4, 14

	.type	.L.str,@object          # @.str
	.section	.rodata,"a",@progbits
	.align	16
.L.str:
	.asciz	"store_kernel/loop 30\ntest_kernel/loop 30\n"
	.size	.L.str, 42

	.type	.L.str5,@object         # @.str5
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str5:
	.asciz	"FAILED: store_loc[%d] = %d, should be %d\n"
	.size	.L.str5, 42

	.type	.L.str16,@object        # @.str16
	.section	.rodata,"a",@progbits
.L.str16:
	.asciz	"store_kernel"
	.size	.L.str16, 13

	.type	.L.str27,@object        # @.str27
.L.str27:
	.asciz	"0:0"
	.size	.L.str27, 4

	.type	.L.str38,@object        # @.str38
.L.str38:
	.asciz	"1"
	.size	.L.str38, 2

	.type	.L.str49,@object        # @.str49
.L.str49:
	.asciz	"store_vals"
	.size	.L.str49, 11

	.type	.L.str510,@object       # @.str510
.L.str510:
	.asciz	"phi"
	.size	.L.str510, 4

	.type	.L.str6,@object         # @.str6
.L.str6:
	.asciz	"2"
	.size	.L.str6, 2

	.type	.L.str7,@object         # @.str7
.L.str7:
	.asciz	"num_vals"
	.size	.L.str7, 9

	.type	.L.str8,@object         # @.str8
.L.str8:
	.asciz	"3"
	.size	.L.str8, 2

	.type	.L.str9,@object         # @.str9
.L.str9:
	.zero	1
	.size	.L.str9, 1

	.type	.L.str10,@object        # @.str10
.L.str10:
	.asciz	"4"
	.size	.L.str10, 2

	.type	.L.str11,@object        # @.str11
.L.str11:
	.asciz	"dmaLoad"
	.size	.L.str11, 8

	.type	.L.str12,@object        # @.str12
.L.str12:
	.asciz	"5"
	.size	.L.str12, 2

	.type	.L.str13,@object        # @.str13
.L.str13:
	.asciz	"store_loc"
	.size	.L.str13, 10

	.type	.L.str14,@object        # @.str14
.L.str14:
	.asciz	"6"
	.size	.L.str14, 2

	.type	.L.str15,@object        # @.str15
.L.str15:
	.asciz	"7"
	.size	.L.str15, 2

	.type	.L.str1611,@object      # @.str1611
.L.str1611:
	.asciz	"0:0-4"
	.size	.L.str1611, 6

	.type	.L.str17,@object        # @.str17
.L.str17:
	.asciz	".lr.ph:1"
	.size	.L.str17, 9

	.type	.L.str18,@object        # @.str18
.L.str18:
	.asciz	"._crit_edge:0"
	.size	.L.str18, 14

	.type	.L.str19,@object        # @.str19
.L.str19:
	.asciz	"indvars.iv"
	.size	.L.str19, 11

	.type	.L.str20,@object        # @.str20
.L.str20:
	.asciz	"indvars.iv.next"
	.size	.L.str20, 16

	.type	.L.str21,@object        # @.str21
.L.str21:
	.asciz	"8"
	.size	.L.str21, 2

	.type	.L.str22,@object        # @.str22
.L.str22:
	.asciz	"9"
	.size	.L.str22, 2

	.type	.L.str23,@object        # @.str23
.L.str23:
	.asciz	"10"
	.size	.L.str23, 3

	.type	.L.str24,@object        # @.str24
.L.str24:
	.asciz	".lr.ph:1-0"
	.size	.L.str24, 11

	.type	.L.str25,@object        # @.str25
.L.str25:
	.asciz	"lftr.wideiv"
	.size	.L.str25, 12

	.type	.L.str26,@object        # @.str26
.L.str26:
	.asciz	"exitcond"
	.size	.L.str26, 9

	.type	.L.str2712,@object      # @.str2712
.L.str2712:
	.asciz	".lr.ph:1-1"
	.size	.L.str2712, 11

	.type	.L.str28,@object        # @.str28
.L.str28:
	.asciz	"11"
	.size	.L.str28, 3

	.type	.L.str29,@object        # @.str29
.L.str29:
	.asciz	"dmaStore"
	.size	.L.str29, 9

	.type	.L.str30,@object        # @.str30
.L.str30:
	.asciz	"._crit_edge:0-0"
	.size	.L.str30, 16

	.type	.L.str31,@object        # @.str31
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str31:
	.asciz	"FAILED: store_loc[%d] = %d, should be %d\n"
	.size	.L.str31, 42

	.type	.L.str132,@object       # @.str132
	.section	.rodata,"a",@progbits
.L.str132:
	.asciz	"test_kernel"
	.size	.L.str132, 12

	.type	.L.str233,@object       # @.str233
.L.str233:
	.asciz	"0:0"
	.size	.L.str233, 4

	.type	.L.str334,@object       # @.str334
.L.str334:
	.asciz	"1"
	.size	.L.str334, 2

	.type	.L.str435,@object       # @.str435
.L.str435:
	.asciz	"store_vals"
	.size	.L.str435, 11

	.type	.L.str536,@object       # @.str536
.L.str536:
	.asciz	"phi"
	.size	.L.str536, 4

	.type	.L.str637,@object       # @.str637
.L.str637:
	.asciz	"2"
	.size	.L.str637, 2

	.type	.L.str738,@object       # @.str738
.L.str738:
	.asciz	"num_vals"
	.size	.L.str738, 9

	.type	.L.str839,@object       # @.str839
.L.str839:
	.asciz	"3"
	.size	.L.str839, 2

	.type	.L.str940,@object       # @.str940
.L.str940:
	.zero	1
	.size	.L.str940, 1

	.type	.L.str1041,@object      # @.str1041
.L.str1041:
	.asciz	"4"
	.size	.L.str1041, 2

	.type	.L.str1142,@object      # @.str1142
.L.str1142:
	.asciz	"dmaLoad"
	.size	.L.str1142, 8

	.type	.L.str1243,@object      # @.str1243
.L.str1243:
	.asciz	"5"
	.size	.L.str1243, 2

	.type	.L.str1344,@object      # @.str1344
.L.str1344:
	.asciz	"store_loc"
	.size	.L.str1344, 10

	.type	.L.str1445,@object      # @.str1445
.L.str1445:
	.asciz	"6"
	.size	.L.str1445, 2

	.type	.L.str1546,@object      # @.str1546
.L.str1546:
	.asciz	"7"
	.size	.L.str1546, 2

	.type	.L.str1647,@object      # @.str1647
.L.str1647:
	.asciz	"0:0-4"
	.size	.L.str1647, 6

	.type	.L.str1748,@object      # @.str1748
.L.str1748:
	.asciz	".lr.ph:1"
	.size	.L.str1748, 9

	.type	.L.str1849,@object      # @.str1849
.L.str1849:
	.asciz	"._crit_edge:0"
	.size	.L.str1849, 14

	.type	.L.str1950,@object      # @.str1950
.L.str1950:
	.asciz	"indvars.iv"
	.size	.L.str1950, 11

	.type	.L.str2051,@object      # @.str2051
.L.str2051:
	.asciz	"indvars.iv.next"
	.size	.L.str2051, 16

	.type	.L.str2152,@object      # @.str2152
.L.str2152:
	.asciz	"8"
	.size	.L.str2152, 2

	.type	.L.str2253,@object      # @.str2253
.L.str2253:
	.asciz	"9"
	.size	.L.str2253, 2

	.type	.L.str2354,@object      # @.str2354
.L.str2354:
	.asciz	"10"
	.size	.L.str2354, 3

	.type	.L.str2455,@object      # @.str2455
.L.str2455:
	.asciz	"11"
	.size	.L.str2455, 3

	.type	.L.str2556,@object      # @.str2556
.L.str2556:
	.asciz	".lr.ph:1-0"
	.size	.L.str2556, 11

	.type	.L.str2657,@object      # @.str2657
.L.str2657:
	.asciz	"lftr.wideiv"
	.size	.L.str2657, 12

	.type	.L.str2758,@object      # @.str2758
.L.str2758:
	.asciz	"exitcond"
	.size	.L.str2758, 9

	.type	.L.str2859,@object      # @.str2859
.L.str2859:
	.asciz	".lr.ph:1-1"
	.size	.L.str2859, 11

	.type	.L.str2960,@object      # @.str2960
.L.str2960:
	.asciz	"12"
	.size	.L.str2960, 3

	.type	.L.str3061,@object      # @.str3061
.L.str3061:
	.asciz	"dmaStore"
	.size	.L.str3061, 9

	.type	.L.str3162,@object      # @.str3162
.L.str3162:
	.asciz	"._crit_edge:0-0"
	.size	.L.str3162, 16

	.type	.L.str67,@object        # @.str67
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str67:
	.asciz	"size > 0"
	.size	.L.str67, 9

	.type	.L.str168,@object       # @.str168
.L.str168:
	.asciz	"/workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c"
	.size	.L.str168, 57

	.type	.L__PRETTY_FUNCTION__._dmaImpl3,@object # @__PRETTY_FUNCTION__._dmaImpl3
.L__PRETTY_FUNCTION__._dmaImpl3:
	.asciz	"int _dmaImpl3(void *, void *, size_t)"
	.size	.L__PRETTY_FUNCTION__._dmaImpl3, 38

	.type	.L.str269,@object       # @.str269
	.section	.rodata,"a",@progbits
.L.str269:
	.asciz	"dst_addr"
	.size	.L.str269, 9

	.type	.L.str370,@object       # @.str370
.L.str370:
	.asciz	"phi"
	.size	.L.str370, 4

	.type	.L.str471,@object       # @.str471
.L.str471:
	.asciz	"src_addr"
	.size	.L.str471, 9

	.type	.L.str572,@object       # @.str572
.L.str572:
	.asciz	"size"
	.size	.L.str572, 5

	.type	.L.str673,@object       # @.str673
.L.str673:
	.asciz	"src_host_addr"
	.size	.L.str673, 14

	.type	.L.str774,@object       # @.str774
.L.str774:
	.asciz	"dst_host_addr"
	.size	.L.str774, 14

	.type	.L.str875,@object       # @.str875
.L.str875:
	.asciz	"start_addr"
	.size	.L.str875, 11

	.type	.L.str976,@object       # @.str976
.L.str976:
	.asciz	"value"
	.size	.L.str976, 6

	.type	initp,@object           # @initp
	.bss
	.globl	initp
initp:
	.byte	0                       # 0x0
	.size	initp, 1

	.type	inst_count,@object      # @inst_count
	.globl	inst_count
	.align	4
inst_count:
	.long	0                       # 0x0
	.size	inst_count, 4

	.type	.L.str81,@object        # @.str81
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str81:
	.asciz	"%%%% LABEL MAP START %%%%\n"
	.size	.L.str81, 27

	.type	.L.str182,@object       # @.str182
.L.str182:
	.asciz	"%%%% LABEL MAP END %%%%\n\n"
	.size	.L.str182, 26

	.type	full_trace_file,@object # @full_trace_file
	.comm	full_trace_file,8,8
	.type	.L.str283,@object       # @.str283
.L.str283:
	.asciz	"dynamic_trace.gz"
	.size	.L.str283, 17

	.type	.L.str384,@object       # @.str384
.L.str384:
	.asciz	"w"
	.size	.L.str384, 2

	.type	.L.str485,@object       # @.str485
.L.str485:
	.asciz	"Failed to open logfile \"dynamic_trace\""
	.size	.L.str485, 39

	.type	current_toplevel_function,@object # @current_toplevel_function
	.comm	current_toplevel_function,8,8
	.type	current_logging_status,@object # @current_logging_status
	.comm	current_logging_status,4,4
	.type	.L.str586,@object       # @.str586
.L.str586:
	.asciz	"false && \"Returning from within a toplevel function before it was called!\""
	.size	.L.str586, 75

	.type	.L.str687,@object       # @.str687
.L.str687:
	.asciz	"/workspace/LLVM-Tracer/profile-func/trace_logger.c"
	.size	.L.str687, 51

	.type	.L__PRETTY_FUNCTION__.log_or_not,@object # @__PRETTY_FUNCTION__.log_or_not
.L__PRETTY_FUNCTION__.log_or_not:
	.asciz	"logging_status log_or_not(_Bool, _Bool, int, char *)"
	.size	.L__PRETTY_FUNCTION__.log_or_not, 53

	.type	.L.str788,@object       # @.str788
.L.str788:
	.asciz	"false && \"Cannot call a top level function from within another one!\""
	.size	.L.str788, 69

	.type	.L.str889,@object       # @.str889
.L.str889:
	.asciz	"0x"
	.size	.L.str889, 3

	.type	.L.str990,@object       # @.str990
.L.str990:
	.asciz	"%02x"
	.size	.L.str990, 5

	.type	.L.str1091,@object      # @.str1091
.L.str1091:
	.asciz	"Stopping logging at inst %d.\n"
	.size	.L.str1091, 30

	.type	.L.str1192,@object      # @.str1192
.L.str1192:
	.asciz	"Starting to log at inst = %d.\n"
	.size	.L.str1192, 31

	.type	.L.str1293,@object      # @.str1293
.L.str1293:
	.asciz	"\nentry,%s,%d,\n"
	.size	.L.str1293, 15

	.type	.L.str1394,@object      # @.str1394
.L.str1394:
	.asciz	"\n0,%d,%s,%s,%s,%d,%d\n"
	.size	.L.str1394, 22

	.type	.L.str1495,@object      # @.str1495
.L.str1495:
	.asciz	"initp == true"
	.size	.L.str1495, 14

	.type	.L__PRETTY_FUNCTION__.trace_logger_log_int,@object # @__PRETTY_FUNCTION__.trace_logger_log_int
.L__PRETTY_FUNCTION__.trace_logger_log_int:
	.asciz	"void trace_logger_log_int(int, int, int64_t, int, char *, int, char *)"
	.size	.L__PRETTY_FUNCTION__.trace_logger_log_int, 71

	.type	.L.str1596,@object      # @.str1596
.L.str1596:
	.asciz	"r,%d,%ld,%d"
	.size	.L.str1596, 12

	.type	.L.str1697,@object      # @.str1697
.L.str1697:
	.asciz	"f,%d,%ld,%d"
	.size	.L.str1697, 12

	.type	.L.str1798,@object      # @.str1798
.L.str1798:
	.asciz	"%d,%d,%ld,%d"
	.size	.L.str1798, 13

	.type	.L.str1899,@object      # @.str1899
.L.str1899:
	.asciz	",%s"
	.size	.L.str1899, 4

	.type	.L.str19100,@object     # @.str19100
.L.str19100:
	.asciz	", "
	.size	.L.str19100, 3

	.type	.L.str20101,@object     # @.str20101
.L.str20101:
	.asciz	",%s,\n"
	.size	.L.str20101, 6

	.type	.L.str21102,@object     # @.str21102
.L.str21102:
	.asciz	",\n"
	.size	.L.str21102, 3

	.type	.L__PRETTY_FUNCTION__.trace_logger_log_ptr,@object # @__PRETTY_FUNCTION__.trace_logger_log_ptr
.L__PRETTY_FUNCTION__.trace_logger_log_ptr:
	.asciz	"void trace_logger_log_ptr(int, int, uint64_t, int, char *, int, char *)"
	.size	.L__PRETTY_FUNCTION__.trace_logger_log_ptr, 72

	.type	.L.str22103,@object     # @.str22103
.L.str22103:
	.asciz	"r,%d,%#llx,%d"
	.size	.L.str22103, 14

	.type	.L.str23104,@object     # @.str23104
.L.str23104:
	.asciz	"f,%d,%#llx,%d"
	.size	.L.str23104, 14

	.type	.L.str24105,@object     # @.str24105
.L.str24105:
	.asciz	"%d,%d,%#llx,%d"
	.size	.L.str24105, 15

	.type	.L__PRETTY_FUNCTION__.trace_logger_log_double,@object # @__PRETTY_FUNCTION__.trace_logger_log_double
.L__PRETTY_FUNCTION__.trace_logger_log_double:
	.asciz	"void trace_logger_log_double(int, int, double, int, char *, int, char *)"
	.size	.L__PRETTY_FUNCTION__.trace_logger_log_double, 73

	.type	.L.str25106,@object     # @.str25106
.L.str25106:
	.asciz	"r,%d,%f,%d"
	.size	.L.str25106, 11

	.type	.L.str26107,@object     # @.str26107
.L.str26107:
	.asciz	"f,%d,%f,%d"
	.size	.L.str26107, 11

	.type	.L.str27108,@object     # @.str27108
.L.str27108:
	.asciz	"%d,%d,%f,%d"
	.size	.L.str27108, 12

	.type	.L__PRETTY_FUNCTION__.trace_logger_log_vector,@object # @__PRETTY_FUNCTION__.trace_logger_log_vector
.L__PRETTY_FUNCTION__.trace_logger_log_vector:
	.asciz	"void trace_logger_log_vector(int, int, uint8_t *, int, char *, int, char *)"
	.size	.L__PRETTY_FUNCTION__.trace_logger_log_vector, 76

	.type	.L.str28109,@object     # @.str28109
.L.str28109:
	.asciz	"r,%d,%s,%d"
	.size	.L.str28109, 11

	.type	.L.str29110,@object     # @.str29110
.L.str29110:
	.asciz	"f,%d,%s,%d"
	.size	.L.str29110, 11

	.type	.L.str30111,@object     # @.str30111
.L.str30111:
	.asciz	"%d,%d,%s,%d"
	.size	.L.str30111, 12

	.text
.Ldebug_end0:
	.section	.debug_str,"MS",@progbits,1
.Linfo_string0:
	.asciz	"clang version 3.4.2 (tags/RELEASE_34/dot2-final)"
.Linfo_string1:
	.asciz	"test_dma_load_store.c"
.Linfo_string2:
	.asciz	"/workspace/gem5-aladdin/src/aladdin/integration-test/with-cpu/test_dma_load_store"
.Linfo_string3:
	.asciz	"num_vals"
.Linfo_string4:
	.asciz	"int"
.Linfo_string5:
	.asciz	"main"
.Linfo_string6:
	.asciz	"store_kernel/store_kernel.c"
.Linfo_string7:
	.asciz	"store_kernel"
.Linfo_string8:
	.asciz	"test_stores"
.Linfo_string9:
	.asciz	"test_kernel/test_kernel.c"
.Linfo_string10:
	.asciz	"test_kernel"
.Linfo_string11:
	.asciz	"test_stores_plus"
.Linfo_string12:
	.asciz	"/workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c"
.Linfo_string13:
	.asciz	"_dmaImpl3"
.Linfo_string14:
	.asciz	"dmaLoad"
.Linfo_string15:
	.asciz	"dmaStore"
.Linfo_string16:
	.asciz	"setReadyBits"
.Linfo_string17:
	.asciz	"dmaFence"
.Linfo_string18:
	.asciz	"store_vals"
.Linfo_string19:
	.asciz	"store_loc"
.Linfo_string20:
	.asciz	"err"
.Linfo_string21:
	.asciz	"num_failures"
.Linfo_string22:
	.asciz	"i"
.Linfo_string23:
	.asciz	"dst_addr"
.Linfo_string24:
	.asciz	"src_addr"
.Linfo_string25:
	.asciz	"size"
.Linfo_string26:
	.asciz	"long unsigned int"
.Linfo_string27:
	.asciz	"size_t"
.Linfo_string28:
	.asciz	"src_host_addr"
.Linfo_string29:
	.asciz	"dst_host_addr"
.Linfo_string30:
	.asciz	"start_addr"
.Linfo_string31:
	.asciz	"value"
.Linfo_string32:
	.asciz	"unsigned int"
	.section	.debug_info,"",@progbits
.L.debug_info_begin0:
	.long	184                     # Length of Unit
	.short	4                       # DWARF version number
	.long	.L.debug_abbrev_begin   # Offset Into Abbrev. Section
	.byte	8                       # Address Size (in bytes)
	.byte	1                       # Abbrev [1] 0xb:0xb1 DW_TAG_compile_unit
	.long	.Linfo_string0          # DW_AT_producer
	.short	12                      # DW_AT_language
	.long	.Linfo_string1          # DW_AT_name
	.quad	0                       # DW_AT_low_pc
	.long	.Lsection_line          # DW_AT_stmt_list
	.long	.Linfo_string2          # DW_AT_comp_dir
                                        # DW_AT_APPLE_optimized
	.byte	2                       # Abbrev [2] 0x26:0xd DW_TAG_variable
	.long	.Linfo_string3          # DW_AT_name
	.long	51                      # DW_AT_type
	.byte	1                       # DW_AT_decl_file
	.byte	23                      # DW_AT_decl_line
	.ascii	"\200\b"                # DW_AT_const_value
	.byte	3                       # Abbrev [3] 0x33:0x5 DW_TAG_const_type
	.long	56                      # DW_AT_type
	.byte	4                       # Abbrev [4] 0x38:0x7 DW_TAG_base_type
	.long	.Linfo_string4          # DW_AT_name
	.byte	5                       # DW_AT_encoding
	.byte	4                       # DW_AT_byte_size
	.byte	5                       # Abbrev [5] 0x3f:0x77 DW_TAG_subprogram
	.long	.Linfo_string5          # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	22                      # DW_AT_decl_line
	.long	56                      # DW_AT_type
                                        # DW_AT_external
	.quad	.Lfunc_begin0           # DW_AT_low_pc
	.quad	.Lfunc_end0             # DW_AT_high_pc
	.byte	1                       # DW_AT_frame_base
	.byte	87
                                        # DW_AT_APPLE_omit_frame_ptr
	.byte	6                       # Abbrev [6] 0x5c:0xf DW_TAG_variable
	.long	.Linfo_string18         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	24                      # DW_AT_decl_line
	.long	182                     # DW_AT_type
	.byte	3                       # DW_AT_location
	.byte	145
	.asciz	"\300"
	.byte	6                       # Abbrev [6] 0x6b:0xe DW_TAG_variable
	.long	.Linfo_string19         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	24                      # DW_AT_decl_line
	.long	182                     # DW_AT_type
	.byte	2                       # DW_AT_location
	.byte	145
	.byte	56
	.byte	7                       # Abbrev [7] 0x79:0xd DW_TAG_variable
	.long	.Linfo_string3          # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	23                      # DW_AT_decl_line
	.long	51                      # DW_AT_type
	.ascii	"\200\b"                # DW_AT_const_value
	.byte	8                       # Abbrev [8] 0x86:0xf DW_TAG_variable
	.long	.Linfo_string20         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	25                      # DW_AT_decl_line
	.long	56                      # DW_AT_type
	.long	.Ldebug_loc0            # DW_AT_location
	.byte	8                       # Abbrev [8] 0x95:0xf DW_TAG_variable
	.long	.Linfo_string21         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	60                      # DW_AT_decl_line
	.long	56                      # DW_AT_type
	.long	.Ldebug_loc3            # DW_AT_location
	.byte	9                       # Abbrev [9] 0xa4:0x11 DW_TAG_lexical_block
	.long	.Ldebug_range           # DW_AT_ranges
	.byte	10                      # Abbrev [10] 0xa9:0xb DW_TAG_variable
	.long	.Linfo_string22         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	31                      # DW_AT_decl_line
	.long	56                      # DW_AT_type
	.byte	0                       # End Of Children Mark
	.byte	0                       # End Of Children Mark
	.byte	11                      # Abbrev [11] 0xb6:0x5 DW_TAG_pointer_type
	.long	56                      # DW_AT_type
	.byte	0                       # End Of Children Mark
.L.debug_info_end0:
.L.debug_info_begin1:
	.long	256                     # Length of Unit
	.short	4                       # DWARF version number
	.long	.L.debug_abbrev_begin   # Offset Into Abbrev. Section
	.byte	8                       # Address Size (in bytes)
	.byte	1                       # Abbrev [1] 0xb:0xf9 DW_TAG_compile_unit
	.long	.Linfo_string0          # DW_AT_producer
	.short	12                      # DW_AT_language
	.long	.Linfo_string6          # DW_AT_name
	.quad	0                       # DW_AT_low_pc
	.long	.Lsection_line          # DW_AT_stmt_list
	.long	.Linfo_string2          # DW_AT_comp_dir
                                        # DW_AT_APPLE_optimized
	.byte	12                      # Abbrev [12] 0x26:0x65 DW_TAG_subprogram
	.long	.Linfo_string7          # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	24                      # DW_AT_decl_line
                                        # DW_AT_prototyped
                                        # DW_AT_external
	.quad	.Lfunc_begin1           # DW_AT_low_pc
	.quad	.Lfunc_end1             # DW_AT_high_pc
	.byte	1                       # DW_AT_frame_base
	.byte	87
                                        # DW_AT_APPLE_omit_frame_ptr
	.byte	13                      # Abbrev [13] 0x3f:0xf DW_TAG_formal_parameter
	.long	.Linfo_string18         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	24                      # DW_AT_decl_line
	.long	.Lsection_info+182      # DW_AT_type
	.long	.Ldebug_loc6            # DW_AT_location
	.byte	13                      # Abbrev [13] 0x4e:0xf DW_TAG_formal_parameter
	.long	.Linfo_string19         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	24                      # DW_AT_decl_line
	.long	.Lsection_info+182      # DW_AT_type
	.long	.Ldebug_loc8            # DW_AT_location
	.byte	13                      # Abbrev [13] 0x5d:0xf DW_TAG_formal_parameter
	.long	.Linfo_string3          # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	24                      # DW_AT_decl_line
	.long	.Lsection_info+56       # DW_AT_type
	.long	.Ldebug_loc10           # DW_AT_location
	.byte	14                      # Abbrev [14] 0x6c:0x1e DW_TAG_lexical_block
	.quad	.Ltmp43                 # DW_AT_low_pc
	.quad	.Ltmp46                 # DW_AT_high_pc
	.byte	15                      # Abbrev [15] 0x7d:0xc DW_TAG_variable
	.long	.Linfo_string22         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	30                      # DW_AT_decl_line
	.long	.Lsection_info+56       # DW_AT_type
	.byte	0                       # DW_AT_const_value
	.byte	0                       # End Of Children Mark
	.byte	0                       # End Of Children Mark
	.byte	16                      # Abbrev [16] 0x8b:0x78 DW_TAG_subprogram
	.long	.Linfo_string8          # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	38                      # DW_AT_decl_line
                                        # DW_AT_prototyped
	.long	.Lsection_info+56       # DW_AT_type
                                        # DW_AT_external
	.quad	.Lfunc_begin2           # DW_AT_low_pc
	.quad	.Lfunc_end2             # DW_AT_high_pc
	.byte	1                       # DW_AT_frame_base
	.byte	87
                                        # DW_AT_APPLE_omit_frame_ptr
	.byte	13                      # Abbrev [13] 0xa8:0xf DW_TAG_formal_parameter
	.long	.Linfo_string18         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	38                      # DW_AT_decl_line
	.long	.Lsection_info+182      # DW_AT_type
	.long	.Ldebug_loc12           # DW_AT_location
	.byte	13                      # Abbrev [13] 0xb7:0xf DW_TAG_formal_parameter
	.long	.Linfo_string19         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	38                      # DW_AT_decl_line
	.long	.Lsection_info+182      # DW_AT_type
	.long	.Ldebug_loc14           # DW_AT_location
	.byte	13                      # Abbrev [13] 0xc6:0xf DW_TAG_formal_parameter
	.long	.Linfo_string3          # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	38                      # DW_AT_decl_line
	.long	.Lsection_info+56       # DW_AT_type
	.long	.Ldebug_loc16           # DW_AT_location
	.byte	17                      # Abbrev [17] 0xd5:0xf DW_TAG_variable
	.long	.Linfo_string21         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	39                      # DW_AT_decl_line
	.long	.Lsection_info+56       # DW_AT_type
	.long	.Ldebug_loc18           # DW_AT_location
	.byte	14                      # Abbrev [14] 0xe4:0x1e DW_TAG_lexical_block
	.quad	.Ltmp70                 # DW_AT_low_pc
	.quad	.Ltmp76                 # DW_AT_high_pc
	.byte	15                      # Abbrev [15] 0xf5:0xc DW_TAG_variable
	.long	.Linfo_string22         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	40                      # DW_AT_decl_line
	.long	.Lsection_info+56       # DW_AT_type
	.byte	0                       # DW_AT_const_value
	.byte	0                       # End Of Children Mark
	.byte	0                       # End Of Children Mark
	.byte	0                       # End Of Children Mark
.L.debug_info_end1:
.L.debug_info_begin2:
	.long	256                     # Length of Unit
	.short	4                       # DWARF version number
	.long	.L.debug_abbrev_begin   # Offset Into Abbrev. Section
	.byte	8                       # Address Size (in bytes)
	.byte	1                       # Abbrev [1] 0xb:0xf9 DW_TAG_compile_unit
	.long	.Linfo_string0          # DW_AT_producer
	.short	12                      # DW_AT_language
	.long	.Linfo_string9          # DW_AT_name
	.quad	0                       # DW_AT_low_pc
	.long	.Lsection_line          # DW_AT_stmt_list
	.long	.Linfo_string2          # DW_AT_comp_dir
                                        # DW_AT_APPLE_optimized
	.byte	12                      # Abbrev [12] 0x26:0x65 DW_TAG_subprogram
	.long	.Linfo_string10         # DW_AT_name
	.byte	3                       # DW_AT_decl_file
	.byte	24                      # DW_AT_decl_line
                                        # DW_AT_prototyped
                                        # DW_AT_external
	.quad	.Lfunc_begin3           # DW_AT_low_pc
	.quad	.Lfunc_end3             # DW_AT_high_pc
	.byte	1                       # DW_AT_frame_base
	.byte	87
                                        # DW_AT_APPLE_omit_frame_ptr
	.byte	13                      # Abbrev [13] 0x3f:0xf DW_TAG_formal_parameter
	.long	.Linfo_string18         # DW_AT_name
	.byte	3                       # DW_AT_decl_file
	.byte	24                      # DW_AT_decl_line
	.long	.Lsection_info+182      # DW_AT_type
	.long	.Ldebug_loc21           # DW_AT_location
	.byte	13                      # Abbrev [13] 0x4e:0xf DW_TAG_formal_parameter
	.long	.Linfo_string19         # DW_AT_name
	.byte	3                       # DW_AT_decl_file
	.byte	24                      # DW_AT_decl_line
	.long	.Lsection_info+182      # DW_AT_type
	.long	.Ldebug_loc23           # DW_AT_location
	.byte	13                      # Abbrev [13] 0x5d:0xf DW_TAG_formal_parameter
	.long	.Linfo_string3          # DW_AT_name
	.byte	3                       # DW_AT_decl_file
	.byte	24                      # DW_AT_decl_line
	.long	.Lsection_info+56       # DW_AT_type
	.long	.Ldebug_loc25           # DW_AT_location
	.byte	14                      # Abbrev [14] 0x6c:0x1e DW_TAG_lexical_block
	.quad	.Ltmp101                # DW_AT_low_pc
	.quad	.Ltmp104                # DW_AT_high_pc
	.byte	15                      # Abbrev [15] 0x7d:0xc DW_TAG_variable
	.long	.Linfo_string22         # DW_AT_name
	.byte	3                       # DW_AT_decl_file
	.byte	30                      # DW_AT_decl_line
	.long	.Lsection_info+56       # DW_AT_type
	.byte	0                       # DW_AT_const_value
	.byte	0                       # End Of Children Mark
	.byte	0                       # End Of Children Mark
	.byte	16                      # Abbrev [16] 0x8b:0x78 DW_TAG_subprogram
	.long	.Linfo_string11         # DW_AT_name
	.byte	3                       # DW_AT_decl_file
	.byte	38                      # DW_AT_decl_line
                                        # DW_AT_prototyped
	.long	.Lsection_info+56       # DW_AT_type
                                        # DW_AT_external
	.quad	.Lfunc_begin4           # DW_AT_low_pc
	.quad	.Lfunc_end4             # DW_AT_high_pc
	.byte	1                       # DW_AT_frame_base
	.byte	87
                                        # DW_AT_APPLE_omit_frame_ptr
	.byte	13                      # Abbrev [13] 0xa8:0xf DW_TAG_formal_parameter
	.long	.Linfo_string18         # DW_AT_name
	.byte	3                       # DW_AT_decl_file
	.byte	38                      # DW_AT_decl_line
	.long	.Lsection_info+182      # DW_AT_type
	.long	.Ldebug_loc27           # DW_AT_location
	.byte	13                      # Abbrev [13] 0xb7:0xf DW_TAG_formal_parameter
	.long	.Linfo_string19         # DW_AT_name
	.byte	3                       # DW_AT_decl_file
	.byte	38                      # DW_AT_decl_line
	.long	.Lsection_info+182      # DW_AT_type
	.long	.Ldebug_loc29           # DW_AT_location
	.byte	13                      # Abbrev [13] 0xc6:0xf DW_TAG_formal_parameter
	.long	.Linfo_string3          # DW_AT_name
	.byte	3                       # DW_AT_decl_file
	.byte	38                      # DW_AT_decl_line
	.long	.Lsection_info+56       # DW_AT_type
	.long	.Ldebug_loc31           # DW_AT_location
	.byte	17                      # Abbrev [17] 0xd5:0xf DW_TAG_variable
	.long	.Linfo_string21         # DW_AT_name
	.byte	3                       # DW_AT_decl_file
	.byte	39                      # DW_AT_decl_line
	.long	.Lsection_info+56       # DW_AT_type
	.long	.Ldebug_loc33           # DW_AT_location
	.byte	14                      # Abbrev [14] 0xe4:0x1e DW_TAG_lexical_block
	.quad	.Ltmp128                # DW_AT_low_pc
	.quad	.Ltmp134                # DW_AT_high_pc
	.byte	15                      # Abbrev [15] 0xf5:0xc DW_TAG_variable
	.long	.Linfo_string22         # DW_AT_name
	.byte	3                       # DW_AT_decl_file
	.byte	40                      # DW_AT_decl_line
	.long	.Lsection_info+56       # DW_AT_type
	.byte	0                       # DW_AT_const_value
	.byte	0                       # End Of Children Mark
	.byte	0                       # End Of Children Mark
	.byte	0                       # End Of Children Mark
.L.debug_info_end2:
.L.debug_info_begin3:
	.long	525                     # Length of Unit
	.short	4                       # DWARF version number
	.long	.L.debug_abbrev_begin   # Offset Into Abbrev. Section
	.byte	8                       # Address Size (in bytes)
	.byte	1                       # Abbrev [1] 0xb:0x206 DW_TAG_compile_unit
	.long	.Linfo_string0          # DW_AT_producer
	.short	12                      # DW_AT_language
	.long	.Linfo_string12         # DW_AT_name
	.quad	0                       # DW_AT_low_pc
	.long	.Lsection_line          # DW_AT_stmt_list
	.long	.Linfo_string2          # DW_AT_comp_dir
                                        # DW_AT_APPLE_optimized
	.byte	18                      # Abbrev [18] 0x26:0x4c DW_TAG_subprogram
	.long	.Linfo_string13         # DW_AT_name
	.byte	4                       # DW_AT_decl_file
	.byte	14                      # DW_AT_decl_line
                                        # DW_AT_prototyped
	.long	.Lsection_info+56       # DW_AT_type
                                        # DW_AT_external
	.quad	.Lfunc_begin5           # DW_AT_low_pc
	.quad	.Lfunc_end5             # DW_AT_high_pc
	.byte	1                       # DW_AT_frame_base
	.byte	87
                                        # DW_AT_APPLE_omit_frame_ptr
	.byte	1                       # DW_AT_inline
	.byte	19                      # Abbrev [19] 0x44:0xf DW_TAG_formal_parameter
	.long	.Linfo_string23         # DW_AT_name
	.byte	4                       # DW_AT_decl_file
	.byte	14                      # DW_AT_decl_line
	.long	502                     # DW_AT_type
	.long	.Ldebug_loc36           # DW_AT_location
	.byte	19                      # Abbrev [19] 0x53:0xf DW_TAG_formal_parameter
	.long	.Linfo_string24         # DW_AT_name
	.byte	4                       # DW_AT_decl_file
	.byte	14                      # DW_AT_decl_line
	.long	502                     # DW_AT_type
	.long	.Ldebug_loc38           # DW_AT_location
	.byte	19                      # Abbrev [19] 0x62:0xf DW_TAG_formal_parameter
	.long	.Linfo_string25         # DW_AT_name
	.byte	4                       # DW_AT_decl_file
	.byte	14                      # DW_AT_decl_line
	.long	503                     # DW_AT_type
	.long	.Ldebug_loc40           # DW_AT_location
	.byte	0                       # End Of Children Mark
	.byte	16                      # Abbrev [16] 0x72:0x90 DW_TAG_subprogram
	.long	.Linfo_string14         # DW_AT_name
	.byte	4                       # DW_AT_decl_file
	.byte	20                      # DW_AT_decl_line
                                        # DW_AT_prototyped
	.long	.Lsection_info+56       # DW_AT_type
                                        # DW_AT_external
	.quad	.Lfunc_begin6           # DW_AT_low_pc
	.quad	.Lfunc_end6             # DW_AT_high_pc
	.byte	1                       # DW_AT_frame_base
	.byte	87
                                        # DW_AT_APPLE_omit_frame_ptr
	.byte	19                      # Abbrev [19] 0x8f:0xf DW_TAG_formal_parameter
	.long	.Linfo_string23         # DW_AT_name
	.byte	4                       # DW_AT_decl_file
	.byte	20                      # DW_AT_decl_line
	.long	502                     # DW_AT_type
	.long	.Ldebug_loc42           # DW_AT_location
	.byte	19                      # Abbrev [19] 0x9e:0xf DW_TAG_formal_parameter
	.long	.Linfo_string28         # DW_AT_name
	.byte	4                       # DW_AT_decl_file
	.byte	20                      # DW_AT_decl_line
	.long	502                     # DW_AT_type
	.long	.Ldebug_loc44           # DW_AT_location
	.byte	19                      # Abbrev [19] 0xad:0xf DW_TAG_formal_parameter
	.long	.Linfo_string25         # DW_AT_name
	.byte	4                       # DW_AT_decl_file
	.byte	20                      # DW_AT_decl_line
	.long	503                     # DW_AT_type
	.long	.Ldebug_loc46           # DW_AT_location
	.byte	20                      # Abbrev [20] 0xbc:0x45 DW_TAG_inlined_subroutine
	.long	38                      # DW_AT_abstract_origin
	.quad	.Ltmp164                # DW_AT_low_pc
	.quad	.Ltmp166                # DW_AT_high_pc
	.byte	4                       # DW_AT_call_file
	.byte	21                      # DW_AT_call_line
	.byte	19                      # Abbrev [19] 0xd3:0xf DW_TAG_formal_parameter
	.long	.Linfo_string23         # DW_AT_name
	.byte	4                       # DW_AT_decl_file
	.byte	14                      # DW_AT_decl_line
	.long	502                     # DW_AT_type
	.long	.Ldebug_loc48           # DW_AT_location
	.byte	19                      # Abbrev [19] 0xe2:0xf DW_TAG_formal_parameter
	.long	.Linfo_string24         # DW_AT_name
	.byte	4                       # DW_AT_decl_file
	.byte	14                      # DW_AT_decl_line
	.long	502                     # DW_AT_type
	.long	.Ldebug_loc50           # DW_AT_location
	.byte	19                      # Abbrev [19] 0xf1:0xf DW_TAG_formal_parameter
	.long	.Linfo_string25         # DW_AT_name
	.byte	4                       # DW_AT_decl_file
	.byte	14                      # DW_AT_decl_line
	.long	503                     # DW_AT_type
	.long	.Ldebug_loc52           # DW_AT_location
	.byte	0                       # End Of Children Mark
	.byte	0                       # End Of Children Mark
	.byte	16                      # Abbrev [16] 0x102:0x90 DW_TAG_subprogram
	.long	.Linfo_string15         # DW_AT_name
	.byte	4                       # DW_AT_decl_file
	.byte	24                      # DW_AT_decl_line
                                        # DW_AT_prototyped
	.long	.Lsection_info+56       # DW_AT_type
                                        # DW_AT_external
	.quad	.Lfunc_begin7           # DW_AT_low_pc
	.quad	.Lfunc_end7             # DW_AT_high_pc
	.byte	1                       # DW_AT_frame_base
	.byte	87
                                        # DW_AT_APPLE_omit_frame_ptr
	.byte	19                      # Abbrev [19] 0x11f:0xf DW_TAG_formal_parameter
	.long	.Linfo_string29         # DW_AT_name
	.byte	4                       # DW_AT_decl_file
	.byte	24                      # DW_AT_decl_line
	.long	502                     # DW_AT_type
	.long	.Ldebug_loc54           # DW_AT_location
	.byte	19                      # Abbrev [19] 0x12e:0xf DW_TAG_formal_parameter
	.long	.Linfo_string24         # DW_AT_name
	.byte	4                       # DW_AT_decl_file
	.byte	24                      # DW_AT_decl_line
	.long	502                     # DW_AT_type
	.long	.Ldebug_loc56           # DW_AT_location
	.byte	19                      # Abbrev [19] 0x13d:0xf DW_TAG_formal_parameter
	.long	.Linfo_string25         # DW_AT_name
	.byte	4                       # DW_AT_decl_file
	.byte	24                      # DW_AT_decl_line
	.long	503                     # DW_AT_type
	.long	.Ldebug_loc58           # DW_AT_location
	.byte	20                      # Abbrev [20] 0x14c:0x45 DW_TAG_inlined_subroutine
	.long	38                      # DW_AT_abstract_origin
	.quad	.Ltmp181                # DW_AT_low_pc
	.quad	.Ltmp183                # DW_AT_high_pc
	.byte	4                       # DW_AT_call_file
	.byte	25                      # DW_AT_call_line
	.byte	19                      # Abbrev [19] 0x163:0xf DW_TAG_formal_parameter
	.long	.Linfo_string23         # DW_AT_name
	.byte	4                       # DW_AT_decl_file
	.byte	14                      # DW_AT_decl_line
	.long	502                     # DW_AT_type
	.long	.Ldebug_loc60           # DW_AT_location
	.byte	19                      # Abbrev [19] 0x172:0xf DW_TAG_formal_parameter
	.long	.Linfo_string24         # DW_AT_name
	.byte	4                       # DW_AT_decl_file
	.byte	14                      # DW_AT_decl_line
	.long	502                     # DW_AT_type
	.long	.Ldebug_loc62           # DW_AT_location
	.byte	19                      # Abbrev [19] 0x181:0xf DW_TAG_formal_parameter
	.long	.Linfo_string25         # DW_AT_name
	.byte	4                       # DW_AT_decl_file
	.byte	14                      # DW_AT_decl_line
	.long	503                     # DW_AT_type
	.long	.Ldebug_loc64           # DW_AT_location
	.byte	0                       # End Of Children Mark
	.byte	0                       # End Of Children Mark
	.byte	16                      # Abbrev [16] 0x192:0x4b DW_TAG_subprogram
	.long	.Linfo_string16         # DW_AT_name
	.byte	4                       # DW_AT_decl_file
	.byte	28                      # DW_AT_decl_line
                                        # DW_AT_prototyped
	.long	.Lsection_info+56       # DW_AT_type
                                        # DW_AT_external
	.quad	.Lfunc_begin8           # DW_AT_low_pc
	.quad	.Lfunc_end8             # DW_AT_high_pc
	.byte	1                       # DW_AT_frame_base
	.byte	87
                                        # DW_AT_APPLE_omit_frame_ptr
	.byte	21                      # Abbrev [21] 0x1af:0xf DW_TAG_formal_parameter
	.long	.Linfo_string30         # DW_AT_name
	.byte	4                       # DW_AT_decl_file
	.byte	28                      # DW_AT_decl_line
	.long	502                     # DW_AT_type
	.byte	3                       # DW_AT_location
	.byte	145
	.asciz	"\340"
	.byte	21                      # Abbrev [21] 0x1be:0xf DW_TAG_formal_parameter
	.long	.Linfo_string25         # DW_AT_name
	.byte	4                       # DW_AT_decl_file
	.byte	28                      # DW_AT_decl_line
	.long	503                     # DW_AT_type
	.byte	3                       # DW_AT_location
	.byte	145
	.asciz	"\330"
	.byte	19                      # Abbrev [19] 0x1cd:0xf DW_TAG_formal_parameter
	.long	.Linfo_string31         # DW_AT_name
	.byte	4                       # DW_AT_decl_file
	.byte	28                      # DW_AT_decl_line
	.long	521                     # DW_AT_type
	.long	.Ldebug_loc66           # DW_AT_location
	.byte	0                       # End Of Children Mark
	.byte	22                      # Abbrev [22] 0x1dd:0x19 DW_TAG_subprogram
	.long	.Linfo_string17         # DW_AT_name
	.byte	4                       # DW_AT_decl_file
	.byte	71                      # DW_AT_decl_line
                                        # DW_AT_external
	.quad	.Lfunc_begin9           # DW_AT_low_pc
	.quad	.Lfunc_end9             # DW_AT_high_pc
	.byte	1                       # DW_AT_frame_base
	.byte	87
                                        # DW_AT_APPLE_omit_frame_ptr
	.byte	23                      # Abbrev [23] 0x1f6:0x1 DW_TAG_pointer_type
	.byte	24                      # Abbrev [24] 0x1f7:0xb DW_TAG_typedef
	.long	514                     # DW_AT_type
	.long	.Linfo_string27         # DW_AT_name
	.byte	4                       # DW_AT_decl_file
	.byte	42                      # DW_AT_decl_line
	.byte	4                       # Abbrev [4] 0x202:0x7 DW_TAG_base_type
	.long	.Linfo_string26         # DW_AT_name
	.byte	7                       # DW_AT_encoding
	.byte	8                       # DW_AT_byte_size
	.byte	4                       # Abbrev [4] 0x209:0x7 DW_TAG_base_type
	.long	.Linfo_string32         # DW_AT_name
	.byte	7                       # DW_AT_encoding
	.byte	4                       # DW_AT_byte_size
	.byte	0                       # End Of Children Mark
.L.debug_info_end3:
	.section	.debug_abbrev,"",@progbits
.L.debug_abbrev_begin:
	.byte	1                       # Abbreviation Code
	.byte	17                      # DW_TAG_compile_unit
	.byte	1                       # DW_CHILDREN_yes
	.byte	37                      # DW_AT_producer
	.byte	14                      # DW_FORM_strp
	.byte	19                      # DW_AT_language
	.byte	5                       # DW_FORM_data2
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	17                      # DW_AT_low_pc
	.byte	1                       # DW_FORM_addr
	.byte	16                      # DW_AT_stmt_list
	.byte	23                      # DW_FORM_sec_offset
	.byte	27                      # DW_AT_comp_dir
	.byte	14                      # DW_FORM_strp
	.ascii	"\341\177"              # DW_AT_APPLE_optimized
	.byte	25                      # DW_FORM_flag_present
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	2                       # Abbreviation Code
	.byte	52                      # DW_TAG_variable
	.byte	0                       # DW_CHILDREN_no
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	28                      # DW_AT_const_value
	.byte	13                      # DW_FORM_sdata
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	3                       # Abbreviation Code
	.byte	38                      # DW_TAG_const_type
	.byte	0                       # DW_CHILDREN_no
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	4                       # Abbreviation Code
	.byte	36                      # DW_TAG_base_type
	.byte	0                       # DW_CHILDREN_no
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	62                      # DW_AT_encoding
	.byte	11                      # DW_FORM_data1
	.byte	11                      # DW_AT_byte_size
	.byte	11                      # DW_FORM_data1
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	5                       # Abbreviation Code
	.byte	46                      # DW_TAG_subprogram
	.byte	1                       # DW_CHILDREN_yes
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	63                      # DW_AT_external
	.byte	25                      # DW_FORM_flag_present
	.byte	17                      # DW_AT_low_pc
	.byte	1                       # DW_FORM_addr
	.byte	18                      # DW_AT_high_pc
	.byte	1                       # DW_FORM_addr
	.byte	64                      # DW_AT_frame_base
	.byte	10                      # DW_FORM_block1
	.ascii	"\347\177"              # DW_AT_APPLE_omit_frame_ptr
	.byte	25                      # DW_FORM_flag_present
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	6                       # Abbreviation Code
	.byte	52                      # DW_TAG_variable
	.byte	0                       # DW_CHILDREN_no
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	2                       # DW_AT_location
	.byte	10                      # DW_FORM_block1
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	7                       # Abbreviation Code
	.byte	52                      # DW_TAG_variable
	.byte	0                       # DW_CHILDREN_no
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	28                      # DW_AT_const_value
	.byte	13                      # DW_FORM_sdata
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	8                       # Abbreviation Code
	.byte	52                      # DW_TAG_variable
	.byte	0                       # DW_CHILDREN_no
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	2                       # DW_AT_location
	.byte	23                      # DW_FORM_sec_offset
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	9                       # Abbreviation Code
	.byte	11                      # DW_TAG_lexical_block
	.byte	1                       # DW_CHILDREN_yes
	.byte	85                      # DW_AT_ranges
	.byte	6                       # DW_FORM_data4
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	10                      # Abbreviation Code
	.byte	52                      # DW_TAG_variable
	.byte	0                       # DW_CHILDREN_no
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	11                      # Abbreviation Code
	.byte	15                      # DW_TAG_pointer_type
	.byte	0                       # DW_CHILDREN_no
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	12                      # Abbreviation Code
	.byte	46                      # DW_TAG_subprogram
	.byte	1                       # DW_CHILDREN_yes
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	39                      # DW_AT_prototyped
	.byte	25                      # DW_FORM_flag_present
	.byte	63                      # DW_AT_external
	.byte	25                      # DW_FORM_flag_present
	.byte	17                      # DW_AT_low_pc
	.byte	1                       # DW_FORM_addr
	.byte	18                      # DW_AT_high_pc
	.byte	1                       # DW_FORM_addr
	.byte	64                      # DW_AT_frame_base
	.byte	10                      # DW_FORM_block1
	.ascii	"\347\177"              # DW_AT_APPLE_omit_frame_ptr
	.byte	25                      # DW_FORM_flag_present
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	13                      # Abbreviation Code
	.byte	5                       # DW_TAG_formal_parameter
	.byte	0                       # DW_CHILDREN_no
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	73                      # DW_AT_type
	.byte	16                      # DW_FORM_ref_addr
	.byte	2                       # DW_AT_location
	.byte	23                      # DW_FORM_sec_offset
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	14                      # Abbreviation Code
	.byte	11                      # DW_TAG_lexical_block
	.byte	1                       # DW_CHILDREN_yes
	.byte	17                      # DW_AT_low_pc
	.byte	1                       # DW_FORM_addr
	.byte	18                      # DW_AT_high_pc
	.byte	1                       # DW_FORM_addr
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	15                      # Abbreviation Code
	.byte	52                      # DW_TAG_variable
	.byte	0                       # DW_CHILDREN_no
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	73                      # DW_AT_type
	.byte	16                      # DW_FORM_ref_addr
	.byte	28                      # DW_AT_const_value
	.byte	13                      # DW_FORM_sdata
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	16                      # Abbreviation Code
	.byte	46                      # DW_TAG_subprogram
	.byte	1                       # DW_CHILDREN_yes
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	39                      # DW_AT_prototyped
	.byte	25                      # DW_FORM_flag_present
	.byte	73                      # DW_AT_type
	.byte	16                      # DW_FORM_ref_addr
	.byte	63                      # DW_AT_external
	.byte	25                      # DW_FORM_flag_present
	.byte	17                      # DW_AT_low_pc
	.byte	1                       # DW_FORM_addr
	.byte	18                      # DW_AT_high_pc
	.byte	1                       # DW_FORM_addr
	.byte	64                      # DW_AT_frame_base
	.byte	10                      # DW_FORM_block1
	.ascii	"\347\177"              # DW_AT_APPLE_omit_frame_ptr
	.byte	25                      # DW_FORM_flag_present
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	17                      # Abbreviation Code
	.byte	52                      # DW_TAG_variable
	.byte	0                       # DW_CHILDREN_no
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	73                      # DW_AT_type
	.byte	16                      # DW_FORM_ref_addr
	.byte	2                       # DW_AT_location
	.byte	23                      # DW_FORM_sec_offset
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	18                      # Abbreviation Code
	.byte	46                      # DW_TAG_subprogram
	.byte	1                       # DW_CHILDREN_yes
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	39                      # DW_AT_prototyped
	.byte	25                      # DW_FORM_flag_present
	.byte	73                      # DW_AT_type
	.byte	16                      # DW_FORM_ref_addr
	.byte	63                      # DW_AT_external
	.byte	25                      # DW_FORM_flag_present
	.byte	17                      # DW_AT_low_pc
	.byte	1                       # DW_FORM_addr
	.byte	18                      # DW_AT_high_pc
	.byte	1                       # DW_FORM_addr
	.byte	64                      # DW_AT_frame_base
	.byte	10                      # DW_FORM_block1
	.ascii	"\347\177"              # DW_AT_APPLE_omit_frame_ptr
	.byte	25                      # DW_FORM_flag_present
	.byte	32                      # DW_AT_inline
	.byte	11                      # DW_FORM_data1
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	19                      # Abbreviation Code
	.byte	5                       # DW_TAG_formal_parameter
	.byte	0                       # DW_CHILDREN_no
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	2                       # DW_AT_location
	.byte	23                      # DW_FORM_sec_offset
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	20                      # Abbreviation Code
	.byte	29                      # DW_TAG_inlined_subroutine
	.byte	1                       # DW_CHILDREN_yes
	.byte	49                      # DW_AT_abstract_origin
	.byte	19                      # DW_FORM_ref4
	.byte	17                      # DW_AT_low_pc
	.byte	1                       # DW_FORM_addr
	.byte	18                      # DW_AT_high_pc
	.byte	1                       # DW_FORM_addr
	.byte	88                      # DW_AT_call_file
	.byte	11                      # DW_FORM_data1
	.byte	89                      # DW_AT_call_line
	.byte	11                      # DW_FORM_data1
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	21                      # Abbreviation Code
	.byte	5                       # DW_TAG_formal_parameter
	.byte	0                       # DW_CHILDREN_no
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	2                       # DW_AT_location
	.byte	10                      # DW_FORM_block1
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	22                      # Abbreviation Code
	.byte	46                      # DW_TAG_subprogram
	.byte	0                       # DW_CHILDREN_no
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	63                      # DW_AT_external
	.byte	25                      # DW_FORM_flag_present
	.byte	17                      # DW_AT_low_pc
	.byte	1                       # DW_FORM_addr
	.byte	18                      # DW_AT_high_pc
	.byte	1                       # DW_FORM_addr
	.byte	64                      # DW_AT_frame_base
	.byte	10                      # DW_FORM_block1
	.ascii	"\347\177"              # DW_AT_APPLE_omit_frame_ptr
	.byte	25                      # DW_FORM_flag_present
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	23                      # Abbreviation Code
	.byte	15                      # DW_TAG_pointer_type
	.byte	0                       # DW_CHILDREN_no
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	24                      # Abbreviation Code
	.byte	22                      # DW_TAG_typedef
	.byte	0                       # DW_CHILDREN_no
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	0                       # EOM(3)
.L.debug_abbrev_end:
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
	.quad	.Ltmp3
	.quad	.Ltmp4
.Lset0 = .Ltmp253-.Ltmp252              # Loc expr size
	.short	.Lset0
.Ltmp252:
	.byte	80                      # DW_OP_reg0
.Ltmp253:
	.quad	.Ltmp5
	.quad	.Ltmp6
.Lset1 = .Ltmp255-.Ltmp254              # Loc expr size
	.short	.Lset1
.Ltmp254:
	.byte	80                      # DW_OP_reg0
.Ltmp255:
	.quad	0
	.quad	0
.Ldebug_loc3:
	.quad	.Ltmp13
	.quad	.Ltmp15
.Lset2 = .Ltmp257-.Ltmp256              # Loc expr size
	.short	.Lset2
.Ltmp256:
	.byte	80                      # DW_OP_reg0
.Ltmp257:
	.quad	.Ltmp15
	.quad	.Ltmp16
.Lset3 = .Ltmp259-.Ltmp258              # Loc expr size
	.short	.Lset3
.Ltmp258:
	.byte	119                     # DW_OP_breg7
	.byte	28
.Ltmp259:
	.quad	0
	.quad	0
.Ldebug_loc6:
	.quad	.Lfunc_begin1
	.quad	.Ltmp45
.Lset4 = .Ltmp261-.Ltmp260              # Loc expr size
	.short	.Lset4
.Ltmp260:
	.byte	119                     # DW_OP_breg7
	.ascii	"\370\007"
.Ltmp261:
	.quad	0
	.quad	0
.Ldebug_loc8:
	.quad	.Lfunc_begin1
	.quad	.Ltmp45
.Lset5 = .Ltmp263-.Ltmp262              # Loc expr size
	.short	.Lset5
.Ltmp262:
	.byte	119                     # DW_OP_breg7
	.ascii	"\360\007"
.Ltmp263:
	.quad	0
	.quad	0
.Ldebug_loc10:
	.quad	.Lfunc_begin1
	.quad	.Ltmp42
.Lset6 = .Ltmp265-.Ltmp264              # Loc expr size
	.short	.Lset6
.Ltmp264:
	.byte	119                     # DW_OP_breg7
	.ascii	"\344\b"
.Ltmp265:
	.quad	0
	.quad	0
.Ldebug_loc12:
	.quad	.Lfunc_begin2
	.quad	.Ltmp71
.Lset7 = .Ltmp267-.Ltmp266              # Loc expr size
	.short	.Lset7
.Ltmp266:
	.byte	119                     # DW_OP_breg7
	.ascii	"\220\001"
.Ltmp267:
	.quad	0
	.quad	0
.Ldebug_loc14:
	.quad	.Lfunc_begin2
	.quad	.Ltmp71
.Lset8 = .Ltmp269-.Ltmp268              # Loc expr size
	.short	.Lset8
.Ltmp268:
	.byte	119                     # DW_OP_breg7
	.ascii	"\210\001"
.Ltmp269:
	.quad	0
	.quad	0
.Ldebug_loc16:
	.quad	.Lfunc_begin2
	.quad	.Ltmp71
.Lset9 = .Ltmp271-.Ltmp270              # Loc expr size
	.short	.Lset9
.Ltmp270:
	.byte	119                     # DW_OP_breg7
	.ascii	"\204\001"
.Ltmp271:
	.quad	0
	.quad	0
.Ldebug_loc18:
	.quad	.Ltmp70
	.quad	.Ltmp74
.Lset10 = .Ltmp273-.Ltmp272             # Loc expr size
	.short	.Lset10
.Ltmp272:
	.byte	16                      # DW_OP_constu
	.byte	0
.Ltmp273:
	.quad	.Ltmp74
	.quad	.Ltmp75
.Lset11 = .Ltmp275-.Ltmp274             # Loc expr size
	.short	.Lset11
.Ltmp274:
	.byte	82                      # DW_OP_reg2
.Ltmp275:
	.quad	0
	.quad	0
.Ldebug_loc21:
	.quad	.Lfunc_begin3
	.quad	.Ltmp103
.Lset12 = .Ltmp277-.Ltmp276             # Loc expr size
	.short	.Lset12
.Ltmp276:
	.byte	119                     # DW_OP_breg7
	.ascii	"\370\007"
.Ltmp277:
	.quad	0
	.quad	0
.Ldebug_loc23:
	.quad	.Lfunc_begin3
	.quad	.Ltmp103
.Lset13 = .Ltmp279-.Ltmp278             # Loc expr size
	.short	.Lset13
.Ltmp278:
	.byte	119                     # DW_OP_breg7
	.ascii	"\360\007"
.Ltmp279:
	.quad	0
	.quad	0
.Ldebug_loc25:
	.quad	.Lfunc_begin3
	.quad	.Ltmp100
.Lset14 = .Ltmp281-.Ltmp280             # Loc expr size
	.short	.Lset14
.Ltmp280:
	.byte	119                     # DW_OP_breg7
	.ascii	"\344\b"
.Ltmp281:
	.quad	0
	.quad	0
.Ldebug_loc27:
	.quad	.Lfunc_begin4
	.quad	.Ltmp129
.Lset15 = .Ltmp283-.Ltmp282             # Loc expr size
	.short	.Lset15
.Ltmp282:
	.byte	119                     # DW_OP_breg7
	.ascii	"\220\001"
.Ltmp283:
	.quad	0
	.quad	0
.Ldebug_loc29:
	.quad	.Lfunc_begin4
	.quad	.Ltmp129
.Lset16 = .Ltmp285-.Ltmp284             # Loc expr size
	.short	.Lset16
.Ltmp284:
	.byte	119                     # DW_OP_breg7
	.ascii	"\210\001"
.Ltmp285:
	.quad	0
	.quad	0
.Ldebug_loc31:
	.quad	.Lfunc_begin4
	.quad	.Ltmp129
.Lset17 = .Ltmp287-.Ltmp286             # Loc expr size
	.short	.Lset17
.Ltmp286:
	.byte	119                     # DW_OP_breg7
	.ascii	"\204\001"
.Ltmp287:
	.quad	0
	.quad	0
.Ldebug_loc33:
	.quad	.Ltmp128
	.quad	.Ltmp132
.Lset18 = .Ltmp289-.Ltmp288             # Loc expr size
	.short	.Lset18
.Ltmp288:
	.byte	16                      # DW_OP_constu
	.byte	0
.Ltmp289:
	.quad	.Ltmp132
	.quad	.Ltmp133
.Lset19 = .Ltmp291-.Ltmp290             # Loc expr size
	.short	.Lset19
.Ltmp290:
	.byte	82                      # DW_OP_reg2
.Ltmp291:
	.quad	0
	.quad	0
.Ldebug_loc36:
	.quad	.Lfunc_begin5
	.quad	.Ltmp149
.Lset20 = .Ltmp293-.Ltmp292             # Loc expr size
	.short	.Lset20
.Ltmp292:
	.byte	119                     # DW_OP_breg7
	.asciz	"\330"
.Ltmp293:
	.quad	0
	.quad	0
.Ldebug_loc38:
	.quad	.Lfunc_begin5
	.quad	.Ltmp149
.Lset21 = .Ltmp295-.Ltmp294             # Loc expr size
	.short	.Lset21
.Ltmp294:
	.byte	119                     # DW_OP_breg7
	.asciz	"\320"
.Ltmp295:
	.quad	0
	.quad	0
.Ldebug_loc40:
	.quad	.Lfunc_begin5
	.quad	.Ltmp149
.Lset22 = .Ltmp297-.Ltmp296             # Loc expr size
	.short	.Lset22
.Ltmp296:
	.byte	119                     # DW_OP_breg7
	.asciz	"\310"
.Ltmp297:
	.quad	0
	.quad	0
.Ldebug_loc42:
	.quad	.Lfunc_begin6
	.quad	.Ltmp165
.Lset23 = .Ltmp299-.Ltmp298             # Loc expr size
	.short	.Lset23
.Ltmp298:
	.byte	119                     # DW_OP_breg7
	.asciz	"\330"
.Ltmp299:
	.quad	0
	.quad	0
.Ldebug_loc44:
	.quad	.Lfunc_begin6
	.quad	.Ltmp165
.Lset24 = .Ltmp301-.Ltmp300             # Loc expr size
	.short	.Lset24
.Ltmp300:
	.byte	119                     # DW_OP_breg7
	.asciz	"\320"
.Ltmp301:
	.quad	0
	.quad	0
.Ldebug_loc46:
	.quad	.Lfunc_begin6
	.quad	.Ltmp165
.Lset25 = .Ltmp303-.Ltmp302             # Loc expr size
	.short	.Lset25
.Ltmp302:
	.byte	119                     # DW_OP_breg7
	.asciz	"\310"
.Ltmp303:
	.quad	0
	.quad	0
.Ldebug_loc48:
	.quad	.Ltmp164
	.quad	.Ltmp165
.Lset26 = .Ltmp305-.Ltmp304             # Loc expr size
	.short	.Lset26
.Ltmp304:
	.byte	119                     # DW_OP_breg7
	.asciz	"\330"
.Ltmp305:
	.quad	0
	.quad	0
.Ldebug_loc50:
	.quad	.Ltmp164
	.quad	.Ltmp165
.Lset27 = .Ltmp307-.Ltmp306             # Loc expr size
	.short	.Lset27
.Ltmp306:
	.byte	119                     # DW_OP_breg7
	.asciz	"\320"
.Ltmp307:
	.quad	0
	.quad	0
.Ldebug_loc52:
	.quad	.Ltmp164
	.quad	.Ltmp165
.Lset28 = .Ltmp309-.Ltmp308             # Loc expr size
	.short	.Lset28
.Ltmp308:
	.byte	119                     # DW_OP_breg7
	.asciz	"\310"
.Ltmp309:
	.quad	0
	.quad	0
.Ldebug_loc54:
	.quad	.Lfunc_begin7
	.quad	.Ltmp182
.Lset29 = .Ltmp311-.Ltmp310             # Loc expr size
	.short	.Lset29
.Ltmp310:
	.byte	119                     # DW_OP_breg7
	.asciz	"\330"
.Ltmp311:
	.quad	0
	.quad	0
.Ldebug_loc56:
	.quad	.Lfunc_begin7
	.quad	.Ltmp182
.Lset30 = .Ltmp313-.Ltmp312             # Loc expr size
	.short	.Lset30
.Ltmp312:
	.byte	119                     # DW_OP_breg7
	.asciz	"\320"
.Ltmp313:
	.quad	0
	.quad	0
.Ldebug_loc58:
	.quad	.Lfunc_begin7
	.quad	.Ltmp182
.Lset31 = .Ltmp315-.Ltmp314             # Loc expr size
	.short	.Lset31
.Ltmp314:
	.byte	119                     # DW_OP_breg7
	.asciz	"\310"
.Ltmp315:
	.quad	0
	.quad	0
.Ldebug_loc60:
	.quad	.Ltmp181
	.quad	.Ltmp182
.Lset32 = .Ltmp317-.Ltmp316             # Loc expr size
	.short	.Lset32
.Ltmp316:
	.byte	119                     # DW_OP_breg7
	.asciz	"\330"
.Ltmp317:
	.quad	0
	.quad	0
.Ldebug_loc62:
	.quad	.Ltmp181
	.quad	.Ltmp182
.Lset33 = .Ltmp319-.Ltmp318             # Loc expr size
	.short	.Lset33
.Ltmp318:
	.byte	119                     # DW_OP_breg7
	.asciz	"\320"
.Ltmp319:
	.quad	0
	.quad	0
.Ldebug_loc64:
	.quad	.Ltmp181
	.quad	.Ltmp182
.Lset34 = .Ltmp321-.Ltmp320             # Loc expr size
	.short	.Lset34
.Ltmp320:
	.byte	119                     # DW_OP_breg7
	.asciz	"\310"
.Ltmp321:
	.quad	0
	.quad	0
.Ldebug_loc66:
	.quad	.Lfunc_begin8
	.quad	.Ltmp207
.Lset35 = .Ltmp323-.Ltmp322             # Loc expr size
	.short	.Lset35
.Ltmp322:
	.byte	119                     # DW_OP_breg7
	.asciz	"\324"
.Ltmp323:
	.quad	0
	.quad	0
.Ldebug_loc68:
	.section	.debug_aranges,"",@progbits
	.long	44                      # Length of ARange Set
	.short	2                       # DWARF Arange version number
	.long	.L.debug_info_begin0    # Offset Into Debug Info Section
	.byte	8                       # Address Size (in bytes)
	.byte	0                       # Segment Size (in bytes)
	.byte	255
	.byte	255
	.byte	255
	.byte	255
	.quad	.Lfunc_begin0
.Lset36 = .Lfunc_begin1-.Lfunc_begin0
	.quad	.Lset36
	.quad	0                       # ARange terminator
	.quad	0
	.long	44                      # Length of ARange Set
	.short	2                       # DWARF Arange version number
	.long	.L.debug_info_begin1    # Offset Into Debug Info Section
	.byte	8                       # Address Size (in bytes)
	.byte	0                       # Segment Size (in bytes)
	.byte	255
	.byte	255
	.byte	255
	.byte	255
	.quad	.Lfunc_begin1
.Lset37 = .Lfunc_begin3-.Lfunc_begin1
	.quad	.Lset37
	.quad	0                       # ARange terminator
	.quad	0
	.long	44                      # Length of ARange Set
	.short	2                       # DWARF Arange version number
	.long	.L.debug_info_begin2    # Offset Into Debug Info Section
	.byte	8                       # Address Size (in bytes)
	.byte	0                       # Segment Size (in bytes)
	.byte	255
	.byte	255
	.byte	255
	.byte	255
	.quad	.Lfunc_begin3
.Lset38 = .Lfunc_begin5-.Lfunc_begin3
	.quad	.Lset38
	.quad	0                       # ARange terminator
	.quad	0
	.long	44                      # Length of ARange Set
	.short	2                       # DWARF Arange version number
	.long	.L.debug_info_begin3    # Offset Into Debug Info Section
	.byte	8                       # Address Size (in bytes)
	.byte	0                       # Segment Size (in bytes)
	.byte	255
	.byte	255
	.byte	255
	.byte	255
	.quad	.Lfunc_begin5
.Lset39 = .Ldebug_end0-.Lfunc_begin5
	.quad	.Lset39
	.quad	0                       # ARange terminator
	.quad	0
	.section	.debug_ranges,"",@progbits
	.quad	.Ltmp7
	.quad	.Ltmp9
	.quad	.Ltmp10
	.quad	.Ltmp12
	.quad	0
	.quad	0
	.section	.debug_macinfo,"",@progbits
	.section	.debug_pubnames,"",@progbits
.Lset40 = .Lpubnames_end2-.Lpubnames_begin2 # Length of Public Names Info
	.long	.Lset40
.Lpubnames_begin2:
	.short	2                       # DWARF Version
	.long	.L.debug_info_begin2    # Offset of Compilation Unit Info
.Lset41 = .L.debug_info_end2-.L.debug_info_begin2 # Compilation Unit Length
	.long	.Lset41
	.long	38                      # DIE offset
	.asciz	"test_kernel"           # External Name
	.long	139                     # DIE offset
	.asciz	"test_stores_plus"      # External Name
	.long	0                       # End Mark
.Lpubnames_end2:
.Lset42 = .Lpubnames_end0-.Lpubnames_begin0 # Length of Public Names Info
	.long	.Lset42
.Lpubnames_begin0:
	.short	2                       # DWARF Version
	.long	.L.debug_info_begin0    # Offset of Compilation Unit Info
.Lset43 = .L.debug_info_end0-.L.debug_info_begin0 # Compilation Unit Length
	.long	.Lset43
	.long	63                      # DIE offset
	.asciz	"main"                  # External Name
	.long	0                       # End Mark
.Lpubnames_end0:
.Lset44 = .Lpubnames_end3-.Lpubnames_begin3 # Length of Public Names Info
	.long	.Lset44
.Lpubnames_begin3:
	.short	2                       # DWARF Version
	.long	.L.debug_info_begin3    # Offset of Compilation Unit Info
.Lset45 = .L.debug_info_end3-.L.debug_info_begin3 # Compilation Unit Length
	.long	.Lset45
	.long	114                     # DIE offset
	.asciz	"dmaLoad"               # External Name
	.long	402                     # DIE offset
	.asciz	"setReadyBits"          # External Name
	.long	477                     # DIE offset
	.asciz	"dmaFence"              # External Name
	.long	38                      # DIE offset
	.asciz	"_dmaImpl3"             # External Name
	.long	258                     # DIE offset
	.asciz	"dmaStore"              # External Name
	.long	0                       # End Mark
.Lpubnames_end3:
.Lset46 = .Lpubnames_end1-.Lpubnames_begin1 # Length of Public Names Info
	.long	.Lset46
.Lpubnames_begin1:
	.short	2                       # DWARF Version
	.long	.L.debug_info_begin1    # Offset of Compilation Unit Info
.Lset47 = .L.debug_info_end1-.L.debug_info_begin1 # Compilation Unit Length
	.long	.Lset47
	.long	38                      # DIE offset
	.asciz	"store_kernel"          # External Name
	.long	139                     # DIE offset
	.asciz	"test_stores"           # External Name
	.long	0                       # End Mark
.Lpubnames_end1:
	.section	.debug_pubtypes,"",@progbits
.Lset48 = .Lpubtypes_end2-.Lpubtypes_begin2 # Length of Public Types Info
	.long	.Lset48
.Lpubtypes_begin2:
	.short	2                       # DWARF Version
	.long	.L.debug_info_begin2    # Offset of Compilation Unit Info
.Lset49 = .L.debug_info_end2-.L.debug_info_begin2 # Compilation Unit Length
	.long	.Lset49
	.long	56                      # DIE offset
	.asciz	"int"                   # External Name
	.long	0                       # End Mark
.Lpubtypes_end2:
.Lset50 = .Lpubtypes_end0-.Lpubtypes_begin0 # Length of Public Types Info
	.long	.Lset50
.Lpubtypes_begin0:
	.short	2                       # DWARF Version
	.long	.L.debug_info_begin0    # Offset of Compilation Unit Info
.Lset51 = .L.debug_info_end0-.L.debug_info_begin0 # Compilation Unit Length
	.long	.Lset51
	.long	56                      # DIE offset
	.asciz	"int"                   # External Name
	.long	0                       # End Mark
.Lpubtypes_end0:
.Lset52 = .Lpubtypes_end3-.Lpubtypes_begin3 # Length of Public Types Info
	.long	.Lset52
.Lpubtypes_begin3:
	.short	2                       # DWARF Version
	.long	.L.debug_info_begin3    # Offset of Compilation Unit Info
.Lset53 = .L.debug_info_end3-.L.debug_info_begin3 # Compilation Unit Length
	.long	.Lset53
	.long	514                     # DIE offset
	.asciz	"long unsigned int"     # External Name
	.long	521                     # DIE offset
	.asciz	"unsigned int"          # External Name
	.long	56                      # DIE offset
	.asciz	"int"                   # External Name
	.long	503                     # DIE offset
	.asciz	"size_t"                # External Name
	.long	0                       # End Mark
.Lpubtypes_end3:
.Lset54 = .Lpubtypes_end1-.Lpubtypes_begin1 # Length of Public Types Info
	.long	.Lset54
.Lpubtypes_begin1:
	.short	2                       # DWARF Version
	.long	.L.debug_info_begin1    # Offset of Compilation Unit Info
.Lset55 = .L.debug_info_end1-.L.debug_info_begin1 # Compilation Unit Length
	.long	.Lset55
	.long	56                      # DIE offset
	.asciz	"int"                   # External Name
	.long	0                       # End Mark
.Lpubtypes_end1:

	.ident	"clang version 3.4.2 (tags/RELEASE_34/dot2-final)"
	.ident	"clang version 3.4.2 (tags/RELEASE_34/dot2-final)"
	.ident	"clang version 3.4.2 (tags/RELEASE_34/dot2-final)"
	.ident	"clang version 3.4.2 (tags/RELEASE_34/dot2-final)"
	.ident	"clang version 3.4.2 (tags/RELEASE_34/dot2-final)"
	.section	".note.GNU-stack","",@progbits
