#ifndef TYPE
#define TYPE int
#endif

void test_kernel(TYPE* store_vals, TYPE* store_loc, int num_vals);
int test_stores_plus(TYPE* store_vals, TYPE* store_loc, int num_vals);
