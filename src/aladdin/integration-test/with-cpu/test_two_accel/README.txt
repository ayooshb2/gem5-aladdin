The two subdirectories "store_kernel" and "test_kernel" contain source files with a #if TRACE protected main function.

In order to create the trace for a *given* accelerator, change to the local directory of the accelerator.

For example, To build the store_kernel trace perfrom the following:

1. vim store_kernel.c and change the define for TRACE to 1 (turn on the main function for the accelerator)
2. make clean-trace (clean up old shit)
3. make dma-trace-binary (create accelerator binary with DMA_MODE=1)
4. make run-trace (run the binary and output the dynamic trace using LLVM tracer)
5. vim store_kernel.c and change the define for TRACE to 0 (turn off the main function for the accelerator - needed in future steps)

Once all accelerators are built and have output traces from the above steps, change back into test_two_accel to build the full trace and gem5 accelerator.

1. Make sure all of the local accelerator sources have their main functions turned off
2. make clean-trace (clean up trace shit)
3. make clean-gem5 (clean up gem5 accelerator binary)
4. make dma-trace-binary (generate full program binary with DMA_MODE=1)
5. make run-trace (run the full binary and output the dynamic trace using LLVM tracer)
6. make gem5-accel (create the binary for use with gem5 having HW acceleration turned on)
7. sh run.sh (calls gem5 with aladdin_se.py and the gem5.cfg and runs the full program in gem5 with accelerator invokations)

To add a new accelerator:

1. create a subdir for the accelerator
2. copy the Makefile from another accelerator subdir - you'll need to modify this for the new accelerator
	2a. change the accel_name, sources, and workload
3. create a accel_name.cfg for the accelerator which specifies which loops to unroll/pipeline, how arrays should be partitioned, etc.
	3a. can copy this from another accelerator subdir, but must be modified to match the functions in the new accelerator's source
4. protect the main function for the new accelerator with a define to save time. The main function must be turned on to create the trace
	for this accelerator, but must be turned off for the full program compilation
5. create a trace for the new accelerator
	5a. enable the new accelerator's main function
	5b. make clean-trace (if this is not the first build)
	5c. make dma-trace-binary
	5d. make run-trace 
	5e. disable the new accelerator's main function
6. add the new accelerator with its name, dynamic trace location, configuration file, etc into the gem5.cfg file for the full program
7. include a header with access to the new accelerator in the full program and invoke the accelerator normally (map arrays, invokeandblock, etc)
	6a. see the examples of how to protect with GEM5_HARNESS (map arrays and invoke vs normal function call)
8. build the full program as described above
9. run full program and accelerators with sh run.sh
