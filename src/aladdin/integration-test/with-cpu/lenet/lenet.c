/* Test stores performed in the kernel.
 *
 * The values stored should be able to be loaded by the CPU after the kernel is
 * finished.
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include "aladdin_sys_connection.h"
#include "aladdin_sys_constants.h"
#include "lenet.h"


#ifdef DMA_MODE
#include "gem5/dma_interface.h"
#endif

#define CACHELINE_SIZE 64

static inline void identity(TYPE *a, unsigned len) {
}

static inline void relu(TYPE *a, unsigned len) {
    for (int i = 0; i < len; i++)
        a[i] = a[i] > 0.0 ? a[i] : 0.0;
}

static inline void leaky_relu(TYPE *a, unsigned len) {
    for (int i = 0; i < len; i++)
        a[i] = a[i] > 0.0 ? a[i] : 0.01 * a[i];
}

static inline void elu(TYPE *a, unsigned len) {
    for (int i = 0; i < len; i++)
        a[i] = a[i] > 0.0 ? a[i] : 0.01 * (expf(a[i]) - 1.0);
}

static inline void sigmoid (TYPE *a, unsigned len) {
    for (int i = 0; i < len; i++)
        a[i] = 1.0 / (1.0 + expf(-a[i]));
}

static inline void hyper_tan(TYPE *a, unsigned len) {
    for (int i = 0; i < len; i++)
        a[i] = tanhf(a[i]);
}

static inline void arctan(TYPE *a, unsigned len) {
    for (int i = 0; i < len; i++)
        a[i] = atanf(a[i]);
}

static inline void soft_plus(TYPE *a, unsigned len) {
    for (int i = 0; i < len; i++)
        a[i] = logf(1.0 + expf(a[i]));
}

// 3 in to 5 out convolution with 5x5 kernel
void 
conv1(float *kernel, 
      float *input, 
      float *output) {

    for (int b = 0; b < BATCH_SIZE; b++) {
cv1:    for (int c_out = 0; c_out < 6; c_out++) {
            for (int y = 2; y < 30; y++) {
                for (int x = 2; x < 30; x++) {
                    // Inner loop
                    float acc = 0;
                    for (int c_in = 0; c_in < 3; c_in++) {
                        for (int off_y = -2; off_y <= 2; off_y++) {
                            for (int off_x = -2; off_x <= 2; off_x++) {
                                acc += input[b*BATCH_SIZE + c_in*1024 + (y+off_y)*32 + x+off_x] *
                                       kernel[((c_out*6 + c_in)*3 + off_y+2)*5 + off_x+2];
                            }
                        }
                    }
                    output[b*BATCH_SIZE + c_out*784 + (y-2)*28 + x-2] = acc;
                }
            }
        }
    }
}

// 6 in to 16 out convolution with 5x5 kernel
void 
conv2(float *kernel, 
      float *input,
      float *output) {

    for (int b = 0; b < BATCH_SIZE; b++) {
cv2:    for (int c_out = 0; c_out < 16; c_out++) {
            for (int y = 2; y < 12; y++) {
                for (int x = 2; x < 12; x++) {
                    // Inner loop
                    float acc = 0;
                    for (int c_in = 0; c_in < 6; c_in++) {
                        for (int off_y = -2; off_y <= 2; off_y++) {
                            for (int off_x = -2; off_x <= 2; off_x++) {
                                acc += input[b*BATCH_SIZE + c_in*196 + (y+off_y)*14 + x+off_x] *
                                       kernel[((c_out*16 + c_in)*6 + off_y+2)*5 + off_x+2];
                            }
                        }
                    }
                    output[b*BATCH_SIZE + c_out*100 + (y-2)*10 + x-2] = acc;
                }
            }
        }
    }
}

// Max pool kernel 2 stride 2
void 
pool1(float *input,
      float *output) {

    for (int b = 0; b < BATCH_SIZE; b++) {
pl1:    for (int c = 0; c < 6; c++) {
            for (int y = 0; y < 14; y++) {
                for (int x = 0; x < 14; x++) {
                    float top_max = fmaxf(input[b*BATCH_SIZE + c*784 + (2*y)*28 + 2*x],
                                          input[b*BATCH_SIZE + c*784 + (2*y)*28 + 2*x+1]);
                    float bot_max = fmaxf(input[b*BATCH_SIZE + c*784 + (2*y+1)*28 + 2*x],
                                          input[b*BATCH_SIZE + c*784 + (2*y+1)*28 + 2*x+1]);
                    output[b*BATCH_SIZE + c*196 + y*14 + x] = fmaxf(top_max, bot_max);
                }
            }
        }
    }
}

// Max pool kernel 2 stride 2
void 
pool2(float *input,
      float *output) {

    for (int b = 0; b < BATCH_SIZE; b++) {
pl2:    for (int c = 0; c < 16; c++) {
            for (int y = 0; y < 5; y++) {
                for (int x = 0; x < 5; x++) {
                    float top_max = fmaxf(input[b*BATCH_SIZE + c*100 + (2*y)*10 + 2*x],
                                          input[b*BATCH_SIZE + c*100 + (2*y)*10 + 2*x+1]);
                    float bot_max = fmaxf(input[b*BATCH_SIZE + c*100 + (2*y+1)*10 + 2*x],
                                          input[b*BATCH_SIZE + c*100 + (2*y+1)*10 + 2*x+1]);
                    output[b*BATCH_SIZE + c*25 + y*5 + x] = fmaxf(top_max, bot_max);
                }
            }
        }
    }
}

// fully connected
void 
fc1(float *input,
    float *weights,
    float *output) {

    for (int b = 0; b < BATCH_SIZE; b++) {
fu1:    for (int i = 0; i < 120; i++) {
            float acc = 0;
            for (int j = 0; j < 16*5*5; j++) {
                acc += weights[i*120 + j] * input[b*BATCH_SIZE + j];
            }
            output[b*BATCH_SIZE + i] = acc;
        }
    }
}

void 
fc2(float *input,
    float *weights,
    float *output) {

    for (int b = 0; b < BATCH_SIZE; b++) {
fu2:    for (int i = 0; i < 84; i++) {
            float acc = 0;
            for (int j = 0; j < 120; j++) {
                acc += weights[i*84 + j] * input[b*BATCH_SIZE + j];
            }
            output[b*BATCH_SIZE + i] = acc;
        }
    }
}

void
fc3(float *input,
    float *weights,
    float *output) {

    for (int b = 0; b < BATCH_SIZE; b++) {
fu3:    for (int i = 0; i < 10; i++) {
            float acc = 0;
            for (int j = 0; j < 84; j++) {
                acc += weights[i*10 + j] * input[b*BATCH_SIZE + j];
            }
            output[b*BATCH_SIZE + i] = acc;
        }
    }
}


void
s1(float *input, float* kern1, float* out1) {
#if RUN_S1 && defined DMA_MODE
    dmaLoad(input, input, SIZE_I  * sizeof(TYPE));
    dmaLoad(kern1, kern1, SIZE_K1 * sizeof(TYPE));
#endif
    conv1(kern1, input, out1);
#if RUN_S1 && defined DMA_MODE
    dmaStore(out1, out1, SIZE_O1 * sizeof(TYPE));
#endif
}


void
s2(float *kern2, float* out1, float* out3) {
#if RUN_S2 && defined DMA_MODE
    dmaLoad(kern2, kern2, SIZE_K1 * sizeof(TYPE));
    dmaLoad(out1,  out1,  SIZE_O1 * sizeof(TYPE));
#endif
    float out2[SIZE_O2];
    pool1(out1, out2);
    conv2(kern2, out2, out3);
#if RUN_S2 && defined DMA_MODE
    dmaStore(out3, out3, SIZE_O3 * sizeof(TYPE));
#endif
}


void
s3(float *out3, float* weight1, float* out5) {
#if RUN_S3 && defined DMA_MODE
    dmaLoad(out3,    out3,    SIZE_O3 * sizeof(TYPE));
    dmaLoad(weight1, weight1, SIZE_W1 * sizeof(TYPE));
#endif
    float out4[SIZE_O4];
    pool2(out3, out4);
    fc1(out4, weight1, out5);
#if RUN_S3 && defined DMA_MODE
    dmaStore(out5, out5, SIZE_O5 * sizeof(TYPE));
#endif
}


void
s4(float *out5, float* weight2, float* out6) {
#if RUN_S4 && defined DMA_MODE
    dmaLoad(out5,    out5,    SIZE_O5 * sizeof(TYPE));
    dmaLoad(weight2, weight2, SIZE_W2 * sizeof(TYPE));
#endif
    fc2(out5, weight2, out6);
#if RUN_S4 && defined DMA_MODE
    dmaStore(out6, out6, SIZE_O6 * sizeof(TYPE));
#endif
}


void
s5(float *out6, float* weight3, float* output) {
#if RUN_S5 && defined DMA_MODE
    dmaLoad(out6,    out6,    SIZE_O6 * sizeof(TYPE));
    dmaLoad(weight3, weight3, SIZE_W3 * sizeof(TYPE));
#endif
    fc3(out6, weight3, output);
#if RUN_S5 && defined DMA_MODE
    dmaStore(output, output, SIZE_O * sizeof(TYPE));
#endif
}


void
big(float *input, float *kern1, float *kern2,
    float *weight1, float *weight2, float *weight3,
    float *output) {


#if RUN_BIG
    float out1[SIZE_O1];
    float out3[SIZE_O3];
    float out5[SIZE_O5];
    float out6[SIZE_O6];
#ifdef DMA_MODE
    dmaLoad(input,   input,   SIZE_I  * sizeof(TYPE));
    dmaLoad(kern1,   kern1,   SIZE_K1 * sizeof(TYPE));
    dmaLoad(kern2,   kern2,   SIZE_K2 * sizeof(TYPE));
    dmaLoad(weight1, weight1, SIZE_W1 * sizeof(TYPE));
    dmaLoad(weight2, weight2, SIZE_W2 * sizeof(TYPE));
    dmaLoad(weight3, weight3, SIZE_W3 * sizeof(TYPE));
#endif
#else
    int err;
    TYPE *out1, *out3, *out5, *out6;
    err = posix_memalign(
        (void **)&out1, CACHELINE_SIZE, sizeof(TYPE)*SIZE_O1);
    assert(err == 0 && "Failed to allocate memory!");

    err = posix_memalign(
        (void **)&out3, CACHELINE_SIZE, sizeof(TYPE)*SIZE_O3);
    assert(err == 0 && "Failed to allocate memory!");

    err = posix_memalign(
        (void **)&out5, CACHELINE_SIZE, sizeof(TYPE)*SIZE_O5);
    assert(err == 0 && "Failed to allocate memory!");

    err = posix_memalign(
        (void **)&out6, CACHELINE_SIZE, sizeof(TYPE)*SIZE_O6);
    assert(err == 0 && "Failed to allocate memory!");
#endif

#if !RUN_BIG && RUN_S1 && defined GEM5_HARNESS
    mapArrayToAccelerator(
        ID_S1, "input", &(input[0]), SIZE_I  * sizeof(TYPE));
    mapArrayToAccelerator(
        ID_S1, "kern1", &(kern1[0]), SIZE_K1 * sizeof(TYPE));
    mapArrayToAccelerator(
        ID_S1, "out1",  &(out1[0]),  SIZE_O1 * sizeof(TYPE));
    fprintf(stdout, "Invoking S1 accelerator!\n");
    invokeAcceleratorAndBlock(ID_S1);
    fprintf(stdout, "S1 Accelerator finished!\n");
#else
    s1(input, kern1, out1);
#endif

    ACTIVATE(out1, BATCH_SIZE*6*28*28);

#if !RUN_BIG && RUN_S2 && defined GEM5_HARNESS
    mapArrayToAccelerator(
        ID_S2, "out1",  &(out1[0]),  SIZE_O1 * sizeof(TYPE));
    mapArrayToAccelerator(
        ID_S2, "kern2", &(kern2[0]), SIZE_K2 * sizeof(TYPE));
    mapArrayToAccelerator(
        ID_S2, "out3",  &(out3[0]),  SIZE_O3 * sizeof(TYPE));
    fprintf(stdout, "Invoking S2 accelerator!\n");
    invokeAcceleratorAndBlock(ID_S2);
    fprintf(stdout, "S2 Accelerator finished!\n");
#else
    s2(input, kern2, out3);
#endif

    ACTIVATE(out3, BATCH_SIZE*16*10*10);

#if !RUN_BIG && RUN_S3 && defined GEM5_HARNESS
    mapArrayToAccelerator(
        ID_S3, "out3",    &(out3[0]),    SIZE_O3 * sizeof(TYPE));
    mapArrayToAccelerator(
        ID_S3, "weight1", &(weight1[0]), SIZE_W1 * sizeof(TYPE));
    mapArrayToAccelerator(
        ID_S3, "out5",    &(out5[0]),    SIZE_O5 * sizeof(TYPE));
    fprintf(stdout, "Invoking S3 accelerator!\n");
    invokeAcceleratorAndBlock(ID_S3);
    fprintf(stdout, "S3 Accelerator finished!\n");
#else
    s3(out3, weight1, out5);
#endif

    ACTIVATE(out5, BATCH_SIZE*120);

#if !RUN_BIG && RUN_S4 && defined GEM5_HARNESS
    mapArrayToAccelerator(
        ID_S4, "out5",    &(out5[0]),    SIZE_O5 * sizeof(TYPE));
    mapArrayToAccelerator(
        ID_S4, "weight2", &(weight2[0]), SIZE_W2 * sizeof(TYPE));
    mapArrayToAccelerator(
        ID_S4, "out6",    &(out6[0]),    SIZE_O6 * sizeof(TYPE));
    fprintf(stdout, "Invoking S4 accelerator!\n");
    invokeAcceleratorAndBlock(ID_S4);
    fprintf(stdout, "S4 Accelerator finished!\n");
#else
    s4(out5, weight2, out6);
#endif

    ACTIVATE(out6, BATCH_SIZE*84);

#if !RUN_BIG && RUN_S4 && defined GEM5_HARNESS
    mapArrayToAccelerator(
        ID_S5, "out6",    &(out6[0]),    SIZE_O6 * sizeof(TYPE));
    mapArrayToAccelerator(
        ID_S5, "weight3", &(weight3[0]), SIZE_W3 * sizeof(TYPE));
    mapArrayToAccelerator(
        ID_S5, "output",  &(output[0]),  SIZE_O  * sizeof(TYPE));
    fprintf(stdout, "Invoking S5 accelerator!\n");
    invokeAcceleratorAndBlock(ID_S5);
    fprintf(stdout, "S5 Accelerator finished!\n");
#else
    s5(out6, weight3, output);
#endif

#if RUN_BIG
#ifdef DMA_MODE
    dmaStore(output, output, SIZE_O * sizeof(TYPE));
#endif
#else
# if FREE
    free(out1);
    free(out3);
    free(out5);
    free(out6);
#endif
#endif
}


void init(TYPE *input, TYPE *kern1, TYPE *kern2,
	  TYPE *weight1, TYPE *weight2, TYPE *weight3) { 
    for (int i = 0; i < SIZE_I ; i++)
        input[i]   = (float)rand() / 10000000000 - 1.0;
    for (int i = 0; i < SIZE_K1; i++)
        kern1[i]   = (float)rand() / 10000000000 - 1.0;
    for (int i = 0; i < SIZE_K2; i++)
        kern2[i]   = (float)rand() / 10000000000 - 1.0;
    for (int i = 0; i < SIZE_W1; i++)
        weight1[i] = (float)rand() / 10000000000 - 1.0;
    for (int i = 0; i < SIZE_W2; i++)
        weight2[i] = (float)rand() / 10000000000 - 1.0;
    for (int i = 0; i < SIZE_W3; i++)
        weight3[i] = (float)rand() / 10000000000 - 1.0;
}


TYPE
run(void) {
    int err;
    TYPE *input, *kern1, *kern2, *weight1, *weight2, *weight3, *output;

    err = posix_memalign(
        (void **)&input, CACHELINE_SIZE, sizeof(TYPE)*SIZE_I);
    assert(err == 0 && "Failed to allocate memory!");
    
    err = posix_memalign(
        (void **)&kern1, CACHELINE_SIZE, sizeof(TYPE)*SIZE_K1);
    assert(err == 0 && "Failed to allocate memory!");
    
    err = posix_memalign(
        (void **)&kern2, CACHELINE_SIZE, sizeof(TYPE)*SIZE_K2);
    assert(err == 0 && "Failed to allocate memory!");
    
    err = posix_memalign(
        (void **)&weight1, CACHELINE_SIZE, sizeof(TYPE)*SIZE_W1);
    assert(err == 0 && "Failed to allocate memory!");
    
    err = posix_memalign(
        (void **)&weight2, CACHELINE_SIZE, sizeof(TYPE)*SIZE_W2);
    assert(err == 0 && "Failed to allocate memory!");
    
    err = posix_memalign(
        (void **)&weight3, CACHELINE_SIZE, sizeof(TYPE)*SIZE_W3);
    assert(err == 0 && "Failed to allocate memory!");

    err = posix_memalign(
        (void **)&output, CACHELINE_SIZE, sizeof(TYPE)*SIZE_O);
    assert(err == 0 && "Failed to allocate memory!");
    
    init(input, kern1, kern2, weight1, weight2, weight3);

#if RUN_BIG && defined GEM5_HARNESS
    mapArrayToAccelerator(
        ID_BIG, "input",   &(input[0]),   SIZE_I  * sizeof(TYPE));
    mapArrayToAccelerator(
        ID_BIG, "kern1",   &(kern1[0]),   SIZE_K1 * sizeof(TYPE));
    mapArrayToAccelerator(
        ID_BIG, "kern2",   &(kern2[0]),   SIZE_K2 * sizeof(TYPE));
    mapArrayToAccelerator(
        ID_BIG, "weight1", &(weight1[0]), SIZE_W1 * sizeof(TYPE));
    mapArrayToAccelerator(
        ID_BIG, "weight2", &(weight2[0]), SIZE_W2 * sizeof(TYPE));
    mapArrayToAccelerator(
        ID_BIG, "weight3", &(weight3[0]), SIZE_W3 * sizeof(TYPE));
    mapArrayToAccelerator(
        ID_BIG, "output",  &(output[0]),  SIZE_O  * sizeof(TYPE));

    fprintf(stdout, "Invoking BIG accelerator!\n");
    invokeAcceleratorAndBlock(ID_BIG);
    fprintf(stdout, "BIG Accelerator finished!\n");
#else

    big(input, kern1, kern2, weight1, weight2, weight3, output);

#endif
    
    float acc = 0;
    for (int i = 0; i < BATCH_SIZE * 10; i++) {
        acc += output[i];
    }
#if FREE
    free(input);
    free(kern1);
    free(kern2);
    free(weight1);
    free(weight2);
    free(weight3);
    free(output);
#endif

    return acc;
}


int main() {
    printf("dear god...\n");

    srand(0);

    printf("wow! it's a %f\n", run());

    return 0;
}
