#define BATCH_SIZE 1
/*
#define SIZE_I  BATCH_SIZE*3*32*32
#define SIZE_K1 6*3*5*5
#define SIZE_K2 16*6*5*5
#define SIZE_W1 120*16*5*5
#define SIZE_W2 84*120
#define SIZE_W3 10*84
#define SIZE_O  BATCH_SIZE*10
*/
#define SIZE_I  8192
#define SIZE_K1 1024
#define SIZE_K2 8192
#define SIZE_W1 48128
#define SIZE_W2 10240
#define SIZE_W3 1024
#define SIZE_O  1024

/*
#define SIZE_O1 BATCH_SIZE*6*28*28
#define SIZE_O2 BATCH_SIZE*6*14*14
#define SIZE_O3 BATCH_SIZE*16*10*10
#define SIZE_O4 BATCH_SIZE*16*5*5
#define SIZE_O5 BATCH_SIZE*120
#define SIZE_O6 BATCH_SIZE*84
*/
#define SIZE_O1 8192
#define SIZE_O2 2048
#define SIZE_O3 2048
#define SIZE_O4 1024
#define SIZE_O5 1024
#define SIZE_O6 1024

#define RUN_BIG 1
#define RUN_S1  0
#define RUN_S2  0
#define RUN_S3  0
#define RUN_S4  0
#define RUN_S5  0

#define ACTIVATE relu
// identity relu leaky_relu elu sigmoid hyper_tan arctan soft_plus

#define TYPE float // Not used consistently, leave it as float
#define FREE 0 // For some arcane reason free tends to panic

void 
conv1(float *kernel, 
      float *input,
      float *output);

void 
conv2(float *kernel, 
      float *input, 
      float *output);

void 
pool1(float *input,
      float *output);

void 
pool2(float *input,
      float *output);

void 
fc1(float *input,
    float *weights,
    float *output);

void 
fc2(float *input,
    float *weights,
    float *output);

void
fc3(float *input,
    float *weights,
    float *output);

void
s1(float *input, float* kern1, float* out1);

void
s2(float *kern2, float* out1, float* out3);

void
s3(float *out3, float* weight1, float* out5);

void
s4(float *out5, float* weight2, float* out6);

void
s5(float *out6, float* weight3, float* output);

void
init(TYPE *, TYPE *, TYPE *, TYPE *, TYPE *, TYPE *);

void
big(float *input, float *kern1, float *kern2,
    float *weight1, float *weight2, float *weight3,
    float *output);

float
run(void);

