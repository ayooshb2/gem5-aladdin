/* Test stores performed in the kernel.
 *
 * The values stored should be able to be loaded by the CPU after the kernel is
 * finished.
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include "aladdin_sys_connection.h"
#include "aladdin_sys_constants.h"

#define TYPE float
#define ACTIVATE soft_plus

#ifdef DMA_MODE
#include "gem5/dma_interface.h"
#endif

#define CACHELINE_SIZE 64

#define RUN_BIG 0
#define RUN_S1  1
#define RUN_S2  1
#define RUN_S3  1

int test_stores(TYPE* store_vals, TYPE* store_loc, int num_vals) {
  int num_failures = 0;
  for (int i = 0; i < num_vals; i++) {
    if (store_loc[i] != store_vals[i] + 1) {
      fprintf(stdout, "FAILED: store_loc[%f] = %f, should be %f\n",
                               i, store_loc[i], store_vals[i] + 1);
      num_failures++;
    }
  }
  return num_failures;
}

static inline float identity(float x) {
  return x;
}

static inline float relu(float x) {
  return x > 0.0 ? x : 0.0;
}

static inline float sigmoid(float x) {
  return 1.0 / (1.0 + expf(-x));
}

static inline float hyper_tan(float x) {
  return tanhf(x);
}

static inline float arctan(float x) {
  return atanf(x);
}

static inline float leaky_relu(float x) {
  return x > 0.0 ? x : 0.01 * x;
}

static inline float elu(float x) {
  return x > 0.0 ? x : 0.1 * (expf(x) - 1.0);
}

static inline float soft_plus(float x) {
  return logf(1.0 + expf(x));
}

void s1(TYPE* store_vals, TYPE* store_loc, int num_vals) {
#if RUN_S1
#ifdef DMA_MODE
  dmaLoad(store_vals, store_vals, num_vals * sizeof(TYPE));
  dmaLoad(store_loc, store_loc, num_vals * sizeof(TYPE));
#endif
#endif

  loop: for (int i = 0; i < num_vals; i++)
    store_loc[i] = store_vals[i] + 1;

#if RUN_S1
#ifdef DMA_MODE
  dmaStore(store_loc, store_loc, num_vals * sizeof(TYPE));
#endif
#endif
}

// Read values from store_vals and copy them into store_loc.
void big(TYPE* store_vals, TYPE* store_loc, int num_vals) {
#if RUN_BIG
#ifdef DMA_MODE
  dmaLoad(store_vals, store_vals, num_vals * sizeof(TYPE));
  dmaLoad(store_loc, store_loc, num_vals * sizeof(TYPE));
#endif
#endif

#if RUN_S1
#ifdef GEM5_HARNESS
  mapArrayToAccelerator(
      ID_S1, "store_vals", &(store_vals[0]), num_vals * sizeof(int));
  mapArrayToAccelerator(
      ID_S1, "store_loc", &(store_loc[0]), num_vals * sizeof(int));

  fprintf(stdout, "Invoking accelerator!\n");
  invokeAcceleratorAndBlock(ID_S1);
  fprintf(stdout, "Accelerator finished!\n");
#else
  s1(store_vals, store_loc, num_vals);
#endif
#else
  s1(store_vals, store_loc, num_vals);
#endif


// ACTIVATE
  for (int i = 0; i < num_vals; i++)
    store_loc[i] = ACTIVATE(store_loc[i]);

#if RUN_S1
#ifdef GEM5_HARNESS
//  mapArrayToAccelerator(
//      ID_S1, "store_vals", &(store_vals[0]), num_vals * sizeof(int));
//  mapArrayToAccelerator(
//      ID_S1, "store_loc", &(store_loc[0]), num_vals * sizeof(int));

  fprintf(stdout, "Invoking accelerator!\n");
  invokeAcceleratorAndBlock(ID_S1);
  fprintf(stdout, "Accelerator finished!\n");
#else
  s1(store_vals, store_loc, num_vals);
#endif
#else
  s1(store_vals, store_loc, num_vals);
#endif

// ACTIVATE
  for (int i = 0; i < num_vals; i++)
    store_loc[i] = ACTIVATE(store_loc[i]);

#if RUN_S1
#ifdef GEM5_HARNESS
//  mapArrayToAccelerator(
//      ID_S1, "store_vals", &(store_vals[0]), num_vals * sizeof(int));
//  mapArrayToAccelerator(
//      ID_S1, "store_loc", &(store_loc[0]), num_vals * sizeof(int));

  fprintf(stdout, "Invoking accelerator!\n");
  invokeAcceleratorAndBlock(ID_S1);
  fprintf(stdout, "Accelerator finished!\n");
#else
  s1(store_vals, store_loc, num_vals);
#endif
#else
  s1(store_vals, store_loc, num_vals);
#endif

#if RUN_BIG
#ifdef DMA_MODE
  dmaStore(store_loc, store_loc, num_vals * sizeof(TYPE));
#endif
#endif
}

int main() {
  const int num_vals = 1024;
  TYPE *store_vals, *store_loc;
  int err = posix_memalign(
      (void**)&store_vals, CACHELINE_SIZE, sizeof(TYPE) * num_vals);
  assert(err == 0 && "Failed to allocate memory!");
  err = posix_memalign(
      (void**)&store_loc, CACHELINE_SIZE, sizeof(TYPE) * num_vals);
  assert(err == 0 && "Failed to allocate memory!");
  for (int i = 0; i < num_vals; i++) {
    store_vals[i] = i;
    store_loc[i] = -1;
  }

#if RUN_BIG
    fprintf(stdout, "running BIG.");
#ifdef GEM5_HARNESS
  mapArrayToAccelerator(
      ID_BIG, "store_vals", &(store_vals[0]), num_vals * sizeof(int));
  mapArrayToAccelerator(
      ID_BIG, "store_loc", &(store_loc[0]), num_vals * sizeof(int));

  fprintf(stdout, "Invoking accelerator!\n");
  invokeAcceleratorAndBlock(ID_BIG);
  fprintf(stdout, "Accelerator finished!\n");
#else
  big(store_vals, store_loc, num_vals);
#endif
#else
  big(store_vals, store_loc, num_vals);
#endif

  int num_failures = test_stores(store_vals, store_loc, num_vals);
  if (num_failures != 0) {
    fprintf(stdout, "Test failed with %d errors.", num_failures);
    return -1;
  }
  fprintf(stdout, "Test passed!\n");
  fprintf(stdout, "store_loc[%f] = %f should be %f\n",
                               0, store_loc[0], store_vals[0]);
  return 0;
}
