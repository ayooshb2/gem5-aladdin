	.file	"full.llvm"
	.section	.debug_info,"",@progbits
.Lsection_info:
	.section	.debug_abbrev,"",@progbits
.Lsection_abbrev:
	.section	.debug_aranges,"",@progbits
	.section	.debug_macinfo,"",@progbits
	.section	.debug_line,"",@progbits
.Lsection_line:
	.section	.debug_loc,"",@progbits
	.section	.debug_pubnames,"",@progbits
	.section	.debug_pubtypes,"",@progbits
	.section	.debug_str,"MS",@progbits,1
.Linfo_string:
	.section	.debug_ranges,"",@progbits
.Ldebug_range:
	.section	.debug_loc,"",@progbits
.Lsection_debug_loc:
	.text
.Ltext_begin:
	.data
	.file	1 "test_dma_load_store.c"
	.file	2 "/workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c"
	.section	.rodata.cst4,"aM",@progbits,4
	.align	4
.LCPI0_0:
	.long	1065353216              # float 1
	.section	.rodata.cst8,"aM",@progbits,8
	.align	8
.LCPI0_1:
	.quad	4607182418800017408     # double 1
	.text
	.globl	test_stores
	.align	16, 0x90
	.type	test_stores,@function
test_stores:                            # @test_stores
	.cfi_startproc
.Lfunc_begin0:
	.loc	1 27 0                  # test_dma_load_store.c:27:0
# BB#0:
	pushq	%rbp
.Ltmp7:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Ltmp8:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Ltmp9:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Ltmp10:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Ltmp11:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Ltmp12:
	.cfi_def_cfa_offset 56
	subq	$1432, %rsp             # imm = 0x598
.Ltmp13:
	.cfi_def_cfa_offset 1488
.Ltmp14:
	.cfi_offset %rbx, -56
.Ltmp15:
	.cfi_offset %r12, -48
.Ltmp16:
	.cfi_offset %r13, -40
.Ltmp17:
	.cfi_offset %r14, -32
.Ltmp18:
	.cfi_offset %r15, -24
.Ltmp19:
	.cfi_offset %rbp, -16
	movabsq	$0, %rax
	movl	$0, %ecx
	movabsq	$1, %r8
	leaq	.L.str9, %r9
	leaq	.L.str11, %r10
	movabsq	$2, %r11
	leaq	.L.str15, %rbx
	movabsq	$3, %r14
	leaq	.L.str14, %r15
	movabsq	$29, %r12
	leaq	.L.str1, %r13
	leaq	.L.str8, %rbp
	movq	%rax, 1424(%rsp)        # 8-byte Spill
	leaq	.L.str13, %rax
	movl	%ecx, 1420(%rsp)        # 4-byte Spill
	movl	$1, %ecx
	movq	%rax, 1408(%rsp)        # 8-byte Spill
	movabsq	$19134, %rax            # imm = 0x4ABE
	movq	%rax, 1400(%rsp)        # 8-byte Spill
	movabsq	$32, %rax
	movq	%rax, 1392(%rsp)        # 8-byte Spill
	leaq	.L.str12, %rax
	movq	%rax, 1384(%rsp)        # 8-byte Spill
	leaq	.L.str10, %rax
	movq	%rax, 1376(%rsp)        # 8-byte Spill
	movabsq	$46, %rax
	movq	%rax, 1368(%rsp)        # 8-byte Spill
	movabsq	$24601, %rax            # imm = 0x6019
	movq	%rax, 1360(%rsp)        # 8-byte Spill
	movabsq	$64, %rax
	movq	%rax, 1352(%rsp)        # 8-byte Spill
	leaq	.L.str22, %rax
	movq	%rax, 1344(%rsp)        # 8-byte Spill
	leaq	.L.str25, %rax
	.loc	1 27 0 prologue_end     # test_dma_load_store.c:27:0
.Ltmp20:
	movq	%rax, 1336(%rsp)        # 8-byte Spill
	movq	1360(%rsp), %rax        # 8-byte Reload
	movq	%rdi, 1328(%rsp)        # 8-byte Spill
	movq	%rax, %rdi
	movq	1352(%rsp), %rax        # 8-byte Reload
	movq	%rsi, 1320(%rsp)        # 8-byte Spill
	movq	%rax, %rsi
	movq	1328(%rsp), %rax        # 8-byte Reload
	movl	%edx, 1316(%rsp)        # 4-byte Spill
	movq	%rax, %rdx
	movl	%ecx, 1312(%rsp)        # 4-byte Spill
	movq	%r8, %rcx
	movq	1336(%rsp), %rax        # 8-byte Reload
	movq	%r8, 1304(%rsp)         # 8-byte Spill
	movq	%rax, %r8
	movq	1424(%rsp), %rax        # 8-byte Reload
	movq	%r9, 1296(%rsp)         # 8-byte Spill
	movq	%rax, %r9
	movq	%r10, (%rsp)
	movq	%r13, 1288(%rsp)        # 8-byte Spill
	movq	%rbp, 1280(%rsp)        # 8-byte Spill
	movq	%r12, 1272(%rsp)        # 8-byte Spill
	movq	%r15, 1264(%rsp)        # 8-byte Spill
	movq	%r14, 1256(%rsp)        # 8-byte Spill
	movq	%rbx, 1248(%rsp)        # 8-byte Spill
	movq	%r10, 1240(%rsp)        # 8-byte Spill
	movq	%r11, 1232(%rsp)        # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	1360(%rsp), %rdi        # 8-byte Reload
	movq	1352(%rsp), %rsi        # 8-byte Reload
	movq	1320(%rsp), %rdx        # 8-byte Reload
	movq	1304(%rsp), %rcx        # 8-byte Reload
	movq	1344(%rsp), %r8         # 8-byte Reload
	movq	1424(%rsp), %r9         # 8-byte Reload
	movq	1240(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movl	1316(%rsp), %esi        # 4-byte Reload
	movl	%esi, %esi
	movl	%esi, %edx
	movq	1360(%rsp), %rdi        # 8-byte Reload
	movq	1392(%rsp), %rsi        # 8-byte Reload
	movq	1304(%rsp), %rcx        # 8-byte Reload
	movq	1384(%rsp), %r8         # 8-byte Reload
	movq	1424(%rsp), %r9         # 8-byte Reload
	movq	1240(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	#DEBUG_VALUE: test_stores:store_vals <- [RSP+1328]
	#DEBUG_VALUE: test_stores:store_loc <- [RSP+1320]
	#DEBUG_VALUE: test_stores:num_vals <- [RSP+1316]
.Ltmp21:
	#DEBUG_VALUE: test_stores:num_failures <- 0
	#DEBUG_VALUE: i <- 0
	.loc	1 29 0                  # test_dma_load_store.c:29:0
	movq	1272(%rsp), %rdi        # 8-byte Reload
	movq	1288(%rsp), %rsi        # 8-byte Reload
	movq	1280(%rsp), %rdx        # 8-byte Reload
	movq	1296(%rsp), %rcx        # 8-byte Reload
	movq	1368(%rsp), %r8         # 8-byte Reload
	movl	1420(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1232(%rsp), %rdi        # 8-byte Reload
	movq	1392(%rsp), %rsi        # 8-byte Reload
	movq	1424(%rsp), %rdx        # 8-byte Reload
	movq	1424(%rsp), %rcx        # 8-byte Reload
	movq	1376(%rsp), %r8         # 8-byte Reload
	movq	1424(%rsp), %r9         # 8-byte Reload
	movq	1240(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	1316(%rsp), %esi        # 4-byte Reload
	movl	%esi, %esi
	movl	%esi, %edx
	movq	1304(%rsp), %rdi        # 8-byte Reload
	movq	1392(%rsp), %rsi        # 8-byte Reload
	movq	1304(%rsp), %rcx        # 8-byte Reload
	movq	1384(%rsp), %r8         # 8-byte Reload
	movq	1424(%rsp), %r9         # 8-byte Reload
	movq	1240(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	1316(%rsp), %esi        # 4-byte Reload
	cmpl	$0, %esi
	setg	%al
	movb	%al, %cl
	andb	$1, %cl
	movzbl	%cl, %ecx
	movl	%ecx, %edx
	movq	1400(%rsp), %rdi        # 8-byte Reload
	movq	1304(%rsp), %rsi        # 8-byte Reload
	movq	1304(%rsp), %rcx        # 8-byte Reload
	movq	1296(%rsp), %r8         # 8-byte Reload
	movq	1424(%rsp), %r9         # 8-byte Reload
	movq	1240(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	movb	%al, 1231(%rsp)         # 1-byte Spill
	callq	trace_logger_log_int
	movq	1272(%rsp), %rdi        # 8-byte Reload
	movq	1288(%rsp), %rsi        # 8-byte Reload
	movq	1280(%rsp), %rdx        # 8-byte Reload
	movq	1408(%rsp), %rcx        # 8-byte Reload
	movq	1232(%rsp), %r8         # 8-byte Reload
	movl	1420(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1256(%rsp), %rdi        # 8-byte Reload
	movq	1424(%rsp), %rsi        # 8-byte Reload
	movq	1424(%rsp), %rdx        # 8-byte Reload
	movq	1304(%rsp), %rcx        # 8-byte Reload
	movq	1264(%rsp), %r8         # 8-byte Reload
	movq	1424(%rsp), %r9         # 8-byte Reload
	movq	1240(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	1232(%rsp), %rdi        # 8-byte Reload
	movq	1424(%rsp), %rsi        # 8-byte Reload
	movq	1424(%rsp), %rdx        # 8-byte Reload
	movq	1304(%rsp), %rcx        # 8-byte Reload
	movq	1248(%rsp), %r8         # 8-byte Reload
	movq	1424(%rsp), %r9         # 8-byte Reload
	movq	1240(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	1231(%rsp), %al         # 1-byte Reload
	andb	$1, %al
	movzbl	%al, %eax
	movl	%eax, %edx
	movq	1304(%rsp), %rdi        # 8-byte Reload
	movq	1304(%rsp), %rsi        # 8-byte Reload
	movq	1304(%rsp), %rcx        # 8-byte Reload
	movq	1296(%rsp), %r8         # 8-byte Reload
	movq	1424(%rsp), %r9         # 8-byte Reload
	movq	1240(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	1231(%rsp), %al         # 1-byte Reload
	testb	$1, %al
	movq	1424(%rsp), %rcx        # 8-byte Reload
	movl	1420(%rsp), %eax        # 4-byte Reload
	movl	%eax, 1224(%rsp)        # 4-byte Spill
	movq	%rcx, 1216(%rsp)        # 8-byte Spill
	jne	.LBB0_1
	jmp	.LBB0_4
.Ltmp22:
.LBB0_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	1224(%rsp), %eax        # 4-byte Reload
	movq	1216(%rsp), %rcx        # 8-byte Reload
	movabsq	$1, %rdx
	leaq	.L.str28, %rsi
	movabsq	$0, %rdi
	leaq	.L.str11, %r8
	movabsq	$2, %r9
	leaq	.L.str18, %r10
	movabsq	$3, %r11
	leaq	.L.str30, %rbx
	movabsq	$30, %r14
	leaq	.L.str1, %r15
	leaq	.L.str14, %r12
	leaq	.L.str29, %r13
	movl	$0, %ebp
	movl	%eax, 1212(%rsp)        # 4-byte Spill
	movl	$1, %eax
	movq	%rcx, 1200(%rsp)        # 8-byte Spill
	movabsq	$19134, %rcx            # imm = 0x4ABE
	movq	%rcx, 1192(%rsp)        # 8-byte Spill
	movabsq	$32, %rcx
	movq	%rcx, 1184(%rsp)        # 8-byte Spill
	leaq	.L.str23, %rcx
	movq	%rcx, 1176(%rsp)        # 8-byte Spill
	leaq	.L.str27, %rcx
	movq	%rcx, 1168(%rsp)        # 8-byte Spill
	movabsq	$47, %rcx
	vmovss	.LCPI0_0, %xmm0
	movq	%rcx, 1160(%rsp)        # 8-byte Spill
	leaq	.L.str26, %rcx
	vmovsd	.LCPI0_1, %xmm1
	movq	%rcx, 1152(%rsp)        # 8-byte Spill
	leaq	.L.str10, %rcx
	movq	%rcx, 1144(%rsp)        # 8-byte Spill
	movabsq	$9, %rcx
	movq	%rcx, 1136(%rsp)        # 8-byte Spill
	movabsq	$64, %rcx
	movq	%rcx, 1128(%rsp)        # 8-byte Spill
	leaq	.L.str24, %rcx
	movq	%rcx, 1120(%rsp)        # 8-byte Spill
	movabsq	$27, %rcx
	movq	%rcx, 1112(%rsp)        # 8-byte Spill
	leaq	.L.str25, %rcx
	movq	%rcx, 1104(%rsp)        # 8-byte Spill
	leaq	.L.str16, %rcx
	movq	%rcx, 1096(%rsp)        # 8-byte Spill
	movabsq	$29, %rcx
	movq	%rcx, 1088(%rsp)        # 8-byte Spill
	leaq	.L.str21, %rcx
	movq	%rcx, 1080(%rsp)        # 8-byte Spill
	leaq	.L.str22, %rcx
	movq	%rcx, 1072(%rsp)        # 8-byte Spill
	leaq	.L.str19, %rcx
	movq	%rcx, 1064(%rsp)        # 8-byte Spill
	leaq	.L.str20, %rcx
	movq	%rcx, 1056(%rsp)        # 8-byte Spill
	leaq	.L.str8, %rcx
	movq	%rcx, 1048(%rsp)        # 8-byte Spill
	movabsq	$-1, %rcx
	movq	%rcx, 1040(%rsp)        # 8-byte Spill
	movabsq	$48, %rcx
	movq	%rcx, 1032(%rsp)        # 8-byte Spill
	leaq	.L.str17, %rcx
	.loc	1 30 0                  # test_dma_load_store.c:30:0
.Ltmp23:
	movq	%rcx, 1024(%rsp)        # 8-byte Spill
	movq	1040(%rsp), %rcx        # 8-byte Reload
	movq	%rdi, 1016(%rsp)        # 8-byte Spill
	movq	%rcx, %rdi
	movq	%rsi, 1008(%rsp)        # 8-byte Spill
	movq	%r15, %rsi
	movq	%rdx, 1000(%rsp)        # 8-byte Spill
	movq	%r12, %rdx
	movq	1096(%rsp), %rcx        # 8-byte Reload
	movq	%r8, 992(%rsp)          # 8-byte Spill
	movq	1032(%rsp), %r8         # 8-byte Reload
	movq	%r9, 984(%rsp)          # 8-byte Spill
	movl	%ebp, %r9d
	movl	$1, (%rsp)
	movl	%eax, 980(%rsp)         # 4-byte Spill
	movq	%r13, 968(%rsp)         # 8-byte Spill
	movl	%ebp, 964(%rsp)         # 4-byte Spill
	vmovss	%xmm0, 960(%rsp)        # 4-byte Spill
	vmovsd	%xmm1, 952(%rsp)        # 8-byte Spill
	movq	%r12, 944(%rsp)         # 8-byte Spill
	movq	%r15, 936(%rsp)         # 8-byte Spill
	movq	%r10, 928(%rsp)         # 8-byte Spill
	movq	%r11, 920(%rsp)         # 8-byte Spill
	movq	%rbx, 912(%rsp)         # 8-byte Spill
	movq	%r14, 904(%rsp)         # 8-byte Spill
	callq	trace_logger_log0
	movq	984(%rsp), %rdi         # 8-byte Reload
	movq	1128(%rsp), %rsi        # 8-byte Reload
	movq	1016(%rsp), %rdx        # 8-byte Reload
	movq	1016(%rsp), %rcx        # 8-byte Reload
	movq	1144(%rsp), %r8         # 8-byte Reload
	movq	1000(%rsp), %r9         # 8-byte Reload
	movq	1048(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	1000(%rsp), %rdi        # 8-byte Reload
	movq	1128(%rsp), %rsi        # 8-byte Reload
	movq	1016(%rsp), %rdx        # 8-byte Reload
	movq	1000(%rsp), %rcx        # 8-byte Reload
	movq	1024(%rsp), %r8         # 8-byte Reload
	movq	1000(%rsp), %r9         # 8-byte Reload
	movq	928(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	1192(%rsp), %rdi        # 8-byte Reload
	movq	1128(%rsp), %rsi        # 8-byte Reload
	movq	1200(%rsp), %rdx        # 8-byte Reload
	movq	1000(%rsp), %rcx        # 8-byte Reload
	movq	1096(%rsp), %r8         # 8-byte Reload
	movq	1016(%rsp), %r9         # 8-byte Reload
	movq	928(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	1040(%rsp), %rdi        # 8-byte Reload
	movq	936(%rsp), %rsi         # 8-byte Reload
	movq	944(%rsp), %rdx         # 8-byte Reload
	movq	1064(%rsp), %rcx        # 8-byte Reload
	movq	1032(%rsp), %r8         # 8-byte Reload
	movl	964(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	984(%rsp), %rdi         # 8-byte Reload
	movq	1184(%rsp), %rsi        # 8-byte Reload
	movq	1016(%rsp), %rdx        # 8-byte Reload
	movq	1016(%rsp), %rcx        # 8-byte Reload
	movq	1144(%rsp), %r8         # 8-byte Reload
	movq	1000(%rsp), %r9         # 8-byte Reload
	movq	1048(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	1000(%rsp), %rdi        # 8-byte Reload
	movq	1184(%rsp), %rsi        # 8-byte Reload
	movq	1016(%rsp), %rdx        # 8-byte Reload
	movq	1000(%rsp), %rcx        # 8-byte Reload
	movq	1056(%rsp), %r8         # 8-byte Reload
	movq	1000(%rsp), %r9         # 8-byte Reload
	movq	928(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movl	1212(%rsp), %eax        # 4-byte Reload
	movl	%eax, %ebp
	movl	%ebp, %edx
	movq	1192(%rsp), %rdi        # 8-byte Reload
	movq	1184(%rsp), %rsi        # 8-byte Reload
	movq	1000(%rsp), %rcx        # 8-byte Reload
	movq	1064(%rsp), %r8         # 8-byte Reload
	movq	1016(%rsp), %r9         # 8-byte Reload
	movq	928(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	904(%rsp), %rdi         # 8-byte Reload
	movq	936(%rsp), %rsi         # 8-byte Reload
	movq	944(%rsp), %rdx         # 8-byte Reload
	movq	1080(%rsp), %rcx        # 8-byte Reload
	movq	1088(%rsp), %r8         # 8-byte Reload
	movl	964(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	984(%rsp), %rdi         # 8-byte Reload
	movq	1128(%rsp), %rsi        # 8-byte Reload
	movq	1200(%rsp), %rdx        # 8-byte Reload
	movq	1000(%rsp), %rcx        # 8-byte Reload
	movq	1096(%rsp), %r8         # 8-byte Reload
	movq	1016(%rsp), %r9         # 8-byte Reload
	movq	992(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	1000(%rsp), %rdi        # 8-byte Reload
	movq	1128(%rsp), %rsi        # 8-byte Reload
	movq	1320(%rsp), %rdx        # 8-byte Reload
	movq	1000(%rsp), %rcx        # 8-byte Reload
	movq	1072(%rsp), %r8         # 8-byte Reload
	movq	1016(%rsp), %r9         # 8-byte Reload
	movq	992(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	1200(%rsp), %rcx        # 8-byte Reload
	shlq	$2, %rcx
	movq	1320(%rsp), %rdx        # 8-byte Reload
	addq	%rcx, %rdx
	movq	1192(%rsp), %rdi        # 8-byte Reload
	movq	1128(%rsp), %rsi        # 8-byte Reload
	movq	%rdx, 896(%rsp)         # 8-byte Spill
	movq	1000(%rsp), %rcx        # 8-byte Reload
	movq	1080(%rsp), %r8         # 8-byte Reload
	movq	1016(%rsp), %r9         # 8-byte Reload
	movq	992(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	904(%rsp), %rdi         # 8-byte Reload
	movq	936(%rsp), %rsi         # 8-byte Reload
	movq	944(%rsp), %rdx         # 8-byte Reload
	movq	1176(%rsp), %rcx        # 8-byte Reload
	movq	1112(%rsp), %r8         # 8-byte Reload
	movl	964(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1000(%rsp), %rdi        # 8-byte Reload
	movq	1128(%rsp), %rsi        # 8-byte Reload
	movq	896(%rsp), %rdx         # 8-byte Reload
	movq	1000(%rsp), %rcx        # 8-byte Reload
	movq	1080(%rsp), %r8         # 8-byte Reload
	movq	1016(%rsp), %r9         # 8-byte Reload
	movq	992(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	1320(%rsp), %rcx        # 8-byte Reload
	movq	1200(%rsp), %rdx        # 8-byte Reload
	vmovss	(%rcx,%rdx,4), %xmm0
	cvtss2sd	%xmm0, %xmm1
	movq	1192(%rsp), %rdi        # 8-byte Reload
	movq	1184(%rsp), %rsi        # 8-byte Reload
	vmovss	%xmm0, 892(%rsp)        # 4-byte Spill
	vmovaps	%xmm1, %xmm0
	movq	1000(%rsp), %rdx        # 8-byte Reload
	movq	1176(%rsp), %rcx        # 8-byte Reload
	movq	1016(%rsp), %r8         # 8-byte Reload
	movq	992(%rsp), %r9          # 8-byte Reload
	callq	trace_logger_log_double
	movq	904(%rsp), %rdi         # 8-byte Reload
	movq	936(%rsp), %rsi         # 8-byte Reload
	movq	944(%rsp), %rdx         # 8-byte Reload
	movq	1120(%rsp), %rcx        # 8-byte Reload
	movq	1088(%rsp), %r8         # 8-byte Reload
	movl	964(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	984(%rsp), %rdi         # 8-byte Reload
	movq	1128(%rsp), %rsi        # 8-byte Reload
	movq	1200(%rsp), %rdx        # 8-byte Reload
	movq	1000(%rsp), %rcx        # 8-byte Reload
	movq	1096(%rsp), %r8         # 8-byte Reload
	movq	1016(%rsp), %r9         # 8-byte Reload
	movq	992(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	1000(%rsp), %rdi        # 8-byte Reload
	movq	1128(%rsp), %rsi        # 8-byte Reload
	movq	1328(%rsp), %rdx        # 8-byte Reload
	movq	1000(%rsp), %rcx        # 8-byte Reload
	movq	1104(%rsp), %r8         # 8-byte Reload
	movq	1016(%rsp), %r9         # 8-byte Reload
	movq	992(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	1200(%rsp), %rcx        # 8-byte Reload
	shlq	$2, %rcx
	movq	1328(%rsp), %rdx        # 8-byte Reload
	addq	%rcx, %rdx
	movq	1192(%rsp), %rdi        # 8-byte Reload
	movq	1128(%rsp), %rsi        # 8-byte Reload
	movq	%rdx, 880(%rsp)         # 8-byte Spill
	movq	1000(%rsp), %rcx        # 8-byte Reload
	movq	1120(%rsp), %r8         # 8-byte Reload
	movq	1016(%rsp), %r9         # 8-byte Reload
	movq	992(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	904(%rsp), %rdi         # 8-byte Reload
	movq	936(%rsp), %rsi         # 8-byte Reload
	movq	944(%rsp), %rdx         # 8-byte Reload
	movq	1152(%rsp), %rcx        # 8-byte Reload
	movq	1112(%rsp), %r8         # 8-byte Reload
	movl	964(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1000(%rsp), %rdi        # 8-byte Reload
	movq	1128(%rsp), %rsi        # 8-byte Reload
	movq	880(%rsp), %rdx         # 8-byte Reload
	movq	1000(%rsp), %rcx        # 8-byte Reload
	movq	1120(%rsp), %r8         # 8-byte Reload
	movq	1016(%rsp), %r9         # 8-byte Reload
	movq	992(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	1328(%rsp), %rcx        # 8-byte Reload
	movq	1200(%rsp), %rdx        # 8-byte Reload
	vmovss	(%rcx,%rdx,4), %xmm0
	cvtss2sd	%xmm0, %xmm1
	movq	1192(%rsp), %rdi        # 8-byte Reload
	movq	1184(%rsp), %rsi        # 8-byte Reload
	vmovss	%xmm0, 876(%rsp)        # 4-byte Spill
	vmovaps	%xmm1, %xmm0
	movq	1000(%rsp), %rdx        # 8-byte Reload
	movq	1152(%rsp), %rcx        # 8-byte Reload
	movq	1016(%rsp), %r8         # 8-byte Reload
	movq	992(%rsp), %r9          # 8-byte Reload
	callq	trace_logger_log_double
	movq	904(%rsp), %rdi         # 8-byte Reload
	movq	936(%rsp), %rsi         # 8-byte Reload
	movq	944(%rsp), %rdx         # 8-byte Reload
	movq	1168(%rsp), %rcx        # 8-byte Reload
	movq	1136(%rsp), %r8         # 8-byte Reload
	movl	964(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	984(%rsp), %rdi         # 8-byte Reload
	movq	1184(%rsp), %rsi        # 8-byte Reload
	vmovsd	952(%rsp), %xmm0        # 8-byte Reload
	movq	1016(%rsp), %rdx        # 8-byte Reload
	movq	1144(%rsp), %rcx        # 8-byte Reload
	movq	1016(%rsp), %r8         # 8-byte Reload
	movq	992(%rsp), %r9          # 8-byte Reload
	callq	trace_logger_log_double
	vmovss	876(%rsp), %xmm0        # 4-byte Reload
	cvtss2sd	%xmm0, %xmm0
	movq	1000(%rsp), %rdi        # 8-byte Reload
	movq	1184(%rsp), %rsi        # 8-byte Reload
	movq	1000(%rsp), %rdx        # 8-byte Reload
	movq	1152(%rsp), %rcx        # 8-byte Reload
	movq	1016(%rsp), %r8         # 8-byte Reload
	movq	992(%rsp), %r9          # 8-byte Reload
	callq	trace_logger_log_double
	vmovss	876(%rsp), %xmm0        # 4-byte Reload
	vmovss	960(%rsp), %xmm1        # 4-byte Reload
	vaddss	%xmm1, %xmm0, %xmm2
	cvtss2sd	%xmm2, %xmm0
	movq	1192(%rsp), %rdi        # 8-byte Reload
	movq	1184(%rsp), %rsi        # 8-byte Reload
	movq	1000(%rsp), %rdx        # 8-byte Reload
	movq	1168(%rsp), %rcx        # 8-byte Reload
	movq	1016(%rsp), %r8         # 8-byte Reload
	movq	992(%rsp), %r9          # 8-byte Reload
	vmovss	%xmm2, 872(%rsp)        # 4-byte Spill
	callq	trace_logger_log_double
	movq	904(%rsp), %rdi         # 8-byte Reload
	movq	936(%rsp), %rsi         # 8-byte Reload
	movq	944(%rsp), %rdx         # 8-byte Reload
	movq	1008(%rsp), %rcx        # 8-byte Reload
	movq	1160(%rsp), %r8         # 8-byte Reload
	movl	964(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	vmovss	872(%rsp), %xmm0        # 4-byte Reload
	cvtss2sd	%xmm0, %xmm0
	movq	984(%rsp), %rdi         # 8-byte Reload
	movq	1184(%rsp), %rsi        # 8-byte Reload
	movq	1000(%rsp), %rdx        # 8-byte Reload
	movq	1168(%rsp), %rcx        # 8-byte Reload
	movq	1016(%rsp), %r8         # 8-byte Reload
	movq	992(%rsp), %r9          # 8-byte Reload
	callq	trace_logger_log_double
	vmovss	892(%rsp), %xmm0        # 4-byte Reload
	cvtss2sd	%xmm0, %xmm0
	movq	1000(%rsp), %rdi        # 8-byte Reload
	movq	1184(%rsp), %rsi        # 8-byte Reload
	movq	1000(%rsp), %rdx        # 8-byte Reload
	movq	1176(%rsp), %rcx        # 8-byte Reload
	movq	1016(%rsp), %r8         # 8-byte Reload
	movq	992(%rsp), %r9          # 8-byte Reload
	callq	trace_logger_log_double
	vmovss	892(%rsp), %xmm0        # 4-byte Reload
	vmovss	872(%rsp), %xmm1        # 4-byte Reload
	vucomiss	%xmm1, %xmm0
	setne	%al
	setp	%cl
	orb	%al, %cl
	movb	%cl, %al
	andb	$1, %al
	movzbl	%al, %ebp
	movl	%ebp, %edx
	movq	1192(%rsp), %rdi        # 8-byte Reload
	movq	1000(%rsp), %rsi        # 8-byte Reload
	movq	1000(%rsp), %r8         # 8-byte Reload
	movb	%cl, 871(%rsp)          # 1-byte Spill
	movq	%r8, %rcx
	movq	1008(%rsp), %r8         # 8-byte Reload
	movq	1016(%rsp), %r9         # 8-byte Reload
	movq	992(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	904(%rsp), %rdi         # 8-byte Reload
	movq	936(%rsp), %rsi         # 8-byte Reload
	movq	944(%rsp), %rdx         # 8-byte Reload
	movq	968(%rsp), %rcx         # 8-byte Reload
	movq	984(%rsp), %r8          # 8-byte Reload
	movl	964(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	920(%rsp), %rdi         # 8-byte Reload
	movq	1016(%rsp), %rsi        # 8-byte Reload
	movq	1016(%rsp), %rdx        # 8-byte Reload
	movq	1000(%rsp), %rcx        # 8-byte Reload
	movq	912(%rsp), %r8          # 8-byte Reload
	movq	1016(%rsp), %r9         # 8-byte Reload
	movq	992(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	984(%rsp), %rdi         # 8-byte Reload
	movq	1016(%rsp), %rsi        # 8-byte Reload
	movq	1016(%rsp), %rdx        # 8-byte Reload
	movq	1000(%rsp), %rcx        # 8-byte Reload
	movq	928(%rsp), %r8          # 8-byte Reload
	movq	1016(%rsp), %r9         # 8-byte Reload
	movq	992(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	871(%rsp), %al          # 1-byte Reload
	andb	$1, %al
	movzbl	%al, %ebp
	movl	%ebp, %edx
	movq	1000(%rsp), %rdi        # 8-byte Reload
	movq	1000(%rsp), %rsi        # 8-byte Reload
	movq	1000(%rsp), %rcx        # 8-byte Reload
	movq	1008(%rsp), %r8         # 8-byte Reload
	movq	1016(%rsp), %r9         # 8-byte Reload
	movq	992(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	871(%rsp), %al          # 1-byte Reload
	testb	$1, %al
	movl	1212(%rsp), %ebp        # 4-byte Reload
	movl	%ebp, 864(%rsp)         # 4-byte Spill
	jne	.LBB0_2
	jmp	.LBB0_3
.LBB0_2:                                #   in Loop: Header=BB0_1 Depth=1
	movabsq	$2, %rdi
	movabsq	$64, %rax
	leaq	.L.str, %rdx
	movabsq	$0, %rcx
	leaq	.L.str10, %r8
	leaq	.L.str11, %rsi
	movabsq	$1, %r9
	leaq	.L.str31, %r10
	movabsq	$6, %r11
	leaq	fprintf, %rbx
	leaq	.L.str37, %r14
	movabsq	$31, %r15
	leaq	.L.str1, %r12
	leaq	.L.str30, %r13
	leaq	.L.str36, %rbp
	movq	%rax, 856(%rsp)         # 8-byte Spill
	movabsq	$49, %rax
	movq	%rsi, 848(%rsp)         # 8-byte Spill
	movl	$0, %esi
	movl	%esi, 844(%rsp)         # 4-byte Spill
	movl	$1, %esi
	movq	%rax, 832(%rsp)         # 8-byte Spill
	movabsq	$19134, %rax            # imm = 0x4ABE
	movq	%rax, 824(%rsp)         # 8-byte Spill
	movabsq	$32, %rax
	movq	%rax, 816(%rsp)         # 8-byte Spill
	leaq	.L.str35, %rax
	movq	%rax, 808(%rsp)         # 8-byte Spill
	leaq	.L.str16, %rax
	movq	%rax, 800(%rsp)         # 8-byte Spill
	movabsq	$33, %rax
	movq	%rax, 792(%rsp)         # 8-byte Spill
	leaq	.L.str34, %rax
	movq	%rax, 784(%rsp)         # 8-byte Spill
	leaq	.L.str27, %rax
	movq	%rax, 776(%rsp)         # 8-byte Spill
	movabsq	$41, %rax
	movq	%rax, 768(%rsp)         # 8-byte Spill
	leaq	.L.str33, %rax
	movq	%rax, 760(%rsp)         # 8-byte Spill
	leaq	.L.str23, %rax
	movq	%rax, 752(%rsp)         # 8-byte Spill
	leaq	stdout, %rax
	movq	%rax, 744(%rsp)         # 8-byte Spill
	leaq	.L.str32, %rax
	movq	%rax, 736(%rsp)         # 8-byte Spill
	movabsq	$27, %rax
	.loc	1 31 0                  # test_dma_load_store.c:31:0
.Ltmp24:
	movq	%rdi, 728(%rsp)         # 8-byte Spill
	movq	%r15, %rdi
	movl	%esi, 724(%rsp)         # 4-byte Spill
	movq	%r12, %rsi
	movq	%rdx, 712(%rsp)         # 8-byte Spill
	movq	%r13, %rdx
	movq	%rcx, 704(%rsp)         # 8-byte Spill
	movq	%r10, %rcx
	movq	%r8, 696(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movl	844(%rsp), %eax         # 4-byte Reload
	movq	%r9, 688(%rsp)          # 8-byte Spill
	movl	%eax, %r9d
	movl	$1, (%rsp)
	movq	%rbp, 680(%rsp)         # 8-byte Spill
	movq	%r14, 672(%rsp)         # 8-byte Spill
	movq	%r12, 664(%rsp)         # 8-byte Spill
	movq	%r15, 656(%rsp)         # 8-byte Spill
	movq	%rbx, 648(%rsp)         # 8-byte Spill
	movq	%r13, 640(%rsp)         # 8-byte Spill
	movq	%r10, 632(%rsp)         # 8-byte Spill
	movq	%r11, 624(%rsp)         # 8-byte Spill
	callq	trace_logger_log0
	movq	688(%rsp), %rdi         # 8-byte Reload
	movq	856(%rsp), %rsi         # 8-byte Reload
	movq	744(%rsp), %rdx         # 8-byte Reload
	movq	688(%rsp), %rcx         # 8-byte Reload
	movq	736(%rsp), %r8          # 8-byte Reload
	movq	704(%rsp), %r9          # 8-byte Reload
	movq	848(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	stdout, %rcx
	movq	824(%rsp), %rdi         # 8-byte Reload
	movq	856(%rsp), %rsi         # 8-byte Reload
	movq	%rcx, %rdx
	movq	688(%rsp), %r8          # 8-byte Reload
	movq	%rcx, 616(%rsp)         # 8-byte Spill
	movq	%r8, %rcx
	movq	632(%rsp), %r8          # 8-byte Reload
	movq	704(%rsp), %r9          # 8-byte Reload
	movq	848(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	656(%rsp), %rdi         # 8-byte Reload
	movq	664(%rsp), %rsi         # 8-byte Reload
	movq	640(%rsp), %rdx         # 8-byte Reload
	movq	760(%rsp), %rcx         # 8-byte Reload
	movq	768(%rsp), %r8          # 8-byte Reload
	movl	844(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	vmovss	892(%rsp), %xmm0        # 4-byte Reload
	cvtss2sd	%xmm0, %xmm0
	movq	688(%rsp), %rdi         # 8-byte Reload
	movq	816(%rsp), %rsi         # 8-byte Reload
	movq	688(%rsp), %rdx         # 8-byte Reload
	movq	752(%rsp), %rcx         # 8-byte Reload
	movq	704(%rsp), %r8          # 8-byte Reload
	movq	848(%rsp), %r9          # 8-byte Reload
	callq	trace_logger_log_double
	vmovss	892(%rsp), %xmm0        # 4-byte Reload
	cvtss2sd	%xmm0, %xmm1
	movq	824(%rsp), %rdi         # 8-byte Reload
	movq	856(%rsp), %rsi         # 8-byte Reload
	vmovaps	%xmm1, %xmm0
	movq	688(%rsp), %rdx         # 8-byte Reload
	movq	760(%rsp), %rcx         # 8-byte Reload
	movq	704(%rsp), %r8          # 8-byte Reload
	movq	848(%rsp), %r9          # 8-byte Reload
	vmovsd	%xmm1, 608(%rsp)        # 8-byte Spill
	callq	trace_logger_log_double
	movq	656(%rsp), %rdi         # 8-byte Reload
	movq	664(%rsp), %rsi         # 8-byte Reload
	movq	640(%rsp), %rdx         # 8-byte Reload
	movq	784(%rsp), %rcx         # 8-byte Reload
	movq	768(%rsp), %r8          # 8-byte Reload
	movl	844(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	vmovss	872(%rsp), %xmm0        # 4-byte Reload
	cvtss2sd	%xmm0, %xmm0
	movq	688(%rsp), %rdi         # 8-byte Reload
	movq	816(%rsp), %rsi         # 8-byte Reload
	movq	688(%rsp), %rdx         # 8-byte Reload
	movq	776(%rsp), %rcx         # 8-byte Reload
	movq	704(%rsp), %r8          # 8-byte Reload
	movq	848(%rsp), %r9          # 8-byte Reload
	callq	trace_logger_log_double
	vmovss	872(%rsp), %xmm0        # 4-byte Reload
	cvtss2sd	%xmm0, %xmm1
	movq	824(%rsp), %rdi         # 8-byte Reload
	movq	856(%rsp), %rsi         # 8-byte Reload
	vmovaps	%xmm1, %xmm0
	movq	688(%rsp), %rdx         # 8-byte Reload
	movq	784(%rsp), %rcx         # 8-byte Reload
	movq	704(%rsp), %r8          # 8-byte Reload
	movq	848(%rsp), %r9          # 8-byte Reload
	vmovsd	%xmm1, 600(%rsp)        # 8-byte Spill
	callq	trace_logger_log_double
	movq	656(%rsp), %rdi         # 8-byte Reload
	movq	664(%rsp), %rsi         # 8-byte Reload
	movq	640(%rsp), %rdx         # 8-byte Reload
	movq	808(%rsp), %rcx         # 8-byte Reload
	movq	792(%rsp), %r8          # 8-byte Reload
	movl	844(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	688(%rsp), %rdi         # 8-byte Reload
	movq	856(%rsp), %rsi         # 8-byte Reload
	movq	1200(%rsp), %rdx        # 8-byte Reload
	movq	688(%rsp), %rcx         # 8-byte Reload
	movq	800(%rsp), %r8          # 8-byte Reload
	movq	704(%rsp), %r9          # 8-byte Reload
	movq	848(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	1200(%rsp), %rcx        # 8-byte Reload
	movl	%ecx, %eax
	movl	%eax, %esi
	movl	%esi, %edx
	movq	824(%rsp), %rdi         # 8-byte Reload
	movq	816(%rsp), %rsi         # 8-byte Reload
	movq	688(%rsp), %rcx         # 8-byte Reload
	movq	808(%rsp), %r8          # 8-byte Reload
	movq	704(%rsp), %r9          # 8-byte Reload
	movq	848(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movl	%eax, 596(%rsp)         # 4-byte Spill
	callq	trace_logger_log_int
	movq	656(%rsp), %rdi         # 8-byte Reload
	movq	664(%rsp), %rsi         # 8-byte Reload
	movq	640(%rsp), %rdx         # 8-byte Reload
	movq	680(%rsp), %rcx         # 8-byte Reload
	movq	832(%rsp), %r8          # 8-byte Reload
	movl	844(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	624(%rsp), %rdi         # 8-byte Reload
	movq	856(%rsp), %rsi         # 8-byte Reload
	movq	648(%rsp), %rdx         # 8-byte Reload
	movq	688(%rsp), %rcx         # 8-byte Reload
	movq	672(%rsp), %r8          # 8-byte Reload
	movq	704(%rsp), %r9          # 8-byte Reload
	movq	848(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	688(%rsp), %rdi         # 8-byte Reload
	movq	856(%rsp), %rsi         # 8-byte Reload
	movq	616(%rsp), %rdx         # 8-byte Reload
	movq	688(%rsp), %rcx         # 8-byte Reload
	movq	632(%rsp), %r8          # 8-byte Reload
	movq	704(%rsp), %r9          # 8-byte Reload
	movq	848(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	728(%rsp), %rdi         # 8-byte Reload
	movq	856(%rsp), %rsi         # 8-byte Reload
	movq	712(%rsp), %rdx         # 8-byte Reload
	movq	704(%rsp), %rcx         # 8-byte Reload
	movq	696(%rsp), %r8          # 8-byte Reload
	movq	704(%rsp), %r9          # 8-byte Reload
	movq	848(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movl	$.L.str, %eax
	movl	%eax, %esi
	movb	$2, %al
	movq	616(%rsp), %rdi         # 8-byte Reload
	movl	596(%rsp), %edx         # 4-byte Reload
	vmovsd	608(%rsp), %xmm0        # 8-byte Reload
	vmovsd	600(%rsp), %xmm1        # 8-byte Reload
	callq	fprintf
	movabsq	$1, %rcx
	movabsq	$0, %rsi
	leaq	.L.str18, %r8
	leaq	.L.str11, %rdi
	movabsq	$34, %r9
	leaq	.L.str1, %r10
	leaq	.L.str30, %r11
	leaq	.L.str39, %rbx
	movabsq	$2, %r14
	movl	$0, %edx
	movl	$1, %ebp
	movabsq	$19134, %r15            # imm = 0x4ABE
	movabsq	$32, %r12
	leaq	.L.str38, %r13
	movq	%rcx, 584(%rsp)         # 8-byte Spill
	leaq	.L.str19, %rcx
	movq	%rcx, 576(%rsp)         # 8-byte Spill
	leaq	.L.str10, %rcx
	movq	%rcx, 568(%rsp)         # 8-byte Spill
	movabsq	$33, %rcx
	movq	%rcx, 560(%rsp)         # 8-byte Spill
	movabsq	$8, %rcx
	movq	%rcx, 552(%rsp)         # 8-byte Spill
	leaq	.L.str36, %rcx
	.loc	1 33 0                  # test_dma_load_store.c:33:0
	movl	%eax, %eax
                                        # kill: RAX<def> EAX<kill>
	movq	%rdi, 544(%rsp)         # 8-byte Spill
	movq	%r15, %rdi
	movq	%rsi, 536(%rsp)         # 8-byte Spill
	movq	%r12, %rsi
	movl	%edx, 532(%rsp)         # 4-byte Spill
	movq	%rax, %rdx
	movq	584(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 520(%rsp)         # 8-byte Spill
	movq	%rax, %rcx
	movq	520(%rsp), %rax         # 8-byte Reload
	movq	%r8, 512(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movq	536(%rsp), %rax         # 8-byte Reload
	movq	%r9, 504(%rsp)          # 8-byte Spill
	movq	%rax, %r9
	movq	544(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	movq	%r13, 496(%rsp)         # 8-byte Spill
	movq	%r12, 488(%rsp)         # 8-byte Spill
	movl	%ebp, 484(%rsp)         # 4-byte Spill
	movq	%r15, 472(%rsp)         # 8-byte Spill
	movq	%r14, 464(%rsp)         # 8-byte Spill
	movq	%r10, 456(%rsp)         # 8-byte Spill
	movq	%r11, 448(%rsp)         # 8-byte Spill
	movq	%rbx, 440(%rsp)         # 8-byte Spill
	callq	trace_logger_log_int
	movq	560(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	448(%rsp), %rdx         # 8-byte Reload
	movq	496(%rsp), %rcx         # 8-byte Reload
	movq	552(%rsp), %r8          # 8-byte Reload
	movl	532(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	464(%rsp), %rdi         # 8-byte Reload
	movq	488(%rsp), %rsi         # 8-byte Reload
	movq	584(%rsp), %rdx         # 8-byte Reload
	movq	536(%rsp), %rcx         # 8-byte Reload
	movq	568(%rsp), %r8          # 8-byte Reload
	movq	536(%rsp), %r9          # 8-byte Reload
	movq	544(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	1212(%rsp), %ebp        # 4-byte Reload
	movl	%ebp, %esi
	movl	%esi, %edx
	movq	584(%rsp), %rdi         # 8-byte Reload
	movq	488(%rsp), %rsi         # 8-byte Reload
	movq	584(%rsp), %rcx         # 8-byte Reload
	movq	576(%rsp), %r8          # 8-byte Reload
	movq	536(%rsp), %r9          # 8-byte Reload
	movq	544(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	1212(%rsp), %ebp        # 4-byte Reload
	addl	$1, %ebp
	movl	%ebp, %esi
	movl	%esi, %edx
	movq	472(%rsp), %rdi         # 8-byte Reload
	movq	488(%rsp), %rsi         # 8-byte Reload
	movq	584(%rsp), %rcx         # 8-byte Reload
	movq	496(%rsp), %r8          # 8-byte Reload
	movq	536(%rsp), %r9          # 8-byte Reload
	movq	544(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	movl	%ebp, 436(%rsp)         # 4-byte Spill
	callq	trace_logger_log_int
.Ltmp25:
	#DEBUG_VALUE: test_stores:num_failures <- [RSP+436]
	.loc	1 34 0                  # test_dma_load_store.c:34:0
	movq	504(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	448(%rsp), %rdx         # 8-byte Reload
	movq	440(%rsp), %rcx         # 8-byte Reload
	movq	464(%rsp), %r8          # 8-byte Reload
	movl	532(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	584(%rsp), %rdi         # 8-byte Reload
	movq	536(%rsp), %rsi         # 8-byte Reload
	movq	536(%rsp), %rdx         # 8-byte Reload
	movq	584(%rsp), %rcx         # 8-byte Reload
	movq	512(%rsp), %r8          # 8-byte Reload
	movq	536(%rsp), %r9          # 8-byte Reload
	movq	544(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	436(%rsp), %ebp         # 4-byte Reload
	movl	%ebp, 864(%rsp)         # 4-byte Spill
.Ltmp26:
.LBB0_3:                                #   in Loop: Header=BB0_1 Depth=1
	movl	864(%rsp), %eax         # 4-byte Reload
	movabsq	$1, %rcx
	leaq	.L.str41, %rdx
	movabsq	$0, %rsi
	leaq	.L.str11, %rdi
	movabsq	$2, %r8
	leaq	.L.str14, %r9
	movabsq	$3, %r10
	leaq	.L.str15, %r11
	movabsq	$29, %rbx
	leaq	.L.str1, %r14
	leaq	.L.str18, %r15
	leaq	.L.str42, %r12
	movl	$0, %ebp
	movl	$1, %r13d
	movq	%rcx, 424(%rsp)         # 8-byte Spill
	movabsq	$19134, %rcx            # imm = 0x4ABE
	movq	%rcx, 416(%rsp)         # 8-byte Spill
	movabsq	$32, %rcx
	movq	%rcx, 408(%rsp)         # 8-byte Spill
	leaq	.L.str40, %rcx
	movq	%rcx, 400(%rsp)         # 8-byte Spill
	leaq	.L.str12, %rcx
	movq	%rcx, 392(%rsp)         # 8-byte Spill
	movabsq	$46, %rcx
	movq	%rcx, 384(%rsp)         # 8-byte Spill
	movabsq	$64, %rcx
	movq	%rcx, 376(%rsp)         # 8-byte Spill
	leaq	.L.str17, %rcx
	movq	%rcx, 368(%rsp)         # 8-byte Spill
	movabsq	$33, %rcx
	movq	%rcx, 360(%rsp)         # 8-byte Spill
	leaq	.L.str16, %rcx
	movq	%rcx, 352(%rsp)         # 8-byte Spill
	leaq	.L.str10, %rcx
	movq	%rcx, 344(%rsp)         # 8-byte Spill
	movabsq	$8, %rcx
	movq	%rcx, 336(%rsp)         # 8-byte Spill
	leaq	.L.str20, %rcx
	movq	%rcx, 328(%rsp)         # 8-byte Spill
	leaq	.L.str30, %rcx
	movq	%rcx, 320(%rsp)         # 8-byte Spill
	leaq	.L.str38, %rcx
	movq	%rcx, 312(%rsp)         # 8-byte Spill
	leaq	.L.str19, %rcx
	movq	%rcx, 304(%rsp)         # 8-byte Spill
	movabsq	$-1, %rcx
	movq	%rcx, 296(%rsp)         # 8-byte Spill
	movabsq	$48, %rcx
	.loc	1 29 0                  # test_dma_load_store.c:29:0
	movq	%rcx, 288(%rsp)         # 8-byte Spill
	movq	296(%rsp), %rcx         # 8-byte Reload
	movq	%rdi, 280(%rsp)         # 8-byte Spill
	movq	%rcx, %rdi
	movq	%rsi, 272(%rsp)         # 8-byte Spill
	movq	%r14, %rsi
	movq	%rdx, 264(%rsp)         # 8-byte Spill
	movq	%r15, %rdx
	movq	328(%rsp), %rcx         # 8-byte Reload
	movq	%r8, 256(%rsp)          # 8-byte Spill
	movq	288(%rsp), %r8          # 8-byte Reload
	movq	%r9, 248(%rsp)          # 8-byte Spill
	movl	%ebp, %r9d
	movl	$1, (%rsp)
	movl	%eax, 244(%rsp)         # 4-byte Spill
	movq	%r15, 232(%rsp)         # 8-byte Spill
	movq	%r12, 224(%rsp)         # 8-byte Spill
	movl	%ebp, 220(%rsp)         # 4-byte Spill
	movl	%r13d, 216(%rsp)        # 4-byte Spill
	movq	%r14, 208(%rsp)         # 8-byte Spill
	movq	%rbx, 200(%rsp)         # 8-byte Spill
	movq	%r10, 192(%rsp)         # 8-byte Spill
	movq	%r11, 184(%rsp)         # 8-byte Spill
	callq	trace_logger_log0
	movq	256(%rsp), %rdi         # 8-byte Reload
	movq	408(%rsp), %rsi         # 8-byte Reload
	movq	272(%rsp), %rdx         # 8-byte Reload
	movq	424(%rsp), %rcx         # 8-byte Reload
	movq	304(%rsp), %r8          # 8-byte Reload
	movq	424(%rsp), %r9          # 8-byte Reload
	movq	248(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	424(%rsp), %rdi         # 8-byte Reload
	movq	408(%rsp), %rsi         # 8-byte Reload
	movq	272(%rsp), %rdx         # 8-byte Reload
	movq	424(%rsp), %rcx         # 8-byte Reload
	movq	312(%rsp), %r8          # 8-byte Reload
	movq	424(%rsp), %r9          # 8-byte Reload
	movq	320(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movl	244(%rsp), %eax         # 4-byte Reload
	movl	%eax, %ebp
	movl	%ebp, %edx
	movq	416(%rsp), %rdi         # 8-byte Reload
	movq	408(%rsp), %rsi         # 8-byte Reload
	movq	424(%rsp), %rcx         # 8-byte Reload
	movq	328(%rsp), %r8          # 8-byte Reload
	movq	272(%rsp), %r9          # 8-byte Reload
	movq	320(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	200(%rsp), %rdi         # 8-byte Reload
	movq	208(%rsp), %rsi         # 8-byte Reload
	movq	232(%rsp), %rdx         # 8-byte Reload
	movq	368(%rsp), %rcx         # 8-byte Reload
	movq	336(%rsp), %r8          # 8-byte Reload
	movl	220(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	256(%rsp), %rdi         # 8-byte Reload
	movq	376(%rsp), %rsi         # 8-byte Reload
	movq	424(%rsp), %rdx         # 8-byte Reload
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	344(%rsp), %r8          # 8-byte Reload
	movq	272(%rsp), %r9          # 8-byte Reload
	movq	280(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	424(%rsp), %rdi         # 8-byte Reload
	movq	376(%rsp), %rsi         # 8-byte Reload
	movq	1200(%rsp), %rdx        # 8-byte Reload
	movq	424(%rsp), %rcx         # 8-byte Reload
	movq	352(%rsp), %r8          # 8-byte Reload
	movq	272(%rsp), %r9          # 8-byte Reload
	movq	280(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	1200(%rsp), %rcx        # 8-byte Reload
	addq	$1, %rcx
	movq	416(%rsp), %rdi         # 8-byte Reload
	movq	376(%rsp), %rsi         # 8-byte Reload
	movq	%rcx, %rdx
	movq	424(%rsp), %r8          # 8-byte Reload
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	movq	%r8, %rcx
	movq	368(%rsp), %r8          # 8-byte Reload
	movq	272(%rsp), %r9          # 8-byte Reload
	movq	280(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	200(%rsp), %rdi         # 8-byte Reload
	movq	208(%rsp), %rsi         # 8-byte Reload
	movq	232(%rsp), %rdx         # 8-byte Reload
	movq	400(%rsp), %rcx         # 8-byte Reload
	movq	360(%rsp), %r8          # 8-byte Reload
	movl	220(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	424(%rsp), %rdi         # 8-byte Reload
	movq	376(%rsp), %rsi         # 8-byte Reload
	movq	176(%rsp), %rdx         # 8-byte Reload
	movq	424(%rsp), %rcx         # 8-byte Reload
	movq	368(%rsp), %r8          # 8-byte Reload
	movq	272(%rsp), %r9          # 8-byte Reload
	movq	280(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	176(%rsp), %rcx         # 8-byte Reload
	movl	%ecx, %eax
	movl	%eax, %ebp
	movl	%ebp, %edx
	movq	416(%rsp), %rdi         # 8-byte Reload
	movq	408(%rsp), %rsi         # 8-byte Reload
	movq	424(%rsp), %rcx         # 8-byte Reload
	movq	400(%rsp), %r8          # 8-byte Reload
	movq	272(%rsp), %r9          # 8-byte Reload
	movq	280(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movl	%eax, 172(%rsp)         # 4-byte Spill
	callq	trace_logger_log_int
	movq	200(%rsp), %rdi         # 8-byte Reload
	movq	208(%rsp), %rsi         # 8-byte Reload
	movq	232(%rsp), %rdx         # 8-byte Reload
	movq	264(%rsp), %rcx         # 8-byte Reload
	movq	384(%rsp), %r8          # 8-byte Reload
	movl	220(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movl	1316(%rsp), %eax        # 4-byte Reload
	movl	%eax, %r9d
	movl	%r9d, %edx
	movq	256(%rsp), %rdi         # 8-byte Reload
	movq	408(%rsp), %rsi         # 8-byte Reload
	movq	424(%rsp), %rcx         # 8-byte Reload
	movq	392(%rsp), %r8          # 8-byte Reload
	movq	272(%rsp), %r9          # 8-byte Reload
	movq	280(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movl	172(%rsp), %eax         # 4-byte Reload
	movl	%eax, %ebp
	movl	%ebp, %edx
	movq	424(%rsp), %rdi         # 8-byte Reload
	movq	408(%rsp), %rsi         # 8-byte Reload
	movq	424(%rsp), %rcx         # 8-byte Reload
	movq	400(%rsp), %r8          # 8-byte Reload
	movq	272(%rsp), %r9          # 8-byte Reload
	movq	280(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movl	172(%rsp), %eax         # 4-byte Reload
	movl	1316(%rsp), %ebp        # 4-byte Reload
	cmpl	%ebp, %eax
	sete	%al
	movb	%al, %cl
	andb	$1, %cl
	movzbl	%cl, %r13d
	movl	%r13d, %edx
	movq	416(%rsp), %rdi         # 8-byte Reload
	movq	424(%rsp), %rsi         # 8-byte Reload
	movq	424(%rsp), %rcx         # 8-byte Reload
	movq	264(%rsp), %r8          # 8-byte Reload
	movq	272(%rsp), %r9          # 8-byte Reload
	movq	280(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movb	%al, 171(%rsp)          # 1-byte Spill
	callq	trace_logger_log_int
	movq	200(%rsp), %rdi         # 8-byte Reload
	movq	208(%rsp), %rsi         # 8-byte Reload
	movq	232(%rsp), %rdx         # 8-byte Reload
	movq	224(%rsp), %rcx         # 8-byte Reload
	movq	256(%rsp), %r8          # 8-byte Reload
	movl	220(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	192(%rsp), %rdi         # 8-byte Reload
	movq	272(%rsp), %rsi         # 8-byte Reload
	movq	272(%rsp), %rdx         # 8-byte Reload
	movq	424(%rsp), %rcx         # 8-byte Reload
	movq	184(%rsp), %r8          # 8-byte Reload
	movq	272(%rsp), %r9          # 8-byte Reload
	movq	280(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	256(%rsp), %rdi         # 8-byte Reload
	movq	272(%rsp), %rsi         # 8-byte Reload
	movq	272(%rsp), %rdx         # 8-byte Reload
	movq	424(%rsp), %rcx         # 8-byte Reload
	movq	248(%rsp), %r8          # 8-byte Reload
	movq	272(%rsp), %r9          # 8-byte Reload
	movq	280(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	171(%rsp), %al          # 1-byte Reload
	andb	$1, %al
	movzbl	%al, %ebp
	movl	%ebp, %edx
	movq	424(%rsp), %rdi         # 8-byte Reload
	movq	424(%rsp), %rsi         # 8-byte Reload
	movq	424(%rsp), %rcx         # 8-byte Reload
	movq	264(%rsp), %r8          # 8-byte Reload
	movq	272(%rsp), %r9          # 8-byte Reload
	movq	280(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	171(%rsp), %al          # 1-byte Reload
	testb	$1, %al
	movq	176(%rsp), %rcx         # 8-byte Reload
	movl	244(%rsp), %ebp         # 4-byte Reload
	movq	%rcx, 1216(%rsp)        # 8-byte Spill
	movl	%ebp, 1224(%rsp)        # 4-byte Spill
	jne	.LBB0_4
	jmp	.LBB0_1
.Ltmp27:
.LBB0_4:                                # %._crit_edge
	movl	1224(%rsp), %eax        # 4-byte Reload
	movabsq	$1, %rcx
	movabsq	$32, %rdx
	leaq	.L.str43, %rsi
	movabsq	$0, %rdi
	leaq	.L.str11, %r8
	movabsq	$36, %r9
	leaq	.L.str1, %r10
	leaq	.L.str15, %r11
	leaq	.L.str44, %rbx
	movl	$0, %ebp
	movl	$1, %r14d
	movabsq	$19134, %r15            # imm = 0x4ABE
	leaq	.L.str8, %r12
	leaq	.L.str10, %r13
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	movabsq	$2, %rcx
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	leaq	.L.str20, %rcx
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	leaq	.L.str18, %rcx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	movabsq	$-1, %rcx
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	movabsq	$48, %rcx
	.loc	1 36 0                  # test_dma_load_store.c:36:0
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	movq	128(%rsp), %rcx         # 8-byte Reload
	movq	%rdi, 112(%rsp)         # 8-byte Spill
	movq	%rcx, %rdi
	movq	%rsi, 104(%rsp)         # 8-byte Spill
	movq	%r10, %rsi
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	movq	%r11, %rdx
	movq	104(%rsp), %rcx         # 8-byte Reload
	movq	%r8, 88(%rsp)           # 8-byte Spill
	movq	120(%rsp), %r8          # 8-byte Reload
	movq	%r9, 80(%rsp)           # 8-byte Spill
	movl	%ebp, %r9d
	movl	$1, (%rsp)
	movl	%eax, 76(%rsp)          # 4-byte Spill
	movl	%r14d, 72(%rsp)         # 4-byte Spill
	movq	%r15, 64(%rsp)          # 8-byte Spill
	movq	%r12, 56(%rsp)          # 8-byte Spill
	movq	%r13, 48(%rsp)          # 8-byte Spill
	movl	%ebp, 44(%rsp)          # 4-byte Spill
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	%r10, 24(%rsp)          # 8-byte Spill
	movq	%r11, 16(%rsp)          # 8-byte Spill
	callq	trace_logger_log0
	movq	152(%rsp), %rdi         # 8-byte Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	112(%rsp), %rdx         # 8-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	144(%rsp), %r8          # 8-byte Reload
	movq	160(%rsp), %r9          # 8-byte Reload
	movq	136(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	160(%rsp), %rdi         # 8-byte Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	112(%rsp), %rdx         # 8-byte Reload
	movq	112(%rsp), %rcx         # 8-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	160(%rsp), %r9          # 8-byte Reload
	movq	56(%rsp), %r10          # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movl	76(%rsp), %eax          # 4-byte Reload
	movl	%eax, %ebp
	movl	%ebp, %edx
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	104(%rsp), %r8          # 8-byte Reload
	movq	112(%rsp), %r9          # 8-byte Reload
	movq	56(%rsp), %r10          # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	80(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	160(%rsp), %r8          # 8-byte Reload
	movl	44(%rsp), %r9d          # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movl	76(%rsp), %eax          # 4-byte Reload
	movl	%eax, %r9d
	movl	%r9d, %edx
	movq	160(%rsp), %rdi         # 8-byte Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	104(%rsp), %r8          # 8-byte Reload
	movq	112(%rsp), %r9          # 8-byte Reload
	movq	88(%rsp), %r10          # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movl	76(%rsp), %eax          # 4-byte Reload
	addq	$1432, %rsp             # imm = 0x598
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.Ltmp28:
.Ltmp29:
	.size	test_stores, .Ltmp29-test_stores
.Lfunc_end0:
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.align	4
.LCPI1_0:
	.long	1065353216              # float 1
	.section	.rodata.cst8,"aM",@progbits,8
	.align	8
.LCPI1_1:
	.quad	4607182418800017408     # double 1
	.text
	.globl	s1
	.align	16, 0x90
	.type	s1,@function
s1:                                     # @s1
	.cfi_startproc
.Lfunc_begin1:
	.loc	1 71 0                  # test_dma_load_store.c:71:0
# BB#0:
	pushq	%rbp
.Ltmp37:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Ltmp38:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Ltmp39:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Ltmp40:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Ltmp41:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Ltmp42:
	.cfi_def_cfa_offset 56
	subq	$616, %rsp              # imm = 0x268
.Ltmp43:
	.cfi_def_cfa_offset 672
.Ltmp44:
	.cfi_offset %rbx, -56
.Ltmp45:
	.cfi_offset %r12, -48
.Ltmp46:
	.cfi_offset %r13, -40
.Ltmp47:
	.cfi_offset %r14, -32
.Ltmp48:
	.cfi_offset %r15, -24
.Ltmp49:
	.cfi_offset %rbp, -16
	movabsq	$0, %rax
	movabsq	$1, %rcx
	leaq	.L.str9, %r8
	leaq	.L.str11, %r9
	movabsq	$2, %r10
	leaq	.L.str15, %r11
	movabsq	$3, %rbx
	leaq	.L.str14, %r14
	movabsq	$79, %r15
	leaq	.L.str45, %r12
	leaq	.L.str8, %r13
	leaq	.L.str46, %rbp
	movl	%edx, 612(%rsp)         # 4-byte Spill
	movl	$0, %edx
	movl	%edx, 608(%rsp)         # 4-byte Spill
	movl	$1, %edx
	movq	%rax, 600(%rsp)         # 8-byte Spill
	movabsq	$19134, %rax            # imm = 0x4ABE
	movq	%rax, 592(%rsp)         # 8-byte Spill
	movabsq	$32, %rax
	movq	%rax, 584(%rsp)         # 8-byte Spill
	leaq	.L.str12, %rax
	movq	%rax, 576(%rsp)         # 8-byte Spill
	leaq	.L.str10, %rax
	movq	%rax, 568(%rsp)         # 8-byte Spill
	movabsq	$46, %rax
	movq	%rax, 560(%rsp)         # 8-byte Spill
	movabsq	$24601, %rax            # imm = 0x6019
	movq	%rax, 552(%rsp)         # 8-byte Spill
	movabsq	$64, %rax
	movq	%rax, 544(%rsp)         # 8-byte Spill
	leaq	.L.str22, %rax
	movq	%rax, 536(%rsp)         # 8-byte Spill
	leaq	.L.str25, %rax
	.loc	1 71 0 prologue_end     # test_dma_load_store.c:71:0
.Ltmp50:
	movq	%rax, 528(%rsp)         # 8-byte Spill
	movq	552(%rsp), %rax         # 8-byte Reload
	movq	%rdi, 520(%rsp)         # 8-byte Spill
	movq	%rax, %rdi
	movq	544(%rsp), %rax         # 8-byte Reload
	movq	%rsi, 512(%rsp)         # 8-byte Spill
	movq	%rax, %rsi
	movq	520(%rsp), %rax         # 8-byte Reload
	movl	%edx, 508(%rsp)         # 4-byte Spill
	movq	%rax, %rdx
	movq	%rcx, 496(%rsp)         # 8-byte Spill
	movq	528(%rsp), %rax         # 8-byte Reload
	movq	%r8, 488(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movq	600(%rsp), %rax         # 8-byte Reload
	movq	%r9, 480(%rsp)          # 8-byte Spill
	movq	%rax, %r9
	movq	480(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	movq	%r13, 472(%rsp)         # 8-byte Spill
	movq	%rbp, 464(%rsp)         # 8-byte Spill
	movq	%r12, 456(%rsp)         # 8-byte Spill
	movq	%r15, 448(%rsp)         # 8-byte Spill
	movq	%r14, 440(%rsp)         # 8-byte Spill
	movq	%rbx, 432(%rsp)         # 8-byte Spill
	movq	%r10, 424(%rsp)         # 8-byte Spill
	movq	%r11, 416(%rsp)         # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	552(%rsp), %rdi         # 8-byte Reload
	movq	544(%rsp), %rsi         # 8-byte Reload
	movq	512(%rsp), %rdx         # 8-byte Reload
	movq	496(%rsp), %rcx         # 8-byte Reload
	movq	536(%rsp), %r8          # 8-byte Reload
	movq	600(%rsp), %r9          # 8-byte Reload
	movq	480(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movl	612(%rsp), %esi         # 4-byte Reload
	movl	%esi, %esi
	movl	%esi, %edx
	movq	552(%rsp), %rdi         # 8-byte Reload
	movq	584(%rsp), %rsi         # 8-byte Reload
	movq	496(%rsp), %rcx         # 8-byte Reload
	movq	576(%rsp), %r8          # 8-byte Reload
	movq	600(%rsp), %r9          # 8-byte Reload
	movq	480(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	#DEBUG_VALUE: s1:store_vals <- [RSP+520]
	#DEBUG_VALUE: s1:store_loc <- [RSP+512]
	#DEBUG_VALUE: s1:num_vals <- [RSP+612]
.Ltmp51:
	#DEBUG_VALUE: i <- 0
	.loc	1 79 0                  # test_dma_load_store.c:79:0
	movq	448(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	472(%rsp), %rdx         # 8-byte Reload
	movq	488(%rsp), %rcx         # 8-byte Reload
	movq	560(%rsp), %r8          # 8-byte Reload
	movl	608(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	424(%rsp), %rdi         # 8-byte Reload
	movq	584(%rsp), %rsi         # 8-byte Reload
	movq	600(%rsp), %rdx         # 8-byte Reload
	movq	600(%rsp), %rcx         # 8-byte Reload
	movq	568(%rsp), %r8          # 8-byte Reload
	movq	600(%rsp), %r9          # 8-byte Reload
	movq	480(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	612(%rsp), %esi         # 4-byte Reload
	movl	%esi, %esi
	movl	%esi, %edx
	movq	496(%rsp), %rdi         # 8-byte Reload
	movq	584(%rsp), %rsi         # 8-byte Reload
	movq	496(%rsp), %rcx         # 8-byte Reload
	movq	576(%rsp), %r8          # 8-byte Reload
	movq	600(%rsp), %r9          # 8-byte Reload
	movq	480(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	612(%rsp), %esi         # 4-byte Reload
	cmpl	$0, %esi
	setg	%al
	movb	%al, %cl
	andb	$1, %cl
	movzbl	%cl, %ecx
	movl	%ecx, %edx
	movq	592(%rsp), %rdi         # 8-byte Reload
	movq	496(%rsp), %rsi         # 8-byte Reload
	movq	496(%rsp), %rcx         # 8-byte Reload
	movq	488(%rsp), %r8          # 8-byte Reload
	movq	600(%rsp), %r9          # 8-byte Reload
	movq	480(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movb	%al, 415(%rsp)          # 1-byte Spill
	callq	trace_logger_log_int
	movq	448(%rsp), %rdi         # 8-byte Reload
	movq	456(%rsp), %rsi         # 8-byte Reload
	movq	472(%rsp), %rdx         # 8-byte Reload
	movq	464(%rsp), %rcx         # 8-byte Reload
	movq	424(%rsp), %r8          # 8-byte Reload
	movl	608(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	432(%rsp), %rdi         # 8-byte Reload
	movq	600(%rsp), %rsi         # 8-byte Reload
	movq	600(%rsp), %rdx         # 8-byte Reload
	movq	496(%rsp), %rcx         # 8-byte Reload
	movq	440(%rsp), %r8          # 8-byte Reload
	movq	600(%rsp), %r9          # 8-byte Reload
	movq	480(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	424(%rsp), %rdi         # 8-byte Reload
	movq	600(%rsp), %rsi         # 8-byte Reload
	movq	600(%rsp), %rdx         # 8-byte Reload
	movq	496(%rsp), %rcx         # 8-byte Reload
	movq	416(%rsp), %r8          # 8-byte Reload
	movq	600(%rsp), %r9          # 8-byte Reload
	movq	480(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	415(%rsp), %al          # 1-byte Reload
	andb	$1, %al
	movzbl	%al, %eax
	movl	%eax, %edx
	movq	496(%rsp), %rdi         # 8-byte Reload
	movq	496(%rsp), %rsi         # 8-byte Reload
	movq	496(%rsp), %rcx         # 8-byte Reload
	movq	488(%rsp), %r8          # 8-byte Reload
	movq	600(%rsp), %r9          # 8-byte Reload
	movq	480(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	415(%rsp), %al          # 1-byte Reload
	testb	$1, %al
	movq	600(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, 400(%rsp)         # 8-byte Spill
	jne	.LBB1_1
	jmp	.LBB1_2
.Ltmp52:
.LBB1_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	400(%rsp), %rax         # 8-byte Reload
	movabsq	$1, %rcx
	leaq	.L.str41, %rdx
	movabsq	$0, %rsi
	leaq	.L.str11, %rdi
	movabsq	$2, %r8
	leaq	.L.str14, %r9
	movabsq	$3, %r10
	leaq	.L.str15, %r11
	movabsq	$79, %rbx
	leaq	.L.str45, %r14
	leaq	.L.str47, %r15
	movl	$0, %ebp
	movl	$1, %r12d
	movabsq	$19134, %r13            # imm = 0x4ABE
	movq	%rax, 392(%rsp)         # 8-byte Spill
	movabsq	$32, %rax
	movq	%rax, 384(%rsp)         # 8-byte Spill
	leaq	.L.str40, %rax
	movq	%rax, 376(%rsp)         # 8-byte Spill
	leaq	.L.str12, %rax
	movq	%rax, 368(%rsp)         # 8-byte Spill
	movabsq	$46, %rax
	movq	%rax, 360(%rsp)         # 8-byte Spill
	movabsq	$64, %rax
	movq	%rax, 352(%rsp)         # 8-byte Spill
	leaq	.L.str17, %rax
	movq	%rax, 344(%rsp)         # 8-byte Spill
	movabsq	$33, %rax
	movq	%rax, 336(%rsp)         # 8-byte Spill
	leaq	.L.str16, %rax
	movq	%rax, 328(%rsp)         # 8-byte Spill
	leaq	.L.str10, %rax
	movq	%rax, 320(%rsp)         # 8-byte Spill
	movabsq	$8, %rax
	movq	%rax, 312(%rsp)         # 8-byte Spill
	leaq	.L.str24, %rax
	movq	%rax, 304(%rsp)         # 8-byte Spill
	leaq	.L.str26, %rax
	movq	%rax, 296(%rsp)         # 8-byte Spill
	movabsq	$80, %rax
	movq	%rax, 288(%rsp)         # 8-byte Spill
	leaq	.L.str29, %rax
	movq	%rax, 280(%rsp)         # 8-byte Spill
	movabsq	$28, %rax
	movq	%rax, 272(%rsp)         # 8-byte Spill
	leaq	.L.str22, %rax
	movq	%rax, 264(%rsp)         # 8-byte Spill
	movabsq	$29, %rax
	vmovss	.LCPI1_0, %xmm0
	movq	%rax, 256(%rsp)         # 8-byte Spill
	leaq	.L.str23, %rax
	vmovsd	.LCPI1_1, %xmm1
	movq	%rax, 248(%rsp)         # 8-byte Spill
	movabsq	$9, %rax
	movq	%rax, 240(%rsp)         # 8-byte Spill
	leaq	.L.str21, %rax
	movq	%rax, 232(%rsp)         # 8-byte Spill
	movabsq	$27, %rax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	leaq	.L.str25, %rax
	movq	%rax, 216(%rsp)         # 8-byte Spill
	leaq	.L.str8, %rax
	movq	%rax, 208(%rsp)         # 8-byte Spill
	movabsq	$-1, %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	movabsq	$48, %rax
	.loc	1 80 0                  # test_dma_load_store.c:80:0
	movq	%rax, 192(%rsp)         # 8-byte Spill
	movq	200(%rsp), %rax         # 8-byte Reload
	movq	%rdi, 184(%rsp)         # 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, 176(%rsp)         # 8-byte Spill
	movq	%r14, %rsi
	movq	%rdx, 168(%rsp)         # 8-byte Spill
	movq	%r9, %rdx
	movq	328(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	movq	%rax, %rcx
	movq	192(%rsp), %rax         # 8-byte Reload
	movq	%r8, 152(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movq	%r9, 144(%rsp)          # 8-byte Spill
	movl	%ebp, %r9d
	movl	$1, (%rsp)
	movl	%r12d, 140(%rsp)        # 4-byte Spill
	movq	%r13, 128(%rsp)         # 8-byte Spill
	vmovss	%xmm0, 124(%rsp)        # 4-byte Spill
	vmovsd	%xmm1, 112(%rsp)        # 8-byte Spill
	movl	%ebp, 108(%rsp)         # 4-byte Spill
	movq	%r15, 96(%rsp)          # 8-byte Spill
	movq	%r10, 88(%rsp)          # 8-byte Spill
	movq	%r11, 80(%rsp)          # 8-byte Spill
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	movq	%r14, 64(%rsp)          # 8-byte Spill
	callq	trace_logger_log0
	movq	152(%rsp), %rdi         # 8-byte Reload
	movq	352(%rsp), %rsi         # 8-byte Reload
	movq	176(%rsp), %rdx         # 8-byte Reload
	movq	176(%rsp), %rcx         # 8-byte Reload
	movq	320(%rsp), %r8          # 8-byte Reload
	movq	160(%rsp), %r9          # 8-byte Reload
	movq	208(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	160(%rsp), %rdi         # 8-byte Reload
	movq	352(%rsp), %rsi         # 8-byte Reload
	movq	176(%rsp), %rdx         # 8-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	344(%rsp), %r8          # 8-byte Reload
	movq	160(%rsp), %r9          # 8-byte Reload
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	128(%rsp), %rdi         # 8-byte Reload
	movq	352(%rsp), %rsi         # 8-byte Reload
	movq	392(%rsp), %rdx         # 8-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	328(%rsp), %r8          # 8-byte Reload
	movq	176(%rsp), %r9          # 8-byte Reload
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	288(%rsp), %rdi         # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	144(%rsp), %rdx         # 8-byte Reload
	movq	232(%rsp), %rcx         # 8-byte Reload
	movq	256(%rsp), %r8          # 8-byte Reload
	movl	108(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	152(%rsp), %rdi         # 8-byte Reload
	movq	352(%rsp), %rsi         # 8-byte Reload
	movq	392(%rsp), %rdx         # 8-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	328(%rsp), %r8          # 8-byte Reload
	movq	176(%rsp), %r9          # 8-byte Reload
	movq	184(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	160(%rsp), %rdi         # 8-byte Reload
	movq	352(%rsp), %rsi         # 8-byte Reload
	movq	520(%rsp), %rdx         # 8-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	216(%rsp), %r8          # 8-byte Reload
	movq	176(%rsp), %r9          # 8-byte Reload
	movq	184(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	392(%rsp), %rax         # 8-byte Reload
	shlq	$2, %rax
	movq	520(%rsp), %rcx         # 8-byte Reload
	addq	%rax, %rcx
	movq	128(%rsp), %rdi         # 8-byte Reload
	movq	352(%rsp), %rsi         # 8-byte Reload
	movq	%rcx, %rdx
	movq	160(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	%rax, %rcx
	movq	232(%rsp), %r8          # 8-byte Reload
	movq	176(%rsp), %r9          # 8-byte Reload
	movq	184(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	288(%rsp), %rdi         # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	144(%rsp), %rdx         # 8-byte Reload
	movq	248(%rsp), %rcx         # 8-byte Reload
	movq	224(%rsp), %r8          # 8-byte Reload
	movl	108(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	160(%rsp), %rdi         # 8-byte Reload
	movq	352(%rsp), %rsi         # 8-byte Reload
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	232(%rsp), %r8          # 8-byte Reload
	movq	176(%rsp), %r9          # 8-byte Reload
	movq	184(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	520(%rsp), %rax         # 8-byte Reload
	movq	392(%rsp), %rcx         # 8-byte Reload
	vmovss	(%rax,%rcx,4), %xmm0
	cvtss2sd	%xmm0, %xmm1
	movq	128(%rsp), %rdi         # 8-byte Reload
	movq	384(%rsp), %rsi         # 8-byte Reload
	vmovss	%xmm0, 52(%rsp)         # 4-byte Spill
	vmovaps	%xmm1, %xmm0
	movq	160(%rsp), %rdx         # 8-byte Reload
	movq	248(%rsp), %rcx         # 8-byte Reload
	movq	176(%rsp), %r8          # 8-byte Reload
	movq	184(%rsp), %r9          # 8-byte Reload
	callq	trace_logger_log_double
	movq	288(%rsp), %rdi         # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	144(%rsp), %rdx         # 8-byte Reload
	movq	304(%rsp), %rcx         # 8-byte Reload
	movq	240(%rsp), %r8          # 8-byte Reload
	movl	108(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	152(%rsp), %rdi         # 8-byte Reload
	movq	384(%rsp), %rsi         # 8-byte Reload
	vmovsd	112(%rsp), %xmm0        # 8-byte Reload
	movq	176(%rsp), %rdx         # 8-byte Reload
	movq	320(%rsp), %rcx         # 8-byte Reload
	movq	176(%rsp), %r8          # 8-byte Reload
	movq	184(%rsp), %r9          # 8-byte Reload
	callq	trace_logger_log_double
	vmovss	52(%rsp), %xmm0         # 4-byte Reload
	cvtss2sd	%xmm0, %xmm0
	movq	160(%rsp), %rdi         # 8-byte Reload
	movq	384(%rsp), %rsi         # 8-byte Reload
	movq	160(%rsp), %rdx         # 8-byte Reload
	movq	248(%rsp), %rcx         # 8-byte Reload
	movq	176(%rsp), %r8          # 8-byte Reload
	movq	184(%rsp), %r9          # 8-byte Reload
	callq	trace_logger_log_double
	vmovss	52(%rsp), %xmm0         # 4-byte Reload
	vmovss	124(%rsp), %xmm1        # 4-byte Reload
	vaddss	%xmm1, %xmm0, %xmm2
	cvtss2sd	%xmm2, %xmm0
	movq	128(%rsp), %rdi         # 8-byte Reload
	movq	384(%rsp), %rsi         # 8-byte Reload
	movq	160(%rsp), %rdx         # 8-byte Reload
	movq	304(%rsp), %rcx         # 8-byte Reload
	movq	176(%rsp), %r8          # 8-byte Reload
	movq	184(%rsp), %r9          # 8-byte Reload
	vmovss	%xmm2, 48(%rsp)         # 4-byte Spill
	callq	trace_logger_log_double
	movq	288(%rsp), %rdi         # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	144(%rsp), %rdx         # 8-byte Reload
	movq	296(%rsp), %rcx         # 8-byte Reload
	movq	256(%rsp), %r8          # 8-byte Reload
	movl	108(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	152(%rsp), %rdi         # 8-byte Reload
	movq	352(%rsp), %rsi         # 8-byte Reload
	movq	392(%rsp), %rdx         # 8-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	328(%rsp), %r8          # 8-byte Reload
	movq	176(%rsp), %r9          # 8-byte Reload
	movq	184(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	160(%rsp), %rdi         # 8-byte Reload
	movq	352(%rsp), %rsi         # 8-byte Reload
	movq	512(%rsp), %rdx         # 8-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	264(%rsp), %r8          # 8-byte Reload
	movq	176(%rsp), %r9          # 8-byte Reload
	movq	184(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	392(%rsp), %rax         # 8-byte Reload
	shlq	$2, %rax
	movq	512(%rsp), %rcx         # 8-byte Reload
	addq	%rax, %rcx
	movq	128(%rsp), %rdi         # 8-byte Reload
	movq	352(%rsp), %rsi         # 8-byte Reload
	movq	%rcx, %rdx
	movq	160(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rax, %rcx
	movq	296(%rsp), %r8          # 8-byte Reload
	movq	176(%rsp), %r9          # 8-byte Reload
	movq	184(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	288(%rsp), %rdi         # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	144(%rsp), %rdx         # 8-byte Reload
	movq	280(%rsp), %rcx         # 8-byte Reload
	movq	272(%rsp), %r8          # 8-byte Reload
	movl	108(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	152(%rsp), %rdi         # 8-byte Reload
	movq	352(%rsp), %rsi         # 8-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	296(%rsp), %r8          # 8-byte Reload
	movq	176(%rsp), %r9          # 8-byte Reload
	movq	184(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	vmovss	48(%rsp), %xmm0         # 4-byte Reload
	cvtss2sd	%xmm0, %xmm0
	movq	160(%rsp), %rdi         # 8-byte Reload
	movq	384(%rsp), %rsi         # 8-byte Reload
	movq	160(%rsp), %rdx         # 8-byte Reload
	movq	304(%rsp), %rcx         # 8-byte Reload
	movq	176(%rsp), %r8          # 8-byte Reload
	movq	184(%rsp), %r9          # 8-byte Reload
	callq	trace_logger_log_double
	movq	512(%rsp), %rax         # 8-byte Reload
	movq	392(%rsp), %rcx         # 8-byte Reload
	vmovss	48(%rsp), %xmm0         # 4-byte Reload
	vmovss	%xmm0, (%rax,%rcx,4)
	.loc	1 79 0                  # test_dma_load_store.c:79:0
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	144(%rsp), %rdx         # 8-byte Reload
	movq	344(%rsp), %rcx         # 8-byte Reload
	movq	312(%rsp), %r8          # 8-byte Reload
	movl	108(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	152(%rsp), %rdi         # 8-byte Reload
	movq	352(%rsp), %rsi         # 8-byte Reload
	movq	160(%rsp), %rdx         # 8-byte Reload
	movq	176(%rsp), %rcx         # 8-byte Reload
	movq	320(%rsp), %r8          # 8-byte Reload
	movq	176(%rsp), %r9          # 8-byte Reload
	movq	184(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	160(%rsp), %rdi         # 8-byte Reload
	movq	352(%rsp), %rsi         # 8-byte Reload
	movq	392(%rsp), %rdx         # 8-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	328(%rsp), %r8          # 8-byte Reload
	movq	176(%rsp), %r9          # 8-byte Reload
	movq	184(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	392(%rsp), %rax         # 8-byte Reload
	addq	$1, %rax
	movq	128(%rsp), %rdi         # 8-byte Reload
	movq	352(%rsp), %rsi         # 8-byte Reload
	movq	%rax, %rdx
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	344(%rsp), %r8          # 8-byte Reload
	movq	176(%rsp), %r9          # 8-byte Reload
	movq	184(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movq	%rax, 32(%rsp)          # 8-byte Spill
	callq	trace_logger_log_int
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	144(%rsp), %rdx         # 8-byte Reload
	movq	376(%rsp), %rcx         # 8-byte Reload
	movq	336(%rsp), %r8          # 8-byte Reload
	movl	108(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	160(%rsp), %rdi         # 8-byte Reload
	movq	352(%rsp), %rsi         # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	344(%rsp), %r8          # 8-byte Reload
	movq	176(%rsp), %r9          # 8-byte Reload
	movq	184(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ebp
	movl	%ebp, %r12d
	movl	%r12d, %edx
	movq	128(%rsp), %rdi         # 8-byte Reload
	movq	384(%rsp), %rsi         # 8-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	376(%rsp), %r8          # 8-byte Reload
	movq	176(%rsp), %r9          # 8-byte Reload
	movq	184(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movl	%ebp, 28(%rsp)          # 4-byte Spill
	callq	trace_logger_log_int
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	144(%rsp), %rdx         # 8-byte Reload
	movq	168(%rsp), %rcx         # 8-byte Reload
	movq	360(%rsp), %r8          # 8-byte Reload
	movl	108(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movl	612(%rsp), %r9d         # 4-byte Reload
	movl	%r9d, %ebp
	movl	%ebp, %edx
	movq	152(%rsp), %rdi         # 8-byte Reload
	movq	384(%rsp), %rsi         # 8-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	368(%rsp), %r8          # 8-byte Reload
	movq	176(%rsp), %r9          # 8-byte Reload
	movq	184(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	28(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %r12d
	movl	%r12d, %edx
	movq	160(%rsp), %rdi         # 8-byte Reload
	movq	384(%rsp), %rsi         # 8-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	376(%rsp), %r8          # 8-byte Reload
	movq	176(%rsp), %r9          # 8-byte Reload
	movq	184(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	28(%rsp), %ebp          # 4-byte Reload
	movl	612(%rsp), %r12d        # 4-byte Reload
	cmpl	%r12d, %ebp
	sete	%al
	movb	%al, %cl
	andb	$1, %cl
	movzbl	%cl, %ecx
	movl	%ecx, %edx
	movq	128(%rsp), %rdi         # 8-byte Reload
	movq	160(%rsp), %rsi         # 8-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	168(%rsp), %r8          # 8-byte Reload
	movq	176(%rsp), %r9          # 8-byte Reload
	movq	184(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movb	%al, 27(%rsp)           # 1-byte Spill
	callq	trace_logger_log_int
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	144(%rsp), %rdx         # 8-byte Reload
	movq	96(%rsp), %rcx          # 8-byte Reload
	movq	152(%rsp), %r8          # 8-byte Reload
	movl	108(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	88(%rsp), %rdi          # 8-byte Reload
	movq	176(%rsp), %rsi         # 8-byte Reload
	movq	176(%rsp), %rdx         # 8-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	80(%rsp), %r8           # 8-byte Reload
	movq	176(%rsp), %r9          # 8-byte Reload
	movq	184(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	152(%rsp), %rdi         # 8-byte Reload
	movq	176(%rsp), %rsi         # 8-byte Reload
	movq	176(%rsp), %rdx         # 8-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	144(%rsp), %r8          # 8-byte Reload
	movq	176(%rsp), %r9          # 8-byte Reload
	movq	184(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	27(%rsp), %al           # 1-byte Reload
	andb	$1, %al
	movzbl	%al, %ebp
	movl	%ebp, %edx
	movq	160(%rsp), %rdi         # 8-byte Reload
	movq	160(%rsp), %rsi         # 8-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	168(%rsp), %r8          # 8-byte Reload
	movq	176(%rsp), %r9          # 8-byte Reload
	movq	184(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	27(%rsp), %al           # 1-byte Reload
	testb	$1, %al
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 400(%rsp)         # 8-byte Spill
	jne	.LBB1_2
	jmp	.LBB1_1
.Ltmp53:
.LBB1_2:                                # %._crit_edge
	movabsq	$87, %rdi
	leaq	.L.str45, %rsi
	leaq	.L.str15, %rdx
	leaq	.L.str44, %rcx
	movabsq	$1, %r8
	movl	$0, %r9d
	movl	$1, %eax
	.loc	1 87 0                  # test_dma_load_store.c:87:0
	movl	$1, (%rsp)
	movl	%eax, 20(%rsp)          # 4-byte Spill
	callq	trace_logger_log0
	addq	$616, %rsp              # imm = 0x268
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.Ltmp54:
.Ltmp55:
	.size	s1, .Ltmp55-s1
.Lfunc_end1:
	.cfi_endproc

	.globl	big
	.align	16, 0x90
	.type	big,@function
big:                                    # @big
	.cfi_startproc
.Lfunc_begin2:
	.loc	1 90 0                  # test_dma_load_store.c:90:0
# BB#0:
	pushq	%rbp
.Ltmp63:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Ltmp64:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Ltmp65:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Ltmp66:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Ltmp67:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Ltmp68:
	.cfi_def_cfa_offset 56
	subq	$2504, %rsp             # imm = 0x9C8
.Ltmp69:
	.cfi_def_cfa_offset 2560
.Ltmp70:
	.cfi_offset %rbx, -56
.Ltmp71:
	.cfi_offset %r12, -48
.Ltmp72:
	.cfi_offset %r13, -40
.Ltmp73:
	.cfi_offset %r14, -32
.Ltmp74:
	.cfi_offset %r15, -24
.Ltmp75:
	.cfi_offset %rbp, -16
	movabsq	$3, %rax
	movabsq	$64, %rcx
	movabsq	$1, %r8
	leaq	.L.str23, %r9
	movabsq	$0, %r10
	leaq	.L.str11, %r11
	movabsq	$2, %rbx
	leaq	.L.str25, %r14
	movabsq	$4, %r15
	leaq	dmaLoad, %r12
	leaq	.L.str49, %r13
	movabsq	$93, %rbp
	movq	%rax, 2496(%rsp)        # 8-byte Spill
	leaq	.L.str48, %rax
	movq	%rax, 2488(%rsp)        # 8-byte Spill
	leaq	.L.str8, %rax
	movq	%rax, 2480(%rsp)        # 8-byte Spill
	leaq	.L.str24, %rax
	movq	%rax, 2472(%rsp)        # 8-byte Spill
	movabsq	$99, %rax
	movl	%edx, 2468(%rsp)        # 4-byte Spill
	movl	$1, %edx
	movq	%rax, 2456(%rsp)        # 8-byte Spill
	movabsq	$19134, %rax            # imm = 0x4ABE
	movq	%rax, 2448(%rsp)        # 8-byte Spill
	leaq	.L.str21, %rax
	movq	%rax, 2440(%rsp)        # 8-byte Spill
	leaq	.L.str10, %rax
	movq	%rax, 2432(%rsp)        # 8-byte Spill
	movabsq	$20, %rax
	movq	%rax, 2424(%rsp)        # 8-byte Spill
	movabsq	$32, %rax
	movq	%rax, 2416(%rsp)        # 8-byte Spill
	leaq	.L.str12, %rax
	movq	%rax, 2408(%rsp)        # 8-byte Spill
	movabsq	$35, %rax
	movq	%rax, 2400(%rsp)        # 8-byte Spill
	leaq	.L.str9, %rax
	movq	%rax, 2392(%rsp)        # 8-byte Spill
	movabsq	$44, %rax
	movq	%rax, 2384(%rsp)        # 8-byte Spill
	leaq	.L.str22, %rax
	.loc	1 90 0 prologue_end     # test_dma_load_store.c:90:0
.Ltmp76:
	movq	%rax, 2376(%rsp)        # 8-byte Spill
	movq	2488(%rsp), %rax        # 8-byte Reload
	movq	%rdi, 2368(%rsp)        # 8-byte Spill
	movq	%rax, %rdi
	movq	2496(%rsp), %rax        # 8-byte Reload
	movq	%rsi, 2360(%rsp)        # 8-byte Spill
	movq	%rax, %rsi
	movq	%r13, 2352(%rsp)        # 8-byte Spill
	movq	%rbp, 2344(%rsp)        # 8-byte Spill
	movl	%edx, 2340(%rsp)        # 4-byte Spill
	movq	%rcx, 2328(%rsp)        # 8-byte Spill
	movq	%r8, 2320(%rsp)         # 8-byte Spill
	movq	%r9, 2312(%rsp)         # 8-byte Spill
	movq	%r10, 2304(%rsp)        # 8-byte Spill
	movq	%r11, 2296(%rsp)        # 8-byte Spill
	movq	%rbx, 2288(%rsp)        # 8-byte Spill
	movq	%r14, 2280(%rsp)        # 8-byte Spill
	movq	%r15, 2272(%rsp)        # 8-byte Spill
	movq	%r12, 2264(%rsp)        # 8-byte Spill
	callq	trace_logger_log_entry
	movq	2320(%rsp), %rdi        # 8-byte Reload
	movq	2328(%rsp), %rsi        # 8-byte Reload
	movq	2368(%rsp), %rdx        # 8-byte Reload
	movq	2320(%rsp), %rcx        # 8-byte Reload
	movq	2280(%rsp), %r8         # 8-byte Reload
	movq	2304(%rsp), %r9         # 8-byte Reload
	movq	2296(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	2288(%rsp), %rdi        # 8-byte Reload
	movq	2328(%rsp), %rsi        # 8-byte Reload
	movq	2360(%rsp), %rdx        # 8-byte Reload
	movq	2320(%rsp), %rcx        # 8-byte Reload
	movq	2376(%rsp), %r8         # 8-byte Reload
	movq	2304(%rsp), %r9         # 8-byte Reload
	movq	2296(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movl	2468(%rsp), %esi        # 4-byte Reload
	movl	%esi, %esi
	movl	%esi, %edx
	movq	2496(%rsp), %rdi        # 8-byte Reload
	movq	2416(%rsp), %rsi        # 8-byte Reload
	movq	2320(%rsp), %rcx        # 8-byte Reload
	movq	2408(%rsp), %r8         # 8-byte Reload
	movq	2304(%rsp), %r9         # 8-byte Reload
	movq	2296(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	#DEBUG_VALUE: big:store_vals <- [RSP+2368]
	#DEBUG_VALUE: big:store_loc <- [RSP+2360]
	#DEBUG_VALUE: big:num_vals <- [RSP+2468]
	.loc	1 93 0                  # test_dma_load_store.c:93:0
	movq	2344(%rsp), %rdi        # 8-byte Reload
	movq	2488(%rsp), %rsi        # 8-byte Reload
	movq	2480(%rsp), %rdx        # 8-byte Reload
	movq	2392(%rsp), %rcx        # 8-byte Reload
	movq	2384(%rsp), %r8         # 8-byte Reload
	movl	2340(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	2320(%rsp), %rdi        # 8-byte Reload
	movq	2328(%rsp), %rsi        # 8-byte Reload
	movq	2368(%rsp), %rdx        # 8-byte Reload
	movq	2320(%rsp), %rcx        # 8-byte Reload
	movq	2280(%rsp), %r8         # 8-byte Reload
	movq	2304(%rsp), %r9         # 8-byte Reload
	movq	2296(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	2368(%rsp), %rax        # 8-byte Reload
	movq	2448(%rsp), %rdi        # 8-byte Reload
	movq	2328(%rsp), %rsi        # 8-byte Reload
	movq	%rax, %rdx
	movq	2320(%rsp), %rcx        # 8-byte Reload
	movq	2280(%rsp), %r8         # 8-byte Reload
	movq	2304(%rsp), %r9         # 8-byte Reload
	movq	2296(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	movq	%rax, 2256(%rsp)        # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	2344(%rsp), %rdi        # 8-byte Reload
	movq	2488(%rsp), %rsi        # 8-byte Reload
	movq	2480(%rsp), %rdx        # 8-byte Reload
	movq	2440(%rsp), %rcx        # 8-byte Reload
	movq	2400(%rsp), %r8         # 8-byte Reload
	movl	2340(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movl	2468(%rsp), %r9d        # 4-byte Reload
	movl	%r9d, %esi
	movl	%esi, %edx
	movq	2320(%rsp), %rdi        # 8-byte Reload
	movq	2416(%rsp), %rsi        # 8-byte Reload
	movq	2320(%rsp), %rcx        # 8-byte Reload
	movq	2408(%rsp), %r8         # 8-byte Reload
	movq	2304(%rsp), %r9         # 8-byte Reload
	movq	2296(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	2468(%rsp), %esi        # 4-byte Reload
	movslq	%esi, %rax
	movq	2448(%rsp), %rdi        # 8-byte Reload
	movq	2328(%rsp), %rsi        # 8-byte Reload
	movq	%rax, %rdx
	movq	2320(%rsp), %rcx        # 8-byte Reload
	movq	2440(%rsp), %r8         # 8-byte Reload
	movq	2304(%rsp), %r9         # 8-byte Reload
	movq	2296(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	movq	%rax, 2248(%rsp)        # 8-byte Spill
	callq	trace_logger_log_int
	movq	2344(%rsp), %rdi        # 8-byte Reload
	movq	2488(%rsp), %rsi        # 8-byte Reload
	movq	2480(%rsp), %rdx        # 8-byte Reload
	movq	2312(%rsp), %rcx        # 8-byte Reload
	movq	2424(%rsp), %r8         # 8-byte Reload
	movl	2340(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	2288(%rsp), %rdi        # 8-byte Reload
	movq	2328(%rsp), %rsi        # 8-byte Reload
	movq	2288(%rsp), %rdx        # 8-byte Reload
	movq	2304(%rsp), %rcx        # 8-byte Reload
	movq	2432(%rsp), %r8         # 8-byte Reload
	movq	2304(%rsp), %r9         # 8-byte Reload
	movq	2296(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	2320(%rsp), %rdi        # 8-byte Reload
	movq	2328(%rsp), %rsi        # 8-byte Reload
	movq	2248(%rsp), %rdx        # 8-byte Reload
	movq	2320(%rsp), %rcx        # 8-byte Reload
	movq	2440(%rsp), %r8         # 8-byte Reload
	movq	2304(%rsp), %r9         # 8-byte Reload
	movq	2296(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	2248(%rsp), %rax        # 8-byte Reload
	shlq	$2, %rax
	movq	2448(%rsp), %rdi        # 8-byte Reload
	movq	2328(%rsp), %rsi        # 8-byte Reload
	movq	%rax, %rdx
	movq	2320(%rsp), %rcx        # 8-byte Reload
	movq	2312(%rsp), %r8         # 8-byte Reload
	movq	2304(%rsp), %r9         # 8-byte Reload
	movq	2296(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	movq	%rax, 2240(%rsp)        # 8-byte Spill
	callq	trace_logger_log_int
	movq	2344(%rsp), %rdi        # 8-byte Reload
	movq	2488(%rsp), %rsi        # 8-byte Reload
	movq	2480(%rsp), %rdx        # 8-byte Reload
	movq	2472(%rsp), %rcx        # 8-byte Reload
	movq	2456(%rsp), %r8         # 8-byte Reload
	movl	2340(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	2272(%rsp), %rdi        # 8-byte Reload
	movq	2328(%rsp), %rsi        # 8-byte Reload
	movq	2264(%rsp), %rdx        # 8-byte Reload
	movq	2320(%rsp), %rcx        # 8-byte Reload
	movq	2352(%rsp), %r8         # 8-byte Reload
	movq	2304(%rsp), %r9         # 8-byte Reload
	movq	2296(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	2320(%rsp), %rdi        # 8-byte Reload
	movq	2328(%rsp), %rsi        # 8-byte Reload
	movq	2256(%rsp), %rdx        # 8-byte Reload
	movq	2320(%rsp), %rcx        # 8-byte Reload
	movq	2280(%rsp), %r8         # 8-byte Reload
	movq	2304(%rsp), %r9         # 8-byte Reload
	movq	2296(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	2288(%rsp), %rdi        # 8-byte Reload
	movq	2328(%rsp), %rsi        # 8-byte Reload
	movq	2256(%rsp), %rdx        # 8-byte Reload
	movq	2320(%rsp), %rcx        # 8-byte Reload
	movq	2280(%rsp), %r8         # 8-byte Reload
	movq	2304(%rsp), %r9         # 8-byte Reload
	movq	2296(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	2496(%rsp), %rdi        # 8-byte Reload
	movq	2328(%rsp), %rsi        # 8-byte Reload
	movq	2240(%rsp), %rdx        # 8-byte Reload
	movq	2320(%rsp), %rcx        # 8-byte Reload
	movq	2312(%rsp), %r8         # 8-byte Reload
	movq	2304(%rsp), %r9         # 8-byte Reload
	movq	2296(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	2256(%rsp), %rdi        # 8-byte Reload
	movq	2256(%rsp), %rsi        # 8-byte Reload
	movq	2240(%rsp), %rdx        # 8-byte Reload
	callq	dmaLoad
.Ltmp77:
	movabsq	$3, %rdi
	movabsq	$64, %rcx
	movabsq	$1, %rdx
	leaq	.L.str23, %r8
	movabsq	$0, %rsi
	leaq	.L.str11, %r9
	movabsq	$2, %r10
	leaq	.L.str22, %r11
	movabsq	$4, %rbx
	leaq	dmaLoad, %r14
	leaq	.L.str49, %r15
	movabsq	$94, %r12
	leaq	.L.str48, %r13
	leaq	.L.str8, %rbp
	movq	%rcx, 2232(%rsp)        # 8-byte Spill
	leaq	.L.str27, %rcx
	movq	%rcx, 2224(%rsp)        # 8-byte Spill
	movabsq	$99, %rcx
	movl	%eax, 2220(%rsp)        # 4-byte Spill
	movl	$1, %eax
	movq	%rcx, 2208(%rsp)        # 8-byte Spill
	movabsq	$19134, %rcx            # imm = 0x4ABE
	movq	%rcx, 2200(%rsp)        # 8-byte Spill
	leaq	.L.str26, %rcx
	movq	%rcx, 2192(%rsp)        # 8-byte Spill
	movabsq	$44, %rcx
	movq	%rcx, 2184(%rsp)        # 8-byte Spill
	movabsq	$32, %rcx
	movq	%rcx, 2176(%rsp)        # 8-byte Spill
	leaq	.L.str24, %rcx
	.loc	1 94 0                  # test_dma_load_store.c:94:0
	movl	%eax, 2172(%rsp)        # 4-byte Spill
	movl	2220(%rsp), %eax        # 4-byte Reload
	movl	%eax, %eax
                                        # kill: RAX<def> EAX<kill>
	movq	%rax, 2160(%rsp)        # 8-byte Spill
	movq	2200(%rsp), %rax        # 8-byte Reload
	movq	%rdi, 2152(%rsp)        # 8-byte Spill
	movq	%rax, %rdi
	movq	2176(%rsp), %rax        # 8-byte Reload
	movq	%rsi, 2144(%rsp)        # 8-byte Spill
	movq	%rax, %rsi
	movq	2160(%rsp), %rax        # 8-byte Reload
	movq	%rdx, 2136(%rsp)        # 8-byte Spill
	movq	%rax, %rdx
	movq	2136(%rsp), %rax        # 8-byte Reload
	movq	%rcx, 2128(%rsp)        # 8-byte Spill
	movq	%rax, %rcx
	movq	2128(%rsp), %rax        # 8-byte Reload
	movq	%r8, 2120(%rsp)         # 8-byte Spill
	movq	%rax, %r8
	movq	2144(%rsp), %rax        # 8-byte Reload
	movq	%r9, 2112(%rsp)         # 8-byte Spill
	movq	%rax, %r9
	movq	2112(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	movq	%rbp, 2104(%rsp)        # 8-byte Spill
	movq	%r15, 2096(%rsp)        # 8-byte Spill
	movq	%r12, 2088(%rsp)        # 8-byte Spill
	movq	%r14, 2080(%rsp)        # 8-byte Spill
	movq	%r13, 2072(%rsp)        # 8-byte Spill
	movq	%rbx, 2064(%rsp)        # 8-byte Spill
	movq	%r10, 2056(%rsp)        # 8-byte Spill
	movq	%r11, 2048(%rsp)        # 8-byte Spill
	callq	trace_logger_log_int
	movq	2088(%rsp), %rdi        # 8-byte Reload
	movq	2072(%rsp), %rsi        # 8-byte Reload
	movq	2104(%rsp), %rdx        # 8-byte Reload
	movq	2192(%rsp), %rcx        # 8-byte Reload
	movq	2184(%rsp), %r8         # 8-byte Reload
	movl	2172(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	2136(%rsp), %rdi        # 8-byte Reload
	movq	2232(%rsp), %rsi        # 8-byte Reload
	movq	2360(%rsp), %rdx        # 8-byte Reload
	movq	2136(%rsp), %rcx        # 8-byte Reload
	movq	2048(%rsp), %r8         # 8-byte Reload
	movq	2144(%rsp), %r9         # 8-byte Reload
	movq	2112(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	2360(%rsp), %rax        # 8-byte Reload
	movq	2200(%rsp), %rdi        # 8-byte Reload
	movq	2232(%rsp), %rsi        # 8-byte Reload
	movq	%rax, %rdx
	movq	2136(%rsp), %rcx        # 8-byte Reload
	movq	2048(%rsp), %r8         # 8-byte Reload
	movq	2144(%rsp), %r9         # 8-byte Reload
	movq	2112(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	movq	%rax, 2040(%rsp)        # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	2088(%rsp), %rdi        # 8-byte Reload
	movq	2072(%rsp), %rsi        # 8-byte Reload
	movq	2104(%rsp), %rdx        # 8-byte Reload
	movq	2224(%rsp), %rcx        # 8-byte Reload
	movq	2208(%rsp), %r8         # 8-byte Reload
	movl	2172(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	2064(%rsp), %rdi        # 8-byte Reload
	movq	2232(%rsp), %rsi        # 8-byte Reload
	movq	2080(%rsp), %rdx        # 8-byte Reload
	movq	2136(%rsp), %rcx        # 8-byte Reload
	movq	2096(%rsp), %r8         # 8-byte Reload
	movq	2144(%rsp), %r9         # 8-byte Reload
	movq	2112(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	2136(%rsp), %rdi        # 8-byte Reload
	movq	2232(%rsp), %rsi        # 8-byte Reload
	movq	2040(%rsp), %rdx        # 8-byte Reload
	movq	2136(%rsp), %rcx        # 8-byte Reload
	movq	2048(%rsp), %r8         # 8-byte Reload
	movq	2144(%rsp), %r9         # 8-byte Reload
	movq	2112(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	2056(%rsp), %rdi        # 8-byte Reload
	movq	2232(%rsp), %rsi        # 8-byte Reload
	movq	2040(%rsp), %rdx        # 8-byte Reload
	movq	2136(%rsp), %rcx        # 8-byte Reload
	movq	2048(%rsp), %r8         # 8-byte Reload
	movq	2144(%rsp), %r9         # 8-byte Reload
	movq	2112(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	2152(%rsp), %rdi        # 8-byte Reload
	movq	2232(%rsp), %rsi        # 8-byte Reload
	movq	2240(%rsp), %rdx        # 8-byte Reload
	movq	2136(%rsp), %rcx        # 8-byte Reload
	movq	2120(%rsp), %r8         # 8-byte Reload
	movq	2144(%rsp), %r9         # 8-byte Reload
	movq	2112(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	2040(%rsp), %rdi        # 8-byte Reload
	movq	2040(%rsp), %rsi        # 8-byte Reload
	movq	2240(%rsp), %rdx        # 8-byte Reload
	callq	dmaLoad
	movabsq	$3, %rdi
	movabsq	$32, %rcx
	movabsq	$1, %rdx
	leaq	.L.str12, %r8
	movabsq	$0, %rsi
	leaq	.L.str11, %r9
	movabsq	$2, %r10
	movabsq	$64, %r11
	leaq	.L.str22, %rbx
	leaq	.L.str25, %r14
	movabsq	$4, %r15
	leaq	s1, %r12
	leaq	.L.str45, %r13
	movabsq	$116, %rbp
	movq	%rcx, 2032(%rsp)        # 8-byte Spill
	leaq	.L.str48, %rcx
	movq	%rcx, 2024(%rsp)        # 8-byte Spill
	leaq	.L.str8, %rcx
	movq	%rcx, 2016(%rsp)        # 8-byte Spill
	leaq	.L.str50, %rcx
	movq	%rcx, 2008(%rsp)        # 8-byte Spill
	movabsq	$49, %rcx
	movl	%eax, 2004(%rsp)        # 4-byte Spill
	movl	$1, %eax
	movq	%rcx, 1992(%rsp)        # 8-byte Spill
	movabsq	$19134, %rcx            # imm = 0x4ABE
	movq	%rcx, 1984(%rsp)        # 8-byte Spill
	leaq	.L.str27, %rcx
	.loc	1 116 0                 # test_dma_load_store.c:116:0
	movl	%eax, 1980(%rsp)        # 4-byte Spill
	movl	2004(%rsp), %eax        # 4-byte Reload
	movl	%eax, %eax
                                        # kill: RAX<def> EAX<kill>
	movq	%rax, 1968(%rsp)        # 8-byte Spill
	movq	1984(%rsp), %rax        # 8-byte Reload
	movq	%rdi, 1960(%rsp)        # 8-byte Spill
	movq	%rax, %rdi
	movq	2032(%rsp), %rax        # 8-byte Reload
	movq	%rsi, 1952(%rsp)        # 8-byte Spill
	movq	%rax, %rsi
	movq	1968(%rsp), %rax        # 8-byte Reload
	movq	%rdx, 1944(%rsp)        # 8-byte Spill
	movq	%rax, %rdx
	movq	1944(%rsp), %rax        # 8-byte Reload
	movq	%rcx, 1936(%rsp)        # 8-byte Spill
	movq	%rax, %rcx
	movq	1936(%rsp), %rax        # 8-byte Reload
	movq	%r8, 1928(%rsp)         # 8-byte Spill
	movq	%rax, %r8
	movq	1952(%rsp), %rax        # 8-byte Reload
	movq	%r9, 1920(%rsp)         # 8-byte Spill
	movq	%rax, %r9
	movq	1920(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	movq	%rbp, 1912(%rsp)        # 8-byte Spill
	movq	%r15, 1904(%rsp)        # 8-byte Spill
	movq	%r12, 1896(%rsp)        # 8-byte Spill
	movq	%r14, 1888(%rsp)        # 8-byte Spill
	movq	%r13, 1880(%rsp)        # 8-byte Spill
	movq	%rbx, 1872(%rsp)        # 8-byte Spill
	movq	%r10, 1864(%rsp)        # 8-byte Spill
	movq	%r11, 1856(%rsp)        # 8-byte Spill
	callq	trace_logger_log_int
	movq	1912(%rsp), %rdi        # 8-byte Reload
	movq	2024(%rsp), %rsi        # 8-byte Reload
	movq	2016(%rsp), %rdx        # 8-byte Reload
	movq	2008(%rsp), %rcx        # 8-byte Reload
	movq	1992(%rsp), %r8         # 8-byte Reload
	movl	1980(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1904(%rsp), %rdi        # 8-byte Reload
	movq	1856(%rsp), %rsi        # 8-byte Reload
	movq	1896(%rsp), %rdx        # 8-byte Reload
	movq	1944(%rsp), %rcx        # 8-byte Reload
	movq	1880(%rsp), %r8         # 8-byte Reload
	movq	1952(%rsp), %r9         # 8-byte Reload
	movq	1920(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	1944(%rsp), %rdi        # 8-byte Reload
	movq	1856(%rsp), %rsi        # 8-byte Reload
	movq	2368(%rsp), %rdx        # 8-byte Reload
	movq	1944(%rsp), %rcx        # 8-byte Reload
	movq	1888(%rsp), %r8         # 8-byte Reload
	movq	1952(%rsp), %r9         # 8-byte Reload
	movq	1920(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	1864(%rsp), %rdi        # 8-byte Reload
	movq	1856(%rsp), %rsi        # 8-byte Reload
	movq	2360(%rsp), %rdx        # 8-byte Reload
	movq	1944(%rsp), %rcx        # 8-byte Reload
	movq	1872(%rsp), %r8         # 8-byte Reload
	movq	1952(%rsp), %r9         # 8-byte Reload
	movq	1920(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movl	2468(%rsp), %esi        # 4-byte Reload
	movl	%esi, %esi
	movl	%esi, %edx
	movq	1960(%rsp), %rdi        # 8-byte Reload
	movq	2032(%rsp), %rsi        # 8-byte Reload
	movq	1944(%rsp), %rcx        # 8-byte Reload
	movq	1928(%rsp), %r8         # 8-byte Reload
	movq	1952(%rsp), %r9         # 8-byte Reload
	movq	1920(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	2368(%rsp), %rdi        # 8-byte Reload
	movq	2360(%rsp), %rsi        # 8-byte Reload
	movl	2468(%rsp), %edx        # 4-byte Reload
	callq	s1
	movabsq	$0, %rax
	movabsq	$1, %rcx
	leaq	.L.str28, %rsi
	leaq	.L.str11, %rdi
	movabsq	$2, %r8
	leaq	.L.str52, %r9
	movabsq	$3, %r10
	leaq	.L.str51, %r11
	movabsq	$121, %rbx
	leaq	.L.str48, %r14
	leaq	.L.str8, %r15
	leaq	.L.str13, %r12
	movl	$1, %edx
	movabsq	$19134, %r13            # imm = 0x4ABE
	movabsq	$32, %rbp
	movq	%rax, 1848(%rsp)        # 8-byte Spill
	leaq	.L.str12, %rax
	movq	%rax, 1840(%rsp)        # 8-byte Spill
	leaq	.L.str10, %rax
	movq	%rax, 1832(%rsp)        # 8-byte Spill
	movabsq	$46, %rax
.Ltmp78:
	#DEBUG_VALUE: i <- 0
	.loc	1 121 0                 # test_dma_load_store.c:121:0
	movq	%rdi, 1824(%rsp)        # 8-byte Spill
	movq	%rbx, %rdi
	movq	%rsi, 1816(%rsp)        # 8-byte Spill
	movq	%r14, %rsi
	movl	%edx, 1812(%rsp)        # 4-byte Spill
	movq	%r15, %rdx
	movq	%rax, 1800(%rsp)        # 8-byte Spill
	movq	1816(%rsp), %rax        # 8-byte Reload
	movq	%rcx, 1792(%rsp)        # 8-byte Spill
	movq	%rax, %rcx
	movq	1800(%rsp), %rax        # 8-byte Reload
	movq	%r8, 1784(%rsp)         # 8-byte Spill
	movq	%rax, %r8
	movl	1812(%rsp), %eax        # 4-byte Reload
	movq	%r9, 1776(%rsp)         # 8-byte Spill
	movl	%eax, %r9d
	movl	$1, (%rsp)
	movq	%r12, 1768(%rsp)        # 8-byte Spill
	movq	%r13, 1760(%rsp)        # 8-byte Spill
	movq	%rbp, 1752(%rsp)        # 8-byte Spill
	movq	%r15, 1744(%rsp)        # 8-byte Spill
	movq	%r14, 1736(%rsp)        # 8-byte Spill
	movq	%r10, 1728(%rsp)        # 8-byte Spill
	movq	%r11, 1720(%rsp)        # 8-byte Spill
	movq	%rbx, 1712(%rsp)        # 8-byte Spill
	callq	trace_logger_log0
	movq	1784(%rsp), %rdi        # 8-byte Reload
	movq	1752(%rsp), %rsi        # 8-byte Reload
	movq	1848(%rsp), %rdx        # 8-byte Reload
	movq	1848(%rsp), %rcx        # 8-byte Reload
	movq	1832(%rsp), %r8         # 8-byte Reload
	movq	1848(%rsp), %r9         # 8-byte Reload
	movq	1824(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movl	2468(%rsp), %eax        # 4-byte Reload
	movl	%eax, %eax
	movl	%eax, %edx
	movq	1792(%rsp), %rdi        # 8-byte Reload
	movq	1752(%rsp), %rsi        # 8-byte Reload
	movq	1792(%rsp), %rcx        # 8-byte Reload
	movq	1840(%rsp), %r8         # 8-byte Reload
	movq	1848(%rsp), %r9         # 8-byte Reload
	movq	1824(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movl	2468(%rsp), %eax        # 4-byte Reload
	cmpl	$0, %eax
	setg	%al
	movb	%al, %cl
	andb	$1, %cl
	movzbl	%cl, %ecx
	movl	%ecx, %edx
	movq	1760(%rsp), %rdi        # 8-byte Reload
	movq	1792(%rsp), %rsi        # 8-byte Reload
	movq	1792(%rsp), %rcx        # 8-byte Reload
	movq	1816(%rsp), %r8         # 8-byte Reload
	movq	1848(%rsp), %r9         # 8-byte Reload
	movq	1824(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	movb	%al, 1711(%rsp)         # 1-byte Spill
	callq	trace_logger_log_int
	movq	1712(%rsp), %rdi        # 8-byte Reload
	movq	1736(%rsp), %rsi        # 8-byte Reload
	movq	1744(%rsp), %rdx        # 8-byte Reload
	movq	1768(%rsp), %rcx        # 8-byte Reload
	movq	1784(%rsp), %r8         # 8-byte Reload
	movl	1812(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1728(%rsp), %rdi        # 8-byte Reload
	movq	1848(%rsp), %rsi        # 8-byte Reload
	movq	1848(%rsp), %rdx        # 8-byte Reload
	movq	1792(%rsp), %rcx        # 8-byte Reload
	movq	1720(%rsp), %r8         # 8-byte Reload
	movq	1848(%rsp), %r9         # 8-byte Reload
	movq	1824(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	1784(%rsp), %rdi        # 8-byte Reload
	movq	1848(%rsp), %rsi        # 8-byte Reload
	movq	1848(%rsp), %rdx        # 8-byte Reload
	movq	1792(%rsp), %rcx        # 8-byte Reload
	movq	1776(%rsp), %r8         # 8-byte Reload
	movq	1848(%rsp), %r9         # 8-byte Reload
	movq	1824(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	1711(%rsp), %al         # 1-byte Reload
	andb	$1, %al
	movzbl	%al, %eax
	movl	%eax, %edx
	movq	1792(%rsp), %rdi        # 8-byte Reload
	movq	1792(%rsp), %rsi        # 8-byte Reload
	movq	1792(%rsp), %rcx        # 8-byte Reload
	movq	1816(%rsp), %r8         # 8-byte Reload
	movq	1848(%rsp), %r9         # 8-byte Reload
	movq	1824(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	1711(%rsp), %al         # 1-byte Reload
	testb	$1, %al
	movq	1848(%rsp), %rcx        # 8-byte Reload
	movq	%rcx, 1696(%rsp)        # 8-byte Spill
	jne	.LBB2_2
.Ltmp79:
# BB#1:                                 # %._crit_edge6.thread
	movabsq	$3, %rdi
	movabsq	$32, %rsi
	movabsq	$1, %rax
	leaq	.L.str12, %r8
	movabsq	$0, %rcx
	leaq	.L.str11, %rdx
	movabsq	$2, %r9
	movabsq	$64, %r10
	leaq	.L.str22, %r11
	leaq	.L.str25, %rbx
	movabsq	$4, %r14
	leaq	s1, %r15
	leaq	.L.str45, %r12
	movabsq	$138, %r13
	leaq	.L.str48, %rbp
	movq	%rax, 1688(%rsp)        # 8-byte Spill
	leaq	.L.str52, %rax
	movq	%rax, 1680(%rsp)        # 8-byte Spill
	leaq	.L.str53, %rax
	movq	%rax, 1672(%rsp)        # 8-byte Spill
	movabsq	$49, %rax
	movq	%rsi, 1664(%rsp)        # 8-byte Spill
	movl	$1, %esi
	.loc	1 138 0                 # test_dma_load_store.c:138:0
	movq	%rdi, 1656(%rsp)        # 8-byte Spill
	movq	%r13, %rdi
	movl	%esi, 1652(%rsp)        # 4-byte Spill
	movq	%rbp, %rsi
	movq	1680(%rsp), %r13        # 8-byte Reload
	movq	%rdx, 1640(%rsp)        # 8-byte Spill
	movq	%r13, %rdx
	movq	1672(%rsp), %rbp        # 8-byte Reload
	movq	%rcx, 1632(%rsp)        # 8-byte Spill
	movq	%rbp, %rcx
	movq	%r8, 1624(%rsp)         # 8-byte Spill
	movq	%rax, %r8
	movl	1652(%rsp), %ebp        # 4-byte Reload
	movq	%r9, 1616(%rsp)         # 8-byte Spill
	movl	%ebp, %r9d
	movl	$1, (%rsp)
	movq	%r12, 1608(%rsp)        # 8-byte Spill
	movq	%r15, 1600(%rsp)        # 8-byte Spill
	movq	%r14, 1592(%rsp)        # 8-byte Spill
	movq	%rbx, 1584(%rsp)        # 8-byte Spill
	movq	%r11, 1576(%rsp)        # 8-byte Spill
	movq	%r10, 1568(%rsp)        # 8-byte Spill
	callq	trace_logger_log0
	movq	1592(%rsp), %rdi        # 8-byte Reload
	movq	1568(%rsp), %rsi        # 8-byte Reload
	movq	1600(%rsp), %rdx        # 8-byte Reload
	movq	1688(%rsp), %rcx        # 8-byte Reload
	movq	1608(%rsp), %r8         # 8-byte Reload
	movq	1632(%rsp), %r9         # 8-byte Reload
	movq	1640(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	1688(%rsp), %rdi        # 8-byte Reload
	movq	1568(%rsp), %rsi        # 8-byte Reload
	movq	2368(%rsp), %rdx        # 8-byte Reload
	movq	1688(%rsp), %rcx        # 8-byte Reload
	movq	1584(%rsp), %r8         # 8-byte Reload
	movq	1632(%rsp), %r9         # 8-byte Reload
	movq	1640(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	1616(%rsp), %rdi        # 8-byte Reload
	movq	1568(%rsp), %rsi        # 8-byte Reload
	movq	2360(%rsp), %rdx        # 8-byte Reload
	movq	1688(%rsp), %rcx        # 8-byte Reload
	movq	1576(%rsp), %r8         # 8-byte Reload
	movq	1632(%rsp), %r9         # 8-byte Reload
	movq	1640(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movl	2468(%rsp), %ebp        # 4-byte Reload
	movl	%ebp, %esi
	movl	%esi, %edx
	movq	1656(%rsp), %rdi        # 8-byte Reload
	movq	1664(%rsp), %rsi        # 8-byte Reload
	movq	1688(%rsp), %rcx        # 8-byte Reload
	movq	1624(%rsp), %r8         # 8-byte Reload
	movq	1632(%rsp), %r9         # 8-byte Reload
	movq	1640(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	2368(%rsp), %rdi        # 8-byte Reload
	movq	2360(%rsp), %rsi        # 8-byte Reload
	movl	2468(%rsp), %edx        # 4-byte Reload
	callq	s1
	movabsq	$1, %rax
	movabsq	$0, %rcx
	leaq	.L.str15, %r8
	leaq	.L.str11, %rsi
	movabsq	$142, %rdi
	leaq	.L.str48, %r9
	leaq	.L.str52, %rdx
	leaq	.L.str54, %r10
	movabsq	$2, %r11
	movl	$1, %ebp
.Ltmp80:
	#DEBUG_VALUE: i <- 0
	.loc	1 142 0                 # test_dma_load_store.c:142:0
	movq	%rsi, 1560(%rsp)        # 8-byte Spill
	movq	%r9, %rsi
	movq	%rcx, 1552(%rsp)        # 8-byte Spill
	movq	%r10, %rcx
	movq	%r8, 1544(%rsp)         # 8-byte Spill
	movq	%r11, %r8
	movl	%ebp, %r9d
	movl	$1, (%rsp)
	movq	%rax, 1536(%rsp)        # 8-byte Spill
	callq	trace_logger_log0
	movq	1536(%rsp), %rdi        # 8-byte Reload
	movq	1552(%rsp), %rsi        # 8-byte Reload
	movq	1552(%rsp), %rdx        # 8-byte Reload
	movq	1536(%rsp), %rcx        # 8-byte Reload
	movq	1544(%rsp), %r8         # 8-byte Reload
	movq	1552(%rsp), %r9         # 8-byte Reload
	movq	1560(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	jmp	.LBB2_5
.Ltmp81:
.LBB2_2:                                # %.lr.ph5
                                        # =>This Inner Loop Header: Depth=1
	movq	1696(%rsp), %rax        # 8-byte Reload
	movabsq	$1, %rcx
	movabsq	$32, %rdx
	leaq	.L.str31, %rsi
	movabsq	$0, %rdi
	leaq	.L.str11, %r8
	movabsq	$2, %r9
	movabsq	$64, %r10
	leaq	soft_plus, %r11
	leaq	.L.str58, %rbx
	movabsq	$122, %r14
	leaq	.L.str48, %r15
	leaq	.L.str51, %r12
	leaq	.L.str33, %r13
	movabsq	$49, %rbp
	movq	%rsi, 1528(%rsp)        # 8-byte Spill
	movl	$1, %esi
	movq	%rax, 1520(%rsp)        # 8-byte Spill
	movabsq	$19134, %rax            # imm = 0x4ABE
	movq	%rax, 1512(%rsp)        # 8-byte Spill
	leaq	.L.str57, %rax
	movq	%rax, 1504(%rsp)        # 8-byte Spill
	movabsq	$27, %rax
	movq	%rax, 1496(%rsp)        # 8-byte Spill
	leaq	.L.str22, %rax
	movq	%rax, 1488(%rsp)        # 8-byte Spill
	leaq	.L.str55, %rax
	movq	%rax, 1480(%rsp)        # 8-byte Spill
	movabsq	$29, %rax
	movq	%rax, 1472(%rsp)        # 8-byte Spill
	leaq	.L.str56, %rax
	movq	%rax, 1464(%rsp)        # 8-byte Spill
	leaq	.L.str10, %rax
	movq	%rax, 1456(%rsp)        # 8-byte Spill
	leaq	.L.str8, %rax
	movq	%rax, 1448(%rsp)        # 8-byte Spill
	movabsq	$-1, %rax
	movq	%rax, 1440(%rsp)        # 8-byte Spill
	movabsq	$48, %rax
	.loc	1 122 20                # test_dma_load_store.c:122:20
.Ltmp82:
	movq	%rax, 1432(%rsp)        # 8-byte Spill
	movq	1440(%rsp), %rax        # 8-byte Reload
	movq	%rdi, 1424(%rsp)        # 8-byte Spill
	movq	%rax, %rdi
	movl	%esi, 1420(%rsp)        # 4-byte Spill
	movq	%r15, %rsi
	movq	%rdx, 1408(%rsp)        # 8-byte Spill
	movq	%r12, %rdx
	movq	1480(%rsp), %rax        # 8-byte Reload
	movq	%rcx, 1400(%rsp)        # 8-byte Spill
	movq	%rax, %rcx
	movq	1432(%rsp), %rax        # 8-byte Reload
	movq	%r8, 1392(%rsp)         # 8-byte Spill
	movq	%rax, %r8
	movl	1420(%rsp), %eax        # 4-byte Reload
	movq	%r9, 1384(%rsp)         # 8-byte Spill
	movl	%eax, %r9d
	movl	$1, (%rsp)
	movq	%r15, 1376(%rsp)        # 8-byte Spill
	movq	%r12, 1368(%rsp)        # 8-byte Spill
	movq	%r13, 1360(%rsp)        # 8-byte Spill
	movq	%rbp, 1352(%rsp)        # 8-byte Spill
	movq	%r14, 1344(%rsp)        # 8-byte Spill
	movq	%rbx, 1336(%rsp)        # 8-byte Spill
	movq	%r10, 1328(%rsp)        # 8-byte Spill
	movq	%r11, 1320(%rsp)        # 8-byte Spill
	callq	trace_logger_log0
	movq	1384(%rsp), %rdi        # 8-byte Reload
	movq	1328(%rsp), %rsi        # 8-byte Reload
	movq	1424(%rsp), %rdx        # 8-byte Reload
	movq	1424(%rsp), %rcx        # 8-byte Reload
	movq	1456(%rsp), %r8         # 8-byte Reload
	movq	1400(%rsp), %r9         # 8-byte Reload
	movq	1448(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	1400(%rsp), %rdi        # 8-byte Reload
	movq	1328(%rsp), %rsi        # 8-byte Reload
	movq	1424(%rsp), %rdx        # 8-byte Reload
	movq	1400(%rsp), %rcx        # 8-byte Reload
	movq	1464(%rsp), %r8         # 8-byte Reload
	movq	1400(%rsp), %r9         # 8-byte Reload
	movq	1368(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	1512(%rsp), %rdi        # 8-byte Reload
	movq	1328(%rsp), %rsi        # 8-byte Reload
	movq	1520(%rsp), %rdx        # 8-byte Reload
	movq	1400(%rsp), %rcx        # 8-byte Reload
	movq	1480(%rsp), %r8         # 8-byte Reload
	movq	1424(%rsp), %r9         # 8-byte Reload
	movq	1368(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	1344(%rsp), %rdi        # 8-byte Reload
	movq	1376(%rsp), %rsi        # 8-byte Reload
	movq	1368(%rsp), %rdx        # 8-byte Reload
	movq	1504(%rsp), %rcx        # 8-byte Reload
	movq	1472(%rsp), %r8         # 8-byte Reload
	movl	1420(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1384(%rsp), %rdi        # 8-byte Reload
	movq	1328(%rsp), %rsi        # 8-byte Reload
	movq	1520(%rsp), %rdx        # 8-byte Reload
	movq	1400(%rsp), %rcx        # 8-byte Reload
	movq	1480(%rsp), %r8         # 8-byte Reload
	movq	1424(%rsp), %r9         # 8-byte Reload
	movq	1392(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	1400(%rsp), %rdi        # 8-byte Reload
	movq	1328(%rsp), %rsi        # 8-byte Reload
	movq	2360(%rsp), %rdx        # 8-byte Reload
	movq	1400(%rsp), %rcx        # 8-byte Reload
	movq	1488(%rsp), %r8         # 8-byte Reload
	movq	1424(%rsp), %r9         # 8-byte Reload
	movq	1392(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	1520(%rsp), %rcx        # 8-byte Reload
	shlq	$2, %rcx
	movq	2360(%rsp), %rdx        # 8-byte Reload
	addq	%rcx, %rdx
	movq	1512(%rsp), %rdi        # 8-byte Reload
	movq	1328(%rsp), %rsi        # 8-byte Reload
	movq	%rdx, 1312(%rsp)        # 8-byte Spill
	movq	1400(%rsp), %rcx        # 8-byte Reload
	movq	1504(%rsp), %r8         # 8-byte Reload
	movq	1424(%rsp), %r9         # 8-byte Reload
	movq	1392(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	1344(%rsp), %rdi        # 8-byte Reload
	movq	1376(%rsp), %rsi        # 8-byte Reload
	movq	1368(%rsp), %rdx        # 8-byte Reload
	movq	1528(%rsp), %rcx        # 8-byte Reload
	movq	1496(%rsp), %r8         # 8-byte Reload
	movl	1420(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1400(%rsp), %rdi        # 8-byte Reload
	movq	1328(%rsp), %rsi        # 8-byte Reload
	movq	1312(%rsp), %rdx        # 8-byte Reload
	movq	1400(%rsp), %rcx        # 8-byte Reload
	movq	1504(%rsp), %r8         # 8-byte Reload
	movq	1424(%rsp), %r9         # 8-byte Reload
	movq	1392(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	2360(%rsp), %rcx        # 8-byte Reload
	movq	1520(%rsp), %rdx        # 8-byte Reload
	vmovss	(%rcx,%rdx,4), %xmm0
	cvtss2sd	%xmm0, %xmm1
	movq	1512(%rsp), %rdi        # 8-byte Reload
	movq	1408(%rsp), %rsi        # 8-byte Reload
	vmovss	%xmm0, 1308(%rsp)       # 4-byte Spill
	vmovaps	%xmm1, %xmm0
	movq	1400(%rsp), %rdx        # 8-byte Reload
	movq	1528(%rsp), %rcx        # 8-byte Reload
	movq	1424(%rsp), %r8         # 8-byte Reload
	movq	1392(%rsp), %r9         # 8-byte Reload
	callq	trace_logger_log_double
	movq	1344(%rsp), %rdi        # 8-byte Reload
	movq	1376(%rsp), %rsi        # 8-byte Reload
	movq	1368(%rsp), %rdx        # 8-byte Reload
	movq	1360(%rsp), %rcx        # 8-byte Reload
	movq	1352(%rsp), %r8         # 8-byte Reload
	movl	1420(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1384(%rsp), %rdi        # 8-byte Reload
	movq	1328(%rsp), %rsi        # 8-byte Reload
	movq	1320(%rsp), %rdx        # 8-byte Reload
	movq	1400(%rsp), %rcx        # 8-byte Reload
	movq	1336(%rsp), %r8         # 8-byte Reload
	movq	1424(%rsp), %r9         # 8-byte Reload
	movq	1392(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	vmovss	1308(%rsp), %xmm0       # 4-byte Reload
	cvtss2sd	%xmm0, %xmm0
	movq	1400(%rsp), %rdi        # 8-byte Reload
	movq	1408(%rsp), %rsi        # 8-byte Reload
	movq	1400(%rsp), %rdx        # 8-byte Reload
	movq	1528(%rsp), %rcx        # 8-byte Reload
	movq	1424(%rsp), %r8         # 8-byte Reload
	movq	1392(%rsp), %r9         # 8-byte Reload
	callq	trace_logger_log_double
	vmovss	1308(%rsp), %xmm0       # 4-byte Reload
	callq	soft_plus
	movabsq	$1, %rcx
	leaq	.L.str61, %rdx
	movabsq	$0, %rsi
	leaq	.L.str11, %rdi
	movabsq	$2, %r8
	leaq	.L.str51, %r9
	movabsq	$3, %r10
	leaq	.L.str63, %r11
	movabsq	$121, %rbx
	leaq	.L.str48, %r14
	leaq	.L.str62, %r15
	movl	$1, %eax
	movabsq	$19134, %r12            # imm = 0x4ABE
	movabsq	$32, %r13
	leaq	.L.str60, %rbp
	movq	%rcx, 1296(%rsp)        # 8-byte Spill
	leaq	.L.str12, %rcx
	movq	%rcx, 1288(%rsp)        # 8-byte Spill
	movabsq	$46, %rcx
	movq	%rcx, 1280(%rsp)        # 8-byte Spill
	movabsq	$64, %rcx
	movq	%rcx, 1272(%rsp)        # 8-byte Spill
	leaq	.L.str56, %rcx
	movq	%rcx, 1264(%rsp)        # 8-byte Spill
	movabsq	$33, %rcx
	movq	%rcx, 1256(%rsp)        # 8-byte Spill
	leaq	.L.str55, %rcx
	movq	%rcx, 1248(%rsp)        # 8-byte Spill
	leaq	.L.str10, %rcx
	movq	%rcx, 1240(%rsp)        # 8-byte Spill
	movabsq	$8, %rcx
	movq	%rcx, 1232(%rsp)        # 8-byte Spill
	leaq	.L.str33, %rcx
	movq	%rcx, 1224(%rsp)        # 8-byte Spill
	leaq	.L.str57, %rcx
	movq	%rcx, 1216(%rsp)        # 8-byte Spill
	movabsq	$122, %rcx
	movq	%rcx, 1208(%rsp)        # 8-byte Spill
	leaq	.L.str59, %rcx
	movq	%rcx, 1200(%rsp)        # 8-byte Spill
	movabsq	$28, %rcx
	cvtss2sd	%xmm0, %xmm1
	movq	%rdi, 1192(%rsp)        # 8-byte Spill
	movq	%r12, %rdi
	movq	%rsi, 1184(%rsp)        # 8-byte Spill
	movq	%r13, %rsi
	vmovss	%xmm0, 1180(%rsp)       # 4-byte Spill
	vmovaps	%xmm1, %xmm0
	movq	%rcx, 1168(%rsp)        # 8-byte Spill
	movq	1296(%rsp), %rcx        # 8-byte Reload
	movq	%rdx, 1160(%rsp)        # 8-byte Spill
	movq	%rcx, %rdx
	movq	1224(%rsp), %rcx        # 8-byte Reload
	movq	%r8, 1152(%rsp)         # 8-byte Spill
	movq	1184(%rsp), %r8         # 8-byte Reload
	movq	%r9, 1144(%rsp)         # 8-byte Spill
	movq	1192(%rsp), %r9         # 8-byte Reload
	movq	%r12, 1136(%rsp)        # 8-byte Spill
	movq	%r15, 1128(%rsp)        # 8-byte Spill
	movl	%eax, 1124(%rsp)        # 4-byte Spill
	movq	%rbp, 1112(%rsp)        # 8-byte Spill
	movq	%r13, 1104(%rsp)        # 8-byte Spill
	movq	%r14, 1096(%rsp)        # 8-byte Spill
	movq	%rbx, 1088(%rsp)        # 8-byte Spill
	movq	%r10, 1080(%rsp)        # 8-byte Spill
	movq	%r11, 1072(%rsp)        # 8-byte Spill
	callq	trace_logger_log_double
	movq	1208(%rsp), %rdi        # 8-byte Reload
	movq	1096(%rsp), %rsi        # 8-byte Reload
	movq	1144(%rsp), %rdx        # 8-byte Reload
	movq	1200(%rsp), %rcx        # 8-byte Reload
	movq	1168(%rsp), %r8         # 8-byte Reload
	movl	1124(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1152(%rsp), %rdi        # 8-byte Reload
	movq	1272(%rsp), %rsi        # 8-byte Reload
	movq	1312(%rsp), %rdx        # 8-byte Reload
	movq	1296(%rsp), %rcx        # 8-byte Reload
	movq	1216(%rsp), %r8         # 8-byte Reload
	movq	1184(%rsp), %r9         # 8-byte Reload
	movq	1192(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	vmovss	1180(%rsp), %xmm0       # 4-byte Reload
	cvtss2sd	%xmm0, %xmm0
	movq	1296(%rsp), %rdi        # 8-byte Reload
	movq	1104(%rsp), %rsi        # 8-byte Reload
	movq	1296(%rsp), %rdx        # 8-byte Reload
	movq	1224(%rsp), %rcx        # 8-byte Reload
	movq	1184(%rsp), %r8         # 8-byte Reload
	movq	1192(%rsp), %r9         # 8-byte Reload
	callq	trace_logger_log_double
	movq	2360(%rsp), %rcx        # 8-byte Reload
	movq	1520(%rsp), %rdx        # 8-byte Reload
	vmovss	1180(%rsp), %xmm0       # 4-byte Reload
	vmovss	%xmm0, (%rcx,%rdx,4)
	.loc	1 121 0                 # test_dma_load_store.c:121:0
	movq	1088(%rsp), %rdi        # 8-byte Reload
	movq	1096(%rsp), %rsi        # 8-byte Reload
	movq	1144(%rsp), %rdx        # 8-byte Reload
	movq	1264(%rsp), %rcx        # 8-byte Reload
	movq	1232(%rsp), %r8         # 8-byte Reload
	movl	1124(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1152(%rsp), %rdi        # 8-byte Reload
	movq	1272(%rsp), %rsi        # 8-byte Reload
	movq	1296(%rsp), %rdx        # 8-byte Reload
	movq	1184(%rsp), %rcx        # 8-byte Reload
	movq	1240(%rsp), %r8         # 8-byte Reload
	movq	1184(%rsp), %r9         # 8-byte Reload
	movq	1192(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	1296(%rsp), %rdi        # 8-byte Reload
	movq	1272(%rsp), %rsi        # 8-byte Reload
	movq	1520(%rsp), %rdx        # 8-byte Reload
	movq	1296(%rsp), %rcx        # 8-byte Reload
	movq	1248(%rsp), %r8         # 8-byte Reload
	movq	1184(%rsp), %r9         # 8-byte Reload
	movq	1192(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	1520(%rsp), %rcx        # 8-byte Reload
	addq	$1, %rcx
	movq	1136(%rsp), %rdi        # 8-byte Reload
	movq	1272(%rsp), %rsi        # 8-byte Reload
	movq	%rcx, %rdx
	movq	1296(%rsp), %r8         # 8-byte Reload
	movq	%rcx, 1064(%rsp)        # 8-byte Spill
	movq	%r8, %rcx
	movq	1264(%rsp), %r8         # 8-byte Reload
	movq	1184(%rsp), %r9         # 8-byte Reload
	movq	1192(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	1088(%rsp), %rdi        # 8-byte Reload
	movq	1096(%rsp), %rsi        # 8-byte Reload
	movq	1144(%rsp), %rdx        # 8-byte Reload
	movq	1112(%rsp), %rcx        # 8-byte Reload
	movq	1256(%rsp), %r8         # 8-byte Reload
	movl	1124(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1296(%rsp), %rdi        # 8-byte Reload
	movq	1272(%rsp), %rsi        # 8-byte Reload
	movq	1064(%rsp), %rdx        # 8-byte Reload
	movq	1296(%rsp), %rcx        # 8-byte Reload
	movq	1264(%rsp), %r8         # 8-byte Reload
	movq	1184(%rsp), %r9         # 8-byte Reload
	movq	1192(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	1064(%rsp), %rcx        # 8-byte Reload
	movl	%ecx, %eax
	movl	%eax, %esi
	movl	%esi, %edx
	movq	1136(%rsp), %rdi        # 8-byte Reload
	movq	1104(%rsp), %rsi        # 8-byte Reload
	movq	1296(%rsp), %rcx        # 8-byte Reload
	movq	1112(%rsp), %r8         # 8-byte Reload
	movq	1184(%rsp), %r9         # 8-byte Reload
	movq	1192(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	movl	%eax, 1060(%rsp)        # 4-byte Spill
	callq	trace_logger_log_int
	movq	1088(%rsp), %rdi        # 8-byte Reload
	movq	1096(%rsp), %rsi        # 8-byte Reload
	movq	1144(%rsp), %rdx        # 8-byte Reload
	movq	1160(%rsp), %rcx        # 8-byte Reload
	movq	1280(%rsp), %r8         # 8-byte Reload
	movl	1124(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movl	2468(%rsp), %eax        # 4-byte Reload
	movl	%eax, %r9d
	movl	%r9d, %edx
	movq	1152(%rsp), %rdi        # 8-byte Reload
	movq	1104(%rsp), %rsi        # 8-byte Reload
	movq	1296(%rsp), %rcx        # 8-byte Reload
	movq	1288(%rsp), %r8         # 8-byte Reload
	movq	1184(%rsp), %r9         # 8-byte Reload
	movq	1192(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movl	1060(%rsp), %eax        # 4-byte Reload
	movl	%eax, %eax
	movl	%eax, %edx
	movq	1296(%rsp), %rdi        # 8-byte Reload
	movq	1104(%rsp), %rsi        # 8-byte Reload
	movq	1296(%rsp), %rcx        # 8-byte Reload
	movq	1112(%rsp), %r8         # 8-byte Reload
	movq	1184(%rsp), %r9         # 8-byte Reload
	movq	1192(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movl	1060(%rsp), %eax        # 4-byte Reload
	movl	2468(%rsp), %esi        # 4-byte Reload
	cmpl	%esi, %eax
	sete	%al
	movb	%al, %cl
	andb	$1, %cl
	movzbl	%cl, %ecx
	movl	%ecx, %edx
	movq	1136(%rsp), %rdi        # 8-byte Reload
	movq	1296(%rsp), %rsi        # 8-byte Reload
	movq	1296(%rsp), %rcx        # 8-byte Reload
	movq	1160(%rsp), %r8         # 8-byte Reload
	movq	1184(%rsp), %r9         # 8-byte Reload
	movq	1192(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	movb	%al, 1059(%rsp)         # 1-byte Spill
	callq	trace_logger_log_int
	movq	1088(%rsp), %rdi        # 8-byte Reload
	movq	1096(%rsp), %rsi        # 8-byte Reload
	movq	1144(%rsp), %rdx        # 8-byte Reload
	movq	1128(%rsp), %rcx        # 8-byte Reload
	movq	1152(%rsp), %r8         # 8-byte Reload
	movl	1124(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1080(%rsp), %rdi        # 8-byte Reload
	movq	1184(%rsp), %rsi        # 8-byte Reload
	movq	1184(%rsp), %rdx        # 8-byte Reload
	movq	1296(%rsp), %rcx        # 8-byte Reload
	movq	1072(%rsp), %r8         # 8-byte Reload
	movq	1184(%rsp), %r9         # 8-byte Reload
	movq	1192(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	1152(%rsp), %rdi        # 8-byte Reload
	movq	1184(%rsp), %rsi        # 8-byte Reload
	movq	1184(%rsp), %rdx        # 8-byte Reload
	movq	1296(%rsp), %rcx        # 8-byte Reload
	movq	1144(%rsp), %r8         # 8-byte Reload
	movq	1184(%rsp), %r9         # 8-byte Reload
	movq	1192(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	1059(%rsp), %al         # 1-byte Reload
	andb	$1, %al
	movzbl	%al, %eax
	movl	%eax, %edx
	movq	1296(%rsp), %rdi        # 8-byte Reload
	movq	1296(%rsp), %rsi        # 8-byte Reload
	movq	1296(%rsp), %rcx        # 8-byte Reload
	movq	1160(%rsp), %r8         # 8-byte Reload
	movq	1184(%rsp), %r9         # 8-byte Reload
	movq	1192(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	1059(%rsp), %al         # 1-byte Reload
	testb	$1, %al
	movq	1064(%rsp), %rcx        # 8-byte Reload
	movq	%rcx, 1696(%rsp)        # 8-byte Spill
	jne	.LBB2_3
	jmp	.LBB2_2
.Ltmp83:
.LBB2_3:                                # %._crit_edge6
	movabsq	$3, %rdi
	movabsq	$32, %rsi
	movabsq	$1, %rax
	leaq	.L.str12, %r8
	movabsq	$0, %rcx
	leaq	.L.str11, %rdx
	movabsq	$2, %r9
	movabsq	$64, %r10
	leaq	.L.str22, %r11
	leaq	.L.str25, %rbx
	movabsq	$4, %r14
	leaq	s1, %r15
	leaq	.L.str45, %r12
	movabsq	$138, %r13
	leaq	.L.str48, %rbp
	movq	%rax, 1048(%rsp)        # 8-byte Spill
	leaq	.L.str63, %rax
	movq	%rax, 1040(%rsp)        # 8-byte Spill
	leaq	.L.str64, %rax
	movq	%rax, 1032(%rsp)        # 8-byte Spill
	movabsq	$49, %rax
	movq	%rsi, 1024(%rsp)        # 8-byte Spill
	movl	$1, %esi
	.loc	1 138 0                 # test_dma_load_store.c:138:0
	movq	%rdi, 1016(%rsp)        # 8-byte Spill
	movq	%r13, %rdi
	movl	%esi, 1012(%rsp)        # 4-byte Spill
	movq	%rbp, %rsi
	movq	1040(%rsp), %r13        # 8-byte Reload
	movq	%rdx, 1000(%rsp)        # 8-byte Spill
	movq	%r13, %rdx
	movq	1032(%rsp), %rbp        # 8-byte Reload
	movq	%rcx, 992(%rsp)         # 8-byte Spill
	movq	%rbp, %rcx
	movq	%r8, 984(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movl	1012(%rsp), %ebp        # 4-byte Reload
	movq	%r9, 976(%rsp)          # 8-byte Spill
	movl	%ebp, %r9d
	movl	$1, (%rsp)
	movq	%r12, 968(%rsp)         # 8-byte Spill
	movq	%r15, 960(%rsp)         # 8-byte Spill
	movq	%r14, 952(%rsp)         # 8-byte Spill
	movq	%rbx, 944(%rsp)         # 8-byte Spill
	movq	%r11, 936(%rsp)         # 8-byte Spill
	movq	%r10, 928(%rsp)         # 8-byte Spill
	callq	trace_logger_log0
	movq	952(%rsp), %rdi         # 8-byte Reload
	movq	928(%rsp), %rsi         # 8-byte Reload
	movq	960(%rsp), %rdx         # 8-byte Reload
	movq	1048(%rsp), %rcx        # 8-byte Reload
	movq	968(%rsp), %r8          # 8-byte Reload
	movq	992(%rsp), %r9          # 8-byte Reload
	movq	1000(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	1048(%rsp), %rdi        # 8-byte Reload
	movq	928(%rsp), %rsi         # 8-byte Reload
	movq	2368(%rsp), %rdx        # 8-byte Reload
	movq	1048(%rsp), %rcx        # 8-byte Reload
	movq	944(%rsp), %r8          # 8-byte Reload
	movq	992(%rsp), %r9          # 8-byte Reload
	movq	1000(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	976(%rsp), %rdi         # 8-byte Reload
	movq	928(%rsp), %rsi         # 8-byte Reload
	movq	2360(%rsp), %rdx        # 8-byte Reload
	movq	1048(%rsp), %rcx        # 8-byte Reload
	movq	936(%rsp), %r8          # 8-byte Reload
	movq	992(%rsp), %r9          # 8-byte Reload
	movq	1000(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movl	2468(%rsp), %ebp        # 4-byte Reload
	movl	%ebp, %esi
	movl	%esi, %edx
	movq	1016(%rsp), %rdi        # 8-byte Reload
	movq	1024(%rsp), %rsi        # 8-byte Reload
	movq	1048(%rsp), %rcx        # 8-byte Reload
	movq	984(%rsp), %r8          # 8-byte Reload
	movq	992(%rsp), %r9          # 8-byte Reload
	movq	1000(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	2368(%rsp), %rdi        # 8-byte Reload
	movq	2360(%rsp), %rsi        # 8-byte Reload
	movl	2468(%rsp), %edx        # 4-byte Reload
	callq	s1
	movabsq	$0, %rax
	movabsq	$1, %rcx
	leaq	.L.str34, %rsi
	leaq	.L.str11, %rdi
	movabsq	$2, %r8
	leaq	.L.str15, %r9
	movabsq	$3, %r10
	leaq	.L.str14, %r11
	movabsq	$142, %rbx
	leaq	.L.str48, %r14
	leaq	.L.str63, %r15
	leaq	.L.str65, %r12
	movl	$1, %edx
	movabsq	$19134, %r13            # imm = 0x4ABE
	movabsq	$32, %rbp
	movq	%rax, 920(%rsp)         # 8-byte Spill
	leaq	.L.str12, %rax
	movq	%rax, 912(%rsp)         # 8-byte Spill
	leaq	.L.str10, %rax
	movq	%rax, 904(%rsp)         # 8-byte Spill
	movabsq	$46, %rax
.Ltmp84:
	#DEBUG_VALUE: i <- 0
	.loc	1 142 0                 # test_dma_load_store.c:142:0
	movq	%rdi, 896(%rsp)         # 8-byte Spill
	movq	%rbx, %rdi
	movq	%rsi, 888(%rsp)         # 8-byte Spill
	movq	%r14, %rsi
	movl	%edx, 884(%rsp)         # 4-byte Spill
	movq	%r15, %rdx
	movq	%rax, 872(%rsp)         # 8-byte Spill
	movq	888(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 864(%rsp)         # 8-byte Spill
	movq	%rax, %rcx
	movq	872(%rsp), %rax         # 8-byte Reload
	movq	%r8, 856(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movl	884(%rsp), %eax         # 4-byte Reload
	movq	%r9, 848(%rsp)          # 8-byte Spill
	movl	%eax, %r9d
	movl	$1, (%rsp)
	movq	%r12, 840(%rsp)         # 8-byte Spill
	movq	%r13, 832(%rsp)         # 8-byte Spill
	movq	%rbp, 824(%rsp)         # 8-byte Spill
	movq	%r15, 816(%rsp)         # 8-byte Spill
	movq	%r14, 808(%rsp)         # 8-byte Spill
	movq	%r10, 800(%rsp)         # 8-byte Spill
	movq	%r11, 792(%rsp)         # 8-byte Spill
	movq	%rbx, 784(%rsp)         # 8-byte Spill
	callq	trace_logger_log0
	movq	856(%rsp), %rdi         # 8-byte Reload
	movq	824(%rsp), %rsi         # 8-byte Reload
	movq	920(%rsp), %rdx         # 8-byte Reload
	movq	920(%rsp), %rcx         # 8-byte Reload
	movq	904(%rsp), %r8          # 8-byte Reload
	movq	920(%rsp), %r9          # 8-byte Reload
	movq	896(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movl	2468(%rsp), %eax        # 4-byte Reload
	movl	%eax, %eax
	movl	%eax, %edx
	movq	864(%rsp), %rdi         # 8-byte Reload
	movq	824(%rsp), %rsi         # 8-byte Reload
	movq	864(%rsp), %rcx         # 8-byte Reload
	movq	912(%rsp), %r8          # 8-byte Reload
	movq	920(%rsp), %r9          # 8-byte Reload
	movq	896(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movl	2468(%rsp), %eax        # 4-byte Reload
	cmpl	$0, %eax
	setg	%al
	movb	%al, %cl
	andb	$1, %cl
	movzbl	%cl, %ecx
	movl	%ecx, %edx
	movq	832(%rsp), %rdi         # 8-byte Reload
	movq	864(%rsp), %rsi         # 8-byte Reload
	movq	864(%rsp), %rcx         # 8-byte Reload
	movq	888(%rsp), %r8          # 8-byte Reload
	movq	920(%rsp), %r9          # 8-byte Reload
	movq	896(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movb	%al, 783(%rsp)          # 1-byte Spill
	callq	trace_logger_log_int
	movq	784(%rsp), %rdi         # 8-byte Reload
	movq	808(%rsp), %rsi         # 8-byte Reload
	movq	816(%rsp), %rdx         # 8-byte Reload
	movq	840(%rsp), %rcx         # 8-byte Reload
	movq	856(%rsp), %r8          # 8-byte Reload
	movl	884(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	800(%rsp), %rdi         # 8-byte Reload
	movq	920(%rsp), %rsi         # 8-byte Reload
	movq	920(%rsp), %rdx         # 8-byte Reload
	movq	864(%rsp), %rcx         # 8-byte Reload
	movq	792(%rsp), %r8          # 8-byte Reload
	movq	920(%rsp), %r9          # 8-byte Reload
	movq	896(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	856(%rsp), %rdi         # 8-byte Reload
	movq	920(%rsp), %rsi         # 8-byte Reload
	movq	920(%rsp), %rdx         # 8-byte Reload
	movq	864(%rsp), %rcx         # 8-byte Reload
	movq	848(%rsp), %r8          # 8-byte Reload
	movq	920(%rsp), %r9          # 8-byte Reload
	movq	896(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	783(%rsp), %al          # 1-byte Reload
	andb	$1, %al
	movzbl	%al, %eax
	movl	%eax, %edx
	movq	864(%rsp), %rdi         # 8-byte Reload
	movq	864(%rsp), %rsi         # 8-byte Reload
	movq	864(%rsp), %rcx         # 8-byte Reload
	movq	888(%rsp), %r8          # 8-byte Reload
	movq	920(%rsp), %r9          # 8-byte Reload
	movq	896(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	783(%rsp), %al          # 1-byte Reload
	testb	$1, %al
	movq	920(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, 768(%rsp)         # 8-byte Spill
	jne	.LBB2_4
	jmp	.LBB2_5
.LBB2_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	768(%rsp), %rax         # 8-byte Reload
	movabsq	$1, %rcx
	movabsq	$32, %rdx
	leaq	.L.str36, %rsi
	movabsq	$0, %rdi
	leaq	.L.str11, %r8
	movabsq	$2, %r9
	movabsq	$64, %r10
	leaq	soft_plus, %r11
	leaq	.L.str58, %rbx
	movabsq	$143, %r14
	leaq	.L.str48, %r15
	leaq	.L.str14, %r12
	leaq	.L.str38, %r13
	movabsq	$49, %rbp
	movq	%rsi, 760(%rsp)         # 8-byte Spill
	movl	$1, %esi
	movq	%rax, 752(%rsp)         # 8-byte Spill
	movabsq	$19134, %rax            # imm = 0x4ABE
	movq	%rax, 744(%rsp)         # 8-byte Spill
	leaq	.L.str35, %rax
	movq	%rax, 736(%rsp)         # 8-byte Spill
	movabsq	$27, %rax
	movq	%rax, 728(%rsp)         # 8-byte Spill
	leaq	.L.str22, %rax
	movq	%rax, 720(%rsp)         # 8-byte Spill
	leaq	.L.str16, %rax
	movq	%rax, 712(%rsp)         # 8-byte Spill
	movabsq	$29, %rax
	movq	%rax, 704(%rsp)         # 8-byte Spill
	leaq	.L.str17, %rax
	movq	%rax, 696(%rsp)         # 8-byte Spill
	leaq	.L.str10, %rax
	movq	%rax, 688(%rsp)         # 8-byte Spill
	leaq	.L.str63, %rax
	movq	%rax, 680(%rsp)         # 8-byte Spill
	movabsq	$-1, %rax
	movq	%rax, 672(%rsp)         # 8-byte Spill
	movabsq	$48, %rax
	.loc	1 143 20                # test_dma_load_store.c:143:20
	movq	%rax, 664(%rsp)         # 8-byte Spill
	movq	672(%rsp), %rax         # 8-byte Reload
	movq	%rdi, 656(%rsp)         # 8-byte Spill
	movq	%rax, %rdi
	movl	%esi, 652(%rsp)         # 4-byte Spill
	movq	%r15, %rsi
	movq	%rdx, 640(%rsp)         # 8-byte Spill
	movq	%r12, %rdx
	movq	712(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 632(%rsp)         # 8-byte Spill
	movq	%rax, %rcx
	movq	664(%rsp), %rax         # 8-byte Reload
	movq	%r8, 624(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movl	652(%rsp), %eax         # 4-byte Reload
	movq	%r9, 616(%rsp)          # 8-byte Spill
	movl	%eax, %r9d
	movl	$1, (%rsp)
	movq	%r15, 608(%rsp)         # 8-byte Spill
	movq	%r12, 600(%rsp)         # 8-byte Spill
	movq	%r13, 592(%rsp)         # 8-byte Spill
	movq	%rbp, 584(%rsp)         # 8-byte Spill
	movq	%r14, 576(%rsp)         # 8-byte Spill
	movq	%rbx, 568(%rsp)         # 8-byte Spill
	movq	%r10, 560(%rsp)         # 8-byte Spill
	movq	%r11, 552(%rsp)         # 8-byte Spill
	callq	trace_logger_log0
	movq	616(%rsp), %rdi         # 8-byte Reload
	movq	560(%rsp), %rsi         # 8-byte Reload
	movq	656(%rsp), %rdx         # 8-byte Reload
	movq	656(%rsp), %rcx         # 8-byte Reload
	movq	688(%rsp), %r8          # 8-byte Reload
	movq	632(%rsp), %r9          # 8-byte Reload
	movq	680(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	632(%rsp), %rdi         # 8-byte Reload
	movq	560(%rsp), %rsi         # 8-byte Reload
	movq	656(%rsp), %rdx         # 8-byte Reload
	movq	632(%rsp), %rcx         # 8-byte Reload
	movq	696(%rsp), %r8          # 8-byte Reload
	movq	632(%rsp), %r9          # 8-byte Reload
	movq	600(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	744(%rsp), %rdi         # 8-byte Reload
	movq	560(%rsp), %rsi         # 8-byte Reload
	movq	752(%rsp), %rdx         # 8-byte Reload
	movq	632(%rsp), %rcx         # 8-byte Reload
	movq	712(%rsp), %r8          # 8-byte Reload
	movq	656(%rsp), %r9          # 8-byte Reload
	movq	600(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	576(%rsp), %rdi         # 8-byte Reload
	movq	608(%rsp), %rsi         # 8-byte Reload
	movq	600(%rsp), %rdx         # 8-byte Reload
	movq	736(%rsp), %rcx         # 8-byte Reload
	movq	704(%rsp), %r8          # 8-byte Reload
	movl	652(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	616(%rsp), %rdi         # 8-byte Reload
	movq	560(%rsp), %rsi         # 8-byte Reload
	movq	752(%rsp), %rdx         # 8-byte Reload
	movq	632(%rsp), %rcx         # 8-byte Reload
	movq	712(%rsp), %r8          # 8-byte Reload
	movq	656(%rsp), %r9          # 8-byte Reload
	movq	624(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	632(%rsp), %rdi         # 8-byte Reload
	movq	560(%rsp), %rsi         # 8-byte Reload
	movq	2360(%rsp), %rdx        # 8-byte Reload
	movq	632(%rsp), %rcx         # 8-byte Reload
	movq	720(%rsp), %r8          # 8-byte Reload
	movq	656(%rsp), %r9          # 8-byte Reload
	movq	624(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	752(%rsp), %rcx         # 8-byte Reload
	shlq	$2, %rcx
	movq	2360(%rsp), %rdx        # 8-byte Reload
	addq	%rcx, %rdx
	movq	744(%rsp), %rdi         # 8-byte Reload
	movq	560(%rsp), %rsi         # 8-byte Reload
	movq	%rdx, 544(%rsp)         # 8-byte Spill
	movq	632(%rsp), %rcx         # 8-byte Reload
	movq	736(%rsp), %r8          # 8-byte Reload
	movq	656(%rsp), %r9          # 8-byte Reload
	movq	624(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	576(%rsp), %rdi         # 8-byte Reload
	movq	608(%rsp), %rsi         # 8-byte Reload
	movq	600(%rsp), %rdx         # 8-byte Reload
	movq	760(%rsp), %rcx         # 8-byte Reload
	movq	728(%rsp), %r8          # 8-byte Reload
	movl	652(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	632(%rsp), %rdi         # 8-byte Reload
	movq	560(%rsp), %rsi         # 8-byte Reload
	movq	544(%rsp), %rdx         # 8-byte Reload
	movq	632(%rsp), %rcx         # 8-byte Reload
	movq	736(%rsp), %r8          # 8-byte Reload
	movq	656(%rsp), %r9          # 8-byte Reload
	movq	624(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	2360(%rsp), %rcx        # 8-byte Reload
	movq	752(%rsp), %rdx         # 8-byte Reload
	vmovss	(%rcx,%rdx,4), %xmm0
	cvtss2sd	%xmm0, %xmm1
	movq	744(%rsp), %rdi         # 8-byte Reload
	movq	640(%rsp), %rsi         # 8-byte Reload
	vmovss	%xmm0, 540(%rsp)        # 4-byte Spill
	vmovaps	%xmm1, %xmm0
	movq	632(%rsp), %rdx         # 8-byte Reload
	movq	760(%rsp), %rcx         # 8-byte Reload
	movq	656(%rsp), %r8          # 8-byte Reload
	movq	624(%rsp), %r9          # 8-byte Reload
	callq	trace_logger_log_double
	movq	576(%rsp), %rdi         # 8-byte Reload
	movq	608(%rsp), %rsi         # 8-byte Reload
	movq	600(%rsp), %rdx         # 8-byte Reload
	movq	592(%rsp), %rcx         # 8-byte Reload
	movq	584(%rsp), %r8          # 8-byte Reload
	movl	652(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	616(%rsp), %rdi         # 8-byte Reload
	movq	560(%rsp), %rsi         # 8-byte Reload
	movq	552(%rsp), %rdx         # 8-byte Reload
	movq	632(%rsp), %rcx         # 8-byte Reload
	movq	568(%rsp), %r8          # 8-byte Reload
	movq	656(%rsp), %r9          # 8-byte Reload
	movq	624(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	vmovss	540(%rsp), %xmm0        # 4-byte Reload
	cvtss2sd	%xmm0, %xmm0
	movq	632(%rsp), %rdi         # 8-byte Reload
	movq	640(%rsp), %rsi         # 8-byte Reload
	movq	632(%rsp), %rdx         # 8-byte Reload
	movq	760(%rsp), %rcx         # 8-byte Reload
	movq	656(%rsp), %r8          # 8-byte Reload
	movq	624(%rsp), %r9          # 8-byte Reload
	callq	trace_logger_log_double
	vmovss	540(%rsp), %xmm0        # 4-byte Reload
	callq	soft_plus
	movabsq	$1, %rcx
	leaq	.L.str41, %rdx
	movabsq	$0, %rsi
	leaq	.L.str11, %rdi
	movabsq	$2, %r8
	leaq	.L.str14, %r9
	movabsq	$3, %r10
	leaq	.L.str15, %r11
	movabsq	$142, %rbx
	leaq	.L.str48, %r14
	leaq	.L.str47, %r15
	movl	$1, %eax
	movabsq	$19134, %r12            # imm = 0x4ABE
	movabsq	$32, %r13
	leaq	.L.str40, %rbp
	movq	%rcx, 528(%rsp)         # 8-byte Spill
	leaq	.L.str12, %rcx
	movq	%rcx, 520(%rsp)         # 8-byte Spill
	movabsq	$46, %rcx
	movq	%rcx, 512(%rsp)         # 8-byte Spill
	movabsq	$64, %rcx
	movq	%rcx, 504(%rsp)         # 8-byte Spill
	leaq	.L.str17, %rcx
	movq	%rcx, 496(%rsp)         # 8-byte Spill
	movabsq	$33, %rcx
	movq	%rcx, 488(%rsp)         # 8-byte Spill
	leaq	.L.str16, %rcx
	movq	%rcx, 480(%rsp)         # 8-byte Spill
	leaq	.L.str10, %rcx
	movq	%rcx, 472(%rsp)         # 8-byte Spill
	movabsq	$8, %rcx
	movq	%rcx, 464(%rsp)         # 8-byte Spill
	leaq	.L.str38, %rcx
	movq	%rcx, 456(%rsp)         # 8-byte Spill
	leaq	.L.str35, %rcx
	movq	%rcx, 448(%rsp)         # 8-byte Spill
	movabsq	$143, %rcx
	movq	%rcx, 440(%rsp)         # 8-byte Spill
	leaq	.L.str29, %rcx
	movq	%rcx, 432(%rsp)         # 8-byte Spill
	movabsq	$28, %rcx
	cvtss2sd	%xmm0, %xmm1
	movq	%rdi, 424(%rsp)         # 8-byte Spill
	movq	%r12, %rdi
	movq	%rsi, 416(%rsp)         # 8-byte Spill
	movq	%r13, %rsi
	vmovss	%xmm0, 412(%rsp)        # 4-byte Spill
	vmovaps	%xmm1, %xmm0
	movq	%rcx, 400(%rsp)         # 8-byte Spill
	movq	528(%rsp), %rcx         # 8-byte Reload
	movq	%rdx, 392(%rsp)         # 8-byte Spill
	movq	%rcx, %rdx
	movq	456(%rsp), %rcx         # 8-byte Reload
	movq	%r8, 384(%rsp)          # 8-byte Spill
	movq	416(%rsp), %r8          # 8-byte Reload
	movq	%r9, 376(%rsp)          # 8-byte Spill
	movq	424(%rsp), %r9          # 8-byte Reload
	movq	%r12, 368(%rsp)         # 8-byte Spill
	movq	%r15, 360(%rsp)         # 8-byte Spill
	movl	%eax, 356(%rsp)         # 4-byte Spill
	movq	%rbp, 344(%rsp)         # 8-byte Spill
	movq	%r13, 336(%rsp)         # 8-byte Spill
	movq	%r14, 328(%rsp)         # 8-byte Spill
	movq	%rbx, 320(%rsp)         # 8-byte Spill
	movq	%r10, 312(%rsp)         # 8-byte Spill
	movq	%r11, 304(%rsp)         # 8-byte Spill
	callq	trace_logger_log_double
	movq	440(%rsp), %rdi         # 8-byte Reload
	movq	328(%rsp), %rsi         # 8-byte Reload
	movq	376(%rsp), %rdx         # 8-byte Reload
	movq	432(%rsp), %rcx         # 8-byte Reload
	movq	400(%rsp), %r8          # 8-byte Reload
	movl	356(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	384(%rsp), %rdi         # 8-byte Reload
	movq	504(%rsp), %rsi         # 8-byte Reload
	movq	544(%rsp), %rdx         # 8-byte Reload
	movq	528(%rsp), %rcx         # 8-byte Reload
	movq	448(%rsp), %r8          # 8-byte Reload
	movq	416(%rsp), %r9          # 8-byte Reload
	movq	424(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	vmovss	412(%rsp), %xmm0        # 4-byte Reload
	cvtss2sd	%xmm0, %xmm0
	movq	528(%rsp), %rdi         # 8-byte Reload
	movq	336(%rsp), %rsi         # 8-byte Reload
	movq	528(%rsp), %rdx         # 8-byte Reload
	movq	456(%rsp), %rcx         # 8-byte Reload
	movq	416(%rsp), %r8          # 8-byte Reload
	movq	424(%rsp), %r9          # 8-byte Reload
	callq	trace_logger_log_double
	movq	2360(%rsp), %rcx        # 8-byte Reload
	movq	752(%rsp), %rdx         # 8-byte Reload
	vmovss	412(%rsp), %xmm0        # 4-byte Reload
	vmovss	%xmm0, (%rcx,%rdx,4)
	.loc	1 142 0                 # test_dma_load_store.c:142:0
	movq	320(%rsp), %rdi         # 8-byte Reload
	movq	328(%rsp), %rsi         # 8-byte Reload
	movq	376(%rsp), %rdx         # 8-byte Reload
	movq	496(%rsp), %rcx         # 8-byte Reload
	movq	464(%rsp), %r8          # 8-byte Reload
	movl	356(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	384(%rsp), %rdi         # 8-byte Reload
	movq	504(%rsp), %rsi         # 8-byte Reload
	movq	528(%rsp), %rdx         # 8-byte Reload
	movq	416(%rsp), %rcx         # 8-byte Reload
	movq	472(%rsp), %r8          # 8-byte Reload
	movq	416(%rsp), %r9          # 8-byte Reload
	movq	424(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	528(%rsp), %rdi         # 8-byte Reload
	movq	504(%rsp), %rsi         # 8-byte Reload
	movq	752(%rsp), %rdx         # 8-byte Reload
	movq	528(%rsp), %rcx         # 8-byte Reload
	movq	480(%rsp), %r8          # 8-byte Reload
	movq	416(%rsp), %r9          # 8-byte Reload
	movq	424(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	752(%rsp), %rcx         # 8-byte Reload
	addq	$1, %rcx
	movq	368(%rsp), %rdi         # 8-byte Reload
	movq	504(%rsp), %rsi         # 8-byte Reload
	movq	%rcx, %rdx
	movq	528(%rsp), %r8          # 8-byte Reload
	movq	%rcx, 296(%rsp)         # 8-byte Spill
	movq	%r8, %rcx
	movq	496(%rsp), %r8          # 8-byte Reload
	movq	416(%rsp), %r9          # 8-byte Reload
	movq	424(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	320(%rsp), %rdi         # 8-byte Reload
	movq	328(%rsp), %rsi         # 8-byte Reload
	movq	376(%rsp), %rdx         # 8-byte Reload
	movq	344(%rsp), %rcx         # 8-byte Reload
	movq	488(%rsp), %r8          # 8-byte Reload
	movl	356(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	528(%rsp), %rdi         # 8-byte Reload
	movq	504(%rsp), %rsi         # 8-byte Reload
	movq	296(%rsp), %rdx         # 8-byte Reload
	movq	528(%rsp), %rcx         # 8-byte Reload
	movq	496(%rsp), %r8          # 8-byte Reload
	movq	416(%rsp), %r9          # 8-byte Reload
	movq	424(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	296(%rsp), %rcx         # 8-byte Reload
	movl	%ecx, %eax
	movl	%eax, %esi
	movl	%esi, %edx
	movq	368(%rsp), %rdi         # 8-byte Reload
	movq	336(%rsp), %rsi         # 8-byte Reload
	movq	528(%rsp), %rcx         # 8-byte Reload
	movq	344(%rsp), %r8          # 8-byte Reload
	movq	416(%rsp), %r9          # 8-byte Reload
	movq	424(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movl	%eax, 292(%rsp)         # 4-byte Spill
	callq	trace_logger_log_int
	movq	320(%rsp), %rdi         # 8-byte Reload
	movq	328(%rsp), %rsi         # 8-byte Reload
	movq	376(%rsp), %rdx         # 8-byte Reload
	movq	392(%rsp), %rcx         # 8-byte Reload
	movq	512(%rsp), %r8          # 8-byte Reload
	movl	356(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movl	2468(%rsp), %eax        # 4-byte Reload
	movl	%eax, %r9d
	movl	%r9d, %edx
	movq	384(%rsp), %rdi         # 8-byte Reload
	movq	336(%rsp), %rsi         # 8-byte Reload
	movq	528(%rsp), %rcx         # 8-byte Reload
	movq	520(%rsp), %r8          # 8-byte Reload
	movq	416(%rsp), %r9          # 8-byte Reload
	movq	424(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movl	292(%rsp), %eax         # 4-byte Reload
	movl	%eax, %eax
	movl	%eax, %edx
	movq	528(%rsp), %rdi         # 8-byte Reload
	movq	336(%rsp), %rsi         # 8-byte Reload
	movq	528(%rsp), %rcx         # 8-byte Reload
	movq	344(%rsp), %r8          # 8-byte Reload
	movq	416(%rsp), %r9          # 8-byte Reload
	movq	424(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movl	292(%rsp), %eax         # 4-byte Reload
	movl	2468(%rsp), %esi        # 4-byte Reload
	cmpl	%esi, %eax
	sete	%al
	movb	%al, %cl
	andb	$1, %cl
	movzbl	%cl, %ecx
	movl	%ecx, %edx
	movq	368(%rsp), %rdi         # 8-byte Reload
	movq	528(%rsp), %rsi         # 8-byte Reload
	movq	528(%rsp), %rcx         # 8-byte Reload
	movq	392(%rsp), %r8          # 8-byte Reload
	movq	416(%rsp), %r9          # 8-byte Reload
	movq	424(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movb	%al, 291(%rsp)          # 1-byte Spill
	callq	trace_logger_log_int
	movq	320(%rsp), %rdi         # 8-byte Reload
	movq	328(%rsp), %rsi         # 8-byte Reload
	movq	376(%rsp), %rdx         # 8-byte Reload
	movq	360(%rsp), %rcx         # 8-byte Reload
	movq	384(%rsp), %r8          # 8-byte Reload
	movl	356(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	312(%rsp), %rdi         # 8-byte Reload
	movq	416(%rsp), %rsi         # 8-byte Reload
	movq	416(%rsp), %rdx         # 8-byte Reload
	movq	528(%rsp), %rcx         # 8-byte Reload
	movq	304(%rsp), %r8          # 8-byte Reload
	movq	416(%rsp), %r9          # 8-byte Reload
	movq	424(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	384(%rsp), %rdi         # 8-byte Reload
	movq	416(%rsp), %rsi         # 8-byte Reload
	movq	416(%rsp), %rdx         # 8-byte Reload
	movq	528(%rsp), %rcx         # 8-byte Reload
	movq	376(%rsp), %r8          # 8-byte Reload
	movq	416(%rsp), %r9          # 8-byte Reload
	movq	424(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	291(%rsp), %al          # 1-byte Reload
	andb	$1, %al
	movzbl	%al, %eax
	movl	%eax, %edx
	movq	528(%rsp), %rdi         # 8-byte Reload
	movq	528(%rsp), %rsi         # 8-byte Reload
	movq	528(%rsp), %rcx         # 8-byte Reload
	movq	392(%rsp), %r8          # 8-byte Reload
	movq	416(%rsp), %r9          # 8-byte Reload
	movq	424(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	291(%rsp), %al          # 1-byte Reload
	testb	$1, %al
	movq	296(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, 768(%rsp)         # 8-byte Spill
	jne	.LBB2_5
	jmp	.LBB2_4
.Ltmp85:
.LBB2_5:                                # %._crit_edge
	movabsq	$3, %rdi
	movabsq	$32, %rsi
	movabsq	$1, %rax
	leaq	.L.str12, %r8
	movabsq	$0, %rcx
	leaq	.L.str11, %rdx
	movabsq	$2, %r9
	movabsq	$64, %r10
	leaq	.L.str22, %r11
	leaq	.L.str25, %rbx
	movabsq	$4, %r14
	leaq	s1, %r15
	leaq	.L.str45, %r12
	movabsq	$159, %r13
	leaq	.L.str48, %rbp
	movq	%rax, 280(%rsp)         # 8-byte Spill
	leaq	.L.str15, %rax
	movq	%rax, 272(%rsp)         # 8-byte Spill
	leaq	.L.str44, %rax
	movq	%rax, 264(%rsp)         # 8-byte Spill
	movabsq	$49, %rax
	movq	%rsi, 256(%rsp)         # 8-byte Spill
	movl	$1, %esi
	.loc	1 159 0                 # test_dma_load_store.c:159:0
	movq	%rdi, 248(%rsp)         # 8-byte Spill
	movq	%r13, %rdi
	movl	%esi, 244(%rsp)         # 4-byte Spill
	movq	%rbp, %rsi
	movq	272(%rsp), %r13         # 8-byte Reload
	movq	%rdx, 232(%rsp)         # 8-byte Spill
	movq	%r13, %rdx
	movq	264(%rsp), %rbp         # 8-byte Reload
	movq	%rcx, 224(%rsp)         # 8-byte Spill
	movq	%rbp, %rcx
	movq	%r8, 216(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movl	244(%rsp), %ebp         # 4-byte Reload
	movq	%r9, 208(%rsp)          # 8-byte Spill
	movl	%ebp, %r9d
	movl	$1, (%rsp)
	movq	%r12, 200(%rsp)         # 8-byte Spill
	movq	%r15, 192(%rsp)         # 8-byte Spill
	movq	%r14, 184(%rsp)         # 8-byte Spill
	movq	%rbx, 176(%rsp)         # 8-byte Spill
	movq	%r11, 168(%rsp)         # 8-byte Spill
	movq	%r10, 160(%rsp)         # 8-byte Spill
	callq	trace_logger_log0
	movq	184(%rsp), %rdi         # 8-byte Reload
	movq	160(%rsp), %rsi         # 8-byte Reload
	movq	192(%rsp), %rdx         # 8-byte Reload
	movq	280(%rsp), %rcx         # 8-byte Reload
	movq	200(%rsp), %r8          # 8-byte Reload
	movq	224(%rsp), %r9          # 8-byte Reload
	movq	232(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	280(%rsp), %rdi         # 8-byte Reload
	movq	160(%rsp), %rsi         # 8-byte Reload
	movq	2368(%rsp), %rdx        # 8-byte Reload
	movq	280(%rsp), %rcx         # 8-byte Reload
	movq	176(%rsp), %r8          # 8-byte Reload
	movq	224(%rsp), %r9          # 8-byte Reload
	movq	232(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	208(%rsp), %rdi         # 8-byte Reload
	movq	160(%rsp), %rsi         # 8-byte Reload
	movq	2360(%rsp), %rdx        # 8-byte Reload
	movq	280(%rsp), %rcx         # 8-byte Reload
	movq	168(%rsp), %r8          # 8-byte Reload
	movq	224(%rsp), %r9          # 8-byte Reload
	movq	232(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movl	2468(%rsp), %ebp        # 4-byte Reload
	movl	%ebp, %esi
	movl	%esi, %edx
	movq	248(%rsp), %rdi         # 8-byte Reload
	movq	256(%rsp), %rsi         # 8-byte Reload
	movq	280(%rsp), %rcx         # 8-byte Reload
	movq	216(%rsp), %r8          # 8-byte Reload
	movq	224(%rsp), %r9          # 8-byte Reload
	movq	232(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	2368(%rsp), %rdi        # 8-byte Reload
	movq	2360(%rsp), %rsi        # 8-byte Reload
	movl	2468(%rsp), %edx        # 4-byte Reload
	callq	s1
	movabsq	$3, %rdi
	movabsq	$64, %rax
	movabsq	$1, %rcx
	leaq	.L.str23, %r8
	movabsq	$0, %rsi
	leaq	.L.str11, %r9
	movabsq	$2, %r10
	leaq	.L.str22, %r11
	movabsq	$4, %rbx
	leaq	dmaStore, %rdx
	leaq	.L.str67, %r14
	movabsq	$164, %r15
	leaq	.L.str48, %r12
	leaq	.L.str15, %r13
	leaq	.L.str66, %rbp
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movabsq	$98, %rax
	movq	%rsi, 144(%rsp)         # 8-byte Spill
	movl	$1, %esi
	.loc	1 164 0                 # test_dma_load_store.c:164:0
	movq	%rdi, 136(%rsp)         # 8-byte Spill
	movq	%r15, %rdi
	movl	%esi, 132(%rsp)         # 4-byte Spill
	movq	%r12, %rsi
	movq	%rdx, 120(%rsp)         # 8-byte Spill
	movq	%r13, %rdx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movq	%rbp, %rcx
	movq	%r8, 104(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movl	132(%rsp), %ebp         # 4-byte Reload
	movq	%r9, 96(%rsp)           # 8-byte Spill
	movl	%ebp, %r9d
	movl	$1, (%rsp)
	movq	%r14, 88(%rsp)          # 8-byte Spill
	movq	%r10, 80(%rsp)          # 8-byte Spill
	movq	%r11, 72(%rsp)          # 8-byte Spill
	movq	%rbx, 64(%rsp)          # 8-byte Spill
	callq	trace_logger_log0
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	152(%rsp), %rsi         # 8-byte Reload
	movq	120(%rsp), %rdx         # 8-byte Reload
	movq	112(%rsp), %rcx         # 8-byte Reload
	movq	88(%rsp), %r8           # 8-byte Reload
	movq	144(%rsp), %r9          # 8-byte Reload
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	112(%rsp), %rdi         # 8-byte Reload
	movq	152(%rsp), %rsi         # 8-byte Reload
	movq	2040(%rsp), %rdx        # 8-byte Reload
	movq	112(%rsp), %rcx         # 8-byte Reload
	movq	72(%rsp), %r8           # 8-byte Reload
	movq	144(%rsp), %r9          # 8-byte Reload
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	80(%rsp), %rdi          # 8-byte Reload
	movq	152(%rsp), %rsi         # 8-byte Reload
	movq	2040(%rsp), %rdx        # 8-byte Reload
	movq	112(%rsp), %rcx         # 8-byte Reload
	movq	72(%rsp), %r8           # 8-byte Reload
	movq	144(%rsp), %r9          # 8-byte Reload
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	136(%rsp), %rdi         # 8-byte Reload
	movq	152(%rsp), %rsi         # 8-byte Reload
	movq	2240(%rsp), %rdx        # 8-byte Reload
	movq	112(%rsp), %rcx         # 8-byte Reload
	movq	104(%rsp), %r8          # 8-byte Reload
	movq	144(%rsp), %r9          # 8-byte Reload
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	2040(%rsp), %rdi        # 8-byte Reload
	movq	2040(%rsp), %rsi        # 8-byte Reload
	movq	2240(%rsp), %rdx        # 8-byte Reload
	callq	dmaStore
	movabsq	$167, %rdi
	leaq	.L.str48, %rsi
	leaq	.L.str15, %rdx
	leaq	.L.str68, %rcx
	movabsq	$1, %r8
	movl	$1, %r9d
	movabsq	$19134, %r10            # imm = 0x4ABE
	movabsq	$32, %r11
	leaq	.L.str66, %rbx
	movabsq	$0, %r14
	leaq	.L.str11, %r15
	.loc	1 167 0                 # test_dma_load_store.c:167:0
	movl	%eax, %eax
	movl	%eax, %r12d
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	movq	%r10, %rdi
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	%r11, %rsi
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	%r12, %rdx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%r8, %rcx
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movq	%rbx, %r8
	movl	%r9d, 20(%rsp)          # 4-byte Spill
	movq	%r14, %r9
	movq	%r15, (%rsp)
	callq	trace_logger_log_int
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	24(%rsp), %r8           # 8-byte Reload
	movl	20(%rsp), %r9d          # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	addq	$2504, %rsp             # imm = 0x9C8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.Ltmp86:
.Ltmp87:
	.size	big, .Ltmp87-big
.Lfunc_end2:
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.align	4
.LCPI3_0:
	.long	1065353216              # float 1
	.section	.rodata.cst8,"aM",@progbits,8
	.align	8
.LCPI3_1:
	.quad	4607182418800017408     # double 1
	.text
	.align	16, 0x90
	.type	soft_plus,@function
soft_plus:                              # @soft_plus
	.cfi_startproc
.Lfunc_begin3:
	.loc	1 67 0                  # test_dma_load_store.c:67:0
# BB#0:
	pushq	%rbp
.Ltmp95:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Ltmp96:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Ltmp97:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Ltmp98:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Ltmp99:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Ltmp100:
	.cfi_def_cfa_offset 56
	subq	$408, %rsp              # imm = 0x198
.Ltmp101:
	.cfi_def_cfa_offset 464
.Ltmp102:
	.cfi_offset %rbx, -56
.Ltmp103:
	.cfi_offset %r12, -48
.Ltmp104:
	.cfi_offset %r13, -40
.Ltmp105:
	.cfi_offset %r14, -32
.Ltmp106:
	.cfi_offset %r15, -24
.Ltmp107:
	.cfi_offset %rbp, -16
	movabsq	$1, %rax
	movabsq	$32, %rcx
	leaq	.L.str70, %rdx
	movabsq	$0, %rsi
	leaq	.L.str11, %rdi
	movabsq	$2, %r8
	movabsq	$64, %r9
	leaq	expf, %r10
	leaq	.L.str69, %r11
	movabsq	$68, %rbx
	leaq	.L.str58, %r14
	leaq	.L.str8, %r15
	leaq	.L.str9, %r12
	movabsq	$49, %r13
	movl	$0, %ebp
	movq	%rsi, 400(%rsp)         # 8-byte Spill
	movl	$1, %esi
	movq	%rax, 392(%rsp)         # 8-byte Spill
	movabsq	$24601, %rax            # imm = 0x6019
	.loc	1 67 0 prologue_end     # test_dma_load_store.c:67:0
.Ltmp108:
	cvtss2sd	%xmm0, %xmm1
	movq	%rdi, 384(%rsp)         # 8-byte Spill
	movq	%rax, %rdi
	movl	%esi, 380(%rsp)         # 4-byte Spill
	movq	%rcx, %rsi
	vmovss	%xmm0, 376(%rsp)        # 4-byte Spill
	vmovaps	%xmm1, %xmm0
	movq	392(%rsp), %rax         # 8-byte Reload
	movq	%rdx, 368(%rsp)         # 8-byte Spill
	movq	%rax, %rdx
	movq	368(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 360(%rsp)         # 8-byte Spill
	movq	%rax, %rcx
	movq	400(%rsp), %rax         # 8-byte Reload
	movq	%r8, 352(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movq	384(%rsp), %rax         # 8-byte Reload
	movq	%r9, 344(%rsp)          # 8-byte Spill
	movq	%rax, %r9
	movq	%r15, 336(%rsp)         # 8-byte Spill
	movq	%r12, 328(%rsp)         # 8-byte Spill
	movq	%r14, 320(%rsp)         # 8-byte Spill
	movq	%r13, 312(%rsp)         # 8-byte Spill
	movl	%ebp, 308(%rsp)         # 4-byte Spill
	movq	%rbx, 296(%rsp)         # 8-byte Spill
	movq	%r11, 288(%rsp)         # 8-byte Spill
	movq	%r10, 280(%rsp)         # 8-byte Spill
	callq	trace_logger_log_double
	#DEBUG_VALUE: soft_plus:x <- [RSP+376]
	.loc	1 68 0                  # test_dma_load_store.c:68:0
	movq	296(%rsp), %rdi         # 8-byte Reload
	movq	320(%rsp), %rsi         # 8-byte Reload
	movq	336(%rsp), %rdx         # 8-byte Reload
	movq	328(%rsp), %rcx         # 8-byte Reload
	movq	312(%rsp), %r8          # 8-byte Reload
	movl	308(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	352(%rsp), %rdi         # 8-byte Reload
	movq	344(%rsp), %rsi         # 8-byte Reload
	movq	280(%rsp), %rdx         # 8-byte Reload
	movq	392(%rsp), %rcx         # 8-byte Reload
	movq	288(%rsp), %r8          # 8-byte Reload
	movq	400(%rsp), %r9          # 8-byte Reload
	movq	384(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	vmovss	376(%rsp), %xmm0        # 4-byte Reload
	cvtss2sd	%xmm0, %xmm0
	movq	392(%rsp), %rdi         # 8-byte Reload
	movq	360(%rsp), %rsi         # 8-byte Reload
	movq	392(%rsp), %rdx         # 8-byte Reload
	movq	368(%rsp), %rcx         # 8-byte Reload
	movq	400(%rsp), %r8          # 8-byte Reload
	movq	384(%rsp), %r9          # 8-byte Reload
	callq	trace_logger_log_double
	vmovss	376(%rsp), %xmm0        # 4-byte Reload
	callq	expf
.Ltmp109:
	movabsq	$1, %rax
	movabsq	$32, %rcx
	leaq	.L.str21, %rdx
	movabsq	$0, %rsi
	leaq	.L.str11, %rdi
	movabsq	$2, %r8
	movabsq	$64, %r9
	leaq	logf, %r10
	leaq	.L.str71, %r11
	movabsq	$68, %rbx
	leaq	.L.str58, %r14
	leaq	.L.str8, %r15
	leaq	.L.str23, %r12
	movabsq	$49, %r13
	movl	$0, %ebp
	movq	%rsi, 272(%rsp)         # 8-byte Spill
	movl	$1, %esi
	movq	%rax, 264(%rsp)         # 8-byte Spill
	movabsq	$19134, %rax            # imm = 0x4ABE
	vmovss	.LCPI3_0, %xmm1
	movq	%rax, 256(%rsp)         # 8-byte Spill
	leaq	.L.str9, %rax
	vmovsd	.LCPI3_1, %xmm2
	movq	%rax, 248(%rsp)         # 8-byte Spill
	leaq	.L.str10, %rax
	movq	%rax, 240(%rsp)         # 8-byte Spill
	movabsq	$9, %rax
	cvtss2sd	%xmm0, %xmm3
	movq	%rax, 232(%rsp)         # 8-byte Spill
	movq	256(%rsp), %rax         # 8-byte Reload
	movq	%rdi, 224(%rsp)         # 8-byte Spill
	movq	%rax, %rdi
	movl	%esi, 220(%rsp)         # 4-byte Spill
	movq	%rcx, %rsi
	vmovss	%xmm0, 216(%rsp)        # 4-byte Spill
	vmovaps	%xmm3, %xmm0
	movq	264(%rsp), %rax         # 8-byte Reload
	movq	%rdx, 208(%rsp)         # 8-byte Spill
	movq	%rax, %rdx
	movq	248(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 200(%rsp)         # 8-byte Spill
	movq	%rax, %rcx
	movq	272(%rsp), %rax         # 8-byte Reload
	movq	%r8, 192(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movq	224(%rsp), %rax         # 8-byte Reload
	movq	%r9, 184(%rsp)          # 8-byte Spill
	movq	%rax, %r9
	movl	%ebp, 180(%rsp)         # 4-byte Spill
	movq	%r12, 168(%rsp)         # 8-byte Spill
	movq	%r13, 160(%rsp)         # 8-byte Spill
	vmovsd	%xmm2, 152(%rsp)        # 8-byte Spill
	vmovss	%xmm1, 148(%rsp)        # 4-byte Spill
	movq	%r15, 136(%rsp)         # 8-byte Spill
	movq	%r14, 128(%rsp)         # 8-byte Spill
	movq	%r10, 120(%rsp)         # 8-byte Spill
	movq	%r11, 112(%rsp)         # 8-byte Spill
	movq	%rbx, 104(%rsp)         # 8-byte Spill
	callq	trace_logger_log_double
	movq	104(%rsp), %rdi         # 8-byte Reload
	movq	128(%rsp), %rsi         # 8-byte Reload
	movq	136(%rsp), %rdx         # 8-byte Reload
	movq	208(%rsp), %rcx         # 8-byte Reload
	movq	232(%rsp), %r8          # 8-byte Reload
	movl	180(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	192(%rsp), %rdi         # 8-byte Reload
	movq	200(%rsp), %rsi         # 8-byte Reload
	vmovsd	152(%rsp), %xmm0        # 8-byte Reload
	movq	272(%rsp), %rdx         # 8-byte Reload
	movq	240(%rsp), %rcx         # 8-byte Reload
	movq	272(%rsp), %r8          # 8-byte Reload
	movq	224(%rsp), %r9          # 8-byte Reload
	callq	trace_logger_log_double
	vmovss	216(%rsp), %xmm0        # 4-byte Reload
	cvtss2sd	%xmm0, %xmm0
	movq	264(%rsp), %rdi         # 8-byte Reload
	movq	200(%rsp), %rsi         # 8-byte Reload
	movq	264(%rsp), %rdx         # 8-byte Reload
	movq	248(%rsp), %rcx         # 8-byte Reload
	movq	272(%rsp), %r8          # 8-byte Reload
	movq	224(%rsp), %r9          # 8-byte Reload
	callq	trace_logger_log_double
	vmovss	216(%rsp), %xmm0        # 4-byte Reload
	vmovss	148(%rsp), %xmm1        # 4-byte Reload
	vaddss	%xmm1, %xmm0, %xmm2
	cvtss2sd	%xmm2, %xmm0
	movq	256(%rsp), %rdi         # 8-byte Reload
	movq	200(%rsp), %rsi         # 8-byte Reload
	movq	264(%rsp), %rdx         # 8-byte Reload
	movq	208(%rsp), %rcx         # 8-byte Reload
	movq	272(%rsp), %r8          # 8-byte Reload
	movq	224(%rsp), %r9          # 8-byte Reload
	vmovss	%xmm2, 100(%rsp)        # 4-byte Spill
	callq	trace_logger_log_double
	movq	104(%rsp), %rdi         # 8-byte Reload
	movq	128(%rsp), %rsi         # 8-byte Reload
	movq	136(%rsp), %rdx         # 8-byte Reload
	movq	168(%rsp), %rcx         # 8-byte Reload
	movq	160(%rsp), %r8          # 8-byte Reload
	movl	180(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	192(%rsp), %rdi         # 8-byte Reload
	movq	184(%rsp), %rsi         # 8-byte Reload
	movq	120(%rsp), %rdx         # 8-byte Reload
	movq	264(%rsp), %rcx         # 8-byte Reload
	movq	112(%rsp), %r8          # 8-byte Reload
	movq	272(%rsp), %r9          # 8-byte Reload
	movq	224(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	vmovss	100(%rsp), %xmm0        # 4-byte Reload
	cvtss2sd	%xmm0, %xmm0
	movq	264(%rsp), %rdi         # 8-byte Reload
	movq	200(%rsp), %rsi         # 8-byte Reload
	movq	264(%rsp), %rdx         # 8-byte Reload
	movq	208(%rsp), %rcx         # 8-byte Reload
	movq	272(%rsp), %r8          # 8-byte Reload
	movq	224(%rsp), %r9          # 8-byte Reload
	callq	trace_logger_log_double
	vmovss	100(%rsp), %xmm0        # 4-byte Reload
	callq	logf
	movabsq	$1, %rax
	movabsq	$32, %rcx
	leaq	.L.str23, %rdx
	movabsq	$0, %rsi
	leaq	.L.str11, %rdi
	movabsq	$68, %r8
	leaq	.L.str58, %r9
	leaq	.L.str8, %r10
	leaq	.L.str72, %r11
	movl	$0, %ebp
	movl	$1, %r14d
	movabsq	$19134, %rbx            # imm = 0x4ABE
	cvtss2sd	%xmm0, %xmm1
	movq	%rdi, 88(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movq	%rcx, %rsi
	vmovss	%xmm0, 76(%rsp)         # 4-byte Spill
	vmovaps	%xmm1, %xmm0
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movq	%rax, %rdx
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	%rbx, %rcx
	movq	80(%rsp), %r15          # 8-byte Reload
	movq	%r8, 48(%rsp)           # 8-byte Spill
	movq	%r15, %r8
	movq	88(%rsp), %r12          # 8-byte Reload
	movq	%r9, 40(%rsp)           # 8-byte Spill
	movq	%r12, %r9
	movq	%r11, 32(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%r10, 16(%rsp)          # 8-byte Spill
	movl	%r14d, 12(%rsp)         # 4-byte Spill
	movl	%ebp, 8(%rsp)           # 4-byte Spill
	callq	trace_logger_log_double
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	24(%rsp), %r8           # 8-byte Reload
	movl	8(%rsp), %r9d           # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	vmovss	76(%rsp), %xmm0         # 4-byte Reload
	cvtss2sd	%xmm0, %xmm0
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	80(%rsp), %r8           # 8-byte Reload
	movq	88(%rsp), %r9           # 8-byte Reload
	callq	trace_logger_log_double
	vmovss	76(%rsp), %xmm0         # 4-byte Reload
	addq	$408, %rsp              # imm = 0x198
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.Ltmp110:
.Ltmp111:
	.size	soft_plus, .Ltmp111-soft_plus
.Lfunc_end3:
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.align	4
.LCPI4_0:
	.long	3212836864              # float -1
	.section	.rodata.cst8,"aM",@progbits,8
	.align	8
.LCPI4_1:
	.quad	-4616189618054758400    # double -1
	.text
	.globl	main
	.align	16, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
.Lfunc_begin4:
	.loc	1 169 0                 # test_dma_load_store.c:169:0
# BB#0:
	pushq	%rbp
.Ltmp119:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Ltmp120:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Ltmp121:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Ltmp122:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Ltmp123:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Ltmp124:
	.cfi_def_cfa_offset 56
	subq	$3608, %rsp             # imm = 0xE18
.Ltmp125:
	.cfi_def_cfa_offset 3664
.Ltmp126:
	.cfi_offset %rbx, -56
.Ltmp127:
	.cfi_offset %r12, -48
.Ltmp128:
	.cfi_offset %r13, -40
.Ltmp129:
	.cfi_offset %r14, -32
.Ltmp130:
	.cfi_offset %r15, -24
.Ltmp131:
	.cfi_offset %rbp, -16
	leaq	.L.str120, %rdi
	movabsq	$11, %rsi
	callq	trace_logger_write_labelmap
	movabsq	$64, %rsi
	movabsq	$4096, %rdi             # imm = 0x1000
	movabsq	$3, %rax
	movabsq	$0, %rcx
	leaq	.L.str10, %rdx
	leaq	.L.str11, %r8
	movabsq	$2, %r9
	movabsq	$1, %r10
	leaq	.L.str25, %r11
	movabsq	$4, %rbx
	leaq	posix_memalign, %r14
	leaq	.L.str74, %r15
	movabsq	$172, %r12
	leaq	.L.str73, %r13
	leaq	.L.str8, %rbp
	movq	%rax, 3584(%rsp)        # 8-byte Spill
	leaq	.L.str21, %rax
	movq	%rax, 3576(%rsp)        # 8-byte Spill
	movabsq	$49, %rax
	movq	%rsi, 3568(%rsp)        # 8-byte Spill
	movl	$0, %esi
	movl	%esi, 3564(%rsp)        # 4-byte Spill
	movl	$1, %esi
	movq	%rax, 3552(%rsp)        # 8-byte Spill
	movabsq	$19134, %rax            # imm = 0x4ABE
	movq	%rax, 3544(%rsp)        # 8-byte Spill
	leaq	3600(%rsp), %rax
	movq	%rax, 3536(%rsp)        # 8-byte Spill
	leaq	.L.str9, %rax
	movq	%rax, 3528(%rsp)        # 8-byte Spill
	movabsq	$44, %rax
	movq	%rax, 3520(%rsp)        # 8-byte Spill
	leaq	.L.str22, %rax
	movq	%rax, 3512(%rsp)        # 8-byte Spill
	leaq	3592(%rsp), %rax
	movq	%rax, 3504(%rsp)        # 8-byte Spill
	movabsq	$32, %rax
	movq	%rax, 3496(%rsp)        # 8-byte Spill
	movabsq	$-1, %rax
	movq	%rax, 3488(%rsp)        # 8-byte Spill
	movabsq	$26, %rax
	movq	%rax, 3480(%rsp)        # 8-byte Spill
	movq	3488(%rsp), %rax        # 8-byte Reload
	movq	%rdi, 3472(%rsp)        # 8-byte Spill
	movq	%rax, %rdi
	movl	%esi, 3468(%rsp)        # 4-byte Spill
	movq	%r13, %rsi
	movq	%rdx, 3456(%rsp)        # 8-byte Spill
	movq	%rbp, %rdx
	movq	%rcx, 3448(%rsp)        # 8-byte Spill
	movq	%r11, %rcx
	movq	3480(%rsp), %rax        # 8-byte Reload
	movq	%r8, 3440(%rsp)         # 8-byte Spill
	movq	%rax, %r8
	movl	3564(%rsp), %eax        # 4-byte Reload
	movq	%r9, 3432(%rsp)         # 8-byte Spill
	movl	%eax, %r9d
	movl	$1, (%rsp)
	movq	%r13, 3424(%rsp)        # 8-byte Spill
	movq	%rbp, 3416(%rsp)        # 8-byte Spill
	movq	%r15, 3408(%rsp)        # 8-byte Spill
	movq	%r12, 3400(%rsp)        # 8-byte Spill
	movq	%r14, 3392(%rsp)        # 8-byte Spill
	movq	%rbx, 3384(%rsp)        # 8-byte Spill
	movq	%r10, 3376(%rsp)        # 8-byte Spill
	movq	%r11, 3368(%rsp)        # 8-byte Spill
	callq	trace_logger_log0
	movq	3376(%rsp), %rdi        # 8-byte Reload
	movq	3496(%rsp), %rsi        # 8-byte Reload
	movq	3376(%rsp), %rdx        # 8-byte Reload
	movq	3448(%rsp), %rcx        # 8-byte Reload
	movq	3456(%rsp), %r8         # 8-byte Reload
	movq	3448(%rsp), %r9         # 8-byte Reload
	movq	3440(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	3544(%rsp), %rdi        # 8-byte Reload
	movq	3568(%rsp), %rsi        # 8-byte Reload
	movq	3536(%rsp), %rdx        # 8-byte Reload
	movq	3376(%rsp), %rcx        # 8-byte Reload
	movq	3368(%rsp), %r8         # 8-byte Reload
	movq	3448(%rsp), %r9         # 8-byte Reload
	movq	3440(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	3488(%rsp), %rdi        # 8-byte Reload
	movq	3424(%rsp), %rsi        # 8-byte Reload
	movq	3416(%rsp), %rdx        # 8-byte Reload
	movq	3512(%rsp), %rcx        # 8-byte Reload
	movq	3480(%rsp), %r8         # 8-byte Reload
	movl	3564(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	3376(%rsp), %rdi        # 8-byte Reload
	movq	3496(%rsp), %rsi        # 8-byte Reload
	movq	3376(%rsp), %rdx        # 8-byte Reload
	movq	3448(%rsp), %rcx        # 8-byte Reload
	movq	3456(%rsp), %r8         # 8-byte Reload
	movq	3448(%rsp), %r9         # 8-byte Reload
	movq	3440(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	.loc	1 170 0 prologue_end    # test_dma_load_store.c:170:0
.Ltmp132:
	movq	3544(%rsp), %rdi        # 8-byte Reload
	movq	3568(%rsp), %rsi        # 8-byte Reload
	movq	3504(%rsp), %rdx        # 8-byte Reload
	movq	3376(%rsp), %rcx        # 8-byte Reload
	movq	3512(%rsp), %r8         # 8-byte Reload
	movq	3448(%rsp), %r9         # 8-byte Reload
	movq	3440(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
.Ltmp133:
	#DEBUG_VALUE: main:num_vals <- 1024
	#DEBUG_VALUE: main:store_vals <- [RSP+3536]
	.loc	1 172 0                 # test_dma_load_store.c:172:0
	movq	3400(%rsp), %rdi        # 8-byte Reload
	movq	3424(%rsp), %rsi        # 8-byte Reload
	movq	3416(%rsp), %rdx        # 8-byte Reload
	movq	3528(%rsp), %rcx        # 8-byte Reload
	movq	3520(%rsp), %r8         # 8-byte Reload
	movl	3564(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	3376(%rsp), %rdi        # 8-byte Reload
	movq	3568(%rsp), %rsi        # 8-byte Reload
	movq	3536(%rsp), %rdx        # 8-byte Reload
	movq	3376(%rsp), %rcx        # 8-byte Reload
	movq	3368(%rsp), %r8         # 8-byte Reload
	movq	3448(%rsp), %r9         # 8-byte Reload
	movq	3440(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	3536(%rsp), %rcx        # 8-byte Reload
	movq	3544(%rsp), %rdi        # 8-byte Reload
	movq	3568(%rsp), %rsi        # 8-byte Reload
	movq	%rcx, %rdx
	movq	3376(%rsp), %r8         # 8-byte Reload
	movq	%rcx, 3360(%rsp)        # 8-byte Spill
	movq	%r8, %rcx
	movq	3368(%rsp), %r8         # 8-byte Reload
	movq	3448(%rsp), %r9         # 8-byte Reload
	movq	3440(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	3400(%rsp), %rdi        # 8-byte Reload
	movq	3424(%rsp), %rsi        # 8-byte Reload
	movq	3416(%rsp), %rdx        # 8-byte Reload
	movq	3576(%rsp), %rcx        # 8-byte Reload
	movq	3552(%rsp), %r8         # 8-byte Reload
	movl	3564(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	3384(%rsp), %rdi        # 8-byte Reload
	movq	3568(%rsp), %rsi        # 8-byte Reload
	movq	3392(%rsp), %rdx        # 8-byte Reload
	movq	3376(%rsp), %rcx        # 8-byte Reload
	movq	3408(%rsp), %r8         # 8-byte Reload
	movq	3448(%rsp), %r9         # 8-byte Reload
	movq	3440(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	3376(%rsp), %rdi        # 8-byte Reload
	movq	3568(%rsp), %rsi        # 8-byte Reload
	movq	3360(%rsp), %rdx        # 8-byte Reload
	movq	3376(%rsp), %rcx        # 8-byte Reload
	movq	3368(%rsp), %r8         # 8-byte Reload
	movq	3448(%rsp), %r9         # 8-byte Reload
	movq	3440(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	3432(%rsp), %rdi        # 8-byte Reload
	movq	3568(%rsp), %rsi        # 8-byte Reload
	movq	3568(%rsp), %rdx        # 8-byte Reload
	movq	3448(%rsp), %rcx        # 8-byte Reload
	movq	3456(%rsp), %r8         # 8-byte Reload
	movq	3448(%rsp), %r9         # 8-byte Reload
	movq	3440(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	3584(%rsp), %rdi        # 8-byte Reload
	movq	3568(%rsp), %rsi        # 8-byte Reload
	movq	3472(%rsp), %rdx        # 8-byte Reload
	movq	3448(%rsp), %rcx        # 8-byte Reload
	movq	3456(%rsp), %r8         # 8-byte Reload
	movq	3448(%rsp), %r9         # 8-byte Reload
	movq	3440(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	3360(%rsp), %rdi        # 8-byte Reload
	movq	3568(%rsp), %rsi        # 8-byte Reload
	movq	3472(%rsp), %rdx        # 8-byte Reload
	callq	posix_memalign
	movabsq	$1, %rcx
	leaq	.L.str23, %rdx
	movabsq	$0, %rsi
	leaq	.L.str11, %rdi
	movabsq	$2, %r8
	leaq	.L.str76, %r9
	movabsq	$3, %r10
	leaq	.L.str75, %r11
	movabsq	$174, %rbx
	leaq	.L.str73, %r14
	leaq	.L.str8, %r15
	leaq	.L.str46, %r12
	movl	$0, %ebp
	movl	$1, %r13d
	movq	%rcx, 3352(%rsp)        # 8-byte Spill
	movabsq	$19134, %rcx            # imm = 0x4ABE
	movq	%rcx, 3344(%rsp)        # 8-byte Spill
	movabsq	$32, %rcx
	movq	%rcx, 3336(%rsp)        # 8-byte Spill
	leaq	.L.str21, %rcx
	movq	%rcx, 3328(%rsp)        # 8-byte Spill
	leaq	.L.str10, %rcx
	movq	%rcx, 3320(%rsp)        # 8-byte Spill
	movabsq	$46, %rcx
	movl	%eax, 3316(%rsp)        # 4-byte Spill
	movl	%eax, %eax
                                        # kill: RAX<def> EAX<kill>
	movq	%rax, 3304(%rsp)        # 8-byte Spill
	movq	3344(%rsp), %rax        # 8-byte Reload
	movq	%rdi, 3296(%rsp)        # 8-byte Spill
	movq	%rax, %rdi
	movq	3336(%rsp), %rax        # 8-byte Reload
	movq	%rsi, 3288(%rsp)        # 8-byte Spill
	movq	%rax, %rsi
	movq	3304(%rsp), %rax        # 8-byte Reload
	movq	%rdx, 3280(%rsp)        # 8-byte Spill
	movq	%rax, %rdx
	movq	3352(%rsp), %rax        # 8-byte Reload
	movq	%rcx, 3272(%rsp)        # 8-byte Spill
	movq	%rax, %rcx
	movq	3328(%rsp), %rax        # 8-byte Reload
	movq	%r8, 3264(%rsp)         # 8-byte Spill
	movq	%rax, %r8
	movq	3288(%rsp), %rax        # 8-byte Reload
	movq	%r9, 3256(%rsp)         # 8-byte Spill
	movq	%rax, %r9
	movq	3296(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	movl	%r13d, 3252(%rsp)       # 4-byte Spill
	movq	%r15, 3240(%rsp)        # 8-byte Spill
	movq	%r12, 3232(%rsp)        # 8-byte Spill
	movl	%ebp, 3228(%rsp)        # 4-byte Spill
	movq	%r14, 3216(%rsp)        # 8-byte Spill
	movq	%rbx, 3208(%rsp)        # 8-byte Spill
	movq	%r10, 3200(%rsp)        # 8-byte Spill
	movq	%r11, 3192(%rsp)        # 8-byte Spill
	callq	trace_logger_log_int
.Ltmp134:
	#DEBUG_VALUE: main:err <- [RSP+3316]
	.loc	1 174 0                 # test_dma_load_store.c:174:0
	movq	3208(%rsp), %rdi        # 8-byte Reload
	movq	3216(%rsp), %rsi        # 8-byte Reload
	movq	3240(%rsp), %rdx        # 8-byte Reload
	movq	3280(%rsp), %rcx        # 8-byte Reload
	movq	3272(%rsp), %r8         # 8-byte Reload
	movl	3228(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	3264(%rsp), %rdi        # 8-byte Reload
	movq	3336(%rsp), %rsi        # 8-byte Reload
	movq	3288(%rsp), %rdx        # 8-byte Reload
	movq	3288(%rsp), %rcx        # 8-byte Reload
	movq	3320(%rsp), %r8         # 8-byte Reload
	movq	3288(%rsp), %r9         # 8-byte Reload
	movq	3296(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	3316(%rsp), %ebp        # 4-byte Reload
	movl	%ebp, %r13d
	movl	%r13d, %edx
	movq	3352(%rsp), %rdi        # 8-byte Reload
	movq	3336(%rsp), %rsi        # 8-byte Reload
	movq	3352(%rsp), %rcx        # 8-byte Reload
	movq	3328(%rsp), %r8         # 8-byte Reload
	movq	3288(%rsp), %r9         # 8-byte Reload
	movq	3296(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	3316(%rsp), %ebp        # 4-byte Reload
	cmpl	$0, %ebp
	sete	%al
	movb	%al, %cl
	andb	$1, %cl
	movzbl	%cl, %r13d
	movl	%r13d, %edx
	movq	3344(%rsp), %rdi        # 8-byte Reload
	movq	3352(%rsp), %rsi        # 8-byte Reload
	movq	3352(%rsp), %rcx        # 8-byte Reload
	movq	3280(%rsp), %r8         # 8-byte Reload
	movq	3288(%rsp), %r9         # 8-byte Reload
	movq	3296(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	movb	%al, 3191(%rsp)         # 1-byte Spill
	callq	trace_logger_log_int
	movq	3208(%rsp), %rdi        # 8-byte Reload
	movq	3216(%rsp), %rsi        # 8-byte Reload
	movq	3240(%rsp), %rdx        # 8-byte Reload
	movq	3232(%rsp), %rcx        # 8-byte Reload
	movq	3264(%rsp), %r8         # 8-byte Reload
	movl	3228(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	3200(%rsp), %rdi        # 8-byte Reload
	movq	3288(%rsp), %rsi        # 8-byte Reload
	movq	3288(%rsp), %rdx        # 8-byte Reload
	movq	3352(%rsp), %rcx        # 8-byte Reload
	movq	3192(%rsp), %r8         # 8-byte Reload
	movq	3288(%rsp), %r9         # 8-byte Reload
	movq	3296(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	3264(%rsp), %rdi        # 8-byte Reload
	movq	3288(%rsp), %rsi        # 8-byte Reload
	movq	3288(%rsp), %rdx        # 8-byte Reload
	movq	3352(%rsp), %rcx        # 8-byte Reload
	movq	3256(%rsp), %r8         # 8-byte Reload
	movq	3288(%rsp), %r9         # 8-byte Reload
	movq	3296(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	3191(%rsp), %al         # 1-byte Reload
	andb	$1, %al
	movzbl	%al, %ebp
	movl	%ebp, %edx
	movq	3352(%rsp), %rdi        # 8-byte Reload
	movq	3352(%rsp), %rsi        # 8-byte Reload
	movq	3352(%rsp), %rcx        # 8-byte Reload
	movq	3280(%rsp), %r8         # 8-byte Reload
	movq	3288(%rsp), %r9         # 8-byte Reload
	movq	3296(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	3191(%rsp), %al         # 1-byte Reload
	testb	$1, %al
	jne	.LBB4_2
.Ltmp135:
# BB#1:
	leaq	.L.str2, %rax
	leaq	.L.str3, %rcx
	movl	$174, %edx
	leaq	.L__PRETTY_FUNCTION__.main, %rsi
	movabsq	$4, %rdi
	movabsq	$64, %r8
	movabsq	$0, %r9
	leaq	.L.str10, %r10
	leaq	.L.str11, %r11
	movabsq	$3, %rbx
	movabsq	$32, %r14
	movabsq	$174, %r15
	movabsq	$2, %r12
	movabsq	$1, %r13
	movabsq	$5, %rbp
	movq	%rax, 3176(%rsp)        # 8-byte Spill
	leaq	__assert_fail, %rax
	movq	%rax, 3168(%rsp)        # 8-byte Spill
	leaq	.L.str78, %rax
	movq	%rax, 3160(%rsp)        # 8-byte Spill
	leaq	.L.str73, %rax
	movq	%rax, 3152(%rsp)        # 8-byte Spill
	leaq	.L.str76, %rax
	movq	%rax, 3144(%rsp)        # 8-byte Spill
	leaq	.L.str77, %rax
	movq	%rax, 3136(%rsp)        # 8-byte Spill
	movabsq	$49, %rax
	movl	%edx, 3132(%rsp)        # 4-byte Spill
	movl	$0, %edx
	movl	%edx, 3128(%rsp)        # 4-byte Spill
	movl	$1, %edx
	movq	%rdi, 3120(%rsp)        # 8-byte Spill
	movq	%r15, %rdi
	movq	%rax, 3112(%rsp)        # 8-byte Spill
	movq	3152(%rsp), %rax        # 8-byte Reload
	movq	%rsi, 3104(%rsp)        # 8-byte Spill
	movq	%rax, %rsi
	movq	3144(%rsp), %rax        # 8-byte Reload
	movl	%edx, 3100(%rsp)        # 4-byte Spill
	movq	%rax, %rdx
	movq	3136(%rsp), %rax        # 8-byte Reload
	movq	%rcx, 3088(%rsp)        # 8-byte Spill
	movq	%rax, %rcx
	movq	3112(%rsp), %rax        # 8-byte Reload
	movq	%r8, 3080(%rsp)         # 8-byte Spill
	movq	%rax, %r8
	movl	3128(%rsp), %eax        # 4-byte Reload
	movq	%r9, 3072(%rsp)         # 8-byte Spill
	movl	%eax, %r9d
	movl	$1, (%rsp)
	movq	%r15, 3064(%rsp)        # 8-byte Spill
	movq	%r12, 3056(%rsp)        # 8-byte Spill
	movq	%r13, 3048(%rsp)        # 8-byte Spill
	movq	%rbp, 3040(%rsp)        # 8-byte Spill
	movq	%r14, 3032(%rsp)        # 8-byte Spill
	movq	%rbx, 3024(%rsp)        # 8-byte Spill
	movq	%r10, 3016(%rsp)        # 8-byte Spill
	movq	%r11, 3008(%rsp)        # 8-byte Spill
	callq	trace_logger_log0
	movq	3040(%rsp), %rdi        # 8-byte Reload
	movq	3080(%rsp), %rsi        # 8-byte Reload
	movq	3168(%rsp), %rdx        # 8-byte Reload
	movq	3048(%rsp), %rcx        # 8-byte Reload
	movq	3160(%rsp), %r8         # 8-byte Reload
	movq	3072(%rsp), %r9         # 8-byte Reload
	movq	3008(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	3048(%rsp), %rdi        # 8-byte Reload
	movq	3080(%rsp), %rsi        # 8-byte Reload
	movq	3176(%rsp), %rdx        # 8-byte Reload
	movq	3072(%rsp), %rcx        # 8-byte Reload
	movq	3016(%rsp), %r8         # 8-byte Reload
	movq	3072(%rsp), %r9         # 8-byte Reload
	movq	3008(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	3056(%rsp), %rdi        # 8-byte Reload
	movq	3080(%rsp), %rsi        # 8-byte Reload
	movq	3088(%rsp), %rdx        # 8-byte Reload
	movq	3072(%rsp), %rcx        # 8-byte Reload
	movq	3016(%rsp), %r8         # 8-byte Reload
	movq	3072(%rsp), %r9         # 8-byte Reload
	movq	3008(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	3024(%rsp), %rdi        # 8-byte Reload
	movq	3032(%rsp), %rsi        # 8-byte Reload
	movq	3064(%rsp), %rdx        # 8-byte Reload
	movq	3072(%rsp), %rcx        # 8-byte Reload
	movq	3016(%rsp), %r8         # 8-byte Reload
	movq	3072(%rsp), %r9         # 8-byte Reload
	movq	3008(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	3120(%rsp), %rdi        # 8-byte Reload
	movq	3080(%rsp), %rsi        # 8-byte Reload
	movq	3104(%rsp), %rdx        # 8-byte Reload
	movq	3072(%rsp), %rcx        # 8-byte Reload
	movq	3016(%rsp), %r8         # 8-byte Reload
	movq	3072(%rsp), %r9         # 8-byte Reload
	movq	3008(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	3176(%rsp), %rdi        # 8-byte Reload
	movq	3088(%rsp), %rsi        # 8-byte Reload
	movl	3132(%rsp), %edx        # 4-byte Reload
	movq	3104(%rsp), %rcx        # 8-byte Reload
	callq	__assert_fail
	movabsq	$174, %rdi
	leaq	.L.str73, %rsi
	leaq	.L.str76, %rdx
	leaq	.L.str79, %rcx
	movabsq	$7, %r8
	movl	$0, %r9d
	movl	$1, %eax
	movl	$1, (%rsp)
	movl	%eax, 3004(%rsp)        # 4-byte Spill
	callq	trace_logger_log0
.LBB4_2:
	movabsq	$64, %rax
	movabsq	$4096, %rcx             # imm = 0x1000
	movabsq	$3, %rdi
	movabsq	$0, %rdx
	leaq	.L.str10, %rsi
	leaq	.L.str11, %r8
	movabsq	$2, %r9
	movabsq	$1, %r10
	leaq	.L.str22, %r11
	movabsq	$4, %rbx
	leaq	posix_memalign, %r14
	leaq	.L.str74, %r15
	movabsq	$175, %r12
	leaq	.L.str73, %r13
	leaq	.L.str75, %rbp
	movq	%rax, 2992(%rsp)        # 8-byte Spill
	leaq	.L.str28, %rax
	movq	%rax, 2984(%rsp)        # 8-byte Spill
	movabsq	$49, %rax
	movq	%rsi, 2976(%rsp)        # 8-byte Spill
	movl	$0, %esi
	movl	%esi, 2972(%rsp)        # 4-byte Spill
	movl	$1, %esi
	movq	%rax, 2960(%rsp)        # 8-byte Spill
	movabsq	$19134, %rax            # imm = 0x4ABE
	movq	%rax, 2952(%rsp)        # 8-byte Spill
	leaq	3592(%rsp), %rax
	movq	%rax, 2944(%rsp)        # 8-byte Spill
	leaq	.L.str27, %rax
	movq	%rax, 2936(%rsp)        # 8-byte Spill
	movabsq	$44, %rax
	.loc	1 175 0                 # test_dma_load_store.c:175:0
	movq	%rdi, 2928(%rsp)        # 8-byte Spill
	movq	%r12, %rdi
	movl	%esi, 2924(%rsp)        # 4-byte Spill
	movq	%r13, %rsi
	movq	%rdx, 2912(%rsp)        # 8-byte Spill
	movq	%rbp, %rdx
	movq	%rax, 2904(%rsp)        # 8-byte Spill
	movq	2936(%rsp), %rax        # 8-byte Reload
	movq	%rcx, 2896(%rsp)        # 8-byte Spill
	movq	%rax, %rcx
	movq	2904(%rsp), %rax        # 8-byte Reload
	movq	%r8, 2888(%rsp)         # 8-byte Spill
	movq	%rax, %r8
	movl	2972(%rsp), %eax        # 4-byte Reload
	movq	%r9, 2880(%rsp)         # 8-byte Spill
	movl	%eax, %r9d
	movl	$1, (%rsp)
	movq	%r15, 2872(%rsp)        # 8-byte Spill
	movq	%rbp, 2864(%rsp)        # 8-byte Spill
	movq	%r12, 2856(%rsp)        # 8-byte Spill
	movq	%r13, 2848(%rsp)        # 8-byte Spill
	movq	%r14, 2840(%rsp)        # 8-byte Spill
	movq	%rbx, 2832(%rsp)        # 8-byte Spill
	movq	%r10, 2824(%rsp)        # 8-byte Spill
	movq	%r11, 2816(%rsp)        # 8-byte Spill
	callq	trace_logger_log0
	movq	2824(%rsp), %rdi        # 8-byte Reload
	movq	2992(%rsp), %rsi        # 8-byte Reload
	movq	2944(%rsp), %rdx        # 8-byte Reload
	movq	2824(%rsp), %rcx        # 8-byte Reload
	movq	2816(%rsp), %r8         # 8-byte Reload
	movq	2912(%rsp), %r9         # 8-byte Reload
	movq	2888(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	2944(%rsp), %rcx        # 8-byte Reload
	movq	2952(%rsp), %rdi        # 8-byte Reload
	movq	2992(%rsp), %rsi        # 8-byte Reload
	movq	%rcx, %rdx
	movq	2824(%rsp), %r8         # 8-byte Reload
	movq	%rcx, 2808(%rsp)        # 8-byte Spill
	movq	%r8, %rcx
	movq	2816(%rsp), %r8         # 8-byte Reload
	movq	2912(%rsp), %r9         # 8-byte Reload
	movq	2888(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	2856(%rsp), %rdi        # 8-byte Reload
	movq	2848(%rsp), %rsi        # 8-byte Reload
	movq	2864(%rsp), %rdx        # 8-byte Reload
	movq	2984(%rsp), %rcx        # 8-byte Reload
	movq	2960(%rsp), %r8         # 8-byte Reload
	movl	2972(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	2832(%rsp), %rdi        # 8-byte Reload
	movq	2992(%rsp), %rsi        # 8-byte Reload
	movq	2840(%rsp), %rdx        # 8-byte Reload
	movq	2824(%rsp), %rcx        # 8-byte Reload
	movq	2872(%rsp), %r8         # 8-byte Reload
	movq	2912(%rsp), %r9         # 8-byte Reload
	movq	2888(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	2824(%rsp), %rdi        # 8-byte Reload
	movq	2992(%rsp), %rsi        # 8-byte Reload
	movq	2808(%rsp), %rdx        # 8-byte Reload
	movq	2824(%rsp), %rcx        # 8-byte Reload
	movq	2816(%rsp), %r8         # 8-byte Reload
	movq	2912(%rsp), %r9         # 8-byte Reload
	movq	2888(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	2880(%rsp), %rdi        # 8-byte Reload
	movq	2992(%rsp), %rsi        # 8-byte Reload
	movq	2992(%rsp), %rdx        # 8-byte Reload
	movq	2912(%rsp), %rcx        # 8-byte Reload
	movq	2976(%rsp), %r8         # 8-byte Reload
	movq	2912(%rsp), %r9         # 8-byte Reload
	movq	2888(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	2928(%rsp), %rdi        # 8-byte Reload
	movq	2992(%rsp), %rsi        # 8-byte Reload
	movq	2896(%rsp), %rdx        # 8-byte Reload
	movq	2912(%rsp), %rcx        # 8-byte Reload
	movq	2976(%rsp), %r8         # 8-byte Reload
	movq	2912(%rsp), %r9         # 8-byte Reload
	movq	2888(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	2808(%rsp), %rdi        # 8-byte Reload
	movq	2992(%rsp), %rsi        # 8-byte Reload
	movq	2896(%rsp), %rdx        # 8-byte Reload
	callq	posix_memalign
	movabsq	$1, %rcx
	leaq	.L.str57, %rdx
	movabsq	$0, %rsi
	leaq	.L.str11, %rdi
	movabsq	$2, %r8
	leaq	.L.str82, %r9
	movabsq	$3, %r10
	leaq	.L.str81, %r11
	movabsq	$177, %rbx
	leaq	.L.str73, %r14
	leaq	.L.str75, %r15
	leaq	.L.str80, %r12
	movl	$0, %ebp
	movl	$1, %r13d
	movq	%rcx, 2800(%rsp)        # 8-byte Spill
	movabsq	$19134, %rcx            # imm = 0x4ABE
	movq	%rcx, 2792(%rsp)        # 8-byte Spill
	movabsq	$32, %rcx
	movq	%rcx, 2784(%rsp)        # 8-byte Spill
	leaq	.L.str28, %rcx
	movq	%rcx, 2776(%rsp)        # 8-byte Spill
	leaq	.L.str10, %rcx
	movq	%rcx, 2768(%rsp)        # 8-byte Spill
	movabsq	$46, %rcx
	movl	%eax, 2764(%rsp)        # 4-byte Spill
	movl	%eax, %eax
                                        # kill: RAX<def> EAX<kill>
	movq	%rax, 2752(%rsp)        # 8-byte Spill
	movq	2792(%rsp), %rax        # 8-byte Reload
	movq	%rdi, 2744(%rsp)        # 8-byte Spill
	movq	%rax, %rdi
	movq	2784(%rsp), %rax        # 8-byte Reload
	movq	%rsi, 2736(%rsp)        # 8-byte Spill
	movq	%rax, %rsi
	movq	2752(%rsp), %rax        # 8-byte Reload
	movq	%rdx, 2728(%rsp)        # 8-byte Spill
	movq	%rax, %rdx
	movq	2800(%rsp), %rax        # 8-byte Reload
	movq	%rcx, 2720(%rsp)        # 8-byte Spill
	movq	%rax, %rcx
	movq	2776(%rsp), %rax        # 8-byte Reload
	movq	%r8, 2712(%rsp)         # 8-byte Spill
	movq	%rax, %r8
	movq	2736(%rsp), %rax        # 8-byte Reload
	movq	%r9, 2704(%rsp)         # 8-byte Spill
	movq	%rax, %r9
	movq	2744(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	movl	%r13d, 2700(%rsp)       # 4-byte Spill
	movq	%r15, 2688(%rsp)        # 8-byte Spill
	movq	%r12, 2680(%rsp)        # 8-byte Spill
	movl	%ebp, 2676(%rsp)        # 4-byte Spill
	movq	%r14, 2664(%rsp)        # 8-byte Spill
	movq	%rbx, 2656(%rsp)        # 8-byte Spill
	movq	%r10, 2648(%rsp)        # 8-byte Spill
	movq	%r11, 2640(%rsp)        # 8-byte Spill
	callq	trace_logger_log_int
.Ltmp136:
	#DEBUG_VALUE: main:err <- [RSP+2764]
	.loc	1 177 0                 # test_dma_load_store.c:177:0
	movq	2656(%rsp), %rdi        # 8-byte Reload
	movq	2664(%rsp), %rsi        # 8-byte Reload
	movq	2688(%rsp), %rdx        # 8-byte Reload
	movq	2728(%rsp), %rcx        # 8-byte Reload
	movq	2720(%rsp), %r8         # 8-byte Reload
	movl	2676(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	2712(%rsp), %rdi        # 8-byte Reload
	movq	2784(%rsp), %rsi        # 8-byte Reload
	movq	2736(%rsp), %rdx        # 8-byte Reload
	movq	2736(%rsp), %rcx        # 8-byte Reload
	movq	2768(%rsp), %r8         # 8-byte Reload
	movq	2736(%rsp), %r9         # 8-byte Reload
	movq	2744(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	2764(%rsp), %ebp        # 4-byte Reload
	movl	%ebp, %r13d
	movl	%r13d, %edx
	movq	2800(%rsp), %rdi        # 8-byte Reload
	movq	2784(%rsp), %rsi        # 8-byte Reload
	movq	2800(%rsp), %rcx        # 8-byte Reload
	movq	2776(%rsp), %r8         # 8-byte Reload
	movq	2736(%rsp), %r9         # 8-byte Reload
	movq	2744(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	2764(%rsp), %ebp        # 4-byte Reload
	cmpl	$0, %ebp
	sete	%al
	movb	%al, %cl
	andb	$1, %cl
	movzbl	%cl, %r13d
	movl	%r13d, %edx
	movq	2792(%rsp), %rdi        # 8-byte Reload
	movq	2800(%rsp), %rsi        # 8-byte Reload
	movq	2800(%rsp), %rcx        # 8-byte Reload
	movq	2728(%rsp), %r8         # 8-byte Reload
	movq	2736(%rsp), %r9         # 8-byte Reload
	movq	2744(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	movb	%al, 2639(%rsp)         # 1-byte Spill
	callq	trace_logger_log_int
	movq	2656(%rsp), %rdi        # 8-byte Reload
	movq	2664(%rsp), %rsi        # 8-byte Reload
	movq	2688(%rsp), %rdx        # 8-byte Reload
	movq	2680(%rsp), %rcx        # 8-byte Reload
	movq	2712(%rsp), %r8         # 8-byte Reload
	movl	2676(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	2648(%rsp), %rdi        # 8-byte Reload
	movq	2736(%rsp), %rsi        # 8-byte Reload
	movq	2736(%rsp), %rdx        # 8-byte Reload
	movq	2800(%rsp), %rcx        # 8-byte Reload
	movq	2640(%rsp), %r8         # 8-byte Reload
	movq	2736(%rsp), %r9         # 8-byte Reload
	movq	2744(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	2712(%rsp), %rdi        # 8-byte Reload
	movq	2736(%rsp), %rsi        # 8-byte Reload
	movq	2736(%rsp), %rdx        # 8-byte Reload
	movq	2800(%rsp), %rcx        # 8-byte Reload
	movq	2704(%rsp), %r8         # 8-byte Reload
	movq	2736(%rsp), %r9         # 8-byte Reload
	movq	2744(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	2639(%rsp), %al         # 1-byte Reload
	andb	$1, %al
	movzbl	%al, %ebp
	movl	%ebp, %edx
	movq	2800(%rsp), %rdi        # 8-byte Reload
	movq	2800(%rsp), %rsi        # 8-byte Reload
	movq	2800(%rsp), %rcx        # 8-byte Reload
	movq	2728(%rsp), %r8         # 8-byte Reload
	movq	2736(%rsp), %r9         # 8-byte Reload
	movq	2744(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	2639(%rsp), %al         # 1-byte Reload
	testb	$1, %al
	jne	.LBB4_3
	jmp	.LBB4_4
.Ltmp137:
.LBB4_3:                                # %.preheader
	movabsq	$0, %rax
	movabsq	$1, %rcx
	leaq	.L.str84, %r8
	leaq	.L.str11, %rdx
	movabsq	$178, %rdi
	leaq	.L.str73, %rsi
	leaq	.L.str81, %r9
	leaq	.L.str83, %r10
	movabsq	$2, %r11
	movl	$0, %ebx
	movl	$1, %ebp
	movabsq	$19134, %r14            # imm = 0x4ABE
	movabsq	$64, %r15
	leaq	.L.str33, %r12
	leaq	.L.str22, %r13
	movq	%rax, 2624(%rsp)        # 8-byte Spill
	leaq	3592(%rsp), %rax
	movq	%rax, 2616(%rsp)        # 8-byte Spill
	movabsq	$180, %rax
	movq	%rax, 2608(%rsp)        # 8-byte Spill
	movabsq	$27, %rax
	movq	%rax, 2600(%rsp)        # 8-byte Spill
	leaq	.L.str31, %rax
	movq	%rax, 2592(%rsp)        # 8-byte Spill
	leaq	.L.str25, %rax
	movq	%rax, 2584(%rsp)        # 8-byte Spill
	leaq	3600(%rsp), %rax
	movq	%rax, 2576(%rsp)        # 8-byte Spill
	movabsq	$179, %rax
.Ltmp138:
	#DEBUG_VALUE: main:store_vals <- [RSP+2576]
	.loc	1 179 0                 # test_dma_load_store.c:179:0
	movq	%rdi, 2568(%rsp)        # 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, 2560(%rsp)        # 8-byte Spill
	movq	%rdx, 2552(%rsp)        # 8-byte Spill
	movq	%r9, %rdx
	movq	2592(%rsp), %rax        # 8-byte Reload
	movq	%rcx, 2544(%rsp)        # 8-byte Spill
	movq	%rax, %rcx
	movq	2600(%rsp), %rax        # 8-byte Reload
	movq	%r8, 2536(%rsp)         # 8-byte Spill
	movq	%rax, %r8
	movq	%r9, 2528(%rsp)         # 8-byte Spill
	movl	%ebx, %r9d
	movl	$1, (%rsp)
	movq	%r13, 2520(%rsp)        # 8-byte Spill
	movl	%ebp, 2516(%rsp)        # 4-byte Spill
	movq	%r14, 2504(%rsp)        # 8-byte Spill
	movq	%r12, 2496(%rsp)        # 8-byte Spill
	movq	%r15, 2488(%rsp)        # 8-byte Spill
	movl	%ebx, 2484(%rsp)        # 4-byte Spill
	movq	%r10, 2472(%rsp)        # 8-byte Spill
	movq	%r11, 2464(%rsp)        # 8-byte Spill
	callq	trace_logger_log0
	movq	2544(%rsp), %rdi        # 8-byte Reload
	movq	2488(%rsp), %rsi        # 8-byte Reload
	movq	2576(%rsp), %rdx        # 8-byte Reload
	movq	2544(%rsp), %rcx        # 8-byte Reload
	movq	2584(%rsp), %r8         # 8-byte Reload
	movq	2624(%rsp), %r9         # 8-byte Reload
	movq	2552(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	3600(%rsp), %rax
	.loc	1 180 0                 # test_dma_load_store.c:180:0
	movq	2504(%rsp), %rdi        # 8-byte Reload
	movq	2488(%rsp), %rsi        # 8-byte Reload
	movq	%rax, %rdx
	movq	2544(%rsp), %rcx        # 8-byte Reload
	movq	2592(%rsp), %r8         # 8-byte Reload
	movq	2624(%rsp), %r9         # 8-byte Reload
	movq	2552(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	movq	%rax, 2456(%rsp)        # 8-byte Spill
	callq	trace_logger_log_ptr
.Ltmp139:
	#DEBUG_VALUE: main:store_loc <- [RSP+2616]
	movq	2608(%rsp), %rdi        # 8-byte Reload
	movq	2560(%rsp), %rsi        # 8-byte Reload
	movq	2528(%rsp), %rdx        # 8-byte Reload
	movq	2496(%rsp), %rcx        # 8-byte Reload
	movq	2600(%rsp), %r8         # 8-byte Reload
	movl	2484(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	2544(%rsp), %rdi        # 8-byte Reload
	movq	2488(%rsp), %rsi        # 8-byte Reload
	movq	2616(%rsp), %rdx        # 8-byte Reload
	movq	2544(%rsp), %rcx        # 8-byte Reload
	movq	2520(%rsp), %r8         # 8-byte Reload
	movq	2624(%rsp), %r9         # 8-byte Reload
	movq	2552(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	3592(%rsp), %rax
.Ltmp140:
	.loc	1 178 0                 # test_dma_load_store.c:178:0
	movq	2504(%rsp), %rdi        # 8-byte Reload
	movq	2488(%rsp), %rsi        # 8-byte Reload
	movq	%rax, %rdx
	movq	2544(%rsp), %rcx        # 8-byte Reload
	movq	2496(%rsp), %r8         # 8-byte Reload
	movq	2624(%rsp), %r9         # 8-byte Reload
	movq	2552(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	movq	%rax, 2448(%rsp)        # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	2568(%rsp), %rdi        # 8-byte Reload
	movq	2560(%rsp), %rsi        # 8-byte Reload
	movq	2528(%rsp), %rdx        # 8-byte Reload
	movq	2472(%rsp), %rcx        # 8-byte Reload
	movq	2464(%rsp), %r8         # 8-byte Reload
	movl	2484(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	2544(%rsp), %rdi        # 8-byte Reload
	movq	2624(%rsp), %rsi        # 8-byte Reload
	movq	2624(%rsp), %rdx        # 8-byte Reload
	movq	2544(%rsp), %rcx        # 8-byte Reload
	movq	2536(%rsp), %r8         # 8-byte Reload
	movq	2624(%rsp), %r9         # 8-byte Reload
	movq	2552(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	2624(%rsp), %rax        # 8-byte Reload
	movq	%rax, 2440(%rsp)        # 8-byte Spill
	jmp	.LBB4_5
.Ltmp141:
.LBB4_4:
	leaq	.L.str2, %rax
	leaq	.L.str3, %rcx
	movl	$177, %edx
	leaq	.L__PRETTY_FUNCTION__.main, %rsi
	movabsq	$4, %rdi
	movabsq	$64, %r8
	movabsq	$0, %r9
	leaq	.L.str10, %r10
	leaq	.L.str11, %r11
	movabsq	$3, %rbx
	movabsq	$32, %r14
	movabsq	$177, %r15
	movabsq	$2, %r12
	movabsq	$1, %r13
	movabsq	$5, %rbp
	movq	%rax, 2432(%rsp)        # 8-byte Spill
	leaq	__assert_fail, %rax
	movq	%rax, 2424(%rsp)        # 8-byte Spill
	leaq	.L.str78, %rax
	movq	%rax, 2416(%rsp)        # 8-byte Spill
	leaq	.L.str73, %rax
	movq	%rax, 2408(%rsp)        # 8-byte Spill
	leaq	.L.str82, %rax
	movq	%rax, 2400(%rsp)        # 8-byte Spill
	leaq	.L.str85, %rax
	movq	%rax, 2392(%rsp)        # 8-byte Spill
	movabsq	$49, %rax
	movl	%edx, 2388(%rsp)        # 4-byte Spill
	movl	$0, %edx
	movl	%edx, 2384(%rsp)        # 4-byte Spill
	movl	$1, %edx
	.loc	1 177 0                 # test_dma_load_store.c:177:0
	movq	%rdi, 2376(%rsp)        # 8-byte Spill
	movq	%r15, %rdi
	movq	%rax, 2368(%rsp)        # 8-byte Spill
	movq	2408(%rsp), %rax        # 8-byte Reload
	movq	%rsi, 2360(%rsp)        # 8-byte Spill
	movq	%rax, %rsi
	movq	2400(%rsp), %rax        # 8-byte Reload
	movl	%edx, 2356(%rsp)        # 4-byte Spill
	movq	%rax, %rdx
	movq	2392(%rsp), %rax        # 8-byte Reload
	movq	%rcx, 2344(%rsp)        # 8-byte Spill
	movq	%rax, %rcx
	movq	2368(%rsp), %rax        # 8-byte Reload
	movq	%r8, 2336(%rsp)         # 8-byte Spill
	movq	%rax, %r8
	movl	2384(%rsp), %eax        # 4-byte Reload
	movq	%r9, 2328(%rsp)         # 8-byte Spill
	movl	%eax, %r9d
	movl	$1, (%rsp)
	movq	%r15, 2320(%rsp)        # 8-byte Spill
	movq	%r12, 2312(%rsp)        # 8-byte Spill
	movq	%r13, 2304(%rsp)        # 8-byte Spill
	movq	%rbp, 2296(%rsp)        # 8-byte Spill
	movq	%r14, 2288(%rsp)        # 8-byte Spill
	movq	%rbx, 2280(%rsp)        # 8-byte Spill
	movq	%r10, 2272(%rsp)        # 8-byte Spill
	movq	%r11, 2264(%rsp)        # 8-byte Spill
	callq	trace_logger_log0
	movq	2296(%rsp), %rdi        # 8-byte Reload
	movq	2336(%rsp), %rsi        # 8-byte Reload
	movq	2424(%rsp), %rdx        # 8-byte Reload
	movq	2304(%rsp), %rcx        # 8-byte Reload
	movq	2416(%rsp), %r8         # 8-byte Reload
	movq	2328(%rsp), %r9         # 8-byte Reload
	movq	2264(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	2304(%rsp), %rdi        # 8-byte Reload
	movq	2336(%rsp), %rsi        # 8-byte Reload
	movq	2432(%rsp), %rdx        # 8-byte Reload
	movq	2328(%rsp), %rcx        # 8-byte Reload
	movq	2272(%rsp), %r8         # 8-byte Reload
	movq	2328(%rsp), %r9         # 8-byte Reload
	movq	2264(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	2312(%rsp), %rdi        # 8-byte Reload
	movq	2336(%rsp), %rsi        # 8-byte Reload
	movq	2344(%rsp), %rdx        # 8-byte Reload
	movq	2328(%rsp), %rcx        # 8-byte Reload
	movq	2272(%rsp), %r8         # 8-byte Reload
	movq	2328(%rsp), %r9         # 8-byte Reload
	movq	2264(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	2280(%rsp), %rdi        # 8-byte Reload
	movq	2288(%rsp), %rsi        # 8-byte Reload
	movq	2320(%rsp), %rdx        # 8-byte Reload
	movq	2328(%rsp), %rcx        # 8-byte Reload
	movq	2272(%rsp), %r8         # 8-byte Reload
	movq	2328(%rsp), %r9         # 8-byte Reload
	movq	2264(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	2376(%rsp), %rdi        # 8-byte Reload
	movq	2336(%rsp), %rsi        # 8-byte Reload
	movq	2360(%rsp), %rdx        # 8-byte Reload
	movq	2328(%rsp), %rcx        # 8-byte Reload
	movq	2272(%rsp), %r8         # 8-byte Reload
	movq	2328(%rsp), %r9         # 8-byte Reload
	movq	2264(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	2432(%rsp), %rdi        # 8-byte Reload
	movq	2344(%rsp), %rsi        # 8-byte Reload
	movl	2388(%rsp), %edx        # 4-byte Reload
	movq	2360(%rsp), %rcx        # 8-byte Reload
	callq	__assert_fail
	movabsq	$177, %rdi
	leaq	.L.str73, %rsi
	leaq	.L.str82, %rdx
	leaq	.L.str86, %rcx
	movabsq	$7, %r8
	movl	$0, %r9d
	movl	$1, %eax
	movl	$1, (%rsp)
	movl	%eax, 2260(%rsp)        # 4-byte Spill
	callq	trace_logger_log0
.LBB4_5:                                # =>This Inner Loop Header: Depth=1
	movq	2440(%rsp), %rax        # 8-byte Reload
	movabsq	$1, %rcx
	leaq	.L.str41, %rdx
	movabsq	$0, %rsi
	leaq	.L.str11, %rdi
	movabsq	$2, %r8
	leaq	.L.str84, %r9
	movabsq	$3, %r10
	leaq	.L.str91, %r11
	movabsq	$178, %rbx
	leaq	.L.str73, %r14
	leaq	.L.str90, %r15
	movl	$0, %ebp
	movl	$1, %r12d
	movabsq	$19134, %r13            # imm = 0x4ABE
	movq	%rax, 2248(%rsp)        # 8-byte Spill
	movabsq	$64, %rax
	movq	%rax, 2240(%rsp)        # 8-byte Spill
	leaq	.L.str17, %rax
	movq	%rax, 2232(%rsp)        # 8-byte Spill
	movabsq	$1024, %rax             # imm = 0x400
	movq	%rax, 2224(%rsp)        # 8-byte Spill
	leaq	.L.str10, %rax
	movq	%rax, 2216(%rsp)        # 8-byte Spill
	movabsq	$46, %rax
	movq	%rax, 2208(%rsp)        # 8-byte Spill
	leaq	.L.str16, %rax
	movq	%rax, 2200(%rsp)        # 8-byte Spill
	movabsq	$8, %rax
	movq	%rax, 2192(%rsp)        # 8-byte Spill
	movabsq	$-1, %rax
	vmovss	.LCPI4_0, %xmm0
	movq	%rax, 2184(%rsp)        # 8-byte Spill
	movabsq	$32, %rax
	vmovsd	.LCPI4_1, %xmm1
	movq	%rax, 2176(%rsp)        # 8-byte Spill
	leaq	.L.str88, %rax
	movq	%rax, 2168(%rsp)        # 8-byte Spill
	movabsq	$180, %rax
	movq	%rax, 2160(%rsp)        # 8-byte Spill
	leaq	.L.str89, %rax
	movq	%rax, 2152(%rsp)        # 8-byte Spill
	movabsq	$28, %rax
	movq	%rax, 2144(%rsp)        # 8-byte Spill
	leaq	.L.str33, %rax
	movq	%rax, 2136(%rsp)        # 8-byte Spill
	movabsq	$29, %rax
	movq	%rax, 2128(%rsp)        # 8-byte Spill
	leaq	.L.str38, %rax
	movq	%rax, 2120(%rsp)        # 8-byte Spill
	leaq	.L.str66, %rax
	movq	%rax, 2112(%rsp)        # 8-byte Spill
	movabsq	$179, %rax
	movq	%rax, 2104(%rsp)        # 8-byte Spill
	leaq	.L.str87, %rax
	movq	%rax, 2096(%rsp)        # 8-byte Spill
	leaq	.L.str31, %rax
	.loc	1 179 0                 # test_dma_load_store.c:179:0
.Ltmp142:
	movq	%rax, 2088(%rsp)        # 8-byte Spill
	movq	%rsp, %rax
	movl	$1, (%rax)
	movl	$.L.str73, %eax
                                        # kill: RAX<def> EAX<kill>
	movq	%rsi, 2080(%rsp)        # 8-byte Spill
	movl	$.L.str84, %esi
                                        # kill: RSI<def> ESI<kill>
	movq	%rsi, 2072(%rsp)        # 8-byte Spill
	movl	$.L.str16, %esi
                                        # kill: RSI<def> ESI<kill>
	movq	%rsi, 2064(%rsp)        # 8-byte Spill
	movl	$48, %esi
                                        # kill: RSI<def> ESI<kill>
	movq	%rax, 2056(%rsp)        # 8-byte Spill
	movq	$-1, %rax
	movq	%rsi, 2048(%rsp)        # 8-byte Spill
	xorl	%esi, %esi
	movq	%rdi, 2040(%rsp)        # 8-byte Spill
	movq	%rax, %rdi
	movq	2056(%rsp), %rax        # 8-byte Reload
	movl	%esi, 2036(%rsp)        # 4-byte Spill
	movq	%rax, %rsi
	movq	2072(%rsp), %rax        # 8-byte Reload
	movq	%rdx, 2024(%rsp)        # 8-byte Spill
	movq	%rax, %rdx
	movq	2064(%rsp), %rax        # 8-byte Reload
	movq	%rcx, 2016(%rsp)        # 8-byte Spill
	movq	%rax, %rcx
	movq	2048(%rsp), %rax        # 8-byte Reload
	movq	%r8, 2008(%rsp)         # 8-byte Spill
	movq	%rax, %r8
	movl	2036(%rsp), %eax        # 4-byte Reload
	movq	%r9, 2000(%rsp)         # 8-byte Spill
	movl	%eax, %r9d
	vmovsd	%xmm1, 1992(%rsp)       # 8-byte Spill
	movl	%r12d, 1988(%rsp)       # 4-byte Spill
	movq	%r13, 1976(%rsp)        # 8-byte Spill
	vmovss	%xmm0, 1972(%rsp)       # 4-byte Spill
	movl	%ebp, 1968(%rsp)        # 4-byte Spill
	movq	%r15, 1960(%rsp)        # 8-byte Spill
	movq	%r10, 1952(%rsp)        # 8-byte Spill
	movq	%r11, 1944(%rsp)        # 8-byte Spill
	movq	%rbx, 1936(%rsp)        # 8-byte Spill
	movq	%r14, 1928(%rsp)        # 8-byte Spill
	callq	trace_logger_log0
	movq	%rsp, %rcx
	movq	$.L.str84, (%rcx)
	movl	$.L.str17, %eax
	movl	%eax, %r8d
	movl	$2, %eax
	movl	%eax, %edi
	movl	$64, %eax
	movl	%eax, %ecx
	movl	2036(%rsp), %eax        # 4-byte Reload
	movl	%eax, %edx
	movl	$1, %r9d
	movl	%r9d, %esi
	movq	%rsi, 1920(%rsp)        # 8-byte Spill
	movq	%rcx, %rsi
	movq	%rdx, 1912(%rsp)        # 8-byte Spill
	movq	1920(%rsp), %r10        # 8-byte Reload
	movq	%rcx, 1904(%rsp)        # 8-byte Spill
	movq	%r10, %rcx
	movq	%r10, %r9
	callq	trace_logger_log_int
	movq	%rsp, %rcx
	movq	$.L.str81, (%rcx)
	movl	$.L.str10, %eax
	movl	%eax, %r8d
	movq	1920(%rsp), %rdi        # 8-byte Reload
	movq	1904(%rsp), %rsi        # 8-byte Reload
	movq	1912(%rsp), %rdx        # 8-byte Reload
	movq	1912(%rsp), %rcx        # 8-byte Reload
	movq	1920(%rsp), %r9         # 8-byte Reload
	callq	trace_logger_log_int
	movq	%rsp, %rcx
	movq	$.L.str81, (%rcx)
	movl	$19134, %eax            # imm = 0x4ABE
	movl	%eax, %ecx
	movq	%rcx, %rdi
	movq	1904(%rsp), %rsi        # 8-byte Reload
	movq	2248(%rsp), %rdx        # 8-byte Reload
	movq	1920(%rsp), %r8         # 8-byte Reload
	movq	%rcx, 1896(%rsp)        # 8-byte Spill
	movq	%r8, %rcx
	movq	2064(%rsp), %r8         # 8-byte Reload
	movq	1912(%rsp), %r9         # 8-byte Reload
	callq	trace_logger_log_int
	movq	%rsp, %rcx
	movl	$1, (%rcx)
	movl	$.L.str36, %eax
	movl	%eax, %ecx
	movl	$179, %eax
	movl	%eax, %edx
	movl	$33, %eax
	movl	%eax, %r8d
	movq	%rdx, %rdi
	movq	2056(%rsp), %rsi        # 8-byte Reload
	movq	2072(%rsp), %r9         # 8-byte Reload
	movq	%rdx, 1888(%rsp)        # 8-byte Spill
	movq	%r9, %rdx
	movq	%rcx, 1880(%rsp)        # 8-byte Spill
	movl	2036(%rsp), %r9d        # 4-byte Reload
	callq	trace_logger_log0
	movq	%rsp, %rcx
	movq	$.L.str11, (%rcx)
	movq	1920(%rsp), %rdi        # 8-byte Reload
	movq	1904(%rsp), %rsi        # 8-byte Reload
	movq	2248(%rsp), %rdx        # 8-byte Reload
	movq	1920(%rsp), %rcx        # 8-byte Reload
	movq	2064(%rsp), %r8         # 8-byte Reload
	movq	1912(%rsp), %r9         # 8-byte Reload
	callq	trace_logger_log_int
	movq	2248(%rsp), %rcx        # 8-byte Reload
	movl	%ecx, %eax
	movl	%eax, %ebp
	movl	%ebp, %edx
	movq	%rsp, %rsi
	movq	$.L.str11, (%rsi)
	movl	$32, %ebp
	movl	%ebp, %esi
	movq	1896(%rsp), %rdi        # 8-byte Reload
	movq	%rsi, 1872(%rsp)        # 8-byte Spill
	movq	%rdx, 1864(%rsp)        # 8-byte Spill
	movq	1920(%rsp), %rcx        # 8-byte Reload
	movq	1880(%rsp), %r8         # 8-byte Reload
	movq	1912(%rsp), %r9         # 8-byte Reload
	movl	%eax, 1860(%rsp)        # 4-byte Spill
	callq	trace_logger_log_int
	movq	%rsp, %rcx
	movl	$1, (%rcx)
	movl	$.L.str38, %eax
	movl	%eax, %ecx
	movl	$39, %eax
	movl	%eax, %r8d
	movq	1888(%rsp), %rdi        # 8-byte Reload
	movq	2056(%rsp), %rsi        # 8-byte Reload
	movq	2072(%rsp), %rdx        # 8-byte Reload
	movl	2036(%rsp), %r9d        # 4-byte Reload
	callq	trace_logger_log0
	movq	%rsp, %rcx
	movq	$.L.str11, (%rcx)
	movq	1920(%rsp), %rdi        # 8-byte Reload
	movq	1872(%rsp), %rsi        # 8-byte Reload
	movq	1864(%rsp), %rdx        # 8-byte Reload
	movq	1920(%rsp), %rcx        # 8-byte Reload
	movq	1880(%rsp), %r8         # 8-byte Reload
	movq	1912(%rsp), %r9         # 8-byte Reload
	callq	trace_logger_log_int
                                        # implicit-def: XMM0
	movl	1860(%rsp), %eax        # 4-byte Reload
	vcvtsi2ssl	%eax, %xmm0, %xmm0
	cvtss2sd	%xmm0, %xmm1
	movq	1976(%rsp), %rdi        # 8-byte Reload
	movq	2176(%rsp), %rsi        # 8-byte Reload
	vmovss	%xmm0, 1856(%rsp)       # 4-byte Spill
	vmovaps	%xmm1, %xmm0
	movq	2016(%rsp), %rdx        # 8-byte Reload
	movq	2120(%rsp), %rcx        # 8-byte Reload
	movq	2080(%rsp), %r8         # 8-byte Reload
	movq	2040(%rsp), %r9         # 8-byte Reload
	callq	trace_logger_log_double
	movq	2104(%rsp), %rdi        # 8-byte Reload
	movq	1928(%rsp), %rsi        # 8-byte Reload
	movq	2000(%rsp), %rdx        # 8-byte Reload
	movq	2112(%rsp), %rcx        # 8-byte Reload
	movq	2128(%rsp), %r8         # 8-byte Reload
	movl	1968(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	2008(%rsp), %rdi        # 8-byte Reload
	movq	2240(%rsp), %rsi        # 8-byte Reload
	movq	2248(%rsp), %rdx        # 8-byte Reload
	movq	2016(%rsp), %rcx        # 8-byte Reload
	movq	2200(%rsp), %r8         # 8-byte Reload
	movq	2080(%rsp), %r9         # 8-byte Reload
	movq	2040(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	2016(%rsp), %rdi        # 8-byte Reload
	movq	2240(%rsp), %rsi        # 8-byte Reload
	movq	2456(%rsp), %rdx        # 8-byte Reload
	movq	2016(%rsp), %rcx        # 8-byte Reload
	movq	2088(%rsp), %r8         # 8-byte Reload
	movq	2080(%rsp), %r9         # 8-byte Reload
	movq	2040(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	2248(%rsp), %rcx        # 8-byte Reload
	shlq	$2, %rcx
	movq	2456(%rsp), %rdx        # 8-byte Reload
	addq	%rcx, %rdx
	movq	1976(%rsp), %rdi        # 8-byte Reload
	movq	2240(%rsp), %rsi        # 8-byte Reload
	movq	%rdx, 1848(%rsp)        # 8-byte Spill
	movq	2016(%rsp), %rcx        # 8-byte Reload
	movq	2112(%rsp), %r8         # 8-byte Reload
	movq	2080(%rsp), %r9         # 8-byte Reload
	movq	2040(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	2104(%rsp), %rdi        # 8-byte Reload
	movq	1928(%rsp), %rsi        # 8-byte Reload
	movq	2000(%rsp), %rdx        # 8-byte Reload
	movq	2096(%rsp), %rcx        # 8-byte Reload
	movq	2144(%rsp), %r8         # 8-byte Reload
	movl	1968(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	2008(%rsp), %rdi        # 8-byte Reload
	movq	2240(%rsp), %rsi        # 8-byte Reload
	movq	1848(%rsp), %rdx        # 8-byte Reload
	movq	2016(%rsp), %rcx        # 8-byte Reload
	movq	2112(%rsp), %r8         # 8-byte Reload
	movq	2080(%rsp), %r9         # 8-byte Reload
	movq	2040(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	vmovss	1856(%rsp), %xmm0       # 4-byte Reload
	cvtss2sd	%xmm0, %xmm0
	movq	2016(%rsp), %rdi        # 8-byte Reload
	movq	2176(%rsp), %rsi        # 8-byte Reload
	movq	2016(%rsp), %rdx        # 8-byte Reload
	movq	2120(%rsp), %rcx        # 8-byte Reload
	movq	2080(%rsp), %r8         # 8-byte Reload
	movq	2040(%rsp), %r9         # 8-byte Reload
	callq	trace_logger_log_double
	movq	2456(%rsp), %rcx        # 8-byte Reload
	movq	2248(%rsp), %rdx        # 8-byte Reload
	vmovss	1856(%rsp), %xmm0       # 4-byte Reload
	vmovss	%xmm0, (%rcx,%rdx,4)
	.loc	1 180 0                 # test_dma_load_store.c:180:0
	movq	2160(%rsp), %rdi        # 8-byte Reload
	movq	1928(%rsp), %rsi        # 8-byte Reload
	movq	2000(%rsp), %rdx        # 8-byte Reload
	movq	2168(%rsp), %rcx        # 8-byte Reload
	movq	2128(%rsp), %r8         # 8-byte Reload
	movl	1968(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	2008(%rsp), %rdi        # 8-byte Reload
	movq	2240(%rsp), %rsi        # 8-byte Reload
	movq	2248(%rsp), %rdx        # 8-byte Reload
	movq	2016(%rsp), %rcx        # 8-byte Reload
	movq	2200(%rsp), %r8         # 8-byte Reload
	movq	2080(%rsp), %r9         # 8-byte Reload
	movq	2040(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	2016(%rsp), %rdi        # 8-byte Reload
	movq	2240(%rsp), %rsi        # 8-byte Reload
	movq	2448(%rsp), %rdx        # 8-byte Reload
	movq	2016(%rsp), %rcx        # 8-byte Reload
	movq	2136(%rsp), %r8         # 8-byte Reload
	movq	2080(%rsp), %r9         # 8-byte Reload
	movq	2040(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	2248(%rsp), %rcx        # 8-byte Reload
	shlq	$2, %rcx
	movq	2448(%rsp), %rdx        # 8-byte Reload
	addq	%rcx, %rdx
	movq	1976(%rsp), %rdi        # 8-byte Reload
	movq	2240(%rsp), %rsi        # 8-byte Reload
	movq	%rdx, 1840(%rsp)        # 8-byte Spill
	movq	2016(%rsp), %rcx        # 8-byte Reload
	movq	2168(%rsp), %r8         # 8-byte Reload
	movq	2080(%rsp), %r9         # 8-byte Reload
	movq	2040(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	2160(%rsp), %rdi        # 8-byte Reload
	movq	1928(%rsp), %rsi        # 8-byte Reload
	movq	2000(%rsp), %rdx        # 8-byte Reload
	movq	2152(%rsp), %rcx        # 8-byte Reload
	movq	2144(%rsp), %r8         # 8-byte Reload
	movl	1968(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	2008(%rsp), %rdi        # 8-byte Reload
	movq	2240(%rsp), %rsi        # 8-byte Reload
	movq	1840(%rsp), %rdx        # 8-byte Reload
	movq	2016(%rsp), %rcx        # 8-byte Reload
	movq	2168(%rsp), %r8         # 8-byte Reload
	movq	2080(%rsp), %r9         # 8-byte Reload
	movq	2040(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	2016(%rsp), %rdi        # 8-byte Reload
	movq	2176(%rsp), %rsi        # 8-byte Reload
	vmovsd	1992(%rsp), %xmm0       # 8-byte Reload
	movq	2080(%rsp), %rdx        # 8-byte Reload
	movq	2216(%rsp), %rcx        # 8-byte Reload
	movq	2080(%rsp), %r8         # 8-byte Reload
	movq	2040(%rsp), %r9         # 8-byte Reload
	callq	trace_logger_log_double
	movq	2448(%rsp), %rcx        # 8-byte Reload
	movq	2248(%rsp), %rdx        # 8-byte Reload
	vmovss	1972(%rsp), %xmm0       # 4-byte Reload
	vmovss	%xmm0, (%rcx,%rdx,4)
.Ltmp143:
	.loc	1 178 0                 # test_dma_load_store.c:178:0
	movq	1936(%rsp), %rdi        # 8-byte Reload
	movq	1928(%rsp), %rsi        # 8-byte Reload
	movq	2000(%rsp), %rdx        # 8-byte Reload
	movq	2232(%rsp), %rcx        # 8-byte Reload
	movq	2192(%rsp), %r8         # 8-byte Reload
	movl	1968(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	2008(%rsp), %rdi        # 8-byte Reload
	movq	2240(%rsp), %rsi        # 8-byte Reload
	movq	2016(%rsp), %rdx        # 8-byte Reload
	movq	2080(%rsp), %rcx        # 8-byte Reload
	movq	2216(%rsp), %r8         # 8-byte Reload
	movq	2080(%rsp), %r9         # 8-byte Reload
	movq	2040(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	2016(%rsp), %rdi        # 8-byte Reload
	movq	2240(%rsp), %rsi        # 8-byte Reload
	movq	2248(%rsp), %rdx        # 8-byte Reload
	movq	2016(%rsp), %rcx        # 8-byte Reload
	movq	2200(%rsp), %r8         # 8-byte Reload
	movq	2080(%rsp), %r9         # 8-byte Reload
	movq	2040(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	2248(%rsp), %rcx        # 8-byte Reload
	addq	$1, %rcx
	movq	1976(%rsp), %rdi        # 8-byte Reload
	movq	2240(%rsp), %rsi        # 8-byte Reload
	movq	%rcx, %rdx
	movq	2016(%rsp), %r8         # 8-byte Reload
	movq	%rcx, 1832(%rsp)        # 8-byte Spill
	movq	%r8, %rcx
	movq	2232(%rsp), %r8         # 8-byte Reload
	movq	2080(%rsp), %r9         # 8-byte Reload
	movq	2040(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	1936(%rsp), %rdi        # 8-byte Reload
	movq	1928(%rsp), %rsi        # 8-byte Reload
	movq	2000(%rsp), %rdx        # 8-byte Reload
	movq	2024(%rsp), %rcx        # 8-byte Reload
	movq	2208(%rsp), %r8         # 8-byte Reload
	movl	1968(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	2008(%rsp), %rdi        # 8-byte Reload
	movq	2240(%rsp), %rsi        # 8-byte Reload
	movq	2224(%rsp), %rdx        # 8-byte Reload
	movq	2080(%rsp), %rcx        # 8-byte Reload
	movq	2216(%rsp), %r8         # 8-byte Reload
	movq	2080(%rsp), %r9         # 8-byte Reload
	movq	2040(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	2016(%rsp), %rdi        # 8-byte Reload
	movq	2240(%rsp), %rsi        # 8-byte Reload
	movq	1832(%rsp), %rdx        # 8-byte Reload
	movq	2016(%rsp), %rcx        # 8-byte Reload
	movq	2232(%rsp), %r8         # 8-byte Reload
	movq	2080(%rsp), %r9         # 8-byte Reload
	movq	2040(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	1832(%rsp), %rcx        # 8-byte Reload
	cmpq	$1024, %rcx             # imm = 0x400
	sete	%al
	movb	%al, %cl
	andb	$1, %cl
	movzbl	%cl, %ebp
	movl	%ebp, %edx
	movq	1976(%rsp), %rdi        # 8-byte Reload
	movq	2016(%rsp), %rsi        # 8-byte Reload
	movq	2016(%rsp), %rcx        # 8-byte Reload
	movq	2024(%rsp), %r8         # 8-byte Reload
	movq	2080(%rsp), %r9         # 8-byte Reload
	movq	2040(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	movb	%al, 1831(%rsp)         # 1-byte Spill
	callq	trace_logger_log_int
	movq	1936(%rsp), %rdi        # 8-byte Reload
	movq	1928(%rsp), %rsi        # 8-byte Reload
	movq	2000(%rsp), %rdx        # 8-byte Reload
	movq	1960(%rsp), %rcx        # 8-byte Reload
	movq	2008(%rsp), %r8         # 8-byte Reload
	movl	1968(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1952(%rsp), %rdi        # 8-byte Reload
	movq	2080(%rsp), %rsi        # 8-byte Reload
	movq	2080(%rsp), %rdx        # 8-byte Reload
	movq	2016(%rsp), %rcx        # 8-byte Reload
	movq	1944(%rsp), %r8         # 8-byte Reload
	movq	2080(%rsp), %r9         # 8-byte Reload
	movq	2040(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	2008(%rsp), %rdi        # 8-byte Reload
	movq	2080(%rsp), %rsi        # 8-byte Reload
	movq	2080(%rsp), %rdx        # 8-byte Reload
	movq	2016(%rsp), %rcx        # 8-byte Reload
	movq	2000(%rsp), %r8         # 8-byte Reload
	movq	2080(%rsp), %r9         # 8-byte Reload
	movq	2040(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	1831(%rsp), %al         # 1-byte Reload
	andb	$1, %al
	movzbl	%al, %ebp
	movl	%ebp, %edx
	movq	2016(%rsp), %rdi        # 8-byte Reload
	movq	2016(%rsp), %rsi        # 8-byte Reload
	movq	2016(%rsp), %rcx        # 8-byte Reload
	movq	2024(%rsp), %r8         # 8-byte Reload
	movq	2080(%rsp), %r9         # 8-byte Reload
	movq	2040(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	1831(%rsp), %al         # 1-byte Reload
	testb	$1, %al
	movq	1832(%rsp), %rcx        # 8-byte Reload
	movq	%rcx, 2440(%rsp)        # 8-byte Spill
	jne	.LBB4_6
	jmp	.LBB4_5
.Ltmp144:
.LBB4_6:
	leaq	.L.str4, %rax
	movabsq	$2, %rdi
	movabsq	$64, %rcx
	movabsq	$0, %rdx
	leaq	.L.str10, %r8
	leaq	.L.str11, %rsi
	movabsq	$1, %r9
	leaq	.L.str92, %r10
	movabsq	$3, %r11
	leaq	fprintf, %rbx
	leaq	.L.str37, %r14
	movabsq	$184, %r15
	leaq	.L.str73, %r12
	leaq	.L.str91, %r13
	leaq	.L.str93, %rbp
	movq	%rax, 1816(%rsp)        # 8-byte Spill
	movabsq	$49, %rax
	movq	%rsi, 1808(%rsp)        # 8-byte Spill
	movl	$0, %esi
	movl	%esi, 1804(%rsp)        # 4-byte Spill
	movl	$1, %esi
	movq	%rax, 1792(%rsp)        # 8-byte Spill
	movabsq	$19134, %rax            # imm = 0x4ABE
	movq	%rax, 1784(%rsp)        # 8-byte Spill
	leaq	stdout, %rax
	movq	%rax, 1776(%rsp)        # 8-byte Spill
	leaq	.L.str32, %rax
	movq	%rax, 1768(%rsp)        # 8-byte Spill
	movabsq	$27, %rax
	.loc	1 184 0                 # test_dma_load_store.c:184:0
	movq	%rdi, 1760(%rsp)        # 8-byte Spill
	movq	%r15, %rdi
	movl	%esi, 1756(%rsp)        # 4-byte Spill
	movq	%r12, %rsi
	movq	%rdx, 1744(%rsp)        # 8-byte Spill
	movq	%r13, %rdx
	movq	%rcx, 1736(%rsp)        # 8-byte Spill
	movq	%r10, %rcx
	movq	%r8, 1728(%rsp)         # 8-byte Spill
	movq	%rax, %r8
	movl	1804(%rsp), %eax        # 4-byte Reload
	movq	%r9, 1720(%rsp)         # 8-byte Spill
	movl	%eax, %r9d
	movl	$1, (%rsp)
	movq	%r14, 1712(%rsp)        # 8-byte Spill
	movq	%rbp, 1704(%rsp)        # 8-byte Spill
	movq	%r15, 1696(%rsp)        # 8-byte Spill
	movq	%r12, 1688(%rsp)        # 8-byte Spill
	movq	%rbx, 1680(%rsp)        # 8-byte Spill
	movq	%r13, 1672(%rsp)        # 8-byte Spill
	movq	%r10, 1664(%rsp)        # 8-byte Spill
	movq	%r11, 1656(%rsp)        # 8-byte Spill
	callq	trace_logger_log0
	movq	1720(%rsp), %rdi        # 8-byte Reload
	movq	1736(%rsp), %rsi        # 8-byte Reload
	movq	1776(%rsp), %rdx        # 8-byte Reload
	movq	1720(%rsp), %rcx        # 8-byte Reload
	movq	1768(%rsp), %r8         # 8-byte Reload
	movq	1744(%rsp), %r9         # 8-byte Reload
	movq	1808(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	stdout, %rcx
	movq	1784(%rsp), %rdi        # 8-byte Reload
	movq	1736(%rsp), %rsi        # 8-byte Reload
	movq	%rcx, %rdx
	movq	1720(%rsp), %r8         # 8-byte Reload
	movq	%rcx, 1648(%rsp)        # 8-byte Spill
	movq	%r8, %rcx
	movq	1664(%rsp), %r8         # 8-byte Reload
	movq	1744(%rsp), %r9         # 8-byte Reload
	movq	1808(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	1696(%rsp), %rdi        # 8-byte Reload
	movq	1688(%rsp), %rsi        # 8-byte Reload
	movq	1672(%rsp), %rdx        # 8-byte Reload
	movq	1704(%rsp), %rcx        # 8-byte Reload
	movq	1792(%rsp), %r8         # 8-byte Reload
	movl	1804(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1656(%rsp), %rdi        # 8-byte Reload
	movq	1736(%rsp), %rsi        # 8-byte Reload
	movq	1680(%rsp), %rdx        # 8-byte Reload
	movq	1720(%rsp), %rcx        # 8-byte Reload
	movq	1712(%rsp), %r8         # 8-byte Reload
	movq	1744(%rsp), %r9         # 8-byte Reload
	movq	1808(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	1720(%rsp), %rdi        # 8-byte Reload
	movq	1736(%rsp), %rsi        # 8-byte Reload
	movq	1648(%rsp), %rdx        # 8-byte Reload
	movq	1720(%rsp), %rcx        # 8-byte Reload
	movq	1664(%rsp), %r8         # 8-byte Reload
	movq	1744(%rsp), %r9         # 8-byte Reload
	movq	1808(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	1760(%rsp), %rdi        # 8-byte Reload
	movq	1736(%rsp), %rsi        # 8-byte Reload
	movq	1816(%rsp), %rdx        # 8-byte Reload
	movq	1744(%rsp), %rcx        # 8-byte Reload
	movq	1728(%rsp), %r8         # 8-byte Reload
	movq	1744(%rsp), %r9         # 8-byte Reload
	movq	1808(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	1648(%rsp), %rdi        # 8-byte Reload
	movq	1816(%rsp), %rsi        # 8-byte Reload
	movb	$0, %al
	callq	fprintf
	movl	$1024, %edx             # imm = 0x400
	movabsq	$3, %rdi
	movabsq	$32, %rcx
	movabsq	$1024, %rsi             # imm = 0x400
	movabsq	$0, %r8
	leaq	.L.str10, %r9
	leaq	.L.str11, %r10
	movabsq	$2, %r11
	movabsq	$64, %rbx
	movabsq	$1, %r14
	leaq	.L.str95, %r15
	leaq	.L.str94, %r12
	movabsq	$4, %r13
	leaq	big, %rbp
	movq	%rcx, 1640(%rsp)        # 8-byte Spill
	leaq	.L.str48, %rcx
	movq	%rcx, 1632(%rsp)        # 8-byte Spill
	movabsq	$195, %rcx
	movq	%rcx, 1624(%rsp)        # 8-byte Spill
	leaq	.L.str73, %rcx
	movq	%rcx, 1616(%rsp)        # 8-byte Spill
	leaq	.L.str91, %rcx
	movq	%rcx, 1608(%rsp)        # 8-byte Spill
	leaq	.L.str96, %rcx
	movq	%rcx, 1600(%rsp)        # 8-byte Spill
	movabsq	$49, %rcx
	movl	%eax, 1596(%rsp)        # 4-byte Spill
	movl	$0, %eax
	movl	%eax, 1592(%rsp)        # 4-byte Spill
	movl	$1, %eax
	movq	%rcx, 1584(%rsp)        # 8-byte Spill
	movabsq	$19134, %rcx            # imm = 0x4ABE
	movq	%rcx, 1576(%rsp)        # 8-byte Spill
	leaq	.L.str22, %rcx
	movq	%rcx, 1568(%rsp)        # 8-byte Spill
	leaq	3592(%rsp), %rcx
	movq	%rcx, 1560(%rsp)        # 8-byte Spill
	movabsq	$27, %rcx
	movq	%rcx, 1552(%rsp)        # 8-byte Spill
	leaq	.L.str25, %rcx
	movq	%rcx, 1544(%rsp)        # 8-byte Spill
	leaq	3600(%rsp), %rcx
	movq	%rcx, 1536(%rsp)        # 8-byte Spill
	leaq	.L.str93, %rcx
	.loc	1 195 0                 # test_dma_load_store.c:195:0
	movl	%eax, 1532(%rsp)        # 4-byte Spill
	movl	1596(%rsp), %eax        # 4-byte Reload
	movl	%eax, %eax
                                        # kill: RAX<def> EAX<kill>
	movq	%rax, 1520(%rsp)        # 8-byte Spill
	movq	1576(%rsp), %rax        # 8-byte Reload
	movq	%rdi, 1512(%rsp)        # 8-byte Spill
	movq	%rax, %rdi
	movq	1640(%rsp), %rax        # 8-byte Reload
	movq	%rsi, 1504(%rsp)        # 8-byte Spill
	movq	%rax, %rsi
	movq	1520(%rsp), %rax        # 8-byte Reload
	movl	%edx, 1500(%rsp)        # 4-byte Spill
	movq	%rax, %rdx
	movq	%rcx, 1488(%rsp)        # 8-byte Spill
	movq	%r14, %rcx
	movq	1488(%rsp), %rax        # 8-byte Reload
	movq	%r8, 1480(%rsp)         # 8-byte Spill
	movq	%rax, %r8
	movq	1480(%rsp), %rax        # 8-byte Reload
	movq	%r9, 1472(%rsp)         # 8-byte Spill
	movq	%rax, %r9
	movq	%r10, (%rsp)
	movq	%rbp, 1464(%rsp)        # 8-byte Spill
	movq	%r12, 1456(%rsp)        # 8-byte Spill
	movq	%r15, 1448(%rsp)        # 8-byte Spill
	movq	%r13, 1440(%rsp)        # 8-byte Spill
	movq	%r14, 1432(%rsp)        # 8-byte Spill
	movq	%rbx, 1424(%rsp)        # 8-byte Spill
	movq	%r10, 1416(%rsp)        # 8-byte Spill
	movq	%r11, 1408(%rsp)        # 8-byte Spill
	callq	trace_logger_log_int
.Ltmp145:
	#DEBUG_VALUE: main:store_vals <- [RSP+1536]
	movq	1624(%rsp), %rdi        # 8-byte Reload
	movq	1616(%rsp), %rsi        # 8-byte Reload
	movq	1608(%rsp), %rdx        # 8-byte Reload
	movq	1456(%rsp), %rcx        # 8-byte Reload
	movq	1552(%rsp), %r8         # 8-byte Reload
	movl	1592(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1432(%rsp), %rdi        # 8-byte Reload
	movq	1424(%rsp), %rsi        # 8-byte Reload
	movq	1536(%rsp), %rdx        # 8-byte Reload
	movq	1432(%rsp), %rcx        # 8-byte Reload
	movq	1544(%rsp), %r8         # 8-byte Reload
	movq	1480(%rsp), %r9         # 8-byte Reload
	movq	1416(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	3600(%rsp), %rax
	movq	1576(%rsp), %rdi        # 8-byte Reload
	movq	1424(%rsp), %rsi        # 8-byte Reload
	movq	%rax, %rdx
	movq	1432(%rsp), %rcx        # 8-byte Reload
	movq	1456(%rsp), %r8         # 8-byte Reload
	movq	1480(%rsp), %r9         # 8-byte Reload
	movq	1416(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	movq	%rax, 1400(%rsp)        # 8-byte Spill
	callq	trace_logger_log_ptr
.Ltmp146:
	#DEBUG_VALUE: main:store_loc <- [RSP+1560]
	movq	1624(%rsp), %rdi        # 8-byte Reload
	movq	1616(%rsp), %rsi        # 8-byte Reload
	movq	1608(%rsp), %rdx        # 8-byte Reload
	movq	1448(%rsp), %rcx        # 8-byte Reload
	movq	1552(%rsp), %r8         # 8-byte Reload
	movl	1592(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1432(%rsp), %rdi        # 8-byte Reload
	movq	1424(%rsp), %rsi        # 8-byte Reload
	movq	1560(%rsp), %rdx        # 8-byte Reload
	movq	1432(%rsp), %rcx        # 8-byte Reload
	movq	1568(%rsp), %r8         # 8-byte Reload
	movq	1480(%rsp), %r9         # 8-byte Reload
	movq	1416(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	3592(%rsp), %rax
	movq	1576(%rsp), %rdi        # 8-byte Reload
	movq	1424(%rsp), %rsi        # 8-byte Reload
	movq	%rax, %rdx
	movq	1432(%rsp), %rcx        # 8-byte Reload
	movq	1448(%rsp), %r8         # 8-byte Reload
	movq	1480(%rsp), %r9         # 8-byte Reload
	movq	1416(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	movq	%rax, 1392(%rsp)        # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	1624(%rsp), %rdi        # 8-byte Reload
	movq	1616(%rsp), %rsi        # 8-byte Reload
	movq	1608(%rsp), %rdx        # 8-byte Reload
	movq	1600(%rsp), %rcx        # 8-byte Reload
	movq	1584(%rsp), %r8         # 8-byte Reload
	movl	1592(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1440(%rsp), %rdi        # 8-byte Reload
	movq	1424(%rsp), %rsi        # 8-byte Reload
	movq	1464(%rsp), %rdx        # 8-byte Reload
	movq	1432(%rsp), %rcx        # 8-byte Reload
	movq	1632(%rsp), %r8         # 8-byte Reload
	movq	1480(%rsp), %r9         # 8-byte Reload
	movq	1416(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	1432(%rsp), %rdi        # 8-byte Reload
	movq	1424(%rsp), %rsi        # 8-byte Reload
	movq	1400(%rsp), %rdx        # 8-byte Reload
	movq	1432(%rsp), %rcx        # 8-byte Reload
	movq	1456(%rsp), %r8         # 8-byte Reload
	movq	1480(%rsp), %r9         # 8-byte Reload
	movq	1416(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	1408(%rsp), %rdi        # 8-byte Reload
	movq	1424(%rsp), %rsi        # 8-byte Reload
	movq	1392(%rsp), %rdx        # 8-byte Reload
	movq	1432(%rsp), %rcx        # 8-byte Reload
	movq	1448(%rsp), %r8         # 8-byte Reload
	movq	1480(%rsp), %r9         # 8-byte Reload
	movq	1416(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	1512(%rsp), %rdi        # 8-byte Reload
	movq	1640(%rsp), %rsi        # 8-byte Reload
	movq	1504(%rsp), %rdx        # 8-byte Reload
	movq	1480(%rsp), %rcx        # 8-byte Reload
	movq	1472(%rsp), %r8         # 8-byte Reload
	movq	1480(%rsp), %r9         # 8-byte Reload
	movq	1416(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	1400(%rsp), %rdi        # 8-byte Reload
	movq	1392(%rsp), %rsi        # 8-byte Reload
	movl	1500(%rsp), %edx        # 4-byte Reload
	callq	big
	movl	$1024, %edx             # imm = 0x400
	movabsq	$3, %rdi
	movabsq	$32, %rsi
	movabsq	$1024, %rax             # imm = 0x400
	movabsq	$0, %rcx
	leaq	.L.str10, %r8
	leaq	.L.str11, %r9
	movabsq	$2, %r10
	movabsq	$64, %r11
	movabsq	$1, %rbx
	leaq	.L.str98, %r14
	leaq	.L.str97, %r15
	movabsq	$4, %r12
	leaq	test_stores, %r13
	leaq	.L.str1, %rbp
	movq	%rax, 1384(%rsp)        # 8-byte Spill
	movabsq	$201, %rax
	movq	%rax, 1376(%rsp)        # 8-byte Spill
	leaq	.L.str73, %rax
	movq	%rax, 1368(%rsp)        # 8-byte Spill
	leaq	.L.str91, %rax
	movq	%rax, 1360(%rsp)        # 8-byte Spill
	leaq	.L.str99, %rax
	movq	%rax, 1352(%rsp)        # 8-byte Spill
	movabsq	$49, %rax
	movl	%edx, 1348(%rsp)        # 4-byte Spill
	movl	$0, %edx
	movl	%edx, 1344(%rsp)        # 4-byte Spill
	movl	$1, %edx
	movq	%rax, 1336(%rsp)        # 8-byte Spill
	movabsq	$19134, %rax            # imm = 0x4ABE
	movq	%rax, 1328(%rsp)        # 8-byte Spill
	leaq	.L.str22, %rax
	movq	%rax, 1320(%rsp)        # 8-byte Spill
	leaq	3592(%rsp), %rax
	movq	%rax, 1312(%rsp)        # 8-byte Spill
	movabsq	$27, %rax
	movq	%rax, 1304(%rsp)        # 8-byte Spill
	leaq	.L.str25, %rax
	movq	%rax, 1296(%rsp)        # 8-byte Spill
	leaq	3600(%rsp), %rax
.Ltmp147:
	#DEBUG_VALUE: main:store_vals <- RAX
	.loc	1 201 0                 # test_dma_load_store.c:201:0
	movq	%rax, 1288(%rsp)        # 8-byte Spill
.Ltmp148:
	#DEBUG_VALUE: main:store_vals <- [RSP+1288]
	movq	1376(%rsp), %rax        # 8-byte Reload
	movq	%rdi, 1280(%rsp)        # 8-byte Spill
	movq	%rax, %rdi
	movq	1368(%rsp), %rax        # 8-byte Reload
	movq	%rsi, 1272(%rsp)        # 8-byte Spill
	movq	%rax, %rsi
	movq	1360(%rsp), %rax        # 8-byte Reload
	movl	%edx, 1268(%rsp)        # 4-byte Spill
	movq	%rax, %rdx
	movq	%rcx, 1256(%rsp)        # 8-byte Spill
	movq	%r15, %rcx
	movq	1304(%rsp), %rax        # 8-byte Reload
	movq	%r8, 1248(%rsp)         # 8-byte Spill
	movq	%rax, %r8
	movl	1344(%rsp), %eax        # 4-byte Reload
	movq	%r9, 1240(%rsp)         # 8-byte Spill
	movl	%eax, %r9d
	movl	$1, (%rsp)
	movq	%r12, 1232(%rsp)        # 8-byte Spill
	movq	%rbp, 1224(%rsp)        # 8-byte Spill
	movq	%r13, 1216(%rsp)        # 8-byte Spill
	movq	%r15, 1208(%rsp)        # 8-byte Spill
	movq	%r14, 1200(%rsp)        # 8-byte Spill
	movq	%rbx, 1192(%rsp)        # 8-byte Spill
	movq	%r10, 1184(%rsp)        # 8-byte Spill
	movq	%r11, 1176(%rsp)        # 8-byte Spill
	callq	trace_logger_log0
	movq	1192(%rsp), %rdi        # 8-byte Reload
	movq	1176(%rsp), %rsi        # 8-byte Reload
	movq	1288(%rsp), %rdx        # 8-byte Reload
	movq	1192(%rsp), %rcx        # 8-byte Reload
	movq	1296(%rsp), %r8         # 8-byte Reload
	movq	1256(%rsp), %r9         # 8-byte Reload
	movq	1240(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	3600(%rsp), %rcx
	movq	1328(%rsp), %rdi        # 8-byte Reload
	movq	1176(%rsp), %rsi        # 8-byte Reload
	movq	%rcx, %rdx
	movq	1192(%rsp), %r8         # 8-byte Reload
	movq	%rcx, 1168(%rsp)        # 8-byte Spill
	movq	%r8, %rcx
	movq	1208(%rsp), %r8         # 8-byte Reload
	movq	1256(%rsp), %r9         # 8-byte Reload
	movq	1240(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
.Ltmp149:
	#DEBUG_VALUE: main:store_loc <- [RSP+1312]
	movq	1376(%rsp), %rdi        # 8-byte Reload
	movq	1368(%rsp), %rsi        # 8-byte Reload
	movq	1360(%rsp), %rdx        # 8-byte Reload
	movq	1200(%rsp), %rcx        # 8-byte Reload
	movq	1304(%rsp), %r8         # 8-byte Reload
	movl	1344(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1192(%rsp), %rdi        # 8-byte Reload
	movq	1176(%rsp), %rsi        # 8-byte Reload
	movq	1312(%rsp), %rdx        # 8-byte Reload
	movq	1192(%rsp), %rcx        # 8-byte Reload
	movq	1320(%rsp), %r8         # 8-byte Reload
	movq	1256(%rsp), %r9         # 8-byte Reload
	movq	1240(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	3592(%rsp), %rcx
	movq	1328(%rsp), %rdi        # 8-byte Reload
	movq	1176(%rsp), %rsi        # 8-byte Reload
	movq	%rcx, %rdx
	movq	1192(%rsp), %r8         # 8-byte Reload
	movq	%rcx, 1160(%rsp)        # 8-byte Spill
	movq	%r8, %rcx
	movq	1200(%rsp), %r8         # 8-byte Reload
	movq	1256(%rsp), %r9         # 8-byte Reload
	movq	1240(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	1376(%rsp), %rdi        # 8-byte Reload
	movq	1368(%rsp), %rsi        # 8-byte Reload
	movq	1360(%rsp), %rdx        # 8-byte Reload
	movq	1352(%rsp), %rcx        # 8-byte Reload
	movq	1336(%rsp), %r8         # 8-byte Reload
	movl	1344(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1232(%rsp), %rdi        # 8-byte Reload
	movq	1176(%rsp), %rsi        # 8-byte Reload
	movq	1216(%rsp), %rdx        # 8-byte Reload
	movq	1192(%rsp), %rcx        # 8-byte Reload
	movq	1224(%rsp), %r8         # 8-byte Reload
	movq	1256(%rsp), %r9         # 8-byte Reload
	movq	1240(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	1192(%rsp), %rdi        # 8-byte Reload
	movq	1176(%rsp), %rsi        # 8-byte Reload
	movq	1168(%rsp), %rdx        # 8-byte Reload
	movq	1192(%rsp), %rcx        # 8-byte Reload
	movq	1208(%rsp), %r8         # 8-byte Reload
	movq	1256(%rsp), %r9         # 8-byte Reload
	movq	1240(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	1184(%rsp), %rdi        # 8-byte Reload
	movq	1176(%rsp), %rsi        # 8-byte Reload
	movq	1160(%rsp), %rdx        # 8-byte Reload
	movq	1192(%rsp), %rcx        # 8-byte Reload
	movq	1200(%rsp), %r8         # 8-byte Reload
	movq	1256(%rsp), %r9         # 8-byte Reload
	movq	1240(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	1280(%rsp), %rdi        # 8-byte Reload
	movq	1272(%rsp), %rsi        # 8-byte Reload
	movq	1384(%rsp), %rdx        # 8-byte Reload
	movq	1256(%rsp), %rcx        # 8-byte Reload
	movq	1248(%rsp), %r8         # 8-byte Reload
	movq	1256(%rsp), %r9         # 8-byte Reload
	movq	1240(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	1168(%rsp), %rdi        # 8-byte Reload
	movq	1160(%rsp), %rsi        # 8-byte Reload
	movl	1348(%rsp), %edx        # 4-byte Reload
	callq	test_stores
	movabsq	$1, %rcx
	leaq	.L.str100, %rsi
	movabsq	$0, %rdi
	leaq	.L.str11, %r8
	movabsq	$2, %r9
	leaq	.L.str104, %r10
	movabsq	$3, %r11
	leaq	.L.str103, %rbx
	movabsq	$202, %r14
	leaq	.L.str73, %r15
	leaq	.L.str91, %r12
	leaq	.L.str102, %r13
	movl	$0, %edx
	movl	$1, %ebp
	movq	%rcx, 1152(%rsp)        # 8-byte Spill
	movabsq	$19134, %rcx            # imm = 0x4ABE
	movq	%rcx, 1144(%rsp)        # 8-byte Spill
	movabsq	$64, %rcx
	movq	%rcx, 1136(%rsp)        # 8-byte Spill
	leaq	.L.str101, %rcx
	movq	%rcx, 1128(%rsp)        # 8-byte Spill
	leaq	stdout, %rcx
	movq	%rcx, 1120(%rsp)        # 8-byte Spill
	leaq	.L.str32, %rcx
	movq	%rcx, 1112(%rsp)        # 8-byte Spill
	movabsq	$203, %rcx
	movq	%rcx, 1104(%rsp)        # 8-byte Spill
	movabsq	$27, %rcx
	movq	%rcx, 1096(%rsp)        # 8-byte Spill
	movabsq	$32, %rcx
	movq	%rcx, 1088(%rsp)        # 8-byte Spill
	leaq	.L.str99, %rcx
	movq	%rcx, 1080(%rsp)        # 8-byte Spill
	leaq	.L.str10, %rcx
	movq	%rcx, 1072(%rsp)        # 8-byte Spill
	movabsq	$46, %rcx
	movl	%eax, 1068(%rsp)        # 4-byte Spill
	movl	%eax, %eax
                                        # kill: RAX<def> EAX<kill>
	movq	%rax, 1056(%rsp)        # 8-byte Spill
	movq	1144(%rsp), %rax        # 8-byte Reload
	movq	%rdi, 1048(%rsp)        # 8-byte Spill
	movq	%rax, %rdi
	movq	1088(%rsp), %rax        # 8-byte Reload
	movq	%rsi, 1040(%rsp)        # 8-byte Spill
	movq	%rax, %rsi
	movq	1056(%rsp), %rax        # 8-byte Reload
	movl	%edx, 1036(%rsp)        # 4-byte Spill
	movq	%rax, %rdx
	movq	1152(%rsp), %rax        # 8-byte Reload
	movq	%rcx, 1024(%rsp)        # 8-byte Spill
	movq	%rax, %rcx
	movq	1080(%rsp), %rax        # 8-byte Reload
	movq	%r8, 1016(%rsp)         # 8-byte Spill
	movq	%rax, %r8
	movq	1048(%rsp), %rax        # 8-byte Reload
	movq	%r9, 1008(%rsp)         # 8-byte Spill
	movq	%rax, %r9
	movq	1016(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	movl	%ebp, 1004(%rsp)        # 4-byte Spill
	movq	%r12, 992(%rsp)         # 8-byte Spill
	movq	%r13, 984(%rsp)         # 8-byte Spill
	movq	%r15, 976(%rsp)         # 8-byte Spill
	movq	%r14, 968(%rsp)         # 8-byte Spill
	movq	%r10, 960(%rsp)         # 8-byte Spill
	movq	%r11, 952(%rsp)         # 8-byte Spill
	movq	%rbx, 944(%rsp)         # 8-byte Spill
	callq	trace_logger_log_int
.Ltmp150:
	#DEBUG_VALUE: main:num_failures <- [RSP+1068]
	.loc	1 202 0                 # test_dma_load_store.c:202:0
	movq	968(%rsp), %rdi         # 8-byte Reload
	movq	976(%rsp), %rsi         # 8-byte Reload
	movq	992(%rsp), %rdx         # 8-byte Reload
	movq	1040(%rsp), %rcx        # 8-byte Reload
	movq	1024(%rsp), %r8         # 8-byte Reload
	movl	1036(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1008(%rsp), %rdi        # 8-byte Reload
	movq	1088(%rsp), %rsi        # 8-byte Reload
	movq	1048(%rsp), %rdx        # 8-byte Reload
	movq	1048(%rsp), %rcx        # 8-byte Reload
	movq	1072(%rsp), %r8         # 8-byte Reload
	movq	1048(%rsp), %r9         # 8-byte Reload
	movq	1016(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	1068(%rsp), %ebp        # 4-byte Reload
	movl	%ebp, %esi
	movl	%esi, %edx
	movq	1152(%rsp), %rdi        # 8-byte Reload
	movq	1088(%rsp), %rsi        # 8-byte Reload
	movq	1152(%rsp), %rcx        # 8-byte Reload
	movq	1080(%rsp), %r8         # 8-byte Reload
	movq	1048(%rsp), %r9         # 8-byte Reload
	movq	1016(%rsp), %rax        # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	1068(%rsp), %ebp        # 4-byte Reload
	cmpl	$0, %ebp
	sete	%al
	.loc	1 203 0                 # test_dma_load_store.c:203:0
.Ltmp151:
	movb	%al, %cl
	andb	$1, %cl
	movzbl	%cl, %ecx
	movl	%ecx, %edx
	movq	1144(%rsp), %rdi        # 8-byte Reload
	movq	1152(%rsp), %rsi        # 8-byte Reload
	movq	1152(%rsp), %rcx        # 8-byte Reload
	movq	1040(%rsp), %r8         # 8-byte Reload
	movq	1048(%rsp), %r9         # 8-byte Reload
	movq	1016(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	movb	%al, 943(%rsp)          # 1-byte Spill
	callq	trace_logger_log_int
	movq	1104(%rsp), %rdi        # 8-byte Reload
	movq	976(%rsp), %rsi         # 8-byte Reload
	movq	992(%rsp), %rdx         # 8-byte Reload
	movq	1128(%rsp), %rcx        # 8-byte Reload
	movq	1096(%rsp), %r8         # 8-byte Reload
	movl	1036(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	1152(%rsp), %rdi        # 8-byte Reload
	movq	1136(%rsp), %rsi        # 8-byte Reload
	movq	1120(%rsp), %rdx        # 8-byte Reload
	movq	1152(%rsp), %rcx        # 8-byte Reload
	movq	1112(%rsp), %r8         # 8-byte Reload
	movq	1048(%rsp), %r9         # 8-byte Reload
	movq	1016(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	stdout, %rcx
.Ltmp152:
	.loc	1 202 0                 # test_dma_load_store.c:202:0
	movq	1144(%rsp), %rdi        # 8-byte Reload
	movq	1136(%rsp), %rsi        # 8-byte Reload
	movq	%rcx, %rdx
	movq	1152(%rsp), %r8         # 8-byte Reload
	movq	%rcx, 928(%rsp)         # 8-byte Spill
	movq	%r8, %rcx
	movq	1128(%rsp), %r8         # 8-byte Reload
	movq	1048(%rsp), %r9         # 8-byte Reload
	movq	1016(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	968(%rsp), %rdi         # 8-byte Reload
	movq	976(%rsp), %rsi         # 8-byte Reload
	movq	992(%rsp), %rdx         # 8-byte Reload
	movq	984(%rsp), %rcx         # 8-byte Reload
	movq	1008(%rsp), %r8         # 8-byte Reload
	movl	1036(%rsp), %r9d        # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	952(%rsp), %rdi         # 8-byte Reload
	movq	1048(%rsp), %rsi        # 8-byte Reload
	movq	1048(%rsp), %rdx        # 8-byte Reload
	movq	1152(%rsp), %rcx        # 8-byte Reload
	movq	944(%rsp), %r8          # 8-byte Reload
	movq	1048(%rsp), %r9         # 8-byte Reload
	movq	1016(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	1008(%rsp), %rdi        # 8-byte Reload
	movq	1048(%rsp), %rsi        # 8-byte Reload
	movq	1048(%rsp), %rdx        # 8-byte Reload
	movq	1152(%rsp), %rcx        # 8-byte Reload
	movq	960(%rsp), %r8          # 8-byte Reload
	movq	1048(%rsp), %r9         # 8-byte Reload
	movq	1016(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	943(%rsp), %al          # 1-byte Reload
	andb	$1, %al
	movzbl	%al, %ebp
	movl	%ebp, %edx
	movq	1152(%rsp), %rdi        # 8-byte Reload
	movq	1152(%rsp), %rsi        # 8-byte Reload
	movq	1152(%rsp), %rcx        # 8-byte Reload
	movq	1040(%rsp), %r8         # 8-byte Reload
	movq	1048(%rsp), %r9         # 8-byte Reload
	movq	1016(%rsp), %r10        # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	943(%rsp), %al          # 1-byte Reload
	testb	$1, %al
	jne	.LBB4_8
.Ltmp153:
# BB#7:
	leaq	.L.str5, %rax
	movabsq	$2, %rdi
	movabsq	$64, %rcx
	movabsq	$0, %rdx
	leaq	.L.str10, %r8
	leaq	.L.str11, %rsi
	movabsq	$1, %r9
	leaq	.L.str101, %r10
	movabsq	$4, %r11
	leaq	fprintf, %rbx
	leaq	.L.str37, %r14
	movabsq	$203, %r15
	leaq	.L.str73, %r12
	leaq	.L.str104, %r13
	leaq	.L.str105, %rbp
	movq	%rax, 920(%rsp)         # 8-byte Spill
	movabsq	$49, %rax
	movq	%rsi, 912(%rsp)         # 8-byte Spill
	movl	$0, %esi
	movl	%esi, 908(%rsp)         # 4-byte Spill
	movl	$1, %esi
	.loc	1 203 0                 # test_dma_load_store.c:203:0
.Ltmp154:
	movq	%rdi, 896(%rsp)         # 8-byte Spill
	movq	%r15, %rdi
	movl	%esi, 892(%rsp)         # 4-byte Spill
	movq	%r12, %rsi
	movq	%rdx, 880(%rsp)         # 8-byte Spill
	movq	%r13, %rdx
	movq	%rcx, 872(%rsp)         # 8-byte Spill
	movq	%rbp, %rcx
	movq	%r8, 864(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movl	908(%rsp), %ebp         # 4-byte Reload
	movq	%r9, 856(%rsp)          # 8-byte Spill
	movl	%ebp, %r9d
	movl	$1, (%rsp)
	movq	%r14, 848(%rsp)         # 8-byte Spill
	movq	%r10, 840(%rsp)         # 8-byte Spill
	movq	%rbx, 832(%rsp)         # 8-byte Spill
	movq	%r11, 824(%rsp)         # 8-byte Spill
	callq	trace_logger_log0
	movq	824(%rsp), %rdi         # 8-byte Reload
	movq	872(%rsp), %rsi         # 8-byte Reload
	movq	832(%rsp), %rdx         # 8-byte Reload
	movq	856(%rsp), %rcx         # 8-byte Reload
	movq	848(%rsp), %r8          # 8-byte Reload
	movq	880(%rsp), %r9          # 8-byte Reload
	movq	912(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	856(%rsp), %rdi         # 8-byte Reload
	movq	872(%rsp), %rsi         # 8-byte Reload
	movq	928(%rsp), %rdx         # 8-byte Reload
	movq	856(%rsp), %rcx         # 8-byte Reload
	movq	840(%rsp), %r8          # 8-byte Reload
	movq	880(%rsp), %r9          # 8-byte Reload
	movq	912(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	896(%rsp), %rdi         # 8-byte Reload
	movq	872(%rsp), %rsi         # 8-byte Reload
	movq	920(%rsp), %rdx         # 8-byte Reload
	movq	880(%rsp), %rcx         # 8-byte Reload
	movq	864(%rsp), %r8          # 8-byte Reload
	movq	880(%rsp), %r9          # 8-byte Reload
	movq	912(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	928(%rsp), %rdi         # 8-byte Reload
	movq	920(%rsp), %rsi         # 8-byte Reload
	movl	1068(%rsp), %edx        # 4-byte Reload
	movb	$0, %al
	callq	fprintf
	movl	$4294967295, %edx       # imm = 0xFFFFFFFF
	movabsq	$1, %rcx
	movabsq	$0, %rsi
	leaq	.L.str107, %r8
	leaq	.L.str11, %rdi
	movabsq	$204, %r9
	leaq	.L.str73, %r10
	leaq	.L.str104, %r11
	leaq	.L.str106, %rbx
	movabsq	$2, %r14
	movl	$0, %ebp
	movl	$1, %r15d
	movabsq	$19134, %r12            # imm = 0x4ABE
	movabsq	$32, %r13
	movq	%rcx, 816(%rsp)         # 8-byte Spill
	leaq	.L.str105, %rcx
	.loc	1 204 0                 # test_dma_load_store.c:204:0
	movl	%eax, %eax
                                        # kill: RAX<def> EAX<kill>
	movq	%rdi, 808(%rsp)         # 8-byte Spill
	movq	%r12, %rdi
	movq	%rsi, 800(%rsp)         # 8-byte Spill
	movq	%r13, %rsi
	movl	%edx, 796(%rsp)         # 4-byte Spill
	movq	%rax, %rdx
	movq	816(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 784(%rsp)         # 8-byte Spill
	movq	%rax, %rcx
	movq	784(%rsp), %r12         # 8-byte Reload
	movq	%r8, 776(%rsp)          # 8-byte Spill
	movq	%r12, %r8
	movq	800(%rsp), %r13         # 8-byte Reload
	movq	%r9, 768(%rsp)          # 8-byte Spill
	movq	%r13, %r9
	movq	808(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	movl	%r15d, 764(%rsp)        # 4-byte Spill
	movq	%rbx, 752(%rsp)         # 8-byte Spill
	movl	%ebp, 748(%rsp)         # 4-byte Spill
	movq	%r14, 736(%rsp)         # 8-byte Spill
	movq	%r10, 728(%rsp)         # 8-byte Spill
	movq	%r11, 720(%rsp)         # 8-byte Spill
	callq	trace_logger_log_int
	movq	768(%rsp), %rdi         # 8-byte Reload
	movq	728(%rsp), %rsi         # 8-byte Reload
	movq	720(%rsp), %rdx         # 8-byte Reload
	movq	752(%rsp), %rcx         # 8-byte Reload
	movq	736(%rsp), %r8          # 8-byte Reload
	movl	748(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	816(%rsp), %rdi         # 8-byte Reload
	movq	800(%rsp), %rsi         # 8-byte Reload
	movq	800(%rsp), %rdx         # 8-byte Reload
	movq	816(%rsp), %rcx         # 8-byte Reload
	movq	776(%rsp), %r8          # 8-byte Reload
	movq	800(%rsp), %r9          # 8-byte Reload
	movq	808(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	796(%rsp), %ebp         # 4-byte Reload
	movl	%ebp, 716(%rsp)         # 4-byte Spill
	jmp	.LBB4_9
.Ltmp155:
.LBB4_8:
	leaq	.L.str6, %rax
	movabsq	$2, %rdi
	movabsq	$64, %rcx
	movabsq	$0, %rdx
	leaq	.L.str10, %r8
	leaq	.L.str11, %rsi
	movabsq	$1, %r9
	leaq	.L.str101, %r10
	movabsq	$3, %r11
	leaq	fprintf, %rbx
	leaq	.L.str37, %r14
	movabsq	$206, %r15
	leaq	.L.str73, %r12
	leaq	.L.str103, %r13
	leaq	.L.str108, %rbp
	movq	%rax, 704(%rsp)         # 8-byte Spill
	movabsq	$49, %rax
	movq	%rsi, 696(%rsp)         # 8-byte Spill
	movl	$0, %esi
	movl	%esi, 692(%rsp)         # 4-byte Spill
	movl	$1, %esi
	.loc	1 206 0                 # test_dma_load_store.c:206:0
	movq	%rdi, 680(%rsp)         # 8-byte Spill
	movq	%r15, %rdi
	movl	%esi, 676(%rsp)         # 4-byte Spill
	movq	%r12, %rsi
	movq	%rdx, 664(%rsp)         # 8-byte Spill
	movq	%r13, %rdx
	movq	%rcx, 656(%rsp)         # 8-byte Spill
	movq	%rbp, %rcx
	movq	%r8, 648(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movl	692(%rsp), %ebp         # 4-byte Reload
	movq	%r9, 640(%rsp)          # 8-byte Spill
	movl	%ebp, %r9d
	movl	$1, (%rsp)
	movq	%r14, 632(%rsp)         # 8-byte Spill
	movq	%r10, 624(%rsp)         # 8-byte Spill
	movq	%rbx, 616(%rsp)         # 8-byte Spill
	movq	%r11, 608(%rsp)         # 8-byte Spill
	callq	trace_logger_log0
	movq	608(%rsp), %rdi         # 8-byte Reload
	movq	656(%rsp), %rsi         # 8-byte Reload
	movq	616(%rsp), %rdx         # 8-byte Reload
	movq	640(%rsp), %rcx         # 8-byte Reload
	movq	632(%rsp), %r8          # 8-byte Reload
	movq	664(%rsp), %r9          # 8-byte Reload
	movq	696(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	640(%rsp), %rdi         # 8-byte Reload
	movq	656(%rsp), %rsi         # 8-byte Reload
	movq	928(%rsp), %rdx         # 8-byte Reload
	movq	640(%rsp), %rcx         # 8-byte Reload
	movq	624(%rsp), %r8          # 8-byte Reload
	movq	664(%rsp), %r9          # 8-byte Reload
	movq	696(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	680(%rsp), %rdi         # 8-byte Reload
	movq	656(%rsp), %rsi         # 8-byte Reload
	movq	704(%rsp), %rdx         # 8-byte Reload
	movq	664(%rsp), %rcx         # 8-byte Reload
	movq	648(%rsp), %r8          # 8-byte Reload
	movq	664(%rsp), %r9          # 8-byte Reload
	movq	696(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	928(%rsp), %rdi         # 8-byte Reload
	movq	704(%rsp), %rsi         # 8-byte Reload
	movb	$0, %al
	callq	fprintf
	leaq	.L.str7, %rcx
	movl	$0, %ebp
	movabsq	$2, %rdi
	movabsq	$64, %rdx
	movabsq	$0, %rsi
	leaq	.L.str10, %r8
	leaq	.L.str11, %r9
	movabsq	$1, %r10
	leaq	.L.str109, %r11
	movabsq	$6, %rbx
	leaq	fprintf, %r14
	leaq	.L.str37, %r15
	movabsq	$207, %r12
	leaq	.L.str73, %r13
	movq	%rcx, 600(%rsp)         # 8-byte Spill
	leaq	.L.str103, %rcx
	movq	%rcx, 592(%rsp)         # 8-byte Spill
	leaq	.L.str116, %rcx
	movq	%rcx, 584(%rsp)         # 8-byte Spill
	movabsq	$49, %rcx
	movl	%eax, 580(%rsp)         # 4-byte Spill
	movl	$1, %eax
	movq	%rcx, 568(%rsp)         # 8-byte Spill
	movabsq	$19134, %rcx            # imm = 0x4ABE
	movq	%rcx, 560(%rsp)         # 8-byte Spill
	leaq	.L.str115, %rcx
	movq	%rcx, 552(%rsp)         # 8-byte Spill
	movabsq	$32, %rcx
	movq	%rcx, 544(%rsp)         # 8-byte Spill
	leaq	.L.str114, %rcx
	movq	%rcx, 536(%rsp)         # 8-byte Spill
	movabsq	$41, %rcx
	movq	%rcx, 528(%rsp)         # 8-byte Spill
	leaq	.L.str113, %rcx
	movq	%rcx, 520(%rsp)         # 8-byte Spill
	movabsq	$27, %rcx
	movq	%rcx, 512(%rsp)         # 8-byte Spill
	leaq	.L.str25, %rcx
	movq	%rcx, 504(%rsp)         # 8-byte Spill
	leaq	3600(%rsp), %rcx
	movq	%rcx, 496(%rsp)         # 8-byte Spill
	leaq	.L.str112, %rcx
	movq	%rcx, 488(%rsp)         # 8-byte Spill
	leaq	.L.str111, %rcx
	movq	%rcx, 480(%rsp)         # 8-byte Spill
	leaq	.L.str110, %rcx
	movq	%rcx, 472(%rsp)         # 8-byte Spill
	leaq	.L.str22, %rcx
	movq	%rcx, 464(%rsp)         # 8-byte Spill
	leaq	3592(%rsp), %rcx
	movq	%rcx, 456(%rsp)         # 8-byte Spill
	leaq	stdout, %rcx
	movq	%rcx, 448(%rsp)         # 8-byte Spill
	leaq	.L.str32, %rcx
	movq	%rcx, 440(%rsp)         # 8-byte Spill
	leaq	.L.str108, %rcx
	.loc	1 207 0                 # test_dma_load_store.c:207:0
	movl	%eax, 436(%rsp)         # 4-byte Spill
	movl	580(%rsp), %eax         # 4-byte Reload
	movl	%eax, %eax
                                        # kill: RAX<def> EAX<kill>
	movq	%rax, 424(%rsp)         # 8-byte Spill
	movq	560(%rsp), %rax         # 8-byte Reload
	movq	%rdi, 416(%rsp)         # 8-byte Spill
	movq	%rax, %rdi
	movq	544(%rsp), %rax         # 8-byte Reload
	movq	%rsi, 408(%rsp)         # 8-byte Spill
	movq	%rax, %rsi
	movq	424(%rsp), %rax         # 8-byte Reload
	movq	%rdx, 400(%rsp)         # 8-byte Spill
	movq	%rax, %rdx
	movq	%rcx, 392(%rsp)         # 8-byte Spill
	movq	%r10, %rcx
	movq	392(%rsp), %rax         # 8-byte Reload
	movq	%r8, 384(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movq	408(%rsp), %rax         # 8-byte Reload
	movq	%r9, 376(%rsp)          # 8-byte Spill
	movq	%rax, %r9
	movq	376(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	movq	%r13, 368(%rsp)         # 8-byte Spill
	movq	%r14, 360(%rsp)         # 8-byte Spill
	movl	%ebp, 356(%rsp)         # 4-byte Spill
	movq	%r15, 344(%rsp)         # 8-byte Spill
	movq	%r12, 336(%rsp)         # 8-byte Spill
	movq	%rbx, 328(%rsp)         # 8-byte Spill
	movq	%r11, 320(%rsp)         # 8-byte Spill
	movq	%r10, 312(%rsp)         # 8-byte Spill
	callq	trace_logger_log_int
	movq	336(%rsp), %rdi         # 8-byte Reload
	movq	368(%rsp), %rsi         # 8-byte Reload
	movq	592(%rsp), %rdx         # 8-byte Reload
	movq	320(%rsp), %rcx         # 8-byte Reload
	movq	512(%rsp), %r8          # 8-byte Reload
	movl	356(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	312(%rsp), %rdi         # 8-byte Reload
	movq	400(%rsp), %rsi         # 8-byte Reload
	movq	448(%rsp), %rdx         # 8-byte Reload
	movq	312(%rsp), %rcx         # 8-byte Reload
	movq	440(%rsp), %r8          # 8-byte Reload
	movq	408(%rsp), %r9          # 8-byte Reload
	movq	376(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	stdout, %rax
	movq	560(%rsp), %rdi         # 8-byte Reload
	movq	400(%rsp), %rsi         # 8-byte Reload
	movq	%rax, %rdx
	movq	312(%rsp), %rcx         # 8-byte Reload
	movq	320(%rsp), %r8          # 8-byte Reload
	movq	408(%rsp), %r9          # 8-byte Reload
	movq	376(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movq	%rax, 304(%rsp)         # 8-byte Spill
	callq	trace_logger_log_ptr
.Ltmp156:
	#DEBUG_VALUE: main:store_loc <- [RSP+456]
	movq	336(%rsp), %rdi         # 8-byte Reload
	movq	368(%rsp), %rsi         # 8-byte Reload
	movq	592(%rsp), %rdx         # 8-byte Reload
	movq	472(%rsp), %rcx         # 8-byte Reload
	movq	512(%rsp), %r8          # 8-byte Reload
	movl	356(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	312(%rsp), %rdi         # 8-byte Reload
	movq	400(%rsp), %rsi         # 8-byte Reload
	movq	456(%rsp), %rdx         # 8-byte Reload
	movq	312(%rsp), %rcx         # 8-byte Reload
	movq	464(%rsp), %r8          # 8-byte Reload
	movq	408(%rsp), %r9          # 8-byte Reload
	movq	376(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	3592(%rsp), %rax
	movq	560(%rsp), %rdi         # 8-byte Reload
	movq	400(%rsp), %rsi         # 8-byte Reload
	movq	%rax, %rdx
	movq	312(%rsp), %rcx         # 8-byte Reload
	movq	472(%rsp), %r8          # 8-byte Reload
	movq	408(%rsp), %r9          # 8-byte Reload
	movq	376(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movq	%rax, 296(%rsp)         # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	336(%rsp), %rdi         # 8-byte Reload
	movq	368(%rsp), %rsi         # 8-byte Reload
	movq	592(%rsp), %rdx         # 8-byte Reload
	movq	480(%rsp), %rcx         # 8-byte Reload
	movq	512(%rsp), %r8          # 8-byte Reload
	movl	356(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	312(%rsp), %rdi         # 8-byte Reload
	movq	400(%rsp), %rsi         # 8-byte Reload
	movq	296(%rsp), %rdx         # 8-byte Reload
	movq	312(%rsp), %rcx         # 8-byte Reload
	movq	472(%rsp), %r8          # 8-byte Reload
	movq	408(%rsp), %r9          # 8-byte Reload
	movq	376(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	296(%rsp), %rax         # 8-byte Reload
	vmovss	(%rax), %xmm0
	cvtss2sd	%xmm0, %xmm1
	movq	560(%rsp), %rdi         # 8-byte Reload
	movq	544(%rsp), %rsi         # 8-byte Reload
	vmovss	%xmm0, 292(%rsp)        # 4-byte Spill
	vmovaps	%xmm1, %xmm0
	movq	312(%rsp), %rdx         # 8-byte Reload
	movq	480(%rsp), %rcx         # 8-byte Reload
	movq	408(%rsp), %r8          # 8-byte Reload
	movq	376(%rsp), %r9          # 8-byte Reload
	callq	trace_logger_log_double
	movq	336(%rsp), %rdi         # 8-byte Reload
	movq	368(%rsp), %rsi         # 8-byte Reload
	movq	592(%rsp), %rdx         # 8-byte Reload
	movq	488(%rsp), %rcx         # 8-byte Reload
	movq	528(%rsp), %r8          # 8-byte Reload
	movl	356(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	vmovss	292(%rsp), %xmm0        # 4-byte Reload
	cvtss2sd	%xmm0, %xmm0
	movq	312(%rsp), %rdi         # 8-byte Reload
	movq	544(%rsp), %rsi         # 8-byte Reload
	movq	312(%rsp), %rdx         # 8-byte Reload
	movq	480(%rsp), %rcx         # 8-byte Reload
	movq	408(%rsp), %r8          # 8-byte Reload
	movq	376(%rsp), %r9          # 8-byte Reload
	callq	trace_logger_log_double
	vmovss	292(%rsp), %xmm0        # 4-byte Reload
	cvtss2sd	%xmm0, %xmm1
	movq	560(%rsp), %rdi         # 8-byte Reload
	movq	400(%rsp), %rsi         # 8-byte Reload
	vmovaps	%xmm1, %xmm0
	movq	312(%rsp), %rdx         # 8-byte Reload
	movq	488(%rsp), %rcx         # 8-byte Reload
	movq	408(%rsp), %r8          # 8-byte Reload
	movq	376(%rsp), %r9          # 8-byte Reload
	vmovsd	%xmm1, 280(%rsp)        # 8-byte Spill
	callq	trace_logger_log_double
.Ltmp157:
	#DEBUG_VALUE: main:store_vals <- [RSP+496]
	movq	336(%rsp), %rdi         # 8-byte Reload
	movq	368(%rsp), %rsi         # 8-byte Reload
	movq	592(%rsp), %rdx         # 8-byte Reload
	movq	520(%rsp), %rcx         # 8-byte Reload
	movq	512(%rsp), %r8          # 8-byte Reload
	movl	356(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	312(%rsp), %rdi         # 8-byte Reload
	movq	400(%rsp), %rsi         # 8-byte Reload
	movq	496(%rsp), %rdx         # 8-byte Reload
	movq	312(%rsp), %rcx         # 8-byte Reload
	movq	504(%rsp), %r8          # 8-byte Reload
	movq	408(%rsp), %r9          # 8-byte Reload
	movq	376(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	3600(%rsp), %rax
	movq	560(%rsp), %rdi         # 8-byte Reload
	movq	400(%rsp), %rsi         # 8-byte Reload
	movq	%rax, %rdx
	movq	312(%rsp), %rcx         # 8-byte Reload
	movq	520(%rsp), %r8          # 8-byte Reload
	movq	408(%rsp), %r9          # 8-byte Reload
	movq	376(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movq	%rax, 272(%rsp)         # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	336(%rsp), %rdi         # 8-byte Reload
	movq	368(%rsp), %rsi         # 8-byte Reload
	movq	592(%rsp), %rdx         # 8-byte Reload
	movq	536(%rsp), %rcx         # 8-byte Reload
	movq	512(%rsp), %r8          # 8-byte Reload
	movl	356(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	312(%rsp), %rdi         # 8-byte Reload
	movq	400(%rsp), %rsi         # 8-byte Reload
	movq	272(%rsp), %rdx         # 8-byte Reload
	movq	312(%rsp), %rcx         # 8-byte Reload
	movq	520(%rsp), %r8          # 8-byte Reload
	movq	408(%rsp), %r9          # 8-byte Reload
	movq	376(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	272(%rsp), %rax         # 8-byte Reload
	vmovss	(%rax), %xmm0
	cvtss2sd	%xmm0, %xmm1
	movq	560(%rsp), %rdi         # 8-byte Reload
	movq	544(%rsp), %rsi         # 8-byte Reload
	vmovss	%xmm0, 268(%rsp)        # 4-byte Spill
	vmovaps	%xmm1, %xmm0
	movq	312(%rsp), %rdx         # 8-byte Reload
	movq	536(%rsp), %rcx         # 8-byte Reload
	movq	408(%rsp), %r8          # 8-byte Reload
	movq	376(%rsp), %r9          # 8-byte Reload
	callq	trace_logger_log_double
	movq	336(%rsp), %rdi         # 8-byte Reload
	movq	368(%rsp), %rsi         # 8-byte Reload
	movq	592(%rsp), %rdx         # 8-byte Reload
	movq	552(%rsp), %rcx         # 8-byte Reload
	movq	528(%rsp), %r8          # 8-byte Reload
	movl	356(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	vmovss	268(%rsp), %xmm0        # 4-byte Reload
	cvtss2sd	%xmm0, %xmm0
	movq	312(%rsp), %rdi         # 8-byte Reload
	movq	544(%rsp), %rsi         # 8-byte Reload
	movq	312(%rsp), %rdx         # 8-byte Reload
	movq	536(%rsp), %rcx         # 8-byte Reload
	movq	408(%rsp), %r8          # 8-byte Reload
	movq	376(%rsp), %r9          # 8-byte Reload
	callq	trace_logger_log_double
	vmovss	268(%rsp), %xmm0        # 4-byte Reload
	cvtss2sd	%xmm0, %xmm1
	movq	560(%rsp), %rdi         # 8-byte Reload
	movq	400(%rsp), %rsi         # 8-byte Reload
	vmovaps	%xmm1, %xmm0
	movq	312(%rsp), %rdx         # 8-byte Reload
	movq	552(%rsp), %rcx         # 8-byte Reload
	movq	408(%rsp), %r8          # 8-byte Reload
	movq	376(%rsp), %r9          # 8-byte Reload
	vmovsd	%xmm1, 256(%rsp)        # 8-byte Spill
	callq	trace_logger_log_double
	movq	336(%rsp), %rdi         # 8-byte Reload
	movq	368(%rsp), %rsi         # 8-byte Reload
	movq	592(%rsp), %rdx         # 8-byte Reload
	movq	584(%rsp), %rcx         # 8-byte Reload
	movq	568(%rsp), %r8          # 8-byte Reload
	movl	356(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	328(%rsp), %rdi         # 8-byte Reload
	movq	400(%rsp), %rsi         # 8-byte Reload
	movq	360(%rsp), %rdx         # 8-byte Reload
	movq	312(%rsp), %rcx         # 8-byte Reload
	movq	344(%rsp), %r8          # 8-byte Reload
	movq	408(%rsp), %r9          # 8-byte Reload
	movq	376(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	312(%rsp), %rdi         # 8-byte Reload
	movq	400(%rsp), %rsi         # 8-byte Reload
	movq	304(%rsp), %rdx         # 8-byte Reload
	movq	312(%rsp), %rcx         # 8-byte Reload
	movq	320(%rsp), %r8          # 8-byte Reload
	movq	408(%rsp), %r9          # 8-byte Reload
	movq	376(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	416(%rsp), %rdi         # 8-byte Reload
	movq	400(%rsp), %rsi         # 8-byte Reload
	movq	600(%rsp), %rdx         # 8-byte Reload
	movq	408(%rsp), %rcx         # 8-byte Reload
	movq	384(%rsp), %r8          # 8-byte Reload
	movq	408(%rsp), %r9          # 8-byte Reload
	movq	376(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	304(%rsp), %rdi         # 8-byte Reload
	movq	600(%rsp), %rsi         # 8-byte Reload
	movl	356(%rsp), %edx         # 4-byte Reload
	vmovsd	280(%rsp), %xmm0        # 8-byte Reload
	vmovsd	256(%rsp), %xmm1        # 8-byte Reload
	movb	$2, %al
	callq	fprintf
	movl	$0, %edx
	movabsq	$1, %rcx
	movabsq	$0, %rsi
	leaq	.L.str107, %r8
	leaq	.L.str11, %rdi
	movabsq	$209, %r9
	leaq	.L.str73, %r10
	leaq	.L.str103, %r11
	leaq	.L.str117, %rbx
	movabsq	$2, %r14
	movl	$1, %ebp
	movabsq	$19134, %r15            # imm = 0x4ABE
	movabsq	$32, %r12
	leaq	.L.str116, %r13
	.loc	1 209 0                 # test_dma_load_store.c:209:0
	movl	%eax, %eax
                                        # kill: RAX<def> EAX<kill>
	movq	%rdi, 248(%rsp)         # 8-byte Spill
	movq	%r15, %rdi
	movq	%rsi, 240(%rsp)         # 8-byte Spill
	movq	%r12, %rsi
	movl	%edx, 236(%rsp)         # 4-byte Spill
	movq	%rax, %rdx
	movq	%rcx, 224(%rsp)         # 8-byte Spill
	movq	%r8, 216(%rsp)          # 8-byte Spill
	movq	%r13, %r8
	movq	240(%rsp), %rax         # 8-byte Reload
	movq	%r9, 208(%rsp)          # 8-byte Spill
	movq	%rax, %r9
	movq	248(%rsp), %r15         # 8-byte Reload
	movq	%r15, (%rsp)
	movq	%r10, 200(%rsp)         # 8-byte Spill
	movq	%rbx, 192(%rsp)         # 8-byte Spill
	movq	%r11, 184(%rsp)         # 8-byte Spill
	movl	%ebp, 180(%rsp)         # 4-byte Spill
	movq	%r14, 168(%rsp)         # 8-byte Spill
	callq	trace_logger_log_int
	movq	208(%rsp), %rdi         # 8-byte Reload
	movq	200(%rsp), %rsi         # 8-byte Reload
	movq	184(%rsp), %rdx         # 8-byte Reload
	movq	192(%rsp), %rcx         # 8-byte Reload
	movq	168(%rsp), %r8          # 8-byte Reload
	movl	236(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	224(%rsp), %rdi         # 8-byte Reload
	movq	240(%rsp), %rsi         # 8-byte Reload
	movq	240(%rsp), %rdx         # 8-byte Reload
	movq	224(%rsp), %rcx         # 8-byte Reload
	movq	216(%rsp), %r8          # 8-byte Reload
	movq	240(%rsp), %r9          # 8-byte Reload
	movq	248(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	236(%rsp), %ebp         # 4-byte Reload
	movl	%ebp, 716(%rsp)         # 4-byte Spill
.Ltmp158:
.LBB4_9:
	movl	716(%rsp), %eax         # 4-byte Reload
	movabsq	$1, %rcx
	movabsq	$32, %rdx
	leaq	.L.str118, %rsi
	movabsq	$0, %rdi
	leaq	.L.str11, %r8
	movabsq	$210, %r9
	leaq	.L.str73, %r10
	leaq	.L.str107, %r11
	leaq	.L.str119, %rbx
	movl	$0, %ebp
	movl	$1, %r14d
	movabsq	$19134, %r15            # imm = 0x4ABE
	leaq	.L.str104, %r12
	movabsq	$4294967295, %r13       # imm = 0xFFFFFFFF
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	leaq	.L.str10, %rcx
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	movabsq	$2, %rcx
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	leaq	.L.str103, %rcx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	movabsq	$-1, %rcx
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	movabsq	$48, %rcx
	.loc	1 210 0                 # test_dma_load_store.c:210:0
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	movq	128(%rsp), %rcx         # 8-byte Reload
	movq	%rdi, 112(%rsp)         # 8-byte Spill
	movq	%rcx, %rdi
	movq	%rsi, 104(%rsp)         # 8-byte Spill
	movq	%r10, %rsi
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	movq	%r11, %rdx
	movq	104(%rsp), %rcx         # 8-byte Reload
	movq	%r8, 88(%rsp)           # 8-byte Spill
	movq	120(%rsp), %r8          # 8-byte Reload
	movq	%r9, 80(%rsp)           # 8-byte Spill
	movl	%ebp, %r9d
	movl	$1, (%rsp)
	movl	%eax, 76(%rsp)          # 4-byte Spill
	movl	%r14d, 72(%rsp)         # 4-byte Spill
	movq	%r15, 64(%rsp)          # 8-byte Spill
	movq	%r12, 56(%rsp)          # 8-byte Spill
	movq	%r13, 48(%rsp)          # 8-byte Spill
	movl	%ebp, 44(%rsp)          # 4-byte Spill
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	%r10, 24(%rsp)          # 8-byte Spill
	movq	%r11, 16(%rsp)          # 8-byte Spill
	callq	trace_logger_log0
	movq	144(%rsp), %rdi         # 8-byte Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	112(%rsp), %rdx         # 8-byte Reload
	movq	112(%rsp), %rcx         # 8-byte Reload
	movq	152(%rsp), %r8          # 8-byte Reload
	movq	160(%rsp), %r9          # 8-byte Reload
	movq	136(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	160(%rsp), %rdi         # 8-byte Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	112(%rsp), %rcx         # 8-byte Reload
	movq	152(%rsp), %r8          # 8-byte Reload
	movq	160(%rsp), %r9          # 8-byte Reload
	movq	56(%rsp), %r10          # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movl	76(%rsp), %eax          # 4-byte Reload
	movl	%eax, %ebp
	movl	%ebp, %edx
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	104(%rsp), %r8          # 8-byte Reload
	movq	112(%rsp), %r9          # 8-byte Reload
	movq	56(%rsp), %r10          # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	80(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	160(%rsp), %r8          # 8-byte Reload
	movl	44(%rsp), %r9d          # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movl	76(%rsp), %eax          # 4-byte Reload
	movl	%eax, %r9d
	movl	%r9d, %edx
	movq	160(%rsp), %rdi         # 8-byte Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	104(%rsp), %r8          # 8-byte Reload
	movq	112(%rsp), %r9          # 8-byte Reload
	movq	88(%rsp), %r10          # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movl	76(%rsp), %eax          # 4-byte Reload
	addq	$3608, %rsp             # imm = 0xE18
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.Ltmp159:
.Ltmp160:
	.size	main, .Ltmp160-main
.Lfunc_end4:
	.cfi_endproc

	.globl	_dmaImpl3
	.align	16, 0x90
	.type	_dmaImpl3,@function
_dmaImpl3:                              # @_dmaImpl3
	.cfi_startproc
.Lfunc_begin5:
	.loc	2 14 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:14:0
# BB#0:
	pushq	%rbp
.Ltmp168:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Ltmp169:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Ltmp170:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Ltmp171:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Ltmp172:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Ltmp173:
	.cfi_def_cfa_offset 56
	subq	$632, %rsp              # imm = 0x278
.Ltmp174:
	.cfi_def_cfa_offset 688
.Ltmp175:
	.cfi_offset %rbx, -56
.Ltmp176:
	.cfi_offset %r12, -48
.Ltmp177:
	.cfi_offset %r13, -40
.Ltmp178:
	.cfi_offset %r14, -32
.Ltmp179:
	.cfi_offset %r15, -24
.Ltmp180:
	.cfi_offset %rbp, -16
	movabsq	$1, %rax
	leaq	.L.str4125, %rcx
	movabsq	$0, %r8
	leaq	.L.str6127, %r9
	movabsq	$2, %r10
	leaq	.L.str10131, %r11
	movabsq	$3, %rbx
	leaq	.L.str9130, %r14
	movabsq	$15, %r15
	leaq	.L.str2123, %r12
	leaq	.L.str3124, %r13
	leaq	.L.str8129, %rbp
	movq	%rsi, 624(%rsp)         # 8-byte Spill
	movl	$0, %esi
	movl	%esi, 620(%rsp)         # 4-byte Spill
	movl	$1, %esi
	movq	%rax, 608(%rsp)         # 8-byte Spill
	movabsq	$19134, %rax            # imm = 0x4ABE
	movq	%rax, 600(%rsp)         # 8-byte Spill
	movabsq	$64, %rax
	movq	%rax, 592(%rsp)         # 8-byte Spill
	leaq	.L.str7128, %rax
	movq	%rax, 584(%rsp)         # 8-byte Spill
	leaq	.L.str5126, %rax
	movq	%rax, 576(%rsp)         # 8-byte Spill
	movabsq	$46, %rax
	movq	%rax, 568(%rsp)         # 8-byte Spill
	movabsq	$24601, %rax            # imm = 0x6019
	movq	%rax, 560(%rsp)         # 8-byte Spill
	leaq	.L.str17138, %rax
	movq	%rax, 552(%rsp)         # 8-byte Spill
	leaq	.L.str16137, %rax
	.loc	2 14 0 prologue_end     # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:14:0
.Ltmp181:
	movq	%rax, 544(%rsp)         # 8-byte Spill
	movq	560(%rsp), %rax         # 8-byte Reload
	movq	%rdi, 536(%rsp)         # 8-byte Spill
	movq	%rax, %rdi
	movq	592(%rsp), %rax         # 8-byte Reload
	movl	%esi, 532(%rsp)         # 4-byte Spill
	movq	%rax, %rsi
	movq	536(%rsp), %rax         # 8-byte Reload
	movq	%rdx, 520(%rsp)         # 8-byte Spill
	movq	%rax, %rdx
	movq	608(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 512(%rsp)         # 8-byte Spill
	movq	%rax, %rcx
	movq	544(%rsp), %rax         # 8-byte Reload
	movq	%r8, 504(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movq	504(%rsp), %rax         # 8-byte Reload
	movq	%r9, 496(%rsp)          # 8-byte Spill
	movq	%rax, %r9
	movq	496(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	movq	%r13, 488(%rsp)         # 8-byte Spill
	movq	%rbp, 480(%rsp)         # 8-byte Spill
	movq	%r12, 472(%rsp)         # 8-byte Spill
	movq	%r15, 464(%rsp)         # 8-byte Spill
	movq	%r14, 456(%rsp)         # 8-byte Spill
	movq	%rbx, 448(%rsp)         # 8-byte Spill
	movq	%r10, 440(%rsp)         # 8-byte Spill
	movq	%r11, 432(%rsp)         # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	560(%rsp), %rdi         # 8-byte Reload
	movq	592(%rsp), %rsi         # 8-byte Reload
	movq	624(%rsp), %rdx         # 8-byte Reload
	movq	608(%rsp), %rcx         # 8-byte Reload
	movq	552(%rsp), %r8          # 8-byte Reload
	movq	504(%rsp), %r9          # 8-byte Reload
	movq	496(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	560(%rsp), %rdi         # 8-byte Reload
	movq	592(%rsp), %rsi         # 8-byte Reload
	movq	520(%rsp), %rdx         # 8-byte Reload
	movq	608(%rsp), %rcx         # 8-byte Reload
	movq	584(%rsp), %r8          # 8-byte Reload
	movq	504(%rsp), %r9          # 8-byte Reload
	movq	496(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	#DEBUG_VALUE: _dmaImpl3:dst_addr <- [RSP+536]
	#DEBUG_VALUE: _dmaImpl3:src_addr <- [RSP+624]
	#DEBUG_VALUE: _dmaImpl3:size <- [RSP+520]
	.loc	2 15 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:15:0
	movq	464(%rsp), %rdi         # 8-byte Reload
	movq	472(%rsp), %rsi         # 8-byte Reload
	movq	488(%rsp), %rdx         # 8-byte Reload
	movq	512(%rsp), %rcx         # 8-byte Reload
	movq	568(%rsp), %r8          # 8-byte Reload
	movl	620(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	440(%rsp), %rdi         # 8-byte Reload
	movq	592(%rsp), %rsi         # 8-byte Reload
	movq	504(%rsp), %rdx         # 8-byte Reload
	movq	504(%rsp), %rcx         # 8-byte Reload
	movq	576(%rsp), %r8          # 8-byte Reload
	movq	504(%rsp), %r9          # 8-byte Reload
	movq	496(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	608(%rsp), %rdi         # 8-byte Reload
	movq	592(%rsp), %rsi         # 8-byte Reload
	movq	520(%rsp), %rdx         # 8-byte Reload
	movq	608(%rsp), %rcx         # 8-byte Reload
	movq	584(%rsp), %r8          # 8-byte Reload
	movq	504(%rsp), %r9          # 8-byte Reload
	movq	496(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	520(%rsp), %rax         # 8-byte Reload
	cmpq	$0, %rax
	sete	%al
	movb	%al, %cl
	andb	$1, %cl
	movzbl	%cl, %ecx
	movl	%ecx, %edx
	movq	600(%rsp), %rdi         # 8-byte Reload
	movq	608(%rsp), %rsi         # 8-byte Reload
	movq	608(%rsp), %rcx         # 8-byte Reload
	movq	512(%rsp), %r8          # 8-byte Reload
	movq	504(%rsp), %r9          # 8-byte Reload
	movq	496(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	movb	%al, 431(%rsp)          # 1-byte Spill
	callq	trace_logger_log_int
	movq	464(%rsp), %rdi         # 8-byte Reload
	movq	472(%rsp), %rsi         # 8-byte Reload
	movq	488(%rsp), %rdx         # 8-byte Reload
	movq	480(%rsp), %rcx         # 8-byte Reload
	movq	440(%rsp), %r8          # 8-byte Reload
	movl	620(%rsp), %r9d         # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	448(%rsp), %rdi         # 8-byte Reload
	movq	504(%rsp), %rsi         # 8-byte Reload
	movq	504(%rsp), %rdx         # 8-byte Reload
	movq	608(%rsp), %rcx         # 8-byte Reload
	movq	456(%rsp), %r8          # 8-byte Reload
	movq	504(%rsp), %r9          # 8-byte Reload
	movq	496(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	440(%rsp), %rdi         # 8-byte Reload
	movq	504(%rsp), %rsi         # 8-byte Reload
	movq	504(%rsp), %rdx         # 8-byte Reload
	movq	608(%rsp), %rcx         # 8-byte Reload
	movq	432(%rsp), %r8          # 8-byte Reload
	movq	504(%rsp), %r9          # 8-byte Reload
	movq	496(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	431(%rsp), %al          # 1-byte Reload
	andb	$1, %al
	movzbl	%al, %eax
	movl	%eax, %edx
	movq	608(%rsp), %rdi         # 8-byte Reload
	movq	608(%rsp), %rsi         # 8-byte Reload
	movq	608(%rsp), %rcx         # 8-byte Reload
	movq	512(%rsp), %r8          # 8-byte Reload
	movq	504(%rsp), %r9          # 8-byte Reload
	movq	496(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movb	431(%rsp), %al          # 1-byte Reload
	testb	$1, %al
	jne	.LBB5_1
	jmp	.LBB5_2
.Ltmp182:
.LBB5_1:
	movabsq	$4, %rdi
	movabsq	$64, %rax
	leaq	.L__PRETTY_FUNCTION__._dmaImpl3, %rdx
	movabsq	$0, %rcx
	leaq	.L.str5126, %rsi
	leaq	.L.str6127, %r8
	movabsq	$3, %r9
	movabsq	$32, %r10
	movabsq	$15, %r11
	movabsq	$2, %rbx
	leaq	.L.str1122, %r14
	movabsq	$1, %r15
	leaq	.L.str121, %r12
	movabsq	$5, %r13
	leaq	__assert_fail, %rbp
	movq	%rax, 416(%rsp)         # 8-byte Spill
	leaq	.L.str12133, %rax
	movq	%rax, 408(%rsp)         # 8-byte Spill
	leaq	.L.str2123, %rax
	movq	%rax, 400(%rsp)         # 8-byte Spill
	leaq	.L.str9130, %rax
	movq	%rax, 392(%rsp)         # 8-byte Spill
	leaq	.L.str11132, %rax
	movq	%rax, 384(%rsp)         # 8-byte Spill
	movabsq	$49, %rax
	movq	%rsi, 376(%rsp)         # 8-byte Spill
	movl	$0, %esi
	movl	%esi, 372(%rsp)         # 4-byte Spill
	movl	$1, %esi
	movq	%rdi, 360(%rsp)         # 8-byte Spill
	movq	%r11, %rdi
	movq	%rax, 352(%rsp)         # 8-byte Spill
	movq	400(%rsp), %rax         # 8-byte Reload
	movl	%esi, 348(%rsp)         # 4-byte Spill
	movq	%rax, %rsi
	movq	392(%rsp), %rax         # 8-byte Reload
	movq	%rdx, 336(%rsp)         # 8-byte Spill
	movq	%rax, %rdx
	movq	384(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 328(%rsp)         # 8-byte Spill
	movq	%rax, %rcx
	movq	352(%rsp), %rax         # 8-byte Reload
	movq	%r8, 320(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movl	372(%rsp), %eax         # 4-byte Reload
	movq	%r9, 312(%rsp)          # 8-byte Spill
	movl	%eax, %r9d
	movl	$1, (%rsp)
	movq	%rbp, 304(%rsp)         # 8-byte Spill
	movq	%r12, 296(%rsp)         # 8-byte Spill
	movq	%r15, 288(%rsp)         # 8-byte Spill
	movq	%r13, 280(%rsp)         # 8-byte Spill
	movq	%r14, 272(%rsp)         # 8-byte Spill
	movq	%rbx, 264(%rsp)         # 8-byte Spill
	movq	%r10, 256(%rsp)         # 8-byte Spill
	movq	%r11, 248(%rsp)         # 8-byte Spill
	callq	trace_logger_log0
	movq	280(%rsp), %rdi         # 8-byte Reload
	movq	416(%rsp), %rsi         # 8-byte Reload
	movq	304(%rsp), %rdx         # 8-byte Reload
	movq	288(%rsp), %rcx         # 8-byte Reload
	movq	408(%rsp), %r8          # 8-byte Reload
	movq	328(%rsp), %r9          # 8-byte Reload
	movq	320(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	288(%rsp), %rdi         # 8-byte Reload
	movq	416(%rsp), %rsi         # 8-byte Reload
	movq	296(%rsp), %rdx         # 8-byte Reload
	movq	328(%rsp), %rcx         # 8-byte Reload
	movq	376(%rsp), %r8          # 8-byte Reload
	movq	328(%rsp), %r9          # 8-byte Reload
	movq	320(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	264(%rsp), %rdi         # 8-byte Reload
	movq	416(%rsp), %rsi         # 8-byte Reload
	movq	272(%rsp), %rdx         # 8-byte Reload
	movq	328(%rsp), %rcx         # 8-byte Reload
	movq	376(%rsp), %r8          # 8-byte Reload
	movq	328(%rsp), %r9          # 8-byte Reload
	movq	320(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movq	312(%rsp), %rdi         # 8-byte Reload
	movq	256(%rsp), %rsi         # 8-byte Reload
	movq	248(%rsp), %rdx         # 8-byte Reload
	movq	328(%rsp), %rcx         # 8-byte Reload
	movq	376(%rsp), %r8          # 8-byte Reload
	movq	328(%rsp), %r9          # 8-byte Reload
	movq	320(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movq	360(%rsp), %rdi         # 8-byte Reload
	movq	416(%rsp), %rsi         # 8-byte Reload
	movq	336(%rsp), %rdx         # 8-byte Reload
	movq	328(%rsp), %rcx         # 8-byte Reload
	movq	376(%rsp), %r8          # 8-byte Reload
	movq	328(%rsp), %r9          # 8-byte Reload
	movq	320(%rsp), %r10         # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_ptr
	movl	$.L.str121, %eax
	movl	%eax, %edi
	movl	$.L.str1122, %eax
	movl	%eax, %esi
	movl	$.L__PRETTY_FUNCTION__._dmaImpl3, %eax
	movl	%eax, %ecx
	movl	$15, %edx
	callq	__assert_fail
	movabsq	$15, %rdi
	leaq	.L.str2123, %rsi
	leaq	.L.str9130, %rdx
	leaq	.L.str13134, %rcx
	movabsq	$7, %r8
	movl	$0, %r9d
	movl	$1, %eax
	movl	$1, (%rsp)
	movl	%eax, 244(%rsp)         # 4-byte Spill
	callq	trace_logger_log0
.LBB5_2:
	movabsq	$3, %rdi
	movabsq	$64, %rax
	movabsq	$1, %rcx
	leaq	.L.str7128, %r8
	movabsq	$0, %rdx
	leaq	.L.str6127, %rsi
	movabsq	$2, %r9
	leaq	.L.str17138, %r10
	leaq	.L.str16137, %r11
	movabsq	$4, %rbx
	leaq	memmove, %r14
	leaq	.L.str15136, %r15
	movabsq	$16, %r12
	leaq	.L.str2123, %r13
	leaq	.L.str10131, %rbp
	movq	%rax, 232(%rsp)         # 8-byte Spill
	leaq	.L.str14135, %rax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	movabsq	$49, %rax
	movq	%rsi, 216(%rsp)         # 8-byte Spill
	movl	$0, %esi
	movl	%esi, 212(%rsp)         # 4-byte Spill
	movl	$1, %esi
	.loc	2 16 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:16:0
	movq	%rdi, 200(%rsp)         # 8-byte Spill
	movq	%r12, %rdi
	movl	%esi, 196(%rsp)         # 4-byte Spill
	movq	%r13, %rsi
	movq	%rdx, 184(%rsp)         # 8-byte Spill
	movq	%rbp, %rdx
	movq	224(%rsp), %r12         # 8-byte Reload
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	movq	%r12, %rcx
	movq	%r8, 168(%rsp)          # 8-byte Spill
	movq	%rax, %r8
	movl	212(%rsp), %ebp         # 4-byte Reload
	movq	%r9, 160(%rsp)          # 8-byte Spill
	movl	%ebp, %r9d
	movl	$1, (%rsp)
	movq	%r14, 152(%rsp)         # 8-byte Spill
	movq	%r11, 144(%rsp)         # 8-byte Spill
	movq	%r10, 136(%rsp)         # 8-byte Spill
	movq	%rbx, 128(%rsp)         # 8-byte Spill
	movq	%r15, 120(%rsp)         # 8-byte Spill
	callq	trace_logger_log0
	movq	128(%rsp), %rdi         # 8-byte Reload
	movq	232(%rsp), %rsi         # 8-byte Reload
	movq	152(%rsp), %rdx         # 8-byte Reload
	movq	176(%rsp), %rcx         # 8-byte Reload
	movq	120(%rsp), %r8          # 8-byte Reload
	movq	184(%rsp), %r9          # 8-byte Reload
	movq	216(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	176(%rsp), %rdi         # 8-byte Reload
	movq	232(%rsp), %rsi         # 8-byte Reload
	movq	536(%rsp), %rdx         # 8-byte Reload
	movq	176(%rsp), %rcx         # 8-byte Reload
	movq	144(%rsp), %r8          # 8-byte Reload
	movq	184(%rsp), %r9          # 8-byte Reload
	movq	216(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	160(%rsp), %rdi         # 8-byte Reload
	movq	232(%rsp), %rsi         # 8-byte Reload
	movq	624(%rsp), %rdx         # 8-byte Reload
	movq	176(%rsp), %rcx         # 8-byte Reload
	movq	136(%rsp), %r8          # 8-byte Reload
	movq	184(%rsp), %r9          # 8-byte Reload
	movq	216(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	200(%rsp), %rdi         # 8-byte Reload
	movq	232(%rsp), %rsi         # 8-byte Reload
	movq	520(%rsp), %rdx         # 8-byte Reload
	movq	176(%rsp), %rcx         # 8-byte Reload
	movq	168(%rsp), %r8          # 8-byte Reload
	movq	184(%rsp), %r9          # 8-byte Reload
	movq	216(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movq	536(%rsp), %rdi         # 8-byte Reload
	movq	624(%rsp), %rsi         # 8-byte Reload
	movq	520(%rsp), %rdx         # 8-byte Reload
	callq	memmove
	movl	$3, %ebp
	movabsq	$1, %rcx
	movabsq	$32, %rsi
	movabsq	$3, %rdx
	movabsq	$0, %rdi
	leaq	.L.str5126, %r8
	leaq	.L.str6127, %r9
	movabsq	$17, %r10
	leaq	.L.str2123, %r11
	leaq	.L.str10131, %rbx
	leaq	.L.str18139, %r14
	movl	$0, %r15d
	movl	$1, %r12d
	movabsq	$19134, %r13            # imm = 0x4ABE
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movabsq	$64, %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	leaq	.L.str14135, %rax
	.loc	2 17 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:17:0
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	movq	%r13, %rdi
	movq	104(%rsp), %r13         # 8-byte Reload
	movq	%rsi, 88(%rsp)          # 8-byte Spill
	movq	%r13, %rsi
	movq	112(%rsp), %r13         # 8-byte Reload
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movq	%r13, %rdx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	%r8, 64(%rsp)           # 8-byte Spill
	movq	%rax, %r8
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	%r9, 56(%rsp)           # 8-byte Spill
	movq	%rax, %r9
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	movl	%ebp, 44(%rsp)          # 4-byte Spill
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movl	%r15d, 28(%rsp)         # 4-byte Spill
	movl	%r12d, 24(%rsp)         # 4-byte Spill
	movq	%r11, 16(%rsp)          # 8-byte Spill
	movq	%r10, 8(%rsp)           # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	72(%rsp), %r8           # 8-byte Reload
	movl	28(%rsp), %r9d          # 4-byte Reload
	movl	$1, (%rsp)
	callq	trace_logger_log0
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	88(%rsp), %rsi          # 8-byte Reload
	movq	80(%rsp), %rdx          # 8-byte Reload
	movq	96(%rsp), %rcx          # 8-byte Reload
	movq	64(%rsp), %r8           # 8-byte Reload
	movq	96(%rsp), %r9           # 8-byte Reload
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	movl	44(%rsp), %eax          # 4-byte Reload
	addq	$632, %rsp              # imm = 0x278
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.Ltmp183:
.Ltmp184:
	.size	_dmaImpl3, .Ltmp184-_dmaImpl3
.Lfunc_end5:
	.cfi_endproc

	.globl	dmaLoad
	.align	16, 0x90
	.type	dmaLoad,@function
dmaLoad:                                # @dmaLoad
	.cfi_startproc
.Lfunc_begin6:
	.loc	2 20 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:20:0
# BB#0:
	pushq	%r15
.Ltmp189:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Ltmp190:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Ltmp191:
	.cfi_def_cfa_offset 32
	subq	$96, %rsp
.Ltmp192:
	.cfi_def_cfa_offset 128
.Ltmp193:
	.cfi_offset %rbx, -32
.Ltmp194:
	.cfi_offset %r14, -24
.Ltmp195:
	.cfi_offset %r15, -16
	movabsq	$24601, %rax            # imm = 0x6019
	movabsq	$64, %rcx
	movabsq	$1, %r8
	leaq	.L.str7128, %r9
	movabsq	$0, %r10
	leaq	.L.str6127, %r11
	leaq	.L.str19140, %rbx
	leaq	.L.str16137, %r14
	.loc	2 20 0 prologue_end     # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:20:0
.Ltmp196:
	movq	%rdi, 88(%rsp)          # 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movq	%rcx, %rsi
	movq	88(%rsp), %r15          # 8-byte Reload
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movq	%r15, %rdx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%r8, %rcx
	movq	%r8, 56(%rsp)           # 8-byte Spill
	movq	%r14, %r8
	movq	%r9, 48(%rsp)           # 8-byte Spill
	movq	%r10, %r9
	movq	%r11, (%rsp)
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movq	%r10, 32(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%r11, 16(%rsp)          # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	80(%rsp), %rdx          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	72(%rsp), %rdx          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	#DEBUG_VALUE: dmaLoad:dst_addr <- [RSP+88]
	#DEBUG_VALUE: dmaLoad:src_host_addr <- [RSP+80]
	#DEBUG_VALUE: dmaLoad:size <- [RSP+72]
.Ltmp197:
	#DEBUG_VALUE: _dmaImpl3:dst_addr <- [RSP+88]
	#DEBUG_VALUE: _dmaImpl3:src_addr <- [RSP+80]
	#DEBUG_VALUE: _dmaImpl3:size <- [RSP+72]
	.loc	2 15 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:15:0
	movq	72(%rsp), %rax          # 8-byte Reload
	cmpq	$0, %rax
	jne	.LBB6_2
.Ltmp198:
# BB#1:
	movl	$.L.str121, %eax
	movl	%eax, %edi
	movl	$.L.str1122, %eax
	movl	%eax, %esi
	movl	$.L__PRETTY_FUNCTION__._dmaImpl3, %eax
	movl	%eax, %ecx
	movl	$15, %edx
	callq	__assert_fail
.LBB6_2:                                # %_dmaImpl3.exit
	.loc	2 16 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:16:0
	movq	88(%rsp), %rdi          # 8-byte Reload
	movq	80(%rsp), %rsi          # 8-byte Reload
	movq	72(%rsp), %rdx          # 8-byte Reload
	callq	memmove
	movl	$3, %ecx
.Ltmp199:
	.loc	2 21 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:21:0
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	%ecx, %eax
	addq	$96, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	ret
.Ltmp200:
.Ltmp201:
	.size	dmaLoad, .Ltmp201-dmaLoad
.Lfunc_end6:
	.cfi_endproc

	.globl	dmaStore
	.align	16, 0x90
	.type	dmaStore,@function
dmaStore:                               # @dmaStore
	.cfi_startproc
.Lfunc_begin7:
	.loc	2 24 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:24:0
# BB#0:
	pushq	%r15
.Ltmp206:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Ltmp207:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Ltmp208:
	.cfi_def_cfa_offset 32
	subq	$96, %rsp
.Ltmp209:
	.cfi_def_cfa_offset 128
.Ltmp210:
	.cfi_offset %rbx, -32
.Ltmp211:
	.cfi_offset %r14, -24
.Ltmp212:
	.cfi_offset %r15, -16
	movabsq	$24601, %rax            # imm = 0x6019
	movabsq	$64, %rcx
	movabsq	$1, %r8
	leaq	.L.str7128, %r9
	movabsq	$0, %r10
	leaq	.L.str6127, %r11
	leaq	.L.str17138, %rbx
	leaq	.L.str20141, %r14
	.loc	2 24 0 prologue_end     # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:24:0
.Ltmp213:
	movq	%rdi, 88(%rsp)          # 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movq	%rcx, %rsi
	movq	88(%rsp), %r15          # 8-byte Reload
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movq	%r15, %rdx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%r8, %rcx
	movq	%r8, 56(%rsp)           # 8-byte Spill
	movq	%r14, %r8
	movq	%r9, 48(%rsp)           # 8-byte Spill
	movq	%r10, %r9
	movq	%r11, (%rsp)
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movq	%r10, 32(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%r11, 16(%rsp)          # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	80(%rsp), %rdx          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_ptr
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	72(%rsp), %rdx          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	callq	trace_logger_log_int
	#DEBUG_VALUE: dmaStore:dst_host_addr <- [RSP+88]
	#DEBUG_VALUE: dmaStore:src_addr <- [RSP+80]
	#DEBUG_VALUE: dmaStore:size <- [RSP+72]
.Ltmp214:
	#DEBUG_VALUE: _dmaImpl3:dst_addr <- [RSP+88]
	#DEBUG_VALUE: _dmaImpl3:src_addr <- [RSP+80]
	#DEBUG_VALUE: _dmaImpl3:size <- [RSP+72]
	.loc	2 15 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:15:0
	movq	72(%rsp), %rax          # 8-byte Reload
	cmpq	$0, %rax
	jne	.LBB7_2
.Ltmp215:
# BB#1:
	movl	$.L.str121, %eax
	movl	%eax, %edi
	movl	$.L.str1122, %eax
	movl	%eax, %esi
	movl	$.L__PRETTY_FUNCTION__._dmaImpl3, %eax
	movl	%eax, %ecx
	movl	$15, %edx
	callq	__assert_fail
.LBB7_2:                                # %_dmaImpl3.exit
	.loc	2 16 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:16:0
	movq	88(%rsp), %rdi          # 8-byte Reload
	movq	80(%rsp), %rsi          # 8-byte Reload
	movq	72(%rsp), %rdx          # 8-byte Reload
	callq	memmove
	movl	$3, %ecx
.Ltmp216:
	.loc	2 25 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:25:0
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	%ecx, %eax
	addq	$96, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	ret
.Ltmp217:
.Ltmp218:
	.size	dmaStore, .Ltmp218-dmaStore
.Lfunc_end7:
	.cfi_endproc

	.globl	setReadyBits
	.align	16, 0x90
	.type	setReadyBits,@function
setReadyBits:                           # @setReadyBits
	.cfi_startproc
.Lfunc_begin8:
	.loc	2 28 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:28:0
# BB#0:
	pushq	%rbp
.Ltmp226:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Ltmp227:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Ltmp228:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Ltmp229:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Ltmp230:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Ltmp231:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Ltmp232:
	.cfi_def_cfa_offset 160
.Ltmp233:
	.cfi_offset %rbx, -56
.Ltmp234:
	.cfi_offset %r12, -48
.Ltmp235:
	.cfi_offset %r13, -40
.Ltmp236:
	.cfi_offset %r14, -32
.Ltmp237:
	.cfi_offset %r15, -24
.Ltmp238:
	.cfi_offset %rbp, -16
	movl	$0, %eax
	movabsq	$24601, %rcx            # imm = 0x6019
	movabsq	$32, %r8
	movabsq	$1, %r9
	leaq	.L.str22143, %r10
	movabsq	$0, %r11
	leaq	.L.str6127, %rbx
	movabsq	$64, %r14
	leaq	.L.str7128, %r15
	leaq	.L.str21142, %r12
	.loc	2 28 0 prologue_end     # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:28:0
.Ltmp239:
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	movq	%rcx, %rdi
	movq	%rsi, 88(%rsp)          # 8-byte Spill
	movq	%r14, %rsi
	movq	96(%rsp), %r13          # 8-byte Reload
	movl	%edx, 84(%rsp)          # 4-byte Spill
	movq	%r13, %rdx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	%r9, %rcx
	movq	%r8, 64(%rsp)           # 8-byte Spill
	movq	%r12, %r8
	movq	%r9, 56(%rsp)           # 8-byte Spill
	movq	%r11, %r9
	movq	%rbx, (%rsp)
	movq	%r15, 48(%rsp)          # 8-byte Spill
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movl	%eax, 36(%rsp)          # 4-byte Spill
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movq	%r11, 16(%rsp)          # 8-byte Spill
	movq	%r10, 8(%rsp)           # 8-byte Spill
	callq	trace_logger_log_ptr
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	88(%rsp), %rdx          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	movq	40(%rsp), %r10          # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	movl	84(%rsp), %eax          # 4-byte Reload
	movl	%eax, %ebp
	movl	%ebp, %edx
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	movq	40(%rsp), %r10          # 8-byte Reload
	movq	%r10, (%rsp)
	callq	trace_logger_log_int
	#DEBUG_VALUE: setReadyBits:start_addr <- [RSP+96]
	#DEBUG_VALUE: setReadyBits:size <- [RSP+88]
	#DEBUG_VALUE: setReadyBits:value <- [RSP+84]
	.loc	2 29 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:29:0
	#APP
	#NO_APP
	.loc	2 30 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:30:0
	movl	36(%rsp), %eax          # 4-byte Reload
	addq	$104, %rsp
.Ltmp240:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.Ltmp241:
.Ltmp242:
	.size	setReadyBits, .Ltmp242-setReadyBits
.Lfunc_end8:
	.cfi_endproc

	.globl	dmaFence
	.align	16, 0x90
	.type	dmaFence,@function
dmaFence:                               # @dmaFence
	.cfi_startproc
.Lfunc_begin9:
	.loc	2 71 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:71:0
# BB#0:
	.loc	2 72 0 prologue_end     # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:72:0
	#APP
	#NO_APP
	.loc	2 73 0                  # /workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c:73:0
	ret
.Ltmp243:
.Ltmp244:
	.size	dmaFence, .Ltmp244-dmaFence
.Lfunc_end9:
	.cfi_endproc

	.globl	trace_logger_write_labelmap
	.align	16, 0x90
	.type	trace_logger_write_labelmap,@function
trace_logger_write_labelmap:            # @trace_logger_write_labelmap
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Ltmp246:
	.cfi_def_cfa_offset 32
	cmpb	$0, initp
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	jne	.LBB10_2
# BB#1:
	callq	trace_logger_init
.LBB10_2:
	movq	full_trace_file(%rip), %rdi
	movl	$.L.str148, %eax
	movl	%eax, %esi
	movl	$26, %edx
	callq	gzwrite
	movq	full_trace_file(%rip), %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%esi, %edx
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	%eax, 4(%rsp)           # 4-byte Spill
	callq	gzwrite
	movq	full_trace_file(%rip), %rdi
	movl	$.L.str1149, %edx
	movl	%edx, %esi
	movl	$25, %edx
	movl	%eax, (%rsp)            # 4-byte Spill
	addq	$24, %rsp
	jmp	gzwrite                 # TAILCALL
.Ltmp247:
	.size	trace_logger_write_labelmap, .Ltmp247-trace_logger_write_labelmap
	.cfi_endproc

	.globl	trace_logger_init
	.align	16, 0x90
	.type	trace_logger_init,@function
trace_logger_init:                      # @trace_logger_init
	.cfi_startproc
# BB#0:
	pushq	%rax
.Ltmp249:
	.cfi_def_cfa_offset 16
	movl	$.L.str2150, %eax
	movl	%eax, %edi
	movl	$.L.str3151, %eax
	movl	%eax, %esi
	callq	gzopen
	movq	%rax, full_trace_file
	cmpq	$0, %rax
	jne	.LBB11_2
# BB#1:
	movl	$.L.str4152, %eax
	movl	%eax, %edi
	callq	perror
	movl	$-1, %edi
	callq	exit
.LBB11_2:
	movl	$512, %eax              # imm = 0x200
	movl	%eax, %edi
	movl	$1, %eax
	movl	%eax, %esi
	callq	calloc
	movq	%rax, current_toplevel_function
	movl	$2, current_logging_status
	movl	$trace_logger_fin, %ecx
	movl	%ecx, %edi
	callq	atexit
	movb	$1, initp
	movl	%eax, 4(%rsp)           # 4-byte Spill
	popq	%rax
	ret
.Ltmp250:
	.size	trace_logger_init, .Ltmp250-trace_logger_init
	.cfi_endproc

	.globl	trace_logger_fin
	.align	16, 0x90
	.type	trace_logger_fin,@function
trace_logger_fin:                       # @trace_logger_fin
	.cfi_startproc
# BB#0:
	pushq	%rax
.Ltmp252:
	.cfi_def_cfa_offset 16
	movq	current_toplevel_function(%rip), %rdi
	callq	free
	movq	full_trace_file(%rip), %rdi
	popq	%rax
	jmp	gzclose                 # TAILCALL
.Ltmp253:
	.size	trace_logger_fin, .Ltmp253-trace_logger_fin
	.cfi_endproc

	.globl	log_or_not
	.align	16, 0x90
	.type	log_or_not,@function
log_or_not:                             # @log_or_not
	.cfi_startproc
# BB#0:
	subq	$40, %rsp
.Ltmp255:
	.cfi_def_cfa_offset 48
	movb	%sil, %al
	movb	%dil, %r8b
	testb	$1, %r8b
	movl	%edx, 36(%rsp)          # 4-byte Spill
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movb	%al, 23(%rsp)           # 1-byte Spill
	jne	.LBB13_2
	jmp	.LBB13_1
.LBB13_1:
	movl	$1, %eax
	movl	$2, %ecx
	movb	23(%rsp), %dl           # 1-byte Reload
	testb	%dl, %dl
	cmovel	%ecx, %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	jmp	.LBB13_9
.LBB13_2:
	movb	23(%rsp), %al           # 1-byte Reload
	testb	$1, %al
	jne	.LBB13_4
	jmp	.LBB13_3
.LBB13_3:
	movl	current_logging_status, %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	jmp	.LBB13_9
.LBB13_4:
	movl	$1, %eax
	movl	36(%rsp), %ecx          # 4-byte Reload
	cmpl	$1, %ecx
	movl	%eax, 16(%rsp)          # 4-byte Spill
	jne	.LBB13_9
# BB#5:
	movq	current_toplevel_function, %rax
	cmpb	$0, (%rax)
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jne	.LBB13_7
# BB#6:
	movl	$.L.str5153, %eax
	movl	%eax, %edi
	movl	$.L.str6154, %eax
	movl	%eax, %esi
	movl	$.L__PRETTY_FUNCTION__.log_or_not, %eax
	movl	%eax, %ecx
	movl	$118, %edx
	callq	__assert_fail
.LBB13_7:
	movl	$0, %eax
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	%eax, 4(%rsp)           # 4-byte Spill
	callq	strcmp
	cmpl	$0, %eax
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	%eax, 16(%rsp)          # 4-byte Spill
	je	.LBB13_9
# BB#8:
	movl	$.L.str7155, %eax
	movl	%eax, %edi
	movl	$.L.str6154, %eax
	movl	%eax, %esi
	movl	$.L__PRETTY_FUNCTION__.log_or_not, %eax
	movl	%eax, %ecx
	movl	$123, %edx
	callq	__assert_fail
.LBB13_9:
	movl	16(%rsp), %eax          # 4-byte Reload
	addq	$40, %rsp
	ret
.Ltmp256:
	.size	log_or_not, .Ltmp256-log_or_not
	.cfi_endproc

	.globl	convert_bytes_to_hex
	.align	16, 0x90
	.type	convert_bytes_to_hex,@function
convert_bytes_to_hex:                   # @convert_bytes_to_hex
	.cfi_startproc
# BB#0:
	subq	$56, %rsp
.Ltmp258:
	.cfi_def_cfa_offset 64
	movabsq	$0, %rax
	movw	.L.str8156, %cx
	movw	%cx, (%rdi)
	movb	.L.str8156+2, %r8b
	movb	%r8b, 2(%rdi)
	addq	$2, %rdi
	cmpl	$0, %edx
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	%edx, 36(%rsp)          # 4-byte Spill
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	jle	.LBB14_2
.LBB14_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	movzbl	(%rdx,%rcx), %edx
	movl	$.L.str9157, %esi
                                        # kill: RSI<def> ESI<kill>
	xorl	%edi, %edi
	movb	%dil, %r8b
	movq	%rax, %rdi
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movb	%r8b, %al
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	callq	sprintf
	movslq	%eax, %rcx
	movq	16(%rsp), %rsi          # 8-byte Reload
	addq	%rcx, %rsi
	movq	8(%rsp), %rcx           # 8-byte Reload
	addq	$1, %rcx
	movl	%ecx, %eax
	movl	36(%rsp), %edx          # 4-byte Reload
	cmpl	%edx, %eax
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	jne	.LBB14_1
.LBB14_2:                               # %._crit_edge
	movq	24(%rsp), %rax          # 8-byte Reload
	movb	$0, (%rax)
	addq	$56, %rsp
	ret
.Ltmp259:
	.size	convert_bytes_to_hex, .Ltmp259-convert_bytes_to_hex
	.cfi_endproc

	.globl	update_logging_status
	.align	16, 0x90
	.type	update_logging_status,@function
update_logging_status:                  # @update_logging_status
	.cfi_startproc
# BB#0:
	subq	$56, %rsp
.Ltmp261:
	.cfi_def_cfa_offset 64
	movb	%cl, %al
	movb	%dl, %r8b
	movl	current_logging_status, %ecx
	cmpl	$0, %ecx
	movl	%esi, 52(%rsp)          # 4-byte Spill
	movb	%al, 51(%rsp)           # 1-byte Spill
	movb	%r8b, 50(%rsp)          # 1-byte Spill
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	movl	%ecx, 36(%rsp)          # 4-byte Spill
	jne	.LBB15_2
# BB#1:
	movl	inst_count, %esi
	movl	$.L.str10158, %eax
	movl	%eax, %edi
	xorl	%eax, %eax
	movb	%al, %cl
	movb	%cl, %al
	callq	printf
	movl	$2, current_logging_status
	movl	%eax, 32(%rsp)          # 4-byte Spill
	jmp	.LBB15_8
.LBB15_2:
	movb	51(%rsp), %al           # 1-byte Reload
	movzbl	%al, %ecx
	andl	$1, %ecx
	movb	50(%rsp), %dl           # 1-byte Reload
	movzbl	%dl, %esi
	andl	$1, %esi
	movl	%ecx, %edi
	movl	52(%rsp), %edx          # 4-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	callq	log_or_not
	movl	%eax, current_logging_status
	movl	36(%rsp), %edx          # 4-byte Reload
	cmpl	$2, %edx
	setne	%r8b
	cmpl	%edx, %eax
	sete	%r9b
	orb	%r9b, %r8b
	testb	$1, %r8b
	movl	%eax, 28(%rsp)          # 4-byte Spill
	jne	.LBB15_4
# BB#3:
	movl	inst_count, %esi
	movl	$.L.str11159, %eax
	movl	%eax, %edi
	xorl	%eax, %eax
	movb	%al, %cl
	movb	%cl, %al
	callq	printf
	movl	current_logging_status, %esi
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movl	%esi, 28(%rsp)          # 4-byte Spill
.LBB15_4:
	movl	28(%rsp), %eax          # 4-byte Reload
	movq	current_toplevel_function, %rcx
	cmpb	$0, (%rcx)
	sete	%dl
	cmpl	$1, %eax
	sete	%sil
	andb	%sil, %dl
	testb	$1, %dl
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	jne	.LBB15_5
	jmp	.LBB15_6
.LBB15_5:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
	callq	strcpy
	movq	%rax, (%rsp)            # 8-byte Spill
	jmp	.LBB15_8
.LBB15_6:
	movl	20(%rsp), %eax          # 4-byte Reload
	cmpl	$0, %eax
	jne	.LBB15_8
# BB#7:
	movl	$0, %esi
	movabsq	$512, %rdx              # imm = 0x200
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	memset
.LBB15_8:
	addq	$56, %rsp
	ret
.Ltmp262:
	.size	update_logging_status, .Ltmp262-update_logging_status
	.cfi_endproc

	.globl	do_not_log
	.align	16, 0x90
	.type	do_not_log,@function
do_not_log:                             # @do_not_log
	.cfi_startproc
# BB#0:
	cmpl	$2, current_logging_status
	sete	%al
	andb	$1, %al
	movzbl	%al, %eax
	ret
.Ltmp263:
	.size	do_not_log, .Ltmp263-do_not_log
	.cfi_endproc

	.globl	trace_logger_log_entry
	.align	16, 0x90
	.type	trace_logger_log_entry,@function
trace_logger_log_entry:                 # @trace_logger_log_entry
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Ltmp265:
	.cfi_def_cfa_offset 32
	cmpb	$0, initp
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movl	%esi, 12(%rsp)          # 4-byte Spill
	jne	.LBB17_2
# BB#1:
	callq	trace_logger_init
.LBB17_2:
	xorl	%esi, %esi
	movl	$1, %eax
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	%eax, %edx
	movl	%eax, %ecx
	callq	update_logging_status
	cmpl	$2, current_logging_status
	je	.LBB17_4
# BB#3:
	movq	full_trace_file, %rdi
	movl	$.L.str12160, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	12(%rsp), %eax          # 4-byte Reload
	movb	%cl, 11(%rsp)           # 1-byte Spill
	movl	%eax, %ecx
	movb	11(%rsp), %al           # 1-byte Reload
	callq	gzprintf
	movl	%eax, 4(%rsp)           # 4-byte Spill
.LBB17_4:
	addq	$24, %rsp
	ret
.Ltmp266:
	.size	trace_logger_log_entry, .Ltmp266-trace_logger_log_entry
	.cfi_endproc

	.globl	trace_logger_log0
	.align	16, 0x90
	.type	trace_logger_log0,@function
trace_logger_log0:                      # @trace_logger_log0
	.cfi_startproc
# BB#0:
	subq	$72, %rsp
.Ltmp268:
	.cfi_def_cfa_offset 80
	movb	%r9b, %al
	movb	80(%rsp), %r10b
	cmpb	$0, initp
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	movb	%r10b, 63(%rsp)         # 1-byte Spill
	movl	%r8d, 56(%rsp)          # 4-byte Spill
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movb	%al, 39(%rsp)           # 1-byte Spill
	movl	%edi, 32(%rsp)          # 4-byte Spill
	jne	.LBB18_2
# BB#1:
	callq	trace_logger_init
.LBB18_2:
	movb	39(%rsp), %al           # 1-byte Reload
	movzbl	%al, %ecx
	andl	$1, %ecx
	movb	63(%rsp), %dl           # 1-byte Reload
	movzbl	%dl, %esi
	andl	$1, %esi
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	56(%rsp), %r8d          # 4-byte Reload
	movl	%esi, 28(%rsp)          # 4-byte Spill
	movl	%r8d, %esi
	movl	%ecx, %edx
	movl	28(%rsp), %ecx          # 4-byte Reload
	callq	update_logging_status
	cmpl	$2, current_logging_status
	je	.LBB18_4
# BB#3:
	movq	full_trace_file, %rdi
	movl	inst_count, %eax
	movq	%rsp, %rcx
	movl	%eax, 8(%rcx)
	movl	56(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%rcx)
	movl	$.L.str13161, %edx
	movl	%edx, %esi
	xorl	%edx, %edx
	movb	%dl, %r8b
	movl	32(%rsp), %edx          # 4-byte Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	40(%rsp), %r9           # 8-byte Reload
	movb	%r8b, 27(%rsp)          # 1-byte Spill
	movq	%r9, %r8
	movq	48(%rsp), %r9           # 8-byte Reload
	movb	27(%rsp), %al           # 1-byte Reload
	callq	gzprintf
	movl	inst_count, %edx
	addl	$1, %edx
	movl	%edx, inst_count
	movl	%eax, 20(%rsp)          # 4-byte Spill
.LBB18_4:
	addq	$72, %rsp
	ret
.Ltmp269:
	.size	trace_logger_log0, .Ltmp269-trace_logger_log0
	.cfi_endproc

	.globl	trace_logger_log_int
	.align	16, 0x90
	.type	trace_logger_log_int,@function
trace_logger_log_int:                   # @trace_logger_log_int
	.cfi_startproc
# BB#0:
	subq	$120, %rsp
.Ltmp271:
	.cfi_def_cfa_offset 128
	movq	128(%rsp), %rax
	cmpb	$0, initp
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movl	%r9d, 108(%rsp)         # 4-byte Spill
	movq	%r8, 96(%rsp)           # 8-byte Spill
	movl	%ecx, 92(%rsp)          # 4-byte Spill
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movl	%esi, 76(%rsp)          # 4-byte Spill
	movl	%edi, 72(%rsp)          # 4-byte Spill
	jne	.LBB19_2
# BB#1:
	movl	$.L.str14162, %eax
	movl	%eax, %edi
	movl	$.L.str6154, %eax
	movl	%eax, %esi
	movl	$.L__PRETTY_FUNCTION__.trace_logger_log_int, %eax
	movl	%eax, %ecx
	movl	$204, %edx
	callq	__assert_fail
.LBB19_2:
	callq	do_not_log
	testb	$1, %al
	jne	.LBB19_14
# BB#3:
	movl	72(%rsp), %eax          # 4-byte Reload
	cmpl	$19134, %eax            # imm = 0x4ABE
	jne	.LBB19_5
# BB#4:
	movq	full_trace_file, %rdi
	movl	$.L.str15163, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movl	76(%rsp), %edx          # 4-byte Reload
	movq	80(%rsp), %r8           # 8-byte Reload
	movb	%cl, 71(%rsp)           # 1-byte Spill
	movq	%r8, %rcx
	movl	92(%rsp), %r8d          # 4-byte Reload
	movb	71(%rsp), %al           # 1-byte Reload
	callq	gzprintf
	movl	%eax, 64(%rsp)          # 4-byte Spill
	jmp	.LBB19_8
.LBB19_5:
	movq	full_trace_file, %rax
	movl	72(%rsp), %ecx          # 4-byte Reload
	cmpl	$24601, %ecx            # imm = 0x6019
	movq	%rax, 56(%rsp)          # 8-byte Spill
	jne	.LBB19_7
# BB#6:
	movl	$.L.str16164, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	56(%rsp), %rdi          # 8-byte Reload
	movl	76(%rsp), %edx          # 4-byte Reload
	movq	80(%rsp), %r8           # 8-byte Reload
	movb	%cl, 55(%rsp)           # 1-byte Spill
	movq	%r8, %rcx
	movl	92(%rsp), %r8d          # 4-byte Reload
	movb	55(%rsp), %al           # 1-byte Reload
	callq	gzprintf
	movl	%eax, 48(%rsp)          # 4-byte Spill
	jmp	.LBB19_8
.LBB19_7:
	movl	$.L.str17165, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	56(%rsp), %rdi          # 8-byte Reload
	movl	72(%rsp), %edx          # 4-byte Reload
	movl	76(%rsp), %eax          # 4-byte Reload
	movb	%cl, 47(%rsp)           # 1-byte Spill
	movl	%eax, %ecx
	movq	80(%rsp), %r8           # 8-byte Reload
	movl	92(%rsp), %r9d          # 4-byte Reload
	movb	47(%rsp), %al           # 1-byte Reload
	callq	gzprintf
	movl	%eax, 40(%rsp)          # 4-byte Spill
.LBB19_8:
	movq	full_trace_file, %rax
	movl	92(%rsp), %ecx          # 4-byte Reload
	cmpl	$0, %ecx
	movq	%rax, 32(%rsp)          # 8-byte Spill
	je	.LBB19_10
# BB#9:
	movl	$.L.str18166, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	96(%rsp), %rdx          # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, 28(%rsp)          # 4-byte Spill
	jmp	.LBB19_11
.LBB19_10:
	movl	$.L.str19167, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	32(%rsp), %rdi          # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, 24(%rsp)          # 4-byte Spill
.LBB19_11:
	movq	full_trace_file, %rax
	movl	108(%rsp), %ecx         # 4-byte Reload
	cmpl	$0, %ecx
	movq	%rax, 16(%rsp)          # 8-byte Spill
	je	.LBB19_13
# BB#12:
	movl	$.L.str20168, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	112(%rsp), %rdx         # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, 12(%rsp)          # 4-byte Spill
	jmp	.LBB19_14
.LBB19_13:
	movl	$.L.str21169, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	16(%rsp), %rdi          # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, 8(%rsp)           # 4-byte Spill
.LBB19_14:
	addq	$120, %rsp
	ret
.Ltmp272:
	.size	trace_logger_log_int, .Ltmp272-trace_logger_log_int
	.cfi_endproc

	.globl	trace_logger_log_ptr
	.align	16, 0x90
	.type	trace_logger_log_ptr,@function
trace_logger_log_ptr:                   # @trace_logger_log_ptr
	.cfi_startproc
# BB#0:
	subq	$120, %rsp
.Ltmp274:
	.cfi_def_cfa_offset 128
	movq	128(%rsp), %rax
	cmpb	$0, initp
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movl	%r9d, 108(%rsp)         # 4-byte Spill
	movq	%r8, 96(%rsp)           # 8-byte Spill
	movl	%ecx, 92(%rsp)          # 4-byte Spill
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movl	%esi, 76(%rsp)          # 4-byte Spill
	movl	%edi, 72(%rsp)          # 4-byte Spill
	jne	.LBB20_2
# BB#1:
	movl	$.L.str14162, %eax
	movl	%eax, %edi
	movl	$.L.str6154, %eax
	movl	%eax, %esi
	movl	$.L__PRETTY_FUNCTION__.trace_logger_log_ptr, %eax
	movl	%eax, %ecx
	movl	$225, %edx
	callq	__assert_fail
.LBB20_2:
	callq	do_not_log
	testb	$1, %al
	jne	.LBB20_14
# BB#3:
	movl	72(%rsp), %eax          # 4-byte Reload
	cmpl	$19134, %eax            # imm = 0x4ABE
	jne	.LBB20_5
# BB#4:
	movq	full_trace_file, %rdi
	movl	$.L.str22170, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movl	76(%rsp), %edx          # 4-byte Reload
	movq	80(%rsp), %r8           # 8-byte Reload
	movb	%cl, 71(%rsp)           # 1-byte Spill
	movq	%r8, %rcx
	movl	92(%rsp), %r8d          # 4-byte Reload
	movb	71(%rsp), %al           # 1-byte Reload
	callq	gzprintf
	movl	%eax, 64(%rsp)          # 4-byte Spill
	jmp	.LBB20_8
.LBB20_5:
	movq	full_trace_file, %rax
	movl	72(%rsp), %ecx          # 4-byte Reload
	cmpl	$24601, %ecx            # imm = 0x6019
	movq	%rax, 56(%rsp)          # 8-byte Spill
	jne	.LBB20_7
# BB#6:
	movl	$.L.str23171, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	56(%rsp), %rdi          # 8-byte Reload
	movl	76(%rsp), %edx          # 4-byte Reload
	movq	80(%rsp), %r8           # 8-byte Reload
	movb	%cl, 55(%rsp)           # 1-byte Spill
	movq	%r8, %rcx
	movl	92(%rsp), %r8d          # 4-byte Reload
	movb	55(%rsp), %al           # 1-byte Reload
	callq	gzprintf
	movl	%eax, 48(%rsp)          # 4-byte Spill
	jmp	.LBB20_8
.LBB20_7:
	movl	$.L.str24172, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	56(%rsp), %rdi          # 8-byte Reload
	movl	72(%rsp), %edx          # 4-byte Reload
	movl	76(%rsp), %eax          # 4-byte Reload
	movb	%cl, 47(%rsp)           # 1-byte Spill
	movl	%eax, %ecx
	movq	80(%rsp), %r8           # 8-byte Reload
	movl	92(%rsp), %r9d          # 4-byte Reload
	movb	47(%rsp), %al           # 1-byte Reload
	callq	gzprintf
	movl	%eax, 40(%rsp)          # 4-byte Spill
.LBB20_8:
	movq	full_trace_file, %rax
	movl	92(%rsp), %ecx          # 4-byte Reload
	cmpl	$0, %ecx
	movq	%rax, 32(%rsp)          # 8-byte Spill
	je	.LBB20_10
# BB#9:
	movl	$.L.str18166, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	96(%rsp), %rdx          # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, 28(%rsp)          # 4-byte Spill
	jmp	.LBB20_11
.LBB20_10:
	movl	$.L.str19167, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	32(%rsp), %rdi          # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, 24(%rsp)          # 4-byte Spill
.LBB20_11:
	movq	full_trace_file, %rax
	movl	108(%rsp), %ecx         # 4-byte Reload
	cmpl	$0, %ecx
	movq	%rax, 16(%rsp)          # 8-byte Spill
	je	.LBB20_13
# BB#12:
	movl	$.L.str20168, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	112(%rsp), %rdx         # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, 12(%rsp)          # 4-byte Spill
	jmp	.LBB20_14
.LBB20_13:
	movl	$.L.str21169, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	16(%rsp), %rdi          # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, 8(%rsp)           # 4-byte Spill
.LBB20_14:
	addq	$120, %rsp
	ret
.Ltmp275:
	.size	trace_logger_log_ptr, .Ltmp275-trace_logger_log_ptr
	.cfi_endproc

	.globl	trace_logger_log_double
	.align	16, 0x90
	.type	trace_logger_log_double,@function
trace_logger_log_double:                # @trace_logger_log_double
	.cfi_startproc
# BB#0:
	subq	$104, %rsp
.Ltmp277:
	.cfi_def_cfa_offset 112
	cmpb	$0, initp
	movl	%esi, 100(%rsp)         # 4-byte Spill
	movq	%r9, 88(%rsp)           # 8-byte Spill
	movl	%r8d, 84(%rsp)          # 4-byte Spill
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movl	%edx, 68(%rsp)          # 4-byte Spill
	vmovsd	%xmm0, 56(%rsp)         # 8-byte Spill
	movl	%edi, 52(%rsp)          # 4-byte Spill
	jne	.LBB21_2
# BB#1:
	movl	$.L.str14162, %eax
	movl	%eax, %edi
	movl	$.L.str6154, %eax
	movl	%eax, %esi
	movl	$.L__PRETTY_FUNCTION__.trace_logger_log_double, %eax
	movl	%eax, %ecx
	movl	$246, %edx
	callq	__assert_fail
.LBB21_2:
	callq	do_not_log
	testb	$1, %al
	jne	.LBB21_14
# BB#3:
	movl	52(%rsp), %eax          # 4-byte Reload
	cmpl	$19134, %eax            # imm = 0x4ABE
	jne	.LBB21_5
# BB#4:
	movq	full_trace_file, %rdi
	movl	$.L.str25173, %eax
	movl	%eax, %esi
	movb	$1, %al
	movl	100(%rsp), %edx         # 4-byte Reload
	vmovsd	56(%rsp), %xmm0         # 8-byte Reload
	movl	68(%rsp), %ecx          # 4-byte Reload
	callq	gzprintf
	movl	%eax, 48(%rsp)          # 4-byte Spill
	jmp	.LBB21_8
.LBB21_5:
	movq	full_trace_file, %rax
	movl	52(%rsp), %ecx          # 4-byte Reload
	cmpl	$24601, %ecx            # imm = 0x6019
	movq	%rax, 40(%rsp)          # 8-byte Spill
	jne	.LBB21_7
# BB#6:
	movl	$.L.str26174, %eax
	movl	%eax, %esi
	movb	$1, %al
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	100(%rsp), %edx         # 4-byte Reload
	vmovsd	56(%rsp), %xmm0         # 8-byte Reload
	movl	68(%rsp), %ecx          # 4-byte Reload
	callq	gzprintf
	movl	%eax, 36(%rsp)          # 4-byte Spill
	jmp	.LBB21_8
.LBB21_7:
	movl	$.L.str27175, %eax
	movl	%eax, %esi
	movb	$1, %al
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	52(%rsp), %edx          # 4-byte Reload
	movl	100(%rsp), %ecx         # 4-byte Reload
	vmovsd	56(%rsp), %xmm0         # 8-byte Reload
	movl	68(%rsp), %r8d          # 4-byte Reload
	callq	gzprintf
	movl	%eax, 32(%rsp)          # 4-byte Spill
.LBB21_8:
	movq	full_trace_file, %rax
	movl	68(%rsp), %ecx          # 4-byte Reload
	cmpl	$0, %ecx
	movq	%rax, 24(%rsp)          # 8-byte Spill
	je	.LBB21_10
# BB#9:
	movl	$.L.str18166, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	72(%rsp), %rdx          # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, 20(%rsp)          # 4-byte Spill
	jmp	.LBB21_11
.LBB21_10:
	movl	$.L.str19167, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	24(%rsp), %rdi          # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, 16(%rsp)          # 4-byte Spill
.LBB21_11:
	movq	full_trace_file, %rax
	movl	84(%rsp), %ecx          # 4-byte Reload
	cmpl	$0, %ecx
	movq	%rax, 8(%rsp)           # 8-byte Spill
	je	.LBB21_13
# BB#12:
	movl	$.L.str20168, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	88(%rsp), %rdx          # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, 4(%rsp)           # 4-byte Spill
	jmp	.LBB21_14
.LBB21_13:
	movl	$.L.str21169, %eax
	movl	%eax, %esi
	xorl	%eax, %eax
	movb	%al, %cl
	movq	8(%rsp), %rdi           # 8-byte Reload
	movb	%cl, %al
	callq	gzprintf
	movl	%eax, (%rsp)            # 4-byte Spill
.LBB21_14:
	addq	$104, %rsp
	ret
.Ltmp278:
	.size	trace_logger_log_double, .Ltmp278-trace_logger_log_double
	.cfi_endproc

	.globl	trace_logger_log_vector
	.align	16, 0x90
	.type	trace_logger_log_vector,@function
trace_logger_log_vector:                # @trace_logger_log_vector
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Ltmp281:
	.cfi_def_cfa_offset 16
.Ltmp282:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
.Ltmp283:
	.cfi_def_cfa_register %rbp
	subq	$128, %rsp
	movq	16(%rbp), %rax
	cmpb	$0, initp
	movq	%rax, -8(%rbp)          # 8-byte Spill
	movl	%r9d, -12(%rbp)         # 4-byte Spill
	movq	%r8, -24(%rbp)          # 8-byte Spill
	movl	%ecx, -28(%rbp)         # 4-byte Spill
	movq	%rdx, -40(%rbp)         # 8-byte Spill
	movl	%esi, -44(%rbp)         # 4-byte Spill
	movl	%edi, -48(%rbp)         # 4-byte Spill
	jne	.LBB22_2
# BB#1:
	leaq	.L.str14162, %rdi
	leaq	.L.str6154, %rsi
	movl	$267, %edx              # imm = 0x10B
	leaq	.L__PRETTY_FUNCTION__.trace_logger_log_vector, %rcx
	callq	__assert_fail
.LBB22_2:
	callq	do_not_log
	testb	$1, %al
	jne	.LBB22_15
# BB#3:
	movl	$8, %eax
	movl	-44(%rbp), %ecx         # 4-byte Reload
	sarl	$31, %ecx
	shrl	$30, %ecx
	movl	-44(%rbp), %edx         # 4-byte Reload
	addl	%ecx, %edx
	sarl	$2, %edx
	addl	$3, %edx
	movl	%edx, %esi
	movq	%rsp, %rdi
	addq	$15, %rsi
	andq	$-16, %rsi
	movq	%rsp, %r8
	subq	%rsi, %r8
	movq	%r8, %rsp
	movl	-44(%rbp), %ecx         # 4-byte Reload
	movl	%eax, -52(%rbp)         # 4-byte Spill
	movl	%ecx, %eax
	cltd
	movl	-52(%rbp), %r9d         # 4-byte Reload
	idivl	%r9d
	movq	%rdi, -64(%rbp)         # 8-byte Spill
	movq	%r8, %rdi
	movq	-40(%rbp), %rsi         # 8-byte Reload
	movl	%eax, %edx
	movq	%r8, -72(%rbp)          # 8-byte Spill
	callq	convert_bytes_to_hex
	movl	-48(%rbp), %eax         # 4-byte Reload
	cmpl	$19134, %eax            # imm = 0x4ABE
	jne	.LBB22_5
# BB#4:
	leaq	.L.str28176, %rsi
	movq	full_trace_file, %rdi
	movl	-44(%rbp), %edx         # 4-byte Reload
	movq	-72(%rbp), %rcx         # 8-byte Reload
	movl	-28(%rbp), %r8d         # 4-byte Reload
	movb	$0, %al
	callq	gzprintf
	movl	%eax, -76(%rbp)         # 4-byte Spill
	jmp	.LBB22_8
.LBB22_5:
	movq	full_trace_file, %rax
	movl	-48(%rbp), %ecx         # 4-byte Reload
	cmpl	$24601, %ecx            # imm = 0x6019
	movq	%rax, -88(%rbp)         # 8-byte Spill
	jne	.LBB22_7
# BB#6:
	leaq	.L.str29177, %rsi
	movq	-88(%rbp), %rdi         # 8-byte Reload
	movl	-44(%rbp), %edx         # 4-byte Reload
	movq	-72(%rbp), %rcx         # 8-byte Reload
	movl	-28(%rbp), %r8d         # 4-byte Reload
	movb	$0, %al
	callq	gzprintf
	movl	%eax, -92(%rbp)         # 4-byte Spill
	jmp	.LBB22_8
.LBB22_7:
	leaq	.L.str30178, %rsi
	movq	-88(%rbp), %rdi         # 8-byte Reload
	movl	-48(%rbp), %edx         # 4-byte Reload
	movl	-44(%rbp), %ecx         # 4-byte Reload
	movq	-72(%rbp), %r8          # 8-byte Reload
	movl	-28(%rbp), %r9d         # 4-byte Reload
	movb	$0, %al
	callq	gzprintf
	movl	%eax, -96(%rbp)         # 4-byte Spill
.LBB22_8:
	movq	full_trace_file, %rax
	movl	-28(%rbp), %ecx         # 4-byte Reload
	cmpl	$0, %ecx
	movq	%rax, -104(%rbp)        # 8-byte Spill
	je	.LBB22_10
# BB#9:
	leaq	.L.str18166, %rsi
	movq	-104(%rbp), %rdi        # 8-byte Reload
	movq	-24(%rbp), %rdx         # 8-byte Reload
	movb	$0, %al
	callq	gzprintf
	movl	%eax, -108(%rbp)        # 4-byte Spill
	jmp	.LBB22_11
.LBB22_10:
	leaq	.L.str19167, %rsi
	movq	-104(%rbp), %rdi        # 8-byte Reload
	movb	$0, %al
	callq	gzprintf
	movl	%eax, -112(%rbp)        # 4-byte Spill
.LBB22_11:
	movq	full_trace_file, %rax
	movl	-12(%rbp), %ecx         # 4-byte Reload
	cmpl	$0, %ecx
	movq	%rax, -120(%rbp)        # 8-byte Spill
	je	.LBB22_13
# BB#12:
	leaq	.L.str20168, %rsi
	movq	-120(%rbp), %rdi        # 8-byte Reload
	movq	-8(%rbp), %rdx          # 8-byte Reload
	movb	$0, %al
	callq	gzprintf
	movl	%eax, -124(%rbp)        # 4-byte Spill
	jmp	.LBB22_14
.LBB22_13:
	leaq	.L.str21169, %rsi
	movq	-120(%rbp), %rdi        # 8-byte Reload
	movb	$0, %al
	callq	gzprintf
	movl	%eax, -128(%rbp)        # 4-byte Spill
.LBB22_14:
	movq	-64(%rbp), %rax         # 8-byte Reload
	movq	%rax, %rsp
.LBB22_15:
	movq	%rbp, %rsp
	popq	%rbp
	ret
.Ltmp284:
	.size	trace_logger_log_vector, .Ltmp284-trace_logger_log_vector
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"FAILED: store_loc[%f] = %f, should be %f\n"
	.size	.L.str, 42

	.type	.L.str2,@object         # @.str2
.L.str2:
	.asciz	"err == 0 && \"Failed to allocate memory!\""
	.size	.L.str2, 41

	.type	.L.str3,@object         # @.str3
.L.str3:
	.asciz	"test_dma_load_store.c"
	.size	.L.str3, 22

	.type	.L__PRETTY_FUNCTION__.main,@object # @__PRETTY_FUNCTION__.main
.L__PRETTY_FUNCTION__.main:
	.asciz	"int main()"
	.size	.L__PRETTY_FUNCTION__.main, 11

	.type	.L.str4,@object         # @.str4
.L.str4:
	.asciz	"running BIG."
	.size	.L.str4, 13

	.type	.L.str5,@object         # @.str5
.L.str5:
	.asciz	"Test failed with %d errors."
	.size	.L.str5, 28

	.type	.L.str6,@object         # @.str6
.L.str6:
	.asciz	"Test passed!\n"
	.size	.L.str6, 14

	.type	.L.str7,@object         # @.str7
.L.str7:
	.asciz	"store_loc[%f] = %f should be %f\n"
	.size	.L.str7, 33

	.type	.L.str1,@object         # @.str1
	.section	.rodata,"a",@progbits
.L.str1:
	.asciz	"test_stores"
	.size	.L.str1, 12

	.type	.L.str8,@object         # @.str8
.L.str8:
	.asciz	"0:0"
	.size	.L.str8, 4

	.type	.L.str9,@object         # @.str9
.L.str9:
	.asciz	"1"
	.size	.L.str9, 2

	.type	.L.str10,@object        # @.str10
.L.str10:
	.zero	1
	.size	.L.str10, 1

	.type	.L.str11,@object        # @.str11
.L.str11:
	.asciz	"phi"
	.size	.L.str11, 4

	.type	.L.str12,@object        # @.str12
.L.str12:
	.asciz	"num_vals"
	.size	.L.str12, 9

	.type	.L.str13,@object        # @.str13
.L.str13:
	.asciz	"0:0-5"
	.size	.L.str13, 6

	.type	.L.str14,@object        # @.str14
.L.str14:
	.asciz	".lr.ph:1"
	.size	.L.str14, 9

	.type	.L.str15,@object        # @.str15
.L.str15:
	.asciz	"._crit_edge:0"
	.size	.L.str15, 14

	.type	.L.str16,@object        # @.str16
.L.str16:
	.asciz	"indvars.iv"
	.size	.L.str16, 11

	.type	.L.str17,@object        # @.str17
.L.str17:
	.asciz	"indvars.iv.next"
	.size	.L.str17, 16

	.type	.L.str18,@object        # @.str18
.L.str18:
	.asciz	"15:1"
	.size	.L.str18, 5

	.type	.L.str19,@object        # @.str19
.L.str19:
	.asciz	"num_failures.01"
	.size	.L.str19, 16

	.type	.L.str20,@object        # @.str20
.L.str20:
	.asciz	"num_failures.1"
	.size	.L.str20, 15

	.type	.L.str21,@object        # @.str21
.L.str21:
	.asciz	"2"
	.size	.L.str21, 2

	.type	.L.str22,@object        # @.str22
.L.str22:
	.asciz	"store_loc"
	.size	.L.str22, 10

	.type	.L.str23,@object        # @.str23
.L.str23:
	.asciz	"3"
	.size	.L.str23, 2

	.type	.L.str24,@object        # @.str24
.L.str24:
	.asciz	"4"
	.size	.L.str24, 2

	.type	.L.str25,@object        # @.str25
.L.str25:
	.asciz	"store_vals"
	.size	.L.str25, 11

	.type	.L.str26,@object        # @.str26
.L.str26:
	.asciz	"5"
	.size	.L.str26, 2

	.type	.L.str27,@object        # @.str27
.L.str27:
	.asciz	"6"
	.size	.L.str27, 2

	.type	.L.str28,@object        # @.str28
.L.str28:
	.asciz	"7"
	.size	.L.str28, 2

	.type	.L.str29,@object        # @.str29
.L.str29:
	.asciz	".lr.ph:1-0"
	.size	.L.str29, 11

	.type	.L.str30,@object        # @.str30
.L.str30:
	.asciz	"8:1"
	.size	.L.str30, 4

	.type	.L.str31,@object        # @.str31
.L.str31:
	.asciz	"9"
	.size	.L.str31, 2

	.type	.L.str32,@object        # @.str32
.L.str32:
	.asciz	"stdout"
	.size	.L.str32, 7

	.type	.L.str33,@object        # @.str33
.L.str33:
	.asciz	"10"
	.size	.L.str33, 3

	.type	.L.str34,@object        # @.str34
.L.str34:
	.asciz	"11"
	.size	.L.str34, 3

	.type	.L.str35,@object        # @.str35
.L.str35:
	.asciz	"12"
	.size	.L.str35, 3

	.type	.L.str36,@object        # @.str36
.L.str36:
	.asciz	"13"
	.size	.L.str36, 3

	.type	.L.str37,@object        # @.str37
.L.str37:
	.asciz	"fprintf"
	.size	.L.str37, 8

	.type	.L.str38,@object        # @.str38
.L.str38:
	.asciz	"14"
	.size	.L.str38, 3

	.type	.L.str39,@object        # @.str39
.L.str39:
	.asciz	"8:1-1"
	.size	.L.str39, 6

	.type	.L.str40,@object        # @.str40
.L.str40:
	.asciz	"lftr.wideiv"
	.size	.L.str40, 12

	.type	.L.str41,@object        # @.str41
.L.str41:
	.asciz	"exitcond"
	.size	.L.str41, 9

	.type	.L.str42,@object        # @.str42
.L.str42:
	.asciz	"15:1-0"
	.size	.L.str42, 7

	.type	.L.str43,@object        # @.str43
	.align	16
.L.str43:
	.asciz	"num_failures.0.lcssa"
	.size	.L.str43, 21

	.type	.L.str44,@object        # @.str44
.L.str44:
	.asciz	"._crit_edge:0-0"
	.size	.L.str44, 16

	.type	.L.str45,@object        # @.str45
.L.str45:
	.asciz	"s1"
	.size	.L.str45, 3

	.type	.L.str46,@object        # @.str46
.L.str46:
	.asciz	"0:0-4"
	.size	.L.str46, 6

	.type	.L.str47,@object        # @.str47
.L.str47:
	.asciz	".lr.ph:1-1"
	.size	.L.str47, 11

	.type	.L.str48,@object        # @.str48
.L.str48:
	.asciz	"big"
	.size	.L.str48, 4

	.type	.L.str49,@object        # @.str49
.L.str49:
	.asciz	"dmaLoad"
	.size	.L.str49, 8

	.type	.L.str50,@object        # @.str50
.L.str50:
	.asciz	"0:0-3"
	.size	.L.str50, 6

	.type	.L.str51,@object        # @.str51
.L.str51:
	.asciz	".lr.ph5:1"
	.size	.L.str51, 10

	.type	.L.str52,@object        # @.str52
	.align	16
.L.str52:
	.asciz	"._crit_edge6.thread:0"
	.size	.L.str52, 22

	.type	.L.str53,@object        # @.str53
	.align	16
.L.str53:
	.asciz	"._crit_edge6.thread:0-0"
	.size	.L.str53, 24

	.type	.L.str54,@object        # @.str54
	.align	16
.L.str54:
	.asciz	"._crit_edge6.thread:0-2"
	.size	.L.str54, 24

	.type	.L.str55,@object        # @.str55
.L.str55:
	.asciz	"indvars.iv7"
	.size	.L.str55, 12

	.type	.L.str56,@object        # @.str56
	.align	16
.L.str56:
	.asciz	"indvars.iv.next8"
	.size	.L.str56, 17

	.type	.L.str57,@object        # @.str57
.L.str57:
	.asciz	"8"
	.size	.L.str57, 2

	.type	.L.str58,@object        # @.str58
.L.str58:
	.asciz	"soft_plus"
	.size	.L.str58, 10

	.type	.L.str59,@object        # @.str59
.L.str59:
	.asciz	".lr.ph5:1-0"
	.size	.L.str59, 12

	.type	.L.str60,@object        # @.str60
.L.str60:
	.asciz	"lftr.wideiv9"
	.size	.L.str60, 13

	.type	.L.str61,@object        # @.str61
.L.str61:
	.asciz	"exitcond10"
	.size	.L.str61, 11

	.type	.L.str62,@object        # @.str62
.L.str62:
	.asciz	".lr.ph5:1-1"
	.size	.L.str62, 12

	.type	.L.str63,@object        # @.str63
.L.str63:
	.asciz	"._crit_edge6:0"
	.size	.L.str63, 15

	.type	.L.str64,@object        # @.str64
	.align	16
.L.str64:
	.asciz	"._crit_edge6:0-0"
	.size	.L.str64, 17

	.type	.L.str65,@object        # @.str65
	.align	16
.L.str65:
	.asciz	"._crit_edge6:0-2"
	.size	.L.str65, 17

	.type	.L.str66,@object        # @.str66
.L.str66:
	.asciz	"15"
	.size	.L.str66, 3

	.type	.L.str67,@object        # @.str67
.L.str67:
	.asciz	"dmaStore"
	.size	.L.str67, 9

	.type	.L.str68,@object        # @.str68
.L.str68:
	.asciz	"._crit_edge:0-1"
	.size	.L.str68, 16

	.type	.L.str69,@object        # @.str69
.L.str69:
	.asciz	"expf"
	.size	.L.str69, 5

	.type	.L.str70,@object        # @.str70
.L.str70:
	.asciz	"x"
	.size	.L.str70, 2

	.type	.L.str71,@object        # @.str71
.L.str71:
	.asciz	"logf"
	.size	.L.str71, 5

	.type	.L.str72,@object        # @.str72
.L.str72:
	.asciz	"0:0-1"
	.size	.L.str72, 6

	.type	.L.str73,@object        # @.str73
.L.str73:
	.asciz	"main"
	.size	.L.str73, 5

	.type	.L.str74,@object        # @.str74
.L.str74:
	.asciz	"posix_memalign"
	.size	.L.str74, 15

	.type	.L.str75,@object        # @.str75
.L.str75:
	.asciz	"5:0"
	.size	.L.str75, 4

	.type	.L.str76,@object        # @.str76
.L.str76:
	.asciz	"4:0"
	.size	.L.str76, 4

	.type	.L.str77,@object        # @.str77
.L.str77:
	.asciz	"4:0-0"
	.size	.L.str77, 6

	.type	.L.str78,@object        # @.str78
.L.str78:
	.asciz	"__assert_fail"
	.size	.L.str78, 14

	.type	.L.str79,@object        # @.str79
.L.str79:
	.asciz	"4:0-1"
	.size	.L.str79, 6

	.type	.L.str80,@object        # @.str80
.L.str80:
	.asciz	"5:0-1"
	.size	.L.str80, 6

	.type	.L.str81,@object        # @.str81
.L.str81:
	.asciz	".preheader:0"
	.size	.L.str81, 13

	.type	.L.str82,@object        # @.str82
.L.str82:
	.asciz	"11:0"
	.size	.L.str82, 5

	.type	.L.str83,@object        # @.str83
.L.str83:
	.asciz	".preheader:0-2"
	.size	.L.str83, 15

	.type	.L.str84,@object        # @.str84
.L.str84:
	.asciz	"12:1"
	.size	.L.str84, 5

	.type	.L.str85,@object        # @.str85
.L.str85:
	.asciz	"11:0-0"
	.size	.L.str85, 7

	.type	.L.str86,@object        # @.str86
.L.str86:
	.asciz	"11:0-1"
	.size	.L.str86, 7

	.type	.L.str87,@object        # @.str87
.L.str87:
	.asciz	"12:1-1"
	.size	.L.str87, 7

	.type	.L.str88,@object        # @.str88
.L.str88:
	.asciz	"16"
	.size	.L.str88, 3

	.type	.L.str89,@object        # @.str89
.L.str89:
	.asciz	"12:1-3"
	.size	.L.str89, 7

	.type	.L.str90,@object        # @.str90
.L.str90:
	.asciz	"12:1-4"
	.size	.L.str90, 7

	.type	.L.str91,@object        # @.str91
.L.str91:
	.asciz	"17:0"
	.size	.L.str91, 5

	.type	.L.str92,@object        # @.str92
.L.str92:
	.asciz	"18"
	.size	.L.str92, 3

	.type	.L.str93,@object        # @.str93
.L.str93:
	.asciz	"19"
	.size	.L.str93, 3

	.type	.L.str94,@object        # @.str94
.L.str94:
	.asciz	"20"
	.size	.L.str94, 3

	.type	.L.str95,@object        # @.str95
.L.str95:
	.asciz	"21"
	.size	.L.str95, 3

	.type	.L.str96,@object        # @.str96
.L.str96:
	.asciz	"17:0-2"
	.size	.L.str96, 7

	.type	.L.str97,@object        # @.str97
.L.str97:
	.asciz	"22"
	.size	.L.str97, 3

	.type	.L.str98,@object        # @.str98
.L.str98:
	.asciz	"23"
	.size	.L.str98, 3

	.type	.L.str99,@object        # @.str99
.L.str99:
	.asciz	"24"
	.size	.L.str99, 3

	.type	.L.str100,@object       # @.str100
.L.str100:
	.asciz	"25"
	.size	.L.str100, 3

	.type	.L.str101,@object       # @.str101
.L.str101:
	.asciz	"26"
	.size	.L.str101, 3

	.type	.L.str102,@object       # @.str102
.L.str102:
	.asciz	"17:0-6"
	.size	.L.str102, 7

	.type	.L.str103,@object       # @.str103
.L.str103:
	.asciz	"29:0"
	.size	.L.str103, 5

	.type	.L.str104,@object       # @.str104
.L.str104:
	.asciz	"27:0"
	.size	.L.str104, 5

	.type	.L.str105,@object       # @.str105
.L.str105:
	.asciz	"28"
	.size	.L.str105, 3

	.type	.L.str106,@object       # @.str106
.L.str106:
	.asciz	"27:0-0"
	.size	.L.str106, 7

	.type	.L.str107,@object       # @.str107
.L.str107:
	.asciz	"39:0"
	.size	.L.str107, 5

	.type	.L.str108,@object       # @.str108
.L.str108:
	.asciz	"30"
	.size	.L.str108, 3

	.type	.L.str109,@object       # @.str109
.L.str109:
	.asciz	"31"
	.size	.L.str109, 3

	.type	.L.str110,@object       # @.str110
.L.str110:
	.asciz	"32"
	.size	.L.str110, 3

	.type	.L.str111,@object       # @.str111
.L.str111:
	.asciz	"33"
	.size	.L.str111, 3

	.type	.L.str112,@object       # @.str112
.L.str112:
	.asciz	"34"
	.size	.L.str112, 3

	.type	.L.str113,@object       # @.str113
.L.str113:
	.asciz	"35"
	.size	.L.str113, 3

	.type	.L.str114,@object       # @.str114
.L.str114:
	.asciz	"36"
	.size	.L.str114, 3

	.type	.L.str115,@object       # @.str115
.L.str115:
	.asciz	"37"
	.size	.L.str115, 3

	.type	.L.str116,@object       # @.str116
.L.str116:
	.asciz	"38"
	.size	.L.str116, 3

	.type	.L.str117,@object       # @.str117
.L.str117:
	.asciz	"29:0-2"
	.size	.L.str117, 7

	.type	.L.str118,@object       # @.str118
.L.str118:
	.asciz	".0"
	.size	.L.str118, 3

	.type	.L.str119,@object       # @.str119
.L.str119:
	.asciz	"39:0-0"
	.size	.L.str119, 7

	.type	.L.str120,@object       # @.str120
.L.str120:
	.asciz	"s1/loop 79\n"
	.size	.L.str120, 12

	.type	.L.str121,@object       # @.str121
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str121:
	.asciz	"size > 0"
	.size	.L.str121, 9

	.type	.L.str1122,@object      # @.str1122
.L.str1122:
	.asciz	"/workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c"
	.size	.L.str1122, 57

	.type	.L__PRETTY_FUNCTION__._dmaImpl3,@object # @__PRETTY_FUNCTION__._dmaImpl3
.L__PRETTY_FUNCTION__._dmaImpl3:
	.asciz	"int _dmaImpl3(void *, void *, size_t)"
	.size	.L__PRETTY_FUNCTION__._dmaImpl3, 38

	.type	.L.str2123,@object      # @.str2123
	.section	.rodata,"a",@progbits
.L.str2123:
	.asciz	"_dmaImpl3"
	.size	.L.str2123, 10

	.type	.L.str3124,@object      # @.str3124
.L.str3124:
	.asciz	"0:0"
	.size	.L.str3124, 4

	.type	.L.str4125,@object      # @.str4125
.L.str4125:
	.asciz	"1"
	.size	.L.str4125, 2

	.type	.L.str5126,@object      # @.str5126
.L.str5126:
	.zero	1
	.size	.L.str5126, 1

	.type	.L.str6127,@object      # @.str6127
.L.str6127:
	.asciz	"phi"
	.size	.L.str6127, 4

	.type	.L.str7128,@object      # @.str7128
.L.str7128:
	.asciz	"size"
	.size	.L.str7128, 5

	.type	.L.str8129,@object      # @.str8129
.L.str8129:
	.asciz	"0:0-3"
	.size	.L.str8129, 6

	.type	.L.str9130,@object      # @.str9130
.L.str9130:
	.asciz	"2:0"
	.size	.L.str9130, 4

	.type	.L.str10131,@object     # @.str10131
.L.str10131:
	.asciz	"3:0"
	.size	.L.str10131, 4

	.type	.L.str11132,@object     # @.str11132
.L.str11132:
	.asciz	"2:0-0"
	.size	.L.str11132, 6

	.type	.L.str12133,@object     # @.str12133
.L.str12133:
	.asciz	"__assert_fail"
	.size	.L.str12133, 14

	.type	.L.str13134,@object     # @.str13134
.L.str13134:
	.asciz	"2:0-1"
	.size	.L.str13134, 6

	.type	.L.str14135,@object     # @.str14135
.L.str14135:
	.asciz	"4"
	.size	.L.str14135, 2

	.type	.L.str15136,@object     # @.str15136
.L.str15136:
	.asciz	"memmove"
	.size	.L.str15136, 8

	.type	.L.str16137,@object     # @.str16137
.L.str16137:
	.asciz	"dst_addr"
	.size	.L.str16137, 9

	.type	.L.str17138,@object     # @.str17138
.L.str17138:
	.asciz	"src_addr"
	.size	.L.str17138, 9

	.type	.L.str18139,@object     # @.str18139
.L.str18139:
	.asciz	"3:0-0"
	.size	.L.str18139, 6

	.type	.L.str19140,@object     # @.str19140
.L.str19140:
	.asciz	"src_host_addr"
	.size	.L.str19140, 14

	.type	.L.str20141,@object     # @.str20141
.L.str20141:
	.asciz	"dst_host_addr"
	.size	.L.str20141, 14

	.type	.L.str21142,@object     # @.str21142
.L.str21142:
	.asciz	"start_addr"
	.size	.L.str21142, 11

	.type	.L.str22143,@object     # @.str22143
.L.str22143:
	.asciz	"value"
	.size	.L.str22143, 6

	.type	initp,@object           # @initp
	.bss
	.globl	initp
initp:
	.byte	0                       # 0x0
	.size	initp, 1

	.type	inst_count,@object      # @inst_count
	.globl	inst_count
	.align	4
inst_count:
	.long	0                       # 0x0
	.size	inst_count, 4

	.type	.L.str148,@object       # @.str148
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str148:
	.asciz	"%%%% LABEL MAP START %%%%\n"
	.size	.L.str148, 27

	.type	.L.str1149,@object      # @.str1149
.L.str1149:
	.asciz	"%%%% LABEL MAP END %%%%\n\n"
	.size	.L.str1149, 26

	.type	full_trace_file,@object # @full_trace_file
	.comm	full_trace_file,8,8
	.type	.L.str2150,@object      # @.str2150
.L.str2150:
	.asciz	"dynamic_trace.gz"
	.size	.L.str2150, 17

	.type	.L.str3151,@object      # @.str3151
.L.str3151:
	.asciz	"w"
	.size	.L.str3151, 2

	.type	.L.str4152,@object      # @.str4152
.L.str4152:
	.asciz	"Failed to open logfile \"dynamic_trace\""
	.size	.L.str4152, 39

	.type	current_toplevel_function,@object # @current_toplevel_function
	.comm	current_toplevel_function,8,8
	.type	current_logging_status,@object # @current_logging_status
	.comm	current_logging_status,4,4
	.type	.L.str5153,@object      # @.str5153
.L.str5153:
	.asciz	"false && \"Returning from within a toplevel function before it was called!\""
	.size	.L.str5153, 75

	.type	.L.str6154,@object      # @.str6154
.L.str6154:
	.asciz	"/workspace/LLVM-Tracer/profile-func/trace_logger.c"
	.size	.L.str6154, 51

	.type	.L__PRETTY_FUNCTION__.log_or_not,@object # @__PRETTY_FUNCTION__.log_or_not
.L__PRETTY_FUNCTION__.log_or_not:
	.asciz	"logging_status log_or_not(_Bool, _Bool, int, char *)"
	.size	.L__PRETTY_FUNCTION__.log_or_not, 53

	.type	.L.str7155,@object      # @.str7155
.L.str7155:
	.asciz	"false && \"Cannot call a top level function from within another one!\""
	.size	.L.str7155, 69

	.type	.L.str8156,@object      # @.str8156
.L.str8156:
	.asciz	"0x"
	.size	.L.str8156, 3

	.type	.L.str9157,@object      # @.str9157
.L.str9157:
	.asciz	"%02x"
	.size	.L.str9157, 5

	.type	.L.str10158,@object     # @.str10158
.L.str10158:
	.asciz	"Stopping logging at inst %d.\n"
	.size	.L.str10158, 30

	.type	.L.str11159,@object     # @.str11159
.L.str11159:
	.asciz	"Starting to log at inst = %d.\n"
	.size	.L.str11159, 31

	.type	.L.str12160,@object     # @.str12160
.L.str12160:
	.asciz	"\nentry,%s,%d,\n"
	.size	.L.str12160, 15

	.type	.L.str13161,@object     # @.str13161
.L.str13161:
	.asciz	"\n0,%d,%s,%s,%s,%d,%d\n"
	.size	.L.str13161, 22

	.type	.L.str14162,@object     # @.str14162
.L.str14162:
	.asciz	"initp == true"
	.size	.L.str14162, 14

	.type	.L__PRETTY_FUNCTION__.trace_logger_log_int,@object # @__PRETTY_FUNCTION__.trace_logger_log_int
.L__PRETTY_FUNCTION__.trace_logger_log_int:
	.asciz	"void trace_logger_log_int(int, int, int64_t, int, char *, int, char *)"
	.size	.L__PRETTY_FUNCTION__.trace_logger_log_int, 71

	.type	.L.str15163,@object     # @.str15163
.L.str15163:
	.asciz	"r,%d,%ld,%d"
	.size	.L.str15163, 12

	.type	.L.str16164,@object     # @.str16164
.L.str16164:
	.asciz	"f,%d,%ld,%d"
	.size	.L.str16164, 12

	.type	.L.str17165,@object     # @.str17165
.L.str17165:
	.asciz	"%d,%d,%ld,%d"
	.size	.L.str17165, 13

	.type	.L.str18166,@object     # @.str18166
.L.str18166:
	.asciz	",%s"
	.size	.L.str18166, 4

	.type	.L.str19167,@object     # @.str19167
.L.str19167:
	.asciz	", "
	.size	.L.str19167, 3

	.type	.L.str20168,@object     # @.str20168
.L.str20168:
	.asciz	",%s,\n"
	.size	.L.str20168, 6

	.type	.L.str21169,@object     # @.str21169
.L.str21169:
	.asciz	",\n"
	.size	.L.str21169, 3

	.type	.L__PRETTY_FUNCTION__.trace_logger_log_ptr,@object # @__PRETTY_FUNCTION__.trace_logger_log_ptr
.L__PRETTY_FUNCTION__.trace_logger_log_ptr:
	.asciz	"void trace_logger_log_ptr(int, int, uint64_t, int, char *, int, char *)"
	.size	.L__PRETTY_FUNCTION__.trace_logger_log_ptr, 72

	.type	.L.str22170,@object     # @.str22170
.L.str22170:
	.asciz	"r,%d,%#llx,%d"
	.size	.L.str22170, 14

	.type	.L.str23171,@object     # @.str23171
.L.str23171:
	.asciz	"f,%d,%#llx,%d"
	.size	.L.str23171, 14

	.type	.L.str24172,@object     # @.str24172
.L.str24172:
	.asciz	"%d,%d,%#llx,%d"
	.size	.L.str24172, 15

	.type	.L__PRETTY_FUNCTION__.trace_logger_log_double,@object # @__PRETTY_FUNCTION__.trace_logger_log_double
.L__PRETTY_FUNCTION__.trace_logger_log_double:
	.asciz	"void trace_logger_log_double(int, int, double, int, char *, int, char *)"
	.size	.L__PRETTY_FUNCTION__.trace_logger_log_double, 73

	.type	.L.str25173,@object     # @.str25173
.L.str25173:
	.asciz	"r,%d,%f,%d"
	.size	.L.str25173, 11

	.type	.L.str26174,@object     # @.str26174
.L.str26174:
	.asciz	"f,%d,%f,%d"
	.size	.L.str26174, 11

	.type	.L.str27175,@object     # @.str27175
.L.str27175:
	.asciz	"%d,%d,%f,%d"
	.size	.L.str27175, 12

	.type	.L__PRETTY_FUNCTION__.trace_logger_log_vector,@object # @__PRETTY_FUNCTION__.trace_logger_log_vector
.L__PRETTY_FUNCTION__.trace_logger_log_vector:
	.asciz	"void trace_logger_log_vector(int, int, uint8_t *, int, char *, int, char *)"
	.size	.L__PRETTY_FUNCTION__.trace_logger_log_vector, 76

	.type	.L.str28176,@object     # @.str28176
.L.str28176:
	.asciz	"r,%d,%s,%d"
	.size	.L.str28176, 11

	.type	.L.str29177,@object     # @.str29177
.L.str29177:
	.asciz	"f,%d,%s,%d"
	.size	.L.str29177, 11

	.type	.L.str30178,@object     # @.str30178
.L.str30178:
	.asciz	"%d,%d,%s,%d"
	.size	.L.str30178, 12

	.text
.Ldebug_end0:
	.section	.debug_str,"MS",@progbits,1
.Linfo_string0:
	.asciz	"clang version 3.4.2 (tags/RELEASE_34/dot2-final)"
.Linfo_string1:
	.asciz	"test_dma_load_store.c"
.Linfo_string2:
	.asciz	"/workspace/gem5-aladdin/src/aladdin/integration-test/with-cpu/test_dma_load_store/big"
.Linfo_string3:
	.asciz	"num_vals"
.Linfo_string4:
	.asciz	"int"
.Linfo_string5:
	.asciz	"test_stores"
.Linfo_string6:
	.asciz	"s1"
.Linfo_string7:
	.asciz	"big"
.Linfo_string8:
	.asciz	"main"
.Linfo_string9:
	.asciz	"soft_plus"
.Linfo_string10:
	.asciz	"float"
.Linfo_string11:
	.asciz	"/workspace/gem5-aladdin/src/aladdin/gem5/dma_interface.c"
.Linfo_string12:
	.asciz	"_dmaImpl3"
.Linfo_string13:
	.asciz	"dmaLoad"
.Linfo_string14:
	.asciz	"dmaStore"
.Linfo_string15:
	.asciz	"setReadyBits"
.Linfo_string16:
	.asciz	"dmaFence"
.Linfo_string17:
	.asciz	"store_vals"
.Linfo_string18:
	.asciz	"store_loc"
.Linfo_string19:
	.asciz	"num_failures"
.Linfo_string20:
	.asciz	"i"
.Linfo_string21:
	.asciz	"x"
.Linfo_string22:
	.asciz	"err"
.Linfo_string23:
	.asciz	"dst_addr"
.Linfo_string24:
	.asciz	"src_addr"
.Linfo_string25:
	.asciz	"size"
.Linfo_string26:
	.asciz	"long unsigned int"
.Linfo_string27:
	.asciz	"size_t"
.Linfo_string28:
	.asciz	"src_host_addr"
.Linfo_string29:
	.asciz	"dst_host_addr"
.Linfo_string30:
	.asciz	"start_addr"
.Linfo_string31:
	.asciz	"value"
.Linfo_string32:
	.asciz	"unsigned int"
	.section	.debug_info,"",@progbits
.L.debug_info_begin0:
	.long	565                     # Length of Unit
	.short	4                       # DWARF version number
	.long	.L.debug_abbrev_begin   # Offset Into Abbrev. Section
	.byte	8                       # Address Size (in bytes)
	.byte	1                       # Abbrev [1] 0xb:0x22e DW_TAG_compile_unit
	.long	.Linfo_string0          # DW_AT_producer
	.short	12                      # DW_AT_language
	.long	.Linfo_string1          # DW_AT_name
	.quad	0                       # DW_AT_low_pc
	.long	.Lsection_line          # DW_AT_stmt_list
	.long	.Linfo_string2          # DW_AT_comp_dir
                                        # DW_AT_APPLE_optimized
	.byte	2                       # Abbrev [2] 0x26:0xd DW_TAG_variable
	.long	.Linfo_string3          # DW_AT_name
	.long	51                      # DW_AT_type
	.byte	1                       # DW_AT_decl_file
	.byte	170                     # DW_AT_decl_line
	.ascii	"\200\b"                # DW_AT_const_value
	.byte	3                       # Abbrev [3] 0x33:0x5 DW_TAG_const_type
	.long	56                      # DW_AT_type
	.byte	4                       # Abbrev [4] 0x38:0x7 DW_TAG_base_type
	.long	.Linfo_string4          # DW_AT_name
	.byte	5                       # DW_AT_encoding
	.byte	4                       # DW_AT_byte_size
	.byte	5                       # Abbrev [5] 0x3f:0x78 DW_TAG_subprogram
	.long	.Linfo_string5          # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	27                      # DW_AT_decl_line
                                        # DW_AT_prototyped
	.long	56                      # DW_AT_type
                                        # DW_AT_external
	.quad	.Lfunc_begin0           # DW_AT_low_pc
	.quad	.Lfunc_end0             # DW_AT_high_pc
	.byte	1                       # DW_AT_frame_base
	.byte	87
                                        # DW_AT_APPLE_omit_frame_ptr
	.byte	6                       # Abbrev [6] 0x5c:0xf DW_TAG_formal_parameter
	.long	.Linfo_string17         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	27                      # DW_AT_decl_line
	.long	563                     # DW_AT_type
	.long	.Ldebug_loc0            # DW_AT_location
	.byte	6                       # Abbrev [6] 0x6b:0xf DW_TAG_formal_parameter
	.long	.Linfo_string18         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	27                      # DW_AT_decl_line
	.long	563                     # DW_AT_type
	.long	.Ldebug_loc2            # DW_AT_location
	.byte	6                       # Abbrev [6] 0x7a:0xf DW_TAG_formal_parameter
	.long	.Linfo_string3          # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	27                      # DW_AT_decl_line
	.long	56                      # DW_AT_type
	.long	.Ldebug_loc4            # DW_AT_location
	.byte	7                       # Abbrev [7] 0x89:0xf DW_TAG_variable
	.long	.Linfo_string19         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	28                      # DW_AT_decl_line
	.long	56                      # DW_AT_type
	.long	.Ldebug_loc6            # DW_AT_location
	.byte	8                       # Abbrev [8] 0x98:0x1e DW_TAG_lexical_block
	.quad	.Ltmp21                 # DW_AT_low_pc
	.quad	.Ltmp27                 # DW_AT_high_pc
	.byte	9                       # Abbrev [9] 0xa9:0xc DW_TAG_variable
	.long	.Linfo_string20         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	29                      # DW_AT_decl_line
	.long	56                      # DW_AT_type
	.byte	0                       # DW_AT_const_value
	.byte	0                       # End Of Children Mark
	.byte	0                       # End Of Children Mark
	.byte	10                      # Abbrev [10] 0xb7:0x65 DW_TAG_subprogram
	.long	.Linfo_string6          # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	71                      # DW_AT_decl_line
                                        # DW_AT_prototyped
                                        # DW_AT_external
	.quad	.Lfunc_begin1           # DW_AT_low_pc
	.quad	.Lfunc_end1             # DW_AT_high_pc
	.byte	1                       # DW_AT_frame_base
	.byte	87
                                        # DW_AT_APPLE_omit_frame_ptr
	.byte	6                       # Abbrev [6] 0xd0:0xf DW_TAG_formal_parameter
	.long	.Linfo_string17         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	71                      # DW_AT_decl_line
	.long	563                     # DW_AT_type
	.long	.Ldebug_loc9            # DW_AT_location
	.byte	6                       # Abbrev [6] 0xdf:0xf DW_TAG_formal_parameter
	.long	.Linfo_string18         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	71                      # DW_AT_decl_line
	.long	563                     # DW_AT_type
	.long	.Ldebug_loc11           # DW_AT_location
	.byte	6                       # Abbrev [6] 0xee:0xf DW_TAG_formal_parameter
	.long	.Linfo_string3          # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	71                      # DW_AT_decl_line
	.long	56                      # DW_AT_type
	.long	.Ldebug_loc13           # DW_AT_location
	.byte	8                       # Abbrev [8] 0xfd:0x1e DW_TAG_lexical_block
	.quad	.Ltmp51                 # DW_AT_low_pc
	.quad	.Ltmp53                 # DW_AT_high_pc
	.byte	9                       # Abbrev [9] 0x10e:0xc DW_TAG_variable
	.long	.Linfo_string20         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	79                      # DW_AT_decl_line
	.long	56                      # DW_AT_type
	.byte	0                       # DW_AT_const_value
	.byte	0                       # End Of Children Mark
	.byte	0                       # End Of Children Mark
	.byte	10                      # Abbrev [10] 0x11c:0x6b DW_TAG_subprogram
	.long	.Linfo_string7          # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	90                      # DW_AT_decl_line
                                        # DW_AT_prototyped
                                        # DW_AT_external
	.quad	.Lfunc_begin2           # DW_AT_low_pc
	.quad	.Lfunc_end2             # DW_AT_high_pc
	.byte	1                       # DW_AT_frame_base
	.byte	87
                                        # DW_AT_APPLE_omit_frame_ptr
	.byte	6                       # Abbrev [6] 0x135:0xf DW_TAG_formal_parameter
	.long	.Linfo_string17         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	90                      # DW_AT_decl_line
	.long	563                     # DW_AT_type
	.long	.Ldebug_loc15           # DW_AT_location
	.byte	6                       # Abbrev [6] 0x144:0xf DW_TAG_formal_parameter
	.long	.Linfo_string18         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	90                      # DW_AT_decl_line
	.long	563                     # DW_AT_type
	.long	.Ldebug_loc17           # DW_AT_location
	.byte	6                       # Abbrev [6] 0x153:0xf DW_TAG_formal_parameter
	.long	.Linfo_string3          # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	90                      # DW_AT_decl_line
	.long	56                      # DW_AT_type
	.long	.Ldebug_loc19           # DW_AT_location
	.byte	11                      # Abbrev [11] 0x162:0x12 DW_TAG_lexical_block
	.long	.Ldebug_range           # DW_AT_ranges
	.byte	9                       # Abbrev [9] 0x167:0xc DW_TAG_variable
	.long	.Linfo_string20         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	121                     # DW_AT_decl_line
	.long	56                      # DW_AT_type
	.byte	0                       # DW_AT_const_value
	.byte	0                       # End Of Children Mark
	.byte	11                      # Abbrev [11] 0x174:0x12 DW_TAG_lexical_block
	.long	.Ldebug_range+48        # DW_AT_ranges
	.byte	9                       # Abbrev [9] 0x179:0xc DW_TAG_variable
	.long	.Linfo_string20         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	142                     # DW_AT_decl_line
	.long	56                      # DW_AT_type
	.byte	0                       # DW_AT_const_value
	.byte	0                       # End Of Children Mark
	.byte	0                       # End Of Children Mark
	.byte	12                      # Abbrev [12] 0x187:0x78 DW_TAG_subprogram
	.long	.Linfo_string8          # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	169                     # DW_AT_decl_line
	.long	56                      # DW_AT_type
                                        # DW_AT_external
	.quad	.Lfunc_begin4           # DW_AT_low_pc
	.quad	.Lfunc_end4             # DW_AT_high_pc
	.byte	1                       # DW_AT_frame_base
	.byte	87
                                        # DW_AT_APPLE_omit_frame_ptr
	.byte	13                      # Abbrev [13] 0x1a4:0xf DW_TAG_variable
	.long	.Linfo_string17         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	171                     # DW_AT_decl_line
	.long	563                     # DW_AT_type
	.byte	3                       # DW_AT_location
	.byte	145
	.ascii	"\220\034"
	.byte	13                      # Abbrev [13] 0x1b3:0xf DW_TAG_variable
	.long	.Linfo_string18         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	171                     # DW_AT_decl_line
	.long	563                     # DW_AT_type
	.byte	3                       # DW_AT_location
	.byte	145
	.ascii	"\210\034"
	.byte	9                       # Abbrev [9] 0x1c2:0xd DW_TAG_variable
	.long	.Linfo_string3          # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	170                     # DW_AT_decl_line
	.long	51                      # DW_AT_type
	.ascii	"\200\b"                # DW_AT_const_value
	.byte	7                       # Abbrev [7] 0x1cf:0xf DW_TAG_variable
	.long	.Linfo_string22         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	172                     # DW_AT_decl_line
	.long	56                      # DW_AT_type
	.long	.Ldebug_loc23           # DW_AT_location
	.byte	7                       # Abbrev [7] 0x1de:0xf DW_TAG_variable
	.long	.Linfo_string19         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	201                     # DW_AT_decl_line
	.long	56                      # DW_AT_type
	.long	.Ldebug_loc26           # DW_AT_location
	.byte	11                      # Abbrev [11] 0x1ed:0x11 DW_TAG_lexical_block
	.long	.Ldebug_range+96        # DW_AT_ranges
	.byte	14                      # Abbrev [14] 0x1f2:0xb DW_TAG_variable
	.long	.Linfo_string20         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	178                     # DW_AT_decl_line
	.long	56                      # DW_AT_type
	.byte	0                       # End Of Children Mark
	.byte	0                       # End Of Children Mark
	.byte	15                      # Abbrev [15] 0x1ff:0x2d DW_TAG_subprogram
	.long	.Linfo_string9          # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	67                      # DW_AT_decl_line
                                        # DW_AT_prototyped
	.long	556                     # DW_AT_type
	.quad	.Lfunc_begin3           # DW_AT_low_pc
	.quad	.Lfunc_end3             # DW_AT_high_pc
	.byte	1                       # DW_AT_frame_base
	.byte	87
                                        # DW_AT_APPLE_omit_frame_ptr
	.byte	6                       # Abbrev [6] 0x21c:0xf DW_TAG_formal_parameter
	.long	.Linfo_string21         # DW_AT_name
	.byte	1                       # DW_AT_decl_file
	.byte	67                      # DW_AT_decl_line
	.long	556                     # DW_AT_type
	.long	.Ldebug_loc21           # DW_AT_location
	.byte	0                       # End Of Children Mark
	.byte	4                       # Abbrev [4] 0x22c:0x7 DW_TAG_base_type
	.long	.Linfo_string10         # DW_AT_name
	.byte	4                       # DW_AT_encoding
	.byte	4                       # DW_AT_byte_size
	.byte	16                      # Abbrev [16] 0x233:0x5 DW_TAG_pointer_type
	.long	556                     # DW_AT_type
	.byte	0                       # End Of Children Mark
.L.debug_info_end0:
.L.debug_info_begin1:
	.long	525                     # Length of Unit
	.short	4                       # DWARF version number
	.long	.L.debug_abbrev_begin   # Offset Into Abbrev. Section
	.byte	8                       # Address Size (in bytes)
	.byte	1                       # Abbrev [1] 0xb:0x206 DW_TAG_compile_unit
	.long	.Linfo_string0          # DW_AT_producer
	.short	12                      # DW_AT_language
	.long	.Linfo_string11         # DW_AT_name
	.quad	0                       # DW_AT_low_pc
	.long	.Lsection_line          # DW_AT_stmt_list
	.long	.Linfo_string2          # DW_AT_comp_dir
                                        # DW_AT_APPLE_optimized
	.byte	17                      # Abbrev [17] 0x26:0x4c DW_TAG_subprogram
	.long	.Linfo_string12         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	14                      # DW_AT_decl_line
                                        # DW_AT_prototyped
	.long	.Lsection_info+56       # DW_AT_type
                                        # DW_AT_external
	.quad	.Lfunc_begin5           # DW_AT_low_pc
	.quad	.Lfunc_end5             # DW_AT_high_pc
	.byte	1                       # DW_AT_frame_base
	.byte	87
                                        # DW_AT_APPLE_omit_frame_ptr
	.byte	1                       # DW_AT_inline
	.byte	6                       # Abbrev [6] 0x44:0xf DW_TAG_formal_parameter
	.long	.Linfo_string23         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	14                      # DW_AT_decl_line
	.long	502                     # DW_AT_type
	.long	.Ldebug_loc28           # DW_AT_location
	.byte	6                       # Abbrev [6] 0x53:0xf DW_TAG_formal_parameter
	.long	.Linfo_string24         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	14                      # DW_AT_decl_line
	.long	502                     # DW_AT_type
	.long	.Ldebug_loc30           # DW_AT_location
	.byte	6                       # Abbrev [6] 0x62:0xf DW_TAG_formal_parameter
	.long	.Linfo_string25         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	14                      # DW_AT_decl_line
	.long	503                     # DW_AT_type
	.long	.Ldebug_loc32           # DW_AT_location
	.byte	0                       # End Of Children Mark
	.byte	18                      # Abbrev [18] 0x72:0x90 DW_TAG_subprogram
	.long	.Linfo_string13         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	20                      # DW_AT_decl_line
                                        # DW_AT_prototyped
	.long	.Lsection_info+56       # DW_AT_type
                                        # DW_AT_external
	.quad	.Lfunc_begin6           # DW_AT_low_pc
	.quad	.Lfunc_end6             # DW_AT_high_pc
	.byte	1                       # DW_AT_frame_base
	.byte	87
                                        # DW_AT_APPLE_omit_frame_ptr
	.byte	6                       # Abbrev [6] 0x8f:0xf DW_TAG_formal_parameter
	.long	.Linfo_string23         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	20                      # DW_AT_decl_line
	.long	502                     # DW_AT_type
	.long	.Ldebug_loc34           # DW_AT_location
	.byte	6                       # Abbrev [6] 0x9e:0xf DW_TAG_formal_parameter
	.long	.Linfo_string28         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	20                      # DW_AT_decl_line
	.long	502                     # DW_AT_type
	.long	.Ldebug_loc36           # DW_AT_location
	.byte	6                       # Abbrev [6] 0xad:0xf DW_TAG_formal_parameter
	.long	.Linfo_string25         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	20                      # DW_AT_decl_line
	.long	503                     # DW_AT_type
	.long	.Ldebug_loc38           # DW_AT_location
	.byte	19                      # Abbrev [19] 0xbc:0x45 DW_TAG_inlined_subroutine
	.long	38                      # DW_AT_abstract_origin
	.quad	.Ltmp197                # DW_AT_low_pc
	.quad	.Ltmp199                # DW_AT_high_pc
	.byte	2                       # DW_AT_call_file
	.byte	21                      # DW_AT_call_line
	.byte	6                       # Abbrev [6] 0xd3:0xf DW_TAG_formal_parameter
	.long	.Linfo_string23         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	14                      # DW_AT_decl_line
	.long	502                     # DW_AT_type
	.long	.Ldebug_loc40           # DW_AT_location
	.byte	6                       # Abbrev [6] 0xe2:0xf DW_TAG_formal_parameter
	.long	.Linfo_string24         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	14                      # DW_AT_decl_line
	.long	502                     # DW_AT_type
	.long	.Ldebug_loc42           # DW_AT_location
	.byte	6                       # Abbrev [6] 0xf1:0xf DW_TAG_formal_parameter
	.long	.Linfo_string25         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	14                      # DW_AT_decl_line
	.long	503                     # DW_AT_type
	.long	.Ldebug_loc44           # DW_AT_location
	.byte	0                       # End Of Children Mark
	.byte	0                       # End Of Children Mark
	.byte	18                      # Abbrev [18] 0x102:0x90 DW_TAG_subprogram
	.long	.Linfo_string14         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	24                      # DW_AT_decl_line
                                        # DW_AT_prototyped
	.long	.Lsection_info+56       # DW_AT_type
                                        # DW_AT_external
	.quad	.Lfunc_begin7           # DW_AT_low_pc
	.quad	.Lfunc_end7             # DW_AT_high_pc
	.byte	1                       # DW_AT_frame_base
	.byte	87
                                        # DW_AT_APPLE_omit_frame_ptr
	.byte	6                       # Abbrev [6] 0x11f:0xf DW_TAG_formal_parameter
	.long	.Linfo_string29         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	24                      # DW_AT_decl_line
	.long	502                     # DW_AT_type
	.long	.Ldebug_loc46           # DW_AT_location
	.byte	6                       # Abbrev [6] 0x12e:0xf DW_TAG_formal_parameter
	.long	.Linfo_string24         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	24                      # DW_AT_decl_line
	.long	502                     # DW_AT_type
	.long	.Ldebug_loc48           # DW_AT_location
	.byte	6                       # Abbrev [6] 0x13d:0xf DW_TAG_formal_parameter
	.long	.Linfo_string25         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	24                      # DW_AT_decl_line
	.long	503                     # DW_AT_type
	.long	.Ldebug_loc50           # DW_AT_location
	.byte	19                      # Abbrev [19] 0x14c:0x45 DW_TAG_inlined_subroutine
	.long	38                      # DW_AT_abstract_origin
	.quad	.Ltmp214                # DW_AT_low_pc
	.quad	.Ltmp216                # DW_AT_high_pc
	.byte	2                       # DW_AT_call_file
	.byte	25                      # DW_AT_call_line
	.byte	6                       # Abbrev [6] 0x163:0xf DW_TAG_formal_parameter
	.long	.Linfo_string23         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	14                      # DW_AT_decl_line
	.long	502                     # DW_AT_type
	.long	.Ldebug_loc52           # DW_AT_location
	.byte	6                       # Abbrev [6] 0x172:0xf DW_TAG_formal_parameter
	.long	.Linfo_string24         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	14                      # DW_AT_decl_line
	.long	502                     # DW_AT_type
	.long	.Ldebug_loc54           # DW_AT_location
	.byte	6                       # Abbrev [6] 0x181:0xf DW_TAG_formal_parameter
	.long	.Linfo_string25         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	14                      # DW_AT_decl_line
	.long	503                     # DW_AT_type
	.long	.Ldebug_loc56           # DW_AT_location
	.byte	0                       # End Of Children Mark
	.byte	0                       # End Of Children Mark
	.byte	18                      # Abbrev [18] 0x192:0x4b DW_TAG_subprogram
	.long	.Linfo_string15         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	28                      # DW_AT_decl_line
                                        # DW_AT_prototyped
	.long	.Lsection_info+56       # DW_AT_type
                                        # DW_AT_external
	.quad	.Lfunc_begin8           # DW_AT_low_pc
	.quad	.Lfunc_end8             # DW_AT_high_pc
	.byte	1                       # DW_AT_frame_base
	.byte	87
                                        # DW_AT_APPLE_omit_frame_ptr
	.byte	20                      # Abbrev [20] 0x1af:0xf DW_TAG_formal_parameter
	.long	.Linfo_string30         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	28                      # DW_AT_decl_line
	.long	502                     # DW_AT_type
	.byte	3                       # DW_AT_location
	.byte	145
	.asciz	"\340"
	.byte	20                      # Abbrev [20] 0x1be:0xf DW_TAG_formal_parameter
	.long	.Linfo_string25         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	28                      # DW_AT_decl_line
	.long	503                     # DW_AT_type
	.byte	3                       # DW_AT_location
	.byte	145
	.asciz	"\330"
	.byte	6                       # Abbrev [6] 0x1cd:0xf DW_TAG_formal_parameter
	.long	.Linfo_string31         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	28                      # DW_AT_decl_line
	.long	521                     # DW_AT_type
	.long	.Ldebug_loc58           # DW_AT_location
	.byte	0                       # End Of Children Mark
	.byte	21                      # Abbrev [21] 0x1dd:0x19 DW_TAG_subprogram
	.long	.Linfo_string16         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	71                      # DW_AT_decl_line
                                        # DW_AT_external
	.quad	.Lfunc_begin9           # DW_AT_low_pc
	.quad	.Lfunc_end9             # DW_AT_high_pc
	.byte	1                       # DW_AT_frame_base
	.byte	87
                                        # DW_AT_APPLE_omit_frame_ptr
	.byte	22                      # Abbrev [22] 0x1f6:0x1 DW_TAG_pointer_type
	.byte	23                      # Abbrev [23] 0x1f7:0xb DW_TAG_typedef
	.long	514                     # DW_AT_type
	.long	.Linfo_string27         # DW_AT_name
	.byte	2                       # DW_AT_decl_file
	.byte	42                      # DW_AT_decl_line
	.byte	4                       # Abbrev [4] 0x202:0x7 DW_TAG_base_type
	.long	.Linfo_string26         # DW_AT_name
	.byte	7                       # DW_AT_encoding
	.byte	8                       # DW_AT_byte_size
	.byte	4                       # Abbrev [4] 0x209:0x7 DW_TAG_base_type
	.long	.Linfo_string32         # DW_AT_name
	.byte	7                       # DW_AT_encoding
	.byte	4                       # DW_AT_byte_size
	.byte	0                       # End Of Children Mark
.L.debug_info_end1:
	.section	.debug_abbrev,"",@progbits
.L.debug_abbrev_begin:
	.byte	1                       # Abbreviation Code
	.byte	17                      # DW_TAG_compile_unit
	.byte	1                       # DW_CHILDREN_yes
	.byte	37                      # DW_AT_producer
	.byte	14                      # DW_FORM_strp
	.byte	19                      # DW_AT_language
	.byte	5                       # DW_FORM_data2
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	17                      # DW_AT_low_pc
	.byte	1                       # DW_FORM_addr
	.byte	16                      # DW_AT_stmt_list
	.byte	23                      # DW_FORM_sec_offset
	.byte	27                      # DW_AT_comp_dir
	.byte	14                      # DW_FORM_strp
	.ascii	"\341\177"              # DW_AT_APPLE_optimized
	.byte	25                      # DW_FORM_flag_present
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	2                       # Abbreviation Code
	.byte	52                      # DW_TAG_variable
	.byte	0                       # DW_CHILDREN_no
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	28                      # DW_AT_const_value
	.byte	13                      # DW_FORM_sdata
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	3                       # Abbreviation Code
	.byte	38                      # DW_TAG_const_type
	.byte	0                       # DW_CHILDREN_no
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	4                       # Abbreviation Code
	.byte	36                      # DW_TAG_base_type
	.byte	0                       # DW_CHILDREN_no
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	62                      # DW_AT_encoding
	.byte	11                      # DW_FORM_data1
	.byte	11                      # DW_AT_byte_size
	.byte	11                      # DW_FORM_data1
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	5                       # Abbreviation Code
	.byte	46                      # DW_TAG_subprogram
	.byte	1                       # DW_CHILDREN_yes
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	39                      # DW_AT_prototyped
	.byte	25                      # DW_FORM_flag_present
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	63                      # DW_AT_external
	.byte	25                      # DW_FORM_flag_present
	.byte	17                      # DW_AT_low_pc
	.byte	1                       # DW_FORM_addr
	.byte	18                      # DW_AT_high_pc
	.byte	1                       # DW_FORM_addr
	.byte	64                      # DW_AT_frame_base
	.byte	10                      # DW_FORM_block1
	.ascii	"\347\177"              # DW_AT_APPLE_omit_frame_ptr
	.byte	25                      # DW_FORM_flag_present
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	6                       # Abbreviation Code
	.byte	5                       # DW_TAG_formal_parameter
	.byte	0                       # DW_CHILDREN_no
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	2                       # DW_AT_location
	.byte	23                      # DW_FORM_sec_offset
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	7                       # Abbreviation Code
	.byte	52                      # DW_TAG_variable
	.byte	0                       # DW_CHILDREN_no
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	2                       # DW_AT_location
	.byte	23                      # DW_FORM_sec_offset
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	8                       # Abbreviation Code
	.byte	11                      # DW_TAG_lexical_block
	.byte	1                       # DW_CHILDREN_yes
	.byte	17                      # DW_AT_low_pc
	.byte	1                       # DW_FORM_addr
	.byte	18                      # DW_AT_high_pc
	.byte	1                       # DW_FORM_addr
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	9                       # Abbreviation Code
	.byte	52                      # DW_TAG_variable
	.byte	0                       # DW_CHILDREN_no
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	28                      # DW_AT_const_value
	.byte	13                      # DW_FORM_sdata
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	10                      # Abbreviation Code
	.byte	46                      # DW_TAG_subprogram
	.byte	1                       # DW_CHILDREN_yes
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	39                      # DW_AT_prototyped
	.byte	25                      # DW_FORM_flag_present
	.byte	63                      # DW_AT_external
	.byte	25                      # DW_FORM_flag_present
	.byte	17                      # DW_AT_low_pc
	.byte	1                       # DW_FORM_addr
	.byte	18                      # DW_AT_high_pc
	.byte	1                       # DW_FORM_addr
	.byte	64                      # DW_AT_frame_base
	.byte	10                      # DW_FORM_block1
	.ascii	"\347\177"              # DW_AT_APPLE_omit_frame_ptr
	.byte	25                      # DW_FORM_flag_present
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	11                      # Abbreviation Code
	.byte	11                      # DW_TAG_lexical_block
	.byte	1                       # DW_CHILDREN_yes
	.byte	85                      # DW_AT_ranges
	.byte	6                       # DW_FORM_data4
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	12                      # Abbreviation Code
	.byte	46                      # DW_TAG_subprogram
	.byte	1                       # DW_CHILDREN_yes
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	63                      # DW_AT_external
	.byte	25                      # DW_FORM_flag_present
	.byte	17                      # DW_AT_low_pc
	.byte	1                       # DW_FORM_addr
	.byte	18                      # DW_AT_high_pc
	.byte	1                       # DW_FORM_addr
	.byte	64                      # DW_AT_frame_base
	.byte	10                      # DW_FORM_block1
	.ascii	"\347\177"              # DW_AT_APPLE_omit_frame_ptr
	.byte	25                      # DW_FORM_flag_present
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	13                      # Abbreviation Code
	.byte	52                      # DW_TAG_variable
	.byte	0                       # DW_CHILDREN_no
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	2                       # DW_AT_location
	.byte	10                      # DW_FORM_block1
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	14                      # Abbreviation Code
	.byte	52                      # DW_TAG_variable
	.byte	0                       # DW_CHILDREN_no
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	15                      # Abbreviation Code
	.byte	46                      # DW_TAG_subprogram
	.byte	1                       # DW_CHILDREN_yes
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	39                      # DW_AT_prototyped
	.byte	25                      # DW_FORM_flag_present
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	17                      # DW_AT_low_pc
	.byte	1                       # DW_FORM_addr
	.byte	18                      # DW_AT_high_pc
	.byte	1                       # DW_FORM_addr
	.byte	64                      # DW_AT_frame_base
	.byte	10                      # DW_FORM_block1
	.ascii	"\347\177"              # DW_AT_APPLE_omit_frame_ptr
	.byte	25                      # DW_FORM_flag_present
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	16                      # Abbreviation Code
	.byte	15                      # DW_TAG_pointer_type
	.byte	0                       # DW_CHILDREN_no
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	17                      # Abbreviation Code
	.byte	46                      # DW_TAG_subprogram
	.byte	1                       # DW_CHILDREN_yes
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	39                      # DW_AT_prototyped
	.byte	25                      # DW_FORM_flag_present
	.byte	73                      # DW_AT_type
	.byte	16                      # DW_FORM_ref_addr
	.byte	63                      # DW_AT_external
	.byte	25                      # DW_FORM_flag_present
	.byte	17                      # DW_AT_low_pc
	.byte	1                       # DW_FORM_addr
	.byte	18                      # DW_AT_high_pc
	.byte	1                       # DW_FORM_addr
	.byte	64                      # DW_AT_frame_base
	.byte	10                      # DW_FORM_block1
	.ascii	"\347\177"              # DW_AT_APPLE_omit_frame_ptr
	.byte	25                      # DW_FORM_flag_present
	.byte	32                      # DW_AT_inline
	.byte	11                      # DW_FORM_data1
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	18                      # Abbreviation Code
	.byte	46                      # DW_TAG_subprogram
	.byte	1                       # DW_CHILDREN_yes
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	39                      # DW_AT_prototyped
	.byte	25                      # DW_FORM_flag_present
	.byte	73                      # DW_AT_type
	.byte	16                      # DW_FORM_ref_addr
	.byte	63                      # DW_AT_external
	.byte	25                      # DW_FORM_flag_present
	.byte	17                      # DW_AT_low_pc
	.byte	1                       # DW_FORM_addr
	.byte	18                      # DW_AT_high_pc
	.byte	1                       # DW_FORM_addr
	.byte	64                      # DW_AT_frame_base
	.byte	10                      # DW_FORM_block1
	.ascii	"\347\177"              # DW_AT_APPLE_omit_frame_ptr
	.byte	25                      # DW_FORM_flag_present
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	19                      # Abbreviation Code
	.byte	29                      # DW_TAG_inlined_subroutine
	.byte	1                       # DW_CHILDREN_yes
	.byte	49                      # DW_AT_abstract_origin
	.byte	19                      # DW_FORM_ref4
	.byte	17                      # DW_AT_low_pc
	.byte	1                       # DW_FORM_addr
	.byte	18                      # DW_AT_high_pc
	.byte	1                       # DW_FORM_addr
	.byte	88                      # DW_AT_call_file
	.byte	11                      # DW_FORM_data1
	.byte	89                      # DW_AT_call_line
	.byte	11                      # DW_FORM_data1
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	20                      # Abbreviation Code
	.byte	5                       # DW_TAG_formal_parameter
	.byte	0                       # DW_CHILDREN_no
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	2                       # DW_AT_location
	.byte	10                      # DW_FORM_block1
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	21                      # Abbreviation Code
	.byte	46                      # DW_TAG_subprogram
	.byte	0                       # DW_CHILDREN_no
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	63                      # DW_AT_external
	.byte	25                      # DW_FORM_flag_present
	.byte	17                      # DW_AT_low_pc
	.byte	1                       # DW_FORM_addr
	.byte	18                      # DW_AT_high_pc
	.byte	1                       # DW_FORM_addr
	.byte	64                      # DW_AT_frame_base
	.byte	10                      # DW_FORM_block1
	.ascii	"\347\177"              # DW_AT_APPLE_omit_frame_ptr
	.byte	25                      # DW_FORM_flag_present
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	22                      # Abbreviation Code
	.byte	15                      # DW_TAG_pointer_type
	.byte	0                       # DW_CHILDREN_no
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	23                      # Abbreviation Code
	.byte	22                      # DW_TAG_typedef
	.byte	0                       # DW_CHILDREN_no
	.byte	73                      # DW_AT_type
	.byte	19                      # DW_FORM_ref4
	.byte	3                       # DW_AT_name
	.byte	14                      # DW_FORM_strp
	.byte	58                      # DW_AT_decl_file
	.byte	11                      # DW_FORM_data1
	.byte	59                      # DW_AT_decl_line
	.byte	11                      # DW_FORM_data1
	.byte	0                       # EOM(1)
	.byte	0                       # EOM(2)
	.byte	0                       # EOM(3)
.L.debug_abbrev_end:
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
	.quad	.Lfunc_begin0
	.quad	.Ltmp22
.Lset0 = .Ltmp286-.Ltmp285              # Loc expr size
	.short	.Lset0
.Ltmp285:
	.byte	119                     # DW_OP_breg7
	.ascii	"\260\n"
.Ltmp286:
	.quad	0
	.quad	0
.Ldebug_loc2:
	.quad	.Lfunc_begin0
	.quad	.Ltmp22
.Lset1 = .Ltmp288-.Ltmp287              # Loc expr size
	.short	.Lset1
.Ltmp287:
	.byte	119                     # DW_OP_breg7
	.ascii	"\250\n"
.Ltmp288:
	.quad	0
	.quad	0
.Ldebug_loc4:
	.quad	.Lfunc_begin0
	.quad	.Ltmp22
.Lset2 = .Ltmp290-.Ltmp289              # Loc expr size
	.short	.Lset2
.Ltmp289:
	.byte	119                     # DW_OP_breg7
	.ascii	"\244\n"
.Ltmp290:
	.quad	0
	.quad	0
.Ldebug_loc6:
	.quad	.Ltmp21
	.quad	.Ltmp25
.Lset3 = .Ltmp292-.Ltmp291              # Loc expr size
	.short	.Lset3
.Ltmp291:
	.byte	16                      # DW_OP_constu
	.byte	0
.Ltmp292:
	.quad	.Ltmp25
	.quad	.Ltmp26
.Lset4 = .Ltmp294-.Ltmp293              # Loc expr size
	.short	.Lset4
.Ltmp293:
	.byte	119                     # DW_OP_breg7
	.ascii	"\264\003"
.Ltmp294:
	.quad	0
	.quad	0
.Ldebug_loc9:
	.quad	.Lfunc_begin1
	.quad	.Ltmp52
.Lset5 = .Ltmp296-.Ltmp295              # Loc expr size
	.short	.Lset5
.Ltmp295:
	.byte	119                     # DW_OP_breg7
	.ascii	"\210\004"
.Ltmp296:
	.quad	0
	.quad	0
.Ldebug_loc11:
	.quad	.Lfunc_begin1
	.quad	.Ltmp52
.Lset6 = .Ltmp298-.Ltmp297              # Loc expr size
	.short	.Lset6
.Ltmp297:
	.byte	119                     # DW_OP_breg7
	.ascii	"\200\004"
.Ltmp298:
	.quad	0
	.quad	0
.Ldebug_loc13:
	.quad	.Lfunc_begin1
	.quad	.Ltmp52
.Lset7 = .Ltmp300-.Ltmp299              # Loc expr size
	.short	.Lset7
.Ltmp299:
	.byte	119                     # DW_OP_breg7
	.ascii	"\344\004"
.Ltmp300:
	.quad	0
	.quad	0
.Ldebug_loc15:
	.quad	.Lfunc_begin2
	.quad	.Ltmp79
.Lset8 = .Ltmp302-.Ltmp301              # Loc expr size
	.short	.Lset8
.Ltmp301:
	.byte	119                     # DW_OP_breg7
	.ascii	"\300\022"
.Ltmp302:
	.quad	0
	.quad	0
.Ldebug_loc17:
	.quad	.Lfunc_begin2
	.quad	.Ltmp79
.Lset9 = .Ltmp304-.Ltmp303              # Loc expr size
	.short	.Lset9
.Ltmp303:
	.byte	119                     # DW_OP_breg7
	.ascii	"\270\022"
.Ltmp304:
	.quad	0
	.quad	0
.Ldebug_loc19:
	.quad	.Lfunc_begin2
	.quad	.Ltmp77
.Lset10 = .Ltmp306-.Ltmp305             # Loc expr size
	.short	.Lset10
.Ltmp305:
	.byte	119                     # DW_OP_breg7
	.ascii	"\244\023"
.Ltmp306:
	.quad	0
	.quad	0
.Ldebug_loc21:
	.quad	.Lfunc_begin3
	.quad	.Ltmp109
.Lset11 = .Ltmp308-.Ltmp307             # Loc expr size
	.short	.Lset11
.Ltmp307:
	.byte	119                     # DW_OP_breg7
	.ascii	"\370\002"
.Ltmp308:
	.quad	0
	.quad	0
.Ldebug_loc23:
	.quad	.Ltmp134
	.quad	.Ltmp135
.Lset12 = .Ltmp310-.Ltmp309             # Loc expr size
	.short	.Lset12
.Ltmp309:
	.byte	119                     # DW_OP_breg7
	.ascii	"\364\031"
.Ltmp310:
	.quad	.Ltmp136
	.quad	.Ltmp137
.Lset13 = .Ltmp312-.Ltmp311             # Loc expr size
	.short	.Lset13
.Ltmp311:
	.byte	119                     # DW_OP_breg7
	.ascii	"\314\025"
.Ltmp312:
	.quad	0
	.quad	0
.Ldebug_loc26:
	.quad	.Ltmp150
	.quad	.Ltmp153
.Lset14 = .Ltmp314-.Ltmp313             # Loc expr size
	.short	.Lset14
.Ltmp313:
	.byte	119                     # DW_OP_breg7
	.ascii	"\254\b"
.Ltmp314:
	.quad	0
	.quad	0
.Ldebug_loc28:
	.quad	.Lfunc_begin5
	.quad	.Ltmp182
.Lset15 = .Ltmp316-.Ltmp315             # Loc expr size
	.short	.Lset15
.Ltmp315:
	.byte	119                     # DW_OP_breg7
	.ascii	"\230\004"
.Ltmp316:
	.quad	0
	.quad	0
.Ldebug_loc30:
	.quad	.Lfunc_begin5
	.quad	.Ltmp182
.Lset16 = .Ltmp318-.Ltmp317             # Loc expr size
	.short	.Lset16
.Ltmp317:
	.byte	119                     # DW_OP_breg7
	.ascii	"\360\004"
.Ltmp318:
	.quad	0
	.quad	0
.Ldebug_loc32:
	.quad	.Lfunc_begin5
	.quad	.Ltmp182
.Lset17 = .Ltmp320-.Ltmp319             # Loc expr size
	.short	.Lset17
.Ltmp319:
	.byte	119                     # DW_OP_breg7
	.ascii	"\210\004"
.Ltmp320:
	.quad	0
	.quad	0
.Ldebug_loc34:
	.quad	.Lfunc_begin6
	.quad	.Ltmp198
.Lset18 = .Ltmp322-.Ltmp321             # Loc expr size
	.short	.Lset18
.Ltmp321:
	.byte	119                     # DW_OP_breg7
	.asciz	"\330"
.Ltmp322:
	.quad	0
	.quad	0
.Ldebug_loc36:
	.quad	.Lfunc_begin6
	.quad	.Ltmp198
.Lset19 = .Ltmp324-.Ltmp323             # Loc expr size
	.short	.Lset19
.Ltmp323:
	.byte	119                     # DW_OP_breg7
	.asciz	"\320"
.Ltmp324:
	.quad	0
	.quad	0
.Ldebug_loc38:
	.quad	.Lfunc_begin6
	.quad	.Ltmp198
.Lset20 = .Ltmp326-.Ltmp325             # Loc expr size
	.short	.Lset20
.Ltmp325:
	.byte	119                     # DW_OP_breg7
	.asciz	"\310"
.Ltmp326:
	.quad	0
	.quad	0
.Ldebug_loc40:
	.quad	.Ltmp197
	.quad	.Ltmp198
.Lset21 = .Ltmp328-.Ltmp327             # Loc expr size
	.short	.Lset21
.Ltmp327:
	.byte	119                     # DW_OP_breg7
	.asciz	"\330"
.Ltmp328:
	.quad	0
	.quad	0
.Ldebug_loc42:
	.quad	.Ltmp197
	.quad	.Ltmp198
.Lset22 = .Ltmp330-.Ltmp329             # Loc expr size
	.short	.Lset22
.Ltmp329:
	.byte	119                     # DW_OP_breg7
	.asciz	"\320"
.Ltmp330:
	.quad	0
	.quad	0
.Ldebug_loc44:
	.quad	.Ltmp197
	.quad	.Ltmp198
.Lset23 = .Ltmp332-.Ltmp331             # Loc expr size
	.short	.Lset23
.Ltmp331:
	.byte	119                     # DW_OP_breg7
	.asciz	"\310"
.Ltmp332:
	.quad	0
	.quad	0
.Ldebug_loc46:
	.quad	.Lfunc_begin7
	.quad	.Ltmp215
.Lset24 = .Ltmp334-.Ltmp333             # Loc expr size
	.short	.Lset24
.Ltmp333:
	.byte	119                     # DW_OP_breg7
	.asciz	"\330"
.Ltmp334:
	.quad	0
	.quad	0
.Ldebug_loc48:
	.quad	.Lfunc_begin7
	.quad	.Ltmp215
.Lset25 = .Ltmp336-.Ltmp335             # Loc expr size
	.short	.Lset25
.Ltmp335:
	.byte	119                     # DW_OP_breg7
	.asciz	"\320"
.Ltmp336:
	.quad	0
	.quad	0
.Ldebug_loc50:
	.quad	.Lfunc_begin7
	.quad	.Ltmp215
.Lset26 = .Ltmp338-.Ltmp337             # Loc expr size
	.short	.Lset26
.Ltmp337:
	.byte	119                     # DW_OP_breg7
	.asciz	"\310"
.Ltmp338:
	.quad	0
	.quad	0
.Ldebug_loc52:
	.quad	.Ltmp214
	.quad	.Ltmp215
.Lset27 = .Ltmp340-.Ltmp339             # Loc expr size
	.short	.Lset27
.Ltmp339:
	.byte	119                     # DW_OP_breg7
	.asciz	"\330"
.Ltmp340:
	.quad	0
	.quad	0
.Ldebug_loc54:
	.quad	.Ltmp214
	.quad	.Ltmp215
.Lset28 = .Ltmp342-.Ltmp341             # Loc expr size
	.short	.Lset28
.Ltmp341:
	.byte	119                     # DW_OP_breg7
	.asciz	"\320"
.Ltmp342:
	.quad	0
	.quad	0
.Ldebug_loc56:
	.quad	.Ltmp214
	.quad	.Ltmp215
.Lset29 = .Ltmp344-.Ltmp343             # Loc expr size
	.short	.Lset29
.Ltmp343:
	.byte	119                     # DW_OP_breg7
	.asciz	"\310"
.Ltmp344:
	.quad	0
	.quad	0
.Ldebug_loc58:
	.quad	.Lfunc_begin8
	.quad	.Ltmp240
.Lset30 = .Ltmp346-.Ltmp345             # Loc expr size
	.short	.Lset30
.Ltmp345:
	.byte	119                     # DW_OP_breg7
	.asciz	"\324"
.Ltmp346:
	.quad	0
	.quad	0
.Ldebug_loc60:
	.section	.debug_aranges,"",@progbits
	.long	44                      # Length of ARange Set
	.short	2                       # DWARF Arange version number
	.long	.L.debug_info_begin0    # Offset Into Debug Info Section
	.byte	8                       # Address Size (in bytes)
	.byte	0                       # Segment Size (in bytes)
	.byte	255
	.byte	255
	.byte	255
	.byte	255
	.quad	.Lfunc_begin0
.Lset31 = .Lfunc_begin5-.Lfunc_begin0
	.quad	.Lset31
	.quad	0                       # ARange terminator
	.quad	0
	.long	44                      # Length of ARange Set
	.short	2                       # DWARF Arange version number
	.long	.L.debug_info_begin1    # Offset Into Debug Info Section
	.byte	8                       # Address Size (in bytes)
	.byte	0                       # Segment Size (in bytes)
	.byte	255
	.byte	255
	.byte	255
	.byte	255
	.quad	.Lfunc_begin5
.Lset32 = .Ldebug_end0-.Lfunc_begin5
	.quad	.Lset32
	.quad	0                       # ARange terminator
	.quad	0
	.section	.debug_ranges,"",@progbits
	.quad	.Ltmp78
	.quad	.Ltmp79
	.quad	.Ltmp82
	.quad	.Ltmp83
	.quad	0
	.quad	0
	.quad	.Ltmp80
	.quad	.Ltmp81
	.quad	.Ltmp84
	.quad	.Ltmp85
	.quad	0
	.quad	0
	.quad	.Ltmp138
	.quad	.Ltmp141
	.quad	.Ltmp142
	.quad	.Ltmp144
	.quad	0
	.quad	0
	.section	.debug_macinfo,"",@progbits
	.section	.debug_pubnames,"",@progbits
.Lset33 = .Lpubnames_end1-.Lpubnames_begin1 # Length of Public Names Info
	.long	.Lset33
.Lpubnames_begin1:
	.short	2                       # DWARF Version
	.long	.L.debug_info_begin1    # Offset of Compilation Unit Info
.Lset34 = .L.debug_info_end1-.L.debug_info_begin1 # Compilation Unit Length
	.long	.Lset34
	.long	114                     # DIE offset
	.asciz	"dmaLoad"               # External Name
	.long	402                     # DIE offset
	.asciz	"setReadyBits"          # External Name
	.long	477                     # DIE offset
	.asciz	"dmaFence"              # External Name
	.long	38                      # DIE offset
	.asciz	"_dmaImpl3"             # External Name
	.long	258                     # DIE offset
	.asciz	"dmaStore"              # External Name
	.long	0                       # End Mark
.Lpubnames_end1:
.Lset35 = .Lpubnames_end0-.Lpubnames_begin0 # Length of Public Names Info
	.long	.Lset35
.Lpubnames_begin0:
	.short	2                       # DWARF Version
	.long	.L.debug_info_begin0    # Offset of Compilation Unit Info
.Lset36 = .L.debug_info_end0-.L.debug_info_begin0 # Compilation Unit Length
	.long	.Lset36
	.long	511                     # DIE offset
	.asciz	"soft_plus"             # External Name
	.long	284                     # DIE offset
	.asciz	"big"                   # External Name
	.long	183                     # DIE offset
	.asciz	"s1"                    # External Name
	.long	391                     # DIE offset
	.asciz	"main"                  # External Name
	.long	63                      # DIE offset
	.asciz	"test_stores"           # External Name
	.long	0                       # End Mark
.Lpubnames_end0:
	.section	.debug_pubtypes,"",@progbits
.Lset37 = .Lpubtypes_end1-.Lpubtypes_begin1 # Length of Public Types Info
	.long	.Lset37
.Lpubtypes_begin1:
	.short	2                       # DWARF Version
	.long	.L.debug_info_begin1    # Offset of Compilation Unit Info
.Lset38 = .L.debug_info_end1-.L.debug_info_begin1 # Compilation Unit Length
	.long	.Lset38
	.long	514                     # DIE offset
	.asciz	"long unsigned int"     # External Name
	.long	521                     # DIE offset
	.asciz	"unsigned int"          # External Name
	.long	56                      # DIE offset
	.asciz	"int"                   # External Name
	.long	503                     # DIE offset
	.asciz	"size_t"                # External Name
	.long	0                       # End Mark
.Lpubtypes_end1:
.Lset39 = .Lpubtypes_end0-.Lpubtypes_begin0 # Length of Public Types Info
	.long	.Lset39
.Lpubtypes_begin0:
	.short	2                       # DWARF Version
	.long	.L.debug_info_begin0    # Offset of Compilation Unit Info
.Lset40 = .L.debug_info_end0-.L.debug_info_begin0 # Compilation Unit Length
	.long	.Lset40
	.long	556                     # DIE offset
	.asciz	"float"                 # External Name
	.long	56                      # DIE offset
	.asciz	"int"                   # External Name
	.long	0                       # End Mark
.Lpubtypes_end0:

	.ident	"clang version 3.4.2 (tags/RELEASE_34/dot2-final)"
	.ident	"clang version 3.4.2 (tags/RELEASE_34/dot2-final)"
	.ident	"clang version 3.4.2 (tags/RELEASE_34/dot2-final)"
	.section	".note.GNU-stack","",@progbits
